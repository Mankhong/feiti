<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once('Model_utils.php');

class Fg_holiday_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->table_fg_holiday = 'fg_holiday';
        $this->table_fg_holiday_type = 'fg_holiday_type';
    }

    public function get_holidays($id = '')
    {
        $this->db->select('*');
        $this->db->select('to_char(hd.date, \'DD/MM/YYYY\') as fm_date');
        $this->db->select(Model_utils::sql_last_update_date('hd'));
        $this->db->select(Model_utils::sql_last_update_user('hd'));
        $this->db->from($this->table_fg_holiday . ' as hd');

        if (!empty($id)) {
            $this->db->where('id', $id);
        }

        return $this->db->get()->result_array();
    }

    public function get_joined_holidays($id = '')
    {
        $this->db->select('hd.*');
        $this->db->select('hdt.id as holiday_type_id, hdt.name as holiday_type_name');
        $this->db->select('to_char(hd.date, \'DD/MM/YYYY\') as fm_date');
        $this->db->select(Model_utils::sql_last_update_date('hd'));
        $this->db->select(Model_utils::sql_last_update_user('hd'));
        $this->db->from($this->table_fg_holiday . ' as hd');
        $this->db->join($this->table_fg_holiday_type . ' as hdt', 'hdt.id = hd.type');

        if (!empty($id)) {
            $this->db->where('hd.id', $id);
        }

        $this->db->order_by('hd.date');

        return $this->db->get()->result_array();
    }

    public function insert_holiday($data, $uid)
    {
        $id = $this->common_model->gen_new_id($this->table_fg_holiday, 'id');
        $data['id'] = $id;
        $this->db->set('create_uid', $uid);
        $this->db->set('create_date', 'NOW()', FALSE);
        return $this->db->insert($this->table_fg_holiday, $data);
    }

    public function update_holiday($id, $data, $uid)
    {
        $this->db->where('id', $id);
        $this->db->set('update_uid', $uid);
        $this->db->set('update_date', 'NOW()', FALSE);
        return $this->db->update($this->table_fg_holiday, $data);
    }

    public function delete_holiday($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete($this->table_fg_holiday);
    }

}