<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once('Model_utils.php');

class Fg_time_daily_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->table_checkinout = 'CHECKINOUT';
        $this->table_fg_checkinout = 'fg_checkinout';
        $this->table_fg_employee = 'fg_employee';
        $this->table_fg_timesheet = 'fg_timesheet';
        $this->table_fg_shift = 'fg_shift';
        $this->table_fg_monthly_data = 'fg_monthly_data';
        $this->table_fg_holiday = 'fg_holiday';
        $this->table_fg_leave = 'fg_leave';
        $this->table_fg_leave_type = 'fg_leave_type';
        $this->table_fg_duration = 'fg_duration';
        $this->table_fg_department = 'fg_department';
    }

    public function get_timesheet_daily($date = NULL, $department = NULL, $employee_type = NULL, $employee_no = NULL)
    {
        $leave_type_sql = 'select concat(lvt2.code, \' (\', dr2.name, \')\') from ' . $this->table_fg_leave 
            . ' as lv2 join ' . $this->table_fg_leave_type . ' as lvt2 on lvt2.code = lv2.type join ' . $this->table_fg_duration . ' as dr2 on dr2.id = lv2.duration'
            . ' where lv2.employee_id = em.id and lv2.from_date <= ts.date and lv2.to_date >= ts.date order by lv2.create_date asc LIMIT 1';

        $this->db->select('em.ems_no, concat(em.firstname_th, \' \', em.lastname_th) as fullname_th, em.type as employee_type');
        $this->db->select('dp.id as department_id, concat(dp.name, \' (\', dp.id, \')\') as department_name');
        $this->db->select('sh.id as shift_id, sh.name as shift_name');
        $this->db->select('to_char(ts.date, \'DD/MM/YYYY\') as date, ts.start_work, ts.end_work');
        $this->db->select('(' . $leave_type_sql . ') as leave_type');
        $this->db->from($this->table_fg_timesheet . ' as ts');
        $this->db->join($this->table_fg_employee . ' as em', ' em.id = ts.sensor_user_id');
        $this->db->join($this->table_fg_department . ' as dp', 'dp.id = em.department_id', 'left');
        $this->db->join($this->table_fg_shift . ' as sh', 'sh.id = em.shift', 'left');
        $this->db->where('ts.date', $date);
        $this->db->where('(em.resign_date is null or (em.resign_date > ts.date))');
        
        if (!empty($department)) {
            $this->db->where('dp.id', $department);
        }

        if (!empty($employee_type)) {
            $this->db->where('em.type', $employee_type);
        }

        if (!empty($employee_no)) {
            $this->db->where('em.ems_no', $employee_no);
        }

        $query = $this->db->get();
        return $query->result_array();
    }
}