<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once('Model_utils.php');

class Fg_summary_leave_department_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->table_fg_checkinout = 'fg_checkinout';
        $this->table_fg_employee = 'fg_employee';
        $this->table_fg_timesheet = 'fg_timesheet';
        $this->table_fg_shift = 'fg_shift';
        $this->table_fg_monthly_data = 'fg_monthly_data';
        $this->table_fg_department = 'fg_department';
        $this->table_fg_position = 'fg_position';
        $this->table_fg_leave = 'fg_leave';
        $this->table_fg_leave_type = 'fg_leave_type';
        $this->table_fg_holiday = 'fg_holiday';
        $this->table_fg_holiday_type = 'fg_holiday_type';
    }

    public function get_summary_data($start_date = NULL, $end_date = NULL, $leave_type = NULL)
    {
        $dp1 = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and em.department_id = \'Office\'';
        $dp2 = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and em.department_id = \'QA\'';
        $dp3 = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and em.department_id = \'Production-control\'';
        $dp4 = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and em.department_id = \'Injection\'';
        $dp5 = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and em.department_id = \'QC\'';
        $dp6 = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and em.department_id = \'FG\'';
        $dp7 = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and em.department_id = \'Assembly\'';
        $dp8 = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and em.department_id = \'Rework\'';
        $dp9 = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and em.department_id = \'Mold\'';
        $dp10 = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and em.department_id = \'Co-ordination\'';
        $dp11 = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and em.department_id = \'Store\'';
        $dp12 = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and em.department_id = \'Material\'';
        $dp13 = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and em.department_id = \'Accounting\'';
        $dp14 = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and em.department_id = \'Personnel\'';
        $dp15 = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and em.department_id = \'Purchase\'';
        $dp16 = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and em.department_id = \'B.O.I\'';
        $dp17 = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and em.department_id = \'DCC\'';

        $this->db->select('COALESCE(sum((' . $dp1 . ')), 0) as dp1');
        $this->db->select('COALESCE(sum((' . $dp2 . ')), 0) as dp2');
        $this->db->select('COALESCE(sum((' . $dp3 . ')), 0) as dp3');
        $this->db->select('COALESCE(sum((' . $dp4 . ')), 0) as dp4');
        $this->db->select('COALESCE(sum((' . $dp5 . ')), 0) as dp5');
        $this->db->select('COALESCE(sum((' . $dp6 . ')), 0) as dp6');
        $this->db->select('COALESCE(sum((' . $dp7 . ')), 0) as dp7');
        $this->db->select('COALESCE(sum((' . $dp8 . ')), 0) as dp8');
        $this->db->select('COALESCE(sum((' . $dp9 . ')), 0) as dp9');
        $this->db->select('COALESCE(sum((' . $dp10 . ')), 0) as dp10');
        $this->db->select('COALESCE(sum((' . $dp11 . ')), 0) as dp11');
        $this->db->select('COALESCE(sum((' . $dp12 . ')), 0) as dp12');
        $this->db->select('COALESCE(sum((' . $dp13 . ')), 0) as dp13');
        $this->db->select('COALESCE(sum((' . $dp14 . ')), 0) as dp14');
        $this->db->select('COALESCE(sum((' . $dp15 . ')), 0) as dp15');
        $this->db->select('COALESCE(sum((' . $dp16 . ')), 0) as dp16');
        $this->db->select('COALESCE(sum((' . $dp17 . ')), 0) as dp17');
        $this->db->from($this->table_fg_leave . ' as lv');
        $this->db->join($this->table_fg_employee . ' as em', 'em.id = lv.employee_id', 'left');

        if (!empty($start_date)) {
            $this->db->where('(lv.from_date >= TO_DATE(\'' . $start_date . '\', \'DD/MM/YYYY\') or lv.to_date >= TO_DATE(\'' . $start_date . '\', \'DD/MM/YYYY\'))');
        }

        if (!empty($end_date)) {
            $this->db->where('(lv.from_date <= TO_DATE(\'' . $end_date . '\', \'DD/MM/YYYY\') or lv.to_date <= TO_DATE(\'' . $end_date . '\', \'DD/MM/YYYY\'))');
        }

        if (!empty($leave_type)) {
            $this->db->where('lv.type', $leave_type);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

}