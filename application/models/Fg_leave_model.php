<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once('Model_utils.php');

class Fg_leave_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->table_fg_leave = 'fg_leave';
        $this->table_fg_leave_type = 'fg_leave_type';
        $this->table_fg_employee = 'fg_employee';
        $this->table_fg_job_position = 'fg_job_position';
        $this->table_fg_duration = 'fg_duration';
    }

    public function get_leaves($id = '')
    {
        $this->db->select('*');
        $this->db->select('to_char(from_date, \'DD/MM/YYYY\') as fm_from_date, to_char(to_date, \'DD/MM/YYYY\') as fm_to_date');
        $this->db->select(Model_utils::sql_last_update_date('lv'));
        $this->db->select(Model_utils::sql_last_update_user('lv'));
        $this->db->from($this->table_fg_leave . ' as lv');

        if (!empty($id)) {
            $this->db->where('id', $id);
        }

        return $this->db->get()->result_array();
    }

    public function get_joined_leaves($id = '')
    {
        
        $this->db->select('lv.*');
        $this->db->select('to_char(lv.from_date, \'DD/MM/YYYY\') as fm_from_date, to_char(lv.to_date, \'DD/MM/YYYY\') as fm_to_date');
        $this->db->select('em.id as employee_id, em.ems_no as employee_no, concat(em.firstname_th, \' \', em.lastname_th) as employee_name');
        $this->db->select('jb.id as job_position_id, jb.name as job_position_name');
        $this->db->select('lvt.code as leave_type_code, concat(lvt.name, \' (\', dr.name, \')\') as leave_type_name');
        $this->db->select(Model_utils::sql_last_update_date('lv'));
        $this->db->select(Model_utils::sql_last_update_user('lv'));
        $this->db->from($this->table_fg_leave . ' as lv');
        $this->db->join($this->table_fg_leave_type . ' as lvt', 'lvt.code = lv.type');
        $this->db->join($this->table_fg_employee . ' as em', 'em.id = lv.employee_id');
        $this->db->join($this->table_fg_job_position . ' as jb', 'jb.id = em.job_position_id');
        $this->db->join($this->table_fg_duration . ' as dr', 'dr.id = lv.duration');

        if (!empty($id)) {
            $this->db->where('lv.id', $id);
        }

        return $this->db->get()->result_array();
    }

    public function insert_leave($data, $uid)
    {
        $id = $this->common_model->gen_new_id($this->table_fg_leave, 'id');
        $data['id'] = $id;
        $this->db->set('create_uid', $uid);
        $this->db->set('create_date', 'NOW()', FALSE);
        return $this->db->insert($this->table_fg_leave, $data);
    }

    public function update_leave($id, $data, $uid)
    {
        $this->db->where('id', $id);
        $this->db->set('update_uid', $uid);
        $this->db->set('update_date', 'NOW()', FALSE);
        return $this->db->update($this->table_fg_leave, $data);
    }

    public function delete_leave($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete($this->table_fg_leave);
    }

    public function has_leave($employee_id, $from_date, $to_date)
    {
        $where = '((lv.from_date <= TO_DATE(\'' . $from_date . '\', \'DD/MM/YYYY\') AND lv.to_date >= TO_DATE(\'' . $from_date . '\', \'DD/MM/YYYY\'))'
                 . ' OR'
                 . '(lv.from_date <= TO_DATE(\'' . $to_date . '\', \'DD/MM/YYYY\') AND lv.to_date >= TO_DATE(\'' . $to_date . '\', \'DD/MM/YYYY\')))';
        $this->db->from($this->table_fg_leave . ' as lv');
        $this->db->where('lv.employee_id', $employee_id);
        $this->db->where($where);
        $query = $this->db->get();

        //echo $this->db->last_query();

        return !empty($query->result_array());
    }
}