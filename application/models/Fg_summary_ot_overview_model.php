<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once('Model_utils.php');

class Fg_summary_ot_overview_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->table_fg_checkinout = 'fg_checkinout';
        $this->table_fg_employee = 'fg_employee';
        $this->table_fg_timesheet = 'fg_timesheet';
        $this->table_fg_shift = 'fg_shift';
        $this->table_fg_monthly_data = 'fg_monthly_data';
        $this->table_fg_department = 'fg_department';
        $this->table_fg_position = 'fg_position';
        $this->table_fg_leave = 'fg_leave';
        $this->table_fg_leave_type = 'fg_leave_type';
        $this->table_fg_holiday = 'fg_holiday';
        $this->table_fg_holiday_type = 'fg_holiday_type';
    }

    public function get_summary_data($year = NULL, $department = NULL)
    {
        $dp_where = ' where em2.department_id = dp.id';

        if (!empty($year)) {
            $dp_where .= ' and ts2.year = ' . $year;
        }

        $m1 = 'select sum(ts2.ot1 + ts2.ot15 + ts2.ot2 + ts2.ot3) from ' . $this->table_fg_timesheet . ' as ts2 join ' . $this->table_fg_employee . ' as em2 on em2.id = ts2.sensor_user_id' . $dp_where . ' and TO_CHAR(ts2.date, \'MM\') = \'01\'';
        $m2 = 'select sum(ts2.ot1 + ts2.ot15 + ts2.ot2 + ts2.ot3) from ' . $this->table_fg_timesheet . ' as ts2 join ' . $this->table_fg_employee . ' as em2 on em2.id = ts2.sensor_user_id' . $dp_where . ' and TO_CHAR(ts2.date, \'MM\') = \'02\'';
        $m3 = 'select sum(ts2.ot1 + ts2.ot15 + ts2.ot2 + ts2.ot3) from ' . $this->table_fg_timesheet . ' as ts2 join ' . $this->table_fg_employee . ' as em2 on em2.id = ts2.sensor_user_id' . $dp_where . ' and TO_CHAR(ts2.date, \'MM\') = \'03\'';
        $m4 = 'select sum(ts2.ot1 + ts2.ot15 + ts2.ot2 + ts2.ot3) from ' . $this->table_fg_timesheet . ' as ts2 join ' . $this->table_fg_employee . ' as em2 on em2.id = ts2.sensor_user_id' . $dp_where . ' and TO_CHAR(ts2.date, \'MM\') = \'04\'';
        $m5 = 'select sum(ts2.ot1 + ts2.ot15 + ts2.ot2 + ts2.ot3) from ' . $this->table_fg_timesheet . ' as ts2 join ' . $this->table_fg_employee . ' as em2 on em2.id = ts2.sensor_user_id' . $dp_where . ' and TO_CHAR(ts2.date, \'MM\') = \'05\'';
        $m6 = 'select sum(ts2.ot1 + ts2.ot15 + ts2.ot2 + ts2.ot3) from ' . $this->table_fg_timesheet . ' as ts2 join ' . $this->table_fg_employee . ' as em2 on em2.id = ts2.sensor_user_id' . $dp_where . ' and TO_CHAR(ts2.date, \'MM\') = \'06\'';
        $m7 = 'select sum(ts2.ot1 + ts2.ot15 + ts2.ot2 + ts2.ot3) from ' . $this->table_fg_timesheet . ' as ts2 join ' . $this->table_fg_employee . ' as em2 on em2.id = ts2.sensor_user_id' . $dp_where . ' and TO_CHAR(ts2.date, \'MM\') = \'07\'';
        $m8 = 'select sum(ts2.ot1 + ts2.ot15 + ts2.ot2 + ts2.ot3) from ' . $this->table_fg_timesheet . ' as ts2 join ' . $this->table_fg_employee . ' as em2 on em2.id = ts2.sensor_user_id' . $dp_where . ' and TO_CHAR(ts2.date, \'MM\') = \'08\'';
        $m9 = 'select sum(ts2.ot1 + ts2.ot15 + ts2.ot2 + ts2.ot3) from ' . $this->table_fg_timesheet . ' as ts2 join ' . $this->table_fg_employee . ' as em2 on em2.id = ts2.sensor_user_id' . $dp_where . ' and TO_CHAR(ts2.date, \'MM\') = \'09\'';
        $m10 = 'select sum(ts2.ot1 + ts2.ot15 + ts2.ot2 + ts2.ot3) from ' . $this->table_fg_timesheet . ' as ts2 join ' . $this->table_fg_employee . ' as em2 on em2.id = ts2.sensor_user_id' . $dp_where . ' and TO_CHAR(ts2.date, \'MM\') = \'10\'';
        $m11 = 'select sum(ts2.ot1 + ts2.ot15 + ts2.ot2 + ts2.ot3) from ' . $this->table_fg_timesheet . ' as ts2 join ' . $this->table_fg_employee . ' as em2 on em2.id = ts2.sensor_user_id' . $dp_where . ' and TO_CHAR(ts2.date, \'MM\') = \'11\'';
        $m12 = 'select sum(ts2.ot1 + ts2.ot15 + ts2.ot2 + ts2.ot3) from ' . $this->table_fg_timesheet . ' as ts2 join ' . $this->table_fg_employee . ' as em2 on em2.id = ts2.sensor_user_id' . $dp_where . ' and TO_CHAR(ts2.date, \'MM\') = \'12\'';

        $this->db->select('sum(COALESCE((' . $m1 . '), 0)) as m1');
        $this->db->select('sum(COALESCE((' . $m2 . '), 0)) as m2');
        $this->db->select('sum(COALESCE((' . $m3 . '), 0)) as m3');
        $this->db->select('sum(COALESCE((' . $m4 . '), 0)) as m4');
        $this->db->select('sum(COALESCE((' . $m5 . '), 0)) as m5');
        $this->db->select('sum(COALESCE((' . $m6 . '), 0)) as m6');
        $this->db->select('sum(COALESCE((' . $m7 . '), 0)) as m7');
        $this->db->select('sum(COALESCE((' . $m8 . '), 0)) as m8');
        $this->db->select('sum(COALESCE((' . $m9 . '), 0)) as m9');
        $this->db->select('sum(COALESCE((' . $m10 . '), 0)) as m10');
        $this->db->select('sum(COALESCE((' . $m11 . '), 0)) as m11');
        $this->db->select('sum(COALESCE((' . $m12 . '), 0)) as m12');
        $this->db->from($this->table_fg_department . ' as dp');

        if (!empty($department)) {
            $this->db->where('dp.id', $department);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

}