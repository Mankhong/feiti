<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once('Model_utils.php');

class Invoice_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->table_invoices = 'invoices';
        $this->table_invoice_products = 'invoice_products';
        $this->table_products = ' products';
        $this->table_order_products = 'order_products';
        $this->table_orders = 'orders';
        $this->table_customers = 'customers';
        $this->table_users = 'users';
        $this->table_unit_price_types = 'unit_price_types';
        $this->table_invoice_types = 'invoice_types';
        $this->table_invoice_product_lots = 'invoice_product_lots';
        $this->table_invoices_history = 'invoices_history';
        $this->table_invoice_products_history = 'invoice_products_history';
        $this->table_invoice_product_lots_history = 'invoice_product_lots_history';
        $this->table_billing_invoices = 'billing_invoices';
        $this->table_billing_invoices_history = 'billing_invoices_history';
    }

    public function get_invoices($id = '')
    {
        $this->db->select('iv.id, iv.delivery_address, iv.customer_id, iv.internal_note, iv.status, iv.invoice_type, iv.refer_invoice_id');
        $this->db->select('to_char(iv.date, \'DD/MM/YYYY\') as date');
        $this->db->select('ivt.name as id_prefix, replace(iv.id, ivt.name, \'\') as id_suffix, ivt.invoice_group');
        $this->db->from($this->table_invoices . ' as iv');
        $this->db->join($this->table_invoice_types. ' as ivt', 'ivt.id = cast(iv.invoice_type as integer)', 'left');

        if (!empty($id)) {
            $this->db->where('iv.id', $id);
        }

        return $this->db->get()->result_array();
    }

    public function get_invoice_products($invoice_id = '')
    {
        // $invoice_quantity_sql = 'COALESCE('
        //     . '(select sum(ivpl2.quantity) from invoice_product_lots ivpl2 where ivpl2.invoice_products_id = ivp.id)'
        //     . ', 0)';
        
        $invoice_quantity_sql = "COALESCE((select sum(ivpl2.quantity)
            from invoice_product_lots ivpl2
            join invoice_products ivp2 on ivp2.id = ivpl2.invoice_products_id
            join order_products odp2 on odp2.order_id = ivp2.order_id and odp2.product_seq = ivp2.product_seq
            where ivp2.product_seq = ivp.product_seq
            and ivp2.order_id = ivp.order_id
            and ivp2.invoice_id = iv.id), 0)";

        $balance_quantity_sql = "COALESCE((select sum(ivpl2.quantity)
            from invoice_product_lots ivpl2
            join invoice_products ivp2 on ivp2.id = ivpl2.invoice_products_id
            join order_products odp2 on odp2.order_id = ivp2.order_id and odp2.product_seq = ivp2.product_seq
            where ivp2.product_seq = ivp.product_seq
            and ivp2.order_id = ivp.order_id
            and ivp2.invoice_id <> iv.id), 0)";

        /*
        $lot_size_sql = "COALESCE((select sum(ivpl2.lot_size)
            from invoice_product_lots ivpl2
            join invoice_products ivp2 on ivp2.id = ivpl2.invoice_products_id
            join order_products odp2 on odp2.order_id = ivp2.order_id and odp2.product_seq = ivp2.product_seq
            where ivp2.product_seq = ivp.product_seq
            and ivp2.order_id = ivp.order_id ), 0)";
            //and ivp2.invoice_id <> iv.id), 0)";
            */
    
        $lot_size_sql = "COALESCE((select sum(ivpl2.lot_size)
            from invoice_product_lots ivpl2
            where ivpl2.invoice_products_id = ivp.id), 0)";
        
        $this->db->select('iv.id as invoice_id, iv.date as invoice_date');
        $this->db->select('cs.id as customer_id, cs.name as customer_name');
        $this->db->select('ivp.id as invoice_products_id, ivp.lot_no, iv.internal_note');
        $this->db->select('(' . $invoice_quantity_sql . ') as invoice_quantity');
        $this->db->select('(' . $lot_size_sql . ') as product_lots_size');
        $this->db->select('odp.quantity, odp.order_id, odp.unit_price');
        $this->db->select('pd.seq as product_seq, pd.id as product_id, pd.desc1, pd.desc2, pd.lot_size ,pd.carton');
        $this->db->select('upt.id as unit_price_type_id, upt.name as unit_price_type_name');
        //$this->db->select('(case when cs.customer_type = 1 then pd.domestic_price else pd.export_price end) as unit_price');
        //$this->db->select('(case when ivp.quantity is not null then odp.quantity - ivp.quantity else odp.quantity end) as balance');
        $this->db->select('(odp.quantity - ' . $balance_quantity_sql . ') as balance');
        $this->db->from($this->table_invoice_products . ' as ivp');
        $this->db->join($this->table_products . ' as pd', 'pd.seq = ivp.product_seq');
        $this->db->join($this->table_invoices . ' as iv', 'iv.id = ivp.invoice_id');
        $this->db->join($this->table_customers . ' as cs', 'cs.id = iv.customer_id');
        $this->db->join($this->table_unit_price_types . ' as upt', 'upt.id = cs.customer_type');
        $this->db->join($this->table_order_products . ' as odp', 'odp.order_id = ivp.order_id and odp.product_seq = ivp.product_seq');
        
        if (!empty($invoice_id)) {
            if (isset($invoice_id)) {
                $this->db->where_in('iv.id', $invoice_id);
            } else {
                $this->db->where('iv.id', $invoice_id);
            }
        }

        $this->db->order_by('invoice_date asc, iv.id asc, pd.id asc, invoice_quantity desc, odp.order_id asc');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_order_products($customer_id, $get_empty_item = FALSE, $invoice_type = '', $invoice_group = '', $order_id = '', $product_id = '', $product_name = '') {
        $balance_sql = "odp.quantity - COALESCE((select sum(ivpl2.quantity)
            from invoice_product_lots ivpl2
            join invoice_products ivp2 on ivp2.id = ivpl2.invoice_products_id
            join order_products odp2 on odp2.order_id = ivp2.order_id and odp2.product_seq = ivp2.product_seq
            where ivp2.product_seq = odp.product_seq
            and ivp2.order_id = odp.order_id), 0)";

        $this->db->select('pd.seq as product_seq, pd.id as product_id, pd.desc1, pd.lot_size');
        $this->db->select('od.id as order_id, od.order_date, odp.quantity, odp.unit_price');
        $this->db->select('(' . $balance_sql . ') as balance');
        $this->db->from($this->table_orders . ' as od');
        $this->db->join($this->table_order_products . ' as odp', 'odp.order_id = od.id');
        $this->db->join($this->table_products . ' as pd', 'pd.seq = odp.product_seq');
        $this->db->join($this->table_customers . ' as cs', 'cs.id = pd.customer_id');
        $this->db->where('od.customer_id', $customer_id);
        //$this->db->where('od.status', 1);

        if ($invoice_group == 'S') {
            //$this->db->where('pd.invoice_type', $invoice_type);
        } else {
            if ($get_empty_item == FALSE) {
                $this->db->where($balance_sql . ' > 0');
            }
        }

        if (!empty($order_id)) {
            $this->db->like('od.id', $order_id);
        }
        if (!empty($product_id)) {
            $this->db->like('pd.id', $product_id);
        }
        if (!empty($product_name)) {
            $this->db->like('pd.desc1', $product_name);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_joined_invoices($id = '', $startDate = '', $dateTimeEnd = '', $invoice_id = '', $customer_name = '')
    {
        $amount_sql = 'select sum(
                        round((select sum(ivpl2.quantity) from invoice_product_lots ivpl2 where ivpl2.invoice_products_id = ivp2.id) 
                        * COALESCE(odp2.unit_price, 0), 2))
                        from ' . $this->table_invoice_products . ' as ivp2
                        join ' . $this->table_order_products . ' as odp2 on odp2.product_seq = ivp2.product_seq and odp2.order_id = ivp2.order_id
                        where ivp2.invoice_id = iv.id';
        
        $vat_sql = 'select cast(cfg.config_value as float) from configs as cfg where cfg.config_key = \'VAT\'';

        $this->db->select('iv.id as invoice_id');
        $this->db->select('to_char(iv.date, \'DD/MM/YYYY\') as invoice_date');
        $this->db->select('cs.id as customer_id, cs.name as customer_name, cs.image_path as customer_image_path');
        $this->db->select('usr.id as user_id, concat(usr.first_name, \' \', last_name) as user_name');
        $this->db->select('(' . $amount_sql . ') as amount');
        $this->db->select('(' . $vat_sql . ') as vat');
        $this->db->select(Model_utils::sql_last_update_date('iv'));
        $this->db->select(Model_utils::sql_last_update_user('iv'));
        //$this->db->select('(' . $unit_price_type_name_sql . ') as unit_price_type_name');
        $this->db->from($this->table_invoices . ' as iv');
        $this->db->join($this->table_customers . ' as cs', 'cs.id = iv.customer_id', 'left');
        $this->db->join($this->table_users . ' as usr', 'usr.id = iv.create_uid');
        $this->db->order_by('iv.id');
        
        if (!empty($id)) {
            $this->db->where('iv.id', $id);
        }
        if (!empty($invoice_id)) {
            $this->db->like('iv.id', $invoice_id);
        }
        
        if (!empty($customer_name)) {
            $this->db->like('cs.name', $customer_name);
        }
        
        if (!empty($startDate)) {
            $this->db->where("iv.date >= to_date('" . $startDate . "', 'DD/MM/YYYY')");
            
        }
        if (!empty($dateTimeEnd)) {
            $this->db->where("iv.date <= to_date('" . $dateTimeEnd . "', 'DD/MM/YYYY')");
        }

        return $this->db->get()->result_array();
    }

    public function has_invoice($id = '')
    {
        $this->db->select('*');
        $this->db->from($this->table_invoices);
        $this->db->where('id', $id);

        return count($this->db->get()->result_array()) > 0;
    }

    public function get_invoice_types($id = '')
    {
        $this->db->select('*');
        $this->db->from($this->table_invoice_types);

        if (!empty($id)) {
            $this->db->where('id', $id);
        }

        $this->db->order_by('id');
        return $this->db->get()->result_array();
    }

    public function insert_invoice_type($data) {
        $this->db->select('COALESCE(MAX(id),0) + 1 as id');
        $this->db->from($this->table_invoice_types);
        $id = $this->db->get()->result_array()[0]['id'];

        $data['id'] = $id;
        $data['used'] = 'Y';

        return $this->db->insert($this->table_invoice_types, $data);
    }

    public function update_invoice_type($id, $used) {
        $this->db->where('id', $id);
        $this->db->set('used', $used);
        return $this->db->update($this->table_invoice_types);
    }

    public function insert_invoice($data, $product_arr, $lotitem_arr, $uid)
    {
        //skip date empty.
        if (empty($data['date'])) {
            $data = array_diff($data, array('date' => ''));
        }

        $data['status'] = 1;
        $data['id'] = $this->get_invoice_type_name($data['invoice_type']) . $data['id_suffix'];
        $data = array_diff($data, array('id_suffix' => $data['id_suffix']));

        $affected_rows = 0;
        $this->db->set('create_uid', $uid);
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->insert($this->table_invoices, $data);
        $affected_rows = $this->db->affected_rows();

        if ($affected_rows > 0) {
            $this->insert_invoice_products($product_arr, $lotitem_arr, $uid);
        }
        return $affected_rows;
    }

    private function get_invoice_type_name($id)
    {
        $this->db->select('name');
        $this->db->from($this->table_invoice_types);
        $this->db->where('used', 'Y');
        $this->db->where('id', $id);
        $result = $this->db->get()->result_array();
        return !empty($result) ? $result[0]['name'] : '';
    }

    public function validate_balances($customer_id, $product_arr, $lotitem_arr)
    {
        $order_products = $this->get_order_products($customer_id, TRUE);
        if (!empty($order_products) && !empty($product_arr) && !empty($lotitem_arr)) {
            for ($i = 0; $i < count($product_arr); $i++) {
                $temp_id = $product_arr[$i]['id'];
                $temp_quantity = intval($product_arr[$i]['temp_quantity']);
                $invoice_product_lot_arr = $this->get_invoice_product_lot_arr($temp_id, $lotitem_arr);
                $order_product = $this->find_order_product($order_products, $product_arr[$i]['product_seq'], $product_arr[$i]['order_id']);
                $quantity = $this->sum_lotitem($invoice_product_lot_arr) - $temp_quantity;
                if ($order_product != NULL && ((intval($order_product['balance']) - $quantity) < 0)) {
                    return FALSE;
                }
            }
        }

        return TRUE;
    }

    private function find_order_product($order_products, $product_seq, $order_id) {
        for ($i = 0; $i < count($order_products); $i++) {
            if ($order_products[$i]['product_seq'] == $product_seq && $order_products[$i]['order_id'] == $order_id) {
                return $order_products[$i];
            }
        }
        return NULL;
    }

    private function sum_lotitem($lotitem_arr) 
    {
        $sum = 0;

        for ($i = 0; $i < count($lotitem_arr); $i++) {
            $sum += intval($lotitem_arr[$i]['quantity']);
        }

        return $sum;
    }

    public function update_invoice($id, $data, $update_product_arr, $insert_product_arr, $delete_product_arr, $lotitem_arr, $uid)
    {
        //skip date empty.
        if (empty($data['date'])) {
            $data = array_diff($data, array('date' => ''));
        }

        $data = array_diff($data, array('id_suffix' => $data['id_suffix']));

        $affected_rows = 0;
        $this->db->where('id', $id);
        $this->db->set('update_uid', $uid);
        $this->db->set('update_date', 'NOW()', FALSE);
        $affected_rows += $this->db->update($this->table_invoices, array_diff($data, array('id' => $data['id'])));
        
        $affected_rows += $this->update_invoice_products($update_product_arr, $lotitem_arr, $uid);
        $affected_rows += $this->insert_invoice_products($insert_product_arr, $lotitem_arr, $uid);
        $affected_rows += $this->delete_invoice_products($delete_product_arr, $lotitem_arr, $uid);
        return $affected_rows;
    }

    private function update_invoice_products($product_arr, $lotitem_arr, $uid)
    {
        $affected_rows = 0;
        if (!empty($product_arr)) {
            for ($i = 0; $i < count($product_arr); $i++) {
                
                $this->db->where('id', $product_arr[$i]['id']);
                $this->db->set('update_uid', $uid);
                $this->db->set('update_date', 'NOW()', FALSE);

                $product_arr_diff = array_diff($product_arr[$i], array('id' => $product_arr[$i]['id'], 'temp_quantity' => $product_arr[$i]['temp_quantity'], 'product_seq' => $product_arr[$i]['product_seq'], 'order_id' => $product_arr[$i]['order_id']));
                //$product_arr_diff['unit_price'] = Model_utils::currencyToNumber($product_arr[$i]['unit_price']);
                $affected_rows += $this->db->update($this->table_invoice_products, $product_arr_diff);

                $this->clear_invoice_product_lots($product_arr[$i]['id']);
                $invoice_product_lot_arr = $this->get_invoice_product_lot_arr($product_arr[$i]['id'], $lotitem_arr);
                $this->insert_invoice_product_lots($product_arr[$i]['id'], $invoice_product_lot_arr, $uid);
            }
        }

        return $affected_rows;
    }

    private function insert_invoice_products($product_arr, $lotitem_arr, $uid)
    {
        $affected_rows = 0;
        if (!empty($product_arr)) {
            for ($i = 0; $i < count($product_arr); $i++) {
                $temp_id = $product_arr[$i]['id'];
                $data = array_diff($product_arr[$i], array('id' => $product_arr[$i]['id'], 'temp_quantity' => $product_arr[$i]['temp_quantity']));
                //$data['unit_price'] = Model_utils::currencyToNumber($product_arr[$i]['unit_price']);

                $this->db->select('COALESCE(MAX(id),0) + 1 as id');
                $this->db->from($this->table_invoice_products);
                $id = $this->db->get()->result_array()[0]['id'];
                
                $this->db->set('id', $id);
                $this->db->set('create_uid', $uid);
                $this->db->set('create_date', 'NOW()', FALSE);
                $affected_rows += $this->db->insert($this->table_invoice_products, $data);
                
                $invoice_product_lot_arr = $this->get_invoice_product_lot_arr($temp_id, $lotitem_arr);
                $this->insert_invoice_product_lots($id, $invoice_product_lot_arr, $uid);
            }
        }
        return $affected_rows;
    }

    private function delete_invoice_products($product_arr, $lotitem_arr, $uid)
    {
        $affected_rows = 0;
        if (!empty($product_arr)) {
            for ($i = 0; $i < count($product_arr); $i++) {
                $this->db->where('id', $product_arr[$i]['id']);
                $affected_rows += $this->db->delete($this->table_invoice_products);

                $this->clear_invoice_product_lots($product_arr[$i]['id']);
            }
        }
        return $affected_rows;
    }

    private function clear_invoice_product_lots($ivp_id)
    {
        $this->db->where('invoice_products_id', $ivp_id);
        return $this->db->delete($this->table_invoice_product_lots);
    }

    private function insert_invoice_product_lots($ivp_id, $invoice_product_lot_arr, $uid)
    {
        $affected_rows = 0;
        if (!empty($invoice_product_lot_arr)) {
            for ($i = 0; $i < count($invoice_product_lot_arr); $i++) {
                $quantity = intval($invoice_product_lot_arr[$i]['quantity']);
                //if ($quantity > 0) {
                    $data = array(
                        'invoice_products_id' => $ivp_id,
                        'lot_size' => $invoice_product_lot_arr[$i]['lot_size'],
                        'quantity' => $invoice_product_lot_arr[$i]['quantity']
                    );

                    $this->db->set('create_uid', $uid);
                    $this->db->set('create_date', 'NOW()', FALSE);
                    $affected_rows += $this->db->insert($this->table_invoice_product_lots, $data);
                //}
            }
        }
        return $affected_rows;
    }

    private function get_invoice_product_lot_arr($ivp_id, $lotitem_arr)
    {
        $arr = array();
        if (!empty($lotitem_arr)) {
            $index = 0;
            for ($i = 0; $i < count($lotitem_arr); $i++) {
                if ($lotitem_arr[$i]['invoice_products_id'] == $ivp_id) {
                    $arr[$index++] = $lotitem_arr[$i];
                }
            }
        }
        return $arr;
    }

    public function get_invoice_product_lots($invoice_id)
    {
        $this->db->select('ivp.id as ivp_id, ivp.product_seq, ivp.order_id');
        $this->db->select('ivpl.lot_size, ivpl.quantity');
        $this->db->select('CAST((ivpl.quantity / (case when ivpl.lot_size = 0 then 1 else ivpl.lot_size end)) AS INTEGER) as lot_num');
        $this->db->from($this->table_invoice_products . ' as ivp');
        $this->db->join($this->table_invoice_product_lots . ' as ivpl', 'ivpl.invoice_products_id = ivp.id');
        $this->db->where('ivp.invoice_id', $invoice_id);
        return $this->db->get()->result_array();
    }

    public function cancel_invoice($invoice_id, $uid)
    {
        $affected_rows = 0;
        //$del_invoice_product_lots_arr = $this->get_del_invoice_product_lots($invoice_id);
        $del_invoice_products_arr = $this->get_del_invoice_products($invoice_id);

        //delete invoice_product_lots
        $this->insert_invoice_product_lots_history($invoice_id);
        if (!empty($del_invoice_products_arr)) {
            for ($i = 0; $i < count($del_invoice_products_arr); $i++) {
                $this->db->where('invoice_products_id', $del_invoice_products_arr[$i]['id']);
                $affected_rows += $this->db->delete($this->table_invoice_product_lots);
            }
        }
        

        //delete invoice_products
        $this->insert_invoice_products_history($invoice_id);
        $this->db->where('invoice_id', $invoice_id);
        $affected_rows += $this->db->delete($this->table_invoice_products);

        //delete billing_invoices
        $this->insert_billing_invoices_history($invoice_id);
        $this->db->where('invoice_id', $invoice_id);
        $affected_rows += $this->db->delete($this->table_billing_invoices);

        //clear customer_id
        $this->insert_invoices_history($invoice_id);
        $this->db->where('id', $invoice_id);
        $this->db->set('customer_id', NULL);
        $this->db->set('update_uid', $uid);
        $this->db->set('update_date', 'NOW()', FALSE);
        $affected_rows += $this->db->update($this->table_invoices);

        return $affected_rows;
    }

    private function get_del_invoice_products($invoice_id)
    {
        $this->db->select('id');
        $this->db->from($this->table_invoice_products);
        $this->db->where('invoice_id', $invoice_id);
        return $this->db->get()->result_array();
    }

    private function get_del_invoice_product_lots($invoice_id)
    {
        $this->db->select('ivpl.*');
        $this->db->from($this->table_invoice_product_lots . ' as ivpl');
        $this->db->join($this->table_invoice_products . ' as ivp', 'ivp.id = ivpl.invoice_products_id');
        $this->db->where('ivp.invoice_id', $invoice_id);
        return $this->db->get()->result_array();
    }

    private function insert_invoices_history($invoice_id)
    {
        $this->db->query('insert into ' . $this->table_invoices_history . ' as th select t.*, now() from ' . $this->table_invoices . ' as t where t.id = \'' . $this->db->escape_str($invoice_id) . '\'');
    }

    private function insert_invoice_products_history($invoice_id)
    {
        $this->db->query('insert into ' . $this->table_invoice_products_history . ' as th select t.*, now() from ' . $this->table_invoice_products . ' as t where t.invoice_id = \'' . $this->db->escape_str($invoice_id) . '\'');
    }

    private function insert_invoice_product_lots_history($invoice_id)
    {
        $this->db->query('insert into ' . $this->table_invoice_product_lots_history . ' as ivplh'
        . ' select ivpl.*, now() from ' . $this->table_invoice_product_lots . ' as ivpl'
        . ' join ' . $this->table_invoice_products . ' as ivp on ivp.id = ivpl.invoice_products_id'
        . ' where ivp.invoice_id = \'' . $this->db->escape_str($invoice_id) . '\'');
    }

    private function insert_billing_invoices_history($invoice_id)
    {
        $this->db->query('insert into ' . $this->table_billing_invoices_history . ' as th select t.*, now() from ' . $this->table_billing_invoices . ' as t where t.invoice_id = \'' . $this->db->escape_str($invoice_id) . '\'');
    }
}
