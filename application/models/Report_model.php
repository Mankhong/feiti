<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Report_model extends CI_Model {

    public function __construct() {
        $this->load->database();
        $this->table_invoices = 'invoices';
        $this->table_invoice_products = 'invoice_products';
        $this->table_invoice_product_lots = 'invoice_product_lots';
        $this->table_products = ' products';
        $this->table_order_products = 'order_products';
        $this->table_orders = 'orders';
        $this->table_customers = 'customers';
        $this->table_users = 'users';
        $this->table_unit_price_types = 'unit_price_types';
        $this->table_billing_invoices = 'billing_invoices';

        $this->table_product_types = 'product_types';
        $this->table_invoice_types = 'invoice_types';
        $this->table_processes = 'processes';
        $this->table_unit_price_types = 'unit_price_types';
        $this->table_products_history = 'products_history';
        $this->table_customer_types = 'customer_types';
    }

    public function get_data_detail_report($invoice_id, $customer_id) {
        $this->db->select("customers.name as cus_name, customers.address as cus_address, invoices.id as invoice_id, invoices.date as invoice_date");
        $this->db->from("invoices");
        $this->db->join('customers', 'customers.id = invoices.customer_id');
        $this->db->where("invoices.id", $invoice_id);
        return $this->db->get()->result_array();
    }

    public function get_invoice_products_list($invoice_id) {

        /*
          $this->db->select('ivp.id as invoice_products_id, ivp.lot_no, ivp.quantity as invoice_quantity');
          $this->db->select('odp.quantity, odp.order_id');
          $this->db->select('pd.id as product_id, pd.desc1, pd.desc2, pd.lot_size');
          $this->db->select('upt.id as unit_price_type_id, upt.name as unit_price_type_name');
          $this->db->select('(case when cs.customer_type = 1 then pd.domestic_price else pd.export_price end) as unit_price');
          $this->db->select('(case when ivp.quantity is not null then odp.quantity - ivp.quantity else odp.quantity end) as balance');
          $this->db->from($this->table_invoice_products . ' as ivp');
          $this->db->join($this->table_products . ' as pd', 'pd.id = ivp.product_id');
          $this->db->join($this->table_invoices . ' as iv', 'iv.id = ivp.invoice_id');
          $this->db->join($this->table_unit_price_types . ' as upt', 'upt.id = pd.unit_price_type');
          $this->db->join($this->table_customers . ' as cs', 'cs.id = pd.customer_id');
          $this->db->join($this->table_order_products . ' as odp', 'odp.order_id = ivp.order_id and odp.product_id = ivp.product_id');
          $this->db->where('ivp.invoice_id', $invoice_id);
         */
        $invoice_quantity_sql = 'COALESCE('
                . '(select sum(ivpl2.quantity) from invoice_product_lots ivpl2 where ivpl2.invoice_products_id = ivp.id)'
                . ', 0)';

        $this->db->select('ivp.id as invoice_products_id, ivp.lot_no');
        $this->db->select('(' . $invoice_quantity_sql . ') as invoice_quantity');
        $this->db->select('odp.quantity, odp.order_id');
        $this->db->select('pd.id as product_id, pd.desc1, pd.desc2, pd.lot_size');
        $this->db->select('upt.id as unit_price_type_id, upt.name as unit_price_type_name');
        $this->db->select('(case when cs.customer_type = 1 then pd.domestic_price else pd.export_price end) as unit_price');
        //$this->db->select('(case when ivp.quantity is not null then odp.quantity - ivp.quantity else odp.quantity end) as balance');
        $this->db->select('(odp.quantity - ' . $invoice_quantity_sql . ') as balance');
        $this->db->from($this->table_invoice_products . ' as ivp');
        $this->db->join($this->table_products . ' as pd', 'pd.id = ivp.product_id');
        $this->db->join($this->table_invoices . ' as iv', 'iv.id = ivp.invoice_id');
        $this->db->join($this->table_customers . ' as cs', 'cs.id = iv.customer_id');
        $this->db->join($this->table_unit_price_types . ' as upt', 'upt.id = cs.customer_type');
        $this->db->join($this->table_order_products . ' as odp', 'odp.order_id = ivp.order_id and odp.product_id = ivp.product_id');
        $this->db->where('iv.id', $invoice_id);

        return $this->db->get()->result_array();
    }

    public function get_data_receipts($billing_id = '') {
        // $invoice_quantity_sql = 'COALESCE('
        //     . '(select sum(ivpl2.quantity) from invoice_product_lots ivpl2 where ivpl2.invoice_products_id = ivp.id)'
        //     . ', 0)';

        $invoice_quantity_sql = "COALESCE((select sum(ivpl2.quantity)
            from invoice_product_lots ivpl2
            join invoice_products ivp2 on ivp2.id = ivpl2.invoice_products_id
            join order_products odp2 on odp2.order_id = ivp2.order_id and odp2.product_seq = ivp2.product_seq
            where ivp2.product_seq = ivp.product_seq
            and ivp2.order_id = ivp.order_id
            and ivp2.invoice_id = iv.id), 0)";

        $balance_quantity_sql = "COALESCE((select sum(ivpl2.quantity)
            from invoice_product_lots ivpl2
            join invoice_products ivp2 on ivp2.id = ivpl2.invoice_products_id
            join order_products odp2 on odp2.order_id = ivp2.order_id and odp2.product_seq = ivp2.product_seq
            where ivp2.product_seq = ivp.product_seq
            and ivp2.order_id = ivp.order_id
            and ivp2.invoice_id <> iv.id), 0)";

        $lot_size_sql = "COALESCE((select sum(ivpl2.lot_size)
            from invoice_product_lots ivpl2
            join invoice_products ivp2 on ivp2.id = ivpl2.invoice_products_id
            join order_products odp2 on odp2.order_id = ivp2.order_id and odp2.product_seq = ivp2.product_seq
            where ivp2.product_seq = ivp.product_seq
            and ivp2.order_id = ivp.order_id ), 0)";
        //and ivp2.invoice_id <> iv.id), 0)";

        $this->db->select('ivp.id as invoice_products_id, ivp.lot_no, iv.internal_note');
        $this->db->select('(' . $invoice_quantity_sql . ') as invoice_quantity');
        $this->db->select('(' . $lot_size_sql . ') as product_lots_size');
        $this->db->select('odp.quantity, odp.order_id');
        $this->db->select('pd.seq as product_seq, pd.id as product_id, pd.desc1, pd.desc2, pd.lot_size ,pd.carton');
        $this->db->select('upt.id as unit_price_type_id, upt.name as unit_price_type_name');
        $this->db->select('odp.unit_price');
        //$this->db->select('(case when cs.customer_type = 1 then pd.domestic_price else pd.export_price end) as unit_price');
        //$this->db->select('(case when ivp.quantity is not null then odp.quantity - ivp.quantity else odp.quantity end) as balance');
        $this->db->select('(odp.quantity - ' . $balance_quantity_sql . ') as balance');
        $this->db->from($this->table_invoice_products . ' as ivp');
        $this->db->join($this->table_products . ' as pd', 'pd.seq = ivp.product_seq');
        $this->db->join($this->table_invoices . ' as iv', 'iv.id = ivp.invoice_id');
        $this->db->join($this->table_customers . ' as cs', 'cs.id = iv.customer_id');
        $this->db->join($this->table_unit_price_types . ' as upt', 'upt.id = cs.customer_type');
        $this->db->join($this->table_order_products . ' as odp', 'odp.order_id = ivp.order_id and odp.product_seq = ivp.product_seq');
        $this->db->join($this->table_billing_invoices . ' as bi', 'bi.invoice_id = ivp.invoice_id', 'left');
        $this->db->where('bi.billing_id', $billing_id);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_joined_products_data($startDate = '', $dateTimeEnd = '', $customerId = '', $invoiceId = '', $invoice_type = '', $pathno = '', $pathname = '') {


        //$this->db->distinct();
        $this->db->select('cus.name as customer_name, cus.customer_type, csu_type.name');
        $this->db->select('prd.id, prd,desc1, prd.boi_desc, prd.domestic_price, prd.export_price');
        $this->db->select('invp.invoice_id, invp.order_id, inv_type.name as invoice_type_name');
        $this->db->select('or_pro.quantity');
        $this->db->from($this->table_invoice_products . ' as invp');
        $this->db->join($this->table_invoices . ' as inv', 'inv.id = invp.invoice_id');
        $this->db->join($this->table_products . ' as prd', 'prd.seq = invp.product_seq', 'left');
        $this->db->join($this->table_invoice_types . ' as inv_type', 'inv_type.id = prd.invoice_type');
        $this->db->join($this->table_order_products . ' as or_pro', 'or_pro.product_seq = invp.product_seq and or_pro.order_id = invp.order_id');
        $this->db->join($this->table_customers . ' as cus', 'cus.id = inv.customer_id');
        $this->db->join($this->table_customer_types . ' as csu_type', 'csu_type.id = cus.customer_type');
        $this->db->where("inv_type.name is not null");
        if (!empty($startDate)) {
            $this->db->where("to_char(inv.date, 'DD/MM/YYYY') >= ", $startDate);
        }
        if (!empty($dateTimeEnd)) {
            $this->db->where("to_char(inv.date, 'DD/MM/YYYY') <= ", $dateTimeEnd);
        }

        if (!empty($customerId)) {
            $this->db->where('cus.id', $customerId);
        }

        if (!empty($invoiceId)) {
            $this->db->where('inv.id', $invoiceId);
        }

        if (!empty($invoice_type)) {
            $this->db->where('inv_type.id', $invoice_type);
        }

        if (!empty($pathno)) {
            $this->db->where('prd.id', $pathno);
        }

        if (!empty($pathname)) {
            $this->db->where('prd.desc1', $pathname);
        }
        //$this->db->order_by('cus.id', 'asc');
        return $this->db->get()->result_array();
    }

    public function get_joined_products_data2($startDate = '', $dateTimeEnd = '', $customerId = '', $invoiceId = '', $invoice_type = '', $pathno = '', $pathname = '', $rate = 1, $input_rate = '') {
        $rate = !empty($rate) ? $rate : "1";

        /* select 
                r1.customer_id,
                r1.customer_name,
                r1.product_id,
                r1.boi_desc,
                r1.price_flag,
                r1.unit_price_domestic,
                r1.unit_price_export,
                sum(r1.invoice_quantity_domestic) as invoice_quantity_domestic,
                sum(r1.invoice_quantity_export) as invoice_quantity_export,
                sum(r1.amount_domestic) as amount_domestic,
                sum((r1.amount_export)) as amount_export
                ,r1.h_row1 as invoice_type_name
                ,r1.h_row2 as phase
                ,r1.unit_type
                ,case when r1.h_row1 = 'Export EN' then '".$input_rate."' else '' end input_rate
                from
                (
                        select 
                        rs.customer_id,
                        rs.customer_name,
                        rs.product_id,
                        rs.boi_desc,
                        case when rs.type_price = 'D' then 1 else 2 end  price_flag,
                        case when rs.type_price = 'D' then rs.unit_price else 0 end  as unit_price_domestic ,
                        case when rs.type_price = 'E' then rs.unit_price else 0 end  as unit_price_export ,
                        case when rs.type_price = 'D' then rs.invoice_quantity else 0 end  as invoice_quantity_domestic ,
                        case when rs.type_price = 'E' then rs.invoice_quantity else 0 end  as invoice_quantity_export, 
                        case when rs.type_price = 'D' then round((rs.unit_price * rs.invoice_quantity),2) else 0 end  as amount_domestic ,
                        case when rs.type_price = 'E' then case when rs.unit_type ='1' then round((rs.unit_price * rs.invoice_quantity),2) else round((rs.unit_price * rs.invoice_quantity),2)* ".$input_rate."  end else 0 end  as amount_export
                        ,rs.header_table||' ' ||rs.invoice_name  as h_row1
                        ,rs.invoice_name
                        ,case when rs.invoice_phase is null or rs.invoice_phase ='' then '' else 'Phase#'||rs.invoice_phase end  as h_row2
                        ,rs.unit_type
                        from 
                        (
                                        select iv.id as invoice_id, iv.date as invoice_date
                                        ,cs.id as customer_id, cs.name as customer_name
                                        ,odp.invoice_type_name as invoice_type_name, odp.invoice_type_phase as phase
                                        ,pd.domestic_price, pd.export_price, odp.quantity
                                        ,ivp.id as invoice_products_id, ivp.lot_no, iv.internal_note
                                        ,(COALESCE((select sum(ivpl2.quantity)
                                                                                        from invoice_product_lots ivpl2
                                                                                        join invoice_products ivp2 on ivp2.id = ivpl2.invoice_products_id
                                                                                        join order_products odp2 on odp2.order_id = ivp2.order_id and odp2.product_seq = ivp2.product_seq
                                                                                        where ivp2.product_seq = ivp.product_seq
                                                                                        and ivp2.order_id = ivp.order_id
                                                                                        and ivp2.invoice_id = iv.id), 0)) as invoice_quantity
                                        ,(COALESCE((select sum(ivpl2.lot_size)
                                                                                        from invoice_product_lots ivpl2
                                                                                        where ivpl2.invoice_products_id = ivp.id), 0)) as product_lots_size
                                        ,odp.quantity, odp.order_id, odp.unit_price, odp.price_flag
                                        ,pd.seq as product_seq, pd.id as product_id, pd.desc1, pd.desc2, pd.lot_size ,pd.carton, pd.boi_desc                                     
                                         ,case when inty.invoice_group='N' then inty.type_price 
								          else (select distinct inty1.type_price
								          from
								           invoice_products as ivp1
								          join invoices as iv1 on
								           iv1.id = ivp1.invoice_id
								          join invoice_types as inty1 on
								           cast(inty1.id as varchar) = iv1.invoice_type and inty1.invoice_group = 'N'
								          where
								           ivp1.product_seq = ivp.product_seq )
								          end type_price
								          ,inty.unit_price as unit_type,inty.header_table,inty.phase as invoice_phase,
                                          case when inty.invoice_group='N' then inty.name
								          else (select distinct inty1.name
								          from
								           invoice_products as ivp1
								          join invoices as iv1 on
								           iv1.id = ivp1.invoice_id
								          join invoice_types as inty1 on
								           cast(inty1.id as varchar) = iv1.invoice_type and inty1.invoice_group = 'N'
								          where
								           ivp1.product_seq = ivp.product_seq )
								          end invoice_name
                                        from invoice_products  as ivp 
                                        join products  as pd on pd.seq = ivp.product_seq
                                        join invoices  as iv on iv.id = ivp.invoice_id
                                        join invoice_types as inty on cast(inty.id  as varchar) =iv.invoice_type
                                        join customers  as cs on cs.id = iv.customer_id
                                        join unit_price_types  as upt on upt.id = cs.customer_type
                                        join order_products  as odp on odp.order_id = ivp.order_id and odp.product_seq = ivp.product_seq
                                        where 1=1 */
                                                  
        $sql = "select
	r1.customer_id,
	r1.customer_name,
	r1.product_id,
	r1.boi_desc,
	r1.price_flag,
	r1.unit_price_domestic,
	r1.unit_price_export,
	sum(r1.invoice_quantity_domestic) as invoice_quantity_domestic,
	sum(r1.invoice_quantity_export) as invoice_quantity_export,
	sum(r1.amount_domestic) as amount_domestic,
	sum((r1.amount_export)) as amount_export ,
	r1.h_row1 as invoice_type_name ,
	r1.h_row2 as phase ,
	r1.unit_type ,
	case
		when r1.h_row1 = 'Export EN' then '".$input_rate."' 
                when r1.h_row1 = 'Export EP' then '".$input_rate."'
                when r1.h_row1 = 'Export ET' then '".$input_rate."'
                when r1.h_row1 = 'Export VE' then '".$input_rate."'
                when r1.h_row1 = 'Mold MD.EX' then '".$input_rate."'
		else ''
	end input_rate ,
	r1.invoice_type
from
	(
	select
		rs.customer_id,
		rs.customer_name,
		rs.product_id,
		rs.boi_desc,
		case
			when rs.type_price = 'D' then 1
			else 2
		end price_flag,
		case
			when rs.type_price = 'D' then rs.unit_price
			else 0
		end as unit_price_domestic ,
		case
			when rs.type_price = 'E' then rs.unit_price
			else 0
		end as unit_price_export ,
		case
			when rs.type_price = 'D' then rs.invoice_quantity
			else 0
		end as invoice_quantity_domestic ,
		case
			when rs.type_price = 'E' then rs.invoice_quantity
			else 0
		end as invoice_quantity_export,
		case
			when rs.type_price = 'D' then round((rs.unit_price * rs.invoice_quantity), 2)
			else 0
		end as amount_domestic ,
		case
			when rs.type_price = 'E' then
			case
				when rs.unit_type = '1' then round((rs.unit_price * rs.invoice_quantity), 2)
				else round((rs.unit_price * rs.invoice_quantity), 2) * ".$rate."
			end
			else 0
		end as amount_export ,
		rs.header_table || ' ' || rs.invoice_name as h_row1 ,
		rs.invoice_name ,
		case
			when rs.invoice_phase is null
			or rs.invoice_phase = '' then ''
			else 'Phase#' || rs.invoice_phase
		end as h_row2 ,
		rs.unit_type ,
		rs.invoice_type
	from
		(
		select
			iv.id as invoice_id,
			iv.date as invoice_date ,
			cs.id as customer_id,
			cs.name as customer_name ,
			--odp.invoice_type_name as invoice_type_name,
			--odp.invoice_type_phase as phase ,
			pd.domestic_price,
			pd.export_price,
			odp.quantity ,
			ivp.id as invoice_products_id,
			ivp.lot_no,
			iv.internal_note ,
			(coalesce((select sum(ivpl2.quantity) from invoice_product_lots ivpl2 join invoice_products ivp2 on ivp2.id = ivpl2.invoice_products_id join order_products odp2 on odp2.order_id = ivp2.order_id and odp2.product_seq = ivp2.product_seq where ivp2.product_seq = ivp.product_seq and ivp2.order_id = ivp.order_id and ivp2.invoice_id = iv.id), 0)) as invoice_quantity ,
			(coalesce((select sum(ivpl2.lot_size) from invoice_product_lots ivpl2 where ivpl2.invoice_products_id = ivp.id), 0)) as product_lots_size ,
			odp.quantity,
			odp.order_id,
			odp.unit_price,
			odp.price_flag ,
			pd.seq as product_seq,
			pd.id as product_id,
			pd.desc1,
			pd.desc2,
			pd.lot_size ,
			pd.carton,
			pd.boi_desc ,
			case
				when inty.invoice_group = 'N' then inty.type_price
				else (
					select inty1.type_price
				from
					invoice_types as inty1 
				where
					inty1.name=SUBSTRING(iv.refer_invoice_id, 1, 2)
					)
			end type_price ,
			inty.unit_price as unit_type,
			inty.header_table,
			case
				when inty.invoice_group = 'N' then inty.phase
				else (
					select inty1.phase
				from
					invoice_types as inty1 
				where
					inty1.name=SUBSTRING(iv.refer_invoice_id, 1, 2)
					)
			end as invoice_phase,
			case
				when inty.invoice_group = 'N' then inty.name
				else SUBSTRING(iv.refer_invoice_id, 1, 2)
			end invoice_name ,
			iv.invoice_type
		from
			invoice_products as ivp
		join products as pd on
			pd.seq = ivp.product_seq
		join invoices as iv on
			iv.id = ivp.invoice_id
		join invoice_types as inty on
			cast(inty.id as varchar) = iv.invoice_type
		join customers as cs on
			cs.id = iv.customer_id
		join unit_price_types as upt on
			upt.id = cs.customer_type
		join order_products as odp on
			odp.order_id = ivp.order_id
			and odp.product_seq = ivp.product_seq
		where
			1 = 1";

        
        //new
        if (!empty($startDate)) {
            $sql .= " and iv.date >= to_date('" . $startDate . "', 'DD/MM/YYYY') ";
        }
        if (!empty($dateTimeEnd)) {
            $sql .= " and iv.date <= to_date('" . $dateTimeEnd . "', 'DD/MM/YYYY') ";
        }

        if (!empty($customerId)) {
            $sql .= " and cs.id='".$customerId."'";
        }
        
        if (!empty($invoiceId)) {
            $sql .= " and ivp.invoice_id='".$invoiceId."'";
        }
        
        //if (!empty($invoice_type)) {    
        //    $sql .= " and (inty.name ='".$invoice_type."' or inty.invoice_group='S' )";
        //}
        
        if (!empty($pathno)) {
            $sql .= " and pd.id ='".$pathno."'";
        }
        
        if (!empty($pathname)) {
            $sql .= " and pd.boi_desc ='".$pathname."'";
        }
        $sql .= "order by invoice_date asc, iv.id asc, pd.id asc, invoice_quantity desc, odp.order_id asc
            )rs
            WHERE 1 = 1";
        
        if (!empty($invoice_type)) {
            $sql .= " and rs.invoice_name ='".$invoice_type."'";
        }
        
        $sql .="    
        )r1
        group by 
        r1.customer_id,
        r1.customer_name,
        r1.product_id,
        r1.boi_desc,
        r1.price_flag,
        r1.unit_price_domestic,
        r1.unit_price_export
        ,r1.h_row1
        ,r1.h_row2
        ,r1.unit_type,
        r1.invoice_type";
        
        //new
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    
    public function get_tracking_order_data($startDate = '', $dateTimeEnd = '', $customerId = '', $productId = '', $invoice_id = '', $orderId = '') {
         
        $sql = "select x.* 
                from
                (
                        select
                        cs.name as customer_name,
                        cs.id as customer_id,
                        to_char(od.order_date, 'DD/MM/YYYY') as order_date,
                        od.id as order_id,
                        pd.id as product_id,
                        op.unit_price,                  
                        op.quantity as quantity,
                        round((op.unit_price * op.quantity), 2) as amount,
                        (op.quantity-(coalesce((select sum(ivpl2.quantity) from invoice_product_lots ivpl2 join invoice_products ivp2 on ivp2.id = ivpl2.invoice_products_id join order_products odp2 on odp2.order_id = ivp2.order_id and odp2.product_seq = ivp2.product_seq where ivp2.product_seq = op.product_seq and ivp2.order_id = op.order_id), 0))) as balance,
                        iv.id as invoices_id,
                        to_char(iv.date, 'DD/MM/YYYY') as invoice_date,
                        (coalesce((select sum(ivpl2.quantity) 
						from invoice_product_lots ivpl2 
						join invoice_products ivp2 on ivp2.id = ivpl2.invoice_products_id 
						join order_products odp2 on odp2.order_id = ivp2.order_id and odp2.product_seq = ivp2.product_seq where ivp2.product_seq = op.product_seq and ivp2.order_id = op.order_id and iv.id=ivp2.invoice_id), 0)) as invoice_quantity,
                        bl.id as billing_id,
                        to_char(bl.billing_date, 'DD/MM/YYYY') as billing_date  
                    from order_products as op
                    left join invoice_products  as ivp  on op.order_id = ivp.order_id and op.product_seq = ivp.product_seq
                    left join invoice_product_lots as ivl on ivp.id = ivl.invoice_products_id 
                    left join invoices  as iv on iv.id = ivp.invoice_id
                    left join billing_invoices as blv on iv.id = blv.invoice_id
                    left join billings as bl on bl.id = blv.billing_id
                    left join products as pd on pd.seq = op.product_seq
                    left join customers as cs on cs.id = pd.customer_id
                    left join orders as od on od.id = op.order_id
                   where 1=1 ";

        if (!empty($startDate)) {
            $sql .= " and od.order_date >= to_date('" . $startDate . "', 'DD/MM/YYYY') ";
        }
        if (!empty($dateTimeEnd)) {
            $sql .= " and od.order_date <= to_date('" . $dateTimeEnd . "', 'DD/MM/YYYY') ";
        }

        if (!empty($customerId)) {
            $sql .= " and cs.id='".$customerId."'";
        }
        
        if (!empty($productId)) {
            $sql .= " and pd.id='".$productId."'";
        }
        
        if (!empty($invoice_id)) {
            $sql .= " and iv.id ='".$invoice_id."'";
        }
        
        if (!empty($orderId)) {
            $sql .= " and op.order_id ='".$orderId."'";
        }
        
        $sql .= " ) x
                    group by 
                    x.customer_name,
                    x.customer_id,
                    x.order_date,
                    x.order_id,
                    x.product_id,
                    x.unit_price,                  
                    x.quantity,
                    x.amount,
                    x.balance,
                    x.invoices_id,
                    x.invoice_date,
                    x.invoice_quantity,
                    x.billing_id,
                    x.billing_date
                    order by
                    x.order_date,
                    x.order_id,
                    x.customer_name";
        
        //new
        $query = $this->db->query($sql);
        $result['data'] = $query->result_array();
        return $result;
    }
    
    public function get_joined_products_data3($startDate = '', $dateTimeEnd = '', $customerId = '', $invoiceId = '', $invoice_type = '', $pathno = '', $pathname = '', $rate = '') {



        $sql = "SELECT
                                RS.customer_id,
                                RS.customer_name,
                                RS.customer_type,
                                SUM(RS.amountDomestic) as amount_domestic,
                                SUM(RS.amountExport) as amount_export,
                                SUM(RS.invoice_quantity) as invoice_quantity
                                FROM
                                (select 
                                        R.customer_id,
                                        R.customer_name,
                                        R.customer_type,
                                        CASE WHEN R.type_price ='D' THEN ROUND((R.invoice_quantity * R.unit_price),2) ELSE 0 END AS amountDomestic,
                                        CASE WHEN R.type_price = 'E' THEN ROUND((R.invoice_quantity * R.unit_price),2) ELSE 0 END AS amountExport,
                                        R.invoice_quantity
                                        from
                                        (
                                        select 
                                        cs.id as customer_id, 
                                        cs.name as customer_name,
                                        cs.customer_type,
                                        iv.id as invoice_id, 
                                        iv.date as invoice_date,
                                        pd.seq as product_seq, 
                                        pd.id as product_id, 
                                        pd.desc1, 
                                        pd.desc2, 
                                        pd.lot_size ,
                                        pd.carton,
                                        pd.invoice_type as invoice_type,
                                        in_type.name as invoice_type_name,
                                        pd.domestic_price,
                                        pd.export_price,
                                        odp. quantity,
                                        ivp.id as invoice_products_id, 
                                        ivp.lot_no, iv.internal_note,
                                        (COALESCE((select sum(ivpl2.quantity)
                                                                                        from invoice_product_lots ivpl2
                                                                                        join invoice_products ivp2 on ivp2.id = ivpl2.invoice_products_id
                                                                                        join order_products odp2 on odp2.order_id = ivp2.order_id and odp2.product_seq = ivp2.product_seq
                                                                                        where ivp2.product_seq = ivp.product_seq
                                                                                        and ivp2.order_id = ivp.order_id
                                                                                        and ivp2.invoice_id = iv.id), 0)) as invoice_quantity,
                                        (COALESCE((select sum(ivpl2.lot_size)
                                                                                        from invoice_product_lots ivpl2
                                                                                        where ivpl2.invoice_products_id = ivp.id), 0)) as product_lots_size,
                                        in_type.type_price,
                                        odp.quantity, 
                                        odp.order_id, 
                                        odp.unit_price,
                                        upt.id as unit_price_type_id, upt.name as unit_price_type_name
                                        from invoice_products as ivp
                                        join products  as pd on pd.seq = ivp.product_seq
                                        join invoices  as iv on iv.id = ivp.invoice_id
                                        left join invoice_types as in_type on in_type.name = substring(ivp.invoice_id,1,2)
                                        join customers  as cs on cs.id = iv.customer_id
                                        join unit_price_types as upt on upt.id = cs.customer_type
                                        join order_products  as odp on  odp.order_id = ivp.order_id and odp.product_seq = ivp.product_seq
                                        where 1 = 1";
        

        if (!empty($startDate)) {
            $sql .= " and iv.date >= to_date('" . $startDate . "', 'DD/MM/YYYY') ";
        }
        if (!empty($dateTimeEnd)) {
            $sql .= " and iv.date <= to_date('" . $dateTimeEnd . "', 'DD/MM/YYYY') ";
        }

        if (!empty($customerId)) {
            $sql .= " and cs.id='".$customerId."'";
        }
        
        if (!empty($invoiceId)) {
            $sql .= " and ivp.invoice_id='".$invoiceId."'";
        }
        
        if (!empty($invoice_type)) {
            $sql .= " and odp.invoice_type_name ='".$invoice_type."'";
        }
        
        if (!empty($pathno)) {
            $sql .= " and pd.id ='".$pathno."'";
        }
        
        if (!empty($pathname)) {
            $sql .= " and pd.boi_desc ='".$pathname."'";
        }
        $sql .= "order by invoice_date asc, iv.id asc, pd.id asc, invoice_quantity desc, odp.order_id asc
                                        )R
                                )RS
                                GROUP BY 
                                RS.customer_id,
                                RS.customer_name, RS.customer_type";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_report_2_data($invoice_id) {
        $this->db->select('pd.id as product_id, pd.desc1, pd.desc2, pd.boi_desc, pd.carton');
        $this->db->select('ivp.order_id, ivp.lot_no, ivpl.lot_size as product_lots_size, ivpl.quantity as invoice_quantity, (ivpl.quantity / ivpl.lot_size) as lot_num');
        $this->db->from($this->table_invoice_products . ' as ivp');
        $this->db->join($this->table_invoice_product_lots . ' as ivpl', 'ivpl.invoice_products_id = ivp.id');
        $this->db->join($this->table_products . ' as pd', 'pd.seq = ivp.product_seq');
        $this->db->where('ivp.invoice_id', $invoice_id);
        $this->db->where('ivpl.lot_size > 0');

        $this->db->order_by('pd.id asc, invoice_quantity desc, ivp.order_id asc');

        return $this->db->get()->result_array();
    }

    public function get_invoice_types() {
        $this->db->select("invoice_types.id, invoice_types.name");
        $this->db->from("invoice_types");
        $this->db->where("invoice_types.used", 'Y');
        $this->db->where("invoice_group", 'N');
        $this->db->order_by('invoice_types.id', 'asc');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

}
