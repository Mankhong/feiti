<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once('Model_utils.php');

class Order_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->table_orders = 'orders';
        $this->table_order_products = 'order_products';
        $this->table_order_types = 'order_types';
        $this->table_products = 'products';
        $this->table_product_types = 'product_types';
        $this->table_invoice_types = 'invoice_types';
        $this->table_unit_price_types = 'unit_price_types';
        $this->table_processes = 'processes';
        $this->table_customers = 'customers';
        $this->table_invoices = 'invoices';
        $this->table_invoice_products = 'invoice_products';
        $this->table_invoice_product_lots = 'invoice_product_lots';
    }

    public function get_orders($id = '', $invoice_type = '')
    {
        $this->db->select('o.id, o.type, o.internal_note, o.customer_id, o.status, o.credit, o.invoice_type');
        $this->db->select('ivt.invoice_group');
        $this->db->select('to_char(o.order_date, \'DD/MM/YYYY\') as order_date, to_char(o.delivery_date, \'DD/MM/YYYY\') as delivery_date');
        $this->db->from($this->table_orders . ' as o');
        $this->db->join($this->table_invoice_types . ' as ivt', 'ivt.id = cast(o.invoice_type as integer)', 'left');

        if (!empty($id)) {
            $this->db->where('o.id', $id);
        }

        if (!empty($invoice_type)) {
            $this->db->where('o.invoice_type', $invoice_type);
        } 

        return $this->db->get()->result_array();
    }

    public function get_order_products_by_order_id($order_id = NULL)
    {
        $invoice_quantity_sql = "COALESCE((select sum(ivpl2.quantity)
            from invoice_product_lots ivpl2
            join invoice_products ivp2 on ivp2.id = ivpl2.invoice_products_id
            join order_products odp2 on odp2.order_id = ivp2.order_id and odp2.product_seq = ivp2.product_seq
            where ivp2.product_seq = op.product_seq
            and ivp2.order_id = op.order_id), 0)";

        $this->db->select('pd.id as product_id, pd.desc1, pd.desc2, pd.boi_desc');
        $this->db->select('ivt.id as invoice_type_id, ivt.name as invoice_type_name, ivt.phase as invoice_type_phase');
        $this->db->select('op.unit_price, op.order_id, op.product_seq, op.quantity, op.price_flag');
        $this->db->select('(' . $invoice_quantity_sql . ') as invoice_quantity');
        $this->db->select('od.id as order_id, od.order_date, od.delivery_date');
        $this->db->select('cs.id as customer_id, cs.name as customer_name');
        $this->db->select('round((op.unit_price * op.quantity), 2) as amount');
        $this->db->select('(op.quantity-(' . $invoice_quantity_sql . ')) as balance');
        $this->db->from($this->table_order_products . ' as op');
        $this->db->join($this->table_products . ' as pd', 'pd.seq = op.product_seq');
        $this->db->join($this->table_product_types . ' as pdt', 'pdt.id = pd.product_type', 'left');
        $this->db->join($this->table_invoice_types . ' as ivt', 'ivt.id = pd.invoice_type', 'left');
        $this->db->join($this->table_processes . ' as pss', 'pss.id = pd.process', 'left');
        $this->db->join($this->table_customers . ' as cs', 'cs.id = pd.customer_id');
        $this->db->join($this->table_orders . ' as od', 'od.id = op.order_id');

        if ($order_id != NULL) {
            if (!isset($order_id)) {
                $this->db->where('op.order_id', $order_id);
            } else {
                $this->db->where_in('op.order_id', $order_id);
            }
        }

        $this->db->order_by('od.delivery_date, op.order_id, pd.id');
        return $this->db->get()->result_array();
    }

    public function copy_order_products_by_order_id($order_id)
    {
        $this->db->select('op.order_id, op.product_seq, op.quantity, pd.*, pd.id as product_id, op.unit_price, op.price_flag');
        $this->db->select('0 as invoice_quantity');
        $this->db->from($this->table_order_products . ' as op');
        $this->db->join($this->table_products . ' as pd', 'pd.seq = op.product_seq');
        $this->db->join($this->table_product_types . ' as pdt', 'pdt.id = pd.product_type', 'left');
        $this->db->join($this->table_invoice_types . ' as ivt', 'ivt.id = pd.invoice_type', 'left');
        $this->db->join($this->table_processes . ' as pss', 'pss.id = pd.process', 'left');
        $this->db->join($this->table_customers . ' as cs', 'cs.id = pd.customer_id');
        $this->db->where('op.order_id', $order_id);
        
        return $this->db->get()->result_array();
    }

    public function get_joined_orders($id = '', $order_id = '', $customer_name = '', $startDate= '', $dateTimeEnd='')
    {
        $this->db->select('od.*');
        $this->db->select('to_char(od.order_date, \'DD/MM/YYYY\') as order_date');
        $this->db->select('to_char(od.delivery_date, \'DD/MM/YYYY\') as delivery_date');
        $this->db->select('odt.id as order_type_id, odt.name as order_type_name');
        $this->db->select('cm.name as customer_name, cm.image_path as customer_image_path');
        $this->db->select(Model_utils::sql_last_update_date('od'));
        $this->db->select(Model_utils::sql_last_update_user('od'));
        $this->db->from($this->table_orders . ' as od');
        $this->db->join($this->table_order_types . ' as odt', 'odt.id = od.type', 'left');
        $this->db->join($this->table_customers . ' as cm', 'cm.id = od.customer_id');
        $this->db->order_by('od.id');

        if (!empty($id)) {
            $this->db->where('od.id', $order_id);
        }
        
        if (!empty($order_id)) {
            $this->db->like('od.id', $order_id);
        }
        
        if (!empty($customer_name)) {
            $this->db->like('cm.name', $customer_name);
        }
        
        if (!empty($startDate)) {
            $this->db->where("od.order_date >= to_date('" . $startDate . "', 'DD/MM/YYYY')");
            
        }
        if (!empty($dateTimeEnd)) {
            $this->db->where("od.order_date <= to_date('" . $dateTimeEnd . "', 'DD/MM/YYYY')");
        }
        
        return $this->db->get()->result_array();
    }

    public function get_picker_products($customer_id)
    {
        $this->db->select('pd.seq, pd.id, pd.desc1, pd.domestic_price as unit_price, ivt.id as invoice_type_id, ivt.name as invoice_type_name, ivt.phase as invoice_type_phase');
        $this->db->select('1 as price_flag');
        $this->db->select('0 as invoice_quantity');
        $this->db->from($this->table_products . ' as pd');
        $this->db->join($this->table_customers . ' as cs', 'cs.id = pd.customer_id');
        $this->db->join($this->table_invoice_types . ' as ivt', 'ivt.id = pd.invoice_type');
        $this->db->where('pd.customer_id', $customer_id);

        $query1 = $this->db->get_compiled_select();

        $this->db->select('pd2.seq, pd2.id, pd2.desc1, pd2.export_price as unit_price, ivt2.id as invoice_type_id, ivt2.name as invoice_type_name, ivt2.phase as invoice_type_phase');
        $this->db->select('2 as price_flag');
        $this->db->select('0 as invoice_quantity');
        $this->db->from($this->table_products . ' as pd2');
        $this->db->join($this->table_customers . ' as cs2', 'cs2.id = pd2.customer_id');
        $this->db->join($this->table_invoice_types . ' as ivt2', 'ivt2.id = pd2.invoice_type');
        $this->db->where('pd2.customer_id', $customer_id);

        $query2 = $this->db->get_compiled_select();

        return $this->db->query($query1 . ' UNION ' . $query2)->result_array();
    }

    public function get_delivery_products($customer_id, $order_id) {
        $quantity_sql = "COALESCE((select sum(ivpl2.quantity)
        from invoice_product_lots ivpl2
        join invoice_products ivp2 on ivp2.id = ivpl2.invoice_products_id
        join order_products odp2 on odp2.order_id = ivp2.order_id and odp2.product_seq = ivp2.product_seq
        where ivp2.product_seq = ivp.product_seq
        and ivp2.order_id = ivp.order_id
        and ivp2.invoice_id = ivp.invoice_id), 0)";

        $this->db->select($quantity_sql . ' as quantity');
        $this->db->select('to_char(od.order_date, \'DD/MM/YYYY\') as order_date, od.id as order_id,');
        $this->db->select('pd.id as product_id, pd.unit_price');
        $this->db->from($this->table_invoice_products . ' as ivp');
        $this->db->join($this->table_orders . ' as od', 'od.id = ivp.order_id');
        $this->db->join($this->table_products . ' as pd', 'pd.seq = ivp.product_seq');
        $this->db->where('pd.customer_id', $customer_id);
        $this->db->where('ivp.order_id', $order_id);
        $this->db->order_by('pd.id');
        return $this->db->get()->result_array();
    }
    
    public function get_order_types($id = '')
    {
        $this->db->select('*');
        $this->db->from($this->table_order_types);

        if (!empty($id)) {
            $this->db->where('id', $id);
        }

        return $this->db->get()->result_array();
    }

    public function has_order($id, $invoice_type)
    {
        $this->db->select('*');
        $this->db->from($this->table_orders);
        $this->db->where('id', $id);

        if (!empty($invoice_type)) {
            $this->db->where('invoice_type', $invoice_type);
        } else {
            $this->db->where('invoice_type is null');
        }

        return count($this->db->get()->result_array()) > 0;
    }

    public function insert_order($data, $product_arr, $uid = NULL)
    {
        $data['status'] = 1;

        //skip numeric null.
        $diff = array();
        if (empty($data['credit'])) {
            $diff['credit'] = '';
        }
        $data = array_diff($data, $diff);

        if ($data['invoice_type'] != NULL) {
            $data['id'] = $this->get_invoice_type_name($data['invoice_type']) . $data['id'];
        }

        $this->db->set('create_uid', $uid);
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->insert($this->table_orders, $data);
        $affected_rows = $this->db->affected_rows();

        if ($affected_rows > 0) {
            $this->insert_products($data['id'], $product_arr, $uid);
        }

        return $affected_rows;
    }

    private function get_invoice_type_name($id)
    {
        $this->db->select('name');
        $this->db->from($this->table_invoice_types);
        $this->db->where('used', 'Y');
        $this->db->where('id', $id);
        $result = $this->db->get()->result_array();
        return !empty($result) ? $result[0]['name'] : '';
    }

    public function insert_products($order_id, $product_arr, $uid)
    {
        $affected_row = 0;

        if (!empty($product_arr)) {
            for ($i = 0; $i < count($product_arr); $i++) {
                $quantity = Model_utils::currencyToNumber($product_arr[$i]['quantity']);
                $unit_price = Model_utils::currencyToNumber($product_arr[$i]['unit_price']);

                $price_flag = $product_arr[$i]['price_flag'];
                if (empty($price_flag)) {
                    $price_flag = 1;
                }

                $data = array(
                    'order_id' => $order_id,
                    'product_seq' => $product_arr[$i]['product_seq'],
                    'quantity' => $quantity,
                    'unit_price' => $unit_price,
                    'price_flag' => $price_flag,
                    'invoice_type_name' => $product_arr[$i]['invoice_type_name'],
                    'invoice_type_phase' => $product_arr[$i]['invoice_type_phase'],
                );
                $this->db->set('create_uid', $uid);
                $this->db->set('create_date', 'NOW()', FALSE);
                $affected_row += $this->db->insert($this->table_order_products, $data);
                
                $this->update_product_price($product_arr[$i]['product_seq'], $product_arr[$i]['price_flag'], $unit_price, $uid);
            }
        }

        return $affected_row;
    }

    public function delete_products($order_id, $product_arr)
    {
        $affected_row = 0;

        if (!empty($product_arr)) {
            for ($i = 0; $i < count($product_arr); $i++) {
                $quantity = Model_utils::currencyToNumber($product_arr[$i]['quantity']);
                $affected_row += $this->db->delete($this->table_order_products, array('order_id' => $order_id, 'product_seq' => $product_arr[$i]['product_seq']));
                $affected_row += $this->delete_invoice_products_and_lots($order_id, $product_arr[$i]['product_seq']);
            }
        }

        return $affected_row;
    }

    private function delete_invoice_products_and_lots($order_id, $product_seq)
    {
        $affected_row = 0;

        $invoice_products_id = $this->get_invoice_products_id($order_id, $product_seq);
        if ($invoice_products_id != NULL) {
            $affected_row += $this->delete_invoice_product_lots($invoice_products_id);

            $this->db->where('id', $invoice_products_id);
            $affected_row += $this->db->delete($this->table_invoice_product);
        }

        return $affected_row;
    }

    private function delete_invoice_product_lots($invoice_products_id)
    {
        $affected_row = 0;

        $this->db->where('invoice_products_id', $invoice_products_id);
        $affected_row = $this->db->delete($this->table_invoice_product_lots);

        return $affected_row;
    }

    private function get_invoice_products_id($order_id, $product_seq)
    {
        $this->db->select('ivp.id as invoice_products_id');
        $this->db->from($this->table_invoice_products . ' as ivp');
        $this->db->where('ivp.order_id', $order_id);
        $this->db->where('ivp.product_seq', $product_seq);
        $result_array = $this->db->get()->result_array();
        if (!empty($result_array)) {
            return $result_array[0]['invoice_products_id'];
        }
        return NULL;
    }

    public function update_products($order_id, $product_arr, $uid)
    {
        $affected_row = 0;

        if (!empty($product_arr)) {
            for ($i = 0; $i < count($product_arr); $i++) {
                $quantity = Model_utils::currencyToNumber($product_arr[$i]['quantity']);
                $unit_price = Model_utils::currencyToNumber($product_arr[$i]['unit_price']);

                $price_flag = $product_arr[$i]['price_flag'];
                if (empty($price_flag)) {
                    $price_flag = 1;
                }

                $data = array(
                    'quantity' => $quantity,
                    'unit_price' => $unit_price,
                    'price_flag' => $price_flag,
                    'invoice_type_name' => $product_arr[$i]['invoice_type_name'],
                    'invoice_type_phase' => $product_arr[$i]['invoice_type_phase'],
                );

                $this->db->where('order_id', $order_id);
                $this->db->where('product_seq', $product_arr[$i]['product_seq']);
                $this->db->set('update_uid', $uid);
                $this->db->set('update_date', 'NOW()', FALSE);
                $affected_row += $this->db->update($this->table_order_products, $data);

                $this->update_product_price($product_arr[$i]['product_seq'], $product_arr[$i]['price_flag'], $unit_price, $uid);
            }
        }

        return $affected_row;
    }

    public function update_order($id, $data, $insert_product_arr, $delete_product_arr, $edit_product_arr, $uid)
    {
        //numeric null.
        if (empty($data['credit'])) {
            $data['credit'] = NULL;
        }

        //skip date empty.
        if (empty($data['order_date'])) {
            $data['order_date'] = NULL;
        }

        if (empty($data['delivery_date'])) {
            $data['delivery_date'] = NULL;
        }

        $this->db->where('id', $id);
        $this->db->set('update_uid', $uid);
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->update($this->table_orders, $data);
        $affected_rows = $this->db->affected_rows();

        $affected_rows += $this->delete_products($id, $delete_product_arr);
        $affected_rows += $this->insert_products($id, $insert_product_arr, $uid);
        $affected_rows += $this->update_products($id, $edit_product_arr, $uid);

        return $affected_rows;
    }

    public function cancel_order($id)
    {
        $data['status'] = 2;
        $this->db->where('id', $id);
        $this->db->update($this->table_orders, $data);
        return $this->db->affected_rows();
    }

    public function get_invoice_quantity($order_id, $product_id)
    {
        $this->db->select('COALESCE(sum(ivpl.quantity), 0) as invoice_quantity');
        $this->db->from($this->table_invoice_products . ' as ivp');
        $this->db->join($this->table_invoice_product_lots . ' as ivpl', 'ivpl.invoice_products_id = ivp.id');
        $this->db->where('ivp.order_id', $order_id);
        $this->db->where('ivp.product_id', $product_id);
        return $this->db->get()->result_array()[0]['invoice_quantity'];
    }

    private function update_product_price($product_seq, $price_flag, $unit_price, $uid)
    {
        $affected_rows = 0;
        $this->db->where('seq', $product_seq);

        if ($price_flag == '1') {
            $this->db->set('domestic_price', $unit_price);
        } else {
            $this->db->set('export_price', $unit_price);
        }

        $this->db->set('update_uid', $uid);
        $this->db->set('update_date', 'NOW()', FALSE);
        return $this->db->update($this->table_products);
    }

    public function delete_order($order_id, $uid)
    {
        $affected_row = 0;

        //delete order_products
        $this->db->where('order_id', $order_id);
        $affected_row += $this->db->delete($this->table_order_products);

        //delete order
        $this->db->where('id', $order_id);
        $affected_row += $this->db->delete($this->table_orders);

        return $affected_row;
    }

    public function has_invoice_order($order_id)
    {
        $this->db->select('count(1) as count');
        $this->db->from($this->table_invoice_products);
        $this->db->where('order_id', $order_id);

        return intval($this->db->get()->result_array()[0]['count']) > 0;
    }

    public function get_invoice_groups()
    {
        $result = array();
        $result[0]['value'] = 'N';
        $result[0]['name'] = 'Normal';

        $result[1]['value'] = 'S';
        $result[1]['name'] = 'Special';

        return $result;
    }
}
