<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once('Model_utils.php');

class Customer_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->table_customers = 'customers';
        $this->table_business_types = 'business_types';
        $this->table_customer_types = 'customer_types';
        $this->table_continents = 'continents';
        $this->table_countries = 'countries';
    }

    public function get_customers($id = '')
    {
        $this->db->select('*');
        $this->db->from($this->table_customers);

        if (!empty($id)) {
            $this->db->where('id', $id);
        }

        return $this->db->get()->result_array();
    }

    public function get_joined_customers($id = '')
    {
        $this->db->select('cs.*');
        $this->db->select('bst.id as business_type_id, bst.name as business_type_name');
        $this->db->select('cst.id as customer_type_id, cst.name as customer_type_name');
        $this->db->select('cont.continent_name');
        $this->db->select('cntr.country_name');
        $this->db->select(Model_utils::sql_last_update_date('cs'));
        $this->db->select(Model_utils::sql_last_update_user('cs'));
        $this->db->from($this->table_customers . ' as cs');
        $this->db->join($this->table_business_types . ' as bst', 'bst.id = ' . 'cs.business_type', 'left');
        $this->db->join($this->table_customer_types . ' as cst', 'cst.id = ' . 'cs.customer_type', 'left');
        $this->db->join($this->table_continents . ' as cont', 'cont.continent_code = ' . 'cs.continent', 'left');
        $this->db->join($this->table_countries . ' as cntr', 'cntr.country_code = ' . 'cs.country', 'left');

        if (!empty($id)) {
            if (isset($id)) {
                $this->db->where_in('cs.id', $id);
            } else {
                $this->db->where('cs.id', $id);
            }
        }

        $this->db->order_by('id');
        return $this->db->get()->result_array();
    }

    public function insert_customer($data, $uid)
    {
        //skip numeric null.
        $diff = array();
        if (empty($data['credit'])) {
            $diff['credit'] = '';
        }

        if (empty($data['discount'])) {
            $diff['discount'] = '';
        }
        $data = array_diff($data, $diff);
        //end skip

        $data['id'] = $this->generate_customer_id($data['name']);
        $this->db->set('create_uid', $uid);
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->insert($this->table_customers, $data);
        return $this->db->affected_rows();
    }

    public function update_customer($id, $data, $uid)
    {
        //numeric null.
        if (empty($data['credit'])) {
            $data['credit'] = NULL;
        }

        if (empty($data['discount'])) {
            $data['discount'] = NULL;
        }

        $this->db->set('update_uid', $uid);
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->where('id', $id);
        $this->db->update($this->table_customers, $data);
        return $this->db->affected_rows();
    }

    public function get_business_types($id = '')
    {
        $this->db->select('*');
        $this->db->from($this->table_business_types);

        if (!empty($id)) {
            $this->db->where('id', $id);
        }
        return $this->db->get()->result_array();
    }

    public function get_customer_types($id = '')
    {
        $this->db->select('*');
        $this->db->from($this->table_customer_types);

        if (!empty($id)) {
            $this->db->where('id', $id);
        }
        return $this->db->get()->result_array();
    }

    public function generate_customer_id($customer_name)
    {
        $first_char = substr($customer_name, 0, 1);
        $num = 1;
        $length = 3;

        $this->db->select('id');
        $this->db->from($this->table_customers);
        $this->db->like('id', $first_char, 'after');
        $this->db->order_by('substring(id from 2 for 3)::int4', 'DESC');

        $result_arr = $this->db->get()->result_array();
        if (!empty($result_arr)) {
            $id = $result_arr[0]['id'];
            $num = intval(substr($id, 1, strlen($id)-1)) + 1; 
        }
        
        return $first_char . $this->numstr_fix_length($num, $length);
    }

    private function numstr_fix_length($number, $length)
    {
        $result = '';
        for ($i = strlen($number); $i < $length; $i++) {
            $result .= '0';
        }
        return $result . $number;
    }


}
