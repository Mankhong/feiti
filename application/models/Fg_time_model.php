<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once('Model_utils.php');

class Fg_time_model extends CI_Model
{
    public function __construct()
    {
       // $this->mdb = $this->load->database('mdb', TRUE);
        $this->load->database();
        $this->load->model(array('fg_shift_switch_model'));

        $this->table_checkinout = 'CHECKINOUT';
        $this->table_fg_checkinout = 'fg_checkinout';
        $this->table_fg_employee = 'fg_employee';
        $this->table_fg_timesheet = 'fg_timesheet';
        $this->table_fg_shift = 'fg_shift';
        $this->table_fg_monthly_data = 'fg_monthly_data';
        $this->table_fg_holiday = 'fg_holiday';
        $this->table_fg_leave = 'fg_leave';
        $this->table_fg_leave_type = 'fg_leave_type';
        $this->table_fg_duration = 'fg_duration';
    }

    public function get_mdb_checkinout($id = '')
    {   
        $query = $this->mdb->query('select * from CHECKINOUT ch left join USERINFO ui on ui.USERID = ch.USERID');
        return $query->result_array();
    }

    public function get_fg_checkinout()
    {
        $this->db->select('cio.*');
        $this->db->select('em.id as employee_id, concat(em.firstname_th, \' \', em.lastname_th) as fullname_th, concat(em.firstname_en, \' \', em.lastname_en) as fullname_en');
        $this->db->from($this->table_fg_checkinout . ' as cio');
        $this->db->join($this->table_fg_employee . ' as em', 'em.sensor_id = cio.userid', 'left');
        $this->db->order_by('cio.checktime desc');
        return $this->db->get()->result_array();
    }

    public function migrate_checkinout()
    {
        $mdb_checkinout_arr = $this->get_mdb_checkinout();
        
        if (!empty($mdb_checkinout_arr)) {
            for ($i = 0; $i < count($mdb_checkinout_arr); $i++) {
                $data = array(
                    'userid' => $mdb_checkinout_arr[$i]['USERID'],
                    'badgenumber' => $mdb_checkinout_arr[$i]['Badgenumber'],
                    'checktime' => $mdb_checkinout_arr[$i]['CHECKTIME'],
                    'checktype' => $mdb_checkinout_arr[$i]['CHECKTYPE'],
                    'verifycode' => $mdb_checkinout_arr[$i]['VERIFYCODE'],
                    'sensorid' => $mdb_checkinout_arr[$i]['SENSORID'],
                    'workcode' => $mdb_checkinout_arr[$i]['WorkCode'],
                    'sn' => $mdb_checkinout_arr[$i]['sn'],
                    'userextfmt' => $mdb_checkinout_arr[$i]['UserExtFmt'],
                );
                
                if(!$this->has_checkinout($mdb_checkinout_arr[$i]['USERID'], $mdb_checkinout_arr[$i]['CHECKTIME'])) {
                    $this->db->set('create_date', 'NOW()', FALSE);
                    $this->db->insert($this->table_fg_checkinout, $data);
                }
            }
        }
    }

    public function migrate_checkinout_by_records($records)
    {
        $affected_rows = 0;

        if (!empty($records)) {
            $this->db->truncate($this->table_fg_checkinout);
            $length = count($records);

            for ($i = 0; $i < $length; $i++) {
                $data = array(
                    'userid' => $records[$i]['userid'],
                    'badgenumber' => $records[$i]['badgenumber'],
                    'checktime' => $records[$i]['checktime'],
                    'sensorid' => $records[$i]['sensorid'],
                    'checktype' => 'X',
                );
                
               // if(!$this->has_checkinout($records[$i]['badgenumber'], $records[$i]['checktime'])) {
                    $this->db->set('create_date', 'NOW()', FALSE);
                    $affected_rows += $this->db->insert($this->table_fg_checkinout, $data);
               // }
            }
        }

        return $affected_rows;
    }

    private function has_checkinout($badgenumber, $checktime) {
        $this->db->where('badgenumber', $badgenumber);
        $this->db->where('checktime', $checktime);
        $query = $this->db->get($this->table_fg_checkinout);
        return count($query->result_array()) > 0;
    }

    public function migrate_checkinout2()
    {
        $mdb_checkinout_arr = $this->get_mdb_checkinout();
        $this->db->truncate($this->table_fg_checkinout);
        
        if (!empty($mdb_checkinout_arr)) {
            for ($i = 0; $i < count($mdb_checkinout_arr); $i++) {
                $data = array(
                    'userid' => $mdb_checkinout_arr[$i]['USERID'],
                    'badgenumber' => $mdb_checkinout_arr[$i]['Badgenumber'],
                    'checktime' => $mdb_checkinout_arr[$i]['CHECKTIME'],
                    'checktype' => $mdb_checkinout_arr[$i]['CHECKTYPE'],
                    'verifycode' => $mdb_checkinout_arr[$i]['VERIFYCODE'],
                    'sensorid' => $mdb_checkinout_arr[$i]['SENSORID'],
                    'workcode' => $mdb_checkinout_arr[$i]['WorkCode'],
                    'sn' => $mdb_checkinout_arr[$i]['sn'],
                    'userextfmt' => $mdb_checkinout_arr[$i]['UserExtFmt'],
                );
                $this->db->set('create_date', 'NOW()', FALSE);
                $this->db->insert($this->table_fg_checkinout, $data);
            }
        }
    }

    public function get_employee_timesheet($ems_no, $year, $month)
    {
        //$this->migrate_checkinout();

        $holiday_sql = 'select hd.type from ' . $this->table_fg_holiday . ' as hd where TO_CHAR(hd.date, \'DD/MM/YYYY\') = TO_CHAR(tm.date, \'DD/MM/YYYY\')';
        //$leave_type_sql = 'select lvt2.code from ' . $this->table_fg_leave . ' as lv2 join ' . $this->table_fg_leave_type . ' as lvt2 on lvt2.code = lv2.type where lv2.employee_id = em.id and lv2.from_date <= tm.date and lv2.to_date >= tm.date order by lv2.create_date asc LIMIT 1';
        $leave_type_sql = 'select concat(lvt2.code, \' (\', dr2.name, \')\') from ' . $this->table_fg_leave 
            . ' as lv2 join ' . $this->table_fg_leave_type . ' as lvt2 on lvt2.code = lv2.type join ' . $this->table_fg_duration . ' as dr2 on dr2.id = lv2.duration'
            . ' where lv2.employee_id = em.id and lv2.from_date <= tm.date and lv2.to_date >= tm.date order by lv2.create_date asc LIMIT 1';

        $this->db->select('tm.*');
        $this->db->select('to_char(tm.date, \'DD/MM/YYYY\') as date');
        $this->db->select('to_char(tm.start_work, \'HH24:MI\') as start_work');
        $this->db->select('to_char(tm.end_work, \'HH24:MI\') as end_work');
        $this->db->select('em.id as employee_id, concat(em.firstname_th, \' \', em.lastname_th) as fullname_th, concat(em.firstname_en, \' \', em.lastname_en) as fullname_en');
        $this->db->select('(' . $holiday_sql . ') as holiday_type');
        $this->db->select('(' . $leave_type_sql . ') as leave_type');
        $this->db->from($this->table_fg_timesheet . ' as tm');
        $this->db->join($this->table_fg_employee . ' as em', 'em.id = tm.sensor_user_id', 'left');
        $this->db->where('em.ems_no', $ems_no);
        $this->db->where('tm.year', $year);
        $this->db->where('tm.month', $month);
        $this->db->order_by('tm.date asc');
        return $this->db->get()->result_array();
    }

    public function get_employee_timesheet2($employeeID)
    {
        //$this->migrate_checkinout();

        $this->db->select('cio.*');
        $this->db->select('to_char(cio.checktime, \'DD/MM/YYYY\') as date');
        $this->db->select('em.id as employee_id, concat(em.firstname_th, \' \', em.lastname_th) as fullname_th, concat(em.firstname_en, \' \', em.lastname_en) as fullname_en');
        $this->db->from($this->table_fg_checkinout . ' as cio');
        $this->db->join($this->table_fg_employee . ' as em', 'em.sensor_id = cio.userid', 'left');
        $this->db->where('em.id', $employeeID);
        $this->db->order_by('cio.checktime desc');
        return $this->db->get()->result_array();
    }

    public function update_employee_timesheet($timesheet_id, $data) {

        if (empty($data['start_work'])) {
            $data['start_work'] = NULL;
        }

        if (empty($data['end_work'])) {
            $data['end_work'] = NULL;
        }

        if (empty($data['leave_type'])) {
            $data['leave_type'] = NULL;
        }

        if (empty($data['total_hours'])) {
            $data['total_hours'] = NULL;
        }

        if (empty($data['working_day'])) {
            $data['working_day'] = NULL;
        }

        if (empty($data['ot1'])) {
            $data['ot1'] = NULL;
        }

        if (empty($data['ot15'])) {
            $data['ot15'] = NULL;
        }

        if (empty($data['ot2'])) {
            $data['ot2'] = NULL;
        }

        if (empty($data['ot3'])) {
            $data['ot3'] = NULL;
        }

        $data['record_type'] = 'M';
        $this->db->where('id', $timesheet_id);
        return $this->db->update($this->table_fg_timesheet, $data);
    }

    public function init_timesheet($year, $month, $date, $sensor_user_id)
    {
        //if case by customer
        $employee = $this->get_employee_by_sensor_user_id($sensor_user_id);

        if ($employee != NULL) {
    
            //Month Before (Start from 21st)
            $year_before = $year;
            $month_before = $month - 1;
            if ($month == 1) {
                $year_before -= 1;
                $month_before = 12;
            }

            if (empty($date)) {
                $days_before = Model_utils::get_days_in_month($year_before, $month_before);
                for ($i = 21; $i <= $days_before; $i++) {
                    $v_date = Model_utils::two_digits($i) . '/' . Model_utils::two_digits($month_before) . '/' . $year_before;
                    $this->init_timesheet_record($v_date, $sensor_user_id, $month, $year);
                }
    
                //Month (End to 20th)
                for ($i = 1; $i <= 20; $i++) {
                    $v_date = Model_utils::two_digits($i) . '/' . Model_utils::two_digits($month) . '/' . $year;
                    $this->init_timesheet_record($v_date, $sensor_user_id, $month, $year);
                }
            } else {
                $this->init_timesheet_record($date, $sensor_user_id, $month, $year);
            }

            return TRUE;
        }

        return FALSE;
    }

    private function init_timesheet_record($date, $sensor_user_id, $ctrMonth, $ctrYear) {
        $holiday = $this->get_holiday($date);
        $holiday_type = ($holiday == NULL ? NULL : $holiday['type']);
        
        $init_data = array('date' => Model_utils::convert_date_display_to_sql($date),
                'sensor_user_id' => $sensor_user_id,
                'record_type' => 'A',
                'month' => $ctrMonth,
                'year' => $ctrYear,
                'holiday_type' => $holiday_type);

        $timesheet_record = $this->get_timesheet_record($sensor_user_id, $date);

        if (count($timesheet_record) == 0) {
            //Insert
            $id = $this->common_model->gen_new_id($this->table_fg_timesheet, 'id');
            $init_data['id'] = $id;
            $this->db->insert($this->table_fg_timesheet, $init_data);

        } else if ($timesheet_record[0]['record_type'] == 'A') {
            //Update Auto Record
            $this->db->where('sensor_user_id', $sensor_user_id);
            $this->db->where('TO_CHAR(date, \'YYYY/MM/DD\') = ', $init_data['date']);
            $this->db->where('record_type', 'A');
            $this->db->update($this->table_fg_timesheet, $init_data);
        }
    }

    public function calculate_timesheet($year, $month, $date, $sensor_user_id, $config, $shift_switches)
    {
        $working_day_hours = intval($config['working_day_hours']);

        //*** Shift type */
        //if case by customer
        $employee = $this->get_employee_by_sensor_user_id($sensor_user_id);

        if ($employee != NULL) {
            $shift_name = $employee['shift_name'];
            $employee_type = $employee['type'];
    
            if (!empty($date)) {
                $date_arr = explode('/', $date);
                $d = $date_arr[0];
                $m = $date_arr[1];
                $y = $date_arr[2];

                $this->generate_timesheet_and_insert($d, $m, $y, 
                $sensor_user_id,
                $shift_name,
                $working_day_hours,
                $month,
                $year,
                $config,
                $shift_switches,
                $employee_type);
                
            } else { //date -- All --
                //Month Before (Start from 21st)
                $year_before = $year;
                $month_before = $month - 1;
                if ($month == 1) {
                    $year_before -= 1;
                    $month_before = 12;
                }

                $days_before = Model_utils::get_days_in_month($year_before, $month_before);
                for ($i = 21; $i <= $days_before; $i++) {
                    $this->generate_timesheet_and_insert($i, $month_before, $year_before, 
                    $sensor_user_id,
                    $shift_name,
                    $working_day_hours,
                    $month,
                    $year,
                    $config,
                    $shift_switches,
                    $employee_type);
                }

                //Month (End to 20th)
                for ($i = 1; $i <= 20; $i++) {
                    $this->generate_timesheet_and_insert($i, $month, $year, 
                    $sensor_user_id,
                    $shift_name,
                    $working_day_hours,
                    $month,
                    $year,
                    $config,
                    $shift_switches,
                    $employee_type);
                }
            }

            return TRUE;
        }

        return FALSE;
    }
    
    public function generate_checkinout_config()
    {
        $config_arr = $this->common_model->get_fg_configs();
        $result_arr = array();

        if (!empty($config_arr)) {
            $result_arr['standard_shift_night_time_in'] = $this->find_config_value('standard_shift_night_time_in', $config_arr);
            $result_arr['standard_shift_night_time_out'] = $this->find_config_value('standard_shift_night_time_out', $config_arr);
            $result_arr['standard_shift_night_ot_1'] = $this->emptyTo2400($this->find_config_value('standard_shift_night_ot_1', $config_arr));
            $result_arr['standard_shift_night_ot_2'] = $this->emptyTo2400($this->find_config_value('standard_shift_night_ot_2', $config_arr));
            $result_arr['standard_shift_night_ot_3'] = $this->emptyTo2400($this->find_config_value('standard_shift_night_ot_3', $config_arr));
            $result_arr['standard_shift_night_ot_4'] = $this->emptyTo2400($this->find_config_value('standard_shift_night_ot_4', $config_arr));
            $result_arr['standard_shift_night_ot_5'] = $this->emptyTo2400($this->find_config_value('standard_shift_night_ot_5', $config_arr));
            $result_arr['standard_shift_night_ot_6'] = $this->emptyTo2400($this->find_config_value('standard_shift_night_ot_6', $config_arr));
            $result_arr['standard_shift_night_ot_7'] = $this->emptyTo2400($this->find_config_value('standard_shift_night_ot_7', $config_arr));
            $result_arr['standard_shift_night_ot_8'] = $this->emptyTo2400($this->find_config_value('standard_shift_night_ot_8', $config_arr));
            $result_arr['standard_shift_night_ot_9'] = $this->emptyTo2400($this->find_config_value('standard_shift_night_ot_9', $config_arr));

            $result_arr['standard_shift_day_time_in'] = $this->find_config_value('standard_shift_day_time_in', $config_arr);
            $result_arr['standard_shift_day_time_out'] = $this->find_config_value('standard_shift_day_time_out', $config_arr);
            $result_arr['standard_shift_day_ot_1'] = $this->emptyTo2400($this->find_config_value('standard_shift_day_ot_1', $config_arr));
            $result_arr['standard_shift_day_ot_2'] = $this->emptyTo2400($this->find_config_value('standard_shift_day_ot_2', $config_arr));
            $result_arr['standard_shift_day_ot_3'] = $this->emptyTo2400($this->find_config_value('standard_shift_day_ot_3', $config_arr));
            $result_arr['standard_shift_day_ot_4'] = $this->emptyTo2400($this->find_config_value('standard_shift_day_ot_4', $config_arr));
            $result_arr['standard_shift_day_ot_5'] = $this->emptyTo2400($this->find_config_value('standard_shift_day_ot_5', $config_arr));
            $result_arr['standard_shift_day_ot_6'] = $this->emptyTo2400($this->find_config_value('standard_shift_day_ot_6', $config_arr));
            $result_arr['standard_shift_day_ot_7'] = $this->emptyTo2400($this->find_config_value('standard_shift_day_ot_7', $config_arr));
            $result_arr['standard_shift_day_ot_8'] = $this->emptyTo2400($this->find_config_value('standard_shift_day_ot_8', $config_arr));
            $result_arr['standard_shift_day_ot_9'] = $this->emptyTo2400($this->find_config_value('standard_shift_day_ot_9', $config_arr));

            $result_arr['standard_office_day_time_in'] = $this->find_config_value('standard_office_day_time_in', $config_arr);
            $result_arr['standard_office_day_time_out'] = $this->find_config_value('standard_office_day_time_out', $config_arr);
            $result_arr['standard_office_day_ot_1'] = $this->emptyTo2400($this->find_config_value('standard_office_day_ot_1', $config_arr));
            $result_arr['standard_office_day_ot_2'] = $this->emptyTo2400($this->find_config_value('standard_office_day_ot_2', $config_arr));
            $result_arr['standard_office_day_ot_3'] = $this->emptyTo2400($this->find_config_value('standard_office_day_ot_3', $config_arr));
            $result_arr['standard_office_day_ot_4'] = $this->emptyTo2400($this->find_config_value('standard_office_day_ot_4', $config_arr));
            $result_arr['standard_office_day_ot_5'] = $this->emptyTo2400($this->find_config_value('standard_office_day_ot_5', $config_arr));
            $result_arr['standard_office_day_ot_6'] = $this->emptyTo2400($this->find_config_value('standard_office_day_ot_6', $config_arr));
            $result_arr['standard_office_day_ot_7'] = $this->emptyTo2400($this->find_config_value('standard_office_day_ot_7', $config_arr));
            $result_arr['standard_office_day_ot_8'] = $this->emptyTo2400($this->find_config_value('standard_office_day_ot_8', $config_arr));
            $result_arr['standard_office_day_ot_9'] = $this->emptyTo2400($this->find_config_value('standard_office_day_ot_9', $config_arr));

            $result_arr['standard_factory_day_time_in'] = $this->find_config_value('standard_factory_day_time_in', $config_arr);
            $result_arr['standard_factory_day_time_out'] = $this->find_config_value('standard_factory_day_time_out', $config_arr);
            $result_arr['standard_factory_day_ot_1'] = $this->emptyTo2400($this->find_config_value('standard_factory_day_ot_1', $config_arr));
            $result_arr['standard_factory_day_ot_2'] = $this->emptyTo2400($this->find_config_value('standard_factory_day_ot_2', $config_arr));
            $result_arr['standard_factory_day_ot_3'] = $this->emptyTo2400($this->find_config_value('standard_factory_day_ot_3', $config_arr));
            $result_arr['standard_factory_day_ot_4'] = $this->emptyTo2400($this->find_config_value('standard_factory_day_ot_4', $config_arr));
            $result_arr['standard_factory_day_ot_5'] = $this->emptyTo2400($this->find_config_value('standard_factory_day_ot_5', $config_arr));
            $result_arr['standard_factory_day_ot_6'] = $this->emptyTo2400($this->find_config_value('standard_factory_day_ot_6', $config_arr));
            $result_arr['standard_factory_day_ot_7'] = $this->emptyTo2400($this->find_config_value('standard_factory_day_ot_7', $config_arr));
            $result_arr['standard_factory_day_ot_8'] = $this->emptyTo2400($this->find_config_value('standard_factory_day_ot_8', $config_arr));
            $result_arr['standard_factory_day_ot_9'] = $this->emptyTo2400($this->find_config_value('standard_factory_day_ot_9', $config_arr));

            $result_arr['working_day_hours'] = $this->find_config_value('working_day_hours', $config_arr);
            return $result_arr;
        }

        return NULL;
    }

    private function emptyTo2400($value)
    {
        return empty($value) ? '24:00' : $value;
    }

    private function find_config_value($key, $config_arr)
    {
        if (!empty($config_arr)) {
            for ($i = 0; $i < count($config_arr); $i++) {
                if ($config_arr[$i]['key'] == $key) {
                    return $config_arr[$i]['value'];
                }
            }
        }

        return NULL;
    }

    public function get_employee_by_sensor_user_id($sensor_user_id)
    {
        $this->db->select('em.*');
        $this->db->select('sh.name as shift_name, sh.id as shift_id');
        $this->db->from($this->table_fg_employee . ' as em');
        $this->db->join($this->table_fg_shift . ' as sh', 'sh.id = em.shift', 'left');
        $this->db->where('em.id', $sensor_user_id);
        
        $result_arr = $this->db->get()->result_array();
        
        if (!empty($result_arr)) {
            return $result_arr[0];
        }
        return NULL;
    }

    private function generate_timesheet_and_insert($day, $month, $year, 
        $sensor_user_id,
        $shift_name,
        $working_day_hours,
        $ctrMonth,
        $ctrYear,
        $config,
        $shift_switches,
        $employee_type)
    {
        $time_in = NULL;
        $time_out = NULL;
        $time_ot_1 = NULL;
        $time_ot_2 = NULL;
        $time_ot_3 = NULL;
        $time_ot_4 = NULL;
        $time_ot_5 = NULL;
        $time_ot_6 = NULL;

        $date = Model_utils::two_digits($day) . '/' . Model_utils::two_digits($month) . '/' . $year;
        $holiday = $this->get_holiday($date);
        $holiday_type = ($holiday == NULL ? NULL : $holiday['type']);

        $timesheet_record = $this->get_timesheet_record($sensor_user_id, $date);

        //Update Manual Record
        if (count($timesheet_record) > 0 && $timesheet_record[0]['record_type'] == 'M') {
            $this->update_timesheet_manual($date, $sensor_user_id, $holiday_type);

        } else {
            $is_overnight = $this->is_overnight($sensor_user_id, $shift_name, $date, $shift_switches);

            if ($shift_name == 'A' || $shift_name == 'B') {
                if($is_overnight == TRUE) {
                    $next_date = Model_utils::get_next_date($date);
                    $time_in = $date . ' ' . $config['standard_shift_night_time_in'];
                    $time_out = $next_date . ' ' . $config['standard_shift_night_time_out'];
                    $time_ot_1 = $next_date . ' ' . $config['standard_shift_night_ot_1'];
                    $time_ot_2 = $next_date . ' ' . $config['standard_shift_night_ot_2'];
                    $time_ot_3 = $next_date . ' ' . $config['standard_shift_night_ot_3'];
                    $time_ot_4 = $next_date . ' ' . $config['standard_shift_night_ot_4'];
                    $time_ot_5 = $next_date . ' ' . $config['standard_shift_night_ot_5'];
                    $time_ot_6 = $next_date . ' ' . $config['standard_shift_night_ot_6'];
                    $time_ot_7 = $next_date . ' ' . $config['standard_shift_night_ot_7'];
                    $time_ot_8 = $next_date . ' ' . $config['standard_shift_night_ot_8'];
                    $time_ot_9 = $next_date . ' ' . $config['standard_shift_night_ot_9'];
                } else {
                    $time_in = $date . ' ' . $config['standard_shift_day_time_in'];
                    $time_out = $date . ' ' . $config['standard_shift_day_time_out'];
                    $time_ot_1 = $date . ' ' . $config['standard_shift_day_ot_1'];
                    $time_ot_2 = $date . ' ' . $config['standard_shift_day_ot_2'];
                    $time_ot_3 = $date . ' ' . $config['standard_shift_day_ot_3'];
                    $time_ot_4 = $date . ' ' . $config['standard_shift_day_ot_4'];
                    $time_ot_5 = $date . ' ' . $config['standard_shift_day_ot_5'];
                    $time_ot_6 = $date . ' ' . $config['standard_shift_day_ot_6'];
                    $time_ot_7 = $date . ' ' . $config['standard_shift_day_ot_7'];
                    $time_ot_8 = $date . ' ' . $config['standard_shift_day_ot_8'];
                    $time_ot_9 = $date . ' ' . $config['standard_shift_day_ot_9'];
                }
    
            } else {
                if ($shift_name == 'Day(Office)'    ) {
                    $time_in = $date . ' ' . $config['standard_office_day_time_in'];
                    $time_out = $date . ' ' . $config['standard_office_day_time_out'];
                    $time_ot_1 = $date . ' ' . $config['standard_office_day_ot_1'];
                    $time_ot_2 = $date . ' ' . $config['standard_office_day_ot_2'];
                    $time_ot_3 = $date . ' ' . $config['standard_office_day_ot_3'];
                    $time_ot_4 = $date . ' ' . $config['standard_office_day_ot_4'];
                    $time_ot_5 = $date . ' ' . $config['standard_office_day_ot_5'];
                    $time_ot_6 = $date . ' ' . $config['standard_office_day_ot_6'];
                    $time_ot_7 = $date . ' ' . $config['standard_office_day_ot_7'];
                    $time_ot_8 = $date . ' ' . $config['standard_office_day_ot_8'];
                    $time_ot_9 = $date . ' ' . $config['standard_office_day_ot_9'];
                } else {
                    $time_in = $date . ' ' . $config['standard_factory_day_time_in'];
                    $time_out = $date . ' ' . $config['standard_factory_day_time_out'];
                    $time_ot_1 = $date . ' ' . $config['standard_factory_day_ot_1'];
                    $time_ot_2 = $date . ' ' . $config['standard_factory_day_ot_2'];
                    $time_ot_3 = $date . ' ' . $config['standard_factory_day_ot_3'];
                    $time_ot_4 = $date . ' ' . $config['standard_factory_day_ot_4'];
                    $time_ot_5 = $date . ' ' . $config['standard_factory_day_ot_5'];
                    $time_ot_6 = $date . ' ' . $config['standard_factory_day_ot_6'];
                    $time_ot_7 = $date . ' ' . $config['standard_factory_day_ot_7'];
                    $time_ot_8 = $date . ' ' . $config['standard_factory_day_ot_8'];
                    $time_ot_9 = $date . ' ' . $config['standard_factory_day_ot_9'];
                }
                
            }
    
            $time_in_minutes = Model_utils::datetimestr_totime($time_in);
            $time_out_minutes = Model_utils::datetimestr_totime($time_out);
            $time_ot_1_minutes = Model_utils::datetimestr_totime($time_ot_1);
            $time_ot_2_minutes = Model_utils::datetimestr_totime($time_ot_2);
            $time_ot_3_minutes = Model_utils::datetimestr_totime($time_ot_3);
            $time_ot_4_minutes = Model_utils::datetimestr_totime($time_ot_4);
            $time_ot_5_minutes = Model_utils::datetimestr_totime($time_ot_5);
            $time_ot_6_minutes = Model_utils::datetimestr_totime($time_ot_6);
            $time_ot_7_minutes = Model_utils::datetimestr_totime($time_ot_7);
            $time_ot_8_minutes = Model_utils::datetimestr_totime($time_ot_8);
            $time_ot_9_minutes = Model_utils::datetimestr_totime($time_ot_9);
    
            //Special Date (1 : Normal, 2 : Special)
            $ot_type = $this->is_special_date($date) ? 2 : 1;
            $special_work_type = $this->get_special_work_type($date, $employee_type);
            $special_ot_type = $this->get_special_ot_type($date, $employee_type);
    
            $cal_data = $this->calculate_timesheet_record($sensor_user_id, 
            $shift_name,
            $date, 
            $working_day_hours, 
            $time_in_minutes,
            $time_out_minutes, 
            $ot_type,
            $special_work_type,
            $special_ot_type,
            $time_ot_1_minutes,
            $time_ot_2_minutes,
            $time_ot_3_minutes,
            $time_ot_4_minutes,
            $time_ot_5_minutes,
            $time_ot_6_minutes,
            $time_ot_7_minutes,
            $time_ot_8_minutes,
            $time_ot_9_minutes,
            $is_overnight,
            $shift_switches);
    
            if ($cal_data == NULL) {
                $cal_data = array('date' => Model_utils::convert_date_display_to_sql($date),
                'sensor_user_id' => $sensor_user_id,
                'record_type' => 'A',
                'month' => $ctrMonth,
                'year' => $ctrYear,
                'holiday_type' => $holiday_type);
            } else {
                $cal_data['month'] = $ctrMonth;
                $cal_data['year'] = $ctrYear;
                $cal_data['holiday_type'] = $holiday_type;
            }

            $this->insert_timesheet_by_calculator($cal_data, $sensor_user_id, $timesheet_record);
        }
        
    }

    private function get_timesheet_record($sensor_user_id, $date)
    {
        $this->db->select('record_type');
        $this->db->from($this->table_fg_timesheet);
        $this->db->where('sensor_user_id', $sensor_user_id);
        $this->db->where('TO_CHAR(date, \'YYYY/MM/DD\') = ', Model_utils::convert_date_display_to_sql($date)); //$cal_data['date']);
        return $this->db->get()->result_array();
    }

    public function insert_timesheet_by_calculator($cal_data, $sensor_user_id, $timesheet_record)
    {
        $affected_rows = 0;

        /*
        $this->db->select('record_type');
        $this->db->from($this->table_fg_timesheet);
        $this->db->where('sensor_user_id', $sensor_user_id);
        $this->db->where('TO_CHAR(date, \'YYYY/MM/DD\') = ', $cal_data['date']);
        $result_arr = $this->db->get()->result_array();
        */

        if (count($timesheet_record) == 0) {
            //Insert
            $id = $this->common_model->gen_new_id($this->table_fg_timesheet, 'id');
            $cal_data['id'] = $id;
            $this->db->insert($this->table_fg_timesheet, $cal_data);
            $affected_rows = $this->db->affected_rows();

        } else if ($timesheet_record[0]['record_type'] == 'A') {
            //Auto Record
            $this->db->where('sensor_user_id', $sensor_user_id);
            $this->db->where('TO_CHAR(date, \'YYYY/MM/DD\') = ', $cal_data['date']);
            $this->db->where('record_type', 'A');
            $this->db->update($this->table_fg_timesheet, $cal_data);
            $affected_rows = $this->db->affected_rows();

        } 
        /*
        else if ($timesheet_record[0]['record_type'] == 'M') {
            //Manual Record
            $data_for_manual_record = array('holiday_type' => $cal_data['holiday_type']);

            $this->db->where('sensor_user_id', $sensor_user_id);
            $this->db->where('TO_CHAR(date, \'YYYY/MM/DD\') = ', $cal_data['date']);
            $this->db->where('record_type', 'M');
            $this->db->update($this->table_fg_timesheet, $data_for_manual_record);
            $affected_rows = $this->db->affected_rows();
        }
        */

        return $affected_rows;
    }

    private function update_timesheet_manual($date, $sensor_user_id, $holiday_type)
    {
        //Manual Record
        $data_for_manual_record = array('holiday_type' => $holiday_type);

        $this->db->where('sensor_user_id', $sensor_user_id);
        $this->db->where('TO_CHAR(date, \'YYYY/MM/DD\') = ', Model_utils::convert_date_display_to_sql($date));
        $this->db->where('record_type', 'M');
        $this->db->update($this->table_fg_timesheet, $data_for_manual_record);
        $affected_rows = $this->db->affected_rows();
    }

    public function calculate_timesheet_record($sensor_user_id, 
        $shift_name,
        $date, 
        $working_day_hours, 
        $time_in_minutes,
        $time_out_minutes,
        $ot_type,
        $special_work_type,
        $special_ot_type,
        $time_ot_1_minutes,
        $time_ot_2_minutes,
        $time_ot_3_minutes,
        $time_ot_4_minutes,
        $time_ot_5_minutes,
        $time_ot_6_minutes,
        $time_ot_7_minutes,
        $time_ot_8_minutes,
        $time_ot_9_minutes,
        $is_overnight,
        $shift_switches)
    {

        $next_date = Model_utils::get_next_date($date);

        //-- Skip shift switching Night > Day\\\
        $current_shift_switch = $this->get_current_shift_switch($date, $shift_switches);
        if ($current_shift_switch != NULL && $current_shift_switch['fm_date'] == $date) {
            
            $prev_date = Model_utils::get_prev_date($date);
            $has_night_shift_time_in = $this->has_night_shift_time_in($sensor_user_id, $is_overnight, $prev_date);

            if (($current_shift_switch['shift_a'] == 'D' && $shift_name == 'A' && $has_night_shift_time_in == TRUE) ||
                ($current_shift_switch['shift_b'] == 'D' && $shift_name == 'B' && $has_night_shift_time_in == TRUE)) {
                return NULL;
            }
        }
        //-- End Skip

        $this->db->select('TO_CHAR(cio.checktime, \'HH24:MI\') as time');
        $this->db->select('TO_CHAR(cio.checktime, \'DD/MM/YYYY\') as date');
        $this->db->from($this->table_fg_checkinout . ' as cio');
        $this->db->where('cio.badgenumber', $sensor_user_id);
        $this->db->order_by('cio.checktime asc');

        if ($is_overnight == TRUE) {
            $this->db->where('cio.checktime >= TO_TIMESTAMP(\'' . $date . ' 12:00\', \'DD/MM/YYYY HH24:MI\')');
            $this->db->where('cio.checktime <= TO_TIMESTAMP(\'' . $next_date . ' 12:00\', \'DD/MM/YYYY HH24:MI\')');
        } else {
            $this->db->where('TO_CHAR(cio.checktime, \'DD/MM/YYYY\') = ', $date);
        }

        $checkinout_arr = $this->db->get()->result_array();

        if (!empty($checkinout_arr)) {
            $start_work = $checkinout_arr[0]['time'];
            $start_work_date = $checkinout_arr[0]['date'];
            $start_work_minutes = Model_utils::datetimestr_totime($start_work_date . ' ' . $start_work);
            $end_work = NULL;
            $end_work_minutes = 0;
            $mid_day_minutes = Model_utils::datetimestr_totime($date . ' 13:00');
            $total_hours = 0;
            $working_day = 0;
            $sum_ot = 0;
            $ot1 = 0;
            $ot15 = 0;
            $ot2 = 0;
            $ot3 = 0;

            //Doesn't match Start Work
            if ($date != $checkinout_arr[0]['date']) {
                $start_work = NULL;
                $start_work_minutes = -1;
            }

            //End Work
            if (count($checkinout_arr) > 1) {
                $end_work = $checkinout_arr[count($checkinout_arr)-1]['time'];
                $end_work_date = $checkinout_arr[count($checkinout_arr)-1]['date'];
                $end_work_minutes = Model_utils::datetimestr_totime($end_work_date . ' ' . $end_work);
            } else {
                $end_work_minutes = $start_work_minutes;

                //Only End Work
                if ($start_work_minutes == -1) {
                    $end_work = $checkinout_arr[0]['time'];
                }
            }

            //-- Skip Next Day is shift switching Night > Day
            if ($this->is_day_shift_switch_date($next_date, $shift_switches) && $start_work_minutes == -1) {
                return NULL;
            }
            //-- End Skip

            //Total Hours
            $start_time = $start_work_minutes < $time_in_minutes ? $time_in_minutes : $start_work_minutes;
            $end_time = $end_work_minutes < $time_out_minutes ? $end_work_minutes : $time_out_minutes;
            $total_hours = floor(($end_time - $start_time) / 3600);

            //--Fix date bug 
            if (date('m-d', $end_time) == '03-31' && date('H:i', $time_out_minutes) == '04:30') {
                $total_hours += 1;
            }
            //--Fix date bug 
            if (date('m-d', $end_time) == '10-27' && date('H:i', $time_out_minutes) == '04:30') {
                $total_hours -= 1;
            }

            if ($end_time > $mid_day_minutes && $total_hours > 4) {
                $total_hours -= 1;
            }

            $total_hours = $total_hours < 0 ? 0 : $total_hours;

            //Night Shift
            $night_shift = 0;
            if ($is_overnight == TRUE && $total_hours >= 4 && ($start_work_minutes != -1 && $start_work_minutes <= $time_in_minutes && $end_work_minutes >= $time_out_minutes)) {
                $night_shift = 1;
            }

            //Food Travel Day
            $food_travel_day = 0;
            if ($total_hours >= 1) {
                $food_travel_day = 1;
            }

            //Working Day
            $working_day = floor($total_hours / $working_day_hours);

            //OT
            if ($working_day > 0) {
                if (($time_ot_1_minutes > -1) && $end_work_minutes >= $time_ot_1_minutes) {
                    $sum_ot += 0.5;
                }
                if (($time_ot_2_minutes > -1) && $end_work_minutes >= $time_ot_2_minutes) {
                    $sum_ot += 0.5;
                }
                if (($time_ot_3_minutes > -1) && $end_work_minutes >= $time_ot_3_minutes) {
                    $sum_ot += 0.5;
                }
                if (($time_ot_4_minutes > -1) && $end_work_minutes >= $time_ot_4_minutes) {
                    $sum_ot += 0.5;
                }
                if (($time_ot_5_minutes > -1) && $end_work_minutes >= $time_ot_5_minutes) {
                    $sum_ot += 0.5;
                }
                if (($time_ot_6_minutes > -1) && $end_work_minutes >= $time_ot_6_minutes) {
                    $sum_ot += 0.5;
                }
                if (($time_ot_7_minutes > -1) && $end_work_minutes >= $time_ot_7_minutes) {
                    $sum_ot += 1;
                }
                if (($time_ot_8_minutes > -1) && $end_work_minutes >= $time_ot_8_minutes) {
                    $sum_ot += 1;
                }
                if (($time_ot_9_minutes > -1) && $end_work_minutes >= $time_ot_9_minutes) {
                    $sum_ot += 1;
                }

                //OT Type (1 : Normal, 2 : Special)
                if ($ot_type == 2) {
                    if ($special_work_type == 1) {
                        $ot1 = $total_hours;
                    } else if ($special_work_type == 2) {
                        $ot2 = $total_hours;
                    }

                    if ($special_ot_type == 3) {
                        $ot3 = $sum_ot;
                    }

                    $total_hours = 0;
                    $working_day = 0;

                } else {
                    $ot15 = $sum_ot;
                }
            }

            return array('sensor_user_id' => $sensor_user_id,
                'date' => Model_utils::convert_date_display_to_sql($date),
                'start_work' => $start_work,
                'end_work' => $end_work,
                'total_hours' => $total_hours,
                'working_day' => $working_day,
                'ot1' => $ot1,
                'ot15' => $ot15,
                'ot2' => $ot2,
                'ot3' => $ot3,
                'month' => Model_utils::get_month_from_date($date),
                'night_shift' => $night_shift,
                'food_travel_day' => $food_travel_day,
                'record_type' => 'A');
        }

        return NULL;
    }

    private function is_overnight($sensor_user_id, $shift_name, $date, $shift_switches)
    {
        $current_shift_switch = $this->get_current_shift_switch($date, $shift_switches);

        /*
        if ($date == '18/03/2019') {
            echo '<< shift_name:' . $shift_name;
            echo '<< shift_a:' . $current_shift_switch['shift_a'];
            print_r($current_shift_switch);
        }
        */

        if ($current_shift_switch != NULL) {
            if ($shift_name == 'A') {
                return $current_shift_switch['shift_a'] == 'N';
            } else if ($shift_name == 'B') {
                return $current_shift_switch['shift_b'] == 'N';
            }
        }

        return FALSE;
    }

    private function get_current_shift_switch($date, $shift_switches)
    {
        $current_shift_switch = NULL;

        if (!empty($shift_switches)) {

            $length = count($shift_switches);
            for ($i = 0; $i < $length; $i++) {
                $next_i = ($i == ($length - 1)) ? $i : $i + 1;

                $date_minutes = Model_utils::datetimestr_totime($date . ' 00:00');
                $from_minutes = Model_utils::datetimestr_totime($shift_switches[$i]['fm_date'] . ' 00:00');

                //Last Item
                if ($next_i == $i && $date_minutes >= $from_minutes) {
                    $current_shift_switch = $shift_switches[$i];

                } else {
                    $to_minutes = Model_utils::datetimestr_totime($shift_switches[$next_i]['fm_date'] . ' 00:00');
                    
                    if ($date_minutes >= $from_minutes &&  $date_minutes < $to_minutes) {
                        $current_shift_switch = $shift_switches[$i];
                        break;
                    }
                }
            }
        }

        return $current_shift_switch;
    }

    private function get_special_work_type($date, $employee_type)
    {
        $holiday = $this->get_holiday($date);
        
        if ($holiday != NULL) {
            $type = $holiday['type'];
            
            if ($type == '1') { //RED
                if ($employee_type == 'A') {
                   return 1;

                } else if ($employee_type == 'B' || $employee_type == 'FP' || $employee_type == 'C') {
                   return 2;
                }

            } else if ($type == '2') { //PURPLE
                if ($employee_type == 'A') {
                    return 1;

                } else if ($employee_type == 'B' || $employee_type == 'FP' || $employee_type == 'C') {
                    return 2;

                }

            } else if ($type == '3') { //YELLOW
                if ($employee_type == 'A') {
                    return 2;

                } else if ($employee_type == 'B' || $employee_type == 'FP' || $employee_type == 'C') {
                    return 2;

                }
                
            } else if ($type == '4') { //GREEN
                if ($employee_type == 'A') {
                    return 2;

                } else if ($employee_type == 'B' || $employee_type == 'FP' || $employee_type == 'C') {
                    return 2;

                }
                
            }
        }

        return 0;
    }

    private function get_special_ot_type($date, $employee_type)
    {
        $holiday = $this->get_holiday($date);
        
        if ($holiday != NULL) {
            return 3;
        }

        return 0;
    }

    private function is_special_date($date)
    {
        $holiday = $this->get_holiday($date);
        return $holiday != NULL && $holiday['type'] != '0';
    }

    public function convert_time_to_minute($str_time) 
    {
        $st_arr = explode(':', $str_time);
        return (60 * intval($st_arr[0])) + intval($st_arr[1]);
    }

    public function insert_monthly_data($data, $uid)
    {
        if (empty($data['year'])) {
            $data['year'] = NULL;
        }

        if (empty($data['month'])) {
            $data['month'] = NULL;
        }

        $id = $this->common_model->gen_new_id($this->table_fg_monthly_data, 'id');
        $data['id'] = $id;
        $this->db->set('create_uid', $uid);
        $this->db->set('create_date', 'NOW()', FALSE);
        return $this->db->insert($this->table_fg_monthly_data, $data);
    }

    public function update_monthly_data($ems_no, $year, $month, $data, $uid)
    {
        $this->db->where('ems_no', $ems_no);
        $this->db->where('year', $year);
        $this->db->where('CAST(month as integer) = ', $month);
        $this->db->set('update_uid', $uid);
        $this->db->set('update_date', 'NOW()', FALSE);
        return $this->db->update($this->table_fg_monthly_data, $data);
    }

    public function get_monthly_data($ems_no, $year, $month)
    {
        $this->db->select('*');
        $this->db->from($this->table_fg_monthly_data);
        $this->db->where('ems_no', $ems_no);
        $this->db->where('year', $year);
        $this->db->where('month', $month);
        return $this->db->get()->result_array();
    }

    public function get_holiday($date) 
    {
        $this->db->select('*');
        $this->db->from($this->table_fg_holiday);
        $this->db->where('TO_CHAR(date, \'DD/MM/YYYY\') = ', $date);
        
        $result = $this->db->get()->result_array();
        return !empty($result) ? $result[0] : NULL;
    }

    public function get_checkinout_badgenumbers()
    {
        $this->db->select('distinct(badgenumber) as badgenumber');
        $this->db->from($this->table_fg_checkinout);
        return $this->db->get()->result_array();
    }

    private function has_night_shift_time_in($badgenumber, $is_overnight, $prev_date) 
    {
        if ($is_overnight) {
            $this->db->select('*');
            $this->db->from($this->table_fg_checkinout);
            $this->db->where('badgenumber', $badgenumber);
            $this->db->where('to_char(checktime, \'DD/MM/YYYY\') = ', $prev_date);
            $this->db->where('checktime >= to_timestamp(\'' . $prev_date . ' 12:00\', \'DD/MM/YYYY HH24:MI\')');
            return !empty($this->db->get()->result_array());
        }
        return FALSE;
    }

    private function is_day_shift_switch_date($date, $shift_switches)
    {
        for ($i = 0; $i  < count($shift_switches); $i++) {
            if ($date == $shift_switches[$i]['fm_date']) {
                return TRUE;
            }
        }
        return FALSE;
    }
}