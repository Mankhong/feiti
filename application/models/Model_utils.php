<?php defined('BASEPATH') or exit('No direct script access allowed');

class Model_utils
{
    public static function sql_last_update_date($table) {
        return '(case when ' . $table . '.update_date is null then to_char(' . $table . '.create_date , \'DD/MM/YYYY\') else to_char(' . $table . '.update_date , \'DD/MM/YYYY\') end) as last_update_date';
    }

    public static function sql_last_update_user($table) {
        return '(case when ' . $table . '.update_date is null then 
                (select concat(us9.first_name, \' \', us9.last_name) from users as us9 where us9.id = ' . $table . '.create_uid)  else (select concat(us9.first_name, \' \', us9.last_name) from users as us9 where us9.id = ' . $table . '.update_uid) end) as last_update_user';
    }

    public static function currencyToNumber($c) {
        return str_replace(',','',$c);
    }

    //Ex. 2018/12/30 to 30/12/2018
    public static function convert_date_sql_to_display($date, $delimiter = '/')
    {
        $d_arr = explode($delimiter, $date);
        if (strlen($d_arr[0]) == 4) {
            return $d_arr[2] . '/' . $d_arr[1] . '/' . $d_arr[0];
        } else {
            return $date;
        }
    }

    //Ex. 30/12/2018 to 2018/12/30
    public static function convert_date_display_to_sql($date)
    {
        $d_arr = explode('/', $date);
        if (strlen($d_arr[2]) == 4) {
            return $d_arr[2] . '/' . $d_arr[1] . '/' . $d_arr[0];
        } else {
            return $date;
        }
    }

    public static function get_month_from_date($date)
    {
        $d_arr = explode('/', $date);
        return intval($d_arr[1]);
    }

    public static function two_digits($number)
    {
        return intval($number) < 10 ? '0' . $number : (string) $number;
    }

    public static function get_days_in_month($year, $month)
    {
        return cal_days_in_month(CAL_GREGORIAN, $month, $year);
    }

    public static function get_days_in_before_month($year, $month)
    {
        if ($month == 1) {
            $year -= 1;
            $month = 12;
        }
        return cal_days_in_month(CAL_GREGORIAN, $month, $year);
    }

    //Example : date = '24/02/2019 20:00'
    public static function datetimestr_totime($date)
    {
        return (DateTime::createFromFormat('d/m/Y H:i', $date)->getTimestamp());
    }

    public static function get_next_date($date)
    {
        $day_time = 86400;
        return date('d/m/Y', (DateTime::createFromFormat('d/m/Y', $date)->getTimestamp() + $day_time));
    }

    public static function get_prev_date($date)
    {
        $day_time = 86400;
        return date('d/m/Y', (DateTime::createFromFormat('d/m/Y', $date)->getTimestamp() - $day_time));
    }
}