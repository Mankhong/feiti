<?php defined('BASEPATH') or exit('No direct script access allowed');

class Common_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->table_statuses = 'statuses';
        $this->table_configs = 'configs';
        $this->table_continents = 'continents';
        $this->table_countries = 'countries';

        $this->table_fg_employee_type = 'fg_employee_type';
        $this->table_fg_nationality = 'fg_nationality';
        $this->table_fg_certificate_level = 'fg_certificate_level';
        $this->table_fg_gender = 'fg_gender';
        $this->table_fg_marital_status = 'fg_marital_status';
        $this->table_fg_department = 'fg_department';
        $this->table_fg_job_position = 'fg_job_position';
        $this->table_fg_manager = 'fg_manager';
        $this->table_fg_shift = 'fg_shift';
        $this->table_fg_title = 'fg_title_name';
        $this->table_fg_config = 'fg_config';
        $this->table_fg_leave_type = 'fg_leave_type';
        $this->table_fg_duration = 'fg_duration';
        $this->table_fg_holiday_type = 'fg_holiday_type';
    }

    public function get_statuses($id = '')
    {
        $this->db->select('*');
        $this->db->from($this->table_statuses);

        if (!empty($id)) {
            $this->db->where('id', $id);
        }
        return $this->db->get()->result_array();
    }

    public function get_vat_config()
    {
        $this->db->where('config_key', 'VAT');
        $row = $this->db->get($this->table_configs)->row();
        if (isset($row)) {
            return $row->config_value;
        }
        return 0;
    }

    public function get_continents()
    {
        $this->db->select('*');
        $this->db->from($this->table_continents);
        return $this->db->get()->result_array();
    }

    public function get_countries($continent)
    {
        $this->db->where('continent', $continent);
        return$this->db->get($this->table_countries)->result_array();
    }

    public function get_employee_type($employee_type = NULL)
    {
        $this->db->select('*');
        $this->db->from($this->table_fg_employee_type);

        if ($employee_type == 'FP' || $employee_type == 'C') {
            $this->db->where('code', $employee_type);
        } else if ($employee_type == 'A' || $employee_type == 'B') {
            $this->db->where_in('code', array('A', 'B'));
        }

        $this->db->order_by('code', 'asc');

        $query =  $this->db->get();
        return $query->result_array();
    }

    public function get_nationality()
    {
        $this->db->select('*');
        $this->db->from($this->table_fg_nationality);
        return $this->db->get()->result_array();
    }

    public function get_certificate_level()
    {
        $this->db->select('*');
        $this->db->from($this->table_fg_certificate_level);
        return $this->db->get()->result_array();
    }

    public function get_gender()
    {
        $this->db->select('*');
        $this->db->from($this->table_fg_gender);
        return $this->db->get()->result_array();
    }

    public function get_marital_status()
    {
        $this->db->select('*');
        $this->db->from($this->table_fg_marital_status);
        return $this->db->get()->result_array();
    }

    public function get_department()
    {
        $this->db->select('id, concat(name, \' (\', id, \')\') as name');
        $this->db->from($this->table_fg_department);
        return $this->db->get()->result_array();
    }

    public function get_job_position()
    {
        $this->db->select('*');
        $this->db->from($this->table_fg_job_position);
        $this->db->order_by('name', 'asc');

        return $this->db->get()->result_array();
    }

    public function get_manager()
    {
        $this->db->select('*');
        $this->db->from($this->table_fg_manager);
        return $this->db->get()->result_array();
    }

    public function get_shift($is_sub_contract = FALSE, $is_admin = FALSE)
    {
        $this->db->select('*');
        $this->db->from($this->table_fg_shift);

        if (!$is_admin && $is_sub_contract) {
            $this->db->where('id <> 4');
        }

        return $this->db->get()->result_array();
    }

    public function get_title()
    {
        $this->db->select('*');
        $this->db->from($this->table_fg_title);
        return $this->db->get()->result_array();
    }

    public function get_fg_configs()
    {
        $this->db->select('*');
        $this->db->from($this->table_fg_config);
        return $this->db->get()->result_array();
    }

    public function get_fg_config_value($key)
    {
        $this->db->select('*');
        $this->db->from($this->table_fg_config);
        $this->db->where('key', $key);

        $result =  $this->db->get()->result_array();
        if (!empty($result)) {
            return $result[0]['value'];
        }

        return NULL;
    }

    public function gen_new_id($table_name, $column_name)
    {
        $this->db->select('COALESCE(MAX(' . $column_name . '),0) + 1 as id');
        $this->db->from($table_name);
        $id = $this->db->get()->result_array()[0]['id'];
        return $id;
    }

    public function get_leave_type()
    {
        $this->db->select('concat(name, \' (\', code, \')\') as name, code, id');
        $this->db->from($this->table_fg_leave_type);
        return $this->db->get()->result_array();
    }

    public function get_duration()
    {
        $this->db->select('*');
        $this->db->from($this->table_fg_duration);
        return $this->db->get()->result_array();
    }

    public function get_holiday_type() {
        $this->db->select('*');
        $this->db->from($this->table_fg_holiday_type);
        return $this->db->get()->result_array();
    }

}
