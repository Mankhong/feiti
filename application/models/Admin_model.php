<?php defined('BASEPATH') or exit('No direct script access allowed');

class Admin_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->table_users = 'users';
        $this->table_groups = 'groups';
        $this->table_users_groups = 'users_groups';
    }

    public function get_users($id, $current_groups)
    {
        $where_sale = 'us.id in (select user_id from ' . $this->table_users_groups . ' where group_id in(3, 4))';
        $where_personal = 'us.id in (select user_id from ' . $this->table_users_groups . ' where group_id in(5, 6, 7, 8))';

        $this->db->select('us.*');
        $this->db->from($this->table_users . ' as us');
        //$this->db->where('us.id <> ', $id);

        if ($this->has_group('1', $current_groups)) {
            //select all
        } else if ($this->has_group('3', $current_groups)) { //Sale Admin
            $this->db->where($where_sale);
            
        } else if ($this->has_group('5', $current_groups)) { //Personal Admin
            $this->db->where($where_personal);
        } else {
            return array();
        }

        //-- HIDE SYSTEM ADMINS --//
        $this->db->where('us.email !=', 'ptnpm555@gmail.com');
        $this->db->where('us.email !=', 'ktree23@gmail.com');
        $this->db->where('us.email !=', 'sadmin1@mail.com');
        $this->db->where('us.email !=', 'padmin1@mail.com');

        $this->db->order_by('us.id');
        
        return $this->db->get()->result();
    }

    public function get_groups($current_groups)
    {
        if ($this->has_group('1', $current_groups)) {
            //select all
        } else if ($this->has_group('3', $current_groups)) { //Sale Admin
            $this->db->where_in('id', array('3', '4'));
            
        } else if ($this->has_group('5', $current_groups)) { //Personal Admin
            $this->db->where_in('id', array('5', '6', '7', '8'));
        } else {
            return array();
        }

        $this->db->order_by('id');
        
        return $this->db->get($this->table_groups)->result_array();
    }

    public function get_user_id_by_email($email) 
    {
        $this->db->where('email', $email);
        $result = $this->db->get($this->table_users)->result_array();
        return !empty($result) ? $result[0]['id'] : NULL;
    }

    public function get_user_id_by_username($username) {
        $this->db->where('username', $username);
        $result = $this->db->get($this->table_users)->result_array();
        return !empty($result) ? $result[0]['id'] : NULL;
    }

    private function has_group($group_id, $current_groups) {
        foreach ($current_groups as $g) :
            if ($g->id == $group_id) {
                return TRUE;
            }
        endforeach;
        return FALSE;
    }

    public function user_has_groups($group_id, $user_id)
    {
        $this->db->select('group_id');
        $this->db->from($this->table_users_groups);
        $this->db->where('user_id', $user_id);
        $groups = $this->db->get()->result_array();
       
        if (isset($groups) && !empty($groups)) {
            foreach ($groups as $g) :
                if ($g['group_id'] == $group_id) {
                    return TRUE;
                }
            endforeach;
        }

        return FALSE;
    }

    public function delete_user($user_id)
    {
        $this->db->where('id', $user_id);
        return $this->db->delete($this->table_users);
    }

    public function is_sale_admin($id) 
    {
        return $this->user_has_groups('3', $id);
    }

    public function is_sale_member($id) 
    {
        return $this->user_has_groups('4', $id);
    }

    public function is_personal_admin($id) 
    {
        return $this->user_has_groups('5', $id);
    }

    public function is_personal_member($id) 
    {
        return $this->user_has_groups('6', $id);
    }

    public function is_personal_account($id) 
    {
        return $this->user_has_groups('7', $id);
    }

    public function is_personal_sub_contract($id) 
    {
        return $this->user_has_groups('8', $id);
    }
}
