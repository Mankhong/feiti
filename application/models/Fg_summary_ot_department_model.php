<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once('Model_utils.php');

class Fg_summary_ot_department_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->table_fg_checkinout = 'fg_checkinout';
        $this->table_fg_employee = 'fg_employee';
        $this->table_fg_timesheet = 'fg_timesheet';
        $this->table_fg_shift = 'fg_shift';
        $this->table_fg_monthly_data = 'fg_monthly_data';
        $this->table_fg_department = 'fg_department';
        $this->table_fg_position = 'fg_position';
        $this->table_fg_leave = 'fg_leave';
        $this->table_fg_leave_type = 'fg_leave_type';
        $this->table_fg_holiday = 'fg_holiday';
        $this->table_fg_holiday_type = 'fg_holiday_type';
    }

    public function get_summary_data($start_date = NULL, $end_date = NULL)
    {
        $dp_where = ' where em2.department_id = dp.id';

        if (!empty($start_date)) {
            $dp_where .= ' and ts2.date >= TO_DATE(\'' . $start_date . '\', \'DD/MM/YYYY\')';
        }

        if (!empty($end_date)) {
            $dp_where .= ' and ts2.date <= TO_DATE(\'' . $end_date . '\', \'DD/MM/YYYY\')';
        }

        $sum_ot_sql = 'select sum(ts2.ot1 + ts2.ot15 + ts2.ot2 + ts2.ot3) from ' . $this->table_fg_timesheet . ' as ts2 join ' . $this->table_fg_employee . ' as em2 on em2.id = ts2.sensor_user_id' . $dp_where;

        $this->db->select('dp.id as department_id, COALESCE((' . $sum_ot_sql . '), 0) as sum_ot');
        $this->db->from($this->table_fg_department . ' as dp');

        $query = $this->db->get();
        return $query->result_array();
    }

}