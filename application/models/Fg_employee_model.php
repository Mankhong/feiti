<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once('Model_utils.php');

class Fg_employee_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->table_fg_employee = 'fg_employee';
        $this->table_fg_employee_type = 'fg_employee_type';
        $this->table_fg_nationality = 'fg_nationality';
        $this->table_fg_certificate_level = 'fg_certificate_level';
        $this->table_fg_gender = 'fg_gender';
        $this->table_fg_marital_status = 'fg_marital_status';
        $this->table_fg_department = 'fg_department';
        $this->table_fg_job_position = 'fg_job_position';
        $this->table_fg_manager = 'fg_manager';
        $this->table_fg_shift = 'fg_shift';
    }

    public function get_employees($id = '', $employee_no = '')
    {
        $this->db->select('*, concat(firstname_th, \' \', lastname_th) as fullname_th');
        $this->db->select('TO_CHAR(birthdate,\'YYYY/MM/DD\') as birthdate_fm');
        $this->db->select('TO_CHAR(startdate,\'YYYY/MM/DD\') as startdate_fm');
        $this->db->select('TO_CHAR(resign_date,\'YYYY/MM/DD\') as resign_date_fm');
        $this->db->select('TO_CHAR(resign_probation_date,\'YYYY/MM/DD\') as resign_probation_date_fm');
        $this->db->from($this->table_fg_employee);

        if (!empty($id)) {
            $this->db->where('id', $id);
        }

        if (!empty($employee_no)) {
            $this->db->where('ems_no', $employee_no);
        }

        return $this->db->get()->result_array();
    }

    public function get_joined_employees($id = '', $is_sub_contract = FALSE, $resign_flag = NULL)
    {
        $this->db->select('em.id as employee_id, em.sensor_id, em.pid, em.house_no, concat(em.firstname_th, \' \', em.lastname_th) as fullname_th, concat(em.firstname_en, \' \', em.lastname_en) as fullname_en, em.ems_no');
        $this->db->select('TO_CHAR(em.startdate,\'DD/MM/YYYY\') as fm_startdate, TO_CHAR(em.resign_date,\'DD/MM/YYYY\') as fm_resign_date, TO_CHAR(em.resign_probation_date,\'DD/MM/YYYY\') as fm_resign_probation_date');
        $this->db->select('TO_CHAR(em.birthdate,\'DD/MM/YYYY\') as fm_birthdate');
        $this->db->select('emt.id as employee_type_id, emt.name as employee_type_name');
        $this->db->select('nt.id as nationality_id, nt.name as nationality_name');
        $this->db->select('cel.id as certificate_level_id, cel.name as certificate_level_name');
        $this->db->select('gd.id as gender_id, gd.name as gender_name');
        $this->db->select('mrs.id as marital_status_id, mrs.name as marital_status_name');
        $this->db->select('dp.id as department_id, dp.name as department_name');
        $this->db->select('jbp.id as job_position_id, jbp.name as job_position_name');
        $this->db->select('mn.id as manager_id, mn.name as manager_name');
        $this->db->select('sh.id as shift_id, sh.name as shift_name');
        $this->db->select(Model_utils::sql_last_update_date('em'));
        $this->db->select(Model_utils::sql_last_update_user('em'));
        $this->db->from($this->table_fg_employee . ' as em');
        $this->db->join($this->table_fg_employee_type . ' as emt', 'emt.code = em.type', 'left');
        $this->db->join($this->table_fg_nationality . ' as nt', 'nt.id = em.nationality', 'left');
        $this->db->join($this->table_fg_certificate_level . ' as cel', 'cel.id = em.certificate_level', 'left');
        $this->db->join($this->table_fg_gender . ' as gd', 'gd.id = em.gender', 'left');
        $this->db->join($this->table_fg_marital_status . ' as mrs', 'mrs.id = em.marital_status', 'left');
        $this->db->join($this->table_fg_department . ' as dp', 'dp.id = em.department_id', 'left');
        $this->db->join($this->table_fg_job_position . ' as jbp', 'jbp.id = em.job_position_id', 'left');
        $this->db->join($this->table_fg_manager . ' as mn', 'mn.id = em.manager_id', 'left');
        $this->db->join($this->table_fg_shift . ' as sh', 'sh.id = em.shift', 'left');

        if (!empty($id)) {
            if (isset($id)) {
                $this->db->where_in('em.id', $id);
            } else {
                $this->db->where('em.id', $id);
            }
        }

        if ($is_sub_contract == TRUE) {
            $this->db->where('(em.ems_no like \'FP%\' or em.ems_no like \'C%\')');
        }

        if ($resign_flag == 'Y') {
            $this->db->where('em.resign_date is not null');
        } else {
            $this->db->where('em.resign_date is null');
        }

        $this->db->order_by('employee_id');
        return $this->db->get()->result_array();
    }

    public function get_employee_by_ems_no($emp_no)
    {
        $this->db->select('em.id as employee_id, em.sensor_id, concat(em.firstname_th, \' \', em.lastname_th) as fullname_th, concat(em.firstname_en, \' \', em.lastname_en) as fullname_en');
        $this->db->select('em.ems_no');
        $this->db->select('emt.id as employee_type_id, emt.name as employee_type_name');
        $this->db->select('nt.id as nationality_id, nt.name as nationality_name');
        $this->db->select('cel.id as certificate_level_id, cel.name as certificate_level_name');
        $this->db->select('gd.id as gender_id, gd.name as gender_name');
        $this->db->select('mrs.id as marital_status_id, mrs.name as marital_status_name');
        $this->db->select('dp.id as department_id, dp.name as department_name');
        $this->db->select('jbp.id as job_position_id, jbp.name as job_position_name');
        $this->db->select('mn.id as manager_id, mn.name as manager_name');
        $this->db->select('sh.id as shift_id, sh.name as shift_name');
        $this->db->select(Model_utils::sql_last_update_date('em'));
        $this->db->select(Model_utils::sql_last_update_user('em'));
        $this->db->from($this->table_fg_employee . ' as em');
        $this->db->join($this->table_fg_employee_type . ' as emt', 'emt.code = em.type', 'left');
        $this->db->join($this->table_fg_nationality . ' as nt', 'nt.id = em.nationality', 'left');
        $this->db->join($this->table_fg_certificate_level . ' as cel', 'cel.id = em.certificate_level', 'left');
        $this->db->join($this->table_fg_gender . ' as gd', 'gd.id = em.gender', 'left');
        $this->db->join($this->table_fg_marital_status . ' as mrs', 'mrs.id = em.marital_status', 'left');
        $this->db->join($this->table_fg_department . ' as dp', 'dp.id = em.department_id', 'left');
        $this->db->join($this->table_fg_job_position . ' as jbp', 'jbp.id = em.job_position_id', 'left');
        $this->db->join($this->table_fg_manager . ' as mn', 'mn.id = em.manager_id', 'left');
        $this->db->join($this->table_fg_shift . ' as sh', 'sh.id = em.shift', 'left');
        $this->db->where('em.ems_no', $emp_no);

        $this->db->order_by('employee_id');
        return $this->db->get()->result_array();
    }

    public function insert_employee($data, $uid)
    {
        //$data['id'] = $this->generate_employee_id($data['firstname_en']);
        $data['ems_no'] = $this->generate_employee_no($data['id'], $data['type']);
        $this->db->set('create_uid', $uid);
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->insert($this->table_fg_employee, $data);
        return $this->db->affected_rows();
    }

    public function update_employee($id, $data, $uid)
    {
        $data['ems_no'] = $this->generate_employee_no($data['id'], $data['type']);
        $this->db->set('update_uid', $uid);
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->where('id', $id);
        $this->db->update($this->table_fg_employee, $data);
        return $this->db->affected_rows();
    }

    public function generate_employee_id($name)
    {
        $first_char = substr($name, 0, 1);
        $num = 1;
        $length = 3;

        $this->db->select('id');
        $this->db->from($this->table_fg_employee);
        $this->db->like('id', $first_char, 'after');
        $this->db->order_by('id', 'DESC');

        $result_arr = $this->db->get()->result_array();
        if (!empty($result_arr)) {
            $id = $result_arr[0]['id'];
            $num = intval(substr($id, 1, strlen($id)-1)) + 1; 
        }

        return $first_char . $this->numstr_fix_length($num, $length);
    }

    function generate_employee_no($id, $type)
    {
        $length = 4;
        return $type . $this->numstr_fix_length($id, $length);
    }

    private function numstr_fix_length($number, $length)
    {
        $result = '';
        for ($i = strlen($number); $i < $length; $i++) {
            $result .= '0';
        }
        return $result . $number;
    }

    public function has_employee($id = '')
    {
        $this->db->select('*');
        $this->db->from($this->table_fg_employee);
        $this->db->where('id', $id);

        return count($this->db->get()->result_array()) > 0;
    }
}
