<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once('Model_utils.php');

class Fg_shift_switch_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();

        $this->table_fg_employee = 'fg_employee';
        $this->table_fg_employee_type = 'fg_employee_type';
        $this->table_fg_nationality = 'fg_nationality';
        $this->table_fg_certificate_level = 'fg_certificate_level';
        $this->table_fg_gender = 'fg_gender';
        $this->table_fg_marital_status = 'fg_marital_status';
        $this->table_fg_department = 'fg_department';
        $this->table_fg_job_position = 'fg_job_position';
        $this->table_fg_manager = 'fg_manager';
        $this->table_fg_shift = 'fg_shift';
        $this->table_fg_shift_switch = 'fg_shift_switch';
    }

    public function get_shift_switches($id = '')
    {
        $this->db->select('sw.*');
        $this->db->select('TO_CHAR(sw.date, \'DD/MM/YYYY\') as fm_date');
        $this->db->select(Model_utils::sql_last_update_date('sw'));
        $this->db->select(Model_utils::sql_last_update_user('sw'));
        $this->db->from($this->table_fg_shift_switch . ' as sw');
        $this->db->order_by('sw.date desc');

        if (!empty($id)) {
            $this->db->where('id', $id);
        }

        return $this->db->get()->result_array();
    }

    public function get_shift_switches_checking()
    {
        $this->db->select('*');
        $this->db->select('TO_CHAR(date, \'DD/MM/YYYY\') as fm_date');
        $this->db->from($this->table_fg_shift_switch);
        $this->db->order_by('date asc');

        if (!empty($id)) {
            $this->db->where('id', $id);
        }

        return $this->db->get()->result_array();
    }

    public function insert_shift_switch($data, $uid)
    {
        $data['id'] = $this->common_model->gen_new_id($this->table_fg_shift_switch, 'id');
        $this->db->set('create_uid', $uid);
        $this->db->set('create_date', 'NOW()', FALSE);
        return $this->db->insert($this->table_fg_shift_switch, $data);
    }

    public function update_shift_switch($id, $data, $uid)
    {
        $this->db->where('id', $id);
        $this->db->set('update_uid', $uid);
        $this->db->set('update_date', 'NOW()', FALSE);
        return $this->db->update($this->table_fg_shift_switch, $data);
    }

}
