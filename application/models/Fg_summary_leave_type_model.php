<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once('Model_utils.php');

class Fg_summary_leave_type_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->table_fg_checkinout = 'fg_checkinout';
        $this->table_fg_employee = 'fg_employee';
        $this->table_fg_timesheet = 'fg_timesheet';
        $this->table_fg_shift = 'fg_shift';
        $this->table_fg_monthly_data = 'fg_monthly_data';
        $this->table_fg_department = 'fg_department';
        $this->table_fg_position = 'fg_position';
        $this->table_fg_leave = 'fg_leave';
        $this->table_fg_leave_type = 'fg_leave_type';
        $this->table_fg_holiday = 'fg_holiday';
        $this->table_fg_holiday_type = 'fg_holiday_type';
    }

    public function get_summary_data($start_date = NULL, $end_date = NULL, $department = NULL)
    {
        $slh = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and lv2.type = \'SL(H)\'';
        $sl = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and lv2.type = \'SL\'';
        $al = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and lv2.type = \'AL\'';
        $ab = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and lv2.type = \'AB\'';
        $co = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and lv2.type = \'CO\'';
        $asl = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and lv2.type = \'ASL\'';
        $dfl = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and lv2.type = \'DFL\'';
        $mrl = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and lv2.type = \'MRL\'';
        $ma = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and lv2.type = \'MA\'';
        $ml = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and lv2.type = \'ML\'';
        $rl = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and lv2.type = \'RL\'';
        $cl = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.id = lv.id and lv2.type = \'CL\'';

        $this->db->select('COALESCE(sum((' . $slh . ')), 0) as slh');
        $this->db->select('COALESCE(sum((' . $sl . ')), 0) as sl');
        $this->db->select('COALESCE(sum((' . $al . ')), 0) as al');
        $this->db->select('COALESCE(sum((' . $ab . ')), 0) as ab');
        $this->db->select('COALESCE(sum((' . $co . ')), 0) as co');
        $this->db->select('COALESCE(sum((' . $asl . ')), 0) as asl');
        $this->db->select('COALESCE(sum((' . $dfl . ')), 0) as dfl');
        $this->db->select('COALESCE(sum((' . $mrl . ')), 0) as mrl');
        $this->db->select('COALESCE(sum((' . $ma . ')), 0) as ma');
        $this->db->select('COALESCE(sum((' . $ml . ')), 0) as ml');
        $this->db->select('COALESCE(sum((' . $rl . ')), 0) as rl');
        $this->db->select('COALESCE(sum((' . $cl . ')), 0) as cl');
        $this->db->from($this->table_fg_leave . ' as lv');
        $this->db->join($this->table_fg_employee . ' as em', 'em.id = lv.employee_id', 'left');

        if (!empty($start_date)) {
            $this->db->where('(lv.from_date >= TO_DATE(\'' . $start_date . '\', \'DD/MM/YYYY\') or lv.to_date >= TO_DATE(\'' . $start_date . '\', \'DD/MM/YYYY\'))');
        }

        if (!empty($end_date)) {
            $this->db->where('(lv.from_date <= TO_DATE(\'' . $end_date . '\', \'DD/MM/YYYY\') or lv.to_date <= TO_DATE(\'' . $end_date . '\', \'DD/MM/YYYY\'))');
        }

        if (!empty($department)) {
            $this->db->where('em.department_id', $department);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

}