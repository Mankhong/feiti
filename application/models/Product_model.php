<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once('Model_utils.php');

class Product_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->table_products = 'products';
        $this->table_product_types = 'product_types';
        $this->table_invoice_types = 'invoice_types';
        $this->table_processes = 'processes';
        $this->table_unit_price_types = 'unit_price_types';
        $this->table_products_history = 'products_history';
        $this->table_customers = 'customers';
    }

    public function get_products($seq = '')
    {
        $this->db->select('*');
        $this->db->from($this->table_products);

        if (!empty($seq)) {
            $this->db->where('seq', $seq);
        }

        return $this->db->get()->result_array();
    }

    public function get_product_types($id = '')
    {
        $this->db->select('*');
        $this->db->from($this->table_product_types);

        if (!empty($id)) {
            $this->db->where('id', $id);
        }

        return $this->db->get()->result_array();
    }

    public function get_invoice_types($id = '', $group = '')
    {
        $this->db->select('*');
        $this->db->from($this->table_invoice_types);
        $this->db->where('used', 'Y');

        if (!empty($id)) {
            $this->db->where('id', $id);
        }

        if (!empty($group)) {
            $this->db->where('invoice_group', $group);
        }

        $this->db->order_by('id');
        return $this->db->get()->result_array();
    }

    public function get_processes($id = '')
    {
        $this->db->select('*');
        $this->db->from($this->table_processes);

        if (!empty($id)) {
            $this->db->where('id', $id);
        }

        return $this->db->get()->result_array();
    }

    public function get_unit_price_types($id = '')
    {
        $this->db->select('*');
        $this->db->from($this->table_unit_price_types);

        if (!empty($id)) {
            $this->db->where('id', $id);
        }

        return $this->db->get()->result_array();
    }

    public function get_joined_products($seq = '')
    {
        $this->db->select('pd.*');
        $this->db->select('pdt.id as product_type_id, pdt.name as product_type_name');
        $this->db->select('ivt.id as invoice_type_id, ivt.name as invoice_type_name');
        $this->db->select('pss.id as process_id, pss.name as process_name');
        $this->db->select('upt.id as unit_price_type_id, upt.name as unit_price_type_name');
        $this->db->select('cs.name as customer_name');
        $this->db->select(Model_utils::sql_last_update_date('pd'));
        $this->db->select(Model_utils::sql_last_update_user('pd'));
        $this->db->from($this->table_products . ' as pd');
        $this->db->join($this->table_customers . ' as cs', 'cs.id = pd.customer_id');
        $this->db->join($this->table_product_types . ' as pdt', 'pdt.id = pd.product_type', 'left');
        $this->db->join($this->table_invoice_types . ' as ivt', 'ivt.id = pd.invoice_type', 'left');
        $this->db->join($this->table_processes . ' as pss', 'pss.id = pd.process', 'left');
        $this->db->join($this->table_unit_price_types . ' as upt', 'upt.id = pd.unit_price_type', 'left');

        if (!empty($seq)) {
            if (isset($seq)) {
                $this->db->where_in('pd.seq', $seq);
            } else {
                $this->db->where('pd.seq', $seq);
            }
        }

        $this->db->order_by('pd.id');

        return $this->db->get()->result_array();
    }

    public function get_products_histories($seq = '')
    {
        $this->db->select('pdh.*, to_char(pdh.history_date, \'YYYY-MM-DD HH24:MI:SS\') as h_date');
        $this->db->select('pdt.id as product_type_id, pdt.name as product_type_name');
        $this->db->select('ivt.id as invoice_type_id, ivt.name as invoice_type_name');
        $this->db->select('pss.id as process_id, pss.name as process_name');
        $this->db->select('upt.id as unit_price_type_id, upt.name as unit_price_type_name');
        $this->db->from($this->table_products_history . ' as pdh');
        $this->db->join($this->table_product_types . ' as pdt', 'pdt.id = pdh.product_type', 'left');
        $this->db->join($this->table_invoice_types . ' as ivt', 'ivt.id = pdh.invoice_type', 'left');
        $this->db->join($this->table_processes . ' as pss', 'pss.id = pdh.process', 'left');
        $this->db->join($this->table_unit_price_types . ' as upt', 'upt.id = pdh.unit_price_type', 'left');

        if (!empty($seq)) {
            $this->db->where('pdh.seq', $seq);
        }

        $this->db->order_by('pdh.seq');

        return $this->db->get()->result_array();
    }

    public function has_product($seq = '')
    {
        $this->db->select('*');
        $this->db->from($this->table_products);
        $this->db->where('seq', $id);

        return count($this->db->get()->result_array()) > 0;
    }

    public function insert_product($data, $uid)
    {
        //default value.
        if (empty($data['unit_price'])) {
            $data['unit_price'] = 0;
        }

        if (empty($data['lot_size'])) {
            $data['lot_size'] = NULL;
        }
        
        if (empty($data['domestic_price'])) {
            $data['domestic_price'] = 0;
        }

        if (empty($data['export_price'])) {
            $data['export_price'] = 0;
        }

        $this->db->select('COALESCE(MAX(seq),0) + 1 as seq');
        $this->db->from($this->table_products);
        $seq = $this->db->get()->result_array()[0]['seq'];

        $this->db->set('seq', $seq);
        $this->db->set('create_uid', $uid);
        $this->db->set('create_date', 'NOW()', FALSE);
        $this->db->insert($this->table_products, $data);
        return $this->db->affected_rows();
    }

    public function update_product($seq, $data, $uid)
    {
        //default value.
        if (empty($data['unit_price'])) {
            $data['unit_price'] = 0;
        }

        if (empty($data['lot_size'])) {
            $data['lot_size'] = NULL;
        }

        if (empty($data['domestic_price'])) {
            $data['domestic_price'] = 0;
        }

        if (empty($data['export_price'])) {
            $data['export_price'] = 0;
        }

        $this->db->flush_cache();
        $this->db->where('seq', $seq);
        $this->db->set('update_uid', $uid);
        $this->db->set('update_date', 'NOW()', FALSE);
        $this->db->update($this->table_products, $data);
        return $this->db->affected_rows();
    }

    public function insert_product_history($seq)
    {
        $this->db->query('insert into products_history as pdh select pd.*, now() from products as pd where pd.seq = \'' . $this->db->escape_str($seq) . '\'');
    }

}
