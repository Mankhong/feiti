<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once('Model_utils.php');

class Fg_time_summary_model extends CI_Model
{
    public function __construct()
    {
        $this->table_fg_checkinout = 'fg_checkinout';
        $this->table_fg_employee = 'fg_employee';
        $this->table_fg_timesheet = 'fg_timesheet';
        $this->table_fg_shift = 'fg_shift';
        $this->table_fg_monthly_data = 'fg_monthly_data';
        $this->table_fg_department = 'fg_department';
        $this->table_fg_position = 'fg_position';
        $this->table_fg_leave = 'fg_leave';
        $this->table_fg_leave_type = 'fg_leave_type';
    }

    public function get_summary_timesheet($year, $month, $department, $employee_type, $employee_no)
    {
        //$diligence_case = 'select count(1) from fg_monthly_data mnd2 where mnd2.ems_no = em.ems_no and CAST(mnd2.month as integer) = CAST(ts.month as integer) and mnd2.year = ts.year LIMIT 1';
        $diligence_sql = 'select mnd.diligence from fg_monthly_data mnd where mnd.ems_no = em.ems_no and CAST(mnd.month as integer) = CAST(ts.month as integer) and mnd.year = ts.year LIMIT 1';
        $prev_diligence_sql = 'select mnd.diligence from fg_monthly_data mnd where mnd.ems_no = em.ems_no 
            and CAST(mnd.month as integer) = (case when cast(ts.month as integer) = 1 then \'12\' else (cast(ts.month as integer) - 1)  end) 
            and mnd.year = (case when cast(ts.month as integer) = 1 then ts.year - 1 else ts.year end) LIMIT 1';
        
        $slh = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.employee_id = em.id and lv2.type = \'SL(H)\' and ts.date >= lv2.from_date and ts.date <= lv2.to_date';
        $sl = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.employee_id = em.id and lv2.type = \'SL\' and ts.date >= lv2.from_date and ts.date <= lv2.to_date';
        $al = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.employee_id = em.id and lv2.type = \'AL\' and ts.date >= lv2.from_date and ts.date <= lv2.to_date';
        $ab = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.employee_id = em.id and lv2.type = \'AB\' and ts.date >= lv2.from_date and ts.date <= lv2.to_date';
        $co = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.employee_id = em.id and lv2.type = \'CO\' and ts.date >= lv2.from_date and ts.date <= lv2.to_date';
        $asl = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.employee_id = em.id and lv2.type = \'ASL\' and ts.date >= lv2.from_date and ts.date <= lv2.to_date';
        $dfl = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.employee_id = em.id and lv2.type = \'DFL\' and ts.date >= lv2.from_date and ts.date <= lv2.to_date';
        $mrl = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.employee_id = em.id and lv2.type = \'MRL\' and ts.date >= lv2.from_date and ts.date <= lv2.to_date';
        $ma = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.employee_id = em.id and lv2.type = \'MA\' and ts.date >= lv2.from_date and ts.date <= lv2.to_date';
        $ml = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.employee_id = em.id and lv2.type = \'ML\' and ts.date >= lv2.from_date and ts.date <= lv2.to_date';
        $rl = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.employee_id = em.id and lv2.type = \'RL\' and ts.date >= lv2.from_date and ts.date <= lv2.to_date';
        $cl = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.employee_id = em.id and lv2.type = \'CL\' and ts.date >= lv2.from_date and ts.date <= lv2.to_date';
        $clh = 'select sum(case when lv2.duration = 1 then 1 else 0.5 end) from ' . $this->table_fg_leave . ' as lv2 where lv2.employee_id = em.id and lv2.type = \'CL(H)\' and ts.date >= lv2.from_date and ts.date <= lv2.to_date';

        $working_day100_sql = 'select CASE WHEN em.type = \'A\' THEN 30 ELSE (select count(1) from fg_timesheet ts2 where ts2.sensor_user_id = ts.sensor_user_id and ts2.year = ts.year and ts2.month = ts.month and ts2.holiday_type is null) END';

        $this->db->select('sum(ts.ot1) as sum_ot1, sum(ts.ot15) as sum_ot15, sum(ts.ot2) as sum_ot2, sum(ts.ot3) as sum_ot3');
        $this->db->select('sum(CAST(ts.night_shift as integer)) as sum_night_shift, sum(CAST(ts.food_travel_day as integer)) as sum_food_travel_day');
        $this->db->select('em.ems_no, em.type as employee_type, concat(em.firstname_th, \' \', em.lastname_th) as fullname_th, TO_CHAR(em.startdate, \'DD/MM/YYYY\') as entry_date');
        $this->db->select('ps.id as position_id, ps.name as position_name');
        $this->db->select('dp.id as department_id, dp.name as department_name');
        $this->db->select('sum((case when COALESCE(ts.holiday_type, 0) = 3 then 1 else 0 end)) as pay_holiday');
        //$this->db->select('(' .  $working_day100_sql . ') as working_day100');
        //$this->db->select('(case when (' . $diligence_case . ') = 1 then (' . $diligence_sql . ') else \'0\' end) as diligence');
        //$this->db->select('(' . $diligence_sql . ') as diligence');
        $this->db->select('sum((' . $slh . ')) as slh');
        $this->db->select('sum((' . $sl . ')) as sl');
        $this->db->select('sum((' . $al . ')) as al');
        $this->db->select('sum((' . $ab . ')) as ab');
        $this->db->select('sum((' . $co . ')) as co');
        $this->db->select('sum((' . $asl . ')) as asl');
        $this->db->select('sum((' . $dfl . ')) as dfl');
        $this->db->select('sum((' . $mrl . ')) as mrl');
        $this->db->select('sum((' . $ma . ')) as ma');
        $this->db->select('sum((' . $ml . ')) as ml');
        $this->db->select('sum((' . $rl . ')) as rl');
        $this->db->select('sum((' . $cl . ')) as cl');
        $this->db->select('sum((' . $clh . ')) as clh');

        //$this->db->select('(' . $diligence_sql . ') as diligence_before');
        $this->db->from($this->table_fg_timesheet . ' as ts');
        $this->db->join($this->table_fg_employee . ' as em', 'em.id = ts.sensor_user_id');
        $this->db->join($this->table_fg_department . ' as dp', 'dp.id = em.department_id');
        $this->db->join($this->table_fg_position . ' as ps', 'ps.id = em.job_position_id');
        $this->db->group_by('em.ems_no, em.department_id, dp.id, dp.name, fullname_th, entry_date, position_id, position_name, diligence, ts.sensor_user_id, em.type');

        if (!empty($year)) {
            $this->db->where('ts.year = ' . $year);
        }

        if (!empty($month)) {
            $this->db->select('(' .  $working_day100_sql . ') as working_day100');
            $this->db->select('(' . $diligence_sql . ') as diligence');
            $this->db->select('(' . $prev_diligence_sql . ') as prev_diligence');
            $this->db->where('CAST(ts.month as integer) = ' . $month);
            $this->db->group_by('ts.year, ts.month');
            $this->db->where('(em.resign_date is null or (CAST(TO_CHAR(em.resign_date, \'YYYY\') as integer) >= ts.year and CAST(TO_CHAR(em.resign_date, \'MM\') as integer) > CAST(ts.month as integer)))');
        } else {
            $this->db->select('\'\' as working_day100');
            $this->db->select('\'\' as diligence');
            $this->db->select('\'\' as prev_diligence');
        }

        if (!empty($employee_no)) {
            $this->db->where('em.ems_no', $employee_no);
        }

        if (!empty($employee_type)) {
            $this->db->where('em.type', $employee_type);
        }

        if (!empty($department)) {
            $this->db->where('em.department_id', $department);
        }

        $query = $this->db->get();

        //echo $this->db->last_query();

        return $query->result_array();
    }

    private function get_working_day100($year, $month, $employee_type)
    {

        if ($employee_type == 'A') {
            //** Waiting Leave day */
            return 30;

        } else if ($employee_type == 'B') {
            $days_in_month = Model_utils::get_days_in_month($year, $month);
            $result_days = 0;

            $month = Model_utils::two_digits($month);
            for ($i = 0; $i < $days_in_month; $i++) {
                $date = Model_utils::two_digits($i + 1) . '/' . $month . '/' .$year;
                $holiday = $this->get_holiday($date);

                //*** Waiting holiday type */
                if ($holiday == NULL) {
                    $result_days++;
                }

                //** Waiting Leave day */
                //- Leave Day
            }

            return $result_days;
        }

        return NULL;
    }
}