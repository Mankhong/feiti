<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once('Model_utils.php');

class Billing_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
        $this->table_billings = 'billings';
        $this->table_billing_invoices = 'billing_invoices';
        $this->table_invoices = 'invoices';
        $this->table_invoice_products = 'invoice_products';
        $this->table_invoice_product_lots = 'invoice_product_lots';
        $this->table_customers = 'customers';
    }

    public function get_billings($id = '')
    {
        $this->db->select('bl.id, bl.receipt_id, bl.customer_id');
        $this->db->select('to_char(bl.receipt_date, \'DD/MM/YYYY\') as receipt_date, to_char(bl.billing_date, \'DD/MM/YYYY\') as billing_date');
        $this->db->from($this->table_billings . ' as bl');

        if (!empty($id)) {
            $this->db->where('id', $id);
        }

        return $this->db->get()->result_array();
    }

    public function get_billing_invoices($billing_id)
    {
        $amount_sql_novat = 'COALESCE((select sum(
            round((op2.unit_price *
            (select sum(ivpl3.quantity) from invoice_product_lots ivpl3 where ivpl3.invoice_products_id = ivp2.id)), 2))
            from invoice_products ivp2
            join order_products op2 on op2.product_seq = ivp2.product_seq and op2.order_id = ivp2.order_id
            where ivp2.invoice_id = iv.id),0)';

        $amount_sql = 'COALESCE((select sum(
                round((op2.unit_price * (select sum(ivpl3.quantity) from invoice_product_lots ivpl3 where ivpl3.invoice_products_id = ivp2.id)), 2)
                )
                +
                round((sum(
                round((op2.unit_price * (select sum(ivpl3.quantity) from invoice_product_lots ivpl3 where ivpl3.invoice_products_id = ivp2.id)), 2)
                ) * 0.07) ,2)
                from invoice_products ivp2
                join order_products op2 on op2.product_seq = ivp2.product_seq and op2.order_id = ivp2.order_id
                where ivp2.invoice_id = iv.id),0)';

        $total_sql = 'COALESCE((select sum((select round(sum(round((ivpl3.quantity * op3.unit_price), 2) + round((ivpl3.quantity * op3.unit_price), 2) * 0.07), 2)
                from invoice_products ivp3
                join invoice_product_lots ivpl3 on ivpl3.invoice_products_id = ivp3.id
                join order_products op3 on op3.product_seq = ivp3.product_seq and op3.order_id = ivp3.order_id
                where ivp3.invoice_id = iv2.id))
        from billing_invoices blv2
        join invoices iv2 on iv2.id = blv2.invoice_id
        where blv2.billing_id = bl.id),0)';

        $vat_sql = 'select cast(cfg.config_value as float) from configs as cfg where cfg.config_key = \'VAT\'';

        $this->db->select('bl.receipt_id, blv.id, blv.billing_id, blv.invoice_id, blv.invoice_id as print_invoice_id');
        $this->db->select($amount_sql . ' as total_amount');
        $this->db->select($amount_sql_novat . ' as total_amount_novat');
        //$this->db->select($total_sql . ' as grand_total');
        //$this->db->select('(' . $amount_sql . ') + (((' . $vat_sql . ')/100) * (' . $amount_sql . ')) as total_amount');
        $this->db->select('to_char(iv.date, \'DD/MM/YYYY\') as invoice_date');
        $this->db->from($this->table_billing_invoices . ' as blv');
        $this->db->join($this->table_invoices . ' as iv', 'iv.id = blv.invoice_id');
        $this->db->join($this->table_billings . ' as bl', 'bl.id = blv.billing_id');
        $this->db->join($this->table_customers . ' as cs', 'cs.id = bl.customer_id');
        $this->db->where('billing_id', $billing_id);
        $this->db->order_by('invoice_id');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_joined_billings($id = '', $startDate = '', $dateTimeEnd = '', $billing_id_search = '', $customer_name = '')
    {
        $amount_sql = 'COALESCE((select sum(round((ivpl2.quantity * op2.unit_price), 2))
            from billing_invoices blv2
            join invoices iv2 on iv2.id = blv2.invoice_id
            join invoice_products ivp2 on ivp2.invoice_id = iv2.id
            join invoice_product_lots ivpl2 on ivpl2.invoice_products_id = ivp2.id
            join order_products op2 on op2.product_seq = ivp2.product_seq and op2.order_id = ivp2.order_id
            where blv2.billing_id = bl.id),0)';

        $total_sql = 'COALESCE((select sum((select round(sum(round((ivpl3.quantity * op3.unit_price), 2) + round((round((ivpl3.quantity * op3.unit_price), 2) * 0.07),2)), 2)
                from invoice_products ivp3
                join invoice_product_lots ivpl3 on ivpl3.invoice_products_id = ivp3.id
                join order_products op3 on op3.product_seq = ivp3.product_seq and op3.order_id = ivp3.order_id
                where ivp3.invoice_id = iv2.id))
        from billing_invoices blv2
        join invoices iv2 on iv2.id = blv2.invoice_id
        where blv2.billing_id = bl.id),0)';

        $vat_sql = 'select cast(cfg.config_value as float) from configs as cfg where cfg.config_key = \'VAT\'';

        $this->db->select('bl.id as billing_id, bl.receipt_id, to_char(bl.receipt_date, \'DD/MM/YYYY\') as receipt_date, to_char(bl.billing_date, \'DD/MM/YYYY\') as billing_date');
        $this->db->select('cs.id as customer_id, cs.name as customer_name, cs.image_path as customer_image_path');
        $this->db->select('(' . $amount_sql . ') as amount');
        $this->db->select('(' . $total_sql . ') as total');
        $this->db->select('(' . $vat_sql . ') as vat');
        $this->db->select(Model_utils::sql_last_update_date('bl'));
        $this->db->select(Model_utils::sql_last_update_user('bl'));
        $this->db->from($this->table_billings . ' as bl');
        $this->db->join($this->table_customers . ' as cs', 'cs.id = bl.customer_id');

        if (!empty($billing_id_search)) {
            $this->db->like('bl.id', $billing_id_search);
        }
        
        if (!empty($customer_name)) {
            $this->db->like('cs.name', $customer_name);
        }
        
        if (!empty($startDate)) {
            $this->db->where("bl.billing_date >= to_date('" . $startDate . "', 'DD/MM/YYYY')");
            
        }
        if (!empty($dateTimeEnd)) {
            $this->db->where("bl.billing_date <= to_date('" . $dateTimeEnd . "', 'DD/MM/YYYY')");
        }
        
        return $this->db->get()->result_array();
    }

    public function get_picker_invoices($customer_id)
    {
        $count_invoice_sql = '(select count(1) from billing_invoices blv2 where blv2.invoice_id = iv.id) = 0';
        $vat_sql = 'select cast(cfg.config_value as float) from configs as cfg where cfg.config_key = \'VAT\'';
        $amount_sql = 'COALESCE((select sum(
                ((
                round((op2.unit_price *
                (select sum(ivpl3.quantity) from invoice_product_lots ivpl3 where ivpl3.invoice_products_id = ivp2.id)), 2)
                )
                +
                (
                round((op2.unit_price *
                (select sum(ivpl3.quantity) from invoice_product_lots ivpl3 where ivpl3.invoice_products_id = ivp2.id)), 2)
                ) * 0.07))
                from invoice_products ivp2
                join order_products op2 on op2.product_seq = ivp2.product_seq and op2.order_id = ivp2.order_id
                where ivp2.invoice_id = iv.id),0)';

        $this->db->select('iv.id as invoice_id, iv.date as invoice_date, iv.id as print_invoice_id');
        $this->db->select('to_char(iv.date, \'DD/MM/YYYY\') as invoice_date');
        $this->db->select('(' . $amount_sql . ') as amount');
        //$this->db->select('(' . $vat_sql . ') as vat');
        $this->db->from($this->table_invoices . ' as iv');
        $this->db->join($this->table_customers . ' as cs', 'cs.id = iv.customer_id');
        //$this->db->where('iv.status', 1);
        $this->db->where('cs.id', $customer_id);
        //$this->db->where($count_invoice_sql);
        return $this->db->get()->result_array();
    }

    public function has_billing($id)
    {
        $this->db->select('*');
        $this->db->from($this->table_billings);
        $this->db->where('id', $id);

        return count($this->db->get()->result_array()) > 0;
    }

    public function has_receipt($receipt_id)
    {
        $this->db->select('*');
        $this->db->from($this->table_billings);
        $this->db->where('receipt_id', $receipt_id);

        return count($this->db->get()->result_array()) > 0;
    }

    public function insert_billing($data, $invoice_arr, $uid)
    {
        $this->db->set('create_uid', $uid);
        $this->db->set('create_date', 'NOW()', FALSE);
        $affected_rows = $this->db->insert($this->table_billings, $data);

        if ($affected_rows > 0) {
            $affected_rows += $this->insert_billing_invoices($data['id'], $invoice_arr, $uid);
        }
        return $affected_rows;
    }

    public function clear_billing_invoices($billing_id)
    {
        $this->db->where('billing_id', $billing_id);
        return $this->db->delete($this->table_billing_invoices);
    }

    public function insert_billing_invoices($billing_id, $invoice_arr, $uid)
    {
        $affected_rows = 0;
        if (!empty($invoice_arr)) {
            for ($i = 0; $i < count($invoice_arr); $i++) {
                $this->db->select('COALESCE(MAX(id),0) + 1 as id');
                $this->db->from($this->table_billing_invoices);
                $id = $this->db->get()->result_array()[0]['id'];

                $this->db->set('id', $id);
                $this->db->set('billing_id', $billing_id);
                $this->db->set('create_uid', $uid);
                $this->db->set('create_date', 'NOW()', FALSE);
                $affected_rows += $this->db->insert($this->table_billing_invoices, $invoice_arr[$i]);
            }
        }
        return $affected_rows;
    }

    public function delete_billing_invoices($invoice_arr, $uid)
    {
        $affected_rows = 0;
        if (!empty($invoice_arr)) {
            for ($i = 0; $i < count($invoice_arr); $i++) {
                $this->db->where('id', $invoice_arr[$i]['id']);
                $affected_rows += $this->db->delete($this->table_billing_invoices, $invoice_arr[$i]);
            }
        }
        return $affected_rows;
    }

    public function update_billing($id, $data, $insert_invoice_arr, $delete_invoice_arr, $uid)
    {
        $affected_rows = 0;
        $this->db->where('id', $id);
        $this->db->set('update_uid', $uid);
        $this->db->set('update_date', 'NOW()', FALSE);
        $affected_rows = $this->db->update($this->table_billings, $data);

        $affected_rows += $this->delete_billing_invoices($delete_invoice_arr, $uid);
        $affected_rows += $this->insert_billing_invoices($id, $insert_invoice_arr, $uid);
        return $affected_rows;
    }
}