<?php echo!empty($message) ? $message : ''; ?>
<?php echo!empty($errorMessage) ? $errorMessage : ''; ?>
<div class="container-fluid">
    <input type="hidden" id="flag_search" />
    <div class="row">
        <div class="col-sm-12 criteria-container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="label label-width-1">
                        Start Date :
                    </div>
                    <div class="input">
                        <input id="dateTimeStart" name="dateTimeStart" maxlength="100" type="text" class="form-control input-lg date-input" placeholder="dd/mm/yyyy" readonly/>
                    </div>
                    <div class="label label-width-4 pl-2">
                        End Date :
                    </div>
                    <div class="input">    
                        <input id="dateTimeEnd" name="dateTimeEnd" maxlength="100" type="text" class="form-control input-lg date-input" placeholder="dd/mm/yyyy" readonly/>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="label label-width-1">
                        Invoice ID :
                    </div>
                    <div class="input">
                        <input id="invoice_id" maxlength="100" type="text" class="form-control input-lg date-input" />
                    </div>

                    <div class="label label-width-4 pl-2">
                        Customer Name :
                    </div>
                    <div class="input">
                        <input maxlength="100" id="customer_name" type="text" class="form-control input-lg date-input" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row pt-0">
        <div class="col-md-12 text-left" style="padding-left: 140px;"> 
            <button type="button" id="btn_search" class="btn btn-primary-pz btn-sm"><i class="fas fa-search"></i> Search</button>
            <button type="button" id="btn_clear" class="btn btn-default btn-sm">Clear</button>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12" style="padding-top: 8px; display: none;" id="rs_div">
            <div class="view-mode-1">
                <div class="text-right px-3 pt-0">
                    <span id="export_buttons_container">
                        <?php echo $export_excel; ?>
                    </span>
                    <a class="btn btn-default btn-sm view-mode-1-button d-none"><i class="fas fa-list"></i></a>
                    <a class="btn btn-default btn-sm view-mode-2-button d-none"><i class="fas fa-th"></i></a>
                </div>

                <table id="rstable" class="table table-sm table-bordered table-striped" style="width: 100%;">
                    <thead>
                        <tr>
                            <th width="7%" class="text-center">No.</th>
                            <th width="16%" class="text-center">Invoice ID</th>
                            <th width="20%" class="text-center">Customer</th>
                            <th width="10%" class="text-center">Invoice Date</th>
                            <th width="10%" class="text-center">Amount</th>
                            <th width="10%" class="text-center">Total</th>
                            <th width="10%" class="text-center">Last Update Date</th>
                            <th width="10%" class="text-center">Last Update User</th>
                            <th width="7%" class="text-center">Edit</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div class="view-mode-2">
        <div id="dataitems" class="mt-2"></div>
    </div>
</div>