<div class="content-panel form-panel">
	<?php echo !empty($message)? $message : ''; ?>
	<?php echo !empty($errorMessage)? $errorMessage : ''; ?>

	<h3 class="content-panel-title">Employee</h3>

	<br>

	<?php echo form_open_multipart($submit_url, array('id' => 'form')); ?>

	<!-- === EMPLOYEE DETAIL === -->
	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<div id="image_upload_group">
				<img id="image_img" src="<?php echo empty($image_path) ? base_url('assets/images/default_image.jpg'): base_url($image_path); ?>"
				 style="width: 100px; height: 100px; display: inline-block" onclick="showImage()" />
				<button type="button" id="image_upload_button" class="btn btn-default btn-sm ml-1" style="display: inline-block"
				 onclick="selectImage()"><i class="fas fa-camera"></i></button>
				<input type="file" id="image_upload" name="image_upload" onchange="displayImage(this)" style="display: none;" />
			</div>
		</div>
		<div class="col-sm-12 col-md-4 pt-0">
			<div class="pl-0">
				<i class="fas fa-grip-vertical"></i>
				<?php echo lang('employee_no', 'employee_no');?>
			</div>
			<div>
				<?php echo form_input($employee_no); ?>
			</div>
		</div>
		<div class="col-sm-12 col-md-6">
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('employee_id', 'employee_id');?> <font color="red">*</font> 
		</div>
		<div class="col-sm-12 col-md-4 pt-1">
			<?php echo form_input($id); ?>
			<span class="text-danger" id="employee_id_err"><?php echo form_error('employee_id'); ?></span>
			<input type="hidden" id="temp_employee_id" name="temp_employee_id" value="<?php echo empty($temp_employee_id) ? '' : $temp_employee_id; ?>"/>
		</div>

		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('employee_type', 'employee_type');?>
		</div>
		<div class="col-sm-12 col-md-4 pt-1">
		<?php echo form_dropdown('type', $type_dropdown_data, $type_dropdown_default, ['class' => 'form-control']); ?>
			<span class="text-danger">
				<?php echo form_error('type'); ?></span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('title', 'title');?>
		</div>
		<div class="col-sm-12 col-md-4 pt-1">
			<?php echo form_dropdown('title_id', $title_id_dropdown_data, $title_id_dropdown_default, ['class' => 'form-control']); ?>
			<span class="text-danger">
				<?php echo form_error('title_id'); ?></span>
		</div>

		<div class="col-sm-12 col-md-2 border-right">
		</div>
		<div class="col-sm-12 col-md-4 pt-1">
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('firstname_th', 'firstname_th');?> <font color="red">*</font> 
		</div>
		<div class="col-sm-12 col-md-4 pt-1">
			<?php echo form_input($firstname_th); ?>
			<span class="text-danger">
				<?php echo form_error('firstname_th'); ?></span>
		</div>

		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('lastname_th', 'lastname_th');?> <font color="red">*</font> 
		</div>
		<div class="col-sm-12 col-md-4 pt-1">
			<?php echo form_input($lastname_th); ?>
			<span class="text-danger">
				<?php echo form_error('lastname_th'); ?></span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('firstname_en', 'firstname_en');?>
		</div>
		<div class="col-sm-12 col-md-4 pt-1">
			<?php echo form_input($firstname_en); ?>
			<span class="text-danger">
				<?php echo form_error('firstname_en'); ?></span>
		</div>

		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('lastname_en', 'lastname_en');?>
		</div>
		<div class="col-sm-12 col-md-4 pt-1">
			<?php echo form_input($lastname_en); ?>
			<span class="text-danger">
				<?php echo form_error('lastname_en'); ?></span>
		</div>
	</div>
	<!-- === END EMPLOYEE DETAIL === -->

	<br><br>
	<!-- === EMPLOYEE PRIVATE INFORMATION === -->
	<ul class="nav nav-tabs">
		<li class="nav-item">
			<a class="nav-link active" id="privateInformationTab" data-toggle="tab" href="#privateInformationTabContent" role="tab"
			 aria-controls="privateInformationTabContent" aria-selected="true">
			 <i class="far fa-address-card"></i> Private Information</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="workInformationTab" data-toggle="tab" href="#workInformationTabContent" role="tab"
			 aria-controls="workInformationTabContent" aria-selected="false">
			 <i class="far fa-building"></i> Work Information</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="departmentHistoryTab" data-toggle="tab" href="#departmentHistoryTabContent" role="tab"
			 aria-controls="departmentHistoryTabContent" aria-selected="false">
			<i class="fas fa-list-alt"></i> Department History</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="shiftHistoryTab" data-toggle="tab" href="#shiftHistoryTabContent" role="tab"
			 aria-controls="shiftHistoryTabContent" aria-selected="false">
			 <i class="fas fa-clipboard-list"></i> Shift History</a>
		</li>
	</ul>

	<br>

	<div class="tab-content">
		<!-- PRIVATE INFORMATION TAB -->
		<div class="tab-pane active" id="privateInformationTabContent" role="tabpanel" aria-labelledby="privateInformationTab">

			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('nationality', 'nationality');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_dropdown('nationality', $nationality_dropdown_data, $nationality_dropdown_default, ['class' => 'form-control']); ?>
					<span class="text-danger">
						<?php echo form_error('nationality'); ?></span>
				</div>
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('pid', 'pid');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($pid); ?>
					<span class="text-danger">
						<?php echo form_error('pid'); ?></span>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('tax_no', 'tax_no');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($tax_no); ?>
					<span class="text-danger">
						<?php echo form_error('tax_no'); ?></span>
				</div>
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('bank_account_no', 'bank_account_no');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($bank_account_no); ?>
					<span class="text-danger">
						<?php echo form_error('bank_account_no'); ?></span>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('certificate_level', 'certificate_level');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_dropdown('certificate_level', $certificate_level_dropdown_data, $certificate_level_dropdown_default, ['class' => 'form-control']); ?>
					<span class="text-danger">
						<?php echo form_error('certificate_level'); ?></span>
				</div>
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('study_field', 'study_field');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($study_field); ?>
					<span class="text-danger">
						<?php echo form_error('study_field'); ?></span>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('school', 'school');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($school); ?>
					<span class="text-danger">
						<?php echo form_error('school'); ?></span>
				</div>

				<div class="col-sm-12 col-md-2 border-right">
				</div>
				<div class="col-sm-12 col-md-4">
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('house_no', 'house_no');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($house_no); ?>
					<span class="text-danger">
						<?php echo form_error('house_no'); ?></span>
				</div>
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('address', 'address');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($address); ?>
					<span class="text-danger">
						<?php echo form_error('address'); ?></span>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('telephone', 'telephone');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($telephone); ?>
					<span class="text-danger">
						<?php echo form_error('telephone'); ?></span>
				</div>
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('line', 'line');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($line); ?>
					<span class="text-danger">
						<?php echo form_error('line'); ?></span>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('birthdate', 'birthdate');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($birthdate); ?>
					<span class="text-danger">
						<?php echo form_error('birthdate'); ?></span>
				</div>
				<div class="col-sm-12 col-md-2 border-right">
				</div>
				<div class="col-sm-12 col-md-4">
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('gender', 'gender');?>
				</div>
				<div class="col-sm-12 col-md-4 pt-2">
					<?php echo form_radio($gender_value_1); ?>
					<?php echo $gender_label_1; ?>
					<?php echo form_radio($gender_value_2); ?>
					<?php echo $gender_label_2; ?>
					<span class="text-danger">
						<?php echo form_error('gender'); ?></span>
				</div>
				<div class="col-sm-12 col-md-2 border-right">
				</div>
				<div class="col-sm-12 col-md-4">
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('marital_status', 'marital_status');?>
				</div>
				<div class="col-sm-12 col-md-4">
				<?php echo form_dropdown('marital_status', $marital_status_dropdown_data, $marital_status_dropdown_default, ['class' => 'form-control']); ?>
					<span class="text-danger">
						<?php echo form_error('marital_status'); ?></span>
				</div>

				<div class="col-sm-12 col-md-2 border-right">
				</div>
				<div class="col-sm-12 col-md-4 pt-1">
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('ems_contact', 'ems_contact');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($ems_contact); ?>
					<span class="text-danger">
						<?php echo form_error('ems_contact'); ?></span>
				</div>
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('ems_phone', 'ems_phone');?>
				</div>
				<div class="col-sm-12 col-md-4">
				<?php echo form_input($ems_phone); ?>
					<span class="text-danger">
						<?php echo form_error('ems_phone'); ?></span>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('ems_relation', 'ems_relation');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($ems_relation); ?>
					<span class="text-danger">
						<?php echo form_error('ems_relation'); ?></span>
				</div>
				<div class="col-sm-12 col-md-2 border-right">
				</div>
				<div class="col-sm-12 col-md-4">
				</div>
			</div>

		</div>
		<!-- END PRIVATE INFORMATION TAB -->

		<!-- WORK INFORMATION TAB -->
		<div class="tab-pane" id="workInformationTabContent" role="tabpanel" aria-labelledby="workInformationTab">
			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('department', 'department');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_dropdown('department_id', $department_id_dropdown_data, $department_id_dropdown_default, ['class' => 'form-control']); ?>
					<span class="text-danger">
						<?php echo form_error('department_id'); ?></span>
				</div>
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('job_position', 'job_position');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_dropdown('job_position_id', $job_position_id_dropdown_data, $job_position_id_dropdown_default, ['class' => 'form-control']); ?>
					<span class="text-danger">
						<?php echo form_error('job_position_id'); ?></span>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('manager', 'manager');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_dropdown('manager_id', $manager_id_dropdown_data, $manager_id_dropdown_default, ['class' => 'form-control']); ?>
					<span class="text-danger">
						<?php echo form_error('manager_id'); ?></span>
				</div>
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('shift', 'shift');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_dropdown('shift', $shift_dropdown_data, $shift_dropdown_default, ['class' => 'form-control']); ?>
					<span class="text-danger">
						<?php echo form_error('shift'); ?></span>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('startdate', 'startdate');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($startdate); ?>
					<span class="text-danger">
						<?php echo form_error('startdate'); ?></span>
				</div>
				<div class="col-sm-12 col-md-2 border-right">
				</div>
				<div class="col-sm-12 col-md-4">
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('work_address', 'work_address');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($work_address); ?>
					<span class="text-danger">
						<?php echo form_error('work_address'); ?></span>
				</div>
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('work_location', 'work_location');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($work_location); ?>
					<span class="text-danger">
						<?php echo form_error('work_location'); ?></span>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('work_email', 'work_email');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($work_email); ?>
					<span class="text-danger">
						<?php echo form_error('work_email'); ?></span>
				</div>
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('work_phone', 'work_phone');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($work_phone); ?>
					<span class="text-danger">
						<?php echo form_error('work_phone'); ?></span>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('resign_date', 'resign_date');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($resign_date); ?>
					<span class="text-danger">
						<?php echo form_error('resign_date'); ?></span>
				</div>
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('resign_layoff', 'resign_layoff');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($resign_layoff); ?>
					<span class="text-danger">
						<?php echo form_error('resign_layoff'); ?></span>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('resign_reason', 'resign_reason');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($resign_reason); ?>
					<span class="text-danger">
						<?php echo form_error('resign_reason'); ?></span>
				</div>
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('resign_probation_date', 'resign_probation_date');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($resign_probation_date); ?>
					<span class="text-danger">
						<?php echo form_error('resign_probation_date'); ?></span>
				</div>
			</div>
		</div>
		<!-- END WORK INFORMATION TAB -->

		<!-- DEPARTMENT HISTORY TAB -->
		<div class="tab-pane text-center" id="departmentHistoryTabContent" role="tabpanel" aria-labelledby="departmentHistoryTab">
			Commint Soon...
		</div>
		<!-- END DEPARTMENT HISTORY TAB -->

		<!-- SHIFT HISTORY TAB -->
		<div class="tab-pane text-center" id="shiftHistoryTabContent" role="tabpanel" aria-labelledby="shiftHistoryTab">
			Commint Soon...
		</div>
		<!-- END SHIFT HISTORY TAB -->
	</div>

	

	<!-- === END EMPLOYEE CONTACT === -->

	<?php echo form_close(); ?>

	<br><br>

</div>

<!-- === CONTACT MODAL === -->
<div id="contactModal" class="modal contact-modal form-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					<strong>
						<h5>Create Contact</h5>
					</strong>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<!-- === EMPLOYEE DETAIL === -->
				<div class="row">
					<div class="col-sm-12 col-md-2 border-right">
						<?php echo lang('mobile', 'mobile');?>
					</div>
					<div class="col-sm-12 col-md-4">
						<?php echo form_input($mobile); ?>
						<span class="text-danger" id="mobile_err"></span>
					</div>
					<div class="col-sm-12 col-md-2 border-right">
						<?php echo lang('facebook', 'facebook');?>
					</div>
					<div class="col-sm-12 col-md-4">
						<?php echo form_input($facebook); ?>
						<span class="text-danger" id="facebook_arr"></span>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12 col-md-2 border-right">
						<?php echo lang('phone', 'phone');?>
					</div>
					<div class="col-sm-12 col-md-4">
						<?php echo form_input($phone); ?>
						<span class="text-danger" id="phone_err"></span>
					</div>
					<div class="col-sm-12 col-md-2 border-right">
						<?php echo lang('line', 'line');?>
					</div>
					<div class="col-sm-12 col-md-4">
						<?php echo form_input($line); ?>
						<span class="text-danger" id="line_arr"></span>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12 col-md-2 border-right">
						<?php echo lang('fax', 'fax');?>
					</div>
					<div class="col-sm-12 col-md-4">
						<?php echo form_input($fax); ?>
						<span class="text-danger" id="fax_arr"></span>
					</div>
					<div class="col-sm-12 col-md-2 border-right">
						<?php echo lang('website', 'website');?>
					</div>
					<div class="col-sm-12 col-md-4">
						<?php echo form_input($website); ?>
						<span class="text-danger" id="website_arr"></span>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12 col-md-2 border-right">
						<?php echo lang('address', 'address');?>
					</div>
					<div class="col-sm-12 col-md-10">
						<?php echo form_input($fax); ?>
						<span class="text-danger" id="address_arr"></span>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12 col-md-2 border-right">
						<?php echo lang('internal_note', 'internal_note');?>
					</div>
					<div class="col-sm-12 col-md-10">
						<?php echo form_input($fax); ?>
						<span class="text-danger" id="internal_note_arr"></span>
					</div>
				</div>
				<!-- === END EMPLOYEE DETAIL === -->
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-info btn-sm" onclick="onclickCreateContact()">Create</button>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- === END MODAL DIALOG === -->