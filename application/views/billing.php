<?php echo!empty($message) ? $message : ''; ?>
<?php echo!empty($errorMessage) ? $errorMessage : ''; ?>

<div class="container-fluid">
    <input type="hidden" id="flag_search" />
    <div class="row">
        <div class="col-sm-12 criteria-container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="label label-width-1">
                        Start Date :
                    </div>
                    <div class="input">
                        <input id="dateTimeStart" name="dateTimeStart" maxlength="100" type="text" class="form-control input-lg date-input" placeholder="dd/mm/yyyy" readonly/>
                    </div>
                    <div class="label label-width-4 pl-2">
                        End Date :
                    </div>
                    <div class="input">    
                        <input id="dateTimeEnd" name="dateTimeEnd" maxlength="100" type="text" class="form-control input-lg date-input" placeholder="dd/mm/yyyy" readonly/>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="label label-width-1">
                        Billing ID :
                    </div>
                    <div class="input">
                        <input id="billing_id_search" maxlength="100" type="text" class="form-control input-lg date-input" />
                    </div>

                    <div class="label label-width-4 pl-2">
                        Customer Name :
                    </div>
                    <div class="input">
                        <input maxlength="100" id="customer_name" type="text" class="form-control input-lg date-input" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row pt-0">
        <div class="col-md-12 text-left" style="padding-left: 140px;"> 
            <button type="button" id="btn_search" class="btn btn-primary-pz btn-sm"><i class="fas fa-search"></i> Search</button>
            <button type="button" id="btn_clear" class="btn btn-default btn-sm">Clear</button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12" style="padding-top: 8px; display: none;" id="rs_div">
            <div class="view-mode-1">
                <table id="rstable" class="table table-sm table-bordered table-striped" style="width: 100%;">
                    <thead>
                        <tr>
                            <th width="7%" class="text-center">No.</th>
                            <th width="12%" class="text-center">Billing Note ID</th>
                            <th width="18%" class="text-center">Customer</th>
                            <th width="12%" class="text-center">Billing Date</th>
                            <th width="12%" class="text-center">Amount</th>
                            <th width="12%" class="text-center">Total</th>
                            <th width="10%" class="text-center">Last Update Date</th>
                            <th width="10%" class="text-center">Last Update User</th>
                            <th width="7%" class="text-center">Edit</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div> 

    <div class="view-mode-2">
        <div id="dataitems" class="mt-2"></div>
    </div>
</div>