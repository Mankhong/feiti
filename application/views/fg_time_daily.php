<input type="hidden" id="isAdmin" value="<?php echo $is_admin; ?>"/>
<input type="hidden" id="isMember" value="<?php echo $is_member; ?>"/>
<input type="hidden" id="isAccount" value="<?php echo $is_account; ?>"/>

<div class="small-topbar criteria-container">
	<div class="row">
		<div class="col-sm-12 col-md-4">
			<div class="label label-width-1">
				Date<font color="red"> *</font> :
			</div>
			<div class="input">
				<input type="text" id="ctrDate" name="ctrDate" class="form-control datepicker" style="width: 220px;" readonly/>
			</div>
		</div>
		<div class="col-sm-12 col-md-4">
			<div class="label label-width-1">
				Department :
			</div>
			<div class="input">
				<?php echo form_dropdown('ctrDepartment', $department_dropdown_data, $department_dropdown_default, ['class' => 'form-control', 'id' => 'ctrDepartment', 'style' => 'width: 220px;']); ?>
			</div>
		</div>
	</div>

	<div class="row <?php echo $is_member ? 'd-none' : ''; ?>">
		<div class="col-sm-12 col-md-4">
			<div class="label label-width-1">
				Employee No. :
			</div>
			<div class="input">
				<input type="text" id="ctrEmployeeNo" class="form-control" style="width: 220px;"/>
			</div>
		</div>
		<div class="col-sm-12 col-md-4 pt-2">
			<div class="label label-width-1">
				Employee Type :
			</div>
			<div class="input">
				<?php echo form_radio($employee_type_value_1); ?>
				<?php echo $employee_type_label_1; ?>
				&nbsp;
				<?php echo form_radio($employee_type_value_2); ?>
				<?php echo $employee_type_label_2; ?>
			</div>
		</div>
	</div>

	<div class="row pt-2">
		<div class="col-sm-12 col-md-7 text-left">
			<button type="button" id="btnSearch" class="btn btn-primary-pz btn-sm"><i class="fas fa-search"></i> Search</button>
			<button type="button" id="btnClear" class="btn btn-default btn-sm">Clear</button>
		</div>
	</div>
</div>

<?php echo !empty($message)? $message : ''; ?>
<?php echo !empty($errorMessage)? $errorMessage : ''; ?>

<div class="text-right px-3 pt-3 pb-3 d-none">
	<span id="export_buttons_container"></span>
</div>

<div id="resultContainer" style="display: none;">
	<div class="text-right px-3 pt-1 pb-0">
		<span id="export_buttons_container">
			<?php echo !empty($export_excel) ? $export_excel : ''; ?>
		</span>
	</div>

	<div style="padding: 20px 10px 10px 20px;" class="d-none">
		<h4>ผลลัพธ์การค้นหา</h4>
	</div>

	<table id="rstable" class="table table-sm table-bordered tb-timesheet" style="width: 100%;">
		<thead>
			<tr>
				<th width="10%" class="text-center no-sort">No.</th>
				<th width="10%" class="text-center">ID No.</th>
				<th width="20%" class="text-center">Employee Name</th>
				<th width="10%" class="text-center">Department</th>
				<th width="10%" class="text-center">Shift</th>
				<th width="10%" class="text-center">Leave Type</th>
				<th width="10%" class="text-center">Date</th>
				<th width="10%" class="text-center">Start Work</th>
				<th width="10%" class="text-center">End Work</th>
			</tr>
		</thead>
	</table>
	
	<br><br><br><br>
</div>