<div class="content-panel form-panel">
	<?php echo !empty($message)? $message : ''; ?>
	<?php echo !empty($errorMessage)? $errorMessage : ''; ?>

	<h3 class="content-panel-title">Customer</h3>

	<br>

	<?php echo form_open($submit_url, array('id' => 'form')); ?>

	<!-- === CUSTOMER DETAIL === -->
	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('business_type', 'business_type');?>
		</div>
		<div class="col-sm-12 col-md-4 pt-2">
			<?php echo form_radio($business_type_value_1); ?>
			<?php echo $business_type_label_1; ?>
			<?php echo form_radio($business_type_value_2); ?>
			<?php echo $business_type_label_2; ?>
			<span class="text-danger">
				<?php echo form_error('business_type'); ?></span>
		</div>
		<div class="col-sm-12 col-md-6">
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('customer_id', 'customer_id');?>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($customer_id); ?>
			<span class="text-danger">
				<?php echo form_error('customer_id'); ?></span>
		</div>
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('customer_type', 'customer_type');?>
		</div>
		<div class="col-sm-12 col-md-4 pt-2">
			<?php echo form_radio($customer_type_value_1); ?>
			<?php echo $customer_type_label_1; ?>
			<?php echo form_radio($customer_type_value_2); ?>
			<?php echo $customer_type_label_2; ?>
			<span class="text-danger">
				<?php echo form_error('customer_type'); ?></span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('customer_name', 'customer_name');?>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($customer_name); ?>
			<span class="text-danger">
				<?php echo form_error('customer_name'); ?></span>
		</div>
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('credit', 'credit');?>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($credit); ?>
			<span class="text-danger">
				<?php echo form_error('credit'); ?></span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('short_name', 'short_name');?>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($short_name); ?>
			<span class="text-danger">
				<?php echo form_error('short_name'); ?></span>
		</div>
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('discount', 'discount');?>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($discount); ?>
			<span class="text-danger">
				<?php echo form_error('discount'); ?></span>
		</div>
	</div>
	<!-- === END CUSTOMER DETAIL === -->

	<br><br>
	<!-- === CUSTOMER CONTACT === -->
	<ul class="nav nav-tabs">
		<li class="nav-item">
			<a class="nav-link active"><i class="fas fa-phone-square fa-lg"></i> Contact Address</a>
		</li>
	</ul>

	<br>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('mobile', 'mobile');?>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($mobile); ?>
			<span class="text-danger" id="mobile_err"></span>
		</div>
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('facebook', 'facebook');?>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($facebook); ?>
			<span class="text-danger" id="facebook_arr"></span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('phone', 'phone');?>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($phone); ?>
			<span class="text-danger" id="phone_err"></span>
		</div>
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('line', 'line');?>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($line); ?>
			<span class="text-danger" id="line_arr"></span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('fax', 'fax');?>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($fax); ?>
			<span class="text-danger" id="fax_arr"></span>
		</div>
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('website', 'website');?>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($website); ?>
			<span class="text-danger" id="website_arr"></span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('address', 'address');?>
		</div>
		<div class="col-sm-12 col-md-10">
			<?php echo form_input($fax); ?>
			<span class="text-danger" id="address_arr"></span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('internal_note', 'internal_note');?>
		</div>
		<div class="col-sm-12 col-md-10">
			<?php echo form_input($fax); ?>
			<span class="text-danger" id="internal_note_arr"></span>
		</div>
	</div>
	<!-- === END CUSTOMER CONTACT === -->

	<?php echo form_close(); ?>

	<br><br>

</div>

<!-- === CONTACT MODAL === -->
<div id="contactModal" class="modal contact-modal form-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					<strong>
						<h5>Create Contact</h5>
					</strong>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<!-- === CUSTOMER DETAIL === -->
				<div class="row">
					<div class="col-sm-12 col-md-2 border-right">
						<?php echo lang('mobile', 'mobile');?>
					</div>
					<div class="col-sm-12 col-md-4">
						<?php echo form_input($mobile); ?>
						<span class="text-danger" id="mobile_err"></span>
					</div>
					<div class="col-sm-12 col-md-2 border-right">
						<?php echo lang('facebook', 'facebook');?>
					</div>
					<div class="col-sm-12 col-md-4">
						<?php echo form_input($facebook); ?>
						<span class="text-danger" id="facebook_arr"></span>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12 col-md-2 border-right">
						<?php echo lang('phone', 'phone');?>
					</div>
					<div class="col-sm-12 col-md-4">
						<?php echo form_input($phone); ?>
						<span class="text-danger" id="phone_err"></span>
					</div>
					<div class="col-sm-12 col-md-2 border-right">
						<?php echo lang('line', 'line');?>
					</div>
					<div class="col-sm-12 col-md-4">
						<?php echo form_input($line); ?>
						<span class="text-danger" id="line_arr"></span>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12 col-md-2 border-right">
						<?php echo lang('fax', 'fax');?>
					</div>
					<div class="col-sm-12 col-md-4">
						<?php echo form_input($fax); ?>
						<span class="text-danger" id="fax_arr"></span>
					</div>
					<div class="col-sm-12 col-md-2 border-right">
						<?php echo lang('website', 'website');?>
					</div>
					<div class="col-sm-12 col-md-4">
						<?php echo form_input($website); ?>
						<span class="text-danger" id="website_arr"></span>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12 col-md-2 border-right">
						<?php echo lang('address', 'address');?>
					</div>
					<div class="col-sm-12 col-md-10">
						<?php echo form_input($fax); ?>
						<span class="text-danger" id="address_arr"></span>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12 col-md-2 border-right">
						<?php echo lang('internal_note', 'internal_note');?>
					</div>
					<div class="col-sm-12 col-md-10">
						<?php echo form_input($fax); ?>
						<span class="text-danger" id="internal_note_arr"></span>
					</div>
				</div>
				<!-- === END CUSTOMER DETAIL === -->
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-info btn-sm" onclick="onclickCreateContact()">Create</button>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- === END MODAL DIALOG === -->