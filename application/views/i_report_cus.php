<div class="container-fluid">
    <style type="text/css">
        .loader {
            position: relative;
            text-align: center;
            margin: 15px auto 35px auto;
            z-index: 9999;
            display: block;
            width: 50px;
            height: 50px;
            border: 10px solid rgba(0, 0, 0, .3);
            border-radius: 50%;
            border-top-color: #000;
            animation: spin 1s ease-in-out infinite;
            -webkit-animation: spin 1s ease-in-out infinite;

        }

        @keyframes spin {
            to {
                -webkit-transform: rotate(360deg);
            }
        }

        @-webkit-keyframes spin {
            to {
                -webkit-transform: rotate(360deg);
            }
        }
       
    



    </style>
    
    <div class="row">
        <div class="col-sm-12 criteria-container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="label label-width-1">
                        Start Date :
                    </div>
                    <div class="input">
                        <input id="dateTimeStart" name="dateTimeStart" maxlength="100" type="text" class="form-control input-lg date-input" placeholder="dd/mm/yyyy" readonly/>
                    </div>
                    <div class="label label-width-4 pl-2">
                        End Date :
                    </div>
                    <div class="input">    
                        <input id="dateTimeEnd" name="dateTimeEnd" maxlength="100" type="text" class="form-control input-lg date-input" placeholder="dd/mm/yyyy" readonly/>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="label label-width-1">
                       Rate:
                    </div>
                    <div class="input">
                        <input maxlength="100" id="rate" type="text" class="form-control input-lg date-input" oninput="floatOnly(this.id);"/>
                    </div>

                    <div class="label label-width-4 pl-2">
                       
                    </div>
                    <div class="input">
                        
                    </div>
                </div>
                
                
                
            </div>
        </div>
    </div>

    <div class="row pt-0">
        <div class="col-md-12 text-left" style="padding-left: 140px;"> 
            <button type="button" id="btn_search" class="btn btn-primary-pz btn-sm"><i class="fas fa-search"></i> Search</button>
            <button type="button" id="btn_clear" class="btn btn-default btn-sm">Clear</button>
        </div>
    </div>
    
    
    <div class="text-right" style="margin: 2px;" id="div_excel">
        <button type="button" class="btn btn-success" id="btn_excel" >Export Excel</button>
    </div>
    <div class="row" >
        <div class="col-md-12 noExl" style="padding-top: 8px;" id="rs_table"></div>
    </div>

    <div class="modal fade" id="loadMe" tabindex="-0" role="dialog" aria-labelledby="loadMeLabel" >
        <div class="modal-dialog modal-sm" role="document" style="max-width: 200px; max-height: 200px; opacity: 4.5;">
            <div class="modal-content" style="background-color: #f5f4f4;">
                <div class="modal-body text-center">
                    <div class="loader" >

                    </div>
                    <label>กำลังค้นหาข้อมูล</label>
                </div>
            </div>
        </div>
    </div>
</div>
