<div class="action-panel">
	<?php if (!empty($billing_url)) { ?>
	<a class="btn btn-default btn-sm ml-1" href="<?php echo $billing_url;?>" target="_blank"><i class="far fa-share-square"></i>
		Billing Note</a>
	<?php } ?>
	<?php if (!empty($receipt_url)) { ?>
	<a class="btn btn-default btn-sm ml-1 d-none" href="<?php echo $receipt_url;?>" target="_blank"><i class="fas fa-share-square"></i>
		Receipt</a>
	<?php } ?>

	<?php if (!empty($receipt_os_url)) { ?>
	<a class="btn btn-default btn-sm ml-1" href="<?php echo $receipt_os_url;?>" target="_blank">
		Receipt</a>
	<?php } ?>
</div>

<div class="content-panel form-panel">

	<h3 class="content-panel-title">Billing Note</h3>

	<?php echo !empty($message)? $message : ''; ?>
	<?php echo !empty($errorMessage)? $errorMessage : ''; ?>

	<?php echo form_open_multipart($submit_url, array('id' => 'form')); ?>

	<!-- === HIDDENS === -->
	<input type="hidden" id="temp_billing_id" name="temp_billing_id" value="<?php echo empty($temp_billing_id) ? '' : $temp_billing_id; ?>"/>
	<input type="hidden" id="temp_receipt_id" name="temp_receipt_id" value="<?php echo empty($temp_receipt_id) ? '' : $temp_receipt_id; ?>"/>

	<!-- === BILLING DETAIL === -->

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('billing_id', 'billing_id');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($billing_id); ?>
			<span class="text-danger" id="billing_id_arr">
				<?php echo form_error('billing_id'); ?></span>
		</div>

		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('billing_date', 'billing_date');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($billing_date); ?>
			<span class="text-danger">
				<?php echo form_error('billing_date'); ?></span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('receipt_id', 'receipt_id');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($receipt_id); ?>
			<span class="text-danger" id="receipt_id_arr">
				<?php echo form_error('receipt_id'); ?></span>
		</div>

		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('receipt_date', 'receipt_date');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($receipt_date); ?>
			<span class="text-danger">
				<?php echo form_error('receipt_date'); ?></span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('customer_id', 'customer_id');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<div class="input-group">
				<?php echo form_input($customer_id); ?>
				<?php if (empty($customer_disabled) || $customer_disabled == FALSE) { ?>
				<button type="button" class="btn btn-default btn-sm ml-1 customerPickerTrigger" style="display: inline-block; padding: 0px 15px;">
					<i class="fas fa-user"></i></button>
				<?php } ?>

			</div>
			<span class="text-danger">
				<?php echo form_error('customer_id'); ?></span>
		</div>
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('customer_name', 'customer_name');?>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($customer_name); ?>
			<span class="text-danger">
				<?php echo form_error('customer_name'); ?></span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('customer_type', 'customer_type');?>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($customer_type); ?>
			<span class="text-danger">
				<?php echo form_error('customer_type'); ?></span>
		</div>

		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('customer_credit', 'customer_credit');?>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($customer_credit); ?>
			<span class="text-danger">
				<?php echo form_error('customer_credit'); ?></span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('customer_address', 'customer_address');?>
		</div>
		<div class="col-sm-12 col-md-10">
			<?php echo form_textarea($customer_address); ?>
			<span class="text-danger">
				<?php echo form_error('customer_address'); ?></span>
		</div>
	</div>

	<!-- === END BILLING DETAIL === -->

	<br><br>
	<!-- === INVOICE LIST === -->
	<ul class="nav nav-tabs">
		<li class="nav-item">
			<a class="nav-link active"><i class="fas fa-bezier-curve"></i> Invoice Lines</a>
		</li>
	</ul>
	<div>
		<br>
		<table id="dataitemTable" class="table table-sm table-bordered table-striped">
			<thead>
				<tr>
					<th width="16%" class="text-center">Invoice Date</th>
					<th width="29%" class="text-center">Invoice ID</th>
					<th width="29%" class="text-center">Invoice ID for print</th>
					<th width="16%" class="text-center">Total Amount</th>
					<th width="10%" class="text-center">Delete</th>
				</tr>
			</thead>
			<tbody class="dataitem-body">
				<?php if(!empty($invoice_list)) { 
					//-- Dataitem: id|invoice_id|invoice_date|print_invoice_id|total_amount|dataitem_type
					$delimiter = '&88&';
					for ($i = 0; $i < count($invoice_list); $i++) {
						$invoice_id = Common::encodeString($invoice_list[$i]['invoice_id']);

						echo '<tr class="dataitem">' .
						'<td class="text-center">' .
						'<input type="hidden" class="dt-dataitem-type" value="DATA"/>' .
						'<input type="hidden" class="dt-id" value="' . $invoice_list[$i]['id'] . '"/>' .
						'<input type="hidden" class="dt-invoice-id" value="' . $invoice_id . '"/>' .
						'<input type="hidden" class="dt-invoice-date" value="' . $invoice_list[$i]['invoice_date'] . '"/>' .
						'<input type="hidden" class="dt-print-invoice-id" value="' . $invoice_list[$i]['print_invoice_id'] . '"/>' .
						'<input type="hidden" class="dt-total-amount" value="' . $invoice_list[$i]['total_amount'] . '"/>' .
						'<span class="dt-invoice-date-sp">' . $invoice_list[$i]['invoice_date'] . '</span>' .
						'</td>' .
						'<td class="text-left"><span class="dt-invoice-id-sp">' . $invoice_list[$i]['invoice_id'] . '</span></td>' .
						'<td class="text-left"><span class="dt-print-invoice-id-sp">' . $invoice_list[$i]['print_invoice_id'] . '</span></td>' .
						'<td class="text-right"><span class="dt-total-amount-sp">' . $invoice_list[$i]['total_amount'] . '</span></td>' .
						'<td class="text-center"><a class="btn btn-default btn-icon" onclick="onclickDeleteDataitem(\'' . $invoice_list[$i]['id'] . '\')"><i class="fas fa-times fa-sm"></i></a></td>' .
						'</tr>';
					}
				}?>

				<?php if(!empty($insert_dataitems)) { 
					$delimiter = '&88&';
					for ($i = 0; $i < count($insert_dataitems); $i++) {
						$values = explode($delimiter, $insert_dataitems[$i]);
						$invoice_id = Common::decodeString($values[1]);

						echo '<tr class="dataitem">' .
						'<td class="text-center">' .
						'<input type="hidden" class="dt-dataitem-type" value="' . $values[5] . '"/>' .
						'<input type="hidden" class="dt-id" value="' . $values[0] . '"/>' .
						'<input type="hidden" class="dt-invoice-id" value="' . $values[1] . '"/>' .
						'<input type="hidden" class="dt-invoice-date" value="' . $values[2] . '"/>' .
						'<input type="hidden" class="dt-print-invoice-id" value="' . $values[3] . '"/>' .
						'<input type="hidden" class="dt-total-amount" value="' . $values[4] . '"/>' .
						'<span class="dt-invoice-date-sp">' . $values[2] . '</span>' .
						'</td>' .
						'<td class="text-left"><span class="dt-invoice-id-sp">' . $invoice_id . '</span></td>' .
						'<td class="text-left"><span class="dt-print-invoice-id-sp">' . $values[3] . '</span></td>' .
						'<td class="text-right"><span class="dt-total-amount-sp">' . $values[4] . '</span></td>' .
						'<td class="text-center"><a class="btn btn-default btn-icon" onclick="onclickDeleteDataitem(\'' . $values[0] . '\')"><i class="fas fa-times fa-sm"></i></a></td>' .
						'</tr>';
					}
				}?>
			</tbody>
		</table>

		<div id="summary_container" style="display: none; font-size: 12px; text-align: right;">
			<table style="width: 240px; float: right;">
				<tr>
					<td style="width: 50%; text-align: right; font-weight: bold;">
						Amount :
					</td>
					<td style="width: 50%;">
						<span id="sm_amount"></span>
					</td>
				</tr>
				<tr>
					<td style="width: 50%; text-align: right; font-weight: bold;">
						Vat <span id="sm_vat">
							<?php echo $vat; ?></span>% :
					</td>
					<td style="width: 50%;">
						<span id="sm_vat_price"></span>
					</td>
				</tr>
				<tr>
					<td style="width: 50%; text-align: right; font-weight: bold;">
						Total :
					</td>
					<td style="width: 50%;">
						<span id="sm_total"></span>
					</td>
				</tr>
			</table>
		</div>

	</div>

	<br>
	<button type="button" class="btn btn-default btn-sm invoicePickerTrigger">Add</button>
	<!-- === END INVOICE LIST === -->

	<!-- === HIDDENS CONTAINER === -->
	<span id="dataitem_hidden_container">
		<?php if(!empty($insert_dataitems)) { 
			$delimiter = '&88&';
			for ($i = 0; $i < count($insert_dataitems); $i++) {
				$values = explode($delimiter, $insert_dataitems[$i]);
				echo '<input type="hidden" name="insert_dataitems[]" value="' . $insert_dataitems[$i] . '"/>';
			}
		}?>

		<?php if(!empty($delete_dataitems)) { 
			$delimiter = '&88&';
			for ($i = 0; $i < count($delete_dataitems); $i++) {
				$values = explode($delimiter, $delete_dataitems[$i]);
				echo '<input type="hidden" name="delete_dataitems[]" value="' . $delete_dataitems[$i] . '"/>';
			}
		}?>
	</span>

	<?php echo form_close(); ?>

	<br><br><br><br><br><br>

</div>

<!-- === CUSTOMER MODAL === -->
<div id="customerPickerModal" class="modal contact-modal form-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					<strong>
						<h5>Select Customer</h5>
					</strong>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<table id="customerPickerTable" class="table table-sm table-bordered table-striped">
					<thead>
						<tr>
							<th width="10%" class="text-center">No.</th>
							<th width="15%" class="text-center">Customer ID</th>
							<th width="45%" class="text-center">Customer Name</th>
							<th width="15%" class="text-center">Customer Type</th>
							<th width="15%" class="text-center">Business Type</th>
						</tr>
					</thead>
				</table>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- === END CUSTOMER MODAL === -->

<!-- === INVOICE PICKER MODAL === -->
<div id="invoicePickerModal" class="modal contact-modal form-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					<strong>
						<h5>Select Invoices</h5>
					</strong>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12 col-lg-8">
						<strong>Invoice Date :</strong> <input type="text" class="datepicker" id="ftStartDate" placeholder="From" style="width: 100px;" readonly/>
						&nbsp; - &nbsp;<input type="text" class="datepicker" id="ftEndDate" placeholder="To" style="width: 100px;" readonly/>
					</div>
					<div class="col-sm-12 col-lg-4">
						<div class="input-group">
							<label for="ftSearch"><strong>Search : &nbsp;</strong></label> <input type="text" class="form-control" id="ftSearch"/>
						</div>
					</div>
				</div>
				<table id="invoicePickerTable" class="table table-sm table-bordered table-striped" style="width: 100%;">
					<thead>
						<tr>
							<th width="10%" class="text-left" style="padding: 0px;"><input type="checkbox" class="invoice-picker-cball" ></th>
							<th width="30%" class="text-center">Invoice ID</th>
							<th width="30%" class="text-center">Invoice Date</th>
							<th width="30%" class="text-center">Total Amount</th>
						</tr>
					</thead>
				</table>
			</div>
			
			<span class="invoice-picker-hidden-container"></span>

			<div class="modal-footer">
				<button type="button" class="btn btn-primary-pz btn-sm invoice-picker-select-all">Select All</button>
				<button type="button" class="btn btn-primary-pz btn-sm invoice-picker-select">Select</button>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- === END INVOICE PICKER MODAL === -->

<!-- === CONFIRM DELETE MODAL === -->
<div id="confirmDeleteModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					<strong><span id="msgModalTitle">Confirm</span></strong>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>
					Do you want to delete this item?
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary-pz" id="confirmDeleteButton">Yes</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<!-- === END CONFIRM DELETE MODAL === -->