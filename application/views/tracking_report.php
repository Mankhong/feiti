<div class="container-fluid">
    <style type="text/css">
        .loader {
            position: relative;
            text-align: center;
            margin: 15px auto 35px auto;
            z-index: 9999;
            display: block;
            width: 50px;
            height: 50px;
            border: 10px solid rgba(0, 0, 0, .3);
            border-radius: 50%;
            border-top-color: #000;
            animation: spin 1s ease-in-out infinite;
            -webkit-animation: spin 1s ease-in-out infinite;

        }

        @keyframes spin {
            to {
                -webkit-transform: rotate(360deg);
            }
        }

        @-webkit-keyframes spin {
            to {
                -webkit-transform: rotate(360deg);
            }
        }
        .noExl{
            border: 0px;
        }

        table.table-bordered.dataTable td  {
            border-left-width: 1px !important;
            border-right-width: 1px !important;
        }
        table.table-bordered.dataTable tbody tr td {
            border-left-width: 1px !important;
            border-right-width: 1px !important;
        }



    </style>

    <div class="row">
        <div class="col-sm-12 criteria-container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="label label-width-1">
                        Start Date :
                    </div>
                    <div class="input">
                        <input id="dateTimeStart" name="dateTimeStart" maxlength="100" type="text" class="form-control input-lg date-input" placeholder="dd/mm/yyyy" readonly/>
                    </div>
                    <div class="label label-width-4 pl-2">
                        End Date :
                    </div>
                    <div class="input">    
                        <input id="dateTimeEnd" name="dateTimeEnd" maxlength="100" type="text" class="form-control input-lg date-input" placeholder="dd/mm/yyyy" readonly/>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="label label-width-1">
                        Customer ID:
                    </div>
                    <div class="input">
                        <input id="customerId" maxlength="100" type="text" class="form-control input-lg date-input" />
                    </div>

                    <div class="label label-width-4 pl-2">
                        Product ID:
                    </div>
                    <div class="input">
                        <input maxlength="100" id="productId" type="text" class="form-control input-lg date-input" />
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="label label-width-1">
                        Invoice ID:
                    </div>
                    <div class="input">
                        <input id="invoiceId" maxlength="100" type="text" class="form-control input-lg date-input" />
                    </div>

                    <div class="label label-width-4 pl-2">
                        Order ID:
                    </div>
                    <div class="input">
                        <input maxlength="100" id="orderId" type="text" class="form-control input-lg date-input" />
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row pt-0">
        <div class="col-md-12 text-left" style="padding-left: 140px;"> 
            <button type="button" id="btn_search" class="btn btn-primary-pz btn-sm"><i class="fas fa-search"></i> Search</button>
            <button type="button" id="btn_clear" class="btn btn-default btn-sm">Clear</button>
        </div>
    </div>


    <!--onclick="tableToExcel('rs_table', 'Export HTML Table to Excel')"-->

    <div class="row" >
        <div class="col-md-12 noExl" style="padding-top: 8px;" id="rs_table"></div>
    </div>

    <div class="row">
        <div class="col-md-12" style="padding-top: 8px;" id="rs_div">
            <table class="table table-sm table-bordered table-striped" id="planlist" style="width: 1800px;">
                <thead>
                    <tr>
                        <td width="280" rowspan="2" class="text-center" style="background-color: #f0f8ff; font-weight: bold; text-align: center; vertical-align: middle;">Customer Name</td>
                        <td colspan="7" class="text-center" style="background-color: #f0f8ff; font-weight: bold; text-align: center; vertical-align: middle;">Order</td>
                        <td colspan="3" class="text-center" style="background-color: #f0f8ff; font-weight: bold; text-align: center; vertical-align: middle;">Invoice</td>
                        <td colspan="2" class="text-center" style="background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Billing</td>
                        
                    </tr>
                    <tr>
                        <td width="120" class="text-center" style="background-color: #f0f8ff; font-weight: bold; text-align: center; vertical-align: middle;">Order Date</td>
                        <td width="100" class="text-center" style="background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Order Id</td>
                        <td width="300" class="text-center" style="background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Product Id</td>
                        <td width="120" class="text-center" style="background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Unit Price</td>
                        <td width="100" class="text-center" style="background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Quantity</td>
                        <td width="100" class="text-center" style="background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Amount</td>
                        <td width="100" class="text-center" style="background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Balance</td>
                        
                        <td width="100" class="text-center" style="background-color: #f0f8ff; font-weight: bold; text-align: center; vertical-align: middle;">Invoice Id</td>
                        <td width="120" class="text-center" style="background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Invoice Date</td>
                        <td width="100" class="text-center" style="background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Quantity</td>
                        
                        <td width="110" class="text-center" style="background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Billing Id</td>
                        <td width="110" class="text-center" style="background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Billing Date</td>
                        
                    </tr>
                </thead>

                
            </table>
        </div>
    </div>
    <div class="modal fade" id="loadMe" tabindex="-0" role="dialog" aria-labelledby="loadMeLabel" >
        <div class="modal-dialog modal-sm" role="document" style="max-width: 200px; max-height: 200px; opacity: 4.5;">
            <div class="modal-content" style="background-color: #f5f4f4;">
                <div class="modal-body text-center">
                    <div class="loader" >

                    </div>
                    <label>กำลังค้นหาข้อมูล</label>
                </div>
            </div>
        </div>
    </div>
</div>
