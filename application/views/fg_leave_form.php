<div class="content-panel form-panel">

	<h3 class="content-panel-title">Leave</h3>

	<?php echo !empty($message)? $message : ''; ?>
	<?php echo !empty($errorMessage)? $errorMessage : ''; ?>

	<?php echo form_open_multipart($submit_url, array('id' => 'form')); ?>
	<?php echo form_input($from_date_temp); ?>
	<?php echo form_input($to_date_temp); ?>

	<!-- === LEAVE === -->
	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('employee_no', 'employee_no');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<div class="input-group">
				<?php echo form_input($id); ?>
				<?php echo form_input($employee_id); ?>
				<?php echo form_input($employee_no); ?>
				<?php echo form_input($action_type); ?>

				<?php if (empty($employee_disabled) || $employee_disabled == FALSE) { ?>
				<button type="button" class="btn btn-default btn-sm ml-1 employeePickerTrigger" style="display: inline-block; padding: 0px 15px;">
					<i class="fas fa-user"></i></button>
				<?php } ?>

			</div>
			<span class="text-danger">
				<?php echo form_error('employee_id'); ?></span>
			<span class="text-danger">
				<?php echo form_error('employee_no'); ?></span>
		</div>

		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('employee_name', 'employee_name');?>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($employee_name); ?>
			<span class="text-danger">
				<?php echo form_error('employee_name'); ?></span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('leave_type', 'leave_type');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_dropdown('type', $type_dropdown_data, $type_dropdown_default, ['class' => 'form-control']); ?>
			<span class="text-danger">
				<?php echo form_error('type'); ?></span>
		</div>

		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('duration', 'duration');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_dropdown('duration', $duration_dropdown_data, $duration_dropdown_default, ['class' => 'form-control']); ?>
			<span class="text-danger">
				<?php echo form_error('duration'); ?></span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('from_date', 'from_date');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($from_date); ?>
			<span class="text-danger">
				<?php echo form_error('from_date'); ?></span>
		</div>

		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('to_date', 'to_date');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($to_date); ?>
			<span class="text-danger">
				<?php echo form_error('to_date'); ?></span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('leave_reason', 'leave_reason');?>
		</div>
		<div class="col-sm-12 col-md-10">
			<?php echo form_textarea($reason); ?>
			<span class="text-danger">
				<?php echo form_error('reason'); ?></span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			
		</div>
		<div class="col-sm-12 col-md-10 pt-2 text-right">
			<?php if(!empty($render_cancel_button)) {?>
				<button type="button" class="btn btn-primary-pz" id="btnCancel">Cancel Leave</button>
			<?php }?>
		</div>
	</div>

	<!-- === END LEAVE === -->

	<br><br><br><br><br><br>

</div>

<!-- === EMPLOYEE PICKER MODAL === -->
<div id="employeePickerModal" class="modal contact-modal form-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					<strong>
						<h5>Select Employee</h5>
					</strong>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<table id="employeePickerTable" class="table table-sm table-bordered table-striped">
					<thead>
						<tr>
							<th width="10%" class="text-center">No.</th>
							<th width="15%" class="text-center">Employee No.</th>
							<th width="45%" class="text-center">Employee Name</th>
							<th width="15%" class="text-center">Department</th>
							<th width="15%" class="text-center">Job Position</th>
						</tr>
					</thead>
				</table>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- === END EMPLOYEE PICKER MODAL === -->

<!-- === CONFIRM CANCEL MODAL === -->
<div id="confirmCancelModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					<strong>ยืนยันการยกเลิก</strong>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p class="text-danger text-center" style="font-size: 16px;">
					<strong>ต้องการยกเลิกการลานี้ใช่หรือไม่?</strong>
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary-pz"  onclick="confirmCancelLeave()" style="width: 80px;">ใช่</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" style="width: 80px;">ไม่ใช่</button>
			</div>
		</div>
	</div>
</div>
<!-- === END CONFIRM CANCEL MODAL === -->

<!-- === CONFIRM CANCEL MODAL === -->
<div id="confirmDuplicateModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					<strong>ยืนยันการทำรายการ</strong>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p class="text-danger text-center" style="font-size: 16px;">
					<strong><span id="confirmDuplicateModalMessage"></span></strong>
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary-pz"  onclick="confirmSubmitLeave()" style="width: 80px;">ยืนยัน</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" style="width: 80px;">ยกเลิก</button>
			</div>
		</div>
	</div>
</div>
<!-- === END CONFIRM CANCEL MODAL === -->