<?php echo !empty($message)? $message : ''; ?>
<?php echo !empty($errorMessage)? $errorMessage : ''; ?>

<div class="text-right px-3 pt-1">
	<span id="export_buttons_container">
		<?php echo $export_excel; ?>
	</span>
	<a class="btn btn-default btn-sm view-mode-1-button"><i class="fas fa-list"></i></a>
	<a class="btn btn-default btn-sm view-mode-2-button"><i class="fas fa-th"></i></a>
</div>

<div class="view-mode-1">
	<table id="rstable" class="table table-sm table-bordered table-striped" style="width: 100%;">
		<thead>
			<tr>
				<th width="5%" class="text-center">No.</th>
				<th width="12%" class="text-center">Product ID/Part ID</th>
				<th width="14%" class="text-center">Discription 1/Part Name</th>
				<th width="12%" class="text-center">Customer</th>
				<th width="10%" class="text-center">Product Type</th>
				<th width="10%" class="text-center">Invoice Type</th>
				<th width="10%" class="text-center">Process</th>
				<th width="10%" class="text-center">Last Update Date</th>
				<th width="10%" class="text-center">Last Update User</th>
				<th width="7%" class="text-center">Action</th>
				<th width="0%" class="d-none">BOI Discription</th>
				<th width="0%" class="d-none">Local Price</th>
				<th width="0%" class="d-none">BOI Price</th>
				<th width="0%" class="d-none">Packing/Lot Size</th>
				<th width="0%" class="d-none">Internal Note</th>
				<th width="0%" class="d-none">Carton</th>
				<th width="0%" class="d-none">Carton Temporary</th>
				<th width="0%" class="d-none">Invoice Type</th>
				<th width="0%" class="d-none">Process</th>
				<th width="0%" class="d-none">Discription 2</th>
				<th width="0%" class="d-none">Customer ID</th>
				<th width="0%" class="d-none">Customer Name</th>
			</tr>
		</thead>
	</table>
</div>

<div class="view-mode-2">
	<div id="dataitems" class="mt-2"></div>
</div>