<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row pt-2">
        <div class="col-12">
            <div class="row">
                <div class="col-2">
                    <image src="<?= base_url('assets/images/default_image.jpg') ?>">
                </div>
                <div class="col-10" style="padding-left: 3.0rem !important;">
                    <div class="invoice-title">
                        <div>
                            <h2>บริษัท เฟยตี้ พรีซิชั่น (ไทยแลนด์) จำกัด<br>FEITI PRECISION (THAILAND)CO.,LTD</h2>
                            <hr style="margin-top: 0px;margin-bottom: 15px;">
                            <small>
                                1/92 หมู่ 5 สวนอุตสาหกรรมโรจนะ ต.คานหอม อ.อุทัย จ.พระนครศรีอยุธยา โทร (035)227916-9 โทรสาร (035)226148
                                <br>
                                1/92 MOO 5 ROJANA TUMBOL KANHAM AMPHUR U-THAI AYUTTHAYA TEL:(035)227916-9 FAX:(035)226148
                            </small>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row pt-3">
                <div class="col-md-12">
                    <div align="center">
                        <h5 class="font-weight-bold">ใบส่งของ <br>DELIVERY ORDER</h5>
                    </div>
                </div>
            </div>

            <div class="row pt-4">
                <div class="col-6">
                    <address>
                        <strong>DELIVERY ORDER NO. 1023456789123</strong>
                    </address>
                </div>
                <div class="col-6 text-right">
                    <address>
                        <span>DATE: 0123456789123</span>
                        <br>
                        <span>วันที่: 0123456789123</span>
                    </address>
                </div>
            </div>
        </div>
    </div>

    <div class="row pt-3">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="table-responsive table-bordered">
                    <table class="table table-condensed ">
                        <thead>
                            <tr>
                                <td class="text-center"><strong>Item<br>ลำดับที่</strong></td>
                                <td class="text-center"><strong>P.O.NO.<br>ใบสั่งสินค้าเลขที่</strong></td>
                                <td class="text-center"><strong>DESCRIPTION <br>รายการ</strong></td>
                                <td class="text-center"><strong>QUANTITY <br> จำนวน</strong></td>

                            </tr>
                        </thead>
                        <tbody>
                            <!-- foreach ($order->lineItems as $line) or some such thing here -->
                            <tr>
                                <td>BS-200</td>
                                <td class="text-center">$10.99</td>
                                <td class="text-center">1</td>
                                <td class="text-center">1</td>
                            </tr>
                            <tr>
                                <td>BS-400</td>
                                <td class="text-center">$20.00</td>
                                <td class="text-center">3</td>
                                <td class="text-center">1</td>
                            </tr>
                            <tr>
                                <td>BS-1000</td>
                                <td class="text-center">$600.00</td>
                                <td class="text-center">1</td>
                                <td class="text-center">1</td>
                            </tr>
                            <tr>
                                <td>BS-1000</td>
                                <td class="text-center">$600.00</td>
                                <td class="text-center">1</td>
                                <td class="text-center">1</td>
                            </tr>
                            <tr>
                                <td>BS-1000</td>
                                <td class="text-center">$600.00</td>
                                <td class="text-center">1</td>
                                <td class="text-center">1</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div align="center">
                                        <address>
                                            <span>Received goods static in good otder & conditon</span><br>
                                            <span>ได้รับสินค้าตามรายการข้างบนนี้ถูกต้องเรียบร้อยแล้ว</span><br>   
                                        </address>
                                    </div>
                                    <div>  
                                        <div style="display: inline-block"></div><span>SIGNED</span><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 37%; display: inline-block;"></div>
                                        <div style="display: inline-block"></div><span>DATE</span><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 37%; display: inline-block;"></div>      
                                    </div>
                                    <div>  
                                        <div style="display: inline-block"></div><span style="padding-left: 21px;">ลงชื่อ</span><div style="border-bottom: 0px dashed #000;text-decoration: none; width: 37%; display: inline-block;"></div>
                                        <div style="display: inline-block"></div><span>วันที่</span><div style="border-bottom: 0px dashed #000;text-decoration: none; width: 37%; display: inline-block;"></div>      
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div align="center">
                                        <address>
                                            <span>FOR FEITI PRECISION (THAILAND) CO.,LTD</span><br>
                                            <span>ในนาม บริษัทเฟยตี้ พรีซิชั่น(ไทยแลนด์) จำกัด</span><br>   
                                        </address>
                                    </div>
                                    <div align="center">
                                        <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 60%; display: inline-block;"></div>
                                        <div><span>Approve by</span></div>       
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row pt-3">

    </div>
</div>