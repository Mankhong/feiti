<style>
    .table-bordered {
        border: 1px solid #000000;
    }
    .table-bordered th, .table-bordered td{
        border: 1px solid #000000;
    }
    .table-bordered thead td {
        border-bottom-width: 1px;
    }
    .setfont{
        font-size: 12px;
    }
    .table-font{
        font-size: 19px;
        font-family: 'Angsana New';
    }
    tbody tr td{
        font-size: 14px;
        font-family: 'Angsana New';
    }

</style>
<div class="container">
    <input type="hidden" name="invoice_id" id="invoice_id" value="<?= $invoice_id ?>">
    <div class="row pt-2">
        <div class="col-12">
            <div class="row">
                <div class="col-2">
                    <image src="<?= base_url('assets/images/default_image.jpg') ?>">
                </div>
                <div class="col-10" style="padding-left: 3.0rem !important;">
                    <div class="invoice-title">
                        <div>
                            <h2>บริษัท เฟยตี้ พรีซิชั่น (ไทยแลนด์) จำกัด<br>FEITI PRECISION (THAILAND)CO.,LTD</h2>
                            <hr style="margin-top: 0px;margin-bottom: 15px;">
                            <small>
                                1/92 หมู่ 5 สวนอุตสาหกรรมโรจนะ ต.คานหาม อ.อุทัย จ.พระนครศรีอยุธยา โทร (035) 227916-9 โทรสาร (035) 226148
                                <br>
                                1/92 MOO 5 ROJANA INDUSTRIAL PARK TAMBOL KANHAM AMPHUR U-THAI AYUTTHAYA 13210 TEL : (035) 227916-9 FAX : (035) 226148
                            </small>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row pt-2">
                <div class="col-md-12">
                    <div align="center">
                        <h5 class="font-weight-bold">DELIVERY ORDER</h5>
                    </div>
                </div>
            </div>

            <div class="row pt-3">
                <div class="col-6" >

                    <div class="panel-body" style="border: 1px solid #000000; padding: 8px;">
                        <span class="pl-1 font-weight-bold" style="padding-left: 3px;">Delivery To</span>
                        <div class="pl-2 setfont" style="padding: 2px;">
                            <address>
                                <span id="v_customer_name"></span><br>
                                <span id="v_cusmer_address"></span><br>   
                            </address>
                        </div>
                    </div>
                </div>

                <div class="col-6 text-left">
                    <address>
                        <label class="font-weight-bold" >NO.</label><br>
                        <label class="font-weight-bold" >DATE: <span id="v_invoice_date"></label><br> 
                        <label class="font-weight-bold" >LORRY NO.: <span id="v_invoice_id"></span></label><br> 
                    </address>
                </div>
            </div>
            <div class="row pt-1">
                <div class="col-12 text-right font-weight-bold" style="padding-right: 45px;" >Customer Copy</div>
            </div>

        </div>
    </div>
    <div class="row pt-3">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="table-responsive">
                    <table class="table table-condensed table-bordered table-font" id="rstable">
                        <thead>
                            <tr>
                                <td style="width: 7%;" class="text-center"><strong>Item</strong></td>
                                <td style="width: 15%;"class="text-center"><strong>P.O.NO.</strong></td>
                                <td class="text-center"><strong>DESCRIPTION</strong></td>
                                <td style="width: 18%;" class="text-center"><strong>NO OFF CARTON</strong></td>
                                <td style="width: 20%;" class="text-center"><strong>QUANTITY</strong></td>
                            </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
    <div class="row" pb-4>
        <div class="col-4">
            <div align="center">
                <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"></div>
                <div><span>Check by</span></div>       
            </div>
        </div>
        <div class="col-4">
            <div align="center">
                <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"></div>
                <div><span>Approve by</span></div>       
            </div>
        </div>
        <div class="col-4">
            <div align="center">
                <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"></div>
                <div><span>Receipt by</span></div>       
            </div>
        </div>

    </div>


    <div class="row pt-3">
        <br><br>
        <div class="col-md-12 text-center"> 
            <!--<button type="button" class="btn btn-success" id="btn_print" >Print</button>-->
        </div>

    </div>
    <div class="container" style="display: none; border-top: 0px solid #dee2e6; margin-left: 2px; max-width: 1160px;" id="contentdiv">
        <div class="printableArea" id="printableAreatest" >
            <style type="text/css">
                @page {
                    margin: 0;
                }
                
                @font-face {
                    font-family: 'Angsana New';
                    src: url('<?php echo base_url('assets/font/angsa.ttf')?>') format('ttf');
                    font-weight: normal;
                    font-style: normal;
                }
            </style>
        </div>
    </div>

    <div class="row pt-3">

    </div>
</div>


