<style>
    #xx.item:empty:before {
        content: "\200b"; 
    }
    @page{
        margin: 0cm;
    }

    .table-bordered {
        border: 1px solid #000000;
    }
    .table-bordered th, .table-bordered td{
        border: 1px solid #000000;
    }
    .table-bordered thead td {
        border-bottom-width: 1px;
    }
    .setfont{
        font-size: 12px;
    }
    .table-font{
        font-size: 19px;
        font-family: 'Angsana New';
    }

    tbody tr td{
        font-size: 14px;
        font-family: 'Angsana New';
    }


</style>

<div class="container">
    <input type="hidden" name="invoice_id" id="invoice_id" value="<?= $invoice_id ?>">
    <div class="row pt-2">
        <div class="col-12">
            <div class="row">
                <div class="col-2">
                    <image src="<?= base_url('assets/images/default_image.jpg') ?>">
                </div>
                <div class="col-10" style="padding-left: 3.0rem !important;">
                    <div class="invoice-title">
                        <div>
                            <h2>บริษัท เฟยตี้ พรีซิชั่น (ไทยแลนด์) จำกัด<br>FEITI PRECISION (THAILAND)CO.,LTD</h2>
                            <hr style="margin-top: 0px;margin-bottom: 15px;">
                            <small>
                                1/92 หมู่ 5 สวนอุตสาหกรรมโรจนะ ต.คานหาม อ.อุทัย จ.พระนครศรีอยุธยา โทร (035) 227916-9 โทรสาร (035) 226148
                                <br>
                                1/92 MOO 5 ROJANA INDUSTRIAL PARK TAMBOL KANHAM AMPHUR U-THAI AYUTTHAYA 13210 TEL : (035) 227916-9 FAX : (035) 226148
                            </small>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row pt-4">
                <div class="col-6">
                    <address>
                        <span>เลขประจำตัวผู้เสียภาษี: 0145547001421</span><br>
                        <span>สำนักงานใหญ่</span><br>
                    </address>
                </div>
                <div class="col-6 text-right">
                    <address>
                        <label>Tax Payer ID No: 0145547001421</label><br>
                        <strong>RoHS  &nbsp;&nbsp;&nbsp;&nbsp; 
                            &nbsp;&nbsp;&nbsp;&nbsp; 
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp; 
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp; 
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;
                        </strong>  
                    </address>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-2" >
                    <div align="center" class="panel-body" style="border: 1px solid #000000;">
                        <address>
                            <span>ต้นฉบับ</span><br>
                            <span>ORIGINAL</span><br>
                        </address>
                    </div>
                </div>
                <div class="col-3" ></div>
                <div class="col-7 text-left">
                    <address>
                        <strong>ใบกำกับภาษี/ใบแจ้งหนี้</strong><br>
                        <strong>TAX INVOICE/INVOICE</strong><br>   
                    </address>
                </div>
            </div>

            <div class="row pt-3">
                <div class="col-1 pt-3" style="width: 98px;">
                    <strong>SOLD TO:</strong>
                </div>
                <div class="col-10">
                    <div class="row">
                        <div class="col-8">
                            <div class="setfont">
                                <address>
                                    <span id="v_customer_name"></span><br>
                                    <span id="v_cusmer_address"></span><br>   
                                </address>
                            </div>
                        </div>
                        <div class="col-2 text-right">
                            <div>
                                <strong>DATE: </strong> <br>
                                <strong>Invoice No: </strong>   
                            </div>
                        </div>
                        <div class="col-2 text-left">
                            <div class="setfont" style="padding: 0px; font-size: 13px;">
                                <span id="v_invoice_date"></span><br>
                                <span id="v_invoice_id"></span>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row pt-3">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="table-responsive">
                    <table class="table table-condensed table-bordered table-font" id="rstable">
                        <thead>
                            <tr>
                                <td class="text-center" style="width:7%;"><strong>Item<br>ลำดับ</strong></td>
                                <td class="text-center" style="width:13%;"><strong>P.O.NO.<br>ใบสั่งสินค้า</strong></td>
                                <td class="text-center"><strong>DESCRIPTION<br>รายการ</strong></td>
                                <td class="text-center" style="width:13%;"><strong>QUANTITY<br>จำนวน</strong></td>
                                <td class="text-center" style="width:13%;"><strong>UNIT PRICE<br>ราคา/หน่วย</strong></td>
                                <td class="text-center" style="width:13%;"><strong>AMOUNT(BATH)<br>จำนวนเงิน</strong></td>
                            </tr>
                        </thead>

                    </table>

                </div>
            </div>
        </div>
    </div>
    <div class="row" pb-4>
        <div class="col-6 ">
            <div class="panel-body table-font" style="border: 1px solid #000000;">

                <div align="center">
                    <address>
                        <span>Received goods stated in good order & condition</span><br>
                        <span>ได้รับสินค้าตามรายการข้างบนนี้ถูกต้องเรียบร้อยแล้ว</span><br>   
                    </address>
                </div>
                <div>  
                    <div style="display: inline-block"></div><span style="padding-left: 20px;">SIGNED</span><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 37%; display: inline-block; "></div>
                    <div style="display: inline-block"></div><span>DATE</span><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 37%; display: inline-block;"></div>      
                </div>
                <div>  
                    <div style="display: inline-block"></div><span style="padding-left: 21px;">ลงชื่อ</span><div style="border-bottom: 0px dashed #000;text-decoration: none; width: 37%; display: inline-block;"></div>
                    <div style="display: inline-block"></div><span style="padding-left: 15px;">วันที่</span><div style="border-bottom: 0px dashed #000;text-decoration: none; width: 37%; display: inline-block; "></div>      
                </div>

            </div>
        </div>
        <div class="col-6">
            <div class="panel-body table-font" style="border: 1px solid #000000;">

                <div align="center">
                    <address>
                        <span>FOR FEITI PRECISION (THAILAND) CO.,LTD</span><br>
                        <span>ในนาม บริษัทเฟยตี้ พรีซิชั่น(ไทยแลนด์) จำกัด</span><br>   
                    </address>
                </div>
                <div align="center">
                    <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 60%; display: inline-block;"></div>
                    <div><span>Approve by</span></div>       
                </div>

            </div>
        </div>
    </div>
    <hr>
    <div class="row pt-3">
        <br><br>
        <div class="col-md-12 text-center"> 
            <!--<button type="button" class="btn btn-success" id="btn_print" >Print</button>-->
        </div>

    </div>

    <div class="container" style="display: none; border: none; padding: 0px;" id="contentdiv">
        <style>
            @page {
                margin: 0;
            }

            .tablecustom td div{
                width: 10px;
                white-space: nowrap;
                overflow: hidden;
            }
            
        </style>
        <div class="printableArea" id="printableAreatest">
            <style type="text/css">
                @font-face {
                    font-family: 'Angsana New';
                    src: url('<?php echo base_url('assets/font/angsa.ttf')?>') format('ttf');
                    font-weight: normal;
                    font-style: normal;
                }

                @font-face {
                    font-family: 'Cordia New';
                    src: url('<?php echo base_url('assets/font/CORDIA.ttf')?>') format('ttf');
                    font-weight: normal;
                    font-style: normal;
                }

                .body-table{
                    table-layout: fixed;
                    white-space: nowrap;
                    overflow: hidden;
                }

                .body-table td {
                    border: 0px solid #333;
                }
            </style>
            <span id="printableAreatestContent"></span>
        </div>
    </div>
</div>







