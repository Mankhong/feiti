<?php echo form_open_multipart($submit_url, array('id' => 'form', 'class' => 'pl-2', 'method' => 'POST')); ?>
	<div class="small-topbar criteria-container">
		<div class="px-1 pt-1" style="width: 50%;	">
			<strong style="font-size: 1.2em; color: #555;"><i class="far fa-clock"></i> Upload Check IN-OUT File</strong>
		</div>

		<div class="pt-3 pl-4">
			<div style="width: 80%; max-width: 600px;">
				<?php echo !empty($message)? $message : ''; ?>
				<?php echo !empty($errorMessage)? $errorMessage : ''; ?>
			</div>

			<div class="pl-2">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="label label-width-4">
							<input type="file" name="ctrFile" class="form-control text-center" style="width: 300px;" id="ctrFile"/>
						</div>
						<div class="input">
							
						</div>
					</div>
				</div>

				<div class="row pt-2">
					<div class="col-sm-12 col-md-7 text-left">
						<button type="submit" id="btnImport" class="btn btn-warning-pz btn-sm" style="width: 100px;"><i class="fas fa-upload"></i> Upload</button>
					</div>
				</div>
			</div>
			
		</div>

		<br>
	</div>
<?php echo form_close(); ?>

<?php echo form_open_multipart($cal_submit_url, array('id' => 'cal_form', 'class' => 'pl-2')); ?>

	<div class="small-topbar criteria-container pl-4 pb-4">
		<br>

		<div class="pl-0 pt-1 pb-3" style="font-size: 1.2em; color: #555;">
			<strong><i class="far fa-newspaper"></i> Calculate Timesheeet</strong>
		</div>

		<div class="pl-3" style="width: 80%; max-width: 600px;">
			<?php echo !empty($cal_message)? $cal_message : ''; ?>
			<?php echo !empty($cal_errorMessage)? $cal_errorMessage : ''; ?>
		</div>

		<div class="pt-2 pl-4">
			<div class="row">
				<div class="col-sm-12 col-md-4">
					<div class="label label-width-1">
						Year :
					</div>
					<div class="input">
						<select id="ctrYear" name="ctrYear" class="form-control" style="width: 220px;">
							<?php 
								$current_year = date('Y');
								$current_month = date('m');
								//$current_date =  date('d', time() + (13 * 60 * 60));
								$current_date =  date('d');

								if ($current_month == 12) {
									$current_year += 1;
								}

								$year_offset = 5;
								
								for ($i = $current_year; $i > ($current_year - $year_offset); $i--) {
									echo '<option value="' . $i . '">' . $i . '</option>';
								}
							?>
						</select>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-4">
					<div class="label label-width-1">
						Month :
					</div>
					<div class="input">
						<select id="ctrMonth" name="ctrMonth" class="form-control" style="width: 220px;">
							<option value="1">January</option>
							<option value="2">Febuary</option>
							<option value="3">March</option>
							<option value="4">April</option>
							<option value="5">May</option>
							<option value="6">June</option>
							<option value="7">July</option>
							<option value="8">August</option>
							<option value="9">September</option>
							<option value="10">October</option>
							<option value="11">November</option>
							<option value="12">December</option>
						</select>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-4">
					<div class="label label-width-1">
						Date :
					</div>
					<div class="input">
						<select id="ctrDate" name="ctrDate" class="form-control" style="width: 220px;">
						</select>
						<input type="hidden" id="ctrCalculatingDate"/>
						<input type="hidden" id="ctrStartDate"/>
						<input type="hidden" id="ctrEndDate"/>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-4">
					<div class="label label-width-1">
						Employee No :
					</div>
					<div class="input">
						<input type="text" id="ctrEmployeeNo" name="ctrEmployeeNo" class="form-control" style="width: 220px;"/>
						<input type="hidden" name="ctrBadgenumber" id="ctrBadgenumber" value="" />
					</div>
				</div>
			</div>

			<div class="row pt-3">
				<div class="col-sm-12 col-md-7 text-left">
					<button type="submit" id="btnCalculate" class="btn btn-warning-pz btn-sm" style="width: 100px;"><i class="fas fa-fingerprint"></i> Calculate</button>
				</div>
			</div>

		</div>
	</div>

<?php echo form_close(); ?>

<?php echo form_open_multipart($init_submit_url, array('id' => 'init_form', 'class' => 'pl-2')); ?>

	<div class="small-topbar criteria-container pl-4 pb-4">
		<br>

		<div class="pl-0 pt-1 pb-3" style="font-size: 1.2em; color: #555;">
			<strong><i class="fa fa-table"></i> Init Timesheeet</strong>
		</div>

		<div class="pl-3" style="width: 80%; max-width: 600px;">
			<?php echo !empty($init_message)? $init_message : ''; ?>
			<?php echo !empty($init_errorMessage)? $init_errorMessage : ''; ?>
		</div>

		<div class="pt-2 pl-4">
			<div class="row">
				<div class="col-sm-12 col-md-4">
					<div class="label label-width-1">
						Year :
					</div>
					<div class="input">
						<select id="initCtrYear" name="initCtrYear" class="form-control" style="width: 220px;">
							<?php 
								for ($i = $current_year; $i > ($current_year - $year_offset); $i--) {
									echo '<option value="' . $i . '">' . $i . '</option>';
								}
							?>
						</select>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-4">
					<div class="label label-width-1">
						Month :
					</div>
					<div class="input">
						<select id="initCtrMonth" name="initCtrMonth" class="form-control" style="width: 220px;">
							<option value="1">January</option>
							<option value="2">Febuary</option>
							<option value="3">March</option>
							<option value="4">April</option>
							<option value="5">May</option>
							<option value="6">June</option>
							<option value="7">July</option>
							<option value="8">August</option>
							<option value="9">September</option>
							<option value="10">October</option>
							<option value="11">November</option>
							<option value="12">December</option>
						</select>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-4">
					<div class="label label-width-1">
						Date :
					</div>
					<div class="input">
						<select id="initCtrDate" name="initCtrDate" class="form-control" style="width: 220px;">
						</select>
						<input type="hidden" id="initCtrCalculatingDate"/>
						<input type="hidden" id="initCtrStartDate"/>
						<input type="hidden" id="initCtrEndDate"/>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-4">
					<div class="label label-width-1">
						Employee No :
					</div>
					<div class="input">
						<input type="text" id="initCtrEmployeeNo" name="initCtrEmployeeNo" class="form-control" style="width: 220px;"/>
						<input type="hidden" name="initCtrBadgenumber" id="initCtrBadgenumber" value="" />
					</div>
				</div>
			</div>

			<div class="row pt-3">
				<div class="col-sm-12 col-md-7 text-left">
					<button type="submit" id="btnInit" class="btn btn-info btn-sm"><i class="fa fa-database"></i> Initial timesheet</button>
				</div>
			</div>

		</div>
	</div>

	<!-- === CUSTOM LOADING DIALOG === -->
	<div id="customLoadingModal" class="modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
			style="transform: translateY(0%);">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-body text-center">
					<h5 class="modal-title"><spand id="customLoadingMessage"></span></h5>
				</div>
			</div>
		</div>
	</div>
	<!-- === END LOADING DIALOG === -->

<?php echo form_close(); ?>

<br><br><br><br><br><br>