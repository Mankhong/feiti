<div class="content-panel form-panel">

	<h3 class="content-panel-title">Holiday</h3>

	<?php echo !empty($message)? $message : ''; ?>
	<?php echo !empty($errorMessage)? $errorMessage : ''; ?>

	<?php echo form_open_multipart($submit_url, array('id' => 'form')); ?>

	<!-- === HOLIDAY === -->
	<br>
	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('holiday_date', 'holiday_date');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($id); ?>
			<?php echo form_input($date); ?>
			<span class="text-danger">
				<?php echo form_error('date'); ?></span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('holiday_desc', 'holiday_desc');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($desc); ?>
			<span class="text-danger">
				<?php echo form_error('desc'); ?></span>
		</div>

		<div class="col-sm-12 col-md-1 border-right">
			<?php echo lang('holiday_type', 'holiday_type');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_dropdown('type', $type_dropdown_data, $type_dropdown_default, ['class' => 'form-control']); ?>
			<span class="text-danger">
				<?php echo form_error('type'); ?></span>
		</div>
	</div>

	<br><br>

	<div class="row">
		<div class="col-sm-12 col-md-2">
			
		</div>
		<div class="col-sm-12 col-md-10 pt-2">
			<?php if(!empty($render_cancel_button)) {?>
				<button type="button" class="btn btn-primary-pz" id="btnCancel">Cancel Holiday</button>
			<?php }?>
		</div>
	</div>

	<!-- === END HOLIDAY === -->

	<br><br><br><br>

</div>

<!-- === CONFIRM CANCEL MODAL === -->
<div id="confirmCancelModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					<strong>ยืนยันการยกเลิก</strong>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p class="text-danger text-center" style="font-size: 16px;">
					<strong>ต้องการยกเลิกวันหยุดรายการนี้ใช่หรือไม่?</strong>
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary-pz"  onclick="confirmCancelHoliday()" style="width: 80px;">ใช่</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" style="width: 80px;">ไม่ใช่</button>
			</div>
		</div>
	</div>
</div>
<!-- === END CONFIRM CANCEL MODAL === -->