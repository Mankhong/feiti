<br>

<div class="content-panel form-panel text-center">

	<br>
	<h3 class="content-panel-title">
		<?php echo lang('edit_user_heading');?>
	</h3>

	<br>
	<?php echo !empty($message)? $message : ''; ?>
	<?php echo !empty($errorMessage)? $errorMessage : ''; ?>

	<?php echo form_open(uri_string());?>

	<div>
		<?php echo lang('edit_user_fname_label', 'first_name');?> <br />
		<?php echo form_input($first_name);?>
		<span class="text-danger">
				<?php echo form_error('first_name'); ?></span>
	</div>

	<div>
		<?php echo lang('edit_user_lname_label', 'last_name');?> <br />
		<?php echo form_input($last_name);?>
		<span class="text-danger">
				<?php echo form_error('last_name'); ?></span>
	</div>

	<div>
		<?php echo lang('edit_user_company_label', 'company');?> <br />
		<?php echo form_input($company);?>
		<span class="text-danger">
				<?php echo form_error('company'); ?></span>
	</div>

	<div>
		<?php echo lang('edit_user_phone_label', 'phone');?> <br />
		<?php echo form_input($phone);?>
		<span class="text-danger">
				<?php echo form_error('phone'); ?></span>
	</div>

	<div>
		<?php echo lang('edit_user_password_label', 'password');?> <br />
		<?php echo form_input($password);?>
		<span class="text-danger">
				<?php echo form_error('password'); ?></span>
	</div>

	<div>
		<?php echo lang('edit_user_password_confirm_label', 'password_confirm');?><br />
		<?php echo form_input($password_confirm);?>
		<span class="text-danger">
				<?php echo form_error('password_confirm'); ?></span>
	</div>

	<?php if ($this->ion_auth->is_admin() || $this->admin_model->is_sale_admin($this->ion_auth->user()->row()->id) || $this->admin_model->is_personal_admin($this->ion_auth->user()->row()->id)): ?>

      <br><br>
      <h3 class="content-panel-title">
            <?php echo lang('edit_user_groups_heading');?>
	</h3>
      
      <br>
	<?php foreach ($groups as $group):?>
	<label class="checkbox">
		<?php
                  $gID=$group['id'];
                  $checked = null;
                  $item = null;
                  foreach($currentGroups as $grp) {
                      if ($gID == $grp->id) {
                          $checked= ' checked="checked"';
                      break;
                      }
                  }
              ?>
		<input type="checkbox" name="groups[]" class="ml-2" value="<?php echo $group['id'];?>" <?php echo $checked;?>>
		<?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
	</label>
	<?php endforeach?>

	<?php endif ?>

	<?php echo form_hidden('id', $user->id);?>
	<?php echo form_hidden($csrf); ?>

      <br><br><br>
	<p>
		<?php echo form_submit('submit', lang('edit_user_submit_btn'), ['class' => 'btn btn-primary-pz btn-sm']);?>
            <a class="btn btn-default btn-sm" href="<?php echo $back_url; ?>">Back</a>
	</p>

	<?php echo form_close();?>

      <br><br><br>
</div>