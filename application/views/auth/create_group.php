<br>

<div class="content-panel form-panel text-center">

	<br>
	<h3 class="content-panel-title">
		<?php echo lang('create_group_heading');?>
	</h3>

	<br>
	<?php echo !empty($message)? $message : ''; ?>
	<?php echo !empty($errorMessage)? $errorMessage : ''; ?>

	<?php echo form_open("auth/create_group");?>

	<p>
		<?php echo lang('create_group_name_label', 'group_name');?> <br />
		<?php echo form_input($group_name);?>
	</p>

	<p>
		<?php echo lang('create_group_desc_label', 'description');?> <br />
		<?php echo form_input($description);?>
	</p>

      <br><br>
	<p>
		<?php echo form_submit('submit', lang('create_group_submit_btn'), ['class' => 'btn btn-primary-pz btn-sm']);?>
            <a class="btn btn-default btn-sm" href="<?php echo $back_url; ?>">Back</a>
	</p>

	<?php echo form_close();?>

      <br><br><br>
</div>