<link rel="icon" href="<?php echo base_url('assets/images/favicon.ico'); ?>">
<link href="<?=base_url('assets/plugins/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<!-- Font Awesome -->
<link href="<?=base_url('assets/plugins/fontawesome/css/all.min.css')?>" rel="stylesheet">
<!-- App -->
<link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet">
<link href="<?=base_url('assets/css/login.css')?>" rel="stylesheet">

<body class="bg-light">
  <div class="container-fluid px-0 py-0">

    <!-- === LOGIN ROW === -->
    <div class="row m-0">
      <div class="col-12 text-center py-2">
        <br><br>

        <table style="width: 400px; margin: auto;">
          <tr>
            <td style="text-align: center;">
              <h4>FPT</h4>
            </td>
          </tr>
        </table>

        <br>

        <!-- === CARD === -->
        <div class="card m-auto rounded" style="width: 350px;">
          <div class="card-body shadow px-4 py-2">
            <?php echo form_open("auth/login"); ?>

            <div class="login-title py-0">
              <?php echo !empty($message) ? $message : ''; ?>
              <?php echo !empty($errorMessage) ? $errorMessage : ''; ?>
            </div>

            <div class="text-left py-2 mt-3">
              <strong><label>Username</label></strong>
              <?php echo form_input($identity); ?>
              <span class="text-danger">
              <?php echo form_error('identity'); ?></span>
            </div>

            <div class="text-left py-2">
              <strong><label>Password</label></strong>
              <?php echo form_input($password); ?>
              <span class="text-danger">
              <?php echo form_error('password'); ?></span>
            </div>

            <br>
            <div class="text-left py-2">
              <button type="submit" class="btn btn-primary-pz" style="width: 100%; cursor: pointer;">Log in</button>
            </div>
            <div class="text-left py-2">
              <a href="<?=base_url('..')?>"><button type="button" class="btn btn-default" style="width: 100%; cursor: pointer;">เลือกสาขา</button>
            </div>

            <?php echo form_close(); ?>

            <br>
          </div>
        </div>
        <!-- === END CARD === -->

        <div style="margin-top: 20px; font-size: 0.8em; color: #aaa;">
          Supported Browsers: Chrome, Firefox, Safari, Microsoft Edge, IE10+
        </div>

      </div>
    </div>

    <!-- === END LOGIN ROW === -->

  </div>
</body>

<script src="<?=base_url('assets/plugins/jquery/jquery-3.3.1.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/bootstrap/js/popper.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/bootstrap/js/bootstrap.min.js')?>"></script>
<script src="<?=base_url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js');?>"></script>
<!-- Datatables -->
<script src="<?=base_url('assets/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?=base_url('assets/plugins/datatables-plugins/export/dataTables.buttons.min.js');?>"></script>
<script src="<?=base_url('assets/plugins/datatables-plugins/export/jszip.min.js');?>"></script>
<script src="<?=base_url('assets/plugins/datatables-plugins/export/pdfmake.min.js');?>"></script>
<script src="<?=base_url('assets/plugins/datatables-plugins/export/vfs_fonts.js');?>"></script>
<script src="<?=base_url('assets/plugins/datatables-plugins/export/buttons.html5.min.js');?>"></script>
<!-- Confirmation -->
<script src="<?=base_url('assets/plugins/bootstrap/js/bootstrap-confirmation.min.js');?>"></script>
<!-- Font Awesome -->
<script src="<?=base_url('assets/plugins/fontawesome/js/all.min.js');?>"></script>
<!-- App -->
<script src="<?=base_url('assets/js/common.js');?>"></script>
<script src="<?=base_url('assets/js/login.js');?>"></script>