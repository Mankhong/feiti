<br>

<div class="content-panel form-panel text-center">

	<br>
	<h3 class="content-panel-title">
		Deactivate User
	</h3>

	<br><br><br>
	<?php echo !empty($message)? $message : ''; ?>
	<?php echo !empty($errorMessage)? $errorMessage : ''; ?>

	<?php echo form_open("auth/deactivate/".$selected_user->id);?>

	<p>
		Yes
		<input type="radio" name="confirm" value="yes" checked="checked"/>

    &nbsp;

		No
		<input type="radio" name="confirm" value="no" />
	</p>

	<?php echo form_hidden($csrf); ?>
	<?php echo form_hidden(array('id'=>$user->id)); ?>

  <br><br>
	<p>
		<?php echo form_submit('submit', lang('deactivate_submit_btn'), ['class' => 'btn btn-primary-pz btn-sm']);?>
		<a class="btn btn-default btn-sm" href="<?php echo $back_url; ?>">Back</a>
	</p>

	<?php echo form_close();?>

  <br><br>

</div>