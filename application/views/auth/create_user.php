<br>

<div class="content-panel form-panel text-center">

	<br>
	<h3 class="content-panel-title">
		<?php echo lang('create_user_heading');?>
	</h3>

	<br>
	<?php echo !empty($message)? $message : ''; ?>
	<?php echo !empty($errorMessage)? $errorMessage : ''; ?>

	<?php echo form_open("auth/create_user");?>

	<div>
		<?php echo lang('create_user_fname_label', 'first_name');?> <br />
		<?php echo form_input($first_name);?>
            <span class="text-danger">
				<?php echo form_error('first_name'); ?></span>
	</div>

	<div>
		<?php echo lang('create_user_lname_label', 'last_name');?> <br />
		<?php echo form_input($last_name);?>
            <span class="text-danger">
				<?php echo form_error('last_name'); ?></span>
	</div>

	<?php
      if($identity_column!=='email') {
          echo '<p>';
          echo lang('create_user_identity_label', 'identity');
          echo '<br />';
          echo form_error('identity');
          echo form_input($identity);
          echo '</p>';
      }
      ?>

	<div>
		<?php echo lang('create_user_company_label', 'company');?> <br />
		<?php echo form_input($company);?>
            <span class="text-danger">
				<?php echo form_error('company'); ?></span>
	</div>

	<div>
		<?php echo lang('create_user_email_label', 'email');?> <br />
		<?php echo form_input($email);?>
            <span class="text-danger">
				<?php echo form_error('email'); ?></span>
	</div>

	<div>
		<?php echo lang('create_user_phone_label', 'phone');?> <br />
		<?php echo form_input($phone);?>
            <span class="text-danger">
				<?php echo form_error('phone'); ?></span>
	</div>

	<div>
		<?php echo lang('create_user_password_label', 'password');?> <br />
		<?php echo form_input($password);?>
            <span class="text-danger">
				<?php echo form_error('password'); ?></span>
	</div>

	<div>
		<?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
		<?php echo form_input($password_confirm);?>
            <span class="text-danger">
				<?php echo form_error('password_confirm'); ?></span>
	</div>

	<br><br>
	<div>
		<h3 class="content-panel-title">
			<?php echo lang('edit_user_groups_heading');?>
		</h3>
		<br>

		<?php 
			for($i = 0; $i < count($groups); $i++) {
		?>
		<label class="checkbox">
			<?php
				$checked = NULL;
				if (($i+1) == count($groups)) {
					$checked= ' checked="checked"';
				}
			?>
			<input type="checkbox" name="groups[]" class="ml-2" value="<?php echo $groups[$i]['id'];?>" <?php echo $checked;?>>
			<?php echo htmlspecialchars($groups[$i]['name'],ENT_QUOTES,'UTF-8');?>
		</label>
		<?php }?>
	</div>

      <br><br>
	<div>
		<?php echo form_submit('submit', lang('create_user_submit_btn'), ['class' => 'btn btn-primary-pz btn-sm']);?>
            <a class="btn btn-default btn-sm" href="<?php echo $back_url; ?>">Back</a>
	</div>

	<?php echo form_close();?>

      <br><br><br>
</div>