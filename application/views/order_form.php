<div class="content-panel form-panel">
	<div class="row">
		<div class="col-sm-6 text-left">
			<h3 class="content-panel-title">Order</h3>
		</div>
		<div class="col-sm-6 text-right">
			<?php if (!empty($duplicate_url)) { ?>
			<a class="btn btn-default btn-sm" href="<?php echo $duplicate_url; ?>"><i class="far fa-copy"></i> Duplicate Order</a>
			<?php } ?>
		</div>
	</div>

	<?php echo !empty($message)? $message : ''; ?>
	<?php echo !empty($errorMessage)? $errorMessage : ''; ?>

	<?php echo form_open_multipart($submit_url, array('id' => 'form')); ?>

	<!-- === HIDDENS === -->
	<input type="hidden" id="temp_order_id" name="temp_order_id" value="<?php echo empty($temp_order_id) ? '' : $temp_order_id; ?>"/>
	<input type="hidden" id="temp_invoice_type" name="temp_invoice_type" value="<?php echo empty($temp_invoice_type) ? '' : $temp_invoice_type; ?>"/>

	<!-- === ORDER DETAIL === -->

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('customer_id', 'customer_id');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<div class="input-group">
				<?php echo form_input($customer_id); ?>
				<input type="hidden" id="customer_type" name="customer_type" value="<?php echo $customer_type; ?>" />

				<?php if (empty($customer_disabled) || $customer_disabled == FALSE) { ?>
				<button type="button" class="btn btn-default btn-sm ml-1 customerPickerTrigger" style="display: inline-block; padding: 0px 15px;">
					<i class="fas fa-user"></i></button>
				<?php } ?>

			</div>
			<span class="text-danger">
				<?php echo form_error('customer_id'); ?></span>
		</div>
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('customer_name', 'customer_name');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($customer_name); ?>
			<span class="text-danger">
				<?php echo form_error('customer_name'); ?></span>
		</div>
	</div>

	<div class="row d-none">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('order_type', 'order_type');?>
		</div>
		<div class="col-sm-12 col-md-4 pt-2">
			<?php echo form_radio($order_type_value_1); ?>
			<?php echo $order_type_label_1; ?>
			<?php echo form_radio($order_type_value_2); ?>
			<?php echo $order_type_label_2; ?>
			<span class="text-danger">
				<?php echo form_error('order_type'); ?></span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('order_id', 'order_id');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($order_id); ?>
			<span class="text-danger" id="order_id_err">
				<?php echo form_error('order_id'); ?></span>
		</div>

		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('order_date', 'order_date');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($order_date); ?>
			<span class="text-danger">
				<?php echo form_error('order_date'); ?></span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('order_type', 'order_type');?>
		</div>
		<div class="col-sm-12 col-md-4 pt-2">
			<?php echo form_radio($invoice_group_value_1); ?>
				<?php echo $invoice_group_label_1; ?>
				<?php echo form_radio($invoice_group_value_2); ?>
				<?php echo $invoice_group_label_2; ?>
				<span class="text-danger">
					<?php echo form_error('invoice_group'); ?></span>
		</div>

		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('delivery_date', 'delivery_date');?>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($delivery_date); ?>
			<span class="text-danger">
				<?php echo form_error('delivery_date'); ?></span>
		</div>
	</div>

	<div class="row" style="height: 35px;">
		<div class="col-sm-12 col-md-2 border-right">
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_dropdown('invoice_type', $invoice_type_dropdown_data, $invoice_type_dropdown_default, ['class' => 'form-control', 'id' => 'invoice_type']); ?>
			<span class="text-danger">
				<?php echo form_error('invoice_type'); ?></span>
		</div>

		<div class="col-sm-12 col-md-2 border-right">
		</div>
		<div class="col-sm-12 col-md-4">
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
		</div>
		<div class="col-sm-12 col-md-10 pt-2 text-right">
			<?php if(!empty($render_cancel_order_button)) {?>
				<button type="button" class="btn btn-primary-pz" id="cancel_order_button">Cancel Order</button>
			<?php }?>
		</div>
	</div>
	<!-- === END ORDER DETAIL === -->

	<br><br>
	<!-- === PRODUCT LIST === -->
	<ul class="nav nav-tabs" id="myTab" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" id="productList" data-toggle="tab" href="#productListContent" role="tab" aria-controls="productListContent"
			 aria-selected="true">
				<i class="far fa-list-alt"></i> Order Lines</a>
		</li>
		<?php if (!empty($display_delivery_detail) && $display_delivery_detail == TRUE) { ?>
		<li class="nav-item">
			<a class="nav-link" id="deliveryProductList" data-toggle="tab" href="#deliveryProductListContent" role="tab"
			 aria-controls="deliveryProductListContent" aria-selected="false">
				<i class="fas fa-truck"></i> Already Delivery Detail</a>
		</li>
		<?php } ?>
	</ul>
	<div class="tab-content" id="myTabContent">
		<div class="tab-pane active" id="productListContent" role="tabpanel" aria-labelledby="productList">
			<br>
			<table id="productListTable" class="table table-sm table-bordered table-striped">
				<thead>
					<tr>
						<th width="30%" class="text-center">Product</th>
						<th width="13%" class="text-center">Quantity</th>
						<th width="13%" class="text-center">Unit Price</th>
						<th width="13%" class="text-center">Amount</th>
						<th width="13%" class="text-center">Balance</th>
						<th width="10%" class="text-center">Price Type</th>
						<th width="18%" class="text-center">Delete</th>
					</tr>
				</thead>
				<tbody>
					<?php if(!empty($product_list)) { 
					//product_seq|product_id|product_desc|quantity|unit_price|amount|balance|invoice_quantity|dataitem_type|price_flag|invoice_type_name|invoice_type_phase

					for ($i = 0; $i < count($product_list); $i++) {
						$product_seq = Common::encodeString($product_list[$i]['product_seq']);
						$product_id = Common::encodeString($product_list[$i]['product_id']);

						echo '<tr class="dataitem">' .
						'<td>' .
						'<input type="hidden" class="dt-product-seq" value="' . $product_seq . '"/>' .
						'<input type="hidden" class="dt-product-id" value="' . $product_id . '"/>' . 
						'<input type="hidden" class="dt-product-desc" value="' . $product_list[$i]['desc1'] . '"/>' .
						'<input type="hidden" class="dt-invoice-quantity" value="' . $product_list[$i]['invoice_quantity'] . '"/>' . 
						'<input type="hidden" class="dt-dataitem-type" name="dataitem_type" value="DATA"/>' .
						'<input type="hidden" class="dt-price-flag" value="' . $product_list[$i]['price_flag'] . '"/>' . 
						'<input type="hidden" class="dt-invoice-type-name" value="' . $product_list[$i]['invoice_type_name'] . '"/>' . 
						'<input type="hidden" class="dt-invoice-type-phase" value="' . $product_list[$i]['invoice_type_phase'] . '"/>' . 
						'<span class="dts-product-id">' . Common::decodeString($product_id) . '</span>' . 
						'</td>' .
						'<td class="text-right">' .
						'<div class="dt-quantity pz-input-editor" data-id="' . $product_id . '" data-type="currency" data-decimal="0">' . $product_list[$i]['quantity'] . '</div>' . 
						'<input type="number" class="dt-quantity-input pz-input-editor" data-id="' . $product_id . '"/>' . 
						'</td>' .
						'<td class="text-right">' . 
						'<div class="dt-unit-price pz-input-editor" data-id="' . $product_id . '_np" data-type="currency" data-decimal="4">' . $product_list[$i]['unit_price'] . '</div>' .
						'<input type="number" class="dt-unit-price-input pz-input-editor" data-id="' . $product_id . '_np" data-product-id="' . $product_id . '"/>' .
						'</td>' .
						'<td class="text-right">' .
						'<span class="dt-amount"></span>' .
						'</td>' .
						'<td class="text-right">' . 
						'<span class="dt-balance"></span>' .
						'</td>' .
						'<td class="text-left">' . 
						'<span class="dts-price-flag">' . ($product_list[$i]['price_flag'] == '1' ? 'Local Price' : ($product_list[$i]['price_flag'] == '2' ? 'BOI Price' : '')) . '</span>' .
						'</td>' .
						'<td class="text-center">' .
						'<a class="btn btn-default btn-sm btn-icon" onclick="confirmDeleteProductDataitem(\'' . $product_id . '\')"><i class="fas fa-times fa-sm"></i></a>' .
						'</td>' .
						'</tr>';
					}
				}?>

					<?php if(!empty($new_dataitems)) { 
					//product_seq|product_id|product_desc|quantity|unit_price|amount|balance|invoice_quantity|dataitem_type|price_flag|invoice_type_name|invoice_type_phase
					$delimiter = '&88&';
					for ($i = 0; $i < count($new_dataitems); $i++) {
						$values = explode($delimiter, $new_dataitems[$i]);

						echo '<tr class="dataitem">' .
						'<td>' .
						'<input type="hidden" class="dt-product-seq" value="' . $values[0] . '"/>' .
						'<input type="hidden" class="dt-product-id" value="' . $values[1] . '"/>' . 
						'<input type="hidden" class="dt-product-desc" value="' . $values[2] . '"/>' .
						'<input type="hidden" class="dt-invoice-quantity" value="' . $values[7] . '"/>' . 
						'<input type="hidden" class="dt-dataitem-type" name="dataitem_type" value="' . $values[8] . '"/>' .
						'<input type="hidden" class="dt-price-flag" value="' . $values[9] . '"/>' . 
						'<input type="hidden" class="dt-invoice-type-name" value="' . $values[10] . '"/>' . 
						'<input type="hidden" class="dt-invoice-type-phase" value="' . $values[11] . '"/>' . 
						'<span class="dts-product-id">' . Common::decodeString($values[1]) . '</span>' . 
						'</td>' .
						'<td class="text-right">' .
						'<div class="dt-quantity pz-input-editor" data-id="' . $values[1] . '" data-type="currency" data-decimal="0">' . $values[3] . '</div>' . 
						'<input type="number" class="dt-quantity-input pz-input-editor" data-id="' . $values[1] . '"/>' . 
						'</td>' .
						'<td class="text-right">' . 
						'<div class="dt-unit-price pz-input-editor" data-id="' . $values[1] . '_np" data-type="currency" data-decimal="4">' . $values[4] . '</div>' .
						'<input type="number" class="dt-unit-price-input pz-input-editor" data-id="' . $values[1] . '_np" data-product-id="' . $values[1] . '"/>' .
						'</td>' .
						'<td class="text-right">' .
						'<span class="dt-amount">' . $values[5] . '</span>' .
						'</td>' .
						'<td class="text-right">' . 
						'<span class="dt-balance">' . $values[6] . '</span>' .
						'</td>' .
						'<td class="text-left">' . 
						'<span class="dts-price-flag">' . ($values[9] == '1' ? 'Local Price' : ($values[9] == '2' ? 'BOI Price' : '')) . '</span>' .
						'</td>' .
						'<td class="text-center">' .
						'<a class="btn btn-default btn-sm btn-icon" onclick="confirmDeleteProductDataitem(\'' . $values[1] . '\')"><i class="fas fa-times fa-sm"></i></a>' .
						'</td>' .
						'</tr>';
					}
				}?>
				</tbody>
			</table>

			<div id="summary_container" style="font-size: 12px; text-align: right;">
				<table style="width: 240px; float: right;">
					<tr>
						<td style="width: 50%; text-align: right; font-weight: bold;">
							Amount :
						</td>
						<td style="width: 50%;">
							<span id="sm_amount"></span>
						</td>
					</tr>
					<tr>
						<td style="width: 50%; text-align: right; font-weight: bold;">
							Vat <span id="sm_vat">
								<?php echo $vat; ?></span>% :
						</td>
						<td style="width: 50%;">
							<span id="sm_vat_price"></span>
						</td>
					</tr>
					<tr>
						<td style="width: 50%; text-align: right; font-weight: bold;">
							Total :
						</td>
						<td style="width: 50%;">
							<span id="sm_total"></span>
						</td>
					</tr>
				</table>
			</div>
		</div>

		<!-- === TAB DELIVERY PRODUCTS === -->
		<div class="tab-pane" id="deliveryProductListContent" role="tabpanel" aria-labelledby="deliveryProductList">
			<br>
			<table id="deliveryProductsTable" class="table table-sm table-bordered table-striped">
				<thead>
					<tr>
						<th width="20%" class="text-center">Product ID</th>
						<th width="16%" class="text-center">Order Date</th>
						<th width="16%" class="text-center">Order ID</th>
						<th width="16%" class="text-center">Quantity</th>
						<th width="16%" class="text-center">Unit Price</th>
						<th width="16%" class="text-center">Amount</th>
					</tr>
				</thead>
			</table>
		</div>

		<br>
		<button type="button" class="btn btn-default btn-sm productPickerTrigger">Add</button>

		<!-- === HIDDENS CONTAINER === -->
		<span id="dataitem_hidden_container">
			<?php if(!empty($new_dataitems)) { 
			//product_seq|product_id|product_desc|quantity|unit_price|amount|balance|invoice_quantity|dataitem_type|price_flag|invoice_type_name|invoice_type_phase
			$delimiter = '&88&';
			for ($i = 0; $i < count($new_dataitems); $i++) {
				$values = explode($delimiter, $new_dataitems[$i]);
				echo '<input type="hidden" class="dataitem-hidden" name="new_dataitems[]" data-product-id="' . $values[1] . '" dataitem-type="' . $values[8] . '" value="' . $new_dataitems[$i] . '"/>';
			}
		}?>

			<?php if(!empty($delete_dataitems)) { 
			$delimiter = '&88&';
			for ($i = 0; $i < count($delete_dataitems); $i++) {
				$values = explode($delimiter, $delete_dataitems[$i]);
				echo '<input type="hidden" class="session-delete-hiddens" data-product-id="' . $values[1] . '" dataitem-type="' . $values[8] . '" value="' . $delete_dataitems[$i] . '"/>';
			}
		}?>
		</span>

		<?php echo form_close(); ?>

		<br><br><br><br>

	</div>
	<!-- === END PRODUCT LIST === -->

</div>

<!-- === CUSTOMER MODAL === -->
<div id="customerPickerModal" class="modal contact-modal form-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					<strong>
						<h5>Select Customer</h5>
					</strong>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<table id="customerPickerTable" class="table table-sm table-bordered table-striped">
					<thead>
						<tr>
							<th width="10%" class="text-center">No.</th>
							<th width="15%" class="text-center">Customer ID</th>
							<th width="45%" class="text-center">Customer Name</th>
							<th width="15%" class="text-center">Customer Type</th>
							<th width="15%" class="text-center">Business Type</th>
						</tr>
					</thead>
				</table>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- === END CUSTOMER MODAL === -->

<!-- === PRODUCT MODAL === -->
<div id="productPickerModal" class="modal contact-modal form-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					<strong>
						<h5>Select Product</h5>
					</strong>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<table id="productPickerTable" class="table table-sm table-bordered table-striped" style="width: 100%;">
					<thead>
						<tr>
							<th width="7%" class="text-center">No.</th>
							<th width="32%" class="text-center">Product ID/Part ID</th>
							<th width="25%" class="text-center">Description 1/Part Name</th>
							<th width="18%" class="text-center">Unit Price</th>
							<th width="18%" class="text-center">Price Type</th>
						</tr>
					</thead>
				</table>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- === END PRODUCT MODAL === -->

<!-- === PRODUCT QUANTITY MODAL === -->
<div id="productQuantityModal" class="modal contact-modal form-modal" style="padding-top: 100px;" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					<strong>
						<h5>Quantity</h5>
					</strong>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<div class="input-group">
					<input type="number" class="form-control" id="productQuantity" />
					<button type="button" class="btn btn-default btn-sm" id="prodcutQuantityButton">Enter</button>
				</div>
				<span class="text-danger ml-1" id="productQuantityError"></span>
			</div>

			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
<!-- === END PRODUCT QUANTITY MODAL === -->

<!-- === CONFIRM DELETE MODAL === -->
<div id="confirmDeleteModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					<strong><span id="msgModalTitle">Confirm</span></strong>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>
					Do you want to delete this item?
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary-pz" id="confirmDeleteButton">Yes</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<!-- === END CONFIRM DELETE MODAL === -->

<!-- === CONFIRM CANCEL MODAL === -->
<div id="confirmCancelModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					<strong>ยืนยันการยกเลิก</strong>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p class="text-danger text-center" style="font-size: 16px;">
					<strong>ต้องการยกเลิก Order นี้ใช่หรือไม่?</strong>
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary-pz"  onclick="confirmCancelOrder()" style="width: 80px;">ใช่</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" style="width: 80px;">ไม่ใช่</button>
			</div>
		</div>
	</div>
</div>
<!-- === END CONFIRM CANCEL MODAL === -->