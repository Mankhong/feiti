<style>
    #xx.item:empty:before {
        content: "\200b"; 
    }
    @page{
        margin: 0cm;
        padding-left: 2cm;
    }

    .table-bordered {
        border: 1px solid #000000;
    }
    .table-bordered th, .table-bordered td{
        border: 1px solid #000000;
    }
    .table-bordered thead td {
        border-bottom-width: 1px;
    }
    .setfont{
        font-size: 12px;
    }
    .table-font{
        font-size: 19px;
        font-family: 'Angsana New';
    }

    tbody tr td{
        font-size: 14px;
        font-family: 'Angsana New';
    }

</style>

<div class="container">
    <!--<div class="printableArea">-->
    <input type="hidden" name="billing_id" id="billing_id" value="<?= $billing_id ?>">
    <input type="hidden" name="receipt_date" id="receipt_date" value="<?= $receipt_date ?>">

    <input type="hidden" name="id_customer_name" id="id_customer_name" value="<?= $customer_name ?>">
    <input type="hidden" name="id_customer_address" id="id_customer_address" value="<?= $customer_address ?>">

    <div class="row pt-2">
        <div class="col-12">
            <div class="row">
                <div class="col-2 text-right">
                    <image src="<?= base_url('assets/images/logo.png') ?>" style="width: 100px; height: 100px;">
                </div>
                <div class="col-10" style="padding-left: 1.0rem !important;">
                    <div class="invoice-title">
                        <div>
                            <h3>บริษัท เฟยตี้ พรีซิชั่น (ไทยแลนด์) จำกัด<br>FEITI PRECISION (THAILAND)CO.,LTD</h3>
                            <hr style="margin-top: 0px;margin-bottom: 15px;">
                            <small>
                                1/92 หมู่ 5 สวนอุตสาหกรรมโรจนะ ต.คานหาม อ.อุทัย จ.พระนครศรีอยุธยา &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>โทร.</strong> &nbsp;:(035) 227916-9 <strong>โทรสาร</strong> (035) 226148
                                <br>
                                1/92 MOO 5 ROJANA INDUSTRIAL PARK TAMBOL KANHAM AMPHUR U-THAI AYUTTHAYA 13210 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>TEL.</strong> &nbsp;:(035) 227916-9 <strong>FAX</strong> : (035) 226148
                            </small>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row pt-4">
                <div class="col-2">
                    <address>
                        <span>สำนักงานใหญ่</span>
                    </address>
                </div>
                <div class="col-3">
                </div>
                <div class="col-2 text-center">
                    <address>
                        <strong>ใบวางบิล</strong><br>
                        <strong>BILLING NOTE</strong><br>   
                    </address>
                </div>
                <div class="col-5">
                </div>
            </div>

            <div class="row pt-3">
                <div class="col-2 text-left"  style="width: 150px !important; flex: 0 0 11.666667%;">
                    <strong>เลขที่บิล: </strong> <br> <strong>No: </strong> 
                </div>
                <div class="col-1 pt-2"> <span><?= $billing_id ?></span> </div>
                <div class="col-5"></div>
                <div class="col-3">
                    <div style="display: inline-block;">
                        <strong>เลขประจำตัวผู้เสียภาษีอากร: </strong> 
                        <br> <strong>Tax Payer I.D. No.: </strong>  
                    </div>
                </div>
                <div class="col-1 pt-2 text-left"><span>0145547001421</span></div>
            </div>

            <div class="row pt-3">
                <div class="col-2 text-left" style="width: 150px !important; flex: 0 0 11.666667%;">
                    <strong>นามลูกค้า:</strong> <br> <strong>Customer: </strong> 
                </div>
                <div class="col-6 pt-2 text-left"> <span><?= $customer_name ?></span> </div>

                <div class="col-1">
                    <div style="display: inline-block;">
                        <strong>วันที่: </strong> 
                        <br> <strong>Date: </strong>  
                    </div>
                </div>
                <div class="col-2 pt-2 text-left"><span id="html_receipt_date"></span></div>
            </div>

            <div class="row pt-3">
                <div class="col-2 text-left" style="width: 150px !important; flex: 0 0 11.666667%;">
                    <strong>ที่อยู่ลูกค้า: </strong> <br> <strong>Address: </strong> 
                </div>
                <div class="col-6 pt-0 text-left"> <span id="spancusAddress"></span> </div>
                <div class="col-2"></div>

            </div>




            <div class="row pt-3">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="table-responsive">
                            <table class="table table-condensed table-bordered table-font" id="rstable">
                                <thead>
                                    <tr>
                                        <td class="text-center" style="width:7%;"><strong>Item<br>ลำดับ</strong></td>
                                        <td class="text-center" style="width:13%;"><strong>Invoice No.<br>เลขที่ใบสั่งของ</strong></td>
                                        <td class="text-center" style="width:13%;"><strong>Date<br>วันที่</strong></td>
                                        <td class="text-center" style="width:13%;"><strong>Total<br>จำนวนเงิน</strong></td>
                                        <td class="text-center" style="width:13%;"><strong>Grand Total(VAT 7%)<br>จำนวนเงินรวม VAT</strong></td>
                                    </tr>
                                </thead>

                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row" pb-4>
                <div class="col-6 ">
                    <div class="panel-body table-font" style="border: 1px solid #000000;">
                        <div>  
                            <div style="display: inline-block"></div><span style="padding-left: 20px;">นัดรับเงินวันที่</span>
                            <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 10%; display: inline-block; "></div>/
                            <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 10%; display: inline-block; "></div>/
                            <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 10%; display: inline-block; "></div>
                        </div>
                        <div class="text-center">  
                            <div style="display: inline-block"></div><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 37%; display: inline-block; "></div>       
                        </div>
                        <div class="text-center">  
                            <div style="display: inline-block"></div><span style="padding-left: 21px;">Receiver/ผู้รับบิล</span>
                        </div>
                        <div class="text-center">  
                            Date:<div style="border-bottom: 1px dashed #000;text-decoration: none; width: 10%; display: inline-block; "></div>/
                            <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 10%; display: inline-block; "></div>/
                            <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 10%; display: inline-block; "></div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="panel-body table-font" style="border: 1px solid #000000;">
                        <div>  
                            <div style="display: inline-block"></div><span style="padding-left: 20px;">ผู้วางบิล</span>
                        </div>
                        <div class="text-center">  
                            <div style="display: inline-block"></div><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 37%; display: inline-block; "></div>       
                        </div>
                        <div class="text-center">  
                            <div style="display: inline-block"></div><span style="padding-left: 21px;">Authorizied Signature/ลายเซ็นผู้ได้รับมอบอำนาจ</span>
                        </div>
                        <div class="text-center">  
                            Date:<div style="border-bottom: 1px dashed #000;text-decoration: none; width: 10%; display: inline-block; "></div>/
                            <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 10%; display: inline-block; "></div>/
                            <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 10%; display: inline-block; "></div>
                        </div>

                    </div>
                </div>
            </div>
            <hr>
            <div class="row pt-3">
                <br><br>
                <div class="col-md-12 text-center"> 
                    <!--<button type="button" class="btn btn-success" id="btn_print" >Print</button>-->
                </div>

            </div>

            <div class="container" style="display: none;" id="contentdiv">
                <div class="printableArea" id="printableAreatest">
                    <style type="text/css">
                        @font-face {
                            font-family: 'Angsana New';
                            src: url('<?php echo base_url('assets/font/angsa.ttf')?>') format('ttf');
                            font-weight: normal;
                            font-style: normal;
                        }

                        .result-table tbody td {
                            padding-top: 0px !important;
                            padding-bottom: 0px !important;
                        }
                    </style>
                    <div id="content"></div>
                </div>
            </div>
        </div>
    </div>
    <!--</div>-->
</div>
