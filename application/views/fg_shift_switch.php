<div class="small-topbar criteria-container">
	<br>

	<div class="row">
		<div class="col-sm-12 col-md-4">
			<div class="label label-width-1">
				Date :
			</div>
			<div class="input">
				<input type="text" class="form-control datepicker text-center" style="width: 200px;" id="ctrDate" readonly/>
			</div>
		</div>
	</div>

	<div class="row mt-2">
		<div class="col-sm-12 col-md-6">
			<div class="label label-width-1">
				Shift A :
			</div>
			<div class="input">
				<input type="radio" name="ctrShiftA" value="D" checked/>
				Day
				<input type="radio" name="ctrShiftA" value="N" class="ml-2"/>
				Night
			</div>
		</div>
	</div>

	<div class="row mt-2">
		<div class="col-sm-12 col-md-6">
			<div class="label label-width-1">
				Shift B :
			</div>
			<div class="input">
				<input type="radio" name="ctrShiftB" value="D" checked/>
				Day
				<input type="radio" name="ctrShiftB" value="N" class="ml-2"/>
				Night
			</div>
		</div>
	</div>

	<br>

	<div class="row pt-2">
		<div class="col-sm-12 col-md-7 text-left">
			<button type="button" id="btnAdd" class="btn btn-primary-pz btn-sm"><i class="fas fa-plus"></i> Add</button>
			<button type="button" id="btnClear" class="btn btn-default btn-sm">Clear</button>
		</div>
	</div>

	<br>
</div>

<?php echo !empty($message)? $message : ''; ?>
<?php echo !empty($errorMessage)? $errorMessage : ''; ?>

<div class="text-right px-3 pt-3 pb-3 d-none">
	<span id="export_buttons_container"></span>
</div>

<div id="resultContainer" style="display: block; width: 100%; margin-top: -7px;">
	<table id="rstable" class="table table-sm table-bordered table-striped">
		<thead>
			<tr>
				<th style="width: 10%;" class="text-center no-sort" rowspan="2">No.</th>
				<th style="width: 20%;"  class="text-center" rowspan="2">Date</th>
				<th style="width: 40%;" class="text-center" colspan="2">Shift</th>
				<th style="width: 10%;"  class="text-center" rowspan="2">Last Update Date</th>
				<th style="width: 10%;"  class="text-center" rowspan="2">Last Update User</th>
				<th style="width: 10%;"  class="text-center" rowspan="2">Edit</th>
			</tr>
			<tr>
				<th style="width: 20%;"  class="text-center">A</th>
				<th style="width: 20%;"  class="text-center border-right">B</th>
			</tr>
		</thead>
	</table>
	
	<br><br><br><br>
</div>

<!-- === EDITOR MODAL === -->
<div id="editorModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					<strong>Modify Shift Switch<span id="editorModalTitle"></span></strong>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
				<div class="label-input-form">
					<div class="row">
						<div class="col-6">
							<div class="label label-width-1 border-right">
								No.
								<input type="hidden" id="edtId"/>
							</div>
							<div class="input">
								<span id="edtNo"></span>
							</div>
						</div>
					</div>

					<div class="row mt-2">
						<div class="col-6">
							<div class="label label-width-1 border-right">
								Date
							</div>
							<div class="input">
								<input type="text" class="form-control datepicker" id="edtDate" readonly/>
							</div>
						</div>
					</div>

					<div class="row mt-2">
						<div class="col-sm-12 col-md-6">
							<div class="label label-width-1 border-right">
								A :
							</div>
							<div class="input">
								<input type="radio" name="edtShiftA" value="D" checked/>
								Day
								<input type="radio" name="edtShiftA" value="N" class="ml-2"/>
								Night
							</div>
						</div>
					</div>

					<div class="row mt-2">
						<div class="col-sm-12 col-md-6">
							<div class="label label-width-1 border-right">
								B :
							</div>
							<div class="input">
								<input type="radio" name="edtShiftB" value="D" checked/>
								Day
								<input type="radio" name="edtShiftB" value="N" class="ml-2"/>
								Night
							</div>
						</div>
					</div>

				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary-pz" id="btnEditorSave">Save</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- === END EDITOR MODAL === -->