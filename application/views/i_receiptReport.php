<style>
    #xx.item:empty:before {
        content: "\200b"; 
    }
    @page{
        margin: 0cm;
    }

    .table-bordered {
        border: 1px solid #000000;
    }
    .table-bordered th, .table-bordered td{
        border: 1px solid #000000;
    }
    .table-bordered thead td {
        border-bottom-width: 1px;
    }
    .setfont{
        font-size: 12px;
    }
    .table-font{
        font-size: 19px;
        font-family: 'Angsana New';
    }

    tbody tr td{
        font-size: 14px;
        font-family: 'Angsana New';
    }

</style>

<div class="container">
    <!--<div class="printableArea">-->
    <input type="hidden" name="billing_id" id="billing_id" value="<?= $billing_id ?>">
    <input type="hidden" name="receipt_date" id="receipt_date" value="<?= $receipt_date ?>">

    <input type="hidden" name="id_customer_name" id="id_customer_name" value="<?= $customer_name ?>">
    <input type="hidden" name="id_customer_address" id="id_customer_address" value="<?= $customer_address ?>">

    <input type="hidden" name="invoice_ids" id="invoice_ids" />
    <input type="hidden" name="total" id="total" />

    <div class="row pt-2">
        <div class="col-12">
            <div class="row">
                <div class="col-2">
                    <image src="<?= base_url('assets/images/default_image.jpg') ?>" style="width: 140px; height: 140px;">
                </div>
                <div class="col-10" style="padding-left: 1.0rem !important;">
                    <div class="invoice-title">
                        <div>
                            <h3>บริษัท เฟยตี้ พรีซิชั่น (ไทยแลนด์) จำกัด<br>FEITI PRECISION (THAILAND)CO.,LTD</h3>
                            <hr style="margin-top: 0px;margin-bottom: 15px;">
                            <small>
                                1/92 หมู่ 5 สวนอุตสาหกรรมโรจนะ ต.คานหาม อ.อุทัย จ.พระนครศรีอยุธยา <strong>โทร.</strong> &nbsp;:(035) 227916-9 <strong>โทรสาร</strong> (035) 226148
                                <br>
                                1/92 MOO 5 ROJANA INDUSTRIAL PARK TAMBOL KANHAM AMPHUR U-THAI AYUTTHAYA 13210  <strong>TEL.</strong> &nbsp;:(035) 227916-9 <strong>FAX</strong> : (035) 226148
                            </small>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-2">

                </div>
                <div class="col-5">
                    <span>เลขประจำตัวผู้เสียภาษี: 0145547001421</span>
                </div>
                <div class="col-5">
                    <span>Tax Payer ID No: 0145547001421</span>
                </div>
            </div>

            <div class="row pt-4">
                <div class="col-2">

                </div>
                <div class="col-3">
                </div>
                <div class="col-2 text-center">
                    <address>
                        <strong>ใบเสร็จรับเงิน</strong><br>
                        <strong>RECEIPT</strong><br>   
                    </address>
                </div>
                <div class="col-5">
                </div>
            </div>

            <div class="row pt-3">
                <div class="col-2 text-left"  style="width: 150px !important; flex: 0 0 11.666667%;">
                    <strong>วันที่: </strong> <br> <strong>DATE: </strong> 
                </div>
                <div class="col-2 pt-2"> <span id="html_receipt_date"></span> </div>
                <div class="col-4"></div>
                <div class="col-3">
                    <div style="display: inline-block;">
                        <strong>RECEIPT NO: </strong> 
                        <br> <strong></strong>  
                    </div>
                </div>
                <div class="col-1 pt-2 text-left"><span id="receipt_id"></span></div>
            </div>

            <div class="row pt-3">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="table-responsive">
                            <table class="table table-condensed table-bordered table-font" style="border: 1px; overflow: hidden;">
                                <thead>
                                    <tr>
                                        <td class="text-center" colspan="2" style="width:7%;">
                                            <div class="row">
                                                <div class="col-2">
                                                    <strong>CUSTOMER<br>
                                                        ชื่อลูกค้า
                                                    </strong>
                                                </div>
                                                <div class="col-8">
                                                    <span><?= $customer_name ?></span>
                                                </div>

                                            </div>

                                        </td>
                                        <td class="text-center" colspan="2" style="width:13%;">

                                            <div class="row">
                                                <div class="col-2">
                                                    <strong>ADDRESS.<br>
                                                        ที่อยู่
                                                    </strong>
                                                </div>
                                                <div class="col-8 text-left">
                                                    <span id="spancusAddress"></span>
                                                </div>

                                            </div>
                                        </td>
                                    </tr>
                                </thead>

                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="table-responsive">
                            <table class="table table-condensed table-bordered table-font" id="rstable" style="width: 100%">
                                <thead>
                                    <tr>
                                        <td class="text-center" style="width:18%;"><strong>DESCRIPTION.<br>รายการ</strong></td>
                                        <td class="text-center" style="width:7%;"><strong>AMOUNT(BATH)<br>จำนวนเงิน</strong></td>
                                    </tr>
                                </thead>
                                <tbody id="id-tbody"></tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="panel-body table-font" style="border: 1px solid #000000;">

                        <div align="center">
                            <address>
                                <span>Pleas issue crossed cheque to FEITI PRECISION (THAILAND) CO.,LTD</span><br>
                                <span>กรุณาสั่งจ่ายเช็คขีดคร่อมในนาม บริษัทเฟยตี้ พรีซิชั่น(ไทยแลนด์) จำกัด</span><br>   
                                <span>FOR FEITI PRECISION (THAILAND) CO.,LTD</span><br>
                                <span>ในนาม บริษัทเฟยตี้ พรีซิชั่น(ไทยแลนด์) จำกัด</span><br>
                            </address>
                        </div>

                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="row" pb-4>
                <div class="col-4">
                    <div align="center">
                        <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"></div>
                        <div><span>กรรมการผู้จัดการ</span></div>       
                    </div>
                </div>
                <div class="col-4">
                    <div align="center">
                        <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"></div>
                        <div><span>ฝ่ายการเงิน</span></div>       
                    </div>
                </div>
                <div class="col-4">
                    <div align="center">
                        <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"></div>
                        <div><span>ผู้รับเช็ค</span></div>       
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="row" pb-4>
                <div class="col-2"></div>
                <div class="col-3">
                    <div align="center">
                        <div><span>สีขาว Customer/สำหรับลูกค้า</span></div>       
                    </div>
                </div>
                <div class="col-3">
                    <div align="center">
                       <div><span>สีฟ้า Markekting/การตลาด</span></div>       
                    </div>
                </div>
                <div class="col-3">
                    <div align="center">
                        <div><span>สีเขียว ACCOUNT/บัญชี</span></div>       
                    </div>
                </div>
                <div class="col-2"></div>
            </div>
            <hr>
            <div class="row pt-3">
                <br><br>
                <div class="col-md-12 text-center"> 
                    <!--<button type="button" class="btn btn-success" id="btn_print" >Print</button>-->
                </div>

            </div>

            <div class="container" style="display: none;" id="contentdiv">
                <div class="printableArea" id="printableAreatest">
                    <style type="text/css">
                        @font-face {
                            font-family: 'Angsana New';
                            src: url('<?php echo base_url('assets/font/angsa.ttf')?>') format('ttf');
                            font-weight: normal;
                            font-style: normal;
                        }

                        .printableArea {
                            font-family: 'Angsana New Bold';
                        }

                        .printableArea #body_content {
                            font-size: 28px;
                        }

                        .printableArea table td {
                            border: none !important;
                        }
                    </style>
                    <div id="content"></div>
                </div>
            </div>
        </div>
    </div>
    <!--</div>-->
</div>
