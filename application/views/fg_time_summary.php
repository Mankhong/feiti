<div class="small-topbar criteria-container">
	<div class="row">
		<div class="col-sm-12 col-md-4">
			<div class="label label-width-1">
				Year :
			</div>
			<div class="input">
				<select id="ctrYear" class="form-control" style="width: 220px;">
					<?php 
						$current_year = date('Y');
						$current_month = date('m');
						$current_date =  date('d');

						if ($current_month == 12) {
							$current_year += 1;
						}
						
						$year_offset = 5;
						
						for ($i = $current_year; $i > ($current_year - $year_offset); $i--) {
							echo '<option value="' . $i . '">' . $i . '</option>';
						}
					?>
				</select>
			</div>
		</div>
		<div class="col-sm-12 col-md-4">
			<div class="label label-width-1">
				Month :
			</div>
			<div class="input">
				<select id="ctrMonth" class="form-control" style="width: 220px;">
					<option value="">-- All --</option>
					<option value="1">January</option>
					<option value="2">Febuary</option>
					<option value="3">March</option>
					<option value="4">April</option>
					<option value="5">May</option>
					<option value="6">June</option>
					<option value="7">July</option>
					<option value="8">August</option>
					<option value="9">September</option>
					<option value="10">October</option>
					<option value="11">November</option>
					<option value="12">December</option>
				</select>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-4">
			<div class="label label-width-1">
				Department :
			</div>
			<div class="input">
				<?php echo form_dropdown('ctrDepartment', $department_dropdown_data, $department_dropdown_default, ['class' => 'form-control', 'id' => 'ctrDepartment', 'style' => 'width: 220px;']); ?>
			</div>
		</div>
		<div class="col-sm-12 col-md-4">
			<div class="label label-width-1">
				Employee Type :
			</div>
			<div class="input">
				<?php echo form_dropdown('ctrEmployeeType', $employee_type_dropdown_data, $employee_type_dropdown_default, ['class' => 'form-control', 'id' => 'ctrEmployeeType', 'style' => 'width: 220px;']); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 col-md-4">
			<div class="label label-width-1">
				Employee No. :
			</div>
			<div class="input">
				<input type="text" id="ctrEmployeeNo" class="form-control" style="width: 220px;"/>
			</div>
		</div>
		<div class="col-sm-12 col-md-4">
		</div>
	</div>

	<div class="row pt-2">
		<div class="col-sm-12 col-md-7 text-left">
			<button type="button" id="btnSearch" class="btn btn-primary-pz btn-sm"><i class="fas fa-search"></i> Search</button>
			<button type="button" id="btnClear" class="btn btn-default btn-sm">Clear</button>
		</div>
	</div>
</div>

<?php echo !empty($message)? $message : ''; ?>
<?php echo !empty($errorMessage)? $errorMessage : ''; ?>

<div id="resultContainer" style="display: none; width: 100%;">
	<div class="text-right px-3 pt-1 pb-1">
		<span id="export_buttons_container">
			<?php echo !empty($export_ot_excel) ? $export_ot_excel : ''; ?>
			<?php echo !empty($export_excel) ? $export_excel : ''; ?>
		</span>
	</div>

    <table id="rstable" class="table-sm tb-timesheet" style="width: 100%">
		<thead>
			<tr>
				<th width="50" class="text-center no-sort" rowspan="2">No.</th>
				<th width="100" class="text-center" rowspan="2">Code Department</th>
				<th width="100" class="text-center" rowspan="2">ID No.</th>
				<th class="text-center" rowspan="2"><div style="width: 150px;">Name-Surname</div></th>
				<th class="text-center" rowspan="2"><div style="width: 90px;">Entry Date<div></th>
				<th width="100" class="text-center" rowspan="2"><div style="width: 120px;">Section</div></th>
				<th width="100" class="text-center" rowspan="2"><div style="width: 120px;">Position</div></th>
				<th class="text-center" colspan="3"><div style="width: 227px;">Working Day</div></th>
				<th class="text-center" colspan="4">OverTime</th>
				<th width="100" class="text-center diligence-col" rowspan="2">Diligence <span id="thMonthBefore"></span></th>
				<th width="100" class="text-center diligence-col" rowspan="2">Diligence <span id="thMonth"></span></th>
				<th class="text-center" colspan="17">Summary Leave of Month</th>
			</tr>
			<tr>
				<th class="text-center"><div style="width: 70px;">Working Day 100%</div></th>
				<th class="text-center"><div style="width: 70px;">Night Shift</div></th>
				<th class="text-center" style="border-right: 1px solid #edd;"><div style="width: 70px;">Food, Travel Day</div></th>

				<th width="50" class="text-center">1.00</th>
				<th width="50" class="text-center">1.50</th>
				<th width="50" class="text-center">2.00</th>
				<th width="50" class="text-center" style="border-right: 1px solid #edd;">3.00</th>

				<th width="40" class="text-center">SL(H)</th>
				<th width="40" class="text-center">SL</th>
				<th width="40" class="text-center">Total SL</th>
				<th width="40" class="text-center">AL</th>
				<th width="40" class="text-center">AB</th>
				<th width="40" class="text-center">CO</th>
				<th width="40" class="text-center">ASL</th>
				<th width="40" class="text-center">DFL</th>
				<th width="40" class="text-center">MRL</th>
				<th width="40" class="text-center">MA</th>
				<th width="40" class="text-center">ML</th>
				<th width="40" class="text-center">RL</th>
				<th width="40" class="text-center">CL</th>
				<th width="40" class="text-center">CL(H)</th>
				<th width="40" class="text-center">Warning Letter</th>
				<th width="40" class="text-center">Late Times</th>
				<th width="40" class="text-center">Late Minute</th>
			</tr>
		</thead>
	</table>
	
	<br><br><br><br>
</div>

