<?php echo !empty($message)? $message : ''; ?>
<?php echo !empty($errorMessage)? $errorMessage : ''; ?>

<div class="px-3 pt-1">
	<div class="row">
		<div class="col-6 pt-2">
			<div class="input-group">
				<input type="radio" name="resign_flag" id="resign_flag_1" value="N" checked/>
				<label for="resign_flag_1"class="resign-flag-label ml-2">พนักงานที่ยังไม่ลาออก</label>
				<input type="radio" class="ml-4" name="resign_flag" id="resign_flag_2" value="Y"/>
				<label for="resign_flag_2" class="resign-flag-label ml-2">พนักงานที่ลาออกแล้ว</label>
			</div>
		</div>
		<div class="col-6 text-right">
			<span id="export_buttons_container">
				<?php echo !empty($export_excel) ? $export_excel : ''; ?>
			</span>
			<a class="btn btn-default btn-sm view-mode-1-button export-button"><i class="fas fa-list"></i></a>
			<a class="btn btn-default btn-sm view-mode-2-button export-button"><i class="fas fa-th"></i></a>
		</div>
	</div>
</div>

<div class="view-mode-1">
	<table id="rstable" class="table table-sm table-bordered table-striped" style="width: 100%;">
		<thead>
			<tr>
				<th width="5%" class="text-center">No.</th>
				<th width="6%" class="text-center">Employee ID</th>
				<th width="12%" class="text-center">Employee Name</th>
				<th width="8%" class="text-center">Department</th>
				<th width="8%" class="text-center">Job Position</th>
				<th width="8%" class="text-center">Start Date</th>
				<th width="8%" class="text-center">Date of Birth</th>
				<th width="8%" class="text-center">ID Card</th>
				<th width="21%" class="text-center">Place of in home</th>
				<th width="8%" class="text-center">Certificate Level</th>
				<th width="8%" class="text-center"><span id="resign_date_column_label">Resign Date</span></th>
				<!-- <th width="10%" class="text-center">Last Update User</th> -->
			</tr>
		</thead>
	</table>
</div>

<div class="view-mode-2">
	<div id="dataitems" class="mt-2"></div>
</div>