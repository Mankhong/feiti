<?php echo !empty($message)? $message : ''; ?>
<?php echo !empty($errorMessage)? $errorMessage : ''; ?>

<div class="text-right px-3 pt-1">
	<span id="export_buttons_container"></span>
	<a class="btn btn-default btn-sm view-mode-1-button"><i class="fas fa-list"></i></a>
</div>

<div class="view-mode-1">
	<table id="rstable" class="table table-sm table-bordered table-striped" style="width: 100%;">
		<thead>
			<tr>
				<th width="7%" class="text-center">No.</th>
				<th width="10%" class="text-center">Employee No.</th>
				<th width="16%" class="text-center">Employee Name</th>
				<th width="16%" class="text-center">Leave Type</th>
				<th width="11%" class="text-center">Leave From Date</th>
				<th width="11%" class="text-center">Leave To Date</th>
				<th width="11%" class="text-center">Last Update Date</th>
				<th width="11%" class="text-center">Last Update User</th>
				<th width="7%" class="text-center">Edit</th>
			</tr>
		</thead>
	</table>
</div>

<div class="view-mode-2">
	<div id="dataitems" class="mt-2"></div>
</div>