<!doctype html>
<html>

    <head>
        <title>
            <?php echo $this->template->title->default("FPT"); ?>
        </title>
        <meta charset="utf-8">
        <meta name="description" content="<?php echo $this->template->description; ?>">
        <meta name="author" content="">

        <link rel="icon" href="<?php echo base_url('assets/images/favicon.ico'); ?>">
        <link href="<?= base_url('assets/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('assets/plugins/datatables/css/dataTables.bootstrap4.min.css') ?>" rel="stylesheet">

        <!-- Font Awesome -->
        <link href="<?= base_url('assets/plugins/fontawesome/css/all.min.css') ?>" rel="stylesheet">
        <!-- Datepcker -->
        <link href="<?= base_url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') ?>" rel="stylesheet">
        <!-- Chartis -->
        <link href="<?=base_url('assets/plugins/chartist-js/dist/chartist.min.css')?>" rel="stylesheet">
        <link href="<?=base_url('assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css')?>" rel="stylesheet">

        <!-- App -->
        <link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet">
        <link href="<?= base_url('assets/css/pz-input-editor.css') ?>" rel="stylesheet">

        <?php echo $this->template->meta; ?>
        <?php echo $this->template->stylesheet; ?>
    </head>

    <body>


        <div class="topbar">
            <div class="topbar-title">FPT</div>

            <?php if ($this->ion_auth->is_admin() || $this->admin_model->is_sale_admin($user->id) || $this->admin_model->is_sale_member($user->id)) { ?>
                <a class="menu-item <?php echo empty($menu_active_1) ? '' : 'menu-active'; ?>" href="<?php echo base_url('customer'); ?>">Customer</a>
                <a class="menu-item <?php echo empty($menu_active_2) ? '' : 'menu-active'; ?>" href="<?php echo base_url('product'); ?>">Product</a>
                <a class="menu-item <?php echo empty($menu_active_3) ? '' : 'menu-active'; ?>" href="<?php echo base_url('order'); ?>">Order</a>
                <a class="menu-item <?php echo empty($menu_active_4) ? '' : 'menu-active'; ?>" href="<?php echo base_url('invoice'); ?>">Invoice</a>
                <a class="menu-item <?php echo empty($menu_active_5) ? '' : 'menu-active'; ?>" href="<?php echo base_url('billing'); ?>">Billing Note</a>
            
                <div class="nav-item dropdown menu-item">
                    <a class="nav-link dropdown-toggle menu-item" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Report
                    </a>
                    <ul class="dropdown-menu menu-item" aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="menu-item <?php echo empty($menu_active_10) ? '' : 'menu-active'; ?>" href="<?php echo base_url('report/tracking_report'); ?>" style="width: 203px; font-size: 14px;">Tracking Order Report</a></li>
                        
                        <?php if ($this->ion_auth->is_admin() || $this->admin_model->is_sale_admin($user->id) || $user->id == '4') { ?>
                            <li><a class="menu-item <?php echo empty($menu_active_8) ? '' : 'menu-active'; ?>" href="<?php echo base_url('report/sumreport'); ?>" style="width: 203px; font-size: 14px;">Sales Report for BOI</a></li>
                    
                            <?php if ($user->id != '4') {?>
                                <li><a class="menu-item <?php echo empty($menu_active_9) ? '' : 'menu-active'; ?>" href="<?php echo base_url('report/cusreport'); ?>" style="width: 203px; font-size: 14px;">Summary Report for BOI</a></li>
                            <?php } ?>
                        <?php }?>
                    </ul>
                </div>
            <?php }?>

            <!-- FG -->

            <?php
                $is_personal_admin = $this->admin_model->is_personal_admin($user->id);
                $is_personal_member = $this->admin_model->is_personal_member($user->id);
                $is_personal_account = $this->admin_model->is_personal_account($user->id);
                $is_personal_sub_contract = $this->admin_model->is_personal_sub_contract($user->id);
            ?>

            <?php if ($this->ion_auth->is_admin() || $is_personal_admin  || $is_personal_member || $is_personal_account || $is_personal_sub_contract) { ?>
                <?php if ($is_personal_admin) { ?>
                    <a class="menu-item <?php echo empty($menu_active_202) ? '' : 'menu-active'; ?>" href="<?php echo base_url('fg_employee'); ?>">Employee</a>
                <?php } ?>

                <!-- TIMESHEET -->
                <?php if ($is_personal_admin || $is_personal_account) { ?>
                    <div class="nav-item dropdown menu-item">
                        <a class="nav-link dropdown-toggle menu-item" href="#" id="timeSheetMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Timesheet
                        </a>
                        <ul class="dropdown-menu menu-item" aria-labelledby="timeSheetMenu" style="width: 200px;">
                            <li><a class="menu-item <?php echo empty($menu_active_201) ? '' : 'menu-active'; ?>" href="<?php echo base_url('fg_time'); ?>">Timesheet</a></li>

                            <?php if ($is_personal_admin || $is_personal_account) { ?>
                                <li><a class="menu-item <?php echo empty($menu_active_203) ? '' : 'menu-active'; ?>" href="<?php echo base_url('fg_time_summary'); ?>">Timesheet Summary</a></li>
                                <li><a class="menu-item <?php echo empty($menu_active_207) ? '' : 'menu-active'; ?>" href="<?php echo base_url('fg_time_daily'); ?>">Timesheet Daily</a></li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } else {?>
                    <a class="menu-item <?php echo empty($menu_active_201) ? '' : 'menu-active'; ?>" href="<?php echo base_url('fg_time'); ?>">Timesheet</a>
                <?php } ?>
                <!-- END TIMESHEET -->

                <?php if ($is_personal_admin) { ?>
                    <a class="menu-item <?php echo empty($menu_active_204) ? '' : 'menu-active'; ?>" href="<?php echo base_url('fg_shift_switch'); ?>">Shift Switch</a>
                    <a class="menu-item <?php echo empty($menu_active_206) ? '' : 'menu-active'; ?>" href="<?php echo base_url('fg_leave'); ?>">Leave</a>
                    <a class="menu-item <?php echo empty($menu_active_208) ? '' : 'menu-active'; ?>" href="<?php echo base_url('fg_holiday'); ?>">Holiday</a>

                    <!-- REPORT -->
                    <div class="nav-item dropdown menu-item">
                        <a class="nav-link dropdown-toggle menu-item" href="#" id="reportMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Report
                        </a>
                        <ul class="dropdown-menu menu-item" aria-labelledby="reportMenu" style="width: 270px;">
                            <li><a class="menu-item <?php echo empty($menu_active_301) ? '' : 'menu-active'; ?>" href="<?php echo base_url('fg_summary_leave_type'); ?>">Summary Leave of Leave Type</a></li>
                            <li><a class="menu-item <?php echo empty($menu_active_302) ? '' : 'menu-active'; ?>" href="<?php echo base_url('fg_summary_leave_department'); ?>">Summary Leave of Department</a></li>
                            <li><a class="menu-item <?php echo empty($menu_active_303) ? '' : 'menu-active'; ?>" href="<?php echo base_url('fg_summary_ot_department'); ?>">Summary O-T of Department</a></li>
                            <li><a class="menu-item <?php echo empty($menu_active_304) ? '' : 'menu-active'; ?>" href="<?php echo base_url('fg_summary_ot_overview'); ?>">Summary O-T of Overview</a></li>
                        </ul>
                    </div>
                    <!-- END REPORT -->
                    
                    <a class="menu-item <?php echo empty($menu_active_205) ? '' : 'menu-active'; ?>" href="<?php echo base_url('fg_import'); ?>">Upload</a>
                <?php } ?>
            <?php } ?>
            
            <!-- End FG -->

            <div class="topbar-user-container">
                <?php if ($this->ion_auth->is_admin() || $this->admin_model->is_sale_admin($user->id) || $this->admin_model->is_personal_admin($user->id)) { ?>
                    <a class="user-item <?php echo empty($menu_active_7) ? '' : 'menu-active'; ?>" href="<?php echo base_url('auth'); ?>"><i class="fas fa-users"></i> Manage User</a>
                <?php } ?>
                <a class="user-item"><i class="fas fa-user"></i> <?php echo $user->first_name . ' ' . $user->last_name; ?></a>
                <a class="user-item" href="<?php echo base_url('auth/logout'); ?>"><i class="fas fa-sign-out-alt"></i> Log out</a>
            </div>

        </div>

        <?php if (empty($hide_sub_topbar) || $hide_sub_topbar == FALSE) {?>
            <div class="sub-topbar">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="sub-topbar-title">
                            <?php echo !empty($title) ? $title : '' ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-8">
                        <?php echo !empty($action_buttons) ? $action_buttons : '' ?>
                    </div>
                    <div class="col-sm-12 col-md-4 align-right">
                        <?php if (!empty($display_action_search) && $display_action_search == TRUE) { ?>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-search"></i></span>
                                </div>
                                <input type="text" class="form-control sub-topbar-search" id="action_search" placeholder="Filter" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php }?>

        <?php if (!empty($display_small_topbar) && $display_small_topbar == TRUE) {?>
            <div class="small-topbar">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="small-topbar-title">
                            <?php echo !empty($title) ? $title : '' ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php }?>

        <?php echo $this->template->content; ?>

        <!-- === HIDDENS === -->
        <input type="hidden" name="view_mode" id="view_mode" value="" />
        <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>" />
        <!-- === END HIDDENS === -->

        <!-- === LOADING DIALOG === -->
        <div id="loadingModal" class="modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
             style="transform: translateY(0%);">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <h5 class="modal-title">Loading...</h5>
                    </div>
                </div>
            </div>
        </div>
        <!-- === END LOADING DIALOG === -->

        <!-- === MSG MODAL === -->
        <div id="msgModal" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            <strong><span id="msgModalTitle">Message</span></strong>
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>
                            <span id="msgModalMessage"></span>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- === END MSG MODAL === -->

        <!-- === IMAGE MODAL === -->
        <div id="imageModal" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            <strong><span id="msgModalTitle">Image Viewer</span></strong>
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>
                            <img id="imageModalImage"/>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- === END IMAGE MODAL === -->

        <!-- === JS === -->
        <script src="<?= base_url('assets/plugins/jquery/jquery-3.3.1.min.js') ?>"></script>
        <script src="<?= base_url('assets/plugins/bootstrap/js/popper.min.js') ?>"></script>
        <script src="<?= base_url('assets/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js'); ?>"></script>
        <script src="<?= base_url('assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js'); ?>"></script>
        <!-- Datatables -->
        <script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
        <script src="<?= base_url('assets/plugins/datatables/js/dataTables.bootstrap4.min.js'); ?>"></script>
        <script src="<?= base_url('assets/plugins/datatables/dataTables.fixedColumns.min.js'); ?>"></script>
        <script src="<?= base_url('assets/plugins/datatables-plugins/export/dataTables.buttons.min.js'); ?>"></script>
        <script src="<?= base_url('assets/plugins/datatables-plugins/export/jszip.min.js'); ?>"></script>
        <script src="<?= base_url('assets/plugins/datatables-plugins/export/pdfmake.min.js'); ?>"></script>
        <script src="<?= base_url('assets/plugins/datatables-plugins/export/vfs_fonts.js'); ?>"></script>
        <script src="<?= base_url('assets/plugins/datatables-plugins/export/buttons.html5.min.js'); ?>"></script>
        <!-- Confirmation -->
        <script src="<?= base_url('assets/plugins/bootstrap/js/bootstrap-confirmation.min.js'); ?>"></script>
        <!-- Font Awesome -->
        <script src="<?= base_url('assets/plugins/fontawesome/js/all.min.js'); ?>"></script>
        <!-- Chartist -->
        <script src="<?=base_url('assets/plugins/chartist-js/dist/chartist.min.js')?>"></script>
        <script src="<?=base_url('assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js')?>"></script>
        <script src="<?=base_url('assets/plugins/chartist-plugin-accessibility-master/dist/chartist-plugin-accessibility.min.js')?>"></script>
        
        <!-- Sale -->
        <script src="<?= base_url('assets/js/sale/sale.js'); ?>"></script>
        <!-- App -->
        <script src="<?= base_url('assets/js/common.js'); ?>"></script>
        <script src="<?= base_url('assets/js/pz-input-editor.js'); ?>"></script>
        <script src="<?= base_url('assets/js/login.js'); ?>"></script>

        <?php echo $this->template->javascript; ?>

    </body>

</html>