<div class="content-panel form-panel">

	<h3 class="content-panel-title">Product</h3>

	<?php echo !empty($message)? $message : ''; ?>
	<?php echo !empty($errorMessage)? $errorMessage : ''; ?>

	<?php echo form_open_multipart($submit_url, array('id' => 'form')); ?>

	<!-- === HIDDENS === -->
	<input type="hidden" id="seq" name="seq" value="<?php echo $seq; ?>" />
	<input type="hidden" id="customer_type" name="customer_type" value="<?php echo $customer_type; ?>" />

	<!-- === PRODUCT DETAIL === -->
	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<div id="image_upload_group">
				<img id="image_img" src="<?php echo empty($image_path) ? base_url('assets/images/default_image.jpg'): base_url($image_path); ?>"
				 style="width: 100px; height: 100px; display: inline-block" onclick="showImage()" />
				<button type="button" id="image_upload_button" class="btn btn-default btn-sm ml-1" style="display: inline-block"
				 onclick="selectImage()"><i class="fas fa-camera"></i></button>
				<input type="file" id="image_upload" name="image_upload" onchange="displayImage(this)" style="display: none;" />
			</div>
		</div>
		<div class="col-sm-12 col-md-4 pt-0">
			<div class="pl-0">
				<i class="fas fa-grip-vertical"></i>
				<?php echo lang('product_id', 'product_id');?><span class="label-danger">*</span>
			</div>
			<div>
				<?php echo form_input($product_id); ?>
				<span class="text-danger">
					<?php echo form_error('product_id'); ?></span>
			</div>
		</div>
		<div class="col-sm-12 col-md-6">
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('customer_id', 'customer_id');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<div class="input-group">
				<?php echo form_input($customer_id); ?>

				<?php if (empty($customer_disabled) || $customer_disabled == FALSE) { ?>
				<button type="button" class="btn btn-default btn-sm ml-1 customerPickerTrigger" style="display: inline-block; padding: 0px 15px;">
					<i class="fas fa-user"></i></button>
				<?php } ?>

			</div>
			<span class="text-danger">
				<?php echo form_error('customer_id'); ?></span>
		</div>
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('customer_name', 'customer_name');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($customer_name); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('product_type', 'product_type');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_dropdown('product_type', $product_type_dropdown_data, $product_type_dropdown_default, ['class' => 'form-control']); ?>
			<span class="text-danger">
				<?php echo form_error('product_type'); ?></span>
		</div>

		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('desc1', 'desc1');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($desc1); ?>
			<span class="text-danger">
				<?php echo form_error('desc1'); ?></span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('invoice_type', 'invoice_type');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_dropdown('invoice_type', $invoice_type_dropdown_data, $invoice_type_dropdown_default, ['class' => 'form-control']); ?>
			<span class="text-danger">
				<?php echo form_error('invoice_type'); ?></span>
		</div>

		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('desc2', 'desc2');?>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($desc2); ?>
			<span class="text-danger">
				<?php echo form_error('desc2'); ?></span>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('process', 'process');?><span class="label-danger">*</span>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_dropdown('process', $process_dropdown_data, $process_dropdown_default, ['class' => 'form-control']); ?>
			<span class="text-danger">
				<?php echo form_error('process'); ?></span>
		</div>

		<div class="col-sm-12 col-md-2 border-right">
			<?php echo lang('boi_desc', 'boi_desc');?>
		</div>
		<div class="col-sm-12 col-md-4">
			<?php echo form_input($boi_desc); ?>
			<span class="text-danger">
				<?php echo form_error('boi_desc'); ?></span>
		</div>
	</div>

	<!-- === END PRODUCT DETAIL === -->

	<br><br>

	<!-- === PRODUCT PRICE === -->

	<ul class="nav nav-tabs">
		<li class="nav-item">
			<a class="nav-link active" id="productPriceTab" data-toggle="tab" href="#productPriceContent" role="tab"
			 aria-controls="productPriceContent" aria-selected="true">
				<i class="fas fa-coins"></i> Product Price</a>
		</li>

		<?php if (!empty($show_product_history) && $show_product_history == TRUE) { ;?>
		<li class="nav-item">
			<a class="nav-link" id="productHistoryTab" data-toggle="tab" href="#productHistoryContent" role="tab" aria-controls="productHistoryContent"
			 aria-selected="false">
				<i class="far fa-clock"></i> Update History
			</a>
		</li>
		<?php } ?>
	</ul>

	<br>

	<div class="tab-content" id="myTabContent">
		<div class="tab-pane active" id="productPriceContent" role="tabpanel" aria-labelledby="productPriceTab">
			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('unit_price_type', 'unit_price_type');?>
				</div>
				<div class="col-sm-12 col-md-4 pt-2">
					<?php echo form_radio($unit_price_type_value_1); ?>
					<?php echo $unit_price_type_label_1; ?>
					<?php echo form_radio($unit_price_type_value_2); ?>
					<?php echo $unit_price_type_label_2; ?>
					<span class="text-danger">
						<?php echo form_error('unit_price_type'); ?></span>
				</div>
				
				<div class="col-sm-12 col-md-2 border-right">
				</div>
				<div class="col-sm-12 col-md-4">
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('domestic_price', 'domestic_price');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($domestic_price); ?>
					<span class="text-danger">
						<?php echo form_error('domestic_price'); ?></span>
				</div>
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('export_price', 'export_price');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($export_price); ?>
					<span class="text-danger">
						<?php echo form_error('export_price'); ?></span>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('carton', 'carton');?><span class="label-danger">*</span>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($carton); ?>
					<span class="text-danger">
						<?php echo form_error('carton'); ?></span>
				</div>
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('lot_size', 'lot_size');?><span class="label-danger">*</span>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($lot_size); ?>
					<span class="text-danger">
						<?php echo form_error('lot_size'); ?></span>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('carton_temp', 'carton_temp');?>
				</div>
				<div class="col-sm-12 col-md-4">
					<?php echo form_input($carton_temp); ?>
					<span class="text-danger">
						<?php echo form_error('carton_temp'); ?></span>
				</div>
				<div class="col-sm-12 col-md-2 border-right">
				</div>
				<div class="col-sm-12 col-md-4">
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-2 border-right">
					<?php echo lang('internal_note', 'internal_note');?>
				</div>
				<div class="col-sm-12 col-md-10">
					<?php echo form_textarea($internal_note); ?>
					<span class="text-danger">
						<?php echo form_error('internal_note'); ?></span>
				</div>
			</div>
		</div>

		<div class="tab-pane" id="productHistoryContent" role="tabpanel" aria-labelledby="productHistoryTab">
			<table id="productHistoryTable" class="table table-sm table-bordered table-striped">
				<thead>
					<tr>
						<th class="text-center">Product</th>
						<th class="text-center">Customer</th>
						<th class="text-center">Product Type</th>
						<th class="text-center">Invoice Type</th>
						<th class="text-center">Process</th>
						<th class="text-center">Discription 1/Part Name</th>
						<th class="text-center">Discription 2/Part Name</th>
						<th class="text-center">BOI Discription</th>
						<th class="text-center">Local Price</th>
						<th class="text-center">BOI Price</th>
						<th class="text-center">Carton</th>
						<th class="text-center">Unit Price</th>
						<th class="text-center">Lot Size</th>
						<th class="text-center">Carton Temporary</th>
						<th class="text-center">Internal Note</th>
						<th class="text-center">History Date</th>
					</tr>
				</thead>

				<tbody>

				</tbody>

			</table>
			<br>
		</div>
	</div>

	<!-- === END PRODUCT PRICE === -->

	<?php echo form_close(); ?>

	<br><br><br><br>

</div>

<!-- === CUSTOMER MODAL === -->
<div id="customerPickerModal" class="modal contact-modal form-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					<strong>
						<h5>Select Customer</h5>
					</strong>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<table id="customerPickerTable" class="table table-sm table-bordered table-striped">
					<thead>
						<tr>
							<th width="10%" class="text-center">No.</th>
							<th width="15%" class="text-center">Customer ID</th>
							<th width="45%" class="text-center">Customer Name</th>
							<th width="15%" class="text-center">Customer Type</th>
							<th width="15%" class="text-center">Business Type</th>
						</tr>
					</thead>
				</table>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- === END MODAL DIALOG === -->