<div class="small-topbar criteria-container">
	<div class="row">
		<div class="col-sm-12 col-md-4">
			<div class="label label-width-1">
				Start Date :
			</div>
			<div class="input">
				<input type="text" id="ctrStartDate" class="form-control datepicker" style="width: 220px;" readonly/>
			</div>
		</div>
		<div class="col-sm-12 col-md-4">
			<div class="label label-width-1">
				End Date:
			</div>
			<div class="input">
				<input type="text" id="ctrEndDate" class="form-control datepicker" style="width: 220px;" readonly/>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 col-md-6">
			<div class="label label-width-1">
				Department :
			</div>
			<div class="input">
				<?php echo form_dropdown('ctrDepartment', $department_dropdown_data, $department_dropdown_default, ['class' => 'form-control', 'id' => 'ctrDepartment', 'style' => 'width: 220px;']); ?>
			</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<div class="label label-width-1">
			</div>
			<div class="input pt-1">
			</div>
		</div>
	</div>

	<div class="row pt-2">
		<div class="col-sm-12 col-md-7 text-left">
			<button type="button" id="btnSearch" class="btn btn-primary-pz btn-sm"><i class="fas fa-search"></i> Search</button>
			<button type="button" id="btnClear" class="btn btn-default btn-sm">Clear</button>
		</div>
	</div>
</div>

<?php echo !empty($message)? $message : ''; ?>
<?php echo !empty($errorMessage)? $errorMessage : ''; ?>

<div id="resultContainer" style="width: 100%; padding: 0px 20px;">

	<div class="text-right px-3 pt-1 pb-1">
		<span id="export_buttons_container">
			<?php echo !empty($export_excel) ? $export_excel : ''; ?>
		</span>
	</div>
	
	<br>

	<!-- CHART -->
	<div class="chart-container-pz">
		<div class="chart-header-pz text-center"></div>
		<div class="chart-sub-header-pz text-center"></div>
		<br><br>
		<div class="ct-chart custom-chart tooltip-chart" style="height: 400px;"></div>
	</div>
	<!-- END CHART -->

	<br><br><br><br>
</div>

