<?php echo!empty($message) ? $message : ''; ?>
<?php echo!empty($errorMessage) ? $errorMessage : ''; ?>

<div class="container-fluid">
    <style type="text/css">
        .loader {
            position: relative;
            text-align: center;
            margin: 15px auto 35px auto;
            z-index: 9999;
            display: block;
            width: 50px;
            height: 50px;
            border: 10px solid rgba(0, 0, 0, .3);
            border-radius: 50%;
            border-top-color: #000;
            animation: spin 1s ease-in-out infinite;
            -webkit-animation: spin 1s ease-in-out infinite;

        }

        @keyframes spin {
            to {
                -webkit-transform: rotate(360deg);
            }
        }

        @-webkit-keyframes spin {
            to {
                -webkit-transform: rotate(360deg);
            }
        }
        .noExl{
            border: 0px;
        }

        table.table-bordered.dataTable td  {
            border-left-width: 1px !important;
            border-right-width: 1px !important;
        }
        table.table-bordered.dataTable tbody tr td {
            border-left-width: 1px !important;
            border-right-width: 1px !important;
        }

        

    </style>

    <div class="row">
        <div class="col-sm-12 criteria-container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="label label-width-1">
                        Start Date :
                    </div>
                    <div class="input">
                        <input id="dateTimeStart" name="dateTimeStart" maxlength="100" type="text" class="form-control input-lg date-input" placeholder="dd/mm/yyyy" readonly/>
                    </div>
                    <div class="label label-width-4 pl-2">
                        End Date :
                    </div>
                    <div class="input">    
                        <input id="dateTimeEnd" name="dateTimeEnd" maxlength="100" type="text" class="form-control input-lg date-input" placeholder="dd/mm/yyyy" readonly/>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="label label-width-1">
                        Order ID :
                    </div>
                    <div class="input">
                        <input id="order_id" maxlength="100" type="text" class="form-control input-lg date-input" />
                    </div>

                    <div class="label label-width-4 pl-2">
                        Customer Name :
                    </div>
                    <div class="input">
                        <input maxlength="100" id="customer_name" type="text" class="form-control input-lg date-input" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row pt-0">
        <div class="col-md-12 text-left" style="padding-left: 140px;"> 
            <button type="button" id="btn_search" class="btn btn-primary-pz btn-sm"><i class="fas fa-search"></i> Search</button>
            <button type="button" id="btn_clear" class="btn btn-default btn-sm">Clear</button>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12" style="padding-top: 8px; display: none;" id="rs_div">
            <div class="view-mode-1">
                <div class="text-right px-3 pt-1">
                    <span id="export_buttons_container">
                        <?php echo $export_excel; ?>
                    </span>
                    <a class="btn btn-default btn-sm view-mode-1-button"><i class="fas fa-list"></i></a>
                    <a class="btn btn-default btn-sm view-mode-2-button d-none"><i class="fas fa-th"></i></a>
                </div>

                <table id="rstable" class="table table-sm table-bordered table-striped" style="width: 100%;">
                    <thead>
                        <tr>
                            <th width="7%" class="text-center">No.</th>
                            <th width="20%" class="text-center">Order ID</th>
                            <th width="15%" class="text-center">Order Date</th>
                            <th width="15%" class="text-center">Customer</th>
                            <th width="12%" class="text-center">Delivery Date</th>
                            <th width="12%" class="text-center">Last Update Date</th>
                            <th width="12%" class="text-center">Last Update User</th>
                            <th width="7%" class="text-center">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>


    <div class="view-mode-2">
        <div id="dataitems" class="mt-2"></div>
    </div>
    
    <div class="modal fade" id="loadMe" tabindex="-0" role="dialog" aria-labelledby="loadMeLabel" >
        <div class="modal-dialog modal-sm" role="document" style="max-width: 200px; max-height: 200px; opacity: 4.5;">
            <div class="modal-content" style="background-color: #f5f4f4;">
                <div class="modal-body text-center">
                    <div class="loader" >

                    </div>
                    <label>กำลังค้นหาข้อมูล</label>
                </div>
            </div>
        </div>
    </div>
    
</div>