<input type="hidden" id="isAdmin" value="<?php echo $is_admin; ?>"/>
<input type="hidden" id="isMember" value="<?php echo $is_member; ?>"/>
<input type="hidden" id="isAccount" value="<?php echo $is_account; ?>"/>
<input type="hidden" id="isSubContract" value="<?php echo $is_sub_contract; ?>"/>

<div class="small-topbar criteria-container">
	<div class="row">
		<div class="col-sm-12 col-md-4">
			<div class="label label-width-1">
				Year :
			</div>
			<div class="input">
				<select id="ctrYear" class="form-control" style="width: 220px;">
					<?php 
						$current_year = date('Y');
						$current_month = date('m');
						//$current_date =  date('d', time() + (13 * 60 * 60));
						$current_date =  date('d');

						if ($current_month == 12) {
							$current_year += 1;
						}

						$year_offset = 5;
						
						for ($i = $current_year; $i > ($current_year - $year_offset); $i--) {
							echo '<option value="' . $i . '">' . $i . '</option>';
						}
					?>
				</select>
			</div>
		</div>
		<div class="col-sm-12 col-md-4">
			<div class="label label-width-1">
				Month :
			</div>
			<div class="input">
				<select id="ctrMonth" class="form-control" style="width: 220px;">
					<option value="1">January</option>
					<option value="2">Febuary</option>
					<option value="3">March</option>
					<option value="4">April</option>
					<option value="5">May</option>
					<option value="6">June</option>
					<option value="7">July</option>
					<option value="8">August</option>
					<option value="9">September</option>
					<option value="10">October</option>
					<option value="11">November</option>
					<option value="12">December</option>
				</select>
			</div>
		</div>
	</div>

	<div class="row <?php echo $is_member ? 'd-none' : ''; ?>">
		<div class="col-sm-12 col-md-4">
			<div class="label label-width-1">
				Employee No.<font color="red"> *</font> :
			</div>
			<div class="input">
				<input type="text" id="ctrEmployeeID" class="form-control" style="width: 220px;" value="<?php echo !empty($employee_no) ? $employee_no : ''; ?>"/>
			</div>
		</div>
		<div class="col-sm-12 col-md-4">
			<div class="label label-width-1">
				Employee Name :
			</div>
			<div class="input">
				<input type="text" id="ctrEmployeeName" class="form-control" style="width: 220px;"/>
			</div>
		</div>
	</div>

	<div class="row pt-2">
		<div class="col-sm-12 col-md-7 text-left">
			<button type="button" id="btnSearch" class="btn btn-primary-pz btn-sm"><i class="fas fa-search"></i> Search</button>
			<button type="button" id="btnClear" class="btn btn-default btn-sm">Clear</button>
		</div>
	</div>
</div>

<?php echo !empty($message)? $message : ''; ?>
<?php echo !empty($errorMessage)? $errorMessage : ''; ?>

<div class="text-right px-3 pt-3 pb-3 d-none">
	<span id="export_buttons_container"></span>
</div>

<div id="resultContainer" style="display: none;">
	<div class="text-right px-3 pt-1 pb-0">
		<span id="export_buttons_container">
			<?php echo !empty($export_excel) ? $export_excel : ''; ?>
		</span>
	</div>

	<div style="padding: 20px 10px 10px 20px;" class="d-none">
		<h4>ผลลัพธ์การค้นหา</h4>
	</div>

	<div class="display-input-container mt-1 mb-3" style="padding-left: 20px;">
		<div class="row">
			<div class="col-sm-12 col-md-4">
				<div class="label label-width-2">
					Employee No. :
				</div>
				<div class="input">
					<input type="text" id="dspEmployeeNo" class="form-control" style="width: 220px;" readonly/>
					<input type="hidden" id="dspEmployeeID"/>
					<input type="hidden" id="dspYear"/>
					<input type="hidden" id="dspMonth"/>
				</div>
			</div>
			<div class="col-sm-12 col-md-5">
				<div class="label label-width-2">
					Employee Type :
				</div>
				<div class="input">
					<input type="text" id="dspEmployeeType" class="form-control" style="width: 250px;" readonly/>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12 col-md-4">
				<div class="label label-width-2">
					Employee Name Thai :
				</div>
				<div class="input">
					<input type="text" id="dspEmployeeName" class="form-control" style="width: 220px;" readonly/>
				</div>
			</div>
			<div class="col-sm-12 col-md-5">
				<div class="label label-width-2">
					Department :
				</div>
				<div class="input">
					<input type="text" id="dspEmployeeDepartment" class="form-control" style="width: 250px;" readonly/>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12 col-md-4">
				<div class="label label-width-2">
					Shift :
				</div>
				<div class="input">
					<input type="text" id="dspShift" class="form-control" style="width: 220px;" readonly/>
				</div>
			</div>
		</div>
	</div>

	<table id="rstable" class="table table-sm table-bordered tb-timesheet" style="width: 100%;">
		<thead>
			<tr>
				<th width="5%" class="text-center no-sort" rowspan="2">No.</th>
				<th width="9%" class="text-center" rowspan="2">Date</th>
				<th width="9%" class="text-center" rowspan="2">Start Work</th>
				<th width="9%" class="text-center" rowspan="2">End Work</th>
				<th width="12%" class="text-center" rowspan="2">Leave Type</th>
				<th width="9%" class="text-center" rowspan="2">Night Shift</th>
				<th width="9%" class="text-center" rowspan="2">Food Travel Day</th>
				<th width="24%" class="text-center" colspan="4">OverTime</th>
				<th class="text-center" rowspan="2">Remark</th>
				<th width="5%" class="text-center" rowspan="2">Edit</th>
			</tr>
			<tr>
				<th class="text-center">1.00</th>
				<th class="text-center">1.50</th>
				<th class="text-center">2.00</th>
				<th class="text-center" style="border-right: 1px solid #edd;">3.00</th>
			</tr>
		</thead>

		<tfoot>
			<tr class="total-tr-color">
				<td colspan="5"><strong>Total</strong></td>
				<!--
				<th class="text-right" width="0%"><span id="sp_sum_working_day"></span></th>
				-->
				<th class="text-right"><span id="sp_sum_night_shift"></span></th>
				<th class="text-right"><span id="sp_sum_food_travel_day"></span></th>
				<th class="text-right"><span id="sp_sum_ot1"></span></th>
				<th class="text-right"><span id="sp_sum_ot15"></span></th>
				<th class="text-right"><span id="sp_sum_ot2"></span></th>
				<th class="text-right"><span id="sp_sum_ot3"></span></th>
				<th class="text-right" colspan="2"></th>
			</tr>
		</tfoot>
	</table>

	<?php if ($is_admin || $is_account || $is_member) { ?>

		<div class="row" style="margin-top: -16px;">
			<div class="col-12">
				<div class="box-panel-1">
					<div class="display-input-container text-center">
						<div class="label label-width-1">
							<strong><i class="fas fa-coins"></i> Diligence</strong> :
						</div>
						<div class="input">
							<input type="number" id="monthly_data_diligence" class="form-control text-right px-2" style="width: 220px; padding: 3px;" <?php echo !$is_admin ? 'readonly': ''; ?>/>
							<input type="hidden" id="monthly_data_id"/>
						</div>
						<div class="input ml-1 mr-2" style="vertical-align: top;">
							<?php if ($is_admin) { ?>
								<button type="button" id="btnDiligence" class="btn btn-primary-pz btn-sm" style="width: 80px; font-size: 12px;">Save</button>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>

	<?php } ?>
	
	<br><br><br><br>
</div>

<!-- === EDITOR MODAL === -->
<div id="editorModal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					<strong>Modify Timesheet : <span id="editorModalTitle"></span></strong>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
				<div class="label-input-form">
					<div class="row">
						<div class="col-6">
							<div class="label label-width-1 border-right">
								No.
								<input type="hidden" id="edtId"/>
							</div>
							<div class="input">
								<input type="text" class="form-control" id="edtNo" readonly/>
							</div>
						</div>
						<div class="col-6">
							<div class="label label-width-1 border-right">
								Date
							</div>
							<div class="input">
								<input type="text" class="form-control" id="edtDate" readonly/>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-6">
							<div class="label label-width-1 border-right">
								Start Work <font color="red">*</font>
							</div>
							<div class="input">
								<div class="input-group" style="width: 140px;">
									<input type="number" class="form-control text-center" id="edtStartWorkHour" maxlength="2"/>
									<label style="margin-top: 10px;">&nbsp; : &nbsp;</label>
									<input type="number" class="form-control text-center" id="edtStartWorkMinute" maxlength="2"/>
									<label style="font-size: 10px; padding-left: 13px; padding-top: 10px;">(HH:MM)</label>
								</div>
							</div>
						</div>
						<div class="col-6">
							<div class="label label-width-1 border-right">
								End Work <font color="red">*</font>
							</div>
							<div class="input">
								<div class="input-group" style="width: 140px;">
									<input type="number" class="form-control text-center" id="edtEndWorkHour" maxlength="2"/>
									<label style="margin-top: 10px;">&nbsp; : &nbsp;</label>
									<input type="number" class="form-control text-center" id="edtEndWorkMinute" maxlength="2"/>
									<label style="font-size: 10px; padding-left: 13px; padding-top: 10px;">(HH:MM)</label>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-6">
							<div class="label label-width-1 border-right">
								Leave Type
							</div>
							<div class="input">
								<input type="text" class="form-control" id="edtLeaveType" readonly/>
							</div>
						</div>
						<div class="col-6">
							<div class="label label-width-1 border-right">
								Total Hours
							</div>
							<div class="input">
								<input type="text" class="form-control" id="edtTotalHours"/>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-6">
							<div class="label label-width-1 border-right">
								Working Day
							</div>
							<div class="input">
								<input type="text" class="form-control" id="edtWorkingDay"/>
							</div>
						</div>
						<div class="col-6">
							<div class="label label-width-1 border-right">
								Night Shift
							</div>
							<div class="input pt-1">
								<input type="radio" name="edtNightShift" value="1"/>
								Yes
								<input type="radio" name="edtNightShift" class="ml-1" value="0"/>
								No
							</div>
						</div>
					</div>

					<div class="row mt-1 mb-1">
						<div class="col-12">
							<div class="label label-width-1 border-right">
								Food Travel Day
							</div>
							<div class="input pt-1 pb-1">
								<input type="radio" name="edtFoodTravelDay" value="1"/>
								Yes
								<input type="radio" name="edtFoodTravelDay" class="ml-1" value="0"/>
								No
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-12">
							<div class="label label-width-1 border-right">
								Over Time
							</div>
							<div class="input p-1">
								<table border="0" style="width: 400px;">
									<tr>
										<td width="25%" align="center" class="bg-ot-1">
											1.00
										</td>
										<td width="25%">
											<input type="number" class="form-control" id="edtOt1"/>
										</td>
										<td width="25%" align="center" class="bg-ot-2">
											1.50
										</td>
										<td width="25%">
											<input type="number" class="form-control" id="edtOt15"/>
										</td>
									</tr>
									<tr>
										<td align="center" class="bg-ot-3">
											2.00
										</td>
										<td>
											<input type="number" class="form-control" id="edtOt2"//>
										</td>
										<td align="center" class="bg-ot-4">
											3.00
										</td>
										<td>
											<input type="number" class="form-control" id="edtOt3"//>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-12">
							<div class="label label-width-1 border-right">
								Remark
							</div>
							<div class="input">
								<input type="text" class="form-control" style="width: 625px; max-width: 90%;" id="edtRemark"//>
							</div>
						</div>
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary-pz" id="btnEditorSave">Save</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- === END EDITOR MODAL === -->