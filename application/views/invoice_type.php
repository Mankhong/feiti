<br>

<div class="content-panel form-panel">

	<h3 class="content-panel-title">Invoice Types</h3>

	<br>
	<?php echo !empty($message)? $message : ''; ?>
	<?php echo !empty($errorMessage)? $errorMessage : ''; ?>
	
	<?php echo form_open_multipart(uri_string(), array('id' => 'form')); ?>

	<br>
	<div class="row">
		<div class="col-sm-6 text-left">
			<button type="button" class="btn btn-primary-pz btn-sm" id="saveButton"><i class="fas fa-save"></i> Save</button>
		</div>
		<div class="col-sm-6 text-right">
			<button type="button" class="btn btn-default btn-sm" id="createButton"><i class="fas fa-plus-circle fa-sm"></i>
				Create New Invoice Type</button>
		</div>
	</div>

	<br>
	<table id="rstable" class="table table-sm table-bordered table-striped" style="width: 100%;">
		<thead>
			<tr>
				<th width="10%" class="text-center"><label>No.</label></th>
				<th width="20%" class="text-center">
					<?php echo lang('invoice_type_id', 'invoice_type_id');?>
				</th>
				<th width="20%" class="text-center">
					<?php echo lang('invoice_type_name', 'invoice_type_name');?>
				</th>
				<th width="20%" class="text-center">
					<?php echo lang('invoice_type_name', 'invoice_type_name');?>
				</th>
				<th width="30%" class="text-center">
					<?php echo lang('status', 'status');?>
				</th>
			</tr>
		</thead>

		<tbody>
			<?php if(!empty($result_list)) {
				for ($i = 0; $i < count($result_list); $i++) {
					echo '<tr class="dataitem">'
					. '<td class="text-center">' . ($i + 1) . '</td>'
					. '<td class="text-left">' . $result_list[$i]['id'] . '</td>'
					. '<td class="text-left">' . $result_list[$i]['name'] . '</td>'
					. '<td class="text-left">' . $result_list[$i]['phase'] . '</td>'
					. '<td class="text-center">'
					. '<input type="hidden" class="dt-id" value="' . $result_list[$i]['id'] . '"/>'
					. '<input type="hidden" class="dt-temp-used" value="' . $result_list[$i]['used'] . '"/>'
					. '<input type="radio" name="used_' . $result_list[$i]['id'] . '" value="Y" ' . ($result_list[$i]['used'] == 'Y' ? 'checked="checked"' : '') . '/> Used'
					. '<input type="radio" class="ml-3" name="used_' . $result_list[$i]['id'] . '" value="N" ' . ($result_list[$i]['used'] != 'Y' ? 'checked="checked"' : '') . '/> Not used'
					. '</td>'
					. '</tr>';
				}
			}
			?>
		</tbody>
	</table>

	<!-- HIDDEN -->
	<input type="hidden" name="insert_invoice_name" id="insert_invoice_name"/>
	<input type="hidden" name="insert_invoice_phase" id="insert_invoice_phase"/>
	<span id="hiddenContainer"></span>

	<?php echo form_close(); ?>

	<br><br>

	<!-- === CREATE ITEM MODAL === -->
	<div id="createItemModal" class="modal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">
						<strong><span id="msgModalTitle">Create New Invoice Type</span></strong>
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<br>
					<input type="text" class="form-control" id="itemInput" placeholder="Enter invoice type"/>
					<span class="text-danger ml-1" id="itemInputError"></span>
					<br>
					<input type="text" class="form-control" id="itemPhaseInput" placeholder="Enter Phase"/>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary-pz" id="createItemButton">Create</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- === END CREATE ITEM MODAL === -->

</div>