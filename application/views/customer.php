<?php echo !empty($message)? $message : ''; ?>
<?php echo !empty($errorMessage)? $errorMessage : ''; ?>

<div class="text-right px-3 pt-1">
	<span id="export_buttons_container">
		<?php echo $export_excel; ?>
	</span>
	<a class="btn btn-default btn-sm view-mode-1-button export-button"><i class="fas fa-list"></i></a>
	<a class="btn btn-default btn-sm view-mode-2-button export-button"><i class="fas fa-th"></i></a>
</div>

<div class="view-mode-1">
	<table id="rstable" class="table table-sm table-bordered table-striped" style="width: 100%;">
		<thead>
			<tr>
				<th width="10%" class="text-center">No.</th>
				<th width="11%" class="text-center">Customer ID</th>
				<th width="25%" class="text-center">Customer Name</th>
				<th width="10%" class="text-center">Customer Type</th>
				<th width="10%" class="text-center">Country</th>
				<th width="12%" class="text-center">Last Update Date</th>
				<th width="12%" class="text-center">Last Update User</th>
				<th width="10%" class="text-center">Action</th>
				<th width="0%" class="d-none">Business Type</th>
				<th width="0%" class="d-none">Telephone No.</th>
				<th width="0%" class="d-none">Phone</th>
				<th width="0%" class="d-none">Fax</th>
				<th width="0%" class="d-none">Facebook</th>
				<th width="0%" class="d-none">Line</th>
				<th width="0%" class="d-none">Website</th>
				<th width="0%" class="d-none">Email</th>
				<th width="0%" class="d-none">Address</th>
				<th width="0%" class="d-none">Internal Note</th>
				<th width="0%" class="d-none">Contact Person</th>
			</tr>
		</thead>
	</table>
</div>

<div class="view-mode-2">
	<div id="dataitems" class="mt-2"></div>
</div>