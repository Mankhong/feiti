<div class="action-panel">
    <?php if (!empty($tax_invoice_url)) { ?>
        <a class="btn btn-default btn-sm ml-1 d-none" href="<?php echo $tax_invoice_url; ?>" target="_blank"><i class="far fa-share-square"></i>
            Tax Invoice</a>
    <?php } ?>

    <?php if (!empty($delivery_order_url)) { ?>
        <a class="btn btn-default btn-sm ml-1 d-none" href="<?php echo $delivery_order_url; ?>" target="_blank"><i class="fas fa-share-square"></i>
            Delivery Order</a>
    <?php } ?>

    <?php if (!empty($tax_invoice_os_url)) { ?>
        <a class="btn btn-default btn-sm ml-1" href="<?php echo $tax_invoice_os_url; ?>" target="_blank">
            Tax Invoice</a>
    <?php } ?>

    <?php if (!empty($do_os_url)) { ?>
        <a class="btn btn-default btn-sm ml-1" href="<?php echo $do_os_url; ?>" target="_blank">
            Delivery Order</a>
    <?php } ?>
</div>

<div class="content-panel form-panel">

    <h3 class="content-panel-title">Invoice</h3>

    <?php echo!empty($message) ? $message : ''; ?>
    <?php echo!empty($errorMessage) ? $errorMessage : ''; ?>

    <?php echo form_open_multipart($submit_url, array('id' => 'form')); ?>

    <!-- === HIDDENS === -->
    <input type="hidden" id="temp_invoice_id" name="temp_invoice_id" value="<?php echo empty($temp_invoice_id) ? '' : $temp_invoice_id; ?>"/>

    <!-- === INVOICE DETAIL === -->

    <div class="row">
        <div class="col-sm-12 col-md-2 border-right">
            <?php echo lang('customer_id', 'customer_id'); ?><span class="label-danger">*</span>
        </div>
        <div class="col-sm-12 col-md-4">
            <div class="input-group">
                <?php echo form_input($customer_id); ?>
                <input type="hidden" id="customer_type" name="customer_type" />

                <?php if (empty($customer_disabled) || $customer_disabled == FALSE) { ?>
                    <button type="button" class="btn btn-default btn-sm ml-1 customerPickerTrigger" style="display: inline-block; padding: 0px 15px;">
                        <i class="fas fa-user"></i></button>
                <?php } ?>

            </div>
            <span class="text-danger">
                <?php echo form_error('customer_id'); ?></span>
        </div>
        <div class="col-sm-12 col-md-2 border-right">
            <?php echo lang('customer_name', 'customer_name'); ?><span class="label-danger">*</span>
        </div>
        <div class="col-sm-12 col-md-4">
            <?php echo form_input($customer_name); ?>
            <span class="text-danger">
                <?php echo form_error('customer_name'); ?></span>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-2 border-right">
            <?php echo lang('invoice_id', 'invoice_id'); ?><span class="label-danger">*</span>
        </div>
        <div class="col-sm-12 col-md-4">
            <div class="<?php echo $mode == 'create' ? 'input-group' : 'd-none'; ?>">
                <?php echo form_dropdown('invoice_type', $invoice_type_dropdown_data, $invoice_type_dropdown_default, ['class' => 'form-control', 'id' => 'invoice_type']); ?>
                <?php echo form_input($id_suffix); ?>
            </div>

            <div class="<?php echo $mode != 'create' ? '' : 'd-none'; ?>">
                <?php echo form_input($invoice_id); ?>
                <span class="text-danger" id="invoice_id_err">
            </div>

            <?php echo form_error('invoice_id'); ?></span>
            <?php echo form_error('invoice_type'); ?></span>
            <?php echo form_error('id_suffix'); ?></span>
        </div>

        <div class="col-sm-12 col-md-2 border-right">
            <?php echo lang('invoice_date', 'invoice_date'); ?><span class="label-danger">*</span>
        </div>
        <div class="col-sm-12 col-md-4">
            <?php echo form_input($invoice_date); ?>
            <span class="text-danger">
                <?php echo form_error('invoice_date'); ?></span>
        </div>
    </div>

    <div class="row" id="refer_invoice_id_container" <?php echo ($invoice_group == 'S' ? '' : 'style="display: none;"'); ?>">
        <div class="col-sm-12 col-md-2 border-right">
            <?php echo lang('refer_invoice_id', 'refer_invoice_id'); ?><span class="label-danger">*</span>
        </div>
        <div class="col-sm-12 col-md-4">
            <?php echo form_input($refer_invoice_id); ?>
            <span class="text-danger">
                <?php echo form_error('refer_invoice_id'); ?></span>
        </div>

        <div class="col-sm-12 col-md-2 border-right">
        </div>
        <div class="col-sm-12 col-md-4">
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-2 border-right">
            <?php echo lang('delivery_address', 'delivery_address'); ?><span class="label-danger">*</span>
        </div>
        <div class="col-sm-12 col-md-10">
            <?php echo form_textarea($delivery_address); ?>
            <span class="text-danger">
                <?php echo form_error('delivery_address'); ?></span>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-2 border-right">
            <?php echo lang('internal_note', 'internal_note'); ?>
        </div>
        <div class="col-sm-12 col-md-10">
            <input type="hidden" name="internal_note" id="internal_note" value="<?php echo $internal_note; ?>"/>
            <input type="text" id="internal_note_1" class="form-control" maxlength="34"/>
            <input type="text" id="internal_note_2" class="form-control" maxlength="34"/>
            <input type="text" id="internal_note_3" class="form-control" maxlength="34"/>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-2 border-right">

        </div>
        <div class="col-sm-12 col-md-10 pt-2 text-right">
            <?php if (!empty($render_cancel_invoice_button)) { ?>
                <button type="button" class="btn btn-primary-pz" id="cancel_invoice_button">Cancel Invoice</button>
            <?php } ?>
        </div>
    </div>
    <!-- === END INVOICE DETAIL === -->

    <br><br>
    <!-- === PRODUCT LIST === -->
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active"><i class="fas fa-bezier-curve"></i> Invoice Lines</a>
        </li>
    </ul>
    <div>
        <br>
        <table id="dataitemTable" class="table table-sm table-bordered table-striped">
            <thead>
                <tr>
                    <th width="10%" class="text-center">PO Order</th>
                    <th width="10%" class="text-center">Product ID</th>
                    <th width="20%" class="text-center">Part Name</th>
                    <th width="10%" class="text-center">Lot No.</th>
                    <th width="8%" class="text-center">Quantity</th>
                    <th width="14%" class="text-center">Unit Price</th>
                    <th width="14%" class="text-center">Amount</th>
                    <th width="7%" class="text-center">Edit</th>
                    <th width="7%" class="text-center">Delete</th>
                </tr>
            </thead>
            <tbody class="dataitem-body">
                <?php
                if (!empty($product_list)) {
                    //-- Dataitem: id|product_id|product_seq|product_name|invoice_quantity|lot_no|lot_size|unit_price|quantity|order_id|balance|temp_quantity|dataitem_type
                    $delimiter = '&88&';
                    for ($i = 0; $i < count($product_list); $i++) {
                        $product_seq = Common::encodeString($product_list[$i]['product_seq']);
                        $product_id = Common::encodeString($product_list[$i]['product_id']);
                        $order_id = Common::encodeString($product_list[$i]['order_id']);

                        echo '<tr class="dataitem">' .
                        '<td>' .
                        '<input type="hidden" class="dt-dataitem-type" value="DATA"/>' .
                        '<input type="hidden" class="dt-id" value="' . $product_list[$i]['invoice_products_id'] . '"/>' .
                        '<input type="hidden" class="dt-lot-size" value="' . $product_list[$i]['lot_size'] . '"/>' .
                        '<input type="hidden" class="dt-quantity" value="' . $product_list[$i]['quantity'] . '"/>' .
                        '<input type="hidden" class="dt-balance" value="' . $product_list[$i]['balance'] . '"/>' .
                        '<input type="hidden" class="dt-temp-quantity" value="' . $product_list[$i]['invoice_quantity'] . '"/>' .
                        '<input type="hidden" class="dt-product-id-hidden" value="' . $product_id . '"/>' .
                        '<input type="hidden" class="dt-product-seq-hidden" value="' . $product_seq . '"/>' .
                        '<input type="hidden" class="dt-order-id-hidden" value="' . $order_id . '"/>' .
                        '<span class="dt-order-id">' . $product_list[$i]['order_id'] . '</span>' .
                        '</td>' .
                        '<td class="text-right"><span class="dt-product-id">' . $product_list[$i]['product_id'] . '</span></td>' .
                        '<td class="text-right"><span class="dt-product-name">' . $product_list[$i]['desc1'] . '</span></td>' .
                        '<td class="text-right"><span class="dt-lot-no">' . $product_list[$i]['lot_no'] . '</span></td>' .
                        '<td class="text-right"><span class="dt-invoice-quantity">' . $product_list[$i]['invoice_quantity'] . '</span></td>' .
                        '<td class="text-right"><span class="dt-unit-price">' . $product_list[$i]['unit_price'] . '</span></td>' .
                        '<td class="text-right"><span class="dt-amount"></span>' .
                        '<td class="text-center"><a class="btn btn-default btn-icon" onclick="onclickEditProductPackage(\'' . $product_list[$i]['invoice_products_id'] . '\')"><i class="fas fa-cube"></i></td>' .
                        '<td class="text-center"><a class="btn btn-default btn-icon" onclick="onclickDeleteDataitem(\'' . $product_list[$i]['invoice_products_id'] . '\')"><i class="fas fa-times fa-sm"></i></a></td>' .
                        '</tr>';
                    }
                }
                ?>

                <?php
                if (!empty($insert_dataitems)) {
                    //-- Dataitem: id|product_id|product_seq|product_name|invoice_quantity|lot_no|lot_size|unit_price|quantity|order_id|balance|temp_quantity|dataitem_type
                    $delimiter = '&88&';
                    for ($i = 0; $i < count($insert_dataitems); $i++) {
                        $values = explode($delimiter, $insert_dataitems[$i]);

                        echo '<tr class="dataitem">' .
                        '<td>' .
                        '<input type="hidden" class="dt-dataitem-type" value="' . $values[12] . '"/>' .
                        '<input type="hidden" class="dt-id" value="' . $values[0] . '"/>' .
                        '<input type="hidden" class="dt-lot-size" value="' . $values[6] . '"/>' .
                        '<input type="hidden" class="dt-quantity" value="' . $values[8] . '"/>' .
                        '<input type="hidden" class="dt-balance" value="' . $values[10] . '"/>' .
                        '<input type="hidden" class="dt-temp-quantity" value="' . $values[11] . '"/>' .
                        '<input type="hidden" class="dt-product-id-hidden" value="' . $values[1] . '"/>' .
                        '<input type="hidden" class="dt-product-seq-hidden" value="' . $values[2] . '"/>' .
                        '<input type="hidden" class="dt-order-id-hidden" value="' . $values[9] . '"/>' .
                        '<span class="dt-order-id">' . Common::decodeString($values[9]) . '</span>' .
                        '</td>' .
                        '<td class="text-right"><span class="dt-product-id">' . Common::decodeString($values[1]) . '</span></td>' .
                        '<td class="text-right"><span class="dt-product-name">' . $values[3] . '</span></td>' .
                        '<td class="text-right"><span class="dt-lot-no">' . $values[5] . '</span></td>' .
                        '<td class="text-right"><span class="dt-invoice-quantity">' . $values[4] . '</span></td>' .
                        '<td class="text-right"><span class="dt-unit-price">' . $values[7] . '</span></td>' .
                        '<td class="text-right"><span class="dt-amount"></span></td>' .
                        '<td class="text-center"><a class="btn btn-default btn-icon" onclick="onclickEditProductPackage(\'' . $values[0] . '\')"><i class="fas fa-cube"></i></td>' .
                        '<td class="text-center"><a class="btn btn-default btn-icon" onclick="onclickDeleteDataitem(\'' . $values[0] . '\')"><i class="fas fa-times fa-sm"></i></a></td>' .
                        '</tr>';
                    }
                }
                ?>
            </tbody>
        </table>

        <div id="invoice_summary_container" style="font-size: 12px; text-align: right;">
            <table style="width: 240px; float: right;">
                <tr>
                    <td style="width: 50%; text-align: right; font-weight: bold;">
                        Amount :
                    </td>
                    <td style="width: 50%;">
                        <span id="sm_amount"></span>
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%; text-align: right; font-weight: bold;">
                        Vat <span id="sm_vat">
<?php echo $vat; ?></span>% :
                    </td>
                    <td style="width: 50%;">
                        <span id="sm_vat_price"></span>
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%; text-align: right; font-weight: bold;">
                        Total :
                    </td>
                    <td style="width: 50%;">
                        <span id="sm_total"></span>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <br>
    <button type="button" class="btn btn-default btn-sm orderProductPickerTrigger">Add</button>
    <!-- === END PRODUCT CONTACT === -->

    <!-- === HIDDENS CONTAINER === -->
    <span id="dataitem_hidden_container">
        <?php
        if (!empty($insert_dataitems)) {
            $delimiter = '&88&';
            for ($i = 0; $i < count($insert_dataitems); $i++) {
                $values = explode($delimiter, $insert_dataitems[$i]);
                echo '<input type="hidden" name="insert_dataitems[]" value="' . $insert_dataitems[$i] . '"/>';
            }
        }
        ?>
    </span>

    <br>
    <span id="lotitem_hidden_container">
        <?php
        if (!empty($lotitem_list)) {
            // ivp_id|product_seq|order_id|lot_size|lot_num|quantity
            $delimiter = '&88&';
            $product_seq = Common::encodeString($lotitem_list[0]['product_seq']);
            $order_id = Common::encodeString($lotitem_list[0]['order_id']);

            for ($i = 0; $i < count($lotitem_list); $i++) {
                $value = $lotitem_list[$i]['ivp_id']
                        . $delimiter . $product_seq
                        . $delimiter . $order_id
                        . $delimiter . $lotitem_list[$i]['lot_size']
                        . $delimiter . $lotitem_list[$i]['lot_num']
                        . $delimiter . $lotitem_list[$i]['quantity'];
                echo '<input type="hidden" name="lotitems[]" class="lotitems" value="' . $value . '"/>';
            }
        }
        ?>

<?php
if (!empty($lotitems)) {
    for ($i = 0; $i < count($lotitems); $i++) {
        echo '<input type="hidden" name="lotitems[]" class="lotitems" value="' . $lotitems[$i] . '"/>';
    }
}
?>
    </span>

<?php echo form_close(); ?>

    <br><br><br><br><br><br>

</div>

<!-- === CUSTOMER MODAL === -->
<div id="customerPickerModal" class="modal contact-modal form-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    <strong>
                        <h5>Select Customer</h5>
                    </strong>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <table id="customerPickerTable" class="table table-sm table-bordered table-striped">
                    <thead>
                        <tr>
                            <th width="10%" class="text-center">No.</th>
                            <th width="15%" class="text-center">Customer ID</th>
                            <th width="45%" class="text-center">Customer Name</th>
                            <th width="15%" class="text-center">Customer Type</th>
                            <th width="15%" class="text-center">Business Type</th>
                        </tr>
                    </thead>
                </table>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- === END CUSTOMER MODAL === -->

<!-- === ORDER PRODUCT MODAL === -->
<div id="orderProductPickerModal" class="modal contact-modal form-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    <strong>
                        <h5>Select Products</h5>
                    </strong>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="row pl-2">
                <div class="col-sm-12 criteria-container">
                    <div class="row">
                        <div class="label label-width-3 text-right pt-1 pr-1">
                            Order ID :
                        </div>
                        <div class="input">
                            <input style="width: 100%; height: 25px; width: 150px;" id="order_id" maxlength="100" type="text" class="form-control input-lg date-input" />
                        </div>

                        <div class="label label-width-3 text-right pt-1 pr-1">
                            Product ID :
                        </div>
                        <div class="input">    
                            <input style="width: 100%; height: 25px; width: 150px;" maxlength="100" id="product_id" type="text" class="form-control input-lg date-input" />
                        </div>

                        <div class="label label-width-2 text-right pt-1 pr-1">
                            Product Name :
                        </div>
                        <div class="input">
                            <input style="width: 100%; height: 25px; width: 150px;" id="product_name" maxlength="100" type="text" class="form-control input-lg date-input" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="row pt-0 pb-1">
                <div class="col-sm-5 text-left" style="padding-left: 93px;"> 
                    <button type="button" id="btn_search" class="btn btn-primary-pz btn-sm"><i class="fas fa-search"></i> Search</button>
                    <button type="button" id="btn_clear" class="btn btn-default btn-sm">Clear</button>
                </div>
                <div class="col-sm-7 pt-0 pl-5">
                    <div class="picker-loading text-left" style="display: none;"><img height="36" src="<?php echo base_url('assets/images/loading-2.gif'); ?>"/></div>
                </div>  
            </div>

            <div class="row">
                <div class="col-md-12" style="padding-top: 8px;" id="rs_div">
                    <table id="orderProductPickerTable" class="table table-sm table-bordered table-striped" style="width: 100%;">
                        <thead>
                            <tr>
                                <th width="7%" class="text-left" style="padding: 0px;"><input type="checkbox" class="order-product-picker-cball" /></th>
                                <th width="21%" class="text-center">Product ID/Part ID</th>
                                <th width="20%" class="text-center">Description 1/Part Name</th>
                                <th width="15%" class="text-center">Order ID</th>
                                <th width="15%" class="text-center">Order Date</th>
                                <th width="15%" class="text-center">Quantity</th>
                                <th width="7%" class="text-center">Balance</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

            <span class="order-product-picker-hidden-container"></span>

            <div class="modal-footer">
                <button type="button" class="btn btn-warning-pz btn-sm order-product-picker-select-all">Select All</button>
                <button type="button" class="btn btn-primary-pz btn-sm order-product-picker-select">Select</button>
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- === END ORDER PRODUCT MODAL === -->

<!-- === CONFIRM DELETE MODAL === -->
<div id="confirmDeleteModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    <strong><span id="msgModalTitle">Confirm</span></strong>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    Do you want to delete this item?
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-pz" id="confirmDeleteButton">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<!-- === END CONFIRM DELETE MODAL === -->

<!-- === CONFIRM CANCEL MODAL === -->
<div id="confirmCancelModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    <strong>ยืนยันการยกเลิก</strong>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text-danger text-center" style="font-size: 16px;">
                    <strong>หากยกเลิก Invoice รายการ Product และ Customer ID ที่ท่านเลือกไว้จะถูกลบ ต้องการยกเลิก Invoice ใช่หรือไม่?</strong>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-pz"  onclick="confirmCancelInvoice()" style="width: 80px;">ใช่</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" style="width: 80px;">ไม่ใช่</button>
            </div>
        </div>
    </div>
</div>
<!-- === END CONFIRM CANCEL MODAL === -->

<!-- === PRODUCT PACKAGE MODAL === -->
<div id="productPackageModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    <strong>Package Size</strong>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="ppkg_balance" />
                <input type="hidden" id="ppkg_ivp_id" />
                <input type="hidden" id="ppkg_temp_quantity" />
                <input type="hidden" id="ppkg_product_seq_hidden" />
                <input type="hidden" id="ppkg_product_id_hidden" />
                <input type="hidden" id="ppkg_order_id_hidden" />
                <input type="hidden" id="ppkg_lot_size" />
                <input type="hidden" id="ppkg_lot_no"/>
                <div class="row">
                    <div class="col-sm-2 border-right">
<?php echo lang('product_id', 'product_id'); ?>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="ppkg_product_id" readonly />
                    </div>
                    <div class="col-sm-2 border-right">
<?php echo lang('product_name', 'product_name'); ?>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="ppkg_product_name" readonly />
                    </div>
                </div>

                <div class="row mt-1">
                    <div class="col-sm-2 border-right">
<?php echo lang('lot_no', 'lot_no'); ?>
                    </div>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="ppkg_lot_no_1" maxlength="35"/>
                        <input type="text" class="form-control" id="ppkg_lot_no_2" maxlength="35"/>
                        <input type="text" class="form-control" id="ppkg_lot_no_3" maxlength="35"/>
                        <br>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-2 border-right">
<?php echo lang('ppkg_lot_size', 'ppkg_lot_size'); ?>
                    </div>
                    <div class="col-sm-10">

                        <div>
                            <span style="font-weight: bold;">Product Lot Size:</span>
                            <span style="font-weight: bold; width: 100px; display: inline-block; color: blue;" id="ppkg_max_lot_size"></span>
                            <span style="font-weight: bold;">Balance:</span>
                            <span style="font-weight: bold; width: 100px; display: inline-block; color: blue;" id="ppkg_balance_display"></span>
                        </div>

                        <br>

                        <table id="ppkg_lotitems_table" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th style="text-align: right;">Lot Size</th>
                                    <th colspan="2" style="text-align: right;">Num</th>
                                    <th colspan="2" style="text-align: right;">Quantity</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td style="text-align: right;"><span id="sum_ppkg_lot_size">Lot Size</span></td>
                                    <td colspan="2" style="text-align: right;"><span id="sum_ppkg_lot_num">Num</span></td>
                                    <td colspan="2" style="text-align: right;"><span id="sum_ppkg_quantity">Quantity</span></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>

                        <br>
                        <div class="text-right">
                            <button type="button" class="btn btn-default btn-sm" id="ppkgAddButton" onclick="onclickAddLotitem()">Add</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary-pz" id="ppkgSaveButton" onclick="onsaveProductPackage()">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- === END PRODUCT PACKAGE MODAL === -->