<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['customer_id']         = 'Customer ID';
$lang['customer_name']         = 'Customer Name';
$lang['short_name']         = 'Short Name';
$lang['business_type']         = 'Business Type';
$lang['customer_type']         = 'Customer Type';
$lang['credit']         = 'Credit';
$lang['discount']         = 'Discount';

$lang['mobile']         = 'Mobile';
$lang['phone']         = 'Phone';
$lang['fax']         = 'Fax';
$lang['facebook']         = 'Facebook';
$lang['line']         = 'Line';
$lang['contact']         = 'Contact';
$lang['website']         = 'Website';
$lang['email']         = 'Email';
$lang['address']         = 'Address';

$lang['internal_note']         = 'Internal Note';

$lang['product_id']         = 'Product ID/Part No.';
$lang['desc1']         = 'Discription 1/Part Name';
$lang['desc2']         = 'Discription 2';
$lang['boi_desc']         = 'BOI Discription';
$lang['product_type']         = 'Product Type';
$lang['invoice_type']         = 'Invoice Type';
$lang['process']         = 'Process';
$lang['domestic_price']         = 'Local Price';
$lang['export_price']         = 'BOI Price';
$lang['carton']         = 'Carton';
$lang['unit_price_type']         = 'Currency';
$lang['lot_size']         = 'Lot Size (Carton)';
$lang['carton_temp']         = 'Carton Temporary';
$lang['unit_price'] = 'Unit Price';

$lang['order_id']         = 'Order ID';
$lang['order_type']         = 'Order Type';
$lang['order_date']         = 'Order Date';
$lang['delivery_date']         = 'Delivery Date';

$lang['invoice_id']         = 'Invoice ID';
$lang['invoice_date']         = 'Invoice Date';
$lang['delivery_address']         = 'Delivery Address';

$lang['product_name']         = 'Product Name';
$lang['lot_no']         = 'Lot NO';
$lang['ppkg_lot_size']         = 'Lot Size';
$lang['ppkg_max_lot_size']         = 'Max Lot Size';
$lang['ppkg_balance']         = 'Balance';

$lang['status']         = 'Status';

$lang['invoice_type_id']         = 'Invoice Type ID';
$lang['invoice_type_name']         = 'Invoice Type Name';

$lang['continent']         = 'Continent';
$lang['country']         = 'Country';

$lang['billing_id']         = 'Billing Note ID';
$lang['billing_date']         = 'Billing Date';
$lang['receipt_id']         = 'Receipt ID';
$lang['receipt_date']         = 'Receipt Date';
$lang['customer_credit']         = 'Credit Term';
$lang['customer_address']         = 'Customer Address';

$lang['refer_invoice_id']         = 'Refer Invoice ID';