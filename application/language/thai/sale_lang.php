<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['customer_id']         = 'ไอดีลูกค้า';
$lang['customer_name']         = 'ชื่อลูกค้า';
$lang['short_name']         = 'ชื่อย่อ';
$lang['business_type']         = 'ประเภทธุรกิจ';
$lang['customer_type']         = 'ประเภทลูกค้า';
$lang['credit']         = 'เครดิต';
$lang['discount']         = 'ส่วนลด';

$lang['mobile']         = 'มือถือ';
$lang['phone']         = 'โทรศัพท์';
$lang['fax']         = 'แฟกซ์';
$lang['facebook']         = 'เฟสบุ๊ค';
$lang['line']         = 'ไลน์';
$lang['contact']         = 'ติดต่อ';
$lang['website']         = 'เว็บไซต์';
$lang['email']         = 'อีเมล์';
$lang['address']         = 'ที่อยู่';

$lang['internal_note']         = 'Internal Notes';

$lang['product_id']         = 'Product ID/Part No.';
$lang['desc1']         = 'Discription 1/Part Name';
$lang['desc2']         = 'Discription 2';
$lang['boi_desc']         = 'BOI Discription';
$lang['product_type']         = 'Product Type';
$lang['invoice_type']         = 'Invoice Type';
$lang['process']         = 'Process';
$lang['domestic_price']         = 'Local Price';
$lang['export_price']         = 'BOI Price';
$lang['carton']         = 'Carton';
$lang['unit_price_type']         = 'Currency';
$lang['lot_size']         = 'Lot Size (Carton)';
$lang['carton_temp']         = 'Carton Temporary';
$lang['unit_price'] = 'Unit Price';

$lang['order_id']         = 'Order ID';
$lang['order_type']         = 'Order Type';
$lang['order_date']         = 'Order Date';
$lang['delivery_date']         = 'Delivery Date';

$lang['invoice_id']         = 'Invoice ID';
$lang['invoice_date']         = 'Invoice Date';
$lang['delivery_address']         = 'Delivery Address';

$lang['product_name']         = 'Product Name';
$lang['lot_no']         = 'Lot NO';
$lang['ppkg_lot_size']         = 'Lot Size';
$lang['ppkg_max_lot_size']         = 'Max Lot Size';
$lang['ppkg_balance']         = 'Balance';

$lang['status']         = 'สถานะ';

$lang['invoice_type_id']         = 'Invoice Type ID';
$lang['invoice_type_name']         = 'Invoice Type Name';

$lang['continent']         = 'ทวีป';
$lang['country']         = 'ประเทศ';

$lang['billing_id']         = 'Billing Note ID';
$lang['billing_date']         = 'Billing Date';
$lang['receipt_id']         = 'Receipt ID';
$lang['receipt_date']         = 'Receipt Date';
$lang['customer_credit']         = 'Credit Term';
$lang['customer_address']         = 'Customer Address';

$lang['refer_invoice_id']         = 'Refer Invoice ID';