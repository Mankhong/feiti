<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['upload_dir']           = 'upload/images/';
$config['upload_path']          = $_SERVER['DOCUMENT_ROOT'] . str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']) . $config['upload_dir'];
$config['allowed_types']        = 'gif|jpg|jpeg|png';
$config['max_size']             = 10240; //10MB
$config['encrypt_name']         = TRUE;