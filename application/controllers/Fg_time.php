<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'libraries/Excel.php';
require_once('Common.php');

class Fg_time extends CI_Controller {

	public function __construct()
	{
	parent::__construct();
	$this->load->model(array('fg_time_model', 'fg_employee_model'));
	//$this->load->model(array('billing_model','customer_model'));
	}

	public function index()
	{
		//== Authen
		$user = $this->ion_auth->user()->row();
		$login_data = Common::authen_personal($this, $user);
		//==

		$is_admin = $login_data['is_admin'];
		$is_member = $login_data['is_member'];
		$is_account = $login_data['is_account'];
		$is_sub_contract = $login_data['is_sub_contract'];

		$data['is_admin'] = $is_admin;
		$data['is_member'] = $is_member;
		$data['is_account'] = $is_account;
		$data['is_sub_contract'] = $is_sub_contract;

		//Default criteria
		if ($is_member) {
			$employee_no = $user->email;
			$employee = $this->fg_employee_model->get_employees(NULL, $employee_no)[0];

			if (!empty($employee['resign_date'])) {
				$this->ion_auth->logout();
				$this->session->set_flashdata('message', 'No permission to login');
				redirect('auth/login', 'refresh');
			}

			$data['employee_no'] = $employee_no;
		}

		$data['checkinout_list'] = $this->fg_time_model->get_fg_checkinout();

		//-- Export Excel
		$params = array('year', 'month', 'employeeNo', 'employeeName');
		if ($is_admin || $is_sub_contract) {
			$data['export_excel'] = Common::html_export_component('export_excel_form', 'exportExcel', $params,  base_url('fg_time/export_excel'), 'Export Excel', 'btn-excel-pz');
		}

		//--- Messages
		$data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

		//=== INPUTS
		/*
		$data['leave_type'] = '';
		$data['leave_type_dropdown_default'] = '';
		$data['leave_type_dropdown_data'] = Common::array_to_options($this->common_model->get_leave_type(), 'code', 'name', ' ');
		*/

		//=== TEMPLATE
		//$create_button = Common::html_button('btn_create', '<i class="fas fa-plus-circle fa-sm"></i> Create', 'btn-primary-pz text-light', 'billing/create', '');
		//$data['action_buttons'] = $create_button;
		$data['title'] = 'Timesheet Employee Report';
		$data['hide_sub_topbar'] = TRUE;
		$data['display_small_topbar'] = TRUE;
		$data['display_action_search'] = FALSE;
		$data['user'] = $user;
		$data['menu_active_201'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/fg/fg.css'));
		$this->template->stylesheet->add(base_url('assets/css/fg/fg_time.css'));
		$this->template->javascript->add(base_url('assets/js/fg/fg_time.js'));
		$this->template->javascript->add(base_url('assets/js/sale/sale.js'));
		$this->template->content->view('fg_time', $data);
		$this->template->publish();
	}

	public function get_employee_timesheet_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$ems_no = Common::decodeString($this->input->get('ems_no'));
			$year = $this->input->get('year');
			$month = $this->input->get('month');
			$resp['data'] = $this->fg_time_model->get_employee_timesheet($ems_no, $year, $month);
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

	public function get_employee_info_ajax()
	{
		$resp = array();
		$success = FALSE;
		$message = 'success';

		if ($this->ion_auth->logged_in()) {
			$user = $this->ion_auth->user()->row();
			$ems_no = Common::decodeString($this->input->get('ems_no'));
			$year = $this->input->get('year');
			$month = $this->input->get('month');

			if ($this->admin_model->is_personal_sub_contract($user->id) && !Common::starts_with($ems_no, 'FP')) {
				$message = 'คุณสามารถดูข้อมูลของพนักงานที่รหัสขึ้นต้นด้วย FP เท่านั้น';
				$sucess = FALSE;
			} else {
				$resp['data'] = $this->fg_employee_model->get_employee_by_ems_no($ems_no);
				$resp['monthly_data'] = $this->fg_time_model->get_monthly_data($ems_no, $year, $month);
				$success = TRUE;
			}
		}

		$resp['success'] = $success;
		$resp['message'] = $message;
		return $this->output
					->set_content_type('application/json')
					->set_output(json_encode($resp));
	}

	public function update_employee_timesheet_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$id = $this->input->post('id');
			$update_data = array(
				'start_work' => $this->input->post('startWork'),
				'end_work' => $this->input->post('endWork'),
				'total_hours' => $this->input->post('totalHours'),
				'working_day' => $this->input->post('workingDay'),
				'night_shift' => $this->input->post('nightShift'),
				'food_travel_day' => $this->input->post('foodTravelDay'),
				'ot1' => $this->input->post('ot1'),
				'ot15' => $this->input->post('ot15'),
				'ot2' => $this->input->post('ot2'),
				'ot3' => $this->input->post('ot3'),
				'remark' => $this->input->post('remark'));

			$this->fg_time_model->update_employee_timesheet($id, $update_data);
			$success = TRUE;
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

	public function create_or_update_diligence_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$uid = $this->ion_auth->user()->row()->id;
			$id = $this->input->post('id');
			$ems_no = $this->input->post('ems_no');
			$year = $this->input->post('year');
			$month = $this->input->post('month');
			$diligence = $this->input->post('diligence');
			$data = array(
				'ems_no' => $ems_no,
				'year' => $year,
				'month' => $month,
				'diligence' => $diligence,
			);

			$current_data = $this->fg_time_model->get_monthly_data($ems_no, $year, $month);

			if (empty($current_data)) {
				$this->fg_time_model->insert_monthly_data($data, $uid);
			} else {
				$this->fg_time_model->update_monthly_data($ems_no, $year, $month, $data, $uid);
			}
			
			$success = $uid;
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

	public function export_excel() {
		$ems_no = Common::decodeString($this->input->post('employeeNo'));
		$ems_name = $this->input->post('employeeName');
		$year = $this->input->post('year');
		$month = $this->input->post('month');

		$info = $this->fg_employee_model->get_employee_by_ems_no($ems_no);
		$data_arr = $this->fg_time_model->get_employee_timesheet($ems_no, $year, $month);
		$monthly_data = $this->fg_time_model->get_monthly_data($ems_no, $year, $month);
		$ems_no = "";
		$diligence = (!empty($monthly_data) ? $monthly_data[0]["diligence"] : "");

        $objPHPExcel = new PHPExcel();

		//Set cell width
		$column_widths = array(3, 11, 6, 6, 15.5, 6, 6, 5, 5, 5, 5, 10);
		$columns = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L");
		$column_names_1 = array("No.", "Date", "Start Work", "End Work", "Leave Type", "Night Shift", "Food Travel", "OverTime", "", "", "", "Approve by");
		$column_names_2 = array("", "", "", "", "", "", "", "1.00 ", "1.50 ", "2.00 ", "3.00 ", "");
		$column_count = count($columns);

		$objPHPExcel->setActiveSheetIndex(0);
		
		//Column Width
		for ($i = 0; $i < $column_count; $i++) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($columns[$i])->setWidth($column_widths[$i]);
		}

        //-- Header Style
        //$objPHPExcel->getActiveSheet()->getStyle($columns[0] . "1:" . $columns[count($columns)-1] . "6")->getFont()->setBold(true);

        //-- Header
        $header = "Timesheet";
        $objPHPExcel->getActiveSheet()->mergeCells($columns[0] . "1:" . $columns[count($columns)-1] . "1");
		$objPHPExcel->getActiveSheet()->setCellValue("A1", $header);

		//-- Merge Cells
		$merg_cells = array("A2:B2", "C2:D2", "E2:F2", "G2:H2","A3:B3", "C3:D3", "E3:F3", "G3:H3", "A4:B4", "C4:D4", "E4:F4", "G4:H4",
							 "H6:K6", "A6:A7", "B6:B7", "C6:C7", "D6:D7", "E6:E7", "F6:F7", "G6:G7", "L6:L7");
		foreach ($merg_cells as $mc) {
			$objPHPExcel->getActiveSheet()->mergeCells($mc);
		}
		
		//-- Info
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("A2", "Employee No.");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("E2", "Employee Type");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("A3", "Employee Name Thai");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("E3", "Department");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("A4", "Shift");

		if (!empty($info)) {
			$ems_no = $info[0]["ems_no"];
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C2", $ems_no);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("G2", $info[0]["employee_type_name"]);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C3", $info[0]["fullname_th"]);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("G3", $info[0]["department_id"]);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C4", $info[0]["shift_name"]);
		}

        //-- Table Header Style
		$header_columns = $columns[0] . "1:" . $columns[count($columns)-1] . "7";
        $objPHPExcel->getActiveSheet()->getStyle($header_columns)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->getActiveSheet()->getStyle($header_columns)->getFont()->setBold(true);
		
		//Info Style
		$header_columns = $columns[2] . "1:" . $columns[2] . "4";
        $objPHPExcel->getActiveSheet()->getStyle($header_columns)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->getActiveSheet()->getStyle($header_columns)->getFont()->setBold(false);
		
		$header_columns = $columns[6] . "1:" . $columns[6] . "4";
        $objPHPExcel->getActiveSheet()->getStyle($header_columns)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle($header_columns)->getFont()->setBold(false);

		//-- Table Header
		$row = 6;
		$bc = '000000';
		$border_style= array('borders' => array(
			'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => $bc)),
			'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => $bc)),
			'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => $bc)),
			'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => $bc))));

		for ($i = 0; $i < $column_count; $i++) {
			$objPHPExcel->getActiveSheet()->setCellValue($columns[$i] . $row, $column_names_1[$i]);
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			//$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DDDDDD');
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->applyFromArray($border_style);
		}

		$row++;
		for ($i = 0; $i < $column_count; $i++) {
			$objPHPExcel->getActiveSheet()->setCellValue($columns[$i] . $row, $column_names_2[$i]);
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			//$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DDDDDD');
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->applyFromArray($border_style);
		}

		//Value Keys
		$value_keys = array(NULL, "date", "start_work", "end_work", "leave_type", "night_shift", "food_travel_day", "ot1", "ot15", "ot2", "ot3", NULL);

		//-- Table Body
		$total_night_shift = 0;
		$total_food_travel_day = 0;
		$total_ot1 = 0;
		$total_ot15 = 0;
		$total_ot2 = 0;
		$total_ot3 = 0;

		if (!empty($data_arr)) {
			$row++;
			$no = 1;
			foreach ($data_arr as $value) {

				//Column Value
				for ($i = 0; $i < $column_count; $i++) {
					$k = $value_keys[$i];
					$v = NULL;

					//Value
					if ($i == 0) { //No.
						$v = ($row - 7);
					} 
					/*else if ($i == 4) {
						$arr = explode(' ', $value[$k]);
						$v = $arr[0];
					}*/
					else {
						$v = ($k == NULL ? "" : $value[$k]);
					}

					//Value Format
					if (($i > 4 && $i < 11)) {
						$v = Common::parse_numeric($v);
					}

					//Total
					if ($i == 5) {
						$total_night_shift += $v;
					} else if ($i == 6) {
						$total_food_travel_day += $v;
					} else if ($i == 7) {
						$total_ot1 += $v;
					} else if ($i == 8) {
						$total_ot15 += $v;
					} else if ($i == 9) {
						$total_ot2 += $v;
					} else if ($i == 10) {
						$total_ot3 += $v;
					}

					$v = Common::zero_to_empty($v);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue($columns[$i] . $row, $v);

					//Cell Style
					$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->applyFromArray($border_style);
					if ($i > 0 && $i < 5) {
						$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					}
				}
				$row++;
			}
		}

		//Cell Style
		$objPHPExcel->getActiveSheet()->getStyle("A6:" . "L" . $row)->getAlignment()->setWrapText(true);

		//-- Total
		$objPHPExcel->getActiveSheet()->mergeCells("A" . $row . ":E" . $row);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("A" . $row, "Total");
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("F" . $row, $total_night_shift);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("G" . $row, $total_food_travel_day);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("H" . $row, $total_ot1);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("I" . $row, $total_ot15);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("J" . $row, $total_ot2);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue("K" . $row, $total_ot3);

		//-- Total Style
		$objPHPExcel->getActiveSheet()->getStyle("A" . $row . ":L" . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("A" . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()->getStyle("A" . $row . ":E" .$row)->applyFromArray($border_style);
		$objPHPExcel->getActiveSheet()->getStyle("G" . $row)->applyFromArray($border_style);
		$objPHPExcel->getActiveSheet()->getStyle("H" . $row)->applyFromArray($border_style);
		$objPHPExcel->getActiveSheet()->getStyle("I" . $row)->applyFromArray($border_style);
		$objPHPExcel->getActiveSheet()->getStyle("J" . $row)->applyFromArray($border_style);
		$objPHPExcel->getActiveSheet()->getStyle("K" . $row)->applyFromArray($border_style);
		$objPHPExcel->getActiveSheet()->getStyle("L" . $row)->applyFromArray($border_style);

		//-- Diligence
		$row++;
		$objPHPExcel->getActiveSheet()->mergeCells("A" . $row . ":E" . $row);
		$objPHPExcel->getActiveSheet()->setCellValue("A" . $row, "Diligence");
		$objPHPExcel->getActiveSheet()->setCellValue("F" . $row, $diligence);

		//-- Diligence Style
		$objPHPExcel->getActiveSheet()->getStyle("A" . $row . ":E" . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("A" . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()->getStyle("A" . $row . ":E" .$row)->applyFromArray($border_style);
		$objPHPExcel->getActiveSheet()->getStyle("F" . $row)->applyFromArray($border_style);

		/*
		//-- Export Date
		$row++;
		$objPHPExcel->getActiveSheet()->getStyle("A" . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("A" . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue("A" . $row, Common::get_current_date());
		$objPHPExcel->getActiveSheet()->mergeCells("A" . $row . ":B" . $row);
		*/

        ob_end_clean();

        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=" . Common::generate_report_name('Timesheet_' . $ems_no, 'xlsx'));
        header("Cache-Control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objWriter->save("php://output");
	}

	public function calculate_timesheet_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			//echo '>>>' . $this->input->post('p1');
			$year = $this->input->get('year');
			$month = $this->input->get('month');
			//$sensor_user_id = $this->input->get('sensor_user_id');

			echo '>>> year' . $year . ' >>> month ' . $month;
			$badgenumber_arr = $this->fg_time_model->get_checkinout_badgenumbers();

			for ($i = 0; $i < count($badgenumber_arr); $i++) {
				//if ($badgenumber_arr[$i]['badgenumber'] == '153') {
					$resp['data'] = $this->fg_time_model->calculate_timesheet($year, $month, $badgenumber_arr[$i]['badgenumber']);
				//}
			}

			$success = TRUE;
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

	public function migrate_checkinout_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$resp['data'] = $this->fg_time_model->migrate_checkinout();
			$success = TRUE;
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}
	
}
