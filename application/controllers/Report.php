<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once('Common.php');

class Report extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('upload'));
        $this->load->model(array('customer_model', 'report_model', 'invoice_model', 'billing_model', 'product_model'));
        $this->config->load('upload');
    }

    public function index($id = '') {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }


        if (!empty($id)) {
            $invoice_id = $id;
            $data['invoice_id'] = $invoice_id;
        }
        //--- Messages
        $data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
        $data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

        //=== TEMPLATE
        $create_button = Common::html_button('btn_print', '<i class="fa fa-print fa-sm"></i> Print', 'btn-primary-pz text-light', '', '', 'text-right');
        $data['action_buttons'] = $create_button;

        $data['title'] = 'Report';
        $data['display_action_search'] = FALSE;

        $user = $this->ion_auth->user()->row();
        $data['user'] = $user;

        $this->template->javascript->add(base_url('assets/js/sale/jquery.PrintArea.js'));
        $this->template->javascript->add(base_url('assets/js/sale/i_report_1.js'));
        $this->template->stylesheet->add(base_url('assets/css/sale/i_report1.css'));
        $this->template->stylesheet->add(base_url('assets/css/sale/dataitems.css'));
        $this->template->javascript->add(base_url('assets/js/sale/dataitems.js'));

        $this->template->javascript->add(base_url('assets/font/angsa.ttf'));
        $this->template->javascript->add(base_url('assets/font/angsab.ttf'));

        $this->template->content->view('i_report1', $data);
        $this->template->publish();
    }

    public function index_os($id = '') {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }


        if (!empty($id)) {
            $invoice_id = $id;
            $data['invoice_id'] = $invoice_id;
        }
        //--- Messages
        $data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
        $data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

        //=== TEMPLATE
        $create_button = Common::html_button('btn_print', '<i class="fa fa-print fa-sm"></i> Print', 'btn-primary-pz text-light', '', '', 'text-right');
        $data['action_buttons'] = $create_button;

        $data['title'] = 'Report';
        $data['display_action_search'] = FALSE;

        $user = $this->ion_auth->user()->row();
        $data['user'] = $user;

        $this->template->javascript->add(base_url('assets/js/sale/jquery.PrintArea.js'));
        $this->template->javascript->add(base_url('assets/js/sale/i_report_1_os.js'));
        $this->template->stylesheet->add(base_url('assets/css/sale/i_report1.css'));
        $this->template->stylesheet->add(base_url('assets/css/sale/dataitems.css'));
        $this->template->javascript->add(base_url('assets/js/sale/dataitems.js'));

        $this->template->javascript->add(base_url('assets/font/angsa.ttf'));
        $this->template->javascript->add(base_url('assets/font/angsab.ttf'));

        $this->template->content->view('i_report1', $data);
        $this->template->publish();
    }

    //*** report **/
    public function iv_report($encode_id) {

        $encode_id = base64_decode(urldecode($encode_id));
        $this->index($encode_id);
    }

    public function iv_report_os($encode_id) {

        $encode_id = base64_decode(urldecode($encode_id));
        $this->index_os($encode_id);
    }

    //*** delivery order **/
    public function iv_delivery_report($encode_id) {
        $encode_id = base64_decode(urldecode($encode_id));
        $this->derivery_report($encode_id);
    }

    public function iv_delivery_report_os($encode_id) {
        $encode_id = base64_decode(urldecode($encode_id));
        $this->derivery_report_os($encode_id);
    }

    public function bl_report($encode_id) {
        $encode_id = base64_decode(urldecode($encode_id));
        $this->billing_report($encode_id);
    }

    public function bl_receipt_report($encode_id) {
        $encode_id = base64_decode(urldecode($encode_id));
        $this->receipt_report($encode_id);
    }

    public function bl_receipt_report_os($encode_id) {
        $encode_id = base64_decode(urldecode($encode_id));
        $this->receipt_report_os($encode_id);
    }

    public function get_detail_report() {
        $resp = array();
        $success = FALSE;

        $oinvoce_id = $this->input->post('invoice_id');
        if ($this->ion_auth->logged_in()) {
            $resp['data'] = $this->report_model->get_data_detail_report($oinvoce_id, '');
        }

        $resp['success'] = $success;
        return $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode($resp));
    }

    public function get_invoice_list() {
        $resp = array();
        $success = FALSE;

        $invoice_id = $this->input->post('invoice_id');
        if ($this->ion_auth->logged_in()) {
            $resp['data'] = $this->get_invoice_products_list($invoice_id);
        }

        $resp['success'] = $success;
        return $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode($resp));
    }

    public function get_report_2_list() {
        $resp = array();
        $success = FALSE;

        if ($this->ion_auth->logged_in()) {
            $invoice_id = $this->input->post('invoice_id');
            $resp['data'] = $this->report_model->get_report_2_data($invoice_id);
            $success = TRUE;
        }

        $resp['success'] = $success;
        return $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode($resp));
    }

    public function get_delevery_list() {
        $resp = array();
        $success = FALSE;

        $invoice_id = $this->input->post('invoice_id');
        if ($this->ion_auth->logged_in()) {
            $resp['data'] = $this->get_invoice_products_list($invoice_id);
        }

        $this->template->stylesheet->add(base_url('assets/css/sale/dataitems.css'));
        $this->template->javascript->add(base_url('assets/js/sale/dataitems.js'));
        $resp['success'] = $success;
        return $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode($resp));
    }

    public function get_invoice_products_list($invoice_id) {
        return $this->invoice_model->get_invoice_products($invoice_id);
    }

    public function derivery_report($id = '') {

        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }

        if (!empty($id)) {
            $invoice_id = $id;
            $data['invoice_id'] = $invoice_id;
        }

        //--- Messages
        $data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
        $data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

        //=== TEMPLATE
        $create_button = Common::html_button('btn_print', '<i class="fa fa-print fa-sm"></i> Print', 'btn-primary-pz text-light', '', '', 'text-right');
        $data['action_buttons'] = $create_button;

        $data['title'] = 'Report';
        $data['display_action_search'] = FALSE;

        $user = $this->ion_auth->user()->row();
        $data['user'] = $user;

        $this->template->javascript->add(base_url('assets/js/sale/jquery.PrintArea.js'));
        $this->template->javascript->add(base_url('assets/js/sale/i_report_2.js'));
        $this->template->stylesheet->add(base_url('assets/css/sale/i_report2.css'));
        $this->template->stylesheet->add(base_url('assets/css/sale/dataitems.css'));
        $this->template->javascript->add(base_url('assets/js/sale/dataitems.js'));

        $this->template->content->view('i_report2', $data);
        $this->template->publish();
    }

    public function derivery_report_os($id = '') {

        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }

        if (!empty($id)) {
            $invoice_id = $id;
            $data['invoice_id'] = $invoice_id;
        }

        //--- Messages
        $data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
        $data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

        //=== TEMPLATE
        $create_button = Common::html_button('btn_print', '<i class="fa fa-print fa-sm"></i> Print', 'btn-primary-pz text-light', '', '', 'text-right');
        $data['action_buttons'] = $create_button;

        $data['title'] = 'Report';
        $data['display_action_search'] = FALSE;

        $user = $this->ion_auth->user()->row();
        $data['user'] = $user;

        $this->template->javascript->add(base_url('assets/js/sale/jquery.PrintArea.js'));
        $this->template->javascript->add(base_url('assets/js/sale/i_report_2_os.js'));
        $this->template->stylesheet->add(base_url('assets/css/sale/i_report2.css'));
        $this->template->stylesheet->add(base_url('assets/css/sale/dataitems.css'));
        $this->template->javascript->add(base_url('assets/js/sale/dataitems.js'));

        $this->template->content->view('i_report2', $data);
        $this->template->publish();
    }

    public function billing_report($id = '') {



        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }

        if (!empty($id)) {
            $billing_id = $id;
            $data['billing_id'] = $billing_id;
        }

        //--- Messages
        $data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
        $data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

        //=== TEMPLATE
        $create_button = Common::html_button('btn_print', '<i class="fa fa-print fa-sm"></i> Print', 'btn-primary-pz text-light', '', '', 'text-right');
        $data['action_buttons'] = $create_button;

        $data['title'] = 'Report';
        $data['display_action_search'] = FALSE;

        $user = $this->ion_auth->user()->row();
        $data['user'] = $user;

        //=== GET DATA

        $billing = $this->billing_model->get_billings($id)[0];
        $customer = $this->customer_model->get_customers($billing['customer_id'])[0];

        $data['customer_name'] = $customer['name'];
        $data['customer_address'] = $customer['address'];
        $data['receipt_date'] = $billing['billing_date'];

        $this->template->javascript->add(base_url('assets/js/sale/jquery.PrintArea.js'));
        $this->template->javascript->add(base_url('assets/js/sale/i_reportBilling.js'));
        $this->template->stylesheet->add(base_url('assets/css/sale/i_reportBilling.css'));
        $this->template->stylesheet->add(base_url('assets/css/sale/dataitems.css'));
        $this->template->javascript->add(base_url('assets/js/sale/dataitems.js'));

        $this->template->content->view('i_reportBilling', $data);
        $this->template->publish();
    }

    public function receipt_report($id = '') {



        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }

        if (!empty($id)) {
            $billing_id = $id;
            $data['billing_id'] = $billing_id;
        }




        //--- Messages
        $data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
        $data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

        //=== TEMPLATE
        $create_button = Common::html_button('btn_print', '<i class="fa fa-print fa-sm"></i> Print', 'btn-primary-pz text-light', '', '', 'text-right');
        $data['action_buttons'] = $create_button;

        $data['title'] = 'Report';
        $data['display_action_search'] = FALSE;

        $user = $this->ion_auth->user()->row();
        $data['user'] = $user;

        //=== GET DATA

        $billing = $this->billing_model->get_billings($id)[0];
        $customer = $this->customer_model->get_customers($billing['customer_id'])[0];

        $data['customer_name'] = $customer['name'];
        $data['customer_address'] = $customer['address'];
        $data['receipt_date'] = $billing['receipt_date'];

        $this->template->javascript->add(base_url('assets/js/sale/jquery.PrintArea.js'));
        $this->template->javascript->add(base_url('assets/js/sale/i_reportReceipt.js'));
        $this->template->stylesheet->add(base_url('assets/css/sale/i_reportReceipt.css'));
        $this->template->stylesheet->add(base_url('assets/css/sale/dataitems.css'));
        $this->template->javascript->add(base_url('assets/js/sale/dataitems.js'));

        $this->template->content->view('i_receiptReport', $data);
        $this->template->publish();
    }

    public function receipt_report_os($id = '') {



        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }

        if (!empty($id)) {
            $billing_id = $id;
            $data['billing_id'] = $billing_id;
        }




        //--- Messages
        $data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
        $data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

        //=== TEMPLATE
        $create_button = Common::html_button('btn_print', '<i class="fa fa-print fa-sm"></i> Print', 'btn-primary-pz text-light', '', '', 'text-right');
        $data['action_buttons'] = $create_button;

        $data['title'] = 'Report';
        $data['display_action_search'] = FALSE;

        $user = $this->ion_auth->user()->row();
        $data['user'] = $user;

        //=== GET DATA

        $billing = $this->billing_model->get_billings($id)[0];
        $customer = $this->customer_model->get_customers($billing['customer_id'])[0];

        $data['customer_name'] = $customer['name'];
        $data['customer_address'] = $customer['address'];
        $data['receipt_date'] = $billing['receipt_date'];

        $this->template->javascript->add(base_url('assets/js/sale/jquery.PrintArea.js'));
        $this->template->javascript->add(base_url('assets/js/sale/i_reportReceipt_os.js'));
        $this->template->stylesheet->add(base_url('assets/css/sale/i_reportReceipt.css'));
        $this->template->stylesheet->add(base_url('assets/css/sale/dataitems.css'));
        $this->template->javascript->add(base_url('assets/js/sale/dataitems.js'));

        $this->template->content->view('i_receiptReport', $data);
        $this->template->publish();
    }

    public function get_data_billing_invoices() {

        $resp = array();
        $success = FALSE;
        $html = '';
        $receipt_id = '';
        $invoice_ids = '';
        $total = 0;
        $billing_id = $this->input->post('billing_id');
        if ($this->ion_auth->logged_in()) {
            $data_arr = $this->billing_model->get_billing_invoices($billing_id);
            for ($i = 0; $i < count($data_arr); $i++) {
                if ($i == 0) {
                    $receipt_id = $data_arr[$i]['receipt_id'];
                } else {
                    $invoice_ids .= ', ';
                }
                $invoice_ids .= $data_arr[$i]['invoice_id'];
                $total += floatval($data_arr[$i]['total_amount']);
            } 
        }

        $this->template->stylesheet->add(base_url('assets/css/sale/dataitems.css'));
        $this->template->javascript->add(base_url('assets/js/sale/dataitems.js'));

        $resp['data'] = $data_arr;
        $resp['receipt_id'] = $receipt_id;
        $resp['invoice_ids'] = $invoice_ids;
        $resp['total'] = $total;
        $resp['success'] = $success;
        return $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode($resp));
    }

    public function get_receipts() {

        $resp = array();
        $success = FALSE;
        $billing_id = $this->input->post('billing_id');
        if ($this->ion_auth->logged_in()) {
            $resp['data'] = $this->report_model->get_data_receipts($billing_id);
        }

        $this->template->stylesheet->add(base_url('assets/css/sale/dataitems.css'));
        $this->template->javascript->add(base_url('assets/js/sale/dataitems.js'));

        $resp['success'] = $success;
        return $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode($resp));
    }

    public function r3() {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }

        //--- Messages
        $data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
        $data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

        //=== TEMPLATE
        $data['title'] = 'Report';
        $data['display_action_search'] = FALSE;
        $this->template->stylesheet->add(base_url('assets/css/sale/i_report1.css'));

        $this->template->content->view('i_report3', $data);
        $this->template->publish();
    }

    public function sumreport() {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }

        $user = $this->ion_auth->user()->row();
        $data['user'] = $user;

        //--- Messages
        $data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
        $data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

        //=== TEMPLATE
        $data['title'] = 'Sales Report for BOI';
        $data['display_action_search'] = FALSE;
        $data['menu_active_8'] = TRUE;
        $data['invoise_types'] = $this->report_model->get_invoice_types();
        $this->template->javascript->add(base_url('assets/js/sale/i_report_sum.js'));
        $this->template->stylesheet->add(base_url('assets/css/sale/i_report_sum.css'));

//        $this->template->javascript->add(base_url('assets/js/excel/jquery.table2excel.js'));
        $this->template->javascript->add(base_url('assets/js/excel/jquery.table2excel.min.js'));

        $this->template->content->view('i_report_sum', $data);
        $this->template->publish();
    }

    public function cusreport() {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }

        $user = $this->ion_auth->user()->row();
        $data['user'] = $user;

        //--- Messages
        $data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
        $data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

        //=== TEMPLATE
        $data['title'] = 'Summary Report for BOI';
        $data['display_action_search'] = FALSE;
        $data['menu_active_9'] = TRUE;
        $data['invoise_types'] = $this->report_model->get_invoice_types();
        $this->template->javascript->add(base_url('assets/js/sale/i_report_cus.js'));
        $this->template->stylesheet->add(base_url('assets/css/sale/i_report_cus.css'));
        $this->template->javascript->add(base_url('assets/js/excel/jquery.table2excel.min.js'));
     

        $this->template->content->view('i_report_cus', $data);
        $this->template->publish();
    }
    
     public function tracking_report() {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }

        $user = $this->ion_auth->user()->row();
        $data['user'] = $user;

        //--- Messages
        $data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
        $data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

        //=== TEMPLATE
        $data['title'] = 'Tracking Order Report';
        $data['display_action_search'] = FALSE;
        $data['menu_active_10'] = TRUE;
       
        $this->template->javascript->add(base_url('assets/js/sale/tracking_report.js'));
        $this->template->stylesheet->add(base_url('assets/css/sale/tracking_report.css'));
        $this->template->javascript->add(base_url('assets/js/excel/jquery.table2excel.min.js'));
     
        $this->template->stylesheet->add(base_url('assets/css/sale/dataitems.css'));
        $this->template->javascript->add(base_url('assets/js/sale/dataitems.js'));
        
        $this->template->content->view('tracking_report', $data);
        $this->template->publish();
    }
    
    public function get_product_list_ajax() {
        $resp = array();
        $success = FALSE;


        $startDate = $this->input->post('startDate');
        $dateTimeEnd = $this->input->post('dateTimeEnd');
        $invoice_type = $this->input->post('invoice_type');
        $customerId = $this->input->post('customerId');
        $invoiceId = $this->input->post('invoiceId');
        $pathno = $this->input->post('pathno');
        $pathname = $this->input->post('pathname');
        $rate = $this->input->post('rate');
        $input_rate = $this->input->post('input_rate');
        
        if ($this->ion_auth->logged_in()) {
            $resp['data'] = $this->report_model->get_joined_products_data2($startDate, $dateTimeEnd, $customerId, $invoiceId, $invoice_type, $pathno, $pathname, $rate, $input_rate);
        }

        $resp['success'] = $success;
        return $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode($resp));
    }

    public function get_tracking_list_ajax() {
        $resp = array();
        $success = FALSE;


        $startDate = $this->input->post('startDate');
        $dateTimeEnd = $this->input->post('dateTimeEnd');
        $customerId = $this->input->post('customerId');
        $productId = $this->input->post('productId');
        $invoiceId = $this->input->post('invoiceId');
        $orderId = $this->input->post('orderId');
        
        if ($this->ion_auth->logged_in()) {
            $resp['data'] = $this->report_model->get_tracking_order_data($startDate, $dateTimeEnd, $customerId, $productId, $invoiceId, $orderId);
        }

        $resp['success'] = $success;
        return $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode($resp));
    }
    
    public function get_tracking_list_ajax_data_table() {
     


        $startDate = $this->input->post('startDate');
        $dateTimeEnd = $this->input->post('dateTimeEnd');
        $customerId = $this->input->post('customerId');
        $productId = $this->input->post('productId');
        $invoiceId = $this->input->post('invoiceId');
        $orderId = $this->input->post('orderId');
        
        $this->template->javascript->add(base_url('assets/js/sale/tracking_report.js'));
        $this->template->stylesheet->add(base_url('assets/css/sale/tracking_report.css'));
        $this->template->stylesheet->add(base_url('assets/css/sale/dataitems.css'));
        $this->template->javascript->add(base_url('assets/js/sale/dataitems.js'));
        
        
        return $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode($this->report_model->get_tracking_order_data($startDate, $dateTimeEnd, $customerId, $productId, $invoiceId, $orderId)));
    }
    
    
    
    public function get_product_list_ajax_cus() {
        $resp = array();
        $success = FALSE;


        $startDate = $this->input->post('startDate');
        $dateTimeEnd = $this->input->post('dateTimeEnd');
        $invoice_type = $this->input->post('invoice_type');
        $customerId = $this->input->post('customerId');
        $invoiceId = $this->input->post('invoiceId');
        $pathno = $this->input->post('pathno');
        $pathname = $this->input->post('pathname');
        $rate = $this->input->post('rate');

        if ($this->ion_auth->logged_in()) {
            $resp['data'] = $this->report_model->get_joined_products_data3($startDate, $dateTimeEnd, $customerId, $invoiceId, $invoice_type, $pathno, $pathname, $rate);
        }

        $resp['success'] = $success;
        return $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode($resp));
    }
    
    public function get_product_list_ajax_by_custmerid_invoicetype() {
        $resp = array();
        $success = FALSE;

        $customer_id = $this->input->post('customer_id');
        $invoice_type = $this->input->post('invoice_type');
        if ($this->ion_auth->logged_in()) {
            $resp['data'] = $this->report_model->get_joined_products_data('', $customer_id, $invoice_type);
        }

        $resp['success'] = $success;
        return $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode($resp));
    }

}
