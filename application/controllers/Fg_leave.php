<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('Common.php');

class Fg_leave extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		$this->load->model(array('fg_leave_model', 'fg_employee_model'));
    }

	public function index()
	{
		//=== AUTHEN
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}

		$user = $this->ion_auth->user()->row();
		$is_admin = $this->admin_model->is_personal_admin($user->id);
		$is_member = $this->admin_model->is_personal_member($user->id);
		$is_account = $this->admin_model->is_personal_account($user->id);
		$is_sub_contract = $this->admin_model->is_personal_sub_contract($user->id);

		$data['is_admin'] = $is_admin;
		$data['is_member'] = $is_member;
		$data['is_account'] = $is_account;

		if (!$is_admin && !$is_member && !$is_account && !$is_sub_contract) {
			$logout = $this->ion_auth->logout();
			$this->session->set_flashdata('message', 'Please login with personal user.');
			redirect('auth/login', 'refresh');
		}
		//=== END AUTHEN

		$this->session->set_flashdata('message', '');
		$this->session->set_flashdata('errorMessage', '');

		//--- Messages
		//$data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
		//$data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

		//=== TEMPLATE
		$create_button = Common::html_button('btn_create', '<i class="fas fa-plus-circle fa-sm"></i> Create', 'btn-primary-pz text-light', 'fg_leave/create', '');
		$data['action_buttons'] = $create_button;
		$data['title'] = 'Leave';
		$data['display_action_search'] = TRUE;
		$data['user'] = $user;
		$data['menu_active_206'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/fg/fg_leave.css'));
		$this->template->javascript->add(base_url('assets/js/fg/fg_leave.js'));
		$this->template->stylesheet->add(base_url('assets/css/fg/fg_employee_picker.css'));
		$this->template->javascript->add(base_url('assets/js/fg/fg_employee_picker.js'));
		$this->template->javascript->add(base_url('assets/js/sale/sale.js'));
        $this->template->content->view('fg_leave', $data);
        $this->template->publish();
	}

	public function edit($id)
	{
		//=== AUTHEN
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}

		$user = $this->ion_auth->user()->row();
		$is_admin = $this->admin_model->is_personal_admin($user->id);
		$is_member = $this->admin_model->is_personal_member($user->id);
		$is_account = $this->admin_model->is_personal_account($user->id);

		$data['is_admin'] = $is_admin;
		$data['is_member'] = $is_member;
		$data['is_account'] = $is_account;

		if (!$is_admin && !$is_member && !$is_account) {
			$logout = $this->ion_auth->logout();
			$this->session->set_flashdata('message', 'Please login with personal user.');
			redirect('auth/login', 'refresh');
		}
		//=== END AUTHEN

		$this->session->set_flashdata('message', '');
		$this->session->set_flashdata('errorMessage', '');

		//=== GET DATA
		$id = Common::decodeString($id);
		$leave_arr = $this->fg_leave_model->get_leaves($id);
		$leave = NULL;

		if (!empty($leave_arr)) {
			$leave = $leave_arr[0];
		} else {
			$this->session->set_flashdata('errorMessage', 'Not found Leave date.');
			redirect(base_url('fg_leave'), 'refresh');
		}

		//=== VALIDATION
		$this->set_leave_validations();

		//=== SUBMIT
		if ($this->input->post()) {
			if($this->form_validation->run() === TRUE) {
				$update_data = $this->get_post_leave_data();

				if (!empty($update_data['from_date'])) {
					$update_data['from_date'] = Common::convert_date_display_to_sql($update_data['from_date']);
				}

				if (!empty($update_data['to_date'])) {
					$update_data['to_date'] = Common::convert_date_display_to_sql($update_data['to_date']);
				}

				$afffected_row = $this->fg_leave_model->update_leave($leave['id'], $update_data, $user->id);
				redirect(base_url('fg_leave'), 'refresh');
			}
		}

		$employee_no = '';
		$employee_name = '';
		$employee = $this->fg_employee_model->get_employees($leave['employee_id']);

		if (!empty($employee)) {
			$employee_no = $employee[0]['ems_no'];
			$employee_name = $employee[0]['fullname_th'];
		}

		//--- HTML Inputs
		$data['id'] = Common::html_input('id', 'hidden', $leave['id']);
		$data['employee_id'] = Common::html_input('employee_id', 'hidden', $leave['employee_id'], '', '', '', TRUE);
		$data['employee_no'] = Common::html_input('employee_no', 'text', $employee_no, '', '', '', TRUE);
		$data['employee_name'] = Common::html_input('employee_name', 'text', $employee_name, '', '', '', TRUE);
		$data['from_date'] = Common::html_input('from_date', 'text', $leave['fm_from_date'], 'datepicker', '', '', TRUE);
		$data['to_date'] = Common::html_input('to_date', 'text', $leave['fm_to_date'], 'datepicker', '', '', TRUE);
		$data['reason'] = Common::html_textarea('reason', 3, 40, $leave['reason']);

		//Dropdown
		$data['type'] = $leave['type'];
		$data['type_dropdown_default'] = empty($data['type']) ? '1' : $data['type'];
		$data['type_dropdown_data'] = Common::array_to_options($this->common_model->get_leave_type(), 'code', 'name');

		$data['duration'] = $leave['duration'];
		$data['duration_dropdown_default'] = empty($data['duration']) ? '1' : $data['duration'];
		$data['duration_dropdown_data'] = Common::array_to_options($this->common_model->get_duration(), 'id', 'name');

		$data['action_type'] = Common::html_input('action_type', 'hidden', 'U');
		$data['from_date_temp'] = Common::html_input('from_date_temp', 'hidden', $leave['fm_from_date']);
		$data['to_date_temp'] = Common::html_input('to_date_temp', 'hidden', $leave['fm_to_date']);
		//--- END HTML Inputs

		//--- Messages
		$data['message'] = Common::html_alert('alert-info',$this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger',$this->session->flashdata('errorMessage'));

		//=== SUBMIT URL
		$data['submit_url'] = base_url('fg_leave/edit/' . Common::encodeString($leave['id']));
		
		//=== DISABLE
		$data['employee_disabled'] = FALSE;

		//=== CANCEL LEAVE
		$data['render_cancel_button'] = TRUE;

		//=== TEMPLATE
		$save_button = Common::html_button('btn_create', '<i class="fas fa-save"></i> Save', 'btn-primary-pz text-light', '', 'onclickSave()');
		$discard_button = Common::html_button('btn_discard', 'Cancel', 'btn-default ml-1', base_url('fg_leave'), '');
		$data['action_buttons'] = $save_button . $discard_button;	
		$data['title'] = 'Edit Leave';
		$data['user'] = $user;
		$data['menu_active_206'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/fg/fg_leave_form.css'));
		$this->template->javascript->add(base_url('assets/js/fg/fg_leave_form.js'));
		$this->template->stylesheet->add(base_url('assets/css/fg/fg_employee_picker.css'));
		$this->template->javascript->add(base_url('assets/js/fg/fg_employee_picker.js'));
		$this->template->javascript->add(base_url('assets/js/sale/sale.js'));
        $this->template->content->view('fg_leave_form', $data);
        $this->template->publish();
	}

	public function create()
	{
		//=== AUTHEN
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}

		$user = $this->ion_auth->user()->row();
		$is_admin = $this->admin_model->is_personal_admin($user->id);
		$is_member = $this->admin_model->is_personal_member($user->id);
		$is_account = $this->admin_model->is_personal_account($user->id);

		$data['is_admin'] = $is_admin;
		$data['is_member'] = $is_member;
		$data['is_account'] = $is_account;

		if (!$is_admin && !$is_member && !$is_account) {
			$logout = $this->ion_auth->logout();
			$this->session->set_flashdata('message', 'Please login with personal user.');
			redirect('auth/login', 'refresh');
		}
		//=== END AUTHEN

		$this->session->set_flashdata('message', '');
		$this->session->set_flashdata('errorMessage', '');

		//=== VALIDATION
		$this->set_leave_validations();

		//=== SUBMIT
		if ($this->input->post()) {
			if ($this->form_validation->run() === TRUE) {
				$insert_data = $this->get_post_leave_data();

				if (!empty($insert_data['from_date'])) {
					$insert_data['from_date'] = Common::convert_date_display_to_sql($insert_data['from_date']);
				}

				if (!empty($insert_data['to_date'])) {
					$insert_data['to_date'] = Common::convert_date_display_to_sql($insert_data['to_date']);
				}
				
				$afffected_row = $this->fg_leave_model->insert_leave($insert_data, $user->id);
				if ($afffected_row > 0) {
					redirect(base_url('fg_leave'), 'refresh');
				} else {
					$this->session->set_flashdata('errorMessage', 'Create fail.');
				}
			}
		}

		//=== SUBMIT URL
		$data['submit_url'] = base_url('fg_leave/create');

		//--- Messages
		$data['message'] = Common::html_alert('alert-info',$this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger',$this->session->flashdata('errorMessage'));

		//--- HTML Inputs
		$data['id'] = Common::html_input('id', 'hidden');
		$data['employee_id'] = Common::html_input('employee_id', 'hidden', $this->form_validation->set_value('employee_id'), '', '', '', TRUE);
		$data['employee_no'] = Common::html_input('employee_no', 'text', $this->form_validation->set_value('employee_no'), '', '', '', TRUE);
		$data['employee_name'] = Common::html_input('employee_name', 'text', $this->form_validation->set_value('employee_name'), '', '', '', TRUE);
		$data['from_date'] = Common::html_input('from_date', 'text', $this->form_validation->set_value('from_date'), 'datepicker', '', '', TRUE);
		$data['to_date'] = Common::html_input('to_date', 'text', $this->form_validation->set_value('to_date'), 'datepicker', '', '', TRUE);
		$data['reason'] = Common::html_textarea('reason', 3, 40, $this->form_validation->set_value('reason'));

		//Dropdown
		$data['type'] = $this->form_validation->set_value('type');
		$data['type_dropdown_default'] = empty($data['type']) ? '1' : $data['type'];
		$data['type_dropdown_data'] = Common::array_to_options($this->common_model->get_leave_type(), 'code', 'name');

		$data['duration'] = $this->form_validation->set_value('duration');
		$data['duration_dropdown_default'] = empty($data['duration']) ? '1' : $data['duration'];
		$data['duration_dropdown_data'] = Common::array_to_options($this->common_model->get_duration(), 'id', 'name');

		$data['action_type'] = Common::html_input('action_type', 'hidden', 'C');
		$data['from_date_temp'] = Common::html_input('from_date_temp', 'hidden', $this->form_validation->set_value('from_date'));
		$data['to_date_temp'] = Common::html_input('to_date_temp', 'hidden', $this->form_validation->set_value('to_date'));
		//--- END HTML Inputs

		//=== TEMPLATE
		$save_button = Common::html_button('btn_create', '<i class="fas fa-save"></i> Save', 'btn-primary-pz text-light', '', 'onclickSave()');
		$discard_button = Common::html_button('btn_discard', 'Cancel', 'btn-default ml-1', base_url('fg_leave'), '');
		$data['action_buttons'] = $save_button . $discard_button;	
		$data['title'] = 'Create Leave';
		$data['user'] = $user;
		$data['menu_active_206'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/fg/fg_leave_form.css'));
		$this->template->javascript->add(base_url('assets/js/fg/fg_leave_form.js'));
		$this->template->stylesheet->add(base_url('assets/css/fg/fg_employee_picker.css'));
		$this->template->javascript->add(base_url('assets/js/fg/fg_employee_picker.js'));
		$this->template->javascript->add(base_url('assets/js/sale/sale.js'));
        $this->template->content->view('fg_leave_form', $data);
        $this->template->publish();
	}

	public function cancel($id)
	{
		//=== AUTHEN
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}

		$user = $this->ion_auth->user()->row();
		$is_admin = $this->admin_model->is_personal_admin($user->id);
		$is_member = $this->admin_model->is_personal_member($user->id);
		$is_account = $this->admin_model->is_personal_account($user->id);

		if (!$is_admin && !$is_member && !$is_account) {
			$logout = $this->ion_auth->logout();
			$this->session->set_flashdata('message', 'Please login with personal user.');
			redirect('auth/login', 'refresh');
		}
		//=== END AUTHEN

		//=== SUBMIT
		if ($this->input->post()) {
			$this->fg_leave_model->delete_leave(Common::decodeString($id));
		}

		redirect('fg_leave', 'refresh');
	}

	private function get_post_leave_data()
	{
		return array(
			'id' => $this->input->post('id'),
			'employee_id' => $this->input->post('employee_id'),
			'type' => $this->input->post('type'),
			'duration' => $this->input->post('duration'),
			'from_date' => $this->input->post('from_date'),
			'to_date' => $this->input->post('to_date'),
			'reason' => $this->input->post('reason'),
		);
	}

	private function set_leave_validations()
	{
		$this->form_validation->set_rules('id', $this->lang->line('leave_id'), 'min_length[0]');
		$this->form_validation->set_rules('employee_id', $this->lang->line('employee_id'), 'required');
		$this->form_validation->set_rules('employee_no', $this->lang->line('employee_no'), 'required');
		$this->form_validation->set_rules('employee_name', $this->lang->line('employee_no'), 'min_length[0]');
		$this->form_validation->set_rules('type', $this->lang->line('leave_type'), 'required');
		$this->form_validation->set_rules('duration', $this->lang->line('duration'), 'required');
		$this->form_validation->set_rules('from_date', $this->lang->line('from_date'), 'required');
		$this->form_validation->set_rules('to_date', $this->lang->line('to_date'), 'required');
		$this->form_validation->set_rules('reason', $this->lang->line('leave_reason'), 'min_length[0]');
	}

	public function get_leave_list_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$resp['data'] = $this->fg_leave_model->get_joined_leaves();
			$success = TRUE;
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

	public function has_leave_ajax()
	{
		$resp = array();
		$success = FALSE;
		$has_leave = FALSE;
		$err_message = '';

		if ($this->ion_auth->logged_in()) {
			//Has Leave
			$has_leave = FALSE;
			$employee_id = Common::decodeString($this->input->get('employee_id'));
			$from_date = Common::decodeString($this->input->get('from_date'));
			$to_date = Common::decodeString($this->input->get('to_date'));
			$from_date_temp = Common::decodeString($this->input->get('from_date_temp'));
			$to_date_temp = Common::decodeString($this->input->get('to_date_temp'));
			$action_type = $this->input->get('action_type');

			if ($action_type == 'C') {
				$has_leave = $this->fg_leave_model->has_leave($employee_id, $from_date, $to_date);
			
			} else {

				$from_date_n = Common::strdate_to_int($from_date);
				$to_date_n = Common::strdate_to_int($to_date);
				$from_date_temp_n = Common::strdate_to_int($from_date_temp);
				$to_date_temp_n = Common::strdate_to_int($to_date_temp);

				//-- from date & to date
				if (!empty($from_date_temp) && !empty($to_date_temp) && $from_date != $from_date_temp && $to_date != $to_date_temp) {
					if ($from_date_n < $from_date_temp_n) {
						if ($to_date_n < $from_date_temp_n) {
							$has_leave = $this->fg_leave_model->has_leave($employee_id, $from_date, $to_date);
						
						} else if ($to_date_n > $from_date_temp_n && $to_date_n < $to_date_temp_n) {
							$has_leave = $this->fg_leave_model->has_leave($employee_id, $from_date, Common::get_prev_date($from_date_temp));

						} else {
							$has_leave = $this->fg_leave_model->has_leave($employee_id, $from_date, Common::get_prev_date($from_date_temp));
							if ($has_leave == FALSE) {
								$has_leave = $this->fg_leave_model->has_leave($employee_id, Common::get_next_date($to_date_temp), $to_date);
							}
						}

					} else if ($from_date_n > $from_date_temp_n && $from_date_n < $to_date_temp_n) {
						if ($to_date_n > $to_date_temp_n) {
							$has_leave = $this->fg_leave_model->has_leave($employee_id, Common::get_next_date($to_date_temp), $to_date);
						}

					} else {
						$has_leave = $this->fg_leave_model->has_leave($employee_id, $from_date, $to_date);
					}

				} else {
					//-- from date
					if (!empty($from_date_temp) && $from_date != $from_date_temp) {
						if ($from_date_n < $from_date_temp_n) {
							$has_leave = $this->fg_leave_model->has_leave($employee_id, $from_date, Common::get_prev_date($from_date_temp));
		
						} else if ($from_date_n > $to_date_temp_n) {
							$has_leave = $this->fg_leave_model->has_leave($employee_id, Common::get_next_date($to_date_temp), $from_date);
						}
		
					} 
					
					//-- to date
					if ($has_leave == FALSE) {
						if (!empty($to_date_temp) && $to_date != $to_date_temp) {
							if ($to_date_n > $to_date_temp_n) {
								$has_leave = $this->fg_leave_model->has_leave($employee_id, Common::get_next_date($to_date_temp), $to_date);
		
							} else if ($to_date_n < $from_date_temp_n) {
								$has_leave = $this->fg_leave_model->has_leave($employee_id, $to_date, Common::get_prev_date($from_date_temp));
							}
						}
					}
				}

			}

			if ($has_leave == TRUE) {
				$employee_arr = $this->fg_employee_model->get_employees($employee_id);
	
				if (empty($employee_arr)) {
					$err_message = 'ไม่พบข้อมูลพนักงานที่ต้องการลา';
				} else {
					$err_message = 'พบข้อมูลการทำรายการลาวันดังกล่าวของ ' . $employee_arr[0]['firstname_th'] . ' ' . $employee_arr[0]['lastname_th'] . ' (' . $employee_arr[0]['ems_no']. ') แล้ว ต้องการบันทึกการลาหรือไม่?';
				}
			}

			$success = TRUE;
		}

		$resp['err_message'] = $err_message;
		$resp['has_leave'] = $has_leave;
		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

}
