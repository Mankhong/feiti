<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'libraries/Excel.php';
require_once('Common.php');

class Fg_summary_ot_department extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('fg_summary_ot_department_model'));
	}

	public function index()
	{
		//== Authen
		$user = $this->ion_auth->user()->row();
		$login_data = Common::authen_personal($this, $user);
		//==

		//--- Messages
		$data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

		//=== TEMPLATE
		$data['title'] = 'Summary O-T Report of Department';
		$data['hide_sub_topbar'] = TRUE;
		$data['display_small_topbar'] = TRUE;
		$data['display_action_search'] = FALSE;
		$data['user'] = $user;
		$data['menu_active_303'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/fg/fg.css'));
		$this->template->stylesheet->add(base_url('assets/css/fg/fg_summary_ot_department.css'));
		$this->template->javascript->add(base_url('assets/js/fg/fg_summary_ot_department.js'));
		$this->template->content->view('fg_summary_ot_department', $data);
		$this->template->publish();
	}

	public function get_summary_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$start_date = Common::decodeString($this->input->get('start_date'));
			$end_date = Common::decodeString($this->input->get('end_date'));
			
			$resp['data'] = $this->fg_summary_ot_department_model->get_summary_data($start_date, $end_date);
			$success = TRUE;
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}
	
}
