<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('Common.php');

class Fg_holiday extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		$this->load->model(array('fg_holiday_model'));
    }

	public function index()
	{
		//=== AUTHEN
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}

		//== Authen
		$user = $this->ion_auth->user()->row();
		$login_data = Common::authen_personal($this, $user);

		$is_admin = $login_data['is_admin'];
		$is_member = $login_data['is_member'];
		$is_account = $login_data['is_account'];
		$is_sub_contract = $login_data['is_sub_contract'];

		$data['is_admin'] = $is_admin;
		$data['is_member'] = $is_member;
		$data['is_account'] = $is_account;
		$data['is_sub_contract'] = $is_sub_contract;

		if (!$is_admin && !$is_member && !$is_account && !$is_sub_contract) {
			$logout = $this->ion_auth->logout();
			$this->session->set_flashdata('message', 'Please login with personal user.');
			redirect('auth/login', 'refresh');
		}
		//=== END AUTHEN

		//--- Messages
		$data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

		

		//=== TEMPLATE
		$create_button = Common::html_button('btn_create', '<i class="fas fa-plus-circle fa-sm"></i> Create', 'btn-primary-pz text-light', 'fg_holiday/create', '');
		$data['action_buttons'] = $create_button;
		$data['title'] = 'Holiday';
		$data['display_action_search'] = TRUE;
		$data['user'] = $user;
		$data['menu_active_208'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/fg/fg_holiday.css'));
		$this->template->javascript->add(base_url('assets/js/fg/fg_holiday.js'));
        $this->template->content->view('fg_holiday', $data);
        $this->template->publish();
	}

	public function edit($id)
	{
		//=== AUTHEN
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}

		//== Authen
		$user = $this->ion_auth->user()->row();
		$login_data = Common::authen_personal($this, $user);

		$is_admin = $login_data['is_admin'];
		$is_member = $login_data['is_member'];
		$is_account = $login_data['is_account'];
		$is_sub_contract = $login_data['is_sub_contract'];

		$data['is_admin'] = $is_admin;
		$data['is_member'] = $is_member;
		$data['is_account'] = $is_account;
		$data['is_sub_contract'] = $is_sub_contract;

		if (!$is_admin && !$is_member && !$is_account && !$is_sub_contract) {
			$logout = $this->ion_auth->logout();
			$this->session->set_flashdata('message', 'Please login with personal user.');
			redirect('auth/login', 'refresh');
		}
		//=== END AUTHEN

		//=== GET DATA
		$id = Common::decodeString($id);
		$holiday_arr = $this->fg_holiday_model->get_holidays($id);
		$holiday = NULL;

		if (!empty($holiday_arr)) {
			$holiday = $holiday_arr[0];
		} else {
			$this->session->set_flashdata('errorMessage', 'Not found data.');
			redirect(base_url('fg_holiday'), 'refresh');
		}

		//=== VALIDATION
		$this->set_holiday_validations();

		//=== SUBMIT
		if ($this->input->post()) {
			if($this->form_validation->run() === TRUE) {
				$update_data = $this->get_post_holiday_data();

				if (!empty($update_data['date'])) {
					$update_data['date'] = Common::convert_date_display_to_sql($update_data['date']);
				}

				$afffected_row = $this->fg_holiday_model->update_holiday($holiday['id'], $update_data, $user->id);
				redirect(base_url('fg_holiday'), 'refresh');
			}
		}

		//--- HTML Inputs
		$data['id'] = Common::html_input('id', 'hidden', $holiday['id']);
		$data['date'] = Common::html_input('date', 'text', $holiday['fm_date'], 'datepicker', '', '', TRUE);
		$data['desc'] = Common::html_input('desc', 'text', $holiday['desc']);

		//Dropdown
		$data['type'] = $holiday['type'];
		$data['type_dropdown_default'] = empty($data['type']) ? '1' : $data['type'];
		$data['type_dropdown_data'] = Common::array_to_options($this->common_model->get_holiday_type(), 'id', 'name');

		//--- Messages
		$data['message'] = Common::html_alert('alert-info',$this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger',$this->session->flashdata('errorMessage'));

		//=== SUBMIT URL
		$data['submit_url'] = base_url('fg_holiday/edit/' . Common::encodeString($holiday['id']));

		//=== CANCEL LEAVE
		$data['render_cancel_button'] = TRUE;

		//=== TEMPLATE
		$save_button = Common::html_button('btn_create', '<i class="fas fa-save"></i> Save', 'btn-primary-pz text-light', '', 'onclickSave()');
		$discard_button = Common::html_button('btn_discard', 'Cancel', 'btn-default ml-1', base_url('fg_holiday'), '');
		$data['action_buttons'] = $save_button . $discard_button;	
		$data['title'] = 'Edit Leave';
		$data['user'] = $user;
		$data['menu_active_206'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/fg/fg_holiday_form.css'));
		$this->template->javascript->add(base_url('assets/js/fg/fg_holiday_form.js'));
        $this->template->content->view('fg_holiday_form', $data);
        $this->template->publish();
	}

	public function create()
	{
		//== Authen
		$user = $this->ion_auth->user()->row();
		$login_data = Common::authen_personal($this, $user);

		$is_admin = $login_data['is_admin'];
		$is_member = $login_data['is_member'];
		$is_account = $login_data['is_account'];
		$is_sub_contract = $login_data['is_sub_contract'];

		$data['is_admin'] = $is_admin;
		$data['is_member'] = $is_member;
		$data['is_account'] = $is_account;
		$data['is_sub_contract'] = $is_sub_contract;

		if (!$is_admin && !$is_member && !$is_account && !$is_sub_contract) {
			$logout = $this->ion_auth->logout();
			$this->session->set_flashdata('message', 'Please login with personal user.');
			redirect('auth/login', 'refresh');
		}
		//=== END AUTHEN

		//=== VALIDATION
		$this->set_holiday_validations();

		//=== SUBMIT
		if ($this->input->post()) {
			if ($this->form_validation->run() === TRUE) {
				$insert_data = $this->get_post_holiday_data();

				if (!empty($insert_data['date'])) {
					$insert_data['date'] = Common::convert_date_display_to_sql($insert_data['date']);
				}
				
				$afffected_row = $this->fg_holiday_model->insert_holiday($insert_data, $user->id);
				if ($afffected_row > 0) {
					redirect(base_url('fg_holiday'), 'refresh');
				} else {
					$this->session->set_flashdata('errorMessage', 'Create fail.');
				}
			}
		}

		//=== SUBMIT URL
		$data['submit_url'] = base_url('fg_holiday/create');

		//--- Messages
		$data['message'] = Common::html_alert('alert-info',$this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger',$this->session->flashdata('errorMessage'));

		//--- HTML Inputs
		$data['id'] = Common::html_input('id', 'hidden', $this->form_validation->set_value('id'));
		$data['date'] = Common::html_input('date', 'text', $this->form_validation->set_value('date'), 'datepicker', '', '', TRUE);
		$data['desc'] = Common::html_input('desc', 'text', $this->form_validation->set_value('desc'));

		//Dropdown
		$data['type'] = $this->form_validation->set_value('type');
		$data['type_dropdown_default'] = empty($data['type']) ? '1' : $data['type'];
		$data['type_dropdown_data'] = Common::array_to_options($this->common_model->get_holiday_type(), 'id', 'name');

		//=== TEMPLATE
		$save_button = Common::html_button('btn_create', '<i class="fas fa-save"></i> Save', 'btn-primary-pz text-light', '', 'onclickSave()');
		$discard_button = Common::html_button('btn_discard', 'Cancel', 'btn-default ml-1', base_url('fg_holiday'), '');
		$data['action_buttons'] = $save_button . $discard_button;	
		$data['title'] = 'Create Holiday';
		$data['user'] = $user;
		$data['menu_active_208'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/fg/fg_holiday_form.css'));
		$this->template->javascript->add(base_url('assets/js/fg/fg_holiday_form.js'));
        $this->template->content->view('fg_holiday_form', $data);
        $this->template->publish();
	}

	public function cancel($id)
	{
		//== Authen
		$user = $this->ion_auth->user()->row();
		$login_data = Common::authen_personal($this, $user);

		$is_admin = $login_data['is_admin'];
		$is_member = $login_data['is_member'];
		$is_account = $login_data['is_account'];
		$is_sub_contract = $login_data['is_sub_contract'];

		if (!$is_admin && !$is_member && !$is_account && !$is_sub_contract) {
			$logout = $this->ion_auth->logout();
			$this->session->set_flashdata('message', 'Please login with personal user.');
			redirect('auth/login', 'refresh');
		}
		//=== END AUTHEN

		//=== SUBMIT
		if ($this->input->post()) {
			$this->fg_holiday_model->delete_holiday(Common::decodeString($id));
		}

		redirect('fg_holiday', 'refresh');
	}

	private function get_post_holiday_data()
	{
		return array(
			'id' => $this->input->post('id'),
			'date' => $this->input->post('date'),
			'type' => $this->input->post('type'),
			'desc' => $this->input->post('desc'),
		);
	}

	private function set_holiday_validations()
	{
		$this->form_validation->set_rules('id', $this->lang->line('holiday_id'), 'min_length[0]');
		$this->form_validation->set_rules('date', $this->lang->line('holiday_date'), 'required');
		$this->form_validation->set_rules('type', $this->lang->line('holiday_type'), 'required');
		$this->form_validation->set_rules('desc', $this->lang->line('holiday_desc'), 'required');
	}

	public function get_holiday_list_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$resp['data'] = $this->fg_holiday_model->get_joined_holidays();
			$success = TRUE;
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

}
