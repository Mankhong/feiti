<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('Common.php');

class Billing extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		$this->load->model(array('billing_model','customer_model'));
    }

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}

		//--- Messages
		$data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

		//=== GET USER LOGIN
		$user = $this->ion_auth->user()->row();

		//=== TEMPLATE
		$create_button = Common::html_button('btn_create', '<i class="fas fa-plus-circle fa-sm"></i> Create', 'btn-primary-pz text-light', 'billing/create', '');
		$data['action_buttons'] = $create_button;
		$data['title'] = 'Billing Note';
		$data['display_action_search'] = FALSE;
		$data['user'] = $user;
		$data['menu_active_5'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/sale/billing.css'));
		$this->template->javascript->add(base_url('assets/js/sale/billing.js'));
		$this->template->javascript->add(base_url('assets/js/sale/customer_picker.js'));
		$this->template->stylesheet->add(base_url('assets/css/sale/dataitems.css'));
		$this->template->javascript->add(base_url('assets/js/sale/dataitems.js'));
		$this->template->javascript->add(base_url('assets/js/sale/sale.js'));
        $this->template->content->view('billing', $data);
        $this->template->publish();
	}

	public function edit($id)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		//=== END AUTHEN

		//=== GET USER LOGIN
		$user = $this->ion_auth->user()->row();

		//=== GET DATA
		$id = Common::decodeString($id);
		$billing = $this->billing_model->get_billings($id)[0];
		$customer = $this->customer_model->get_customers($billing['customer_id'])[0];

		$data['temp_billing_id'] = $billing['id'];
		$data['temp_receipt_id'] = $billing['receipt_id'];

		//=== VAT
		$data['vat'] = $this->common_model->get_vat_config();

		//=== VALIDATION
		$this->set_billing_validations();

		//=== SUBMIT
		if ($this->input->post()) {
			$insert_dataitems = $this->input->post('insert_dataitems[]');
			$insert_product_arr = $this->convert_dataitems_to_insert_array($insert_dataitems);

			$delete_dataitems = $this->input->post('delete_dataitems[]');
			$delete_product_arr = $this->convert_dataitems_to_delete_array($delete_dataitems);

			$data['insert_dataitems'] = $insert_dataitems;
			$data['delete_dataitems'] = $delete_dataitems;

			if($this->form_validation->run() === TRUE) {
				$update_data = $this->get_post_billing_data();

				if (!empty($update_data['receipt_date'])) {
					$update_data['receipt_date'] = Common::convert_date_display_to_sql($update_data['receipt_date']);
				}

				if (!empty($update_data['billing_date'])) {
					$update_data['billing_date'] = Common::convert_date_display_to_sql($update_data['billing_date']);
				}

				$afffected_row = $this->billing_model->update_billing($billing['id'], $update_data, $insert_product_arr, $delete_product_arr, $user->id);
				redirect(base_url('billing'), 'refresh');
			}
		}

		//--- HTML Inputs
		$data['billing_id'] = Common::html_input('billing_id', 'text', $billing['id'], '', '', '', TRUE);
		$data['receipt_id'] = Common::html_input('receipt_id', 'text', $billing['receipt_id']);
		$data['receipt_date'] = Common::html_input('receipt_date', 'text', Common::convert_date_sql_to_display($billing['receipt_date']), 'datepicker', '', '', TRUE);
		$data['billing_date'] = Common::html_input('billing_date', 'text', Common::convert_date_sql_to_display($billing['billing_date']), 'datepicker', '', '', TRUE);

		$data['customer_id'] = Common::html_input('customer_id', 'text', $customer['id'], '', '', '', TRUE);
		$data['customer_name'] = Common::html_input('customer_name', 'text', $customer['name'], '', '', '', TRUE);
		$data['customer_type'] = Common::html_input('customer_type', 'text', $customer['customer_type'], '', '', '', TRUE);
		$data['customer_credit'] = Common::html_input('customer_credit', 'text', $customer['credit'], '', '', '', TRUE);
		$data['customer_address'] = Common::html_textarea('customer_address', 3, 40, $customer['address'], '', TRUE);
		//--- END HTML Inputs

		//-- INVOICE LIST //
		$data['invoice_list'] = $this->billing_model->get_billing_invoices($billing['id']);

		//=== Report Billing
		$data['billing_url'] = base_url('report/bl_report/' . urlencode(base64_encode($billing['id'])));
		$data['receipt_url'] = base_url('report/bl_receipt_report/' . urlencode(base64_encode($billing['id'])));

		//*** */
		$data['receipt_os_url'] = base_url('report/bl_receipt_report_os/' . urlencode(base64_encode($billing['id'])));

		//--- Messages
		$data['message'] = Common::html_alert('alert-info',$this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger',$this->session->flashdata('errorMessage'));

		//=== SUBMIT URL
		$data['submit_url'] = base_url('billing/edit/' . Common::encodeString($billing['id']));
		
		//=== DISABLE
		$data['customer_disabled'] = TRUE;

		//=== TEMPLATE
		$save_button = Common::html_button('btn_create', '<i class="fas fa-save"></i> Save', 'btn-primary-pz text-light', '', 'onclickSave()');
		$discard_button = Common::html_button('btn_discard', 'Cancel', 'btn-default ml-1', base_url('billing'), '');
		$data['action_buttons'] = $save_button . $discard_button;	
		$data['title'] = 'Edit Billing Note';
		$data['user'] = $user;
		$data['menu_active_5'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/sale/billing_form.css'));
		$this->template->javascript->add(base_url('assets/js/sale/billing_form.js'));
		$this->template->javascript->add(base_url('assets/js/sale/customer_picker.js'));
		$this->template->javascript->add(base_url('assets/js/sale/invoice_picker.js'));
		$this->template->javascript->add(base_url('assets/js/sale/sale.js'));
        $this->template->content->view('billing_form', $data);
        $this->template->publish();
	}

	public function create()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		//=== END AUTHEN

		//=== GET USER LOGIN
		$user = $this->ion_auth->user()->row();

		//=== VAT
		$data['vat'] = $this->common_model->get_vat_config();

		//=== VALIDATION
		$this->set_billing_validations();

		//=== SUBMIT
		if ($this->input->post()) {
			$insert_data = $this->get_post_billing_data();

			$insert_dataitems = $this->input->post('insert_dataitems[]');
			$insert_product_arr = $this->convert_dataitems_to_insert_array($insert_dataitems);

			$data['insert_dataitems'] = $insert_dataitems;

			if ($this->form_validation->run() === TRUE) {
				if (!empty($insert_data['receipt_date'])) {
					$insert_data['receipt_date'] = Common::convert_date_display_to_sql($insert_data['receipt_date']);
				}

				if (!empty($insert_data['billing_date'])) {
					$insert_data['billing_date'] = Common::convert_date_display_to_sql($insert_data['billing_date']);
				}
				
				//Validate id
				if ($this->billing_model->has_billing($insert_data['id'])) {
					$this->session->set_flashdata('errorMessage', 'Billing ID already in use.');
				
				} else {
					$afffected_row = $this->billing_model->insert_billing($insert_data, $insert_product_arr, $user->id);
					if ($afffected_row > 0) {
						redirect(base_url('billing'), 'refresh');
					} else {
						$this->session->set_flashdata('errorMessage', 'Create fail.');
					}
				}
			}
		}

		//=== SUBMIT URL
		$data['submit_url'] = base_url('billing/create');

		//--- Messages
		$data['message'] = Common::html_alert('alert-info',$this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger',$this->session->flashdata('errorMessage'));

		//--- HTML Inputs
		$data['billing_id'] = Common::html_input('billing_id', 'text', $this->form_validation->set_value('billing_id'));
		$data['receipt_id'] = Common::html_input('receipt_id', 'text', $this->form_validation->set_value('receipt_id'));
		$data['receipt_date'] = Common::html_input('receipt_date', 'text', Common::convert_date_sql_to_display($this->form_validation->set_value('receipt_date')), 'datepicker', '', '', TRUE);
		$data['billing_date'] = Common::html_input('billing_date', 'text', Common::convert_date_sql_to_display($this->form_validation->set_value('billing_date')), 'datepicker', '', '', TRUE);
		
		$data['customer_id'] = Common::html_input('customer_id', 'text', $this->form_validation->set_value('customer_id'), '', '', '', TRUE);
		$data['customer_name'] = Common::html_input('customer_name', 'text', $this->form_validation->set_value('customer_name'), '', '', '', TRUE);
		$data['customer_type'] = Common::html_input('customer_type', 'text', $this->form_validation->set_value('customer_type'), '', '', '', TRUE);
		$data['customer_credit'] = Common::html_input('customer_credit', 'text', $this->form_validation->set_value('customer_credit'), '', '', '', TRUE);
		$data['customer_address'] = Common::html_textarea('customer_address', 3, 40, $this->form_validation->set_value('customer_address'), '', TRUE);
		//--- END HTML Inputs

		//=== GET USER LOGIN
		$user = $this->ion_auth->user()->row();

		//=== TEMPLATE
		$save_button = Common::html_button('btn_create', '<i class="fas fa-save"></i> Save', 'btn-primary-pz text-light', '', 'onclickSave()');
		$discard_button = Common::html_button('btn_discard', 'Cancel', 'btn-default ml-1', base_url('billing'), '');
		$data['action_buttons'] = $save_button . $discard_button;	
		$data['title'] = 'Create Billing Note';
		$data['user'] = $user;
		$data['menu_active_5'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/sale/billing_form.css'));
		$this->template->javascript->add(base_url('assets/js/sale/billing_form.js'));
		$this->template->javascript->add(base_url('assets/js/sale/customer_picker.js'));
		$this->template->javascript->add(base_url('assets/js/sale/invoice_picker.js'));
		$this->template->javascript->add(base_url('assets/js/sale/sale.js'));
        $this->template->content->view('billing_form', $data);
        $this->template->publish();
	}

	private function get_post_billing_data()
	{
		return array(
			'id' => $this->input->post('billing_id'),
			'customer_id' => $this->input->post('customer_id'),
			'receipt_id' => $this->input->post('receipt_id'),
			'receipt_date' => $this->input->post('receipt_date'),
			'billing_date' => $this->input->post('billing_date'),
		);
	}

	private function set_billing_validations()
	{
		//--- Product Detail
		$this->form_validation->set_rules('customer_id', $this->lang->line('customer_id'), 'required');
		$this->form_validation->set_rules('customer_name', $this->lang->line('customer_name'), 'min_length[0]');
		$this->form_validation->set_rules('customer_type', $this->lang->line('customer_type'), 'min_length[0]');
		$this->form_validation->set_rules('customer_credit', $this->lang->line('customer_credit'), 'min_length[0]');
		$this->form_validation->set_rules('customer_address', $this->lang->line('customer_address'), 'min_length[0]');
		$this->form_validation->set_rules('billing_id', $this->lang->line('billing_id'), 'required|regex_match[/^[a-zA-Z0-9]+$/]');
		$this->form_validation->set_rules('receipt_id', $this->lang->line('receipt_id'), 'required');
		$this->form_validation->set_rules('receipt_date', $this->lang->line('receipt_date'), 'required');
		$this->form_validation->set_rules('billing_date', $this->lang->line('billing_date'), 'required');
	}

	//-- Dataitem: id|invoice_id|invoice_date|print_invoice_id|total_amount|dataitem_type
	private function convert_dataitems_to_insert_array($dataitems) {
		$delimiter = '&88&';
		$arr = array();

		if (!empty($dataitems)) {
			for ($i = 0; $i < count($dataitems); $i++) {
				$values = explode($delimiter, $dataitems[$i]);
				$arr[$i]['invoice_id'] = Common::decodeString($values[1]);
			}
		}

		return $arr;
	}

	private function convert_dataitems_to_delete_array($dataitems) {
		$delimiter = '&88&';
		$arr = array();

		if (!empty($dataitems)) {
			for ($i = 0; $i < count($dataitems); $i++) {
				$values = explode($delimiter, $dataitems[$i]);
				$arr[$i]['id'] = $values[0];
			}
		}

		return $arr;
	}

	public function get_billing_list_ajax()
	{
            
            $startDate = $this->input->post('startDate');
            $dateTimeEnd = $this->input->post('dateTimeEnd');
            $billing_id_search = $this->input->post('billing_id_search');
            $customer_name = $this->input->post('customer_name');
            
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$resp['data'] = $this->billing_model->get_joined_billings('',$startDate, $dateTimeEnd, $billing_id_search, $customer_name);
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

	public function get_invoice_list_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$resp['data'] = $this->billing_model->get_picker_invoices($this->input->get('customer_id'));
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

	public function has_billing_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$billing_id = Common::decodeString($this->input->get('billing_id'));
			$resp['has_data'] = $this->billing_model->has_billing($billing_id);
			$success = TRUE;
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

	public function has_receipt_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$receipt_id = Common::decodeString($this->input->get('receipt_id'));
			$resp['has_data'] = $this->billing_model->has_billing($receipt_id);
			$success = TRUE;
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

}
