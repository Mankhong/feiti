<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('Common.php');

class Invoice_type extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		$this->load->model(array('invoice_model','customer_model'));
    }

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}

		//--- HTML Inputs
		$data['invoice_type_id'] = Common::html_input('invoice_type_id', 'text', $this->form_validation->set_value('invoice_type_id'), '', '', '', TRUE);
		$data['invoice_type_name'] = Common::html_input('invoice_type_name', 'text', $this->form_validation->set_value('invoice_type_name'));
		//--- END HTML Inputs

		$data['result_list'] = $this->invoice_model->get_invoice_types();

		//--- Messages
		$data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

		//=== GET USER LOGIN
		$user = $this->ion_auth->user()->row();

		//=== TEMPLATE
		$data['title'] = 'Invoice';
		$data['user'] = $user;
		$data['menu_active_6'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/sale/invoice_type.css'));
		$this->template->javascript->add(base_url('assets/js/sale/invoice_type.js'));
        $this->template->content->view('invoice_type', $data);
        $this->template->publish('manage_template');
	}

	public function save()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}

		if ($this->input->post()) {
			$update_ids = $this->input->post('update_ids[]');
			$update_useds = $this->input->post('update_useds[]');

			if (!empty($update_ids)) {
				for ($i = 0; $i < count($update_ids); $i++) {
					$this->invoice_model->update_invoice_type($update_ids[$i], $update_useds[$i]);
				}
				$this->session->set_flashdata('message', 'Save successfully.');
			}
		}

		//--- HTML Inputs
		$data['invoice_type_id'] = Common::html_input('invoice_type_id', 'text', $this->form_validation->set_value('invoice_type_id'), '', '', '', TRUE);
		$data['invoice_type_name'] = Common::html_input('invoice_type_name', 'text', $this->form_validation->set_value('invoice_type_name'));
		//--- END HTML Inputs

		$data['result_list'] = $this->invoice_model->get_invoice_types();

		//--- Messages
		$data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

		//=== GET USER LOGIN
		$user = $this->ion_auth->user()->row();

		//=== TEMPLATE
		$data['title'] = 'Invoice';
		$data['user'] = $user;
		$data['menu_active_6'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/sale/invoice_type.css'));
		$this->template->javascript->add(base_url('assets/js/sale/invoice_type.js'));
        $this->template->content->view('invoice_type', $data);
        $this->template->publish('manage_template');
	}

	public function create()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}

		if ($this->input->post()) {
			$insert_data = array(
				'name' => $this->input->post('insert_invoice_name'),
				'phase' => $this->input->post('insert_invoice_phase')
			);

			if ($this->invoice_model->insert_invoice_type($insert_data) > 0) {
				$this->session->set_flashdata('message', 'Create successfully.');
			} else {
				$this->session->set_flashdata('errorMessage', 'Create fail.');
			}
		}

		//--- HTML Inputs
		$data['invoice_type_id'] = Common::html_input('invoice_type_id', 'text', $this->form_validation->set_value('invoice_type_id'), '', '', '', TRUE);
		$data['invoice_type_name'] = Common::html_input('invoice_type_name', 'text', $this->form_validation->set_value('invoice_type_name'));
		//--- END HTML Inputs

		$data['result_list'] = $this->invoice_model->get_invoice_types();

		//--- Messages
		$data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

		//=== GET USER LOGIN
		$user = $this->ion_auth->user()->row();

		//=== TEMPLATE
		$data['title'] = 'Invoice';
		$data['user'] = $user;
		$data['menu_active_6'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/sale/invoice_type.css'));
		$this->template->javascript->add(base_url('assets/js/sale/invoice_type.js'));
        $this->template->content->view('invoice_type', $data);
        $this->template->publish('manage_template');
	}

}
