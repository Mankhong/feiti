<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Common extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    //encodeURI and base64
    public static function encodeString($str)
    {
        return urlencode(base64_encode($str));
    }

    public static function decodeString($str)
    {
        return base64_decode(urldecode($str));
    }

    public static function convertCurrencyToFloat($currency)
    {
        return floatval(str_replace(',', '', $currency));
    }

    public static function html_alert($type, $message, $option = 'm-2')
    {
        if (!empty($message)){
            $message = str_replace('<p>', '', $message);
            $message = str_replace('</p>', '', $message);

            return '<div class="alert ' . $type . ' alert-dismissible fade show ' . $option . '" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>' . $message . '</div>';
        } else {
            return '';
        }
}

    public static function html_button($id, $value, $class = 'btn-info', $href = '', $onclick = '', $style = '') {
        return '<a' 
               . ' class="btn btn-sm ' . $class . '"'
               . ' id="' . $id . '" name="' . $id . '"'
               . (!empty($href) ? ' href="' . $href . '"' : '')
               . (!empty($onclick) ? ' onclick="' . $onclick . '"' : '')
               . (!empty($style) ? ' style="' . $style . '"' : '')
               . '>' . $value . '</a>';
    }

    public static function html_input($id, $type, $value = '', $class = '', $style = '', $placeholder = '', $readonly = false) {
        $arr = array(
            'name' => $id,
            'id' => $id,
            'type' => $type,
            'class' => 'form-control' . (!empty($class) ? ' ' . $class : ''),
            'value' => $value
        );

        if (!empty($style)) {
            $arr['style'] = $style;
        }

        if (!empty($placeholder)) {
            $arr['placeholder'] = $placeholder;
        }
    
        if ($readonly === TRUE) {
            $arr['readonly'] = $readonly;
        }
    
        return $arr;
    }

    public static function html_textarea($id, $rows, $cols, $value = '', $class = '', $readonly = false) 
    {
        $arr = array(
            'name' => $id,
            'id' => $id,
            'rows' => $rows,
            'cols' => $cols,
            'class' => 'form-control' . (!empty($class) ? ' ' . $class : ''),
            'value' => $value
        );
    
        if ($readonly === TRUE) {
            $arr['readonly'] = $readonly;
        }
    
        return $arr;
    }

    public static function get_current_date_for_sql()
    {
        return date('Y/m/d');
    }

    //Ex. 2018/12/30 to 30/12/2018
    public static function convert_date_sql_to_display($date, $delimiter = '/')
    {
        $d_arr = explode($delimiter, $date);
        if (strlen($d_arr[0]) == 4) {
            return $d_arr[2] . '/' . $d_arr[1] . '/' . $d_arr[0];
        } else {
            return $date;
        }
    }

    //Ex. 30/12/2018 to 2018/12/30
    public static function convert_date_display_to_sql($date)
    {
        $d_arr = explode('/', $date);
        if (strlen($d_arr[2]) == 4) {
            return $d_arr[2] . '/' . $d_arr[1] . '/' . $d_arr[0];
        } else {
            return $date;
        }
    }

    public static function get_current_date() 
    {
       return Common::convert_date_sql_to_display(Common::get_current_date_for_sql());
    }

    public static function array_to_options($arr = array(), $valueCol = '', $labelCol = '', $empty_item = FALSE)
    {
        $options = array();

        if ($empty_item) {
            $options[''] = $empty_item;
        }

		for ($i = 0; $i < count($arr); $i++)
		{
			$options[$arr[$i][$valueCol]] = $arr[$i][$labelCol];
		}
		return $options;
    }
    
    public static function generate_report_name($title, $ext)
    {
        return $title . '_' . date("Ymd") . '.' .$ext;
    }

    public static function roundup($number, $precision)
    {
        $p = pow(10, $precision);
		return $number = intval($number * $p) / $p;
    }

    public static function html_export_component($form_id, $onclick, $param_names, $submit_url, $text = 'Excel', $class = 'btn-default')
    {
        $html = '<form action="' . $submit_url . '" id="' . $form_id . '" class="export-excel-form" enctype="multipart/form-data" method="post" accept-charset="utf-8">';
        
        if (is_array($param_names)) {
            if (!empty($param_names)) {
                for ($i = 0; $i < count($param_names); $i++) {
                    $html = $html . '<input type="hidden" id="' .$param_names[$i] . '" name="' .$param_names[$i] . '"/>';
                }
            }
        } else {
            $html = $html . '<input type="hidden" id="' .$param_names . '" name="' .$param_names . '"/>';
        }
        
        $html = $html . '<a class="btn btn-sm ' . $class . ' view-mode-1 pz-excel-button" onclick="' . $onclick . '()"><i class="far fa-file-excel"></i> ' . $text . '</a>'
        . '</form>';
        
        return $html;
    }

    public static function convert_value_to_array($value) 
    {
		$delimiter = '&88&';
		return explode($delimiter, $value);
    }

    public static function parse_numeric($value)
    {
        return empty($value) ? 0 : doubleval($value);
    }

    public static function zero_to_empty($value)
    {
        return $value == '0' ? '' : $value;
    }

    public static function authen_personal($controller, $user) 
    {
        if (!$controller->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
        }
        
        $login_data = array();

		//=== GET USER LOGIN
		$login_data['user'] = $controller->ion_auth->user()->row();
		$login_data['is_admin'] = $controller->admin_model->is_personal_admin($user->id);
		$login_data['is_member'] = $controller->admin_model->is_personal_member($user->id);
		$login_data['is_account'] = $controller->admin_model->is_personal_account($user->id);
		$login_data['is_sub_contract'] = $controller->admin_model->is_personal_sub_contract($user->id);

		if (!$login_data['is_admin'] && !$login_data['is_member'] && !$login_data['is_account'] && !$login_data['is_sub_contract']) {
			$logout = $controller->ion_auth->logout();
			$controller->session->set_flashdata('message', 'Please login with personal user.');
			redirect('auth/login', 'refresh');
        }
        
        return $login_data;
    }

    public static function starts_with($text, $start)
    {
        return (substr($text, 0, strlen($start)) === $start);
    }

    //Example 24/12/2019 to 20191224
    public static function strdate_to_int($date)
    {
        if (!empty($date) && strlen($date) == 10) {
            $date_arr = explode('/', $date);
            return intval($date_arr[2] . Common::two_digits($date_arr[1]) . Common::two_digits($date_arr[0]));
        }
        return -1;
    }

    public static function two_digits($number)
    {
        return intval($number) < 10 ? '0' . $number : (string) $number;
    }

    public static function get_next_date($date)
    {
        $day_time = 86400;
        return date('d/m/Y', (DateTime::createFromFormat('d/m/Y', $date)->getTimestamp() + $day_time));
    }

    public static function get_prev_date($date)
    {
        $day_time = 86400;
        return date('d/m/Y', (DateTime::createFromFormat('d/m/Y', $date)->getTimestamp() - $day_time));
    }

    public static function get_ext_from_filename($filename)
    {
        $arr = explode('.', $filename);
        return $arr[count($arr) - 1];
    }
}