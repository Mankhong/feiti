<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'libraries/Excel.php';
require_once('Common.php');

class Product extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		$this->load->library(array('upload'));
		$this->load->model(array('product_model','customer_model'));
		$this->config->load('upload');
    }

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}

		//=== GET USER LOGIN
		$user = $this->ion_auth->user()->row();

		//-- Export Excel
		$data['export_excel'] = Common::html_export_component('export_excel_form', 'exportExcel', 'product_seqs',  base_url('product/export_excel'));

		//--- Messages
		$data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

		//=== TEMPLATE
		$create_button = Common::html_button('btn_create', '<i class="fas fa-plus-circle fa-sm"></i> Create', 'btn-primary-pz text-light', 'product/create', '');
		$data['action_buttons'] = $create_button;
		$data['title'] = 'Products';
		$data['display_action_search'] = TRUE;
		$data['user'] = $user;
		$data['menu_active_2'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/sale/product.css'));
		$this->template->javascript->add(base_url('assets/js/sale/product.js'));
		$this->template->stylesheet->add(base_url('assets/css/sale/dataitems.css'));
		$this->template->javascript->add(base_url('assets/js/sale/dataitems.js'));
        $this->template->content->view('product', $data);
        $this->template->publish();
	}

	public function edit($seq)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		//=== END AUTHEN

		$seq = Common::decodeString($seq);;

		//=== GET USER LOGIN
		$user = $this->ion_auth->user()->row();

		//=== GET DATA
		$pdata = $this->product_model->get_products($seq);
		if (empty($pdata)) {
			$this->session->set_flashdata('errorMessage', 'Product not found.');

		} else {
			$product = $pdata[0];

			$cdata = $this->customer_model->get_customers($product['customer_id']);
			if (empty($cdata)) {
				$this->session->set_flashdata('errorMessage', 'Customer not found.');
			} else {
				$customer = $cdata[0];

				//=== VALIDATION
				$this->set_product_validations();
				
				//=== SUBMIT
				if ($this->input->post() && $this->form_validation->run() === TRUE) {
					$update_data = $this->get_post_product_data();

					//Upload file
					$success = TRUE;
					$file_path = '';
					if (!empty($_FILES['image_upload']['name'])) {
						$success = $this->upload->do_upload('image_upload');
						$upload_data = $this->upload->data();
						$file_path =  $this->config->item('upload_dir') . $upload_data['file_name'];
						$update_data['image_path'] = $file_path;
					}

					if ($success) {
						$this->product_model->insert_product_history($product['seq']);
						$afffected_row = $this->product_model->update_product($product['seq'], $update_data, $user->id);
						redirect(base_url('product'), 'refresh');
					} else {
						$this->session->set_flashdata('errorMessage', 'Update fail, cannot upload file.');
					}
				}
			}
		}

		//--- HTML Inputs
		$data['seq'] = $product['seq'];
		$data['customer_id'] = Common::html_input('customer_id', 'text', $customer['id'], '', '', '', TRUE);
		$data['customer_name'] = Common::html_input('customer_name', 'text', $customer['name'], '', '', '', TRUE);
		$data['product_id'] = Common::html_input('product_id', 'text', $product['id'], '', '', '', TRUE);
		$data['desc1'] = Common::html_input('desc1', 'text', $product['desc1']);
		$data['desc2'] = Common::html_input('desc2', 'text', $product['desc2']);
		$data['boi_desc'] = Common::html_input('boi_desc', 'text', $product['boi_desc']);
		$data['carton'] = Common::html_input('carton', 'text', $product['carton']);
		$data['carton_temp'] = Common::html_input('carton_temp', 'text', $product['carton_temp']);
		$data['internal_note'] = Common::html_textarea('internal_note', 2, 40, $product['internal_note']);

		//$data['unit_price'] = Common::html_input('unit_price', 'number', $product['unit_price'], '', '', '0.0000');
		$data['domestic_price'] = Common::html_input('domestic_price', 'number', $product['domestic_price'], '', '', '0.0000');
		$data['export_price'] = Common::html_input('export_price', 'number', $product['export_price'], '', '', '0.0000');
		$data['lot_size'] = Common::html_input('lot_size', 'number', $product['lot_size'], '', '', '0.00');

		//-- RADIO
		$unit_price_type_list = $this->product_model->get_unit_price_types();
		if (!empty($unit_price_type_list) && count($unit_price_type_list) >= 2) {
			$data['unit_price_type_label_1'] = $unit_price_type_list[0]['name'];
			$data['unit_price_type_label_2'] = $unit_price_type_list[1]['name'];
			$data['unit_price_type_value_1'] = array('name' => 'unit_price_type', 'value' => 1, 'disabled' => TRUE);
			$data['unit_price_type_value_2'] = array('name' => 'unit_price_type', 'value' => 2, 'disabled' => TRUE, 'class' => 'ml-2');
		}

		$data['product_type'] = $product['product_type'];
		$data['product_type_dropdown_default'] = empty($data['product_type']) ? '1' : $data['product_type'];
		$data['product_type_dropdown_data'] = Common::array_to_options($this->product_model->get_product_types(), 'id', 'name');

		$data['invoice_type'] = $product['invoice_type'];
		$data['invoice_type_dropdown_default'] = empty($data['invoice_type']) ? '1' : $data['invoice_type'];
		$data['invoice_type_dropdown_data'] = Common::array_to_options($this->product_model->get_invoice_types(), 'id', 'name');

		$data['process'] = $product['process'];
		$data['process_dropdown_default'] = empty($data['process']) ? '2' : $data['process'];
		$data['process_dropdown_data'] = Common::array_to_options($this->product_model->get_processes(), 'id', 'name');

		//-- IMAGE
		$data['image_path'] = $product['image_path'];

		//--- Messages
		$data['message'] = Common::html_alert('alert-info',$this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger',$this->session->flashdata('errorMessage'));

		//=== SUBMIT URL
		$data['submit_url'] = base_url('product/edit/' . Common::encodeString($product['seq']));

		//=== DISABLE
		$data['customer_disabled'] = TRUE;

		//=== Customer Type
		$data['customer_type'] = $customer['customer_type'];

		//=== Show Product History
		$data['show_product_history'] = TRUE;

		//=== TEMPLATE
		$save_button = Common::html_button('btn_create', '<i class="fas fa-save"></i> Save', 'btn-primary-pz text-light', '', 'onclickSave()');
		$discard_button = Common::html_button('btn_discard', 'Cancel', 'btn-default ml-1', base_url('product'), '');
		$data['action_buttons'] = $save_button . $discard_button;	
		$data['title'] = 'Edit Product';
		$data['user'] = $user;
		$data['menu_active_2'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/sale/product_form.css'));
		$this->template->javascript->add(base_url('assets/js/sale/product_form.js'));
		$this->template->stylesheet->add(base_url('assets/css/sale/customer_picker.css'));
		$this->template->javascript->add(base_url('assets/js/sale/customer_picker.js'));
		$this->template->stylesheet->add(base_url('assets/css/image_upload.css'));
		$this->template->javascript->add(base_url('assets/js/image_upload.js'));
        $this->template->content->view('product_form', $data);
        $this->template->publish();
	}

	public function create()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		//=== END AUTHEN

		//=== GET USER LOGIN
		$user = $this->ion_auth->user()->row();

		//=== VALIDATION
		$this->set_product_validations();

		//=== SUBMIT
		if ($this->input->post() && $this->form_validation->run() === TRUE) {
			$insert_data = $this->get_post_product_data();

			//Upload file
			$success = TRUE;
			$file_path = '';
			if (!empty($_FILES['image_upload']['name'])) {
				$success = $this->upload->do_upload('image_upload');
				$upload_data = $this->upload->data();
				$file_path =  $this->config->item('upload_dir') . $upload_data['file_name'];
				$insert_data['image_path'] = $file_path;
			}

			if ($success) {
				$afffected_row = $this->product_model->insert_product($insert_data, $user->id);
				if ($afffected_row > 0) {
					redirect(base_url('product'), 'refresh');
				} else {
					$this->session->set_flashdata('errorMessage', 'Create fail.');
				}
			} else {
				$this->session->set_flashdata('errorMessage', 'Create fail, cannot upload file.');
			}
			
			//Validate product_id
			// if ($this->product_model->has_product($data['id'])) {
			// 	$this->session->set_flashdata('errorMessage', 'Product ID already in use.');
			
			// } else {
				
			//}
		}

		//--- Messages
		$data['message'] = Common::html_alert('alert-info',$this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger',$this->session->flashdata('errorMessage'));

		//--- HTML Inputs
		$data['seq'] = '';
		$data['customer_id'] = Common::html_input('customer_id', 'text', $this->form_validation->set_value('customer_id'), '', '', '', TRUE);
		$data['customer_name'] = Common::html_input('customer_name', 'text', $this->form_validation->set_value('customer_name'), '', '', '', TRUE);
		$data['product_id'] = Common::html_input('product_id', 'text', $this->form_validation->set_value('product_id'));
		$data['desc1'] = Common::html_input('desc1', 'text', $this->form_validation->set_value('desc1'));
		$data['desc2'] = Common::html_input('desc2', 'text', $this->form_validation->set_value('desc2'));
		$data['boi_desc'] = Common::html_input('boi_desc', 'text', $this->form_validation->set_value('boi_desc'));
		$data['carton'] = Common::html_input('carton', 'text', $this->form_validation->set_value('carton'));
		$data['carton_temp'] = Common::html_input('carton_temp', 'text', $this->form_validation->set_value('carton_temp'));
		$data['internal_note'] = Common::html_textarea('internal_note', 2, 40, $this->form_validation->set_value('internal_note'));

		//$data['unit_price'] = Common::html_input('unit_price', 'number', $this->form_validation->set_value('unit_price'), '', '', '0.0000');
		$data['domestic_price'] = Common::html_input('domestic_price', 'number', $this->form_validation->set_value('domestic_price'), '', '', '0.0000');
		$data['export_price'] = Common::html_input('export_price', 'number', $this->form_validation->set_value('export_price'), '', '', '0.0000');
		$data['lot_size'] = Common::html_input('lot_size', 'number', $this->form_validation->set_value('lot_size'), '', '', '0.00');

		//-- RADIO
		$unit_price_type_list = $this->product_model->get_unit_price_types();
		if (!empty($unit_price_type_list) && count($unit_price_type_list) >= 2) {
			$data['unit_price_type_label_1'] = $unit_price_type_list[0]['name'];
			$data['unit_price_type_label_2'] = $unit_price_type_list[1]['name'];
			$data['unit_price_type_value_1'] = array('name' => 'unit_price_type', 'value' => 1, 'disabled' => TRUE);
			$data['unit_price_type_value_2'] = array('name' => 'unit_price_type', 'value' => 2, 'disabled' => TRUE, 'class' => 'ml-2');
		}

		$data['product_type'] = $this->form_validation->set_value('product_type');
		$data['product_type_dropdown_default'] = empty($data['product_type']) ? '1' : $data['product_type'];
		$data['product_type_dropdown_data'] = Common::array_to_options($this->product_model->get_product_types(), 'id', 'name');

		$data['invoice_type'] = $this->form_validation->set_value('invoice_type');
		$data['invoice_type_dropdown_default'] = empty($data['invoice_type']) ? '1' : $data['invoice_type'];
		$data['invoice_type_dropdown_data'] = Common::array_to_options($this->product_model->get_invoice_types(), 'id', 'name');

		$data['process'] = $this->form_validation->set_value('process');
		$data['process_dropdown_default'] = empty($data['process']) ? '2' : $data['process'];
		$data['process_dropdown_data'] = Common::array_to_options($this->product_model->get_processes(), 'id', 'name');

		//=== Customer Type
		$data['customer_type'] = 1;

		//-- IMAGE
		$data['image_path'] = '';

		//=== SUBMIT URL
		$data['submit_url'] = base_url('product/create');

		//=== TEMPLATE
		$save_button = Common::html_button('btn_create', '<i class="fas fa-save"></i> Save', 'btn-primary-pz text-light', '', 'onclickSave()');
		$discard_button = Common::html_button('btn_discard', 'Cancel', 'btn-default ml-1', base_url('product'), '');
		$data['action_buttons'] = $save_button . $discard_button;	
		$data['title'] = 'Create Product';
		$data['user'] = $user;
		$data['menu_active_2'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/sale/product_form.css'));
		$this->template->javascript->add(base_url('assets/js/sale/product_form.js'));
		$this->template->stylesheet->add(base_url('assets/css/sale/customer_picker.css'));
		$this->template->javascript->add(base_url('assets/js/sale/customer_picker.js'));
		$this->template->stylesheet->add(base_url('assets/css/image_upload.css'));
		$this->template->javascript->add(base_url('assets/js/image_upload.js'));
        $this->template->content->view('product_form', $data);
        $this->template->publish();
	}

	private function get_post_product_data()
	{
		return array(
			'id' => $this->input->post('product_id'),
			'desc1' => $this->input->post('desc1'),
			'desc2' => $this->input->post('desc2'),
			'boi_desc' => $this->input->post('boi_desc'),
			'product_type' => $this->input->post('product_type'),
			'invoice_type' => $this->input->post('invoice_type'),
			'process' => $this->input->post('process'),

			//'unit_price' => $this->input->post('unit_price'),
			'domestic_price' => $this->input->post('domestic_price'),
			'export_price' => $this->input->post('export_price'),
			'carton' => $this->input->post('carton'),
			'lot_size' => $this->input->post('lot_size'),
			'carton_temp' => $this->input->post('carton_temp'),
			'unit_price_type' => $this->input->post('unit_price_type'),
			'customer_id' => $this->input->post('customer_id'),
			'internal_note' => $this->input->post('internal_note'),
		);
	}

	private function set_product_validations()
	{
		//--- Product Detail
		$this->form_validation->set_rules('customer_id', $this->lang->line('customer_id'), 'required');
		$this->form_validation->set_rules('product_id', $this->lang->line('product_id'), 'required');
		$this->form_validation->set_rules('product_type', $this->lang->line('product_type'), 'required');
		$this->form_validation->set_rules('invoice_type', $this->lang->line('invoice_type'), 'required');
		$this->form_validation->set_rules('process', $this->lang->line('process'), 'required');
		$this->form_validation->set_rules('desc1', $this->lang->line('desc1'), 'required|min_length[0]');
		$this->form_validation->set_rules('desc2', $this->lang->line('desc2'), 'min_length[0]');
		$this->form_validation->set_rules('boi_desc', $this->lang->line('boi_desc'), 'min_length[0]');

		//--- Product Price
		//$this->form_validation->set_rules('unit_price', $this->lang->line('unit_price'), 'required|numeric');
		$this->form_validation->set_rules('domestic_price', $this->lang->line('domestic_price'), 'numeric');
		$this->form_validation->set_rules('export_price', $this->lang->line('export_price'), 'numeric');
		$this->form_validation->set_rules('lot_size', $this->lang->line('lot_size'), 'required|numeric');

		$this->form_validation->set_rules('carton', $this->lang->line('carton'), 'required|min_length[0]');
		$this->form_validation->set_rules('unit_price_type', $this->lang->line('unit_price_type'), 'min_length[0]');
		$this->form_validation->set_rules('carton_temp', $this->lang->line('carton_temp'), 'min_length[0]');
		$this->form_validation->set_rules('internal_note', $this->lang->line('internal_note'), 'min_length[0]');
	}

	public function get_product_list_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$resp['data'] = $this->product_model->get_joined_products();
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

	public function get_products_history_list_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$seq = base64_decode(urldecode($this->input->get('seq')));
			$resp['data'] = $this->product_model->get_products_histories($seq);
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

	public function export_excel() {
		$product_seq_arr = Common::convert_value_to_array($this->input->post('product_seqs'));
        $data_arr = $this->product_model->get_joined_products($product_seq_arr);

        $objPHPExcel = new PHPExcel();

        //Set cell width
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension("B")->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension("C")->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension("D")->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension("E")->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension("F")->setWidth(40);
		$objPHPExcel->getActiveSheet()->getColumnDimension("G")->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension("H")->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension("I")->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension("J")->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension("K")->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension("L")->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension("M")->setWidth(20);
        //-- Header Style
        $objPHPExcel->getActiveSheet()->getStyle("A1:M1")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A2:M2")->getFont()->setBold(true);

        //-- Header
        $header = "PRODUCTS";

        $objPHPExcel->getActiveSheet()->mergeCells("A1:M1");
        $objPHPExcel->getActiveSheet()
                ->setCellValue("A1", $header);

        //-- Table Header Style
		$header_columns = "A2:M2";
        $objPHPExcel->getActiveSheet()->getStyle($header_columns)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle($header_columns)->getFont()->setBold(true);

		//-- Table Header
		$row = 2;
        $objPHPExcel->getActiveSheet()
                ->setCellValue("A" . $row, "Customer ID")
                ->setCellValue("B" . $row, "Customer")
                ->setCellValue("C" . $row, "Product ID/Part No.")
                ->setCellValue("D" . $row, "Discription 1/Part Name")
				->setCellValue("E" . $row, "Discription 2")
				->setCellValue("F" . $row, "BOI Discription")
				->setCellValue("G" . $row, "Local Price")
				->setCellValue("H" . $row, "BOI Price")
				->setCellValue("I" . $row, "Invoice Type")
				->setCellValue("J" . $row, "Process")
				->setCellValue("K" . $row, "Packing/Lot Size")
				->setCellValue("L" . $row, "Carton")
				->setCellValue("M" . $row, "Carton Temporary");

		//-- Table Body
		if (!empty($data_arr)) {
			$row = 3;
			foreach ($data_arr as $value) {
				//Column Style
				$objPHPExcel->getActiveSheet()->getStyle("A" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("B" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("C" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT); 
				$objPHPExcel->getActiveSheet()->getStyle("D" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("E" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("F" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("G" . $row)->getNumberFormat()->setFormatCode("#,##0.0000");
				$objPHPExcel->getActiveSheet()->getStyle("H" . $row)->getNumberFormat()->setFormatCode("#,##0.0000");
				$objPHPExcel->getActiveSheet()->getStyle("I" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("J" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("K" . $row)->getNumberFormat()->setFormatCode("#,##0");
				$objPHPExcel->getActiveSheet()->getStyle("L" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("M" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

				$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue("A" . $row, $value["customer_id"])
						->setCellValue("B" . $row, $value["customer_name"])
						->setCellValue("C" . $row, $value["id"])
						->setCellValue("D" . $row, $value["desc1"])
						->setCellValue("E" . $row, $value["desc2"])
						->setCellValue("F" . $row, $value["boi_desc"])
						->setCellValue("G" . $row, $value["domestic_price"])
						->setCellValue("H" . $row, $value["export_price"])
						->setCellValue("I" . $row, $value["invoice_type_name"])
						->setCellValue("J" . $row, $value["process_name"])
						->setCellValue("K" . $row, $value["lot_size"])
						->setCellValue("L" . $row, $value["carton"])
						->setCellValue("M" . $row, $value["carton_temp"]);
				
				//Cell Style
				$objPHPExcel->getActiveSheet()->getStyle("A" . $row . ":" . "M" . $row)->getAlignment()->setWrapText(true);
				$objPHPExcel->getActiveSheet()->getStyle("A" . $row . ":" . "M" . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

				$row++;
			}

			//-- Export Date
			$row++;
			$objPHPExcel->getActiveSheet()->getStyle("M" . $row)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("M" . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->setCellValue("M" . $row, Common::get_current_date());
		}

        ob_end_clean();

        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=" . Common::generate_report_name('PRODUCTS', 'xlsx'));
        header("Cache-Control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objWriter->save("php://output");
	}

}
