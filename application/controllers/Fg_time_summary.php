<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'libraries/Excel.php';
require_once('Common.php');

class Fg_time_summary extends CI_Controller {

	public function __construct()
	{
	parent::__construct();
	$this->load->model(array('fg_time_model', 'fg_employee_model', 'fg_time_summary_model'));
	}

	public function index()
	{
		/*
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		*/

		//== Authen
		$user = $this->ion_auth->user()->row();
		$login_data = Common::authen_personal($this, $user);
		//==

		$data['checkinout_list'] = $this->fg_time_model->get_fg_checkinout();

		//--- Messages
		$data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

		//=== INPUTS
		$data['department'] = '';
		$data['department_dropdown_default'] = '';
		$data['department_dropdown_data'] = Common::array_to_options($this->common_model->get_department(), 'id', 'name', ' ');

		$temp_employee_type_list = $this->common_model->get_employee_type();
		$employee_type_list = array();
		$index = 0;
		for ($i = 0; $i < count($temp_employee_type_list); $i++) {
			if ($temp_employee_type_list[$i]['code'] != 'C') {
				$employee_type_list[$index++] = $temp_employee_type_list[$i];
			}
		}

		$data['employee_type'] = '';
		$data['employee_type_dropdown_data'] = Common::array_to_options($employee_type_list, 'code', 'name');
		$data['employee_type_dropdown_default'] = empty($employee_type_list) ? '' : $employee_type_list[0]['code'];
		
		/*if (!empty($employee_type_list) && count($employee_type_list) >= 2) {
			$data['employee_type_label_1'] = $employee_type_list[0]['name'];
			$data['employee_type_label_2'] = $employee_type_list[1]['name'];
			$data['employee_type_label_3'] = $employee_type_list[2]['name'];
			$data['employee_type_label_4'] = $employee_type_list[3]['name'];
			$data['employee_type_value_1'] = array('name' => 'ctrEmployeeType', 'value' => $employee_type_list[0]['code'], 'checked' => TRUE);
			$data['employee_type_value_2'] = array('name' => 'ctrEmployeeType', 'value' => $employee_type_list[1]['code']);
			$data['employee_type_value_3'] = array('name' => 'ctrEmployeeType', 'value' => $employee_type_list[2]['code']);
			$data['employee_type_value_4'] = array('name' => 'ctrEmployeeType', 'value' => $employee_type_list[3]['code']);
		}*/

		//-- Export Excel
		$params = array('year', 'month', 'department', 'employeeType', 'employeeNo');
		if ($this->admin_model->is_personal_admin($user->id)) {
			$data['export_ot_excel'] = Common::html_export_component('export_excel_ot_form', 'exportOTExcel', $params,  base_url('fg_time_summary/export_ot_excel'), 'Export OT', 'btn-primary text-white');
			$data['export_excel'] = Common::html_export_component('export_excel_form', 'exportExcel', $params,  base_url('fg_time_summary/export_excel'), 'Export Excel', 'btn-excel-pz');
		}

		//=== TEMPLATE
		//$create_button = Common::html_button('btn_create', '<i class="fas fa-plus-circle fa-sm"></i> Create', 'btn-primary-pz text-light', 'billing/create', '');
		//$data['action_buttons'] = $create_button;
		$data['title'] = 'Timesheet Summary Report';
		$data['hide_sub_topbar'] = TRUE;
		$data['display_small_topbar'] = TRUE;
		$data['display_action_search'] = FALSE;
		$data['user'] = $user;
		$data['menu_active_203'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/fg/fg.css'));
		$this->template->stylesheet->add(base_url('assets/css/fg/fg_time_summary.css'));
		$this->template->javascript->add(base_url('assets/js/fg/fg_time_summary.js'));
		$this->template->javascript->add(base_url('assets/js/sale/sale.js'));
		$this->template->content->view('fg_time_summary', $data);
		$this->template->publish();
	}

	public function get_summary_timesheet_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$year = $this->input->get('year');
			$month = $this->input->get('month');
			$department = $this->input->get('department');
			$employee_type = $this->input->get('employeeType');
			$employee_no = Common::decodeString($this->input->get('employeeNo'));

			$resp['data'] = $this->fg_time_summary_model->get_summary_timesheet($year, $month, $department, $employee_type, $employee_no);
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}
	
	public function export_excel() {
		$year = $this->input->post('year');
		$month = $this->input->post('month');
		$department = $this->input->post('department');
		$employee_type = $this->input->post('employeeType');
		$employee_no = Common::decodeString($this->input->post('employeeNo'));
		$data_arr = $this->fg_time_summary_model->get_summary_timesheet($year, $month, $department, $employee_type, $employee_no);
		
		$employee_type_name = ($employee_type == 'A' ? 'Monthly' : 'Daily');

        $objPHPExcel = new PHPExcel();

		//Set cell width
		$columns = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG");
		$column_names_1 = array("No.", "Code Department", "ID No.", "Name-Surname", "Entry Date", "Section", "Position", "Working Day", "", "", "OverTime", "", "", "", "Diligence", "Diligence", "Summary Leave of Month", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
		$column_names_2 = array("", "", "", "", "", "", "", "Working Day 100%", "Night Shift", "Food, Travel Day", "1.00 ", "1.50 ", "2.00 ", "3.00 ", "", "", "SL(H)", "SL", "Total SL", "AL", "AB", "CO", "ASL", "DFL", "MRL", "MA", "ML", "RL", "CL", "CL(H)", "Warning Letter", "Late Times", "Late Minute");
		$column_count = count($columns);

		$objPHPExcel->setActiveSheetIndex(0);
		
		//Column Width
		for ($i = 0; $i < $column_count; $i++) {
			$cw = 15;
			if ($i == 0) {
				$cw = 5;
			} else if ($i == 3) {
				$cw = 30;
			} else if ($i == 5) {
				$cw = 20;
			}

			$objPHPExcel->getActiveSheet()->getColumnDimension($columns[$i])->setWidth($cw);
		}

        //-- Header Style
        $objPHPExcel->getActiveSheet()->getStyle($columns[0] . "1:" . $columns[count($columns)-1] . "3")->getFont()->setBold(true);

        //-- Header
        $header = "Timesheet Summary : " . $employee_type_name;

        $objPHPExcel->getActiveSheet()->mergeCells($columns[0] . "1:" . $columns[count($columns)-1] . "1");
        $objPHPExcel->getActiveSheet()
                ->setCellValue("A1", $header);

        //-- Table Header Style
		$header_columns = $columns[0] . "2:" . $columns[count($columns)-1] . "3";
        $objPHPExcel->getActiveSheet()->getStyle($header_columns)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle($header_columns)->getFont()->setBold(true);

		//-- Table Header
		$row = 2;
		$bc = '000000';
		$border_style= array('borders' => array(
			'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => $bc)),
			'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => $bc)),
			'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => $bc)),
			'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => $bc))));

		for ($i = 0; $i < $column_count; $i++) {
			$objPHPExcel->getActiveSheet()->setCellValue($columns[$i] . $row, $column_names_1[$i]);
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			//$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DDDDDD');
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->applyFromArray($border_style);
		}

		$row++;
		for ($i = 0; $i < $column_count; $i++) {
			$objPHPExcel->getActiveSheet()->setCellValue($columns[$i] . $row, $column_names_2[$i]);
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			//$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DDDDDD');
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->applyFromArray($border_style);
		}

		//-- Merge Cells
		$merg_cells = array("H2:J2", "K2:N2", "Q2:AG2", "A2:A3", "B2:B3", "C2:C3", "D2:D3", "E2:E3", "F2:F3", "G2:G3");
		foreach ($merg_cells as $mc) {
			$objPHPExcel->getActiveSheet()->mergeCells($mc);
		}

		//Value Keys
		$value_keys = array(NULL, 'department_name', "ems_no", "fullname_th", "entry_date", 'department_id', "position_name", "working_day100", "sum_night_shift", "sum_food_travel_day", "sum_ot1", "sum_ot15", "sum_ot2", "sum_ot3", "prev_diligence", "diligence",
							"slh", "sl", NULL, "al", "ab", "co", "asl", "dfl", "mrl", "ma", "ml", "rl", "cl", "clh", NULL, NULL, NULL);

		//-- Table Body
		if (!empty($data_arr)) {
			$row++;
			$no = 1;
			foreach ($data_arr as $value) {

				//Column Value
				for ($i = 0; $i < $column_count; $i++) {
					$k = $value_keys[$i];
					$v = NULL;

					//-- VALUES
					if ($i == 0) { //No.
						$v = ($row - 3);
					} else if ($i == 18) { //Total SL
						$slh = Common::parse_numeric($value[$value_keys[16]]);
						$sl = Common::parse_numeric($value[$value_keys[17]]);
						$v = Common::zero_to_empty($slh + $sl);
					} else {
						$v = ($k == NULL ? "" : $value[$k]);
					}

					//Working day 100%
					if($i == 7) {
						if (!empty($value["working_day100"])) {
							$v = Common::parse_numeric($value["working_day100"] - $value["co"]);

							if ($value["employee_type"] == "B") {
								$v += Common::parse_numeric($value["pay_holiday"]);
							}
						}
					}
					//-- END VALUES

					//Value Format
					if (($i > 6 && $i < 14) || ($i > 15 && $i < 30)) {
						$v = Common::zero_to_empty(Common::parse_numeric($v));
					}

					$objPHPExcel->setActiveSheetIndex(0)->setCellValue($columns[$i] . $row, $v);

					//Cell Style
					$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->applyFromArray($border_style);
					if ($i == 14 || $i == 15) {
						$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getNumberFormat()->setFormatCode("#,##0");
					} else if ($i == 1 || $i == 2 || $i ==4 ) {
						$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					}
				}
				
				//Cell Style
				$objPHPExcel->getActiveSheet()->getStyle("A" . $row . ":" . "F" . $row)->getAlignment()->setWrapText(true);
				$objPHPExcel->getActiveSheet()->getStyle("A" . $row . ":" . "F" . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

				$row++;
			}

			/*
			//-- Export Date
			$row++;
			$objPHPExcel->getActiveSheet()->getStyle("A" . $row)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("A" . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->setCellValue("A" . $row, Common::get_current_date());
			$objPHPExcel->getActiveSheet()->mergeCells("A" . $row . ":B" . $row);
			*/
		}

		ob_end_clean();
		
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=" . Common::generate_report_name('Timesheet_summary_' . $employee_type_name, 'xlsx'));
        header("Cache-Control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objWriter->save("php://output");
	}

	public function export_ot_excel() {
		$year = $this->input->post('year');
		$month = $this->input->post('month');
		$department = $this->input->post('department');
		$employee_type = $this->input->post('employeeType');
		$employee_no = Common::decodeString($this->input->post('employeeNo'));
		$data_arr = $this->fg_time_summary_model->get_summary_timesheet($year, $month, $department, $employee_type, $employee_no);
		
		$employee_type_name = ($employee_type == 'A' ? 'Monthly' : 'Daily');

        $objPHPExcel = new PHPExcel();

		//Set cell width
		$columns = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O");
		$column_names_1 = array("No.", "Code Department", "ID No.", "Name-Surname", "Entry Date", "Section", "Position", "Net Day", "1", "1.5", "2", "3", "OT.", "Net. Hrs.", "Working day +");
		$column_names_2 = array("", "", "", "", "", "", "", "", "OT. 1 ", "OT. 1.5", "OT. 2", "OT. 3", "1+1.5+2+3", "", "");
		$column_count = count($columns);

		$objPHPExcel->setActiveSheetIndex(0);
		
		//Column Width
		for ($i = 0; $i < $column_count; $i++) {
			$cw = 15;
			if ($i == 0) {
				$cw = 5;
			} else if ($i == 3) {
				$cw = 30;
			} else if ($i == 5) {
				$cw = 20;
			}

			$objPHPExcel->getActiveSheet()->getColumnDimension($columns[$i])->setWidth($cw);
		}

        //-- Header Style
        $objPHPExcel->getActiveSheet()->getStyle($columns[0] . "1:" . $columns[count($columns)-1] . "3")->getFont()->setBold(true);

        //-- Header
        $header = "Timesheet Summary Overtime: " . $employee_type_name;

        $objPHPExcel->getActiveSheet()->mergeCells($columns[0] . "1:" . $columns[count($columns)-1] . "1");
        $objPHPExcel->getActiveSheet()
                ->setCellValue("A1", $header);

        //-- Table Header Style
		$header_columns = $columns[0] . "2:" . $columns[count($columns)-1] . "3";
        $objPHPExcel->getActiveSheet()->getStyle($header_columns)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle($header_columns)->getFont()->setBold(true);

		//-- Table Header
		$row = 2;
		$bc = '000000';
		$border_style= array('borders' => array(
			'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => $bc)),
			'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => $bc)),
			'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => $bc)),
			'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => $bc))));

		for ($i = 0; $i < $column_count; $i++) {
			$objPHPExcel->getActiveSheet()->setCellValue($columns[$i] . $row, $column_names_1[$i]);
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			//$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DDDDDD');
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->applyFromArray($border_style);
		}

		$row++;
		for ($i = 0; $i < $column_count; $i++) {
			$objPHPExcel->getActiveSheet()->setCellValue($columns[$i] . $row, $column_names_2[$i]);
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			//$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DDDDDD');
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->applyFromArray($border_style);
		}

		//-- Merge Cells
		$merg_cells = array("A2:A3", "B2:B3", "C2:C3", "D2:D3", "E2:E3", "F2:F3", "G2:G3", "H2:H3", "N2:N3", "O2:O3");
		foreach ($merg_cells as $mc) {
			$objPHPExcel->getActiveSheet()->mergeCells($mc);
		}

		//Value Keys
		$value_keys = array(NULL, 'department_name', "ems_no", "fullname_th", "entry_date", 'department_id', "position_name", "working_day100", "sum_ot1", "sum_ot15", "sum_ot2", "sum_ot3", NULL, NULL, NULL);

		//-- Table Body
		if (!empty($data_arr)) {
			$row++;
			$no = 1;
			foreach ($data_arr as $value) {

				//Column Value
				$sum_ot = 0;
				$net_day = 0;
				$net_hour = 0;
				for ($i = 0; $i < $column_count; $i++) {
					$k = $value_keys[$i];
					$v = NULL;

					//-- VALUES
					if ($i == 0) { //No.
						$v = ($row - 3);
					} else {
						$v = ($k == NULL ? "" : $value[$k]);
					}
					//-- END VALUES

					//Value Format
					if (($i >=7 && $i <= 14)) {
						$v = Common::zero_to_empty(Common::parse_numeric($v));
					} 
					
					if ($i == 7) {
						$net_day = Common::parse_numeric($v);
					} else if ($i == 8) {
						$sum_ot = Common::parse_numeric($v);
					} else if ($i > 8 && $i <= 11) {
						$sum_ot += Common::parse_numeric($v);
					} else if ($i == 12) {
						$v = $sum_ot;
					} else if ($i == 13) {
						$net_hour = $net_day * 8;
						$v = $net_hour;
					} else if ($i == 14) {
						$v = $net_hour + $sum_ot;
					}

					$objPHPExcel->setActiveSheetIndex(0)->setCellValue($columns[$i] . $row, $v);

					//Cell Style
					$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->applyFromArray($border_style);
					if ($i == 14) {
						$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getNumberFormat()->setFormatCode("#,##0");
					} else if ($i == 1 || $i == 2 || $i ==4 ) {
						$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					}
				}
				
				//Cell Style
				$objPHPExcel->getActiveSheet()->getStyle("A" . $row . ":" . "O" . $row)->getAlignment()->setWrapText(true);
				$objPHPExcel->getActiveSheet()->getStyle("A" . $row . ":" . "O" . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

				$row++;
			}

			/*
			//-- Export Date
			$row++;
			$objPHPExcel->getActiveSheet()->getStyle("A" . $row)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("A" . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->setCellValue("A" . $row, Common::get_current_date());
			$objPHPExcel->getActiveSheet()->mergeCells("A" . $row . ":B" . $row);
			*/
		}

		ob_end_clean();
		
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=" . Common::generate_report_name('Timesheet_summary_overtime_' . $employee_type_name, 'xlsx'));
        header("Cache-Control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objWriter->save("php://output");
	}
}
