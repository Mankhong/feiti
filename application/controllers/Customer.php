<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'libraries/Excel.php';
require_once('Common.php');

class Customer extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		$this->load->library(array('upload'));
		$this->load->model(array('customer_model'));
		$this->config->load('upload');
    }

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}

		//=== GET USER LOGIN
		$user = $this->ion_auth->user()->row();

		//-- Export Excel
		$data['export_excel'] = Common::html_export_component('export_excel_form', 'exportExcel', 'customer_ids',  base_url('customer/export_excel'));

		

		//--- Messages
		$data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

		//=== TEMPLATE
		$create_button = Common::html_button('btn_create', '<i class="fas fa-plus-circle fa-sm"></i> Create', 'btn-primary-pz text-light', 'customer/create', '');
		$data['action_buttons'] = $create_button;
		$data['title'] = 'Customers';
		$data['display_action_search'] = TRUE;
		$data['user'] = $user;
		$data['menu_active_1'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/sale/customer.css'));
		$this->template->javascript->add(base_url('assets/js/sale/customer.js'));
		$this->template->stylesheet->add(base_url('assets/css/sale/dataitems.css'));
		$this->template->javascript->add(base_url('assets/js/sale/dataitems.js'));
        $this->template->content->view('customer', $data);
        $this->template->publish();
	}

	public function edit($id)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		//=== END AUTHEN

		//=== GET USER LOGIN
		$user = $this->ion_auth->user()->row();

		//=== GET DATA
		$id = base64_decode(urldecode($id));
		$customer = $this->customer_model->get_customers($id)[0];

		//=== VALIDATION
		$this->set_customer_validations();

		//=== SUBMIT
		if ($this->input->post() && $this->form_validation->run() === TRUE) {
			$update_data = $this->get_post_customer_data();

			//Upload file
			$success = TRUE;
			$file_path = '';
			if (!empty($_FILES['image_upload']['name'])) {
				$success = $this->upload->do_upload('image_upload');
				$upload_data = $this->upload->data();
				$file_path =  $this->config->item('upload_dir') . $upload_data['file_name'];
				$update_data['image_path'] = $file_path;
			}

			if ($success) {
				$afffected_row = $this->customer_model->update_customer($customer['id'], $update_data, $user->id);
				redirect(base_url('customer'), 'refresh');
			} else {
				$this->session->set_flashdata('errorMessage', 'Update fail, cannot upload file.');
			}
			
			
		}

		//--- HTML Inputs
		$data['customer_id'] = Common::html_input('customer_id', 'text', $customer['id'], '', 'font-size: 20px;', '', TRUE);
		$data['customer_name'] = Common::html_input('customer_name', 'text', $customer['name']);
		$data['short_name'] = Common::html_input('short_name', 'text', $customer['short_name']);
		$data['credit'] = Common::html_input('credit', 'number', $customer['credit'], '', '', '0.0');
		$data['discount'] = Common::html_input('discount', 'number', $customer['discount'], '', '', '0.0');

		$data['mobile'] = Common::html_input('mobile', 'text', $customer['mobile']);
		$data['phone'] = Common::html_input('phone', 'text', $customer['phone']);
		$data['fax'] = Common::html_input('fax', 'text', $customer['fax']);
		$data['facebook'] = Common::html_input('facebook', 'text', $customer['facebook']);
		$data['line'] = Common::html_input('line', 'text', $customer['line']);
		$data['website'] = Common::html_input('website', 'text', $customer['website']);
		$data['email'] = Common::html_input('email', 'text', $customer['email']);
		$data['contact'] = Common::html_input('contact', 'text', $customer['contact']);
		$data['address'] = Common::html_textarea('address', 3, 40, $customer['address']);
		$data['internal_note'] = Common::html_textarea('internal_note', 3, 40, $customer['internal_note']);
		$data['country'] = $customer['country'];

		$business_type_list = $this->customer_model->get_business_types();
		if (!empty($business_type_list) && count($business_type_list) >= 2) {
			$data['business_type_label_1'] = $business_type_list[0]['name'];
			$data['business_type_label_2'] = $business_type_list[1]['name'];
			$data['business_type_value_1'] = array('name' => 'business_type', 'value' => 2, 'checked' => $customer['business_type'] == $business_type_list[0]['id']);
			$data['business_type_value_2'] = array('name' => 'business_type', 'value' => 1, 'checked' => $customer['business_type'] == $business_type_list[1]['id'], 'class' => 'ml-2');
		}

		$customer_type_list = $this->customer_model->get_customer_types();
		if (!empty($customer_type_list) && count($customer_type_list) >= 2) {
			$data['customer_type_label_1'] = $customer_type_list[0]['name'];
			$data['customer_type_label_2'] = $customer_type_list[1]['name'];
			$data['customer_type_value_1'] = array('name' => 'customer_type', 'value' => $customer_type_list[0]['id'], 'checked' => $customer['customer_type'] == $customer_type_list[0]['id']);
			$data['customer_type_value_2'] = array('name' => 'customer_type', 'value' => $customer_type_list[1]['id'], 'checked' => $customer['customer_type'] == $customer_type_list[1]['id'], 'class' => 'ml-2');
		}

		//-- Dropdown
		$data['continent'] = $customer['continent'];
		$data['continent_dropdown_default'] = empty($data['continent']) ? 'AS' : $data['continent'];
		$data['continent_dropdown_data'] = Common::array_to_options($this->common_model->get_continents(), 'continent_code', 'continent_name');

		//-- IMAGE
		$data['image_path'] = $customer['image_path'];

		//--- Messages
		$data['message'] = Common::html_alert('alert-info',$this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger',$this->session->flashdata('errorMessage'));

		//=== SUBMIT URL
		$data['submit_url'] = base_url('customer/edit/' . urlencode(base64_encode($customer['id'])));

		//=== TEMPLATE
		$save_button = Common::html_button('btn_create', '<i class="fas fa-save"></i> Save', 'btn-primary-pz text-light', '', 'onclickSave()');
		$discard_button = Common::html_button('btn_discard', 'Cancel', 'btn-default ml-1', base_url('customer'), '');
		$data['action_buttons'] = $save_button . $discard_button;	
		$data['title'] = 'Edit Customer';
		$data['user'] = $user;
		$data['menu_active_1'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/sale/edit_customer.css'));
		$this->template->javascript->add(base_url('assets/js/sale/customer_form.js'));
		$this->template->stylesheet->add(base_url('assets/css/image_upload.css'));
		$this->template->javascript->add(base_url('assets/js/image_upload.js'));
        $this->template->content->view('customer_form', $data);
        $this->template->publish();
	}

	public function create()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		//=== END AUTHEN

		//=== GET USER LOGIN
		$user = $this->ion_auth->user()->row();

		//=== VALIDATION
		$this->set_customer_validations();

		//=== SUBMIT
		if ($this->input->post() && $this->form_validation->run() === TRUE) {
			$insert_data = $this->get_post_customer_data();

			//Upload file
			$success = TRUE;
			$file_path = '';
			if (!empty($_FILES['image_upload']['name'])) {
				$success = $this->upload->do_upload('image_upload');
				$upload_data = $this->upload->data();
				$file_path =  $this->config->item('upload_dir') . $upload_data['file_name'];
				$insert_data['image_path'] = $file_path;
			}

			if ($success) {
				$afffected_row = $this->customer_model->insert_customer($insert_data, $user->id);
				if ($afffected_row > 0) {
					redirect(base_url('customer'), 'refresh');
				} else {
					$this->session->set_flashdata('errorMessage', 'Create fail.');
				}
			} else {
				$this->session->set_flashdata('errorMessage', 'Create fail, cannot upload file.');
			}
		}

		//--- Messages
		$data['message'] = Common::html_alert('alert-info',$this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger',$this->session->flashdata('errorMessage'));

		//--- HTML Inputs
		$data['customer_id'] = Common::html_input('customer_id', 'text', '', '', 'font-size: 20px;', '', TRUE);
		$data['customer_name'] = Common::html_input('customer_name', 'text', $this->form_validation->set_value('customer_name'));
		$data['short_name'] = Common::html_input('short_name', 'text', $this->form_validation->set_value('short_name'));
		$data['credit'] = Common::html_input('credit', 'number', $this->form_validation->set_value('credit'), '', '', '0.0');
		$data['discount'] = Common::html_input('discount', 'number', $this->form_validation->set_value('discount'), '', '', '0.0');

		$data['mobile'] = Common::html_input('mobile', 'text', $this->form_validation->set_value('mobile'));
		$data['phone'] = Common::html_input('phone', 'text', $this->form_validation->set_value('phone'));
		$data['fax'] = Common::html_input('fax', 'text', $this->form_validation->set_value('fax'));
		$data['facebook'] = Common::html_input('facebook', 'text', $this->form_validation->set_value('facebook'));
		$data['line'] = Common::html_input('line', 'text', $this->form_validation->set_value('line'));
		$data['website'] = Common::html_input('website', 'text', $this->form_validation->set_value('website'));
		$data['email'] = Common::html_input('email', 'text', $this->form_validation->set_value('email'));
		$data['contact'] = Common::html_input('contact', 'text', $this->form_validation->set_value('contact'));
		$data['address'] = Common::html_textarea('address', 3, 40, $this->form_validation->set_value('address'));
		$data['internal_note'] = Common::html_textarea('internal_note', 3, 40, $this->form_validation->set_value('internal_note'));
		$data['country'] = $this->form_validation->set_value('country');

		$business_type_list = $this->customer_model->get_business_types();
		if (!empty($business_type_list) && count($business_type_list) >= 2) {
			$data['business_type_label_1'] = $business_type_list[0]['name'];
			$data['business_type_label_2'] = $business_type_list[1]['name'];
			$data['business_type_value_1'] = array('name' => 'business_type', 'value' => 1, 'checked' => (empty($this->form_validation->set_value('business_type')) || $this->form_validation->set_value('business_type') == $business_type_list[0]['id']));
			$data['business_type_value_2'] = array('name' => 'business_type', 'value' => 2, 'checked' => $this->form_validation->set_value('business_type') == $business_type_list[1]['id'], 'class' => 'ml-2');
		}

		$customer_type_list = $this->customer_model->get_customer_types();
		if (!empty($customer_type_list) && count($customer_type_list) >= 2) {
			$data['customer_type_label_1'] = $customer_type_list[0]['name'];
			$data['customer_type_label_2'] = $customer_type_list[1]['name'];
			$data['customer_type_value_1'] = array('name' => 'customer_type', 'value' => $customer_type_list[0]['id'], 'checked' => $this->form_validation->set_value('customer_type') == $customer_type_list[0]['id']);
			$data['customer_type_value_2'] = array('name' => 'customer_type', 'value' => $customer_type_list[1]['id'], 'checked' => $this->form_validation->set_value('customer_type') == $customer_type_list[1]['id'], 'class' => 'ml-2');
		}

		//-- Dropdown
		$data['continent'] = $this->form_validation->set_value('continent');
		$data['continent_dropdown_default'] = empty($data['continent']) ? 'AS' : $data['continent'];
		$data['continent_dropdown_data'] = Common::array_to_options($this->common_model->get_continents(), 'continent_code', 'continent_name');

		//-- IMAGE
		$data['image_path'] = '';

		//=== SUBMIT URL
		$data['submit_url'] = base_url('customer/create');

		//=== TEMPLATE
		$save_button = Common::html_button('btn_create', '<i class="fas fa-save"></i> Save', 'btn-primary-pz text-light', '', 'onclickSave()');
		$discard_button = Common::html_button('btn_discard', 'Cancel', 'btn-default ml-1', base_url('customer'), '');
		$data['action_buttons'] = $save_button . $discard_button;	
		$data['title'] = 'Create Customer';
		$data['user'] = $user;
		$data['menu_active_1'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/sale/create_customer.css'));
		$this->template->javascript->add(base_url('assets/js/sale/customer_form.js'));
		$this->template->stylesheet->add(base_url('assets/css/image_upload.css'));
		$this->template->javascript->add(base_url('assets/js/image_upload.js'));
        $this->template->content->view('customer_form', $data);
        $this->template->publish();
	}

	private function get_post_customer_data()
	{
		return array(
			'name' => $this->input->post('customer_name'),
			'short_name' => $this->input->post('short_name'),
			'business_type' => $this->input->post('business_type'),
			'customer_type' => $this->input->post('customer_type'),
			'credit' => $this->input->post('credit'),
			'discount' => $this->input->post('discount'),
			'continent' => $this->input->post('continent'),
			'country' => $this->input->post('country'),

			'mobile' => $this->input->post('mobile'),
			'phone' => $this->input->post('phone'),
			'fax' => $this->input->post('fax'),
			'facebook' => $this->input->post('facebook'),
			'line' => $this->input->post('line'),
			'website' => $this->input->post('website'),
			'email' => $this->input->post('email'),
			'contact' => $this->input->post('contact'),
			'address' => $this->input->post('address'),
			'internal_note' => $this->input->post('internal_note'),
		);
	}

	private function set_customer_validations()
	{
		//--- Customer Detail
		// $this->form_validation->set_rules('customer_id', $this->lang->line('customer_id'), 'required');
		$this->form_validation->set_rules('customer_name', $this->lang->line('customer_name'), 'required|min_length[0]');
		$this->form_validation->set_rules('short_name', $this->lang->line('short_name'), 'min_length[0]');
		$this->form_validation->set_rules('credit', $this->lang->line('credit'), 'numeric|min_length[0]');
		$this->form_validation->set_rules('discount', $this->lang->line('discount'), 'numeric|min_length[0]');
		$this->form_validation->set_rules('business_type', $this->lang->line('business_type'), 'required');
		$this->form_validation->set_rules('customer_type', $this->lang->line('customer_type'), 'required');
		$this->form_validation->set_rules('continent', $this->lang->line('continent'), 'min_length[0]');
		$this->form_validation->set_rules('country', $this->lang->line('country'), 'min_length[0]');

		//--- Customer Contact
		$this->form_validation->set_rules('mobile', $this->lang->line('mobile'), 'min_length[0]');
		$this->form_validation->set_rules('phone', $this->lang->line('phone'), 'min_length[0]');
		$this->form_validation->set_rules('fax', $this->lang->line('fax'), 'min_length[0]');
		$this->form_validation->set_rules('facebook', $this->lang->line('facebook'), 'min_length[0]');
		$this->form_validation->set_rules('line', $this->lang->line('line'), 'min_length[0]');
		$this->form_validation->set_rules('website', $this->lang->line('website'), 'min_length[0]');
		$this->form_validation->set_rules('email', $this->lang->line('email'), 'min_length[0]');
		$this->form_validation->set_rules('address', $this->lang->line('address'), 'required|min_length[0]');
		$this->form_validation->set_rules('internal_note', $this->lang->line('internal_note'), 'min_length[0]');
	}

	public function get_customer_list_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$resp['data'] = $this->customer_model->get_joined_customers();
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}
    
    public function get_countries_list_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$resp['data'] = $this->common_model->get_countries($this->input->get('continent'));
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

	public function export_excel() {
		$customer_id_arr = Common::convert_value_to_array($this->input->post('customer_ids'));
        $data_arr = $this->customer_model->get_joined_customers($customer_id_arr);

        $objPHPExcel = new PHPExcel();

        //Set cell width
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension("B")->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension("C")->setWidth(40);
        $objPHPExcel->getActiveSheet()->getColumnDimension("D")->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension("E")->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension("F")->setWidth(20);
        //-- Header Style
        $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A2:F2")->getFont()->setBold(true);

        //-- Header
        $header = "CUSTOMERS";

        $objPHPExcel->getActiveSheet()->mergeCells("A1:F1");
        $objPHPExcel->getActiveSheet()
                ->setCellValue("A1", $header);

        //-- Table Header Style
		$header_columns = "A2:F2";
        $objPHPExcel->getActiveSheet()->getStyle($header_columns)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle($header_columns)->getFont()->setBold(true);

		//-- Table Header
		$row = 2;
        $objPHPExcel->getActiveSheet()
                ->setCellValue("A" . $row, "Customer ID")
                ->setCellValue("B" . $row, "Customer")
                ->setCellValue("C" . $row, "Address")
                ->setCellValue("D" . $row, "Telephone No.")
				->setCellValue("E" . $row, "Contact Person")
				->setCellValue("F" . $row, "E-mail Address");

		//-- Table Body
		if (!empty($data_arr)) {
			$row = 3;
			foreach ($data_arr as $value) {
				//Column Style
				$objPHPExcel->getActiveSheet()->getStyle("A" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("B" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("C" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT); 
				$objPHPExcel->getActiveSheet()->getStyle("D" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("E" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("F" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

				$tel_no = $this->generate_tel_no($value["mobile"], $value["phone"]);

				$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue("A" . $row, $value["id"])
						->setCellValue("B" . $row, $value["name"])
						->setCellValue("C" . $row, $value["address"])
						->setCellValue("D" . $row, $tel_no)
						->setCellValue("E" . $row, $value["contact"])
						->setCellValue("F" . $row, $value["email"]);
				
				//Cell Style
				$objPHPExcel->getActiveSheet()->getStyle("A" . $row . ":" . "F" . $row)->getAlignment()->setWrapText(true);
				$objPHPExcel->getActiveSheet()->getStyle("A" . $row . ":" . "F" . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

				$row++;
			}

			//-- Export Date
			$row++;
			$objPHPExcel->getActiveSheet()->getStyle("F" . $row)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("F" . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->setCellValue("F" . $row, Common::get_current_date());
		}

        ob_end_clean();

        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=" . Common::generate_report_name('CUSTOMERS', 'xlsx'));
        header("Cache-Control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objWriter->save("php://output");
	}

	private function generate_tel_no($mobile, $phone)
	{
		if ($mobile != '') {
			if ($phone != '') {
				return $mobile . ',' . $phone;
			} else {
				return $mobile;
			}

		} else {
			return $phone;
		}
	}
}
