<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'libraries/Excel.php';
require_once('Common.php');

class Fg_shift_switch extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		$this->load->model(array('fg_shift_switch_model'));
    }

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}

		//=== GET USER LOGIN
		$user = $this->ion_auth->user()->row();

		//-- Export Excel
		//$data['export_excel'] = Common::html_export_component('export_excel_form', 'exportExcel', 'employee_ids',  base_url('employee/export_excel'));

		//--- Messages
		$data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

		//=== TEMPLATE
		$data['title'] = 'Shift Switch';
		$data['display_action_search'] = FALSE;
		$data['user'] = $user;
		$data['menu_active_204'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/fg/fg_shift_switch.css'));
		$this->template->javascript->add(base_url('assets/js/fg/fg_shift_switch.js'));
        $this->template->content->view('fg_shift_switch', $data);
        $this->template->publish();
	}

	public function get_shift_switch_list_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$resp['data'] = $this->fg_shift_switch_model->get_shift_switches();
			$success = TRUE;
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

	public function insert_shift_switch_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$user = $this->ion_auth->user()->row();
			$date = $this->input->post('date');
			$shift_a = $this->input->post('shift_a');
			$shift_b = $this->input->post('shift_b');

			if (!empty($date) && !empty($shift_a) && !empty($shift_b)) {
				$data = array('date' => Common::convert_date_display_to_sql($date),
				'shift_a' => $shift_a,
				'shift_b' => $shift_b);
				$affected_rows = $this->fg_shift_switch_model->insert_shift_switch($data, $user->id);
				$success = $affected_rows > 0;
			}
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

	public function edit_shift_switch_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$user = $this->ion_auth->user()->row();
			$id = $this->input->post('id');
			$date = $this->input->post('date');
			$shift_a = $this->input->post('shift_a');
			$shift_b = $this->input->post('shift_b');

			if (!empty($id) && !empty($date) && !empty($shift_a) && !empty($shift_b)) {
				$data = array('id' => $id,
				'date' => Common::convert_date_display_to_sql($date),
				'shift_a' => $shift_a,
				'shift_b' => $shift_b);
				$affected_rows = $this->fg_shift_switch_model->update_shift_switch($id, $data, $user->id);
				$success = $affected_rows > 0;
			}
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}
}
