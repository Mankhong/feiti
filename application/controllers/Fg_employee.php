<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'libraries/Excel.php';
require_once('Common.php');

class Fg_employee extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		$this->load->library(array('upload'));
		$this->load->model(array('fg_employee_model', 'admin_model'));
		$this->config->load('upload');
    }

	public function index()
	{
		//== Authen
		$user = $this->ion_auth->user()->row();
		$login_data = Common::authen_personal($this, $user);

		if (!$login_data['is_admin']) {
			$this->ion_auth->logout();
			$this->session->set_flashdata('message', 'Please login with personal user.');
			redirect('auth/login', 'refresh');
        }
		//== END Authen

		//-- Export Excel
		$data['export_excel'] = Common::html_export_component('export_excel_form', 'exportExcel', array('employee_ids', 'resign_flag'),  base_url('fg_employee/export_excel'));

		//--- Messages
		$data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

		//=== TEMPLATE
		$create_button = Common::html_button('btn_create', '<i class="fas fa-plus-circle fa-sm"></i> Create', 'btn-primary-pz text-light', 'fg_employee/create', '');
		$data['action_buttons'] = $create_button;
		$data['title'] = 'Employees';
		$data['display_action_search'] = TRUE;
		$data['user'] = $user;
		$data['menu_active_202'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/fg/fg_employee.css'));
		$this->template->javascript->add(base_url('assets/js/fg/fg_employee.js'));
		$this->template->stylesheet->add(base_url('assets/css/sale/dataitems.css'));
		$this->template->javascript->add(base_url('assets/js/sale/dataitems.js'));
        $this->template->content->view('fg_employee', $data);
        $this->template->publish();
	}

	public function edit($id)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		
		//== Authen
		$user = $this->ion_auth->user()->row();
		$login_data = Common::authen_personal($this, $user);

		$is_admin = $login_data['is_admin'];
		$is_member = $login_data['is_member'];
		$is_account = $login_data['is_account'];
		$is_sub_contract = $login_data['is_sub_contract'];

		$data['is_admin'] = $is_admin;
		$data['is_member'] = $is_member;
		$data['is_account'] = $is_account;
		$data['is_sub_contract'] = $is_sub_contract;

		if (!$is_admin && !$is_member && !$is_account && !$is_sub_contract) {
			$logout = $this->ion_auth->logout();
			$this->session->set_flashdata('message', 'Please login with personal user.');
			redirect('auth/login', 'refresh');
		}
		//=== END AUTHEN

		//=== GET DATA
		$id = Common::decodeString($id);
		$employee = $this->fg_employee_model->get_employees($id)[0];
		$temp_employee_id = $employee['id'];

		//=== VALIDATION
		$this->set_employee_validations();

		//=== SUBMIT
		if ($this->input->post() && $this->form_validation->run() === TRUE) {
			$update_data = $this->get_post_employee_data();

			if (!empty($update_data['birthdate'])) {
				$update_data['birthdate'] = Common::convert_date_display_to_sql($update_data['birthdate']);
			} else {
				$update_data['birthdate'] = NULL;
			}

			if (!empty($update_data['startdate'])) {
				$update_data['startdate'] = Common::convert_date_display_to_sql($update_data['startdate']);
			} else {
				$update_data['startdate'] = NULL;
			}
			
			if (!empty($update_data['resign_date'])) {
				$update_data['resign_date'] = Common::convert_date_display_to_sql($update_data['resign_date']);
			} else {
				$update_data['resign_date'] = NULL;
			}
			
			if (!empty($update_data['resign_probation_date'])) {
				$update_data['resign_probation_date'] = Common::convert_date_display_to_sql($update_data['resign_probation_date']);
			} else {
				$update_data['resign_probation_date'] = NULL;
			}

			if (empty($update_data['shift'])) {
				$update_data['shift'] = NULL;
			}

			//Upload file
			$success = TRUE;
			$file_path = '';
			if (!empty($_FILES['image_upload']['name'])) {
				$success = $this->upload->do_upload('image_upload');
				$upload_data = $this->upload->data();
				$file_path =  $this->config->item('upload_dir') . $upload_data['file_name'];
				$update_data['image_path'] = $file_path;
			}

			if ($success) {
				$afffected_row = $this->fg_employee_model->update_employee($temp_employee_id, $update_data, $user->id);
				if ($afffected_row > 0) {
					$edited_employee = $this->fg_employee_model->get_employees($update_data['id'])[0];
					$this->update_employee_user($edited_employee);
				}

				redirect(base_url('fg_employee'), 'refresh');
			} else {
				$this->session->set_flashdata('errorMessage', 'Update fail, cannot upload file.');
			}
			
			
		}
		
		//--- HTML Inputs
		$data['temp_employee_id'] = $temp_employee_id;
		$data['id'] = Common::html_input('id', 'number', $employee['id'], '', 'font-size: 20px;');
		$data['employee_no'] = Common::html_input('employee_no', 'text', $employee['ems_no'], '', 'font-size: 20px;', '', TRUE);
		//$data['sensor_id'] = Common::html_input('sensor_id', 'text', $employee['sensor_id']);
		$data['firstname_th'] = Common::html_input('firstname_th', 'text', $employee['firstname_th']);
		$data['lastname_th'] = Common::html_input('lastname_th', 'text', $employee['lastname_th']);
		$data['firstname_en'] = Common::html_input('firstname_en', 'text', $employee['firstname_en']);
		$data['lastname_en'] = Common::html_input('lastname_en', 'text', $employee['lastname_en']);

		$data['pid'] = Common::html_input('pid', 'text', $employee['pid']);
		$data['tax_no'] = Common::html_input('tax_no', 'text', $employee['tax_no']);
		$data['bank_account_no'] = Common::html_input('bank_account_no', 'text', $employee['bank_account_no']);
		$data['study_field'] = Common::html_input('study_field', 'text', $employee['study_field']);
		$data['school'] = Common::html_input('school', 'text', $employee['school']);
		$data['ems_contact'] = Common::html_input('ems_contact', 'text', $employee['ems_contact']);
		$data['ems_phone'] = Common::html_input('ems_phone', 'text', $employee['ems_phone']);
		$data['ems_relation'] = Common::html_input('ems_relation', 'text', $employee['ems_relation']);
		$data['telephone'] = Common::html_input('telephone', 'text', $employee['telephone']);
		$data['house_no'] = Common::html_input('house_no', 'text', $employee['house_no']);
		$data['address'] = Common::html_input('address', 'text', $employee['address']);
		$data['line'] = Common::html_input('line', 'text', $employee['line']);
		$data['image_path'] = Common::html_input('image_path', 'text', $employee['image_path']);
		$data['work_address'] = Common::html_input('work_address', 'text', $employee['work_address']);
		$data['work_location'] = Common::html_input('work_location', 'text', $employee['work_location']);
		$data['work_email'] = Common::html_input('work_email', 'email', $employee['work_email']);
		$data['work_phone'] = Common::html_input('work_phone', 'text', $employee['work_phone']);
		$data['resign_reason'] = Common::html_input('resign_reason', 'text', $employee['resign_reason']);
		$data['resign_layoff'] = Common::html_input('resign_layoff', 'text', $employee['resign_layoff']);

		$data['birthdate'] = Common::html_input('birthdate', 'text', Common::convert_date_sql_to_display($employee['birthdate_fm']), 'datepicker', '', '', TRUE);
		$data['startdate'] = Common::html_input('startdate', 'text', Common::convert_date_sql_to_display($employee['startdate_fm']), 'datepicker', '', '', TRUE);
		$data['resign_date'] = Common::html_input('resign_date', 'text', Common::convert_date_sql_to_display($employee['resign_date_fm']), 'datepicker', '', '', FALSE);
		$data['resign_probation_date'] = Common::html_input('resign_probation_date', 'text', Common::convert_date_sql_to_display($employee['resign_probation_date_fm']), 'datepicker', '', '', TRUE);

		//*** DROPDOWNS */
		$data['type'] = $employee['type'];
		$data['type_dropdown_default'] = empty($data['type']) ? '1' : $data['type'];
		$data['type_dropdown_data'] = Common::array_to_options($this->common_model->get_employee_type($employee['type']), 'code', 'name');

		$data['nationality'] = $employee['nationality'];
		$data['nationality_dropdown_default'] = empty($data['nationality']) ? 'TH' : $data['nationality'];
		$data['nationality_dropdown_data'] = Common::array_to_options($this->common_model->get_nationality(), 'id', 'name');

		$data['certificate_level'] = $employee['certificate_level'];
		$data['certificate_level_dropdown_default'] = empty($data['certificate_level']) ? '1' : $data['certificate_level'];
		$data['certificate_level_dropdown_data'] = Common::array_to_options($this->common_model->get_certificate_level(), 'id', 'name');

		$data['marital_status'] = $employee['marital_status'];
		$data['marital_status_dropdown_default'] = empty($data['marital_status']) ? '1' : $data['marital_status'];
		$data['marital_status_dropdown_data'] = Common::array_to_options($this->common_model->get_marital_status(), 'id', 'name');

		$data['department_id'] = $employee['department_id'];
		$data['department_id_dropdown_default'] = empty($data['department_id']) ? '1' : $data['department_id'];
		$data['department_id_dropdown_data'] = Common::array_to_options($this->common_model->get_department(), 'id', 'name');

		$data['job_position_id'] = $employee['job_position_id'];
		$data['job_position_id_dropdown_default'] = empty($data['job_position_id']) ? '1' : $data['job_position_id'];
		$data['job_position_id_dropdown_data'] = Common::array_to_options($this->common_model->get_job_position(), 'id', 'name');

		$data['manager_id'] = $employee['manager_id'];
		$data['manager_id_dropdown_default'] = empty($data['manager_id']) ? '1' : $data['manager_id'];
		$data['manager_id_dropdown_data'] = Common::array_to_options($this->common_model->get_manager(), 'id', 'name');

		$data['shift'] = $employee['shift'];
		$data['shift_dropdown_default'] = empty($data['shift']) ? '' : $data['shift'];
		$data['shift_dropdown_data'] = Common::array_to_options($this->common_model->get_shift($is_sub_contract, $is_admin), 'id', 'name', '');

		$data['title_id'] = $employee['title_id'];
		$data['title_id_dropdown_default'] = empty($data['title_id']) ? '1' : $data['title_id'];
		$data['title_id_dropdown_data'] = Common::array_to_options($this->common_model->get_title(), 'id', 'name_eng');

		//--- Radio
		$gender_list = $this->common_model->get_gender();
		if (!empty($gender_list) && count($gender_list) >= 2) {
			$data['gender_label_1'] = $gender_list[0]['name'];
			$data['gender_label_2'] = $gender_list[1]['name'];
			$data['gender_value_1'] = array('name' => 'gender', 'value' => 1, 'checked' => $employee['gender'] == $gender_list[0]['id']);
			$data['gender_value_2'] = array('name' => 'gender', 'value' => 2, 'checked' => $employee['gender'] == $gender_list[1]['id'], 'class' => 'ml-2');
		}

		//-- IMAGE
		$data['image_path'] = $employee['image_path'];

		//--- Messages
		$data['message'] = Common::html_alert('alert-info',$this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger',$this->session->flashdata('errorMessage'));

		//=== SUBMIT URL
		$data['submit_url'] = base_url('fg_employee/edit/' . urlencode(base64_encode($employee['id'])));

		//=== TEMPLATE
		$save_button = Common::html_button('btn_create', '<i class="fas fa-save"></i> Save', 'btn-primary-pz text-light', '', 'onclickSave()');
		$discard_button = Common::html_button('btn_discard', 'Cancel', 'btn-default ml-1', base_url('fg_employee'), '');
		$data['action_buttons'] = $save_button . $discard_button;	
		$data['title'] = 'Edit Employee';
		$data['user'] = $user;
		$data['menu_active_202'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/fg/fg_employee_form.css'));
		$this->template->javascript->add(base_url('assets/js/fg/fg_employee_form.js'));
		$this->template->stylesheet->add(base_url('assets/css/image_upload.css'));
		$this->template->javascript->add(base_url('assets/js/image_upload.js'));
        $this->template->content->view('fg_employee_form', $data);
        $this->template->publish();
	}

	public function create()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		
		//== Authen
		$user = $this->ion_auth->user()->row();
		$login_data = Common::authen_personal($this, $user);

		$is_admin = $login_data['is_admin'];
		$is_member = $login_data['is_member'];
		$is_account = $login_data['is_account'];
		$is_sub_contract = $login_data['is_sub_contract'];

		$data['is_admin'] = $is_admin;
		$data['is_member'] = $is_member;
		$data['is_account'] = $is_account;
		$data['is_sub_contract'] = $is_sub_contract;

		if (!$is_admin && !$is_member && !$is_account && !$is_sub_contract) {
			$logout = $this->ion_auth->logout();
			$this->session->set_flashdata('message', 'Please login with personal user.');
			redirect('auth/login', 'refresh');
		}
		//=== END AUTHEN

		//=== VALIDATION
		$this->set_employee_validations();

		//=== SUBMIT
		if ($this->input->post() && $this->form_validation->run() === TRUE) {
			$insert_data = $this->get_post_employee_data();

			if (!empty($insert_data['birthdate'])) {
				$insert_data['birthdate'] = Common::convert_date_display_to_sql($insert_data['birthdate']);
			} else {
				$insert_data['birthdate'] = NULL;
			}

			if (!empty($insert_data['startdate'])) {
				$insert_data['startdate'] = Common::convert_date_display_to_sql($insert_data['startdate']);
			} else {
				$insert_data['startdate'] = NULL;
			}
			
			if (!empty($insert_data['resign_date'])) {
				$insert_data['resign_date'] = Common::convert_date_display_to_sql($insert_data['resign_date']);
			} else {
				$insert_data['resign_date'] = NULL;
			}
			
			if (!empty($insert_data['resign_probation_date'])) {
				$insert_data['resign_probation_date'] = Common::convert_date_display_to_sql($insert_data['resign_probation_date']);
			} else {
				$insert_data['resign_probation_date'] = NULL;
			}

			if (empty($insert_data['shift'])) {
				$insert_data['shift'] = NULL;
			}

			//Upload file
			$success = TRUE;
			$file_path = '';
			if (!empty($_FILES['image_upload']['name'])) {
				$success = $this->upload->do_upload('image_upload');
				$upload_data = $this->upload->data();
				$file_path =  $this->config->item('upload_dir') . $upload_data['file_name'];
				$insert_data['image_path'] = $file_path;
			}

			if ($success) {
				$afffected_row = $this->fg_employee_model->insert_employee($insert_data, $user->id);
				if ($afffected_row > 0) {
					redirect(base_url('fg_employee'), 'refresh');
				} else {
					$this->session->set_flashdata('errorMessage', 'Create fail.');
				}
			} else {
				$this->session->set_flashdata('errorMessage', 'Create fail, cannot upload file.');
			}
		}

		//--- Messages
		$data['message'] = Common::html_alert('alert-info',$this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger',$this->session->flashdata('errorMessage'));

		//--- HTML Inputs
		$data['id'] = Common::html_input('id', 'number', '', '', 'font-size: 20px;', '');
		$data['employee_no'] = Common::html_input('employee_no', 'text', '', '', 'font-size: 20px;', '', TRUE);
		//$data['sensor_id'] = Common::html_input('sensor_id', 'text', $this->form_validation->set_value('sensor_id'));
		$data['firstname_th'] = Common::html_input('firstname_th', 'text', $this->form_validation->set_value('firstname_th'));
		$data['lastname_th'] = Common::html_input('lastname_th', 'text', $this->form_validation->set_value('lastname_th'));
		$data['firstname_en'] = Common::html_input('firstname_en', 'text', $this->form_validation->set_value('firstname_en'));
		$data['lastname_en'] = Common::html_input('lastname_en', 'text', $this->form_validation->set_value('lastname_en'));

		$data['pid'] = Common::html_input('pid', 'text', $this->form_validation->set_value('pid'));
		$data['tax_no'] = Common::html_input('tax_no', 'text', $this->form_validation->set_value('tax_no'));
		$data['bank_account_no'] = Common::html_input('bank_account_no', 'text', $this->form_validation->set_value('bank_account_no'));
		$data['study_field'] = Common::html_input('study_field', 'text', $this->form_validation->set_value('study_field'));
		$data['school'] = Common::html_input('school', 'text', $this->form_validation->set_value('school'));
		$data['ems_contact'] = Common::html_input('ems_contact', 'text', $this->form_validation->set_value('ems_contact'));
		$data['ems_phone'] = Common::html_input('ems_phone', 'text', $this->form_validation->set_value('ems_phone'));
		$data['ems_relation'] = Common::html_input('ems_relation', 'text', $this->form_validation->set_value('ems_relation'));
		$data['telephone'] = Common::html_input('telephone', 'text', $this->form_validation->set_value('telephone'));
		$data['house_no'] = Common::html_input('house_no', 'text', $this->form_validation->set_value('house_no'));
		$data['address'] = Common::html_input('address', 'text', $this->form_validation->set_value('address'));
		$data['line'] = Common::html_input('line', 'text', $this->form_validation->set_value('line'));
		$data['image_path'] = Common::html_input('image_path', 'text', $this->form_validation->set_value('image_path'));
		$data['work_address'] = Common::html_input('work_address', 'text', $this->form_validation->set_value('work_address'));
		$data['work_location'] = Common::html_input('work_location', 'text', $this->form_validation->set_value('work_location'));
		$data['work_email'] = Common::html_input('work_email', 'text', $this->form_validation->set_value('work_email'));
		$data['work_phone'] = Common::html_input('work_phone', 'text', $this->form_validation->set_value('work_phone'));
		$data['resign_reason'] = Common::html_input('resign_reason', 'text', $this->form_validation->set_value('resign_reason'));
		$data['resign_layoff'] = Common::html_input('resign_layoff', 'text', $this->form_validation->set_value('resign_layoff'));

		$data['birthdate'] = Common::html_input('birthdate', 'text', Common::convert_date_sql_to_display($this->form_validation->set_value('birthdate')), 'datepicker', '', '', TRUE);
		$data['startdate'] = Common::html_input('startdate', 'text', Common::convert_date_sql_to_display($this->form_validation->set_value('startdate')), 'datepicker', '', '', TRUE);
		$data['resign_date'] = Common::html_input('resign_date', 'text', Common::convert_date_sql_to_display($this->form_validation->set_value('resign_date')), 'datepicker', '', '', FALSE);
		$data['resign_probation_date'] = Common::html_input('resign_probation_date', 'text', Common::convert_date_sql_to_display($this->form_validation->set_value('resign_probation_date')), 'datepicker', '', '', TRUE);


		//*** DROPDOWNS */
		$data['type'] = $this->form_validation->set_value('type');
		$data['type_dropdown_default'] = empty($data['type']) ? '1' : $data['type'];
		$data['type_dropdown_data'] = Common::array_to_options($this->common_model->get_employee_type(), 'code', 'name');

		$data['nationality'] = $this->form_validation->set_value('nationality');
		$data['nationality_dropdown_default'] = empty($data['nationality']) ? 'TH' : $data['nationality'];
		$data['nationality_dropdown_data'] = Common::array_to_options($this->common_model->get_nationality(), 'id', 'name');

		$data['certificate_level'] = $this->form_validation->set_value('certificate_level');
		$data['certificate_level_dropdown_default'] = empty($data['certificate_level']) ? '1' : $data['certificate_level'];
		$data['certificate_level_dropdown_data'] = Common::array_to_options($this->common_model->get_certificate_level(), 'id', 'name');

		$data['marital_status'] = $this->form_validation->set_value('marital_status');
		$data['marital_status_dropdown_default'] = empty($data['marital_status']) ? '1' : $data['marital_status'];
		$data['marital_status_dropdown_data'] = Common::array_to_options($this->common_model->get_marital_status(), 'id', 'name');

		$data['department_id'] = $this->form_validation->set_value('department_id');
		$data['department_id_dropdown_default'] = empty($data['department_id']) ? '1' : $data['department_id'];
		$data['department_id_dropdown_data'] = Common::array_to_options($this->common_model->get_department(), 'id', 'name');

		$data['job_position_id'] = $this->form_validation->set_value('job_position_id');
		$data['job_position_id_dropdown_default'] = empty($data['job_position_id']) ? '1' : $data['job_position_id'];
		$data['job_position_id_dropdown_data'] = Common::array_to_options($this->common_model->get_job_position(), 'id', 'name');

		$data['manager_id'] = $this->form_validation->set_value('manager_id');
		$data['manager_id_dropdown_default'] = empty($data['manager_id']) ? '1' : $data['manager_id'];
		$data['manager_id_dropdown_data'] = Common::array_to_options($this->common_model->get_manager(), 'id', 'name');

		$data['shift'] = $this->form_validation->set_value('shift');
		$data['shift_dropdown_default'] = empty($data['shift']) ? '' : $data['shift'];
		$data['shift_dropdown_data'] = Common::array_to_options($this->common_model->get_shift($is_sub_contract, $is_admin), 'id', 'name', '');

		$data['title_id'] = $this->form_validation->set_value('title_id');
		$data['title_id_dropdown_default'] = empty($data['title_id']) ? '1' : $data['title_id'];
		$data['title_id_dropdown_data'] = Common::array_to_options($this->common_model->get_title(), 'id', 'name_eng');

		//--- Radio
		$gender_list = $this->common_model->get_gender();
		if (!empty($gender_list) && count($gender_list) >= 2) {
			$data['gender_label_1'] = $gender_list[0]['name'];
			$data['gender_label_2'] = $gender_list[1]['name'];
			$data['gender_value_1'] = array('name' => 'gender', 'value' => 1, 'checked' => (empty($this->form_validation->set_value('gender')) || $this->form_validation->set_value('gender') == $gender_list[0]['id']));
			$data['gender_value_2'] = array('name' => 'gender', 'value' => 2, 'checked' => $this->form_validation->set_value('gender') == $gender_list[1]['id'], 'class' => 'ml-2');
		}

		//-- IMAGE
		$data['image_path'] = '';

		//=== SUBMIT URL
		$data['submit_url'] = base_url('fg_employee/create');

		//=== TEMPLATE
		$save_button = Common::html_button('btn_create', '<i class="fas fa-save"></i> Save', 'btn-primary-pz text-light', '', 'onclickSave()');
		$discard_button = Common::html_button('btn_discard', 'Cancel', 'btn-default ml-1', base_url('fg_employee'), '');
		$data['action_buttons'] = $save_button . $discard_button;	
		$data['title'] = 'Create Employee';
		$data['user'] = $user;
		$data['menu_active_202'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/fg/fg_employee_form.css'));
		$this->template->javascript->add(base_url('assets/js/fg/fg_employee_form.js'));
		$this->template->stylesheet->add(base_url('assets/css/image_upload.css'));
		$this->template->javascript->add(base_url('assets/js/image_upload.js'));
        $this->template->content->view('fg_employee_form', $data);
        $this->template->publish();
	}

	private function get_post_employee_data()
	{
		return array(
			'id' => $this->input->post('id'),
			'type' => $this->input->post('type'),
			//'sensor_id' => $this->input->post('sensor_id'),
			'firstname_th' => $this->input->post('firstname_th'),
			'lastname_th' => $this->input->post('lastname_th'),
			'firstname_en' => $this->input->post('firstname_en'),
			'lastname_en' => $this->input->post('lastname_en'),
			'nationality' => $this->input->post('nationality'),
			'pid' => $this->input->post('pid'),
			'tax_no' => $this->input->post('tax_no'),
			'bank_account_no' => $this->input->post('bank_account_no'),
			'certificate_level' => $this->input->post('certificate_level'),
			'study_field' => $this->input->post('study_field'),
			'school' => $this->input->post('school'),
			'ems_contact' => $this->input->post('ems_contact'),
			'ems_phone' => $this->input->post('ems_phone'),
			'ems_relation' => $this->input->post('ems_relation'),
			'telephone' => $this->input->post('telephone'),
			'house_no' => $this->input->post('house_no'),
			'address' => $this->input->post('address'),
			'line' => $this->input->post('line'),
			'birthdate' => $this->input->post('birthdate'),
			'gender' => $this->input->post('gender'),
			'marital_status' => $this->input->post('marital_status'),

			'department_id' => $this->input->post('department_id'),
			'job_position_id' => $this->input->post('job_position_id'),
			'manager_id' => $this->input->post('manager_id'),
			'shift' => $this->input->post('shift'),
			'startdate' => $this->input->post('startdate'),
			'work_address' => $this->input->post('work_address'),
			'work_location' => $this->input->post('work_location'),
			'work_email' => $this->input->post('work_email'),
			'work_phone' => $this->input->post('work_phone'),
			'resign_date' => $this->input->post('resign_date'),
			'resign_reason' => $this->input->post('resign_reason'),
			'resign_layoff' => $this->input->post('resign_layoff'),
			'resign_probation_date' => $this->input->post('resign_probation_date'),
			'title_id' => $this->input->post('title_id'),
		);
	}

	private function set_employee_validations()
	{
		//--- Employee
		$this->form_validation->set_rules('id', $this->lang->line('id'), 'required');
		$this->form_validation->set_rules('type', $this->lang->line('type'), 'min_length[0]');
		//$this->form_validation->set_rules('sensor_id', $this->lang->line('sensor_id'), 'min_length[0]');
		$this->form_validation->set_rules('firstname_th', $this->lang->line('firstname_th'), 'required|min_length[0]');
		$this->form_validation->set_rules('lastname_th', $this->lang->line('lastname_th'), 'required|min_length[0]');
		$this->form_validation->set_rules('firstname_en', $this->lang->line('firstname_en'), 'min_length[0]');
		$this->form_validation->set_rules('lastname_en', $this->lang->line('lastname_en'), 'min_length[0]');
		$this->form_validation->set_rules('nationality', $this->lang->line('nationality'), 'min_length[0]');
		$this->form_validation->set_rules('pid', $this->lang->line('pid'), 'min_length[0]');
		$this->form_validation->set_rules('tax_no', $this->lang->line('tax_no'), 'min_length[0]');
		$this->form_validation->set_rules('bank_account_no', $this->lang->line('bank_account_no'), 'min_length[0]');
		$this->form_validation->set_rules('certificate_level', $this->lang->line('certificate_level'), 'min_length[0]');
		$this->form_validation->set_rules('study_field', $this->lang->line('study_field'), 'min_length[0]');
		$this->form_validation->set_rules('school', $this->lang->line('school'), 'min_length[0]');
		$this->form_validation->set_rules('ems_contact', $this->lang->line('ems_contact'), 'min_length[0]');
		$this->form_validation->set_rules('ems_phone', $this->lang->line('ems_phone'), 'min_length[0]');
		$this->form_validation->set_rules('ems_relation', $this->lang->line('ems_relation'), 'min_length[0]');
		$this->form_validation->set_rules('telephone', $this->lang->line('telephone'), 'min_length[0]');
		$this->form_validation->set_rules('house_no', $this->lang->line('house_no'), 'min_length[0]');
		$this->form_validation->set_rules('address', $this->lang->line('address'), 'min_length[0]');
		$this->form_validation->set_rules('line', $this->lang->line('line'), 'min_length[0]');
		$this->form_validation->set_rules('birthdate', $this->lang->line('birthdate'), 'min_length[0]');
		$this->form_validation->set_rules('gender', $this->lang->line('gender'), 'min_length[0]');
		$this->form_validation->set_rules('marital_status', $this->lang->line('marital_status'), 'min_length[0]');
		$this->form_validation->set_rules('image_path', $this->lang->line('image_path'), 'min_length[0]');

		//--- Work Information
		$this->form_validation->set_rules('department_id', $this->lang->line('department_id'), 'min_length[0]');
		$this->form_validation->set_rules('job_position_id', $this->lang->line('job_position_id'), 'min_length[0]');
		$this->form_validation->set_rules('manager_id', $this->lang->line('manager_id'), 'min_length[0]');
		$this->form_validation->set_rules('shift', $this->lang->line('shift'), 'min_length[0]');
		$this->form_validation->set_rules('startdate', $this->lang->line('startdate'), 'min_length[0]');
		$this->form_validation->set_rules('work_address', $this->lang->line('work_address'), 'min_length[0]');
		$this->form_validation->set_rules('work_location', $this->lang->line('work_location'), 'min_length[0]');
		$this->form_validation->set_rules('work_email', $this->lang->line('work_email'), 'valid_email');
		$this->form_validation->set_rules('work_phone', $this->lang->line('work_phone'), 'min_length[0]');
		$this->form_validation->set_rules('resign_date', $this->lang->line('resign_date'), 'min_length[0]');
		$this->form_validation->set_rules('resign_reason', $this->lang->line('resign_reason'), 'min_length[0]');
		$this->form_validation->set_rules('resign_layoff', $this->lang->line('resign_layoff'), 'min_length[0]');
		$this->form_validation->set_rules('resign_probation_date', $this->lang->line('resign_probation_date'), 'min_length[0]');
	}

	public function get_employee_list_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$user = $this->ion_auth->user()->row();
			$login_data = Common::authen_personal($this, $user);
			$resign_flag = $this->input->get('resign_flag');

			$resp['data'] = $this->fg_employee_model->get_joined_employees('', $login_data['is_sub_contract'], $resign_flag);
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

	public function export_excel() {
		$user = $this->ion_auth->user()->row();
		$login_data = Common::authen_personal($this, $user);

		$employee_id_arr = Common::convert_value_to_array($this->input->post('employee_ids'));
		$resign_flag = $this->input->post('resign_flag');
        $data_arr = $this->fg_employee_model->get_joined_employees($employee_id_arr, $login_data['is_sub_contract'], $resign_flag);

		$objPHPExcel = new PHPExcel();
		$resign_date_column = ($resign_flag == "Y" ? "Resign Date" : "Probation Date");
		$resign_date_column_query = ($resign_flag == "Y" ? "fm_resign_date" : "fm_resign_probation_date");
		
		//Set cell width
		$columns = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K");
		$column_names = array("No.", "Employee ID", "Employee Name", "Department", "Job Position", "Start Date", "Date of Birth", "ID Card", "Place of in home", "Certificate Level", $resign_date_column);
		$column_count = count($columns);

		//Cell Width
		for ($i = 0; $i < $column_count; $i++) {
			$cw = 15;
			if ($i == 0) {
				$cw = 5;
			} else if ($i == 2 || $i == 3 || $i ==4 || $i == 7 || $i == 9) {
				$cw = 20;
			} else if ($i == 8) {
				$cw = 60;
			}

			$objPHPExcel->getActiveSheet()->getColumnDimension($columns[$i])->setWidth($cw);
		}

		 //-- Header Style
		 $objPHPExcel->getActiveSheet()->getStyle($columns[0] . "1:" . $columns[count($columns)-1] . "2")->getFont()->setBold(true);

        //-- Header
		$header = "EMPLOYEE";

        $objPHPExcel->getActiveSheet()->mergeCells($columns[0] . "1:" . $columns[count($columns)-1] . "1");
        $objPHPExcel->getActiveSheet()->setCellValue("A1", $header);

        //-- Table Header Style
		$header_columns = $columns[0] . "2:" . $columns[count($columns)-1] . "2";
        $objPHPExcel->getActiveSheet()->getStyle($header_columns)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle($header_columns)->getFont()->setBold(true);

		//-- Table Header
		$row = 2;
		$bc = '000000';
		$border_style= array('borders' => array(
			'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => $bc)),
			'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => $bc)),
			'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => $bc)),
			'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => $bc))));

		for ($i = 0; $i < $column_count; $i++) {
			$objPHPExcel->getActiveSheet()->setCellValue($columns[$i] . $row, $column_names[$i]);
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			//$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DDDDDD');
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->applyFromArray($border_style);
		}

		//Value Keys
		$value_keys = array(NULL, "ems_no", "fullname_th", "department_id", "job_position_name", "fm_startdate", "fm_birthdate", "pid", "house_no", "certificate_level_name", $resign_date_column_query);

		//-- Table Body
		if (!empty($data_arr)) {
			$row++;
			$no = 1;
			foreach ($data_arr as $value) {

				//Column Value
				for ($i = 0; $i < $column_count; $i++) {
					$k = $value_keys[$i];
					$v = NULL;

					//-- VALUES
					if ($i == 0) { //No.
						$v = ($row - 2);
					} else {
						$v = ($k == NULL ? "" : $value[$k]);
					}

					$objPHPExcel->setActiveSheetIndex(0)->setCellValue($columns[$i] . $row, $v);

					//Cell Style
					$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->applyFromArray($border_style);
					if ($i == 5 || $i == 6 || $i ==4 ) {
						$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					}
				}

				$row++;
			}
		}

        ob_end_clean();

        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=" . Common::generate_report_name('EMPLOYEE', 'xlsx'));
        header("Cache-Control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objWriter->save("php://output");
	}

	public function has_data_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$employee_id = Common::decodeString($this->input->get('employee_id'));
			$resp['has_data'] = $this->fg_employee_model->has_employee($employee_id);
			$success = TRUE;
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

	public function generate_employee_user_login_ajax()
	{
		$resp = array();
		$success = FALSE;

		//if ($this->ion_auth->logged_in()) {
			$employee_arr = $this->fg_employee_model->get_employees();

			if (!empty($employee_arr)) {
				for ($i = 0; $i < count($employee_arr); $i++) {
					$this->update_employee_user($employee_arr[$i]);
				}
			}
		//}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

	private function update_employee_user($employee)
	{
		$groupData = array(6); //Personal Member
		$company = 'feiti_employee';

		$user_id = $this->admin_model->get_user_id_by_username((string) $employee['id']);
		if ($user_id != NULL) {
			$user_id = $this->admin_model->delete_user($user_id);
		}

		$identity = (string) $employee['id'];
		$email = $employee['ems_no'];
		$password = $this->generate_employee_password($employee);

		$additional_data = array(
			'first_name' => $employee['firstname_th'],
			'last_name' => $employee['lastname_th'],
			'company' => $company,
		);

		if ($this->ion_auth->register($identity, $password, $email, $additional_data, $groupData)) {
			return TRUE;
		}

		return FALSE;
	}

	private function generate_employee_password($employee)
	{
		$birthdate = $employee['birthdate_fm'];

		if (strlen($birthdate) == 10) {
			$d_arr = explode('/', $birthdate);
			return $d_arr[2] . '' . $d_arr[1] . '' . (intval($d_arr[0]) + 543);
		}

		return 'password';
	}
	
}
