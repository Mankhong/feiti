<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'libraries/Excel.php';
require_once('Common.php');

class Fg_time_daily extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('fg_time_daily_model', 'fg_employee_model'));
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}

		//=== GET USER LOGIN
		$user = $this->ion_auth->user()->row();
		$is_admin = $this->admin_model->is_personal_admin($user->id);
		$is_member = $this->admin_model->is_personal_member($user->id);
		$is_account = $this->admin_model->is_personal_account($user->id);

		$data['is_admin'] = $is_admin;
		$data['is_member'] = $is_member;
		$data['is_account'] = $is_account;

		if (!$is_admin && !$is_member && !$is_account) {
			$logout = $this->ion_auth->logout();
			$this->session->set_flashdata('message', 'Please login with personal user.');
			redirect('auth/login', 'refresh');
		}

		//=== INPUTS
		$data['department'] = '';
		$data['department_dropdown_default'] = '';
		$data['department_dropdown_data'] = Common::array_to_options($this->common_model->get_department(), 'id', 'name', '-- All --');

		$employee_type_list = $this->common_model->get_employee_type();
		if (!empty($employee_type_list) && count($employee_type_list) >= 2) {
			$data['employee_type_label_1'] = $employee_type_list[0]['name'];
			$data['employee_type_label_2'] = $employee_type_list[1]['name'];
			$data['employee_type_value_1'] = array('name' => 'ctrEmployeeType', 'value' => $employee_type_list[0]['code'], 'checked' => TRUE);
			$data['employee_type_value_2'] = array('name' => 'ctrEmployeeType', 'value' => $employee_type_list[1]['code']);
		}

		//-- Export Excel
		$params = array('date', 'department', 'employeeNo', 'employeeType');
		if ($this->admin_model->is_personal_admin($user->id)) {
			$data['export_excel'] = Common::html_export_component('export_excel_form', 'exportExcel', $params,  base_url('fg_time_daily/export_excel'), 'Export Excel', 'btn-excel-pz');
		}

		//--- Messages
		$data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

		$data['title'] = 'Timesheet Daily';
		$data['hide_sub_topbar'] = TRUE;
		$data['display_small_topbar'] = TRUE;
		$data['display_action_search'] = FALSE;
		$data['user'] = $user;
		$data['menu_active_207'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/fg/fg.css'));
		$this->template->stylesheet->add(base_url('assets/css/fg/fg_time_daily.css'));
		$this->template->javascript->add(base_url('assets/js/fg/fg_time_daily.js'));
		$this->template->content->view('fg_time_daily', $data);
		$this->template->publish();
	}

	public function get_daily_timesheet_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$date = Common::decodeString($this->input->get('date'));
			$department = $this->input->get('department');
			$employee_type = $this->input->get('employee_type');
			$employee_no = Common::decodeString($this->input->get('employee_no'));

			if (!empty($date)) {
				$date = Common::convert_date_display_to_sql($date);
			}

			$resp['data'] = $this->fg_time_daily_model->get_timesheet_daily($date, $department, $employee_type, $employee_no);
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

	public function export_excel() {
		$date = Common::decodeString($this->input->post('date'));
		$department = $this->input->post('department');
		$employee_no = Common::decodeString($this->input->post('employeeNo'));
		$employee_type = $this->input->post('employeeType');

		if (!empty($date)) {
			$date = Common::convert_date_display_to_sql($date);
		}

		$data_arr = $this->fg_time_daily_model->get_timesheet_daily($date, $department, $employee_type, $employee_no);

        $objPHPExcel = new PHPExcel();

		//Set cell width
		$column_widths = array(8, 10, 30, 30, 20, 15, 15, 15, 15);
		$columns = array("A", "B", "C", "D", "E", "F", "G", "H", "I");
		$column_names = array("No.", "ID No.", "Employee Name", "Department", "Shift", "Leave Type", "Date", "Start Work", "End Work");
		$column_count = count($columns);

		$objPHPExcel->setActiveSheetIndex(0);
		
		//Column Width
		for ($i = 0; $i < $column_count; $i++) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($columns[$i])->setWidth($column_widths[$i]);
		}

        //-- Header Style
        //$objPHPExcel->getActiveSheet()->getStyle($columns[0] . "1:" . $columns[count($columns)-1] . "6")->getFont()->setBold(true);

		//-- Table Header Style
		$header_columns = $columns[0] . "1:" . $columns[count($columns)-1] . "2";
        $objPHPExcel->getActiveSheet()->getStyle($header_columns)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$objPHPExcel->getActiveSheet()->getStyle($header_columns)->getFont()->setBold(true);
		
        //-- Header
        $header = "Timesheet Daily";
        $objPHPExcel->getActiveSheet()->mergeCells($columns[0] . "1:" . $columns[count($columns)-1] . "1");
		$objPHPExcel->getActiveSheet()->setCellValue("A1", $header);

		//-- Table Header
		$row = 2;
		$bc = '000000';
		$border_style= array('borders' => array(
			'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => $bc)),
			'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => $bc)),
			'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => $bc)),
			'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => $bc))));

		for ($i = 0; $i < $column_count; $i++) {
			$objPHPExcel->getActiveSheet()->setCellValue($columns[$i] . $row, $column_names[$i]);
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			//$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DDDDDD');
			$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->applyFromArray($border_style);
		}

		//Value Keys
		$value_keys = array(NULL, "ems_no", "fullname_th", "department_name", "shift_name", "leave_type", "date", "start_work", "end_work");

		if (!empty($data_arr)) {
			$row++;
			$no = 1;
			foreach ($data_arr as $value) {

				//Column Value
				for ($i = 0; $i < $column_count; $i++) {
					$k = $value_keys[$i];
					$v = NULL;

					//Value
					if ($i == 0) { //No.
						$v = ($row - 2);
					} else {
						$v = ($k == NULL ? "" : $value[$k]);
					}

					//$v = Common::zero_to_empty($v);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue($columns[$i] . $row, $v);

					//Cell Style
					$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->applyFromArray($border_style);
					if ($i >= 2 && $i <=4) {
						$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					} else {
						$objPHPExcel->getActiveSheet()->getStyle($columns[$i] . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					}
				}
				$row++;
			}
		}

        ob_end_clean();

        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=" . Common::generate_report_name('Timesheet_Daily', 'xlsx'));
        header("Cache-Control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objWriter->save("php://output");
	}
	
}
