<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'libraries/Excel.php';
require_once('Common.php');

class Invoice extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		$this->load->model(array('invoice_model','customer_model', 'product_model'));
    }

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}

		//=== GET USER LOGIN
		$user = $this->ion_auth->user()->row();

		//-- Export Excel
		$data['export_excel'] = Common::html_export_component('export_excel_form', 'exportExcel', 'invoice_ids',  base_url('invoice/export_excel'));

		//--- Messages
		$data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

		//=== TEMPLATE
		$create_button = Common::html_button('btn_create', '<i class="fas fa-plus-circle fa-sm"></i> Create', 'btn-primary-pz text-light', 'invoice/create', '');
		$data['action_buttons'] = $create_button;
		$data['title'] = 'Invoice';
		$data['display_action_search'] = FALSE;
		$data['user'] = $user;
		$data['menu_active_4'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/sale/invoice.css'));
		$this->template->javascript->add(base_url('assets/js/sale/invoice.js'));
		$this->template->stylesheet->add(base_url('assets/css/sale/customer_picker.css'));
		$this->template->javascript->add(base_url('assets/js/sale/customer_picker.js'));
		$this->template->stylesheet->add(base_url('assets/css/sale/dataitems.css'));
		$this->template->javascript->add(base_url('assets/js/sale/dataitems.js'));
		$this->template->javascript->add(base_url('assets/js/sale/sale.js'));
        $this->template->content->view('invoice', $data);
        $this->template->publish();
	}

	public function edit($id)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		//=== END AUTHEN

		//=== GET USER LOGIN
		$user = $this->ion_auth->user()->row();

		//=== GET DATA
		$id = Common::decodeString($id);
		$invoice = $this->invoice_model->get_invoices($id)[0];
		$invoice_customer_id = $invoice['customer_id'];

		$cdata = empty($invoice_customer_id) ? NULL : $this->customer_model->get_customers($invoice_customer_id);

		$customer = NULL;
		if (!empty($cdata)) {
			$customer = $cdata[0];
		}

		$data['temp_invoice_id'] = $invoice['id'];

		//=== VALIDATION
		$this->set_invoice_validations();

		//=== SUBMIT
		if ($this->input->post()) {
			$update_dataitems = $this->input->post('update_dataitems[]');
			$update_product_arr = $this->convert_dataitems_to_update_array($update_dataitems);

			$insert_dataitems = $this->input->post('insert_dataitems[]');
			$insert_product_arr = $this->convert_dataitems_to_insert_array($invoice['id'], $insert_dataitems);

			$delete_dataitems = $this->input->post('delete_dataitems[]');
			$delete_product_arr = $this->convert_dataitems_to_delete_array($delete_dataitems);

			$data['update_dataitems'] = $update_dataitems;
			$data['insert_dataitems'] = $insert_dataitems;
			$data['delete_dataitems'] = $delete_dataitems;

			//Lotitems
			$lotitems = $this->input->post('lotitems[]');
			$lotitem_arr = $this->convert_lotitems_to_array($lotitems);

			if($this->form_validation->run() === TRUE) {
				$update_data = $this->get_post_invoice_data();

				if (!empty($update_data['date'])) {
					$update_data['date'] = Common::convert_date_display_to_sql($update_data['date']);
				}

				//if ($this->invoice_model->validate_balances($update_data['customer_id'], $insert_product_arr, $lotitem_arr)
				//	&& $this->invoice_model->validate_balances($update_data['customer_id'], $update_product_arr, $lotitem_arr)) {
					$afffected_row = $this->invoice_model->update_invoice($invoice['id'], $update_data, $update_product_arr, $insert_product_arr, $delete_product_arr, $lotitem_arr, $user->id);
					redirect(base_url('invoice'), 'refresh');
				//}else {
				//	$this->session->set_flashdata('errorMessage', 'Product unavailable, please check product quantity and balance.');
				//}
			}
		}

		//--- HTML Inputs
		$customer_id = '';
		$customer_name = '';
		if (!empty($customer)) {
			$customer_id = $customer['id'];
			$customer_name = $customer['name'];
			$data['customer_disabled'] = TRUE;

			//--- Cancel Invoice Button
			$data['render_cancel_invoice_button'] = TRUE;
		} else {
			$data['customer_disabled'] = FALSE;
		}

		$data['customer_id'] = Common::html_input('customer_id', 'text', $customer_id, '', '', '', TRUE);
		$data['customer_name'] = Common::html_input('customer_name', 'text', $customer_name, '', '', '', TRUE);
		$data['invoice_id'] = Common::html_input('invoice_id', 'text', $invoice['id'], '', '', '', TRUE);
		$data['id_suffix'] = Common::html_input('id_suffix', 'text', $invoice['id_suffix'], '', '', '', TRUE);
		$data['invoice_date'] = Common::html_input('invoice_date', 'text', Common::convert_date_sql_to_display($invoice['date']), 'datepicker', '', '', TRUE);
		$data['delivery_address'] = Common::html_textarea('delivery_address', 3, 40, $customer['address'], '', TRUE);
		$data['internal_note'] = $invoice['internal_note']; //Common::html_textarea('internal_note', 3, 40, $invoice['internal_note']);
		$data['refer_invoice_id'] = Common::html_textarea('refer_invoice_id', 3, 40, $invoice['refer_invoice_id']);

		//Invoice Group
		$data['invoice_group'] = $invoice['invoice_group'];

		//--- Radio
		$status_list = $this->common_model->get_statuses();
		if (!empty($status_list) && count($status_list) >= 2) {
			$data['status_label_1'] = $status_list[0]['name'];
			$data['status_label_2'] = $status_list[1]['name'];
			$data['status_value_1'] = array('name' => 'status', 'value' => 1, 'checked' => $invoice['status'] == $status_list[0]['id']);
			$data['status_value_2'] = array('name' => 'status', 'value' => 2, 'checked' => $invoice['status'] == $status_list[1]['id'], 'class' => 'ml-2');
		}

		// DROPDOWN
		$invoice_list = $this->product_model->get_invoice_types();
		$data['invoice_type'] = $invoice['invoice_type'];
		$data['invoice_type_dropdown_default'] = empty($data['invoice_type']) ? (!empty($invoice_list) ? $invoice_list[0]['id'] : '') : $data['invoice_type'];
		$data['invoice_type_dropdown_data'] = Common::array_to_options($invoice_list, 'id', 'name');

		//--- END HTML Inputs

		//-- PRODUCT LIST
		$data['product_list'] = $this->invoice_model->get_invoice_products($invoice['id']);

		//-- invoice_product_lots
		$data['lotitem_list'] = $this->invoice_model->get_invoice_product_lots($invoice['id']);

		//=== VAT
		$data['vat'] = $this->common_model->get_vat_config();

		//--- Messages
		$data['message'] = Common::html_alert('alert-info',$this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger',$this->session->flashdata('errorMessage'));

		if ($invoice['invoice_group'] != 'S') {
			//=== Report Invoice
			$data['tax_invoice_url'] = base_url('report/iv_report/' . urlencode(base64_encode($invoice['id'])));
			$data['delivery_order_url'] = base_url('report/iv_delivery_report/' . urlencode(base64_encode($invoice['id'])));

			//*** */
			$data['tax_invoice_os_url'] = base_url('report/iv_report_os/' . urlencode(base64_encode($invoice['id'])));
			$data['do_os_url'] = base_url('report/iv_delivery_report_os/' . urlencode(base64_encode($invoice['id'])));
		}

		//=== SUBMIT URL
		$data['submit_url'] = base_url('invoice/edit/' . urlencode(base64_encode($invoice['id'])));
		

		//=== TEMPLATE
		$save_button = Common::html_button('btn_create', '<i class="fas fa-save"></i> Save', 'btn-primary-pz text-light', '', 'onclickSave()');
		$discard_button = Common::html_button('btn_discard', 'Cancel', 'btn-default ml-1', base_url('invoice'), '');
		$data['action_buttons'] = $save_button . $discard_button;	
		$data['title'] = 'Edit Invoice';
		$data['user'] = $user;
		$data['menu_active_4'] = TRUE;
		$data['mode'] = 'edit';
		$this->template->stylesheet->add(base_url('assets/css/sale/edit_invoice.css'));
		$this->template->stylesheet->add(base_url('assets/css/sale/invoice_form.css'));
		$this->template->javascript->add(base_url('assets/js/sale/edit_invoice.js'));
		$this->template->stylesheet->add(base_url('assets/css/sale/customer_picker.css'));
		$this->template->javascript->add(base_url('assets/js/sale/customer_picker.js'));
		$this->template->javascript->add(base_url('assets/js/sale/order_product_picker.js'));
		$this->template->javascript->add(base_url('assets/js/sale/invoice_form.js'));
		$this->template->javascript->add(base_url('assets/js/sale/sale.js'));
        $this->template->content->view('invoice_form', $data);
        $this->template->publish();
	}

	public function create()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		//=== END AUTHEN

		//=== GET USER LOGIN
		$user = $this->ion_auth->user()->row();

		//=== VALIDATION
		$this->set_invoice_validations();

		//=== SUBMIT
		if ($this->input->post()) {
			$insert_data = $this->get_post_invoice_data();

			$insert_dataitems = $this->input->post('insert_dataitems[]');
			$insert_product_arr = $this->convert_dataitems_to_insert_array($insert_data['id'], $insert_dataitems, $user->id);

			$data['insert_dataitems'] = $insert_dataitems;

			//Lotitems
			$lotitems = $this->input->post('lotitems[]');
			$lotitem_arr = $this->convert_lotitems_to_array($lotitems);

			$data['lotitems'] = $lotitems;

			if ($this->form_validation->run() === TRUE) {
				if (!empty($insert_data['date'])) {
					$insert_data['date'] = Common::convert_date_display_to_sql($insert_data['date']);
				}
				
				//Validate invoice_id
				if ($this->invoice_model->has_invoice($insert_data['id'])) {
					$this->session->set_flashdata('errorMessage', 'Invoice ID already in use.');
				
				} else {
					//if ($this->invoice_model->validate_balances($insert_data['customer_id'], $insert_product_arr, $lotitem_arr)) {
						$afffected_row = $this->invoice_model->insert_invoice($insert_data, $insert_product_arr, $lotitem_arr, $user->id);
						redirect(base_url('invoice'), 'refresh');
						/*
						if ($afffected_row > 0) {
							
						} else {
							$this->session->set_flashdata('errorMessage', 'Create fail.');
						}
					} else {
						$this->session->set_flashdata('errorMessage', 'Product unavailable, please check product quantity and balance.');
					}
					*/
				}
			}
		}

		//--- Messages
		$data['message'] = Common::html_alert('alert-info',$this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger',$this->session->flashdata('errorMessage'));

		//--- HTML Inputs
		$data['customer_id'] = Common::html_input('customer_id', 'text', $this->form_validation->set_value('customer_id'), '', '', '', TRUE);
		$data['customer_name'] = Common::html_input('customer_name', 'text', $this->form_validation->set_value('customer_name'), '', '', '', TRUE);
		$data['invoice_id'] = Common::html_input('invoice_id', 'text', $this->form_validation->set_value('invoice_id'));
		$data['id_suffix'] = Common::html_input('id_suffix', 'text', $this->form_validation->set_value('id_suffix'));
		$data['invoice_date'] = Common::html_input('invoice_date', 'text', Common::convert_date_sql_to_display($this->form_validation->set_value('invoice_date')), 'datepicker', '', '', TRUE);
		$data['delivery_address'] = Common::html_textarea('delivery_address', 3, 40, $this->form_validation->set_value('delivery_address'), '', TRUE);
		$data['internal_note'] = $this->form_validation->set_value('internal_note');//Common::html_textarea('internal_note', 3, 40,$this->form_validation->set_value('internal_note'));
		$data['refer_invoice_id'] = Common::html_input('refer_invoice_id', 'text', $this->form_validation->set_value('refer_invoice_id'));

		//--- Radio
		$status_list = $this->common_model->get_statuses();
		if (!empty($status_list) && count($status_list) >= 2) {
			$data['status_label_1'] = $status_list[0]['name'];
			$data['status_label_2'] = $status_list[1]['name'];
			$data['status_value_1'] = array('name' => 'status', 'value' => 1, 'disabled' => TRUE, 'checked' => (empty($this->form_validation->set_value('status')) || $this->form_validation->set_value('status') == $status_list[0]['id']));
			$data['status_value_2'] = array('name' => 'status', 'value' => 2, 'disabled' => TRUE, 'checked' => $this->form_validation->set_value('status') == $status_list[1]['id'], 'class' => 'ml-2');
		}

		// DROPDOWN
		$invoice_list = $this->product_model->get_invoice_types();
		$data['invoice_type'] = $this->form_validation->set_value('invoice_type');
		$data['invoice_type_dropdown_default'] = empty($data['invoice_type']) ? (!empty($invoice_list) ? $invoice_list[0]['id'] : '') : $data['invoice_type'];
		$data['invoice_type_dropdown_data'] = Common::array_to_options($invoice_list, 'id', 'name');

		//--- END HTML Inputs

		$data['invoice_group'] = NULL;

		//=== VAT
		$data['vat'] = $this->common_model->get_vat_config();

		//=== SUBMIT URL
		$data['submit_url'] = base_url('invoice/create');

		//=== TEMPLATE
		$save_button = Common::html_button('btn_create', '<i class="fas fa-save"></i> Save', 'btn-primary-pz text-light', '', 'onclickSave()');
		$discard_button = Common::html_button('btn_discard', 'Cancel', 'btn-default ml-1', base_url('invoice'), '');
		$data['action_buttons'] = $save_button . $discard_button;	
		$data['title'] = 'Create Invoice';
		$data['user'] = $user;
		$data['menu_active_4'] = TRUE;
		$data['mode'] = 'create';
		$this->template->stylesheet->add(base_url('assets/css/sale/create_invoice.css'));
		$this->template->stylesheet->add(base_url('assets/css/sale/invoice_form.css'));
		$this->template->javascript->add(base_url('assets/js/sale/create_invoice.js'));
		$this->template->stylesheet->add(base_url('assets/css/sale/customer_picker.css'));
		$this->template->javascript->add(base_url('assets/js/sale/customer_picker.js'));
		$this->template->javascript->add(base_url('assets/js/sale/order_product_picker.js'));
		$this->template->javascript->add(base_url('assets/js/sale/invoice_form.js'));
		$this->template->javascript->add(base_url('assets/js/sale/sale.js'));
        $this->template->content->view('invoice_form', $data);
        $this->template->publish();
	}

	public function cancel($id)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		//=== END AUTHEN

		//=== GET USER LOGIN
		$user = $this->ion_auth->user()->row();

		//=== SUBMIT
		if ($this->input->post()) {
			$this->invoice_model->cancel_invoice(Common::decodeString($id), $user->id);
		}

		redirect('invoice/edit/' . $id, 'refresh');
	}

	private function get_post_invoice_data()
	{
		return array(
			'id' => $this->input->post('invoice_id'),
			'customer_id' => $this->input->post('customer_id'),
			'date' => $this->input->post('invoice_date'),
			'delivery_address' => $this->input->post('delivery_address'),
			'internal_note' => $this->input->post('internal_note'),
			'status' => $this->input->post('status'),
			'invoice_type' => $this->input->post('invoice_type'),
			'id_suffix' => $this->input->post('id_suffix'),
			'refer_invoice_id' => $this->input->post('refer_invoice_id'),
		);
	}

	private function set_invoice_validations()
	{
		//--- Product Detail
		$this->form_validation->set_rules('customer_id', $this->lang->line('customer_id'), 'required');
		$this->form_validation->set_rules('customer_name', $this->lang->line('customer_name'), 'min_length[0]');
		$this->form_validation->set_rules('id_suffix', $this->lang->line('id_suffix'), 'required');
		$this->form_validation->set_rules('invoice_type', $this->lang->line('invoice_type'), 'required');
		$this->form_validation->set_rules('invoice_date', $this->lang->line('invoice_date'), 'required|min_length[0]');
		$this->form_validation->set_rules('delivery_address', $this->lang->line('delivery_address'), 'required|min_length[0]');
		$this->form_validation->set_rules('internal_note', $this->lang->line('internal_note'), 'min_length[0]');
		$this->form_validation->set_rules('refer_invocie_id', $this->lang->line('refer_invocie_id'), 'min_length[0]');
	}

	//-- Dataitem: id|product_id|product_seq|product_name|invoice_quantity|lot_no|lot_size|unit_price|quantity|order_id|balance|temp_quantity|dataitem_type
	private function convert_dataitems_to_update_array($dataitems) {
		$delimiter = '&88&';
		$arr = array();

		if (!empty($dataitems)) {
			for ($i = 0; $i < count($dataitems); $i++) {
				$values = explode($delimiter, $dataitems[$i]);
				$arr[$i]['id'] = $values[0];
				$arr[$i]['lot_no'] = $values[5];
				$arr[$i]['product_seq'] = Common::decodeString($values[2]);
				$arr[$i]['order_id'] = Common::decodeString($values[9]);
				$arr[$i]['temp_quantity'] = $values[11];
				//$arr[$i]['unit_price'] = $values[7];
			}
		}

		return $arr;
	}

	private function convert_dataitems_to_insert_array($invoice_id, $dataitems) {
		$delimiter = '&88&';
		$arr = array();

		if (!empty($dataitems)) {
			for ($i = 0; $i < count($dataitems); $i++) {
				$values = explode($delimiter, $dataitems[$i]);
				$arr[$i]['id'] = $values[0];
				$arr[$i]['invoice_id'] = $invoice_id;
				$arr[$i]['product_seq'] = Common::decodeString($values[2]);
				$arr[$i]['order_id'] = Common::decodeString($values[9]);
				$arr[$i]['lot_no'] = $values[5];
				$arr[$i]['temp_quantity'] = $values[11];
				//$arr[$i]['unit_price'] = $values[7];
			}
		}

		return $arr;
	}

	private function convert_dataitems_to_delete_array($dataitems) {
		$delimiter = '&88&';
		$arr = array();

		if (!empty($dataitems)) {
			for ($i = 0; $i < count($dataitems); $i++) {
				$values = explode($delimiter, $dataitems[$i]);
				$arr[$i]['id'] = $values[0];
			}
		}

		return $arr;
	}

	// ivp_id|product_id|order_id|lot_size|lot_num|quantity
	private function convert_lotitems_to_array($lotitems) {
		$delimiter = '&88&';
		$arr = array();

		if (!empty($lotitems)) {
			for ($i = 0; $i < count($lotitems); $i++) {
				$values = explode($delimiter, $lotitems[$i]);
				$arr[$i]['invoice_products_id'] = $values[0];
				$arr[$i]['lot_size'] = $values[3];
				$arr[$i]['quantity'] = $values[5];
			}
		}

		return $arr;
	}

	public function get_invoice_list_ajax()
	{
		$resp = array();
		$success = FALSE;

		$startDate = $this->input->post('startDate');
                $dateTimeEnd = $this->input->post('dateTimeEnd');
                $invoice_id = $this->input->post('invoice_id');
                $customer_name = $this->input->post('customer_name');

                if ($this->ion_auth->logged_in()) {
                    $resp['data'] = $this->invoice_model->get_joined_invoices('',$startDate, $dateTimeEnd, $invoice_id, $customer_name);
                }

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

	public function get_order_product_list_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$resp['data'] = $this->invoice_model->get_order_products($this->input->get('customer_id'), FALSE, $this->input->get('invoice_type'), $this->input->get('invoice_group'), $this->input->get('order_id'), $this->input->get('product_id'), $this->input->get('product_name'));
			$success = true;
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

	//*** */
	public function test()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$invoice_id = Common::decodeString($this->input->get('invoice_id'));
			$resp['data'] = $this->invoice_model->get_invoice_products($invoice_id);
			$success = true;
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

	public function has_data_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$invoice_id = Common::decodeString($this->input->get('invoice_id'));
			$resp['has_data'] = $this->invoice_model->has_invoice($invoice_id);
			$success = TRUE;
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}

	public function export_excel() {
		$invoice_id_arr = Common::convert_value_to_array($this->input->post('invoice_ids'));
        $data_arr = $this->invoice_model->get_invoice_products($invoice_id_arr);

        $objPHPExcel = new PHPExcel();

        //Set cell width
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension("B")->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension("D")->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension("E")->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension("F")->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension("G")->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension("H")->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension("I")->setWidth(20);

        //-- Header Style
        $objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A2:I2")->getFont()->setBold(true);

        //-- Header
        $header = "INVOICES";

        $objPHPExcel->getActiveSheet()->mergeCells("A1:I1");
        $objPHPExcel->getActiveSheet()
                ->setCellValue("A1", $header);

        //-- Table Header Style
		$header_columns = "A2:I2";
       	$objPHPExcel->getActiveSheet()->getStyle($header_columns)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle($header_columns)->getFont()->setBold(true);

		//-- Table Header
		$row = 2;
        $objPHPExcel->getActiveSheet()
                ->setCellValue("A" . $row, "Invoice Date")
				->setCellValue("B" . $row, "Customer ID")
				->setCellValue("C" . $row, "Customer Name")
				->setCellValue("D" . $row, "Invoice ID")
				->setCellValue("E" . $row, "Order ID")
                ->setCellValue("F" . $row, "Product ID/Part No.")
				->setCellValue("G" . $row, "Quantity")
				->setCellValue("H" . $row, "Unit Price")
                ->setCellValue("I" . $row, "Amount");

		//-- Table Body
		if (!empty($data_arr)) {
			$total = 0;
			$row = 3;

			$sum_include_vat = 0;
			$sum_invoice_amount = 0;
			$prev_invoice = '';
			
			$amount_include_vat = 0;
			$index = 0;
			
			foreach ($data_arr as $value) {
				//Column Style
				$objPHPExcel->getActiveSheet()->getStyle("A" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("B" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("C" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("D" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("E" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("F" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objPHPExcel->getActiveSheet()->getStyle("G" . $row)->getNumberFormat()->setFormatCode("#,##0");
				$objPHPExcel->getActiveSheet()->getStyle("H" . $row)->getNumberFormat()->setFormatCode("#,##0.0000"); 
				$objPHPExcel->getActiveSheet()->getStyle("I" . $row)->getNumberFormat()->setFormatCode("#,##0.00");

				if ($index > 0 && $value["invoice_id"] != $prev_invoice) {
					$amount_include_vat = $sum_invoice_amount + round(($sum_invoice_amount * 0.07), 2);
					$sum_include_vat += $amount_include_vat;
					$sum_invoice_amount = 0;
				} else {
					$amount_include_vat = '';
				}

				$amount =  round(floatval($value["invoice_quantity"]) * floatval($value["unit_price"]), 2);
				$total += floatval($amount);
				$sum_invoice_amount += $amount;

				if ($index == count($data_arr) - 1) {
					$amount_include_vat = $sum_invoice_amount + round(($sum_invoice_amount * 0.07), 2);
					$sum_include_vat += $amount_include_vat;
					$sum_invoice_amount = 0;
				}

				$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue("A" . $row, Common::convert_date_sql_to_display($value["invoice_date"], '-'))
						->setCellValue("B" . $row, $value["customer_id"])
						->setCellValue("C" . $row, $value["customer_name"])
						->setCellValue("D" . $row, $value["invoice_id"])
						->setCellValue("E" . $row, $value["order_id"])
						->setCellValue("F" . $row, $value["product_id"])
						->setCellValue("G" . $row, $value["invoice_quantity"])
						->setCellValue("H" . $row, $value["unit_price"])
						->setCellValue("I" . $row, $amount);
				
				//Cell Style
				$objPHPExcel->getActiveSheet()->getStyle("A" . $row . ":" . "I" . $row)->getAlignment()->setWrapText(true);
				$objPHPExcel->getActiveSheet()->getStyle("A" . $row . ":" . "I" . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

				$prev_invoice = $value["invoice_id"];
				
				$index++;
				$row++;
			}

			//=== VAT
			//$vat = floatval($this->common_model->get_vat_config());
			$vat_total = $sum_include_vat - $total;
			$grand_total = $sum_include_vat;
			$export_date = Common::get_current_date();

			//-- Summary
			$format = "#,##0.00";

			$row++;
			$objPHPExcel->getActiveSheet()->getStyle("H" . $row)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("I" . $row)->getNumberFormat()->setFormatCode($format); 
			$objPHPExcel->getActiveSheet()->setCellValue("H" . $row, "Total");
			$objPHPExcel->getActiveSheet()->setCellValue("I" . $row, $total);

			$row++;
			$objPHPExcel->getActiveSheet()->getStyle("H" . $row)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("I" . $row)->getNumberFormat()->setFormatCode($format); 
			$objPHPExcel->getActiveSheet()->setCellValue("H" . $row, "Vat 7%");
			$objPHPExcel->getActiveSheet()->setCellValue("I" . $row, $vat_total);

			$row++;
			$objPHPExcel->getActiveSheet()->getStyle("H" . $row)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("I" . $row)->getNumberFormat()->setFormatCode($format); 
			$objPHPExcel->getActiveSheet()->setCellValue("H" . $row, "Grand Total");
			$objPHPExcel->getActiveSheet()->setCellValue("I" . $row, $grand_total);

			//-- Export Date
			$row += 2;
			$objPHPExcel->getActiveSheet()->getStyle("H" . $row)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->mergeCells("H" . $row . ":I" . $row);

			
			$objPHPExcel->getActiveSheet()->getStyle("H" . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->setCellValue("H" . $row, $export_date);
		}

        ob_end_clean();

        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=" . Common::generate_report_name('INVOICES', 'xlsx'));
        header("Cache-Control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objWriter->save("php://output");
	}
}
