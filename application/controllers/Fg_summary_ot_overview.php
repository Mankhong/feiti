<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'libraries/Excel.php';
require_once('Common.php');

class Fg_summary_ot_overview extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('fg_summary_ot_overview_model'));
	}

	public function index()
	{
		//== Authen
		$user = $this->ion_auth->user()->row();
		$login_data = Common::authen_personal($this, $user);
		//==

		//--- Messages
		$data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
		$data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

		//=== INPUTS
		$data['department'] = '';
		$data['department_dropdown_default'] = '';
		$data['department_dropdown_data'] = Common::array_to_options($this->common_model->get_department(), 'id', 'name', '-- All --');

		//=== TEMPLATE
		$data['title'] = 'Summary O-T Overview';
		$data['hide_sub_topbar'] = TRUE;
		$data['display_small_topbar'] = TRUE;
		$data['display_action_search'] = FALSE;
		$data['user'] = $user;
		$data['menu_active_304'] = TRUE;
		$this->template->stylesheet->add(base_url('assets/css/fg/fg.css'));
		$this->template->stylesheet->add(base_url('assets/css/fg/fg_summary_ot_overview.css'));
		$this->template->javascript->add(base_url('assets/js/fg/fg_summary_ot_overview.js'));
		$this->template->content->view('fg_summary_ot_overview', $data);
		$this->template->publish();
	}

	public function get_summary_ajax()
	{
		$resp = array();
		$success = FALSE;

		if ($this->ion_auth->logged_in()) {
			$year = Common::decodeString($this->input->get('year'));
			$department = Common::decodeString($this->input->get('department'));
			
			$resp['data'] = $this->fg_summary_ot_overview_model->get_summary_data($year, $department);
			$success = TRUE;
		}

		$resp['success'] = $success;
			return $this->output
						->set_content_type('application/json')
						->set_output(json_encode($resp));
	}
	
}
