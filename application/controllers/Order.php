<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'libraries/Excel.php';
require_once('Common.php');

class Order extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('order_model', 'customer_model', 'product_model'));
    }

    public function index() {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }
        //=== END AUTHEN
        //=== GET USER LOGIN
        $user = $this->ion_auth->user()->row();

        //-- Export Excel
        $data['export_excel'] = Common::html_export_component('export_excel_form', 'exportExcel', 'order_ids', base_url('order/export_excel'));

        //--- Messages
        $data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
        $data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

        //=== TEMPLATE
        $create_button = Common::html_button('btn_create', '<i class="fas fa-plus-circle fa-sm"></i> Create', 'btn-primary-pz text-light', 'order/create', '');
        $data['action_buttons'] = $create_button;
        $data['title'] = 'Order';
        //$data['display_action_search'] = TRUE;
        $data['user'] = $user;
        $data['menu_active_3'] = TRUE;
        $this->template->stylesheet->add(base_url('assets/css/sale/order.css'));
        $this->template->javascript->add(base_url('assets/js/sale/order.js'));
        $this->template->stylesheet->add(base_url('assets/css/sale/dataitems.css'));
        $this->template->javascript->add(base_url('assets/js/sale/dataitems.js'));
        $this->template->content->view('order', $data);
        $this->template->publish();
    }

    public function edit($id) {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }
        //=== END AUTHEN
        //=== GET USER LOGIN
        $user = $this->ion_auth->user()->row();

        //=== GET DATA
        $id = Common::decodeString($id);

        $odata = $this->order_model->get_orders($id);
        if (empty($odata)) {
            $this->session->set_flashdata('errorMessage', 'Order not found.');
        } else {
            $order = $odata[0];

            //--- Cancel Order
            $data['render_cancel_order_button'] = !$this->order_model->has_invoice_order($order['id']);

            $cdata = $this->customer_model->get_customers($order['customer_id']);
            $data['temp_order_id'] = $order['id'];
            $data['temp_invoice_type'] = $order['invoice_type'];

            if (empty($cdata)) {
                $this->session->set_flashdata('errorMessage', 'Customer not found.');
            } else {
                $customer = $cdata[0];

                //=== VALIDATION
                $this->set_order_validations();

                //=== SUBMIT
                if ($this->input->post()) {
                    $new_dataitems = $this->input->post('new_dataitems[]');
                    $delete_dataitems = $this->input->post('delete_dataitems[]');
                    $edit_dataitems = $this->input->post('edit_dataitems[]');
                    $new_product_items = $this->convert_dataitems_to_array($new_dataitems, $this->input->post('order_type'));
                    $delete_product_items = $this->convert_dataitems_to_array($delete_dataitems, $this->input->post('order_type'));
                    $edit_product_items = $this->convert_dataitems_to_array($edit_dataitems, $this->input->post('order_type'));
                    $data['new_dataitems'] = $new_dataitems;
                    $data['delete_dataitems'] = $delete_dataitems;
                    $data['edit_dataitems'] = $edit_dataitems;

                    if ($this->form_validation->run() === TRUE) {
                        $update_data = $this->get_post_order_data();

                        //Validate order_id
                        // if ($this->order_model->has_order($data['id'])) {
                        // 	$this->session->set_flashdata('errorMessage', 'Order ID already in use.');
                        // } else {
                        if (!empty($update_data['order_date'])) {
                            $update_data['order_date'] = Common::convert_date_display_to_sql($update_data['order_date']);
                        }

                        if (!empty($update_data['delivery_date'])) {
                            $update_data['delivery_date'] = Common::convert_date_display_to_sql($update_data['delivery_date']);
                        }

                        $afffected_row = $this->order_model->update_order($order['id'], $update_data, $new_product_items, $delete_product_items, $edit_product_items, $user->id);
                        redirect(base_url('order'), 'refresh');
                        //}
                    }
                }
            }
        }

        //=== HTML Inputs
        $data['customer_id'] = Common::html_input('customer_id', 'text', $customer['id'], '', '', '', TRUE);
        $data['customer_name'] = Common::html_input('customer_name', 'text', $customer['name'], '', '', '', TRUE);
        $data['order_id'] = Common::html_input('order_id', 'text', $order['id'], '', '', '', TRUE);
        $data['order_date'] = Common::html_input('order_date', 'text', Common::convert_date_sql_to_display($order['order_date']), 'datepicker', '', '', TRUE);
        $data['delivery_date'] = Common::html_input('delivery_date', 'text', Common::convert_date_sql_to_display($order['delivery_date']), 'datepicker', '', '', TRUE);
        $data['credit'] = Common::html_input('credit', 'number', $order['credit'], '', '', '0.00');

        $data['internal_note'] = Common::html_textarea('internal_note', 2, 40, $order['internal_note']);

        //--- Radio
        $status_list = $this->common_model->get_statuses();
        if (!empty($status_list) && count($status_list) >= 2) {
            $data['status_label_1'] = $status_list[0]['name'];
            $data['status_label_2'] = $status_list[1]['name'];
            $data['status_value_1'] = array('name' => 'status', 'value' => 1, 'checked' => $order['status'] == $status_list[0]['id']);
            $data['status_value_2'] = array('name' => 'status', 'value' => 2, 'checked' => $order['status'] == $status_list[1]['id'], 'class' => 'ml-2');
        }

        //-- RADIO
        $order_type_list = $this->order_model->get_order_types();
        if (!empty($order_type_list) && count($order_type_list) >= 2) {
            $data['order_type_label_1'] = $order_type_list[0]['name'];
            $data['order_type_label_2'] = $order_type_list[1]['name'];
            $data['order_type_value_1'] = array('name' => 'order_type', 'value' => 1, 'checked' => (empty($customer['customer_type']) || ($customer['customer_type'] == $order_type_list[0]['id'])));
            $data['order_type_value_2'] = array('name' => 'order_type', 'value' => 2, 'checked' => $customer['customer_type'] == $order_type_list[1]['id'], 'class' => 'ml-2');
        }

        $invoice_group_list = $this->order_model->get_invoice_groups();
        if (!empty($invoice_group_list) && count($invoice_group_list) >= 2) {
            $data['invoice_group_label_1'] = $invoice_group_list[0]['name'];
            $data['invoice_group_label_2'] = $invoice_group_list[1]['name'];
            $data['invoice_group_value_1'] = array('name' => 'invoice_group', 'value' => $invoice_group_list[0]['value'], 'checked' => (empty($order['invoice_group']) || ($order['invoice_group'] == $invoice_group_list[0]['value'])));
            $data['invoice_group_value_2'] = array('name' => 'invoice_group', 'value' => $invoice_group_list[1]['value'], 'checked' => $order['invoice_group'] == $invoice_group_list[1]['value'], 'class' => 'ml-2');
        }

        // DROPDOWN
        $invoice_list = $this->product_model->get_invoice_types('', 'S');
        $data['invoice_type'] = $order['invoice_type'];
        $data['invoice_type_dropdown_default'] = empty($data['invoice_type']) ? (!empty($invoice_list) ? $invoice_list[0]['id'] : '') : $data['invoice_type'];
        $data['invoice_type_dropdown_data'] = Common::array_to_options($invoice_list, 'id', 'name');

        //-- PRODUCT LIST
        $data['product_list'] = $this->order_model->get_order_products_by_order_id($order['id']);
        //=== END HTML Inputs
        //=== Customer Type
        $data['customer_type'] = $customer['customer_type'];

        //=== VAT
        $data['vat'] = $this->common_model->get_vat_config();

        //--- Messages
        $data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
        $data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

        //=== SUBMIT URL
        $data['submit_url'] = base_url('order/edit/' . Common::encodeString($order['id']));

        //=== DUPLICATE URL
        $data['duplicate_url'] = base_url('order/copy/' . Common::encodeString($order['id']));

        //=== DISABLE
        $data['customer_disabled'] = TRUE;

        //=== Display Delivery Detail
        $data['display_delivery_detail'] = TRUE;

        //=== TEMPLATE
        $save_button = Common::html_button('btn_create', '<i class="fas fa-save"></i> Save', 'btn-primary-pz text-light', '', 'onclickSave()');
        $discard_button = Common::html_button('btn_discard', 'Cancel', 'btn-default ml-1', base_url('order'), '');
        $data['action_buttons'] = $save_button . $discard_button;
        $data['title'] = 'Edit Order';
        $data['user'] = $user;
        $data['menu_active_3'] = TRUE;
        $this->template->stylesheet->add(base_url('assets/css/sale/order_form.css'));
        $this->template->javascript->add(base_url('assets/js/sale/order_form.js'));
        $this->template->stylesheet->add(base_url('assets/css/sale/customer_picker.css'));
        $this->template->javascript->add(base_url('assets/js/sale/customer_picker.js'));
        $this->template->javascript->add(base_url('assets/js/sale/product_picker.js'));
        $this->template->content->view('order_form', $data);
        $this->template->publish();
    }

    public function copy($id) {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }
        //=== END AUTHEN
        //=== GET USER LOGIN
        $user = $this->ion_auth->user()->row();

        //=== GET DATA
        $id = Common::decodeString($id);
        $order = $this->order_model->get_orders($id)[0];
        $customer = $this->customer_model->get_customers($order['customer_id'])[0];

        //=== VALIDATION
        $this->set_order_validations();

        //=== HTML Inputs
        $data['customer_id'] = Common::html_input('customer_id', 'text', $customer['id'], '', '', '', TRUE);
        $data['customer_name'] = Common::html_input('customer_name', 'text', $customer['name'], '', '', '', TRUE);
        $data['order_id'] = Common::html_input('order_id', 'text', '');
        $data['order_date'] = Common::html_input('order_date', 'text', Common::convert_date_sql_to_display($order['order_date']), 'datepicker', '', '', TRUE);
        $data['delivery_date'] = Common::html_input('delivery_date', 'text', Common::convert_date_sql_to_display($order['delivery_date']), 'datepicker', '', '', TRUE);
        $data['credit'] = Common::html_input('credit', 'number', $order['credit'], '', '', '0.00');

        $data['internal_note'] = Common::html_textarea('internal_note', 2, 40, $order['internal_note']);

        //--- RADIO
        $status_list = $this->common_model->get_statuses();
        if (!empty($status_list) && count($status_list) >= 2) {
            $data['status_label_1'] = $status_list[0]['name'];
            $data['status_label_2'] = $status_list[1]['name'];
            $data['status_value_1'] = array('name' => 'status', 'value' => 1, 'disabled' => TRUE, 'checked' => TRUE);
            $data['status_value_2'] = array('name' => 'status', 'value' => 2, 'disabled' => TRUE);
        }

        $order_type_list = $this->order_model->get_order_types();
        if (!empty($order_type_list) && count($order_type_list) >= 2) {
            $data['order_type_label_1'] = $order_type_list[0]['name'];
            $data['order_type_label_2'] = $order_type_list[1]['name'];
            $data['order_type_value_1'] = array('name' => 'order_type', 'value' => 1, 'checked' => (empty($customer['customer_type']) || ($customer['customer_type'] == $order_type_list[0]['id'])));
            $data['order_type_value_2'] = array('name' => 'order_type', 'value' => 2, 'checked' => $customer['customer_type'] == $order_type_list[1]['id'], 'class' => 'ml-2');
        }

        $invoice_group_list = $this->order_model->get_invoice_groups();
        if (!empty($invoice_group_list) && count($invoice_group_list) >= 2) {
            $data['invoice_group_label_1'] = $invoice_group_list[0]['name'];
            $data['invoice_group_label_2'] = $invoice_group_list[1]['name'];
            $data['invoice_group_value_1'] = array('name' => 'invoice_group', 'value' => $invoice_group_list[0]['value'], 'checked' => (empty($order['invoice_group']) || ($order['invoice_group'] == $invoice_group_list[0]['value'])));
            $data['invoice_group_value_2'] = array('name' => 'invoice_group', 'value' => $invoice_group_list[1]['value'], 'checked' => $order['invoice_group'] == $invoice_group_list[1]['value'], 'class' => 'ml-2');
        }

        // DROPDOWN
        $invoice_list = $this->product_model->get_invoice_types('', 'S');
        $data['invoice_type'] = $order['invoice_type'];
        $data['invoice_type_dropdown_default'] = empty($data['invoice_type']) ? (!empty($invoice_list) ? $invoice_list[0]['id'] : '') : $data['invoice_type'];
        $data['invoice_type_dropdown_data'] = Common::array_to_options($invoice_list, 'id', 'name');

        //=== END HTML Inputs
        //-- New Dataitem (PRODUCT LIST)
        $product_list = $this->order_model->copy_order_products_by_order_id($order['id']);
        $new_dataitems = $this->convert_data_list_to_dataitems($product_list, 'NEW');
        $data['new_dataitems'] = $new_dataitems;

        //=== Customer Type
        $data['customer_type'] = $customer['customer_type'];

        //=== VAT
        $data['vat'] = $this->common_model->get_vat_config();

        //--- Messages
        $data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
        $data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

        //=== SUBMIT URL
        $data['submit_url'] = base_url('order/create');

        //=== TEMPLATE
        $save_button = Common::html_button('btn_create', '<i class="fas fa-save"></i> Save', 'btn-primary-pz text-light', '', 'onclickSave()');
        $discard_button = Common::html_button('btn_discard', 'Cancel', 'btn-default ml-1', base_url('order'), '');
        $data['action_buttons'] = $save_button . $discard_button;
        $data['title'] = 'Duplicate Order';
        $data['user'] = $user;
        $data['menu_active_3'] = TRUE;
        $this->template->stylesheet->add(base_url('assets/css/sale/order_form.css'));
        $this->template->javascript->add(base_url('assets/js/sale/order_form.js'));
        $this->template->stylesheet->add(base_url('assets/css/sale/customer_picker.css'));
        $this->template->javascript->add(base_url('assets/js/sale/customer_picker.js'));
        $this->template->javascript->add(base_url('assets/js/sale/product_picker.js'));
        $this->template->content->view('order_form', $data);
        $this->template->publish();
    }

    public function create() {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }
        //=== END AUTHEN
        //=== GET USER LOGIN
        $user = $this->ion_auth->user()->row();

        //=== VALIDATION
        $this->set_order_validations();

        //=== SUBMIT
        if ($this->input->post()) {
            $new_dataitems = $this->input->post('new_dataitems[]');
            $new_product_items = $this->convert_dataitems_to_array($new_dataitems, $this->input->post('order_type'));
            $data['new_dataitems'] = $new_dataitems;

            if ($this->form_validation->run() === TRUE) {
                $insert_data = $this->get_post_order_data();

                //Validate order_id
                if ($this->order_model->has_order($insert_data['id'], $insert_data['invoice_type'])) {
                    $this->session->set_flashdata('errorMessage', 'Order ID already in use.');
                } else {
                    if (!empty($insert_data['order_date'])) {
                        $insert_data['order_date'] = Common::convert_date_display_to_sql($insert_data['order_date']);
                    }

                    if (!empty($insert_data['delivery_date'])) {
                        $insert_data['delivery_date'] = Common::convert_date_display_to_sql($insert_data['delivery_date']);
                    }

                    $afffected_row = $this->order_model->insert_order($insert_data, $new_product_items, $user->id);
                    if ($afffected_row > 0) {
                        redirect(base_url('order'), 'refresh');
                    } else {
                        $this->session->set_flashdata('errorMessage', 'Create fail.');
                    }
                }
            }
        }

        //--- Messages
        $data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
        $data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

        //=== HTML Inputs
        $data['customer_id'] = Common::html_input('customer_id', 'text', $this->form_validation->set_value('customer_id'), '', '', '', TRUE);
        $data['customer_name'] = Common::html_input('customer_name', 'text', $this->form_validation->set_value('customer_name'), '', '', '', TRUE);
        $data['order_id'] = Common::html_input('order_id', 'text', $this->form_validation->set_value('order_id'));
        $data['order_date'] = Common::html_input('order_date', 'text', $this->form_validation->set_value('order_date'), 'datepicker', '', '', TRUE);
        $data['delivery_date'] = Common::html_input('delivery_date', 'text', $this->form_validation->set_value('delivery_date'), 'datepicker', '', '', TRUE);
        $data['credit'] = Common::html_input('credit', 'number', $this->form_validation->set_value('credit'), '', '', '0.00');
        $data['internal_note'] = Common::html_textarea('internal_note', 2, 40, $this->form_validation->set_value('internal_note'));

        //-- RADIO
        $order_type_list = $this->order_model->get_order_types();
        if (!empty($order_type_list) && count($order_type_list) >= 2) {
            $data['order_type_label_1'] = $order_type_list[0]['name'];
            $data['order_type_label_2'] = $order_type_list[1]['name'];
            $data['order_type_value_1'] = array('name' => 'order_type', 'value' => 1, 'checked' => (empty($this->form_validation->set_value('order_type')) || ($this->form_validation->set_value('order_type') == $order_type_list[0]['id'])));
            $data['order_type_value_2'] = array('name' => 'order_type', 'value' => 2, 'checked' => $this->form_validation->set_value('order_type') == $order_type_list[1]['id'], 'class' => 'ml-2');
        }

        $status_list = $this->common_model->get_statuses();
        if (!empty($status_list) && count($status_list) >= 2) {
            $data['status_label_1'] = $status_list[0]['name'];
            $data['status_label_2'] = $status_list[1]['name'];
            $data['status_value_1'] = array('name' => 'status', 'value' => 1, 'disabled' => TRUE, 'checked' => (empty($this->form_validation->set_value('status')) || $this->form_validation->set_value('status') == $status_list[0]['id']));
            $data['status_value_2'] = array('name' => 'status', 'value' => 2, 'disabled' => TRUE, 'checked' => $this->form_validation->set_value('status') == $status_list[1]['id'], 'class' => 'ml-2');
        }

        $invoice_group_list = $this->order_model->get_invoice_groups();
        if (!empty($invoice_group_list) && count($invoice_group_list) >= 2) {
            $data['invoice_group_label_1'] = $invoice_group_list[0]['name'];
            $data['invoice_group_label_2'] = $invoice_group_list[1]['name'];
            $data['invoice_group_value_1'] = array('name' => 'invoice_group', 'value' => $invoice_group_list[0]['value'], 'checked' => (empty($this->form_validation->set_value('invoice_group')) || ($this->form_validation->set_value('invoice_group') == $invoice_group_list[0]['value'])));
            $data['invoice_group_value_2'] = array('name' => 'invoice_group', 'value' => $invoice_group_list[1]['value'], 'checked' => $this->form_validation->set_value('invoice_group') == $invoice_group_list[1]['value'], 'class' => 'ml-2');
        }

        // DROPDOWN
        $data['invoice_type'] = $this->form_validation->set_value('invoice_type');
        $data['invoice_type_dropdown_default'] = empty($data['invoice_type']) ? '1' : $data['invoice_type'];
        $data['invoice_type_dropdown_data'] = Common::array_to_options($this->product_model->get_invoice_types('', 'S'), 'id', 'name');

        //=== END HTML Inputs
        //=== Customer Type
        $data['customer_type'] = $this->form_validation->set_value('customer_type');

        //=== VAT
        $data['vat'] = $this->common_model->get_vat_config();

        //=== SUBMIT URL
        $data['submit_url'] = base_url('order/create');

        //=== TEMPLATE
        $save_button = Common::html_button('btn_create', '<i class="fas fa-save"></i> Save', 'btn-primary-pz text-light', '', 'onclickSave()');
        $discard_button = Common::html_button('btn_discard', 'Cancel', 'btn-default ml-1', base_url('order'), '');
        $data['action_buttons'] = $save_button . $discard_button;
        $data['title'] = 'Create Order';
        $data['user'] = $user;
        $data['menu_active_3'] = TRUE;
        $this->template->stylesheet->add(base_url('assets/css/sale/order_form.css'));
        $this->template->javascript->add(base_url('assets/js/sale/order_form.js'));
        $this->template->stylesheet->add(base_url('assets/css/sale/customer_picker.css'));
        $this->template->javascript->add(base_url('assets/js/sale/customer_picker.js'));
        $this->template->javascript->add(base_url('assets/js/sale/product_picker.js'));
        $this->template->content->view('order_form', $data);
        $this->template->publish();
    }

    //product_seq|product_id|product_desc|quantity|unit_price|amount|balance|invoice_quantity|dataitem_type|price_flag|invoice_type_name|invoice_type_phase
    private function convert_dataitems_to_array($dataitems, $order_type) {
        $delimiter = '&88&';
        $arr = array();

        if (!empty($dataitems)) {
            for ($i = 0; $i < count($dataitems); $i++) {
                $values = explode($delimiter, $dataitems[$i]);
                $arr[$i]['product_seq'] = Common::decodeString($values[0]);
                $arr[$i]['quantity'] = $values[3];
                $arr[$i]['unit_price'] = $values[4];
                $arr[$i]['price_flag'] = $values[9];
                $arr[$i]['invoice_type_name'] = $values[10];
                $arr[$i]['invoice_type_phase'] = $values[11];
            }
        }

        return $arr;
    }

    //product_seq|product_id|product_desc|quantity|unit_price|amount|balance|invoice_quantity|dataitem_type|price_flag|invoice_type_name|invoice_type_phase
    private function convert_data_list_to_dataitems($data_list, $dataitem_type) {
        $delimiter = '&88&';
        $arr = array();
        if (!empty($data_list)) {
            for ($i = 0; $i < count($data_list); $i++) {
                $value = Common::encodeString($data_list[$i]['product_seq'])
                        . $delimiter . Common::encodeString($data_list[$i]['product_id'])
                        . $delimiter . $data_list[$i]['desc1']
                        . $delimiter . $data_list[$i]['quantity']
                        . $delimiter . $data_list[$i]['unit_price']
                        . $delimiter . 0 //amount
                        . $delimiter . 0 //balance
                        . $delimiter . $data_list[$i]['invoice_quantity']
                        . $delimiter . $dataitem_type
                        . $delimiter . $data_list[$i]['price_flag']
                        . $delimiter . $data_list[$i]['invoice_type_name']
                        . $delimiter . $data_list[$i]['invoice_type_phase'];
                $arr[$i] = $value;
            }
        }

        return $arr;
    }

    public function cancel($id) {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }
        //=== END AUTHEN
        //=== GET USER LOGIN
        $user = $this->ion_auth->user()->row();

        //=== SUBMIT
        if ($this->input->post()) {
            $this->order_model->delete_order(Common::decodeString($id), $user->id);
        }

        redirect('order', 'refresh');
    }

    private function get_post_order_data() {
        $result = array(
            'customer_id' => $this->input->post('customer_id'),
            'id' => $this->input->post('order_id'),
            'order_date' => $this->input->post('order_date'),
            'delivery_date' => $this->input->post('delivery_date'),
            'credit' => $this->input->post('credit'),
            'internal_note' => $this->input->post('internal_note'),
            'status' => $this->input->post('status'),
        );

        $invoice_group = $this->input->post('invoice_group');
        if ($invoice_group == 'S') {
            $result['invoice_type'] = $this->input->post('invoice_type');
        } else {
            $result['invoice_type'] = NULL;
        }

        return $result;
    }

    private function set_order_validations() {
        //--- Order Detail
        $this->form_validation->set_rules('customer_id', $this->lang->line('customer_id'), 'required');
        $this->form_validation->set_rules('customer_type', $this->lang->line('customer_id'), 'min_length[0]');
        $this->form_validation->set_rules('customer_name', $this->lang->line('customer_name'), 'min_length[0]');
        $this->form_validation->set_rules('order_id', $this->lang->line('order_id'), 'required');
        $this->form_validation->set_rules('order_date', $this->lang->line('order_date'), 'required|min_length[0]');
        $this->form_validation->set_rules('delivery_date', $this->lang->line('delivery_date'), 'min_length[0]');
        $this->form_validation->set_rules('credit', $this->lang->line('credit'), 'numeric');
        $this->form_validation->set_rules('internal_note', $this->lang->line('internal_note'), 'min_length[0]');
    }

    public function get_order_list_ajax() {
        $resp = array();
        $success = FALSE;

        $startDate = $this->input->post('startDate');
        $dateTimeEnd = $this->input->post('dateTimeEnd');
        $order_id = $this->input->post('order_id');
        $customer_name = $this->input->post('customer_name');

        if ($this->ion_auth->logged_in()) {
            $resp['data'] = $this->order_model->get_joined_orders('', $order_id, $customer_name, $startDate, $dateTimeEnd);
        }

        $resp['success'] = $success;
        return $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode($resp));
    }

    public function get_picker_products_ajax() {
        $resp = array();
        $success = FALSE;

        if ($this->ion_auth->logged_in()) {
            $resp['data'] = $this->order_model->get_picker_products($this->input->get('customer_id'));
        }

        $resp['success'] = $success;
        return $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode($resp));
    }

    public function get_delivery_products_ajax() {
        $resp = array();
        $success = FALSE;

        if ($this->ion_auth->logged_in()) {
            $customer_id = Common::decodeString($this->input->get('customer_id'));
            $order_id = Common::decodeString($this->input->get('order_id'));
            $resp['data'] = $this->order_model->get_delivery_products($customer_id, $order_id);
        }

        $resp['success'] = $success;
        return $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode($resp));
    }

    public function has_data_ajax() {
        $resp = array();
        $success = FALSE;

        if ($this->ion_auth->logged_in()) {
            $order_id = Common::decodeString($this->input->get('order_id'));
            $invoice_type = Common::decodeString($this->input->get('invoice_type'));

            $resp['has_data'] = $this->order_model->has_order($order_id, $invoice_type);
            $success = TRUE;
        }

        $resp['success'] = $success;
        return $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode($resp));
    }

    public function export_excel() {
        $order_id_arr = $this->convert_value_to_array($this->input->post('order_ids'));
        $data_arr = $this->order_model->get_order_products_by_order_id($order_id_arr);

        $objPHPExcel = new PHPExcel();

        //Set cell width
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension("B")->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension("C")->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension("D")->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension("E")->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension("F")->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension("G")->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension("H")->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension("I")->setWidth(20);

        //-- Header Style
        $objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A2:I2")->getFont()->setBold(true);

        //-- Header
        $header = "ORDERS";

        $objPHPExcel->getActiveSheet()->mergeCells("A1:I1");
        $objPHPExcel->getActiveSheet()
                ->setCellValue("A1", $header);

        //-- Table Header Style
        $header_color = "FFFFFF";
        $header_columns = "A2:H2";
        // $objPHPExcel->getActiveSheet()->getStyle($header_columns)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($header_color);
        $objPHPExcel->getActiveSheet()->getStyle($header_columns)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objPHPExcel->getActiveSheet()->getStyle($header_columns)->getFont()->setBold(true);

        //-- Table Header
        $row = 2;
        $objPHPExcel->getActiveSheet()
                ->setCellValue("A" . $row, "Delivery Date")
                ->setCellValue("B" . $row, "Customer ID")
                ->setCellValue("C" . $row, "Customer Name")
                ->setCellValue("D" . $row, "Order ID")
                ->setCellValue("E" . $row, "Product ID/Part No.")
                ->setCellValue("F" . $row, "Quantity")
                ->setCellValue("G" . $row, "Unit Price")
                ->setCellValue("H" . $row, "Amount")
                ->setCellValue("I" . $row, "Balance");

        //-- Table Body
        if (!empty($data_arr)) {
            $total = 0;
            $row = 3;
            foreach ($data_arr as $value) {
                //Column Style
                $objPHPExcel->getActiveSheet()->getStyle("A" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $objPHPExcel->getActiveSheet()->getStyle("B" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $objPHPExcel->getActiveSheet()->getStyle("C" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $objPHPExcel->getActiveSheet()->getStyle("D" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $objPHPExcel->getActiveSheet()->getStyle("E" . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $objPHPExcel->getActiveSheet()->getStyle("F" . $row)->getNumberFormat()->setFormatCode("#,##0");
                $objPHPExcel->getActiveSheet()->getStyle("G" . $row)->getNumberFormat()->setFormatCode("#,##0.0000");
                $objPHPExcel->getActiveSheet()->getStyle("H" . $row)->getNumberFormat()->setFormatCode("#,##0.00");
                $objPHPExcel->getActiveSheet()->getStyle("I" . $row)->getNumberFormat()->setFormatCode("#,##0");

                $amount = $value["amount"];
                $total += floatval($amount);

                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("A" . $row, Common::convert_date_sql_to_display($value["delivery_date"], '-'))
                        ->setCellValue("B" . $row, $value["customer_id"])
                        ->setCellValue("C" . $row, $value["customer_name"])
                        ->setCellValue("D" . $row, $value["order_id"])
                        ->setCellValue("E" . $row, $value["desc1"])
                        ->setCellValue("F" . $row, $value["quantity"])
                        ->setCellValue("G" . $row, $value["unit_price"])
                        ->setCellValue("H" . $row, $amount)
                        ->setCellValue("I" . $row, $value["balance"]);

                //Cell Style
                $objPHPExcel->getActiveSheet()->getStyle("A" . $row . ":" . "I" . $row)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle("A" . $row . ":" . "I" . $row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

                $row++;
            }

            //=== VAT
            $vat = floatval($this->common_model->get_vat_config());
            $vat_total = ($vat / 100) * $total;
            $grand_total = $total + $vat_total;
            $export_date = Common::get_current_date();

            //-- Summary
            $format = "#,##0.00";

            $row++;
            $objPHPExcel->getActiveSheet()->getStyle("H" . $row)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle("I" . $row)->getNumberFormat()->setFormatCode($format);
            $objPHPExcel->getActiveSheet()->setCellValue("H" . $row, "Total");
            $objPHPExcel->getActiveSheet()->setCellValue("I" . $row, $total);

            $row++;
            $objPHPExcel->getActiveSheet()->getStyle("H" . $row)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle("I" . $row)->getNumberFormat()->setFormatCode($format);
            $objPHPExcel->getActiveSheet()->setCellValue("H" . $row, "Vat " . $vat . "%");
            $objPHPExcel->getActiveSheet()->setCellValue("I" . $row, $vat_total);

            $row++;
            $objPHPExcel->getActiveSheet()->getStyle("H" . $row)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle("I" . $row)->getNumberFormat()->setFormatCode($format);
            $objPHPExcel->getActiveSheet()->setCellValue("H" . $row, "Grand Total");
            $objPHPExcel->getActiveSheet()->setCellValue("I" . $row, $grand_total);

            //-- Export Date
            $row += 2;
            $objPHPExcel->getActiveSheet()->getStyle("H" . $row)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->mergeCells("H" . $row . ":I" . $row);


            $objPHPExcel->getActiveSheet()->getStyle("H" . $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->setCellValue("H" . $row, $export_date);
        }

        ob_end_clean();

        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=" . Common::generate_report_name('ORDERS', 'xlsx'));
        header("Cache-Control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objWriter->save("php://output");
    }

    private function convert_value_to_array($value) {
        $delimiter = '&88&';
        return explode($delimiter, $value);
    }

}
