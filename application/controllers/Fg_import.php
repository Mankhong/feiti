<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . 'libraries/Excel.php';
require_once 'Common.php';

class Fg_import extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('fg_time_model'));
    }

    public function index()
    {
        //== Authen
        $user = $this->ion_auth->user()->row();
        $login_data = Common::authen_personal($this, $user);

        if (!$login_data['is_admin']) {
            $this->ion_auth->logout();
            $this->session->set_flashdata('message', 'Please login with personal user.');
            redirect('auth/login', 'refresh');
        }
        //== End Authen

        $this->session->set_flashdata('message', '');
        $this->session->set_flashdata('errorMessage', '');

        if (!empty($_FILES['mdb_file']['name'])) {
            $this->session->set_flashdata('errorMessage', $_FILES['mdb_file']['name']);
            if (Common::get_ext_from_filename($_FILES['mdb_file']['name']) != 'xls') {
                $this->session->set_flashdata('errorMessage', 'Import fail - file name must be "*.xls"');

            } else {
                $path = $_FILES['mdb_file']['tmp_name'];
                $reader = PHPExcel_IOFactory::load($path);
                $object = $reader->load($path);
                $sheet = $object->getSheet(0);
                $highest_row = $sheet->getHighestRow();
                $highest_column = $sheet->getHighestColumn();

                for ($row = 1; $row <= $highest_row; $row++) {
                    echo $sheet->getCell('A' . $row)->getValue() . '|' . $sheet->getCell('B' . $row)->getValue();
                }

            }
        }

        //=== SUBMIT URL
        $data['submit_url'] = base_url('fg_import/');
        $data['cal_submit_url'] = base_url('fg_import/calculate_timesheet');
        $data['init_submit_url'] = base_url('fg_import/init_timesheet');

        //--- Messages
        $data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
        $data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

        //=== TEMPLATE
        $data['title'] = 'Upload';
        $data['display_action_search'] = false;
        $data['user'] = $user;
        $data['menu_active_205'] = true;
        $this->template->stylesheet->add(base_url('assets/css/fg/fg_import.css'));
        $this->template->javascript->add(base_url('assets/js/fg/fg_import.js'));
        $this->template->content->view('fg_import', $data);
        $this->template->publish();
    }

    public function upload_excel()
    {
        $resp = array();
        $success = false;
        $message = '';
        $count_fail = 0;
        $count_success = 0;
        $count_total = 0;
        $count_inserted = 0;

        //== Authen
        $user = $this->ion_auth->user()->row();
        $login_data = Common::authen_personal($this, $user);

        if (!$login_data['is_admin']) {
            $success = false;
            $message = 'Only admin supported.';

        } else {
            $filename = $_FILES['ctrFile']['name'];
            if (!empty($filename)) {
                if (Common::get_ext_from_filename($filename) != 'xls' && Common::get_ext_from_filename($filename) != 'xlsx') {
                    $message = 'Import fail - file name must be "*.xls"';

                } else {
                    $path = $_FILES['ctrFile']['tmp_name'];

                    $inputFileType = PHPExcel_IOFactory::identify($path);
                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($path);
                    $sheet = $objPHPExcel->setActiveSheetIndex(0);
                    $highest_row = $sheet->getHighestRow();
                    $highest_column = $sheet->getHighestColumn();
					$count_total = $highest_row;

                    $index = 0;
                    $records = array();
                    for ($row = 1; $row <= $highest_row; $row++) {
                        $date_cell = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(3, $row);
                        $col_checktime = $this->format_date_cell_to_str($date_cell);
                        $col_badgenumber = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(2, $row)->getValue();
                        $col_sensorid = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(4, $row)->getValue();
                        $col_data_type = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(6, $row);

                        if (!empty($col_badgenumber) && !empty($col_checktime)) {
                            if ($col_data_type != 'Password') {
                                $records[$index] = array(
                                    'userid' => $col_badgenumber,
                                    'badgenumber' => $col_badgenumber,
                                    'checktime' => $col_checktime,
                                    'sensorid' => $col_sensorid,
                                );
                                $index++;
                            }

                            $count_success++;
                        } else {
                            $count_fail++;
						}

                    }

                    //MIGRATE
                    $count_inserted = $this->fg_time_model->migrate_checkinout_by_records($records);

                    $success = true;
                }
            }

        }

        //== End Authen

        $resp['count_total'] = $count_total;
        $resp['count_success'] = $count_success;
        $resp['count_fail'] = $count_fail;
        $resp['count_inserted'] = $count_inserted;
        $resp['success'] = $success;
        $resp['message'] = $message;
        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($resp));
    }

    private function format_date_cell_to_str($date_cell)
    {
        if (PHPExcel_Shared_Date::isDateTime($date_cell)) {
            $format = 'Y-m-d H:i:s';

            //! Fix by using gmdate 26-10-2022
            $str_date = gmdate($format, PHPExcel_Shared_Date::ExcelToPHP($date_cell->getValue()));
            //$str_date = date($format, PHPExcel_Shared_Date::ExcelToPHP($date_cell->getValue()));
            $date = date_create($str_date);

            /*
            //Fix bug 26/10/2019
            $check_date = date_create(date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($date_cell->getValue())));
            $fix_date = DateTime::createFromFormat('Y-m-d', '2019-10-26');
             */
            
            /* Fix by using gmdate 26-10-2022
            //Fix bug -1 -2 hours
            $check_date = date_create(date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($date_cell->getValue())));
            $fix_date1 = DateTime::createFromFormat('Y-m-d', date('Y') . '-03-27'); //day of problem 2021-03-28
            $fix_date2 = DateTime::createFromFormat('Y-m-d', date('Y') . '-10-24');
            $fix_date3 = DateTime::createFromFormat('Y-m-d', date('Y') . '-10-30');
            $fix_date4 = DateTime::createFromFormat('Y-m-d', date('Y') . '-12-25');

            if ($check_date > $fix_date4) {
                $date->modify('-3 hour');
            } else if ($check_date > $fix_date3) {
                $date->modify('-2 hour');
            } else if ($check_date > $fix_date2) {
                $date->modify('-3 hour');
            } else if ($check_date > $fix_date1){
                $date->modify('-2 hour');
            } else {
                $date->modify('-3 hour');
            }
            */

            return $date->format($format);
        }
        return '';
    }

    //use excel instead of mdb
    public function index2()
    {
        //== Authen
        $user = $this->ion_auth->user()->row();
        $login_data = Common::authen_personal($this, $user);

        if (!$login_data['is_admin']) {
            $this->ion_auth->logout();
            $this->session->set_flashdata('message', 'Please login with personal user.');
            redirect('auth/login', 'refresh');
        }
        //== End Authen

        $this->session->set_flashdata('message', '');
        $this->session->set_flashdata('errorMessage', '');

        if (!empty($_FILES['mdb_file']['name'])) {
            if ($_FILES['mdb_file']['name'] != 'att2000.mdb') {
                $this->session->set_flashdata('errorMessage', 'Import fail - file name should be "att2000.mdb"');

            } else {
                $config = array();
                $config['allowed_types'] = '*';
                $config['max_size'] = 102400; //100MB
                $config['upload_path'] = 'C:/mdb/';
                $config['max_input_time'] = 600;

                $this->load->library('upload', $config, 'mdb_upload');
                $this->mdb_upload->initialize($config);
                $this->mdb_upload->overwrite = true;

                if ($this->mdb_upload->do_upload('mdb_file')) {
                    $this->fg_time_model->migrate_checkinout();
                    $this->session->set_flashdata('message', 'Upload success.');
                } else {
                    $this->session->set_flashdata('errorMessage', 'Upload fail ' . $this->mdb_upload->display_errors());
                }
            }
        }

        //=== SUBMIT URL
        $data['submit_url'] = base_url('fg_import/');
        $data['cal_submit_url'] = base_url('fg_import/calculate_timesheet');
        $data['init_submit_url'] = base_url('fg_import/init_timesheet');

        //--- Messages
        $data['message'] = Common::html_alert('alert-info', $this->session->flashdata('message'));
        $data['errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('errorMessage'));

        //=== TEMPLATE
        $data['title'] = 'Upload';
        $data['display_action_search'] = false;
        $data['user'] = $user;
        $data['menu_active_205'] = true;
        $this->template->stylesheet->add(base_url('assets/css/fg/fg_import.css'));
        $this->template->javascript->add(base_url('assets/js/fg/fg_import.js'));
        $this->template->content->view('fg_import', $data);
        $this->template->publish();
    }

    public function calculate_timesheet()
    {
        //== Authen
        $user = $this->ion_auth->user()->row();
        $login_data = Common::authen_personal($this, $user);

        if (!$login_data['is_admin']) {
            $this->ion_auth->logout();
            $this->session->set_flashdata('message', 'Please login with personal user.');
            redirect('auth/login', 'refresh');
        }
        //== End Authen

        $this->session->set_flashdata('cal_message', '');
        $this->session->set_flashdata('cal_errorMessage', '');

        if ($this->input->post()) {
            $year = $this->input->post('ctrYear');
            $month = $this->input->post('ctrMonth');
            $date = $this->input->post('ctrDate');
            $badgenumber = $this->input->post('ctrBadgenumber');

            $config = $this->fg_time_model->generate_checkinout_config();
            $shift_switches = $this->fg_shift_switch_model->get_shift_switches_checking();

            if ($badgenumber) {
                $this->fg_time_model->calculate_timesheet($year, $month, $date, $badgenumber, $config, $shift_switches);
            } else {
                $badgenumber_arr = $this->fg_time_model->get_checkinout_badgenumbers();
                for ($i = 0; $i < count($badgenumber_arr); $i++) {
                    $this->fg_time_model->calculate_timesheet($year, $month, $date, $badgenumber_arr[$i]['badgenumber'], $config, $shift_switches);
                }
            }

            $this->session->set_flashdata('cal_message', 'Calculate Timesheet success.');
        }

        //=== SUBMIT URL
        $data['submit_url'] = base_url('fg_import/');
        $data['cal_submit_url'] = base_url('fg_import/calculate_timesheet');
        $data['init_submit_url'] = base_url('fg_import/init_timesheet');

        //--- Messages
        $data['cal_message'] = Common::html_alert('alert-info', $this->session->flashdata('cal_message'));
        $data['cal_errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('cal_errorMessage'));

        //=== TEMPLATE
        $data['title'] = 'Upload Time';
        $data['display_action_search'] = false;
        $data['user'] = $user;
        $data['menu_active_205'] = true;
        $this->template->stylesheet->add(base_url('assets/css/fg/fg_import.css'));
        $this->template->javascript->add(base_url('assets/js/fg/fg_import.js'));
        $this->template->content->view('fg_import', $data);
        $this->template->publish();
    }

    public function init_timesheet()
    {
        //== Authen
        $user = $this->ion_auth->user()->row();
        $login_data = Common::authen_personal($this, $user);

        if (!$login_data['is_admin']) {
            $this->ion_auth->logout();
            $this->session->set_flashdata('message', 'Please login with personal user.');
            redirect('auth/login', 'refresh');
        }
        //== End Authen

        $this->session->set_flashdata('init_message', '');
        $this->session->set_flashdata('init_errorMessage', '');

        if ($this->input->post()) {
            $year = $this->input->post('initCtrYear');
            $month = $this->input->post('initCtrMonth');
            $date = $this->input->post('initCtrDate');
            $badgenumber = $this->input->post('initCtrBadgenumber');

            $badgenumber_arr = $this->fg_time_model->get_checkinout_badgenumbers();

            if (!empty($badgenumber)) {
                $this->fg_time_model->init_timesheet($year, $month, $date, $badgenumber);
            } else {
                for ($i = 0; $i < count($badgenumber_arr); $i++) {
                    $this->fg_time_model->init_timesheet($year, $month, $date, $badgenumber_arr[$i]['badgenumber']);
                }
            }

            $this->session->set_flashdata('init_message', 'Init Timesheet success.');
        }

        //=== SUBMIT URL
        $data['submit_url'] = base_url('fg_import/');
        $data['cal_submit_url'] = base_url('fg_import/calculate_timesheet');
        $data['init_submit_url'] = base_url('fg_import/init_timesheet');

        //--- Messages
        $data['init_message'] = Common::html_alert('alert-info', $this->session->flashdata('init_message'));
        $data['init_errorMessage'] = Common::html_alert('alert-danger', $this->session->flashdata('init_errorMessage'));

        //=== TEMPLATE
        $data['title'] = 'Upload Time';
        $data['display_action_search'] = false;
        $data['user'] = $user;
        $data['menu_active_205'] = true;
        $this->template->stylesheet->add(base_url('assets/css/fg/fg_import.css'));
        $this->template->javascript->add(base_url('assets/js/fg/fg_import.js'));
        $this->template->content->view('fg_import', $data);
        $this->template->publish();
    }

}
