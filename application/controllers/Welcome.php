<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}

		$user = $this->ion_auth->user()->row();
		
		if ($this->ion_auth->is_admin() || $this->admin_model->is_sale_admin($user->id) || $this->admin_model->is_sale_member($user->id)) {
			redirect('customer', 'refresh');
		} else {
			if ($this->admin_model->is_personal_admin($user->id)) {
				redirect('fg_employee', 'refresh');
			} else {
				redirect('fg_time', 'refresh');
			}
		}
	}
}
