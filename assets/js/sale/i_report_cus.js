$(document).ready(function () {

    console.log("ready i report customer");
    $("#div_excel").hide();
    //    var s = '10/02/2019';
    //    var date = s.substring(0, 2);
    //    var month = s.substring(3, 5);
    //    var year = s.substring(6, 10);


    $("#dateTimeStart").datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        orientation: "bottom auto",
        todayHighlight: true,
        todayBtn: true,
        language: 'EN', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
        thaiyear: false, //Set เป็นปี พ.ศ.
    }).datepicker("setDate");
    $("#dateTimeEnd").datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        orientation: "bottom auto",
        todayHighlight: true,
        todayBtn: true,
        language: 'EN', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
        thaiyear: false, //Set เป็นปี พ.ศ.
    }).datepicker("setDate");
    //loadInitTable();

});


function process(date) {
    var parts = date.split("/");
    return new Date(parts[2], parts[1] - 1, parts[0]);
}

$("#btn_search").click(function () {


    try {

        console.log("### start btn search ###");
        var startDate = $("#dateTimeStart").val();
        var dateTimeEnd = $("#dateTimeEnd").val();
        var customerId = $("#customerId").val();
        var invoiceId = $("#invoiceid").val();
        var invoicetype = $("#invoicetype").val();
        var pathno = $("#pathno").val();
        var pathname = $("#pathname").val();
        var rate = $("#rate").val();





        if ((startDate === null || startDate === '') &&
            (dateTimeEnd === null || dateTimeEnd === '')
            //                (customerId === null || customerId === '') &&
            //                (invoiceId === null || invoiceId === '') &&
            //                (invoicetype === null || invoicetype === '') &&
            //                (pathno === null || pathno === '') &&
            //                (pathname === null || pathname === '')
        ) {

            notfoundData();
            $('#msgModalMessage').html("กรุณาระบุเงื่อนไขการค้นหา");
            $('#msgModal').modal('show');
        } else {
            //console.log("startDate: " + startDate);
            //console.log("dateTimeEnd: " + dateTimeEnd);

            var search = false;
            if ((startDate !== null && startDate !== '') && (dateTimeEnd !== null && dateTimeEnd !== '')) {

                if (process(startDate) > process(dateTimeEnd)) {
                    console.log("yyy");
                    search = false;
                    $('#msgModalMessage').html("กรุณาระบุวันที่ค้นให้ถูกต้อง");
                    $('#msgModal').modal('show');
                } else {
                    console.log("xxxx");
                    search = true;
                }

            } else {
                search = true;
            }

            console.log("search cus: " + search);
            if (search) {

                $("#loadMe").modal({
                    backdrop: "static", //remove ability to close modal with click
                    keyboard: false, //remove option to close with keyboard
                    show: true //Display loader!
                });


                var v_reate = '1';
                if (rate === null || rate === '') {
                    v_reate = '1';
                } else {
                    v_reate = rate;
                }
                console.log("v_reate: " + v_reate);
                var xhr = $.ajax({
                    url: $('#base_url').val() + "report/get_product_list_ajax_cus",
                    type: 'POST',
                    data: {
                        startDate: startDate,
                        dateTimeEnd: dateTimeEnd,
                        customerId: customerId,
                        invoiceId: invoiceId,
                        invoice_type: invoicetype,
                        pathno: pathno,
                        pathname: pathname,
                        rate: v_reate
                    },
                    async: true,
                    dataType: 'json',
                    success: function (data) {

                        var html = '';
                        var sumCulom1 = 0;
                        var sumCulom2 = 0;
                        var sumCusType = 0;
                        var allSum = 0;
                        var customer_type = '';
                        var sumQuantity = 0;

                        if (data.data.length === 0) {
                            closePop();
                            notfoundData();
                            $("#div_excel").hide();

                        } else {
                            $("#div_excel").show();

                            html += '<div>\n\
                                        <table class="table table-condensed table-bordered table-font"  style="width: 100%; margin-bottom: 0px;" border="1">\n\
                                            <thead>\n\
                                                    <tr>\n\
                                                        <th class="text-center" colspan="4" style="text-align: center; font-weight: bold; border: 1px solid black; vertical-align: middle;"><span class="text-center">Summary Report for BOI</span></th>\n\
                                                        <th class="text-right" style="width:18%; text-align: right; font-weight: bold; border: 1px solid black; vertical-align: middle;">Start Date ' + startDate + '<br> End Date ' + dateTimeEnd + '</th>\n\
                                                    </tr>\n\
                                             </thead>\n\
                                        </table>\n\
                                    </div>';
                            html += '<table class="table table-condensed table-bordered table-font cus-table" style="width: 100%;" border="1">\n\
                            <thead>\n\
                                <tr>\n\
                                    <th class="text-center" style="width:20%; background-color: #f0f8ff; font-weight: bold; border: 1px solid black; text-align: center;">Customer Id</th>\n\
                                    <th class="text-center" style="background-color: #f0f8ff; font-weight: bold; border: 1px solid black; text-align: center;">Customer Name</th>\n\
                                    <th class="text-center" style="width:18%; background-color: #f0f8ff; font-weight: bold; border: 1px solid black; text-align: center;">Rate</th>\n\
                                    <th class="text-center" style="width:18%; background-color: #f0f8ff; font-weight: bold; border: 1px solid black; text-align: center;">Quantity</th>\n\
                                    <th class="text-center" style="width:18%; background-color: #f0f8ff; font-weight: bold; border: 1px solid black; text-align: center;">Amount Total</th>\n\
                                </tr>\n\
                            </thead>\n\
                         <tbody>';


                            for (var i = 0; i < data.data.length; i++) {
                                console.log("customerId: " + data.data[i].customer_id);
                                console.log("customerName: " + data.data[i].customer_name);
                                console.log("customer_type: " + data.data[i].customer_type);
                                console.log("AmountDomestic: " + parseFloat(data.data[i].amount_domestic));
                                console.log("AmountExport: " + parseFloat(data.data[i].amount_export));

                                sumCulom1 = formatCurrencyToNumber(data.data[i].amount_domestic);
                                sumCulom2 = formatCurrencyToNumber(data.data[i].amount_export);

                                if (data.data[i].customer_type == '2') {
                                    console.log("customer_type2: " + data.data[i].customer_id);
                                    sumCusType = ((sumCulom1 + sumCulom2) * v_reate);
                                    console.log("sumCusType: " + sumCusType);
                                    allSum += sumCusType;
                                } else {
                                    sumCusType = sumCulom1 + sumCulom2;
                                    allSum += sumCusType;
                                }

                                sumQuantity += parseFloat(data.data[i].invoice_quantity);

                                html += '<tr>\n\
                                            <td class="text-left" style="text-align: left; border: 1px solid black;">' + data.data[i].customer_id + '</td>\n\
                                            <td class="text-left " style="text-align: left; border: 1px solid black;">' + data.data[i].customer_name + '</td>\n\
                                            <td class="text-right" style="text-align: right; border: 1px solid black;">&nbsp;' + rate + '</td>\n\
                                            <td class="text-right" style="text-align: right; border: 1px solid black;">&nbsp;' + formatMoney(parseFloat(data.data[i].invoice_quantity), 0) + '</td>\n\
                                            <td class="text-right" style="text-align: right; border: 1px solid black;">&nbsp;' + formatMoney(parseFloat(sumCusType.toFixed(2))) + '</td>\n\
                                        </tr>';


                            }
                            html += '<tr>\n\
                                        <td colspan="3" style="background-color: #fde9d9; font-weight: bold; text-align: right; font-size: 13px; border: 1px solid black; height: 30px;">Total</td>\n\
                                        <td style="background-color: #fde9d9; font-weight: bold; text-align: right; font-size: 13px; border: 1px solid black; height: 30px;">' + formatMoney(sumQuantity, 0) + '</td>\n\
                                        <td style="background-color: #fde9d9; font-weight: bold; text-align: right; font-size: 13px; border: 1px solid black; height: 30px;">' + formatMoney((allSum).toFixed(2)) + '</td>\n\
                                     </tr>';

                $("#rs_table").html(html);
                closePop();
            }
        }

    });
return false;
            }



        }

    } catch (e) {
    console.log("Error" + e);
} finally {
    console.log("finally");
    $("#loadMe").modal("hide");
}


});

function closePop() {
    console.log("Start colse popup");
    $("#loadMe").modal("hide");
    setTimeout(function () {
        $("#loadMe").modal("hide");
    }, 3000);

}

$("#btn_excel").click(function () {

    var type_page = $("#invoicetype option:selected").attr("name");
    var invoicetype = '';
    if (type_page === undefined) {
        invoicetype = '_All'
    } else {
        invoicetype = "_" + type_page;
    }

    var file_name = "SummaryReportBOI_" + getFormattedDate(new Date()) + invoicetype + ".xls";

    $("#rs_table").table2excel({
        filename: file_name

    });

    //$("#rs_table").table2excel({});
    //tableToExcel('rs_table', 'Export HTML Table to Excel');
    //$("#rs_table").tableToExcel({});
});

var tableToExcel = (function () {

    var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" \n\
                          xmlns:x="urn:schemas-microsoft-com:office:excel" \n\
                          xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]>\n\
                            <xml><x:ExcelWorkbook>\n\
                                <x:ExcelWorksheets>\n\
                                    <x:ExcelWorksheet>\n\
                                        <x:Name>{worksheet}</x:Name>\n\
                                        <x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions>\n\
                                        </x:ExcelWorksheet></x:ExcelWorksheets>\n\
                                    </x:ExcelWorkbook></xml><![endif]--></head>\n\
                            <body><table>{table}</table></body>\n\
                          </html>'
        , base64 = function (s) {
            return window.btoa(unescape(encodeURIComponent(s)))
        }
        , format = function (s, c) {
            return s.replace(/{(\w+)}/g, function (m, p) {
                return c[p];
            })
        }
    return function (table, name) {
        console.log("name: " + name);
        var type_page = $("#invoicetype option:selected").attr("name");
        var invoicetype = '';
        if (type_page === undefined) {
            invoicetype = '_All'
        } else {
            invoicetype = "_" + type_page;
        }
        console.log("type_page: " + type_page);
        name = 'Template_Export_Sale';
        if (!table.nodeType)
            table = document.getElementById(table)
        var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
        var blob = new Blob([format(template, ctx)]);
        var blobURL = window.URL.createObjectURL(blob);

        if (ifIE()) {
            console.log(" ifIE name: " + name);
            csvData = table.innerHTML;
            if (window.navigator.msSaveBlob) {
                var blob = new Blob([format(template, ctx)], {
                    type: "text/html"
                });
                navigator.msSaveBlob(blob, '' + 'Report_Export_Sale' + '.xls');
            }
        } else
            console.log(" else name: " + name);
        //window.location.href = uri + base64(format(template, ctx));
        var link = document.createElement("a");
        link.download = "SummaryReportBOI_" + getFormattedDate(new Date()) + invoicetype + ".xls";
        link.href = uri + base64(format(template, ctx));
        link.click();
    }
})()

function ifIE() {
    var isIE11 = navigator.userAgent.indexOf(".NET CLR") > -1;
    var isIE11orLess = isIE11 || navigator.appVersion.indexOf("MSIE") != -1;
    return isIE11orLess;
}


function getFormattedDate(date) {
    let year = date.getFullYear();
    let month = (1 + date.getMonth()).toString().padStart(2, '0');
    let day = date.getDate().toString().padStart(2, '0');

    return year + '-' + month + '-' + day;
}

function isDupItem(item, arrayCus) {
    for (var i = 0; i < arrayCus.length; i++) {
        if (arrayCus[i] === item) {
            return true;
        }
    }
    return false;
}


function sumAllFunction(data, colum) {

    var result = 0;
    if (data) {
        for (var i = 0; i < data.length; i++) {
            if (colum === 1) {
                if (data[i]) {
                    result += formatCurrencyToNumber(data[i].price_flag == 1 ? data[i].unit_price : 0);
                }
            }
            if (colum === 2) {
                if (data[i]) {
                    result += formatCurrencyToNumber(data[i].price_flag == 2 ? data[i].unit_price : 0);
                }
            }
            if (colum === 3) {
                if (data[i]) {
                    result += formatCurrencyToNumber((data[i].price_flag == 1 ? data[i].invoice_quantity : 0));
                }
            }
            if (colum === 4) {
                if (data[i]) {
                    result += formatCurrencyToNumber((data[i].price_flag == 2 ? data[i].invoice_quantity : 0));
                }
            }
            if (colum === 5) {
                if (data[i]) {
                    result += data[i].price_flag == 1 ? formatCurrencyToNumber(formatCurrency((data[i].unit_price * data[i].invoice_quantity), 2)) : 0;
                }
            }
            if (colum === 6) {
                if (data[i]) {
                    result += data[i].price_flag == 2 ? formatCurrencyToNumber(formatCurrency((data[i].unit_price * data[i].invoice_quantity), 2)) : 0;
                }
            }
        }
    }
    return result;
}

function notfoundData() {
    var tr = '<table class="table table-condensed table-bordered table-font cus-table" style="width: 100%;">\n\
                      <thead>\n\
                            <tr>\n\
                                <td class="text-center" style="width:20%;"><strong>Customer Id</strong></td>\n\
                                <td class="text-center"><strong>Customer Name</strong></td>\n\
                                <td class="text-center" style="width:18%;"><strong>Rate</strong></td>\n\
                                <td class="text-center" style="width:18%;"><strong>Quantity</strong></td>\n\
                                <td class="text-center" style="width:18%;"><strong>Amount Total</strong></td>\n\
                            </tr>\n\
                        </thead>\n\
                        <tbody id="id-tbody">\n\
                            <tr><td colspan="11" class="text-center">ไม่พบข้อมูลการค้นหา</td></tr>\n\
                        </tbody>\n\
              </table>';
    $("#rs_table").html(tr);
}

$("#btn_clear").click(function () {

    console.log("### start btn clear ###");
    $("#dateTimeStart").val("");
    $("#dateTimeEnd").val("");
    $("#customerId").val("");
    $("#productId").val("");
    $("#invoiceid").val("");
    $("#invoicetype").val("");
    $("#pathno").val("");
    $("#pathname").val("");
    $("#rate").val("");
    $("#div_excel").hide();
    notfoundData();

});

function formatMoney(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

function floatOnly(id) {
    var element = document.getElementById(id);
    var regex = /[^0-9.]/gi;
    element.value = element.value.replace(regex, "");
}