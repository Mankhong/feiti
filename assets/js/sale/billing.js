$(document).ready(function () {
    //processDefaultMode();

    $('.view-mode-1-button').on('click', function () {
        $('#flag_search').val("1");
        processViewMode(1);
    });

    $('.view-mode-2-button').on('click', function () {
        $('#flag_search').val("2");
        processViewMode(2);
    });

    $('#action_search').on('keyup', function () {
        Dataitems.filterData(this.value);
    });

    init_date();
    $("#rs_div").hide();
    
});

function init_date() {
    $("#dateTimeStart").datepicker({
                    format: 'dd/mm/yyyy',
                    autoclose: true,
                    orientation: "bottom auto",
                    todayHighlight: true,
                    todayBtn: true,
                    language: 'EN', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                    thaiyear: false, //Set เป็นปี พ.ศ.
        }).datepicker("setDate");
    $("#dateTimeEnd").datepicker({
                    format: 'dd/mm/yyyy',
                    autoclose: true,
                    orientation: "bottom auto",
                    todayHighlight: true,
                    todayBtn: true,
                    language: 'EN', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                    thaiyear: false, //Set เป็นปี พ.ศ.
        }).datepicker("setDate");

}


$("#btn_search").click(function () {
    onSearch();
});

function  onSearch() {

    var search = onValidate();
    if (search) {
        var flag_search = $('#flag_search').val();
        if (flag_search == "2") {
            loadDataitems();
        } else {
            var startDate = $("#dateTimeStart").val();
            var dateTimeEnd = $("#dateTimeEnd").val();
            var billing_id_search = $("#billing_id_search").val();
            var customer_name = $("#customer_name").val();

            $("#rs_div").show();
            $("#dataitems").hide();
            loadDatatable(startDate, dateTimeEnd, billing_id_search, customer_name);
        }
    }

}

function onValidate() {
    var startDate = $("#dateTimeStart").val();
    var dateTimeEnd = $("#dateTimeEnd").val();
    var billing_id_search = $("#billing_id_search").val();
    var customer_name = $("#customer_name").val();
    var search = false;
    if ((startDate === null || startDate === '') &&
            (dateTimeEnd === null || dateTimeEnd === '') &&
            (billing_id_search === null || billing_id_search === '') &&
            (customer_name === null || customer_name === '')) {

        var table = $('#rstable').DataTable();
        table.clear().draw();


        $('#msgModalMessage').html("กรุณาระบุเงื่อนไขการค้นหา");
        $('#msgModal').modal('show');
    } else {

        if ((startDate !== null && startDate !== '') && (dateTimeEnd !== null && dateTimeEnd !== '')) {

            if (process(startDate) > process(dateTimeEnd)) {
                search = false;
                var table = $('#rstable').DataTable();
                table.clear().draw();

                $('#msgModalMessage').html("กรุณาระบุวันที่ค้นให้ถูกต้อง");
                $('#msgModal').modal('show');

            } else {
                search = true;
            }
        } else {
            search = true;
        }
    }
    return search;
}

function process(date) {
    var parts = date.split("/");
    return new Date(parts[2], parts[1] - 1, parts[0]);
}


$("#btn_clear").click(function () {

//    $("#planlist").hide();
//    console.log("### start btn clear ###");
    $("#dateTimeStart").val("");
    $("#dateTimeEnd").val("");
    $("#billing_id_search").val("");
    $("#customer_name").val("");
    var table = $('#rstable').DataTable();
    table.clear().draw();
    
    $("#dataitems").hide();
    $("#rs_div").hide();

});

function processDefaultMode() {
    var viewMode = localStorage.getItem('viewMode');
    if (viewMode) {
        processViewMode(viewMode);
    } else {
        processViewMode(1);
    }
}

//ViewModde 1 = table, 2 = item
function processViewMode(viewMode) {
    if (viewMode == 2) {
        $('.view-mode-1').hide();
        var search = onValidate();
        console.log("search: " + search);
        if (search) {
            loadDataitems();
        }
        $('.view-mode-2').show();

    } else {
        $('.view-mode-2').hide();
        //loadDatatable();
        onSearch();
        hideSearch();
        $('.view-mode-1').show();
    }
    $('#view_mode').val(viewMode);
    localStorage.setItem('viewMode', viewMode);
}

function hideSearch() {
    $('#rstable_filter').css('display', 'none');
}

function loadDatatable(startDate, dateTimeEnd, billing_id_search, customer_name) {
    
    $("#rstable").dataTable().fnDestroy();
    var rowId = 1;
    showLoading();
    var t = $('#rstable').DataTable({
        //ajax: 'billing/get_billing_list_ajax',
        
        pageLength: 20,
        searching: false,
        ordering: true,
        lengthChange: false,
        "info": false,
        responsive: true,
        
//        searching: true,
//        ordering: true,
//        order: [
//            [1, 'asc']
//        ],
//        lengthChange: false,
//        pageLength: 20,

        ajax: {
            url: "billing/get_billing_list_ajax",
            type: "POST",
            async: true,
            data: {startDate: startDate,
                dateTimeEnd: dateTimeEnd,
                billing_id_search: billing_id_search,
                customer_name: customer_name
            },
        },
        "rowId": 'extn',
        columns: [
            {targets: 1,
                data: null,
                "className": "text-center",
                render: function (data, type, row, meta) {
                    return rowId++;
                }
            },
            {
                'data': 'billing_id'
            },
            {
                'data': 'customer_name'
            },
            {
                'data': 'billing_date'
            },
            {
                'data': 'amount',
                'render': function (data, type, row, meta) {
                    return formatCurrency(data);
                }
            },
            {
                'data': 'total',
                'render': function (data, type, row, meta) {
                    //var amount =  parseFloat(row['amount']) + (parseFloat(row['amount']) * (parseFloat(row['vat']) / 100));
                    return formatCurrency(data);
                }
            },
            {
                'data': 'last_update_date'
            },
            {
                'data': 'last_update_user'
            },
            {
                'data': "billing_id",
                'render': function (data, type, row, meta) {
                    var base_url = $('#base_url').val();
                    var id = encodeURIComponent(btoa(data));
                    return '<a class="btn btn-default btn-icon" href="' + base_url + 'billing/edit/' + id + '"><i class="far fa-edit"></i></a>';
                }
            },
        ],
        columnDefs: [{
                "targets": 0,
                "className": "text-center"
            },
            {
                "targets": 3,
                "className": "text-center"
            },
            {
                "targets": 4,
                "className": "text-right"
            },
            {
                "targets": 5,
                "className": "text-right"
            },
            {
                "targets": 6,
                "className": "text-center"
            },
            {
                "targets": 8,
                "className": "text-center"
            },
        ],
        initComplete: function(settings, json){
            hideLoading();
        }
    });

    //Sort without No.
//    t.on('order.dt search.dt', function () {
//        t.column(0, {
//            search: 'applied',
//            order: 'applied'
//        }).nodes().each(function (cell, i) {
//            cell.innerHTML = i + 1;
//        });
//    }).draw();

    //=== Custom Filter
    $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var match = true;
                var search = $('#action_search').val();
                if (search != '') {
                    match = (data[1].toLowerCase().indexOf(search.toLowerCase()) == 0 || data[2].toLowerCase().indexOf(search.toLowerCase()) == 0);
                }

                return match;
            }
    );

    $('#action_search').keyup(function (event) {
        if ($('#view_mode').val() == 1) {
            t.draw();
        }
    });
    //=== End Custom Filter

    $('#rstable tbody').on('click', 'tr', function () {
        var t = $('#rstable').DataTable();
        var data = t.row(this).data();
        var id = encodeString(data['billing_id']);
        window.location.href = $('#base_url').val() + 'billing/edit/' + id;
    });
}

function loadDataitems() {
    
    var startDate = $("#dateTimeStart").val();
    var dateTimeEnd = $("#dateTimeEnd").val();
    var billing_id_search = $("#billing_id_search").val();
    var customer_name = $("#customer_name").val();
    $('.view-mode-2').show();
    $("#dataitems").show();
    
    Dataitems.loadData('billing/get_billing_list_ajax', 
    {startDate, dateTimeEnd, billing_id_search, customer_name},
    'dataitems', 'billing_id', 'customer_image_path', 'billing_id', ['customer_name', 'receipt_date'], ['Customer', 'Billing Date'], 'onclickItem');
}

function onclickItem(id) {
    var base_url = $('#base_url').val();
    window.location.href = base_url + 'billing/edit/' + id;
}