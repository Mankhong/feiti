$(document).ready(function(){
	$('select[name=continent]').on('change', function(){
		loadCountryDropdown();
	});

	$('select[name=country]').on('change', function(){
		$('#country_value').val($('select[name=country]').val());
	});

	loadCountryDropdown();
});

function onclickSave() {
	$('#form').submit();
}

function loadCountryDropdown() {
	var continent = strNotNull($('select[name=continent]').val());

	showCountryLoading();
	$.ajax({
		type: 'GET',
		url: $('#base_url').val() + "customer/get_countries_list_ajax?continent=" + continent,
		success: function(resp) {
			var html = '';
			for (var i = 0; i < resp.data.length; i++) {
				html += '<option value="' + resp.data[i]['country_code'] + '">' + resp.data[i]['country_name'] + '</option>';
			}
			$('select[name=country]').html(html);
			setCountrySelect($('#country_value').val());

			hideCountryLoading();
		},
		error: function() {
			hideCountryLoading();
		}
	});
}

function setCountrySelect(value) {
	var options = $('select[name=country] option');
	var select = options[0].value;
	for (var i = 0; i < options.length; i++) {
		if (options[i].value == value) {
			select = value;
			break;
		}
	}
	$('select[name=country]').val(select);
	$('#country_value').val(select);
}

function showCountryLoading() {
	$('#country_loading').removeClass('d-none');
	$('select[name=country]').addClass('d-none');
}

function hideCountryLoading() {
	$('select[name=country]').removeClass('d-none');
	$('#country_loading').addClass('d-none');
}