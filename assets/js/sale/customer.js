$(document).ready(function () {
	processDefaultMode();

	$('.view-mode-1-button').on('click', function () {
		processViewMode(1);
	});

	$('.view-mode-2-button').on('click', function () {
		processViewMode(2);
	});

	$('#action_search').on('keyup', function () {
		Dataitems.filterData(this.value);
	});
});

function processDefaultMode() {
	var viewMode = localStorage.getItem('viewMode');
	if (viewMode) {
		processViewMode(viewMode);
	} else {
		processViewMode(1);
	}
}

//ViewModde 1 = table, 2 = item
function processViewMode(viewMode) {
	if (viewMode == 2) {
		$('.view-mode-1').hide();
		loadDataitems();
		$('.view-mode-2').show();

	} else {
		$('.view-mode-2').hide();
		loadDatatable();
		hideSearch();
		$('.view-mode-1').show();
	}
	$('#view_mode').val(viewMode);
	localStorage.setItem('viewMode', viewMode);
}

function hideSearch() {
	$('#rstable_filter').css('display', 'none');

	// var html = $('#rstable_filter').html();
	// html = '<a class="btn btn-default btn-sm"><i class="far fa-file-excel"></i> Excel</a>' +
	// 	'<a class="btn btn-default btn-sm ml-1 mr-1"><i class="far fa-file-pdf"></i> Pdf</a>' +
	// 	html;
	// $('#rstable_filter').html(html);
}

function loadDatatable() {
	$("#rstable").dataTable().fnDestroy();

	var t = $('#rstable').DataTable({
		ajax: 'customer/get_customer_list_ajax',
		searching: true,
		ordering: true,
		order: [
			[1, 'asc']
		],
		lengthChange: false,
		pageLength: 20,
		columns: [{
				'data': null,
				'render': function (data, type, row, meta) {
					return meta.row + 1;
				}
			},
			{
				'data': 'id'
			},
			{
				'data': 'name'
			},
			{
				'data': 'customer_type_name'
			},
			{
				'data': 'country_name'
			},
			{
				'data': 'last_update_date'
			},
			{
				'data': 'last_update_user'
			},
			{
				'data': "id",
				'render': function (data, type, row, meta) {
					var base_url = $('#base_url').val();
					var id = encodeString(data);
					return '<a class="btn btn-default btn-icon" href="' + base_url + 'customer/edit/' + id + '"><i class="far fa-edit"></i></a>';
				}
			},
			{
				'data': 'business_type_name'
			},
			{
				'data': 'mobile',
				'render': function (data, type, row, meta) {
					var mobile = strNotNull(row['mobile']);
					var phone = strNotNull(row['phone']);
					var tel_no = mobile;

					if (mobile != '') {
						if (phone != '') {
							return mobile + ',' + phone;
						} else {
							return mobile;
						}

					} else {
						return phone;
					}
				}
			},
			{
				'data': 'phone'
			},
			{
				'data': 'fax'
			},
			{
				'data': 'facebook'
			},
			{
				'data': 'line'
			},
			{
				'data': 'website'
			},
			{
				'data': 'email'
			},
			{
				'data': 'address'
			},
			{
				'data': 'internal_note'
			},
			{
				'data': 'contact'
			},
		],
		columnDefs: [{
				"targets": 0,
				"className": "text-center"
			},
			{
				"targets": 1,
				"className": "text-center"
			},
			{
				"targets": 5,
				"className": "text-center"
			},
			{
				"targets": 7,
				"className": "text-center"
			},
			{
				"targets": 8,
				"className": "d-none"
			},
			{
				"targets": 9,
				"className": "d-none"
			},
			{
				"targets": 10,
				"className": "d-none"
			},
			{
				"targets": 11,
				"className": "d-none"
			},
			{
				"targets": 12,
				"className": "d-none"
			},
			{
				"targets": 13,
				"className": "d-none"
			},
			{
				"targets": 14,
				"className": "d-none"
			},
			{
				"targets": 15,
				"className": "d-none"
			},
			{
				"targets": 16,
				"className": "d-none"
			},
			{
				"targets": 17,
				"className": "d-none"
			},
			{
				"targets": 18,
				"className": "d-none"
			},
		],
		fnDrawCallback: function (settings) {
			//initExportButtons('#rstable', 'Customers', generateReportName('CUSTOMERS'), [1, 2, 16, 9, 18, 15]);
		},
	});

	//Sort without No.
	t.on('order.dt search.dt', function () {
		t.column(0, {
			search: 'applied',
			order: 'applied'
		}).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();

	//=== Custom Filter
	$.fn.dataTable.ext.search.push(
		function (settings, data, dataIndex) {
			var match = true;
			var search = $('#action_search').val();
			if (search != '') {
				match = (data[1].toLowerCase().indexOf(search.toLowerCase()) == 0 || data[2].toLowerCase().indexOf(search.toLowerCase()) == 0);
			}

			return match;
		}
	);

	$('#action_search').keyup(function (event) {
		if ($('#view_mode').val() == 1) {
			t.draw();
		}
	});
	//=== End Custom Filter

	$('#rstable tbody').on('click', 'tr', function () {
		var data = t.row(this).data();
		var id = encodeString(data['id']);
		window.location.href = $('#base_url').val() + 'customer/edit/' + id;
	});

}

function loadDataitems() {
	Dataitems.loadData('customer/get_customer_list_ajax', null, 'dataitems', 'id', 'image_path', 'id', ['name', 'country_name', 'customer_type_name'], ['Name', 'Contry', 'Customer Type'], 'onclickItem');
}

function onclickItem(id) {
	var base_url = $('#base_url').val();
	window.location.href = base_url + 'customer/edit/' + id;
}

function exportExcel() {
	var delimiter = '&88&';
	var rows = $('#rstable').DataTable().rows( {filter : 'applied'}).data();
	var value = '';
	for (var i = 0; i < rows.length; i++) {
		if (i > 0) {
			value += delimiter;
		}
		value += rows[i].id;
	}

	$('#customer_ids').val(value);
	$('#export_excel_form').submit();
}