$(document).ready(function () {
	$('.datepicker').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true
	});

	$('.customerPickerTrigger').on('click', function () {
		CustomerPicker.showPicker('selectedCustomer');
	});

	$('#cancel_order_button').on('click', function() {
		cancelOrder();
	});

	$('input[name=\'invoice_group\']').on('click', function() {
		renderInvoiceType();
	});

	CustomerPicker.selectedCallBack = function (data) {
		CustomerPicker.hidePicker();
		$('#customer_id').val(data['id']);
		$('#customer_name').val(data['name']);
		$('#customer_type').val(data['customer_type']);
		setOrderTypeByCustomerType();

		clearDataitemsAndHiddens();

		var order_id = $('#order_id').val();
		loadDeliveryProductsTable(customer_id, order_id);

		$('#order_id').focus();

		//renderProductListTableByOrderType();
		//renderDeliveryProductListTableByOrderType();
	}

	$('.productPickerTrigger').on('click', function () {
		var customer_id = $('#customer_id').val();
		if (customer_id) {
			ProductPicker.showPicker('selectedProduct', customer_id);
		} else {
			showMessageModal("Customer ID is required, please select Customer first.");
		}
	});

	ProductPicker.selectedCallBack = function (data) {
		selectedProduct(data['seq'], data['id'], data['desc1'], data['unit_price'], data['invoice_quantity'], data['price_flag'], data['invoice_type_name'], data['invoice_type_phase']);
	}

	$('#prodcutQuantityButton').on('click', function () {
		if ($('#productQuantity').val()) {
			$('#productQuantityModal').modal('hide');
			setSelectedProduct();
		} else {
			$('#productQuantityError').html('Please enter quantity.');
		}
	});

	$('#productQuantity').on('keyup', function (event) {
		if (event.keyCode === 13) {
			if ($('#productQuantity').val()) {
				$('#productQuantityModal').modal('hide');
				setSelectedProduct();
			} else {
				$('#productQuantityError').html('Please enter quantity.');
			}
		}
	});

	$('input[name=order_type]').on('change', function () {
		renderProductListTableByOrderType();
		renderDeliveryProductListTableByOrderType();
	});

	$('#deliveryProductList').on('click', function () {
		var customer_id = $('#customer_id').val();
		var order_id = $('#order_id').val();
		loadDeliveryProductsTable(customer_id, order_id);
	});

	$('#confirmDeleteButton').on('click', function () {
		$('#confirmDeleteModal').modal('hide');
		onDeleteProductDataitem();
		calculateSummary();
	});

	$('#order_id').on('blur', function(){
		check_order_id();
	});

	$('input[name=\'invoice_group\']').on('change', function(){
		check_order_id();
	});

	$('#invoice_type').on('change', function() {
		check_order_id();
	});

	setOrderTypeByCustomerType();
	initInputEditorEvent();
	deleteProductDataitemsBySession();
	calculateDataitems();
	calculateSummary();
	renderProductListTableByOrderType();
	setDefaultOrderDate();
	setReadonlyOrderType();
	checkDisableButton();
	renderInvoiceType();
});

function renderInvoiceType() {
	var invoice_group = $('input[name=\'invoice_group\']:checked').val();
	if (invoice_group == 'S') {
		$('#invoice_type').show();
	} else {
		$('#invoice_type').hide();
	}
}

function setReadonlyOrderType() {
	$('input[name=order_type]').on('click', function (e) {
		e.preventDefault();
		return false;
	});
}

function clearInitInputEditorEvent() {
	$('input.pz-input-editor').unbind('blur');
}

function initInputEditorEvent() {
	$('input.dt-quantity-input.pz-input-editor').on('blur', function () {
		onEditOrderProduct($(this).attr('data-id'));
	});

	$('input.dt-unit-price-input.pz-input-editor').on('blur', function () {
		onEditOrderProduct($(this).attr('data-product-id'));
	});
}

function setDefaultOrderDate() {
	if ($('#order_date').val() === '') {
		$('#order_date').val(getCurrentDateDDMMYYYY());
	}
}

function loadDeliveryProductsTable(customer_id, order_id) {
	$("#deliveryProductsTable").dataTable().fnDestroy();

	customer_id = encodeString(customer_id);
	order_id = encodeString(order_id);

	if (customer_id !== '' && order_id !== '') {

		var t = $('#deliveryProductsTable').DataTable({
			ajax: $('#base_url').val() + 'order/get_delivery_products_ajax?customer_id=' + customer_id + '&order_id=' + order_id,
			searching: false,
			ordering: true,
			order: [
				[1, 'asc']
			],
			lengthChange: false,
			pageLength: 10,
			columns: [{
					'data': 'product_id'
				},
				{
					'data': 'order_date'
				},
				{
					'data': 'order_id'
				},
				{
					'data': 'quantity'
				},
				{
					'data': 'product_id',
					'render': function (data, type, row, meta) {
						return '<span class="dv-unit-price">' + (formatCurrency(row['unit_price'], 4)) + '</span>';
					}
				},
				{
					'data': 'product_id',
					'render': function (data, type, row, meta) {
						amount = parseFloatOrNumeric(row['quantity']) * parseFloatOrNumeric(row['unit_price']);
						return '<span class="dv-unit-price">' + (formatCurrency(amount)) + '</span>';
					}
				},
			],
			columnDefs: [{
					"targets": 1,
					"className": "text-center"
				},
				{
					"targets": 3,
					"className": "text-right"
				},
				{
					"targets": 4,
					"className": "text-right"
				},
				{
					"targets": 5,
					"className": "text-right"
				},
			],
			fnDrawCallback: function (settings) {
				renderDeliveryProductListTableByOrderType();
			},
		});

		//Sort without No.
		// t.on('order.dt search.dt', function () {
		// 	t.column(0, {
		// 		search: 'applied',
		// 		order: 'applied'
		// 	}).nodes().each(function (cell, i) {
		// 		cell.innerHTML = i + 1;
		// 	});
		// }).draw();
	}
}

function calculateDataitems() {
	var dataitems = $('#productListTable .dataitem');
	if (dataitems) {
		for (var i = 0; i < dataitems.length; i++) {
			var quantity = $(dataitems[i]).find('.dt-quantity').text();
			var unit_price = $(dataitems[i]).find('.dt-unit-price').text();
			var amount = '';
			var balance = $(dataitems[i]).find('.dt-balance').text();
			var invoice_quantity = $(dataitems[i]).find('.dt-invoice-quantity').val();

			// calculate
			quantity = formatCurrencyToNumber(quantity);
			amount = roundup(formatCurrencyToNumber(unit_price), 4) * quantity;
			amount = formatCurrency(amount);
			balance = quantity - formatCurrencyToNumber(invoice_quantity);

			$(dataitems[i]).find('.dt-amount').text(formatCurrency(amount));
			$(dataitems[i]).find('.dt-balance').text(formatCurrency(balance, 0));
		}
	}
}

function deleteProductDataitemsBySession() {
	var delimiter = '&88&';
	var session_delete_hiddens = $('.session-delete-hiddens');

	if (session_delete_hiddens) {
		for (var i = 0; i < session_delete_hiddens.length; i++) {
			var values = session_delete_hiddens[i].value.split(delimiter);
			deleteProductDataitem(values[0]);
		}
	}
}

function onclickSave() {
	$('#form').submit();
}

function confirmDeleteProductDataitem(product_id) {
	localStorage.setItem("delete_product_id", product_id);
	$('#confirmDeleteModal').modal('show');
}

function onDeleteProductDataitem() {
	var product_id = localStorage.getItem("delete_product_id");
	if (hasProductDataitem(product_id)) {
		deleteProductDataitem(product_id);
	}
}

function selectedCustomer(customer_id, customer_name, customer_type, customer_address, customer_credit, customer_type_name) {
	CustomerPicker.hidePicker();
	$('#customer_id').val(customer_id);
	$('#customer_name').val(customer_name);
	$('#customer_type').val(customer_type);
	setOrderTypeByCustomerType();

	clearDataitemsAndHiddens();

	var order_id = $('#order_id').val();
	loadDeliveryProductsTable(customer_id, order_id);

	// var deliveryTab = $('#deliveryProductList');
	// if (deliveryTab.lenght > 0 && $(deliveryTab).attr('class').indexOf('show') >= 0) {
	// 	var order_id = $('#order_id').val();
	// 	loadDeliveryProductsTable(customer_id, order_id);
	// } else {
	// 	$("#deliveryProductsTable").dataTable().fnDestroy();
	// }

	renderProductListTableByOrderType();
	renderDeliveryProductListTableByOrderType();

}

function setOrderTypeByCustomerType() {
	var customer_type = $('#customer_type').val();
	var order_types = $('input[name=order_type]');

	for (var i = 0; i < order_types.length; i++) {
		if (order_types[i].value == customer_type) {
			$(order_types[i]).attr('checked', true);
			break;
		}
	}
}

function selectedProduct(product_seq, product_id, desc1, unit_price, invoice_quantity, price_flag, invoice_type_name, invoice_type_phase) {
	ProductPicker.hidePicker();
	var product_id = encodeString(product_id);

	if (hasProductDataitem(product_id)) {
		showMessageModal('Product is already selected.');
	} else {
		localStorage.setItem('product_seq', encodeString(product_seq));
		localStorage.setItem('product_id', product_id);
		localStorage.setItem('desc1', desc1);
		localStorage.setItem('unit_price', parseFloatOrNumeric(unit_price));
		localStorage.setItem('invoice_quantity', parseFloatOrNumeric(invoice_quantity));
		localStorage.setItem('price_flag', price_flag);
		localStorage.setItem('invoice_type_name', invoice_type_name);
		localStorage.setItem('invoice_type_phase', invoice_type_phase);

		$('#productQuantityModal').modal('show');
		$('#productQuantity').focus();
	}
}

function setSelectedProduct() {
	var product_seq = localStorage.getItem('product_seq');
	var product_id = localStorage.getItem('product_id');
	var product_desc = localStorage.getItem('desc1');
	var unit_price = localStorage.getItem('unit_price');
	var invoice_quantity = localStorage.getItem('invoice_quantity');
	var price_flag = localStorage.getItem('price_flag');
	var quantity = parseFloatOrNumeric($('#productQuantity').val());
	var invoice_type_name = localStorage.getItem('invoice_type_name');
	var invoice_type_phase = localStorage.getItem('invoice_type_phase');
	var amount = quantity * unit_price;
	var balance = 0;

	quantity = formatCurrency(quantity, 0);
	unit_price = formatCurrency(unit_price, 4);
	amount = formatCurrency(amount);
	//*** */
	addProductDataitem('NEW', product_seq, product_id, product_desc, quantity, unit_price, amount, balance, invoice_quantity, price_flag, invoice_type_name, invoice_type_phase);
	createDataitemHidden('NEW', product_seq, product_id, product_desc, quantity, unit_price, amount, balance, invoice_quantity, price_flag, invoice_type_name, invoice_type_phase);

	//renderProductListTableByOrderType();
	$('#productQuantity').val('');

	calculateDataitems();
	calculateSummary();
	pzClearEvent();
	clearInitInputEditorEvent();
	pzInitEvent();
	initInputEditorEvent();
}

function addProductDataitem(dataitem_type, product_seq, product_id, product_desc, quantity, unit_price, amount, balance, invoice_quantity, price_flag, invoice_type_name, invoice_type_phase) {
	var html = '<tr class="dataitem">' +
		'<td>' +
		'<input type="hidden" class="dt-product-seq" value="' + product_seq + '"/>' +
		'<input type="hidden" class="dt-product-id" value="' + product_id + '"/>' + 
		'<input type="hidden" class="dt-product-desc" value="' + product_desc + '"/>' +
		'<input type="hidden" class="dt-invoice-quantity" value="' + invoice_quantity + '"/>' + 
		'<input type="hidden" class="dt-dataitem-type" name="dataitem_type" value="' + dataitem_type + '"/>' +
		'<input type="hidden" class="dt-price-flag" value="' + price_flag + '"/>' + 
		'<input type="hidden" class="dt-invoice-type-name" value="' + invoice_type_name + '"/>' + 
		'<input type="hidden" class="dt-invoice-type-phase" value="' + invoice_type_phase + '"/>' + 
		'<span class="dts-product-id">' + decodeString(product_id) + '</span>' + 
		'</td>' +
		'<td class="text-right">' +
		'<div class="dt-quantity pz-input-editor" data-id="' + product_id + '" data-type="currency" data-decimal="0">' + quantity + '</div>' + 
		'<input type="number" class="dt-quantity-input pz-input-editor" data-id="' + product_id + '"/>' + 
		'</td>' +
		'<td class="text-right">' + 
		'<div class="dt-unit-price pz-input-editor" data-id="' + product_id + '_np" data-type="currency" data-decimal="4">' + unit_price + '</div>' +
		'<input type="number" class="dt-unit-price-input pz-input-editor" data-id="' + product_id + '_np" data-product-id="' + product_id + '"/>' +
		'</td>' +
		'<td class="text-right">' +
		'<span class="dt-amount">' + amount + '</span>' +
		'</td>' +
		'<td class="text-right">' + 
		'<span class="dt-balance">' + balance + '</span>' +
		'</td>' +
		'<td class="text-left">' + 
		'<span class="dts-price-flag">' + (price_flag == '1' ? 'Local Price' : (price_flag == '2' ? 'BOI Price' : '')) + '</span>' +
		'</td>' +
		'<td class="text-center">' +
		'<a class="btn btn-default btn-sm btn-icon" onclick="confirmDeleteProductDataitem(\'' + product_id + '\')"><i class="fas fa-times fa-sm"></i></a>' +
		'</td>' +
		'</tr>';

	$('#productListTable tbody').append(html);
}

function deleteProductDataitem(product_id) {
	var dataitems = $('#productListTable .dataitem');
	for (var i = 0; i < dataitems.length; i++) {
		var dt_product_seq = $(dataitems[i]).find('.dt-product-seq').val();
		var dt_product_id = $(dataitems[i]).find('.dt-product-id').val();
		var dt_dataitem_type = $(dataitems[i]).find('.dt-dataitem-type').val();
		var dt_product_desc = $(dataitems[i]).find('.dt-product-desc').val();
		var dt_quantity = $(dataitems[i]).find('.dt-quantity').val();
		var dt_unit_price = $(dataitems[i]).find('.dt-unit-price').val();
		var dt_amount = $(dataitems[i]).find('.dt-amount').val();
		var dt_balance = $(dataitems[i]).find('.dt-balance').val();
		var dt_invoice_quantity = $(dataitems[i]).find('.dt-invoice-quantity').val();
		var dt_price_flag = $(dataitems[i]).find('.dt-price-flag').val();
		var dt_invoice_type_name = $(dataitems[i]).find('.dt-invoice-type-name').val();
		var dt_invoice_type_phase = $(dataitems[i]).find('.dt-invoice-type-phase').val();

		if (dt_product_id == product_id) {
			if (dt_dataitem_type == 'DATA') {
				deleteDataitemHidden('EDIT', product_id);
				createDataitemHidden('DEL', dt_product_seq, dt_product_id, dt_product_desc, dt_quantity, dt_unit_price, dt_amount, dt_balance, dt_invoice_quantity, dt_price_flag, dt_invoice_type_name, dt_invoice_type_phase);
			} else if (dt_dataitem_type == 'NEW') {
				deleteDataitemHidden('NEW', product_id);
			}

			$(dataitems[i]).remove();
			break;
		}
	}
}

//product_seq|product_id|product_desc|quantity|unit_price|amount|balance|invoice_quantity|dataitem_type|price_flag|invoice_type_name|invoice_type_phase
function createDataitemHidden(dataitem_type, product_seq, product_id, product_desc, quantity, unit_price, amount, balance, invoice_quantity, price_flag, invoice_type_name, invoice_type_phase) {
	var delimiter = '&88&';
	var str_values = product_seq +
		delimiter + product_id +
		delimiter + product_desc +
		delimiter + quantity +
		delimiter + unit_price +
		delimiter + amount +
		delimiter + balance +
		delimiter + invoice_quantity +
		delimiter + dataitem_type +
		delimiter + price_flag +
		delimiter + invoice_type_name +
		delimiter + invoice_type_phase;


	var hidden_name = '';
	if (dataitem_type == 'NEW') {
		hidden_name = 'new_dataitems[]';

	} else if (dataitem_type == 'DEL') {
		hidden_name = 'delete_dataitems[]';

	} else if (dataitem_type == 'EDIT') {
		hidden_name = 'edit_dataitems[]';
	}

	var hidden = '<input type="hidden" class="dataitem-hidden" name="' + hidden_name + '" data-product-id="' + product_id + '" dataitem-type="' + dataitem_type + '" value="' + str_values + '"/>';
	$('#dataitem_hidden_container').append(hidden);
}

function deleteDataitemHidden(dataitem_type, product_id) {
	var dataitem_hiddens = $('.dataitem-hidden');
	if (dataitem_hiddens) {
		for (var i = 0; i < dataitem_hiddens.length; i++) {
			var dth_product_id = $(dataitem_hiddens[i]).attr('data-product-id');
			var dth_type = $(dataitem_hiddens[i]).attr('dataitem-type');
			if (dth_product_id == product_id && dth_type == dataitem_type) {
				$(dataitem_hiddens[i]).remove();
				break;
			}
		}
	}
}

function hasProductDataitem(product_id) {
	var dataitems = $('#productListTable .dataitem');

	if (dataitems) {
		for (var i = 0; i < dataitems.length; i++) {
			var dt_product_id = $(dataitems[i]).find('.dt-product-id').val();
			if (dt_product_id == product_id) {
				return true;
			}
		}
	}

	return false;
}

function hasProductDataitemHidden(product_id) {
	//delete
	var dataitem_hiddens = $('.dataitem-hidden');
	if (dataitem_hiddens) {
		for (var i = 0; i < dataitem_hiddens.length; i++) {
			var dth_product_id = $(dataitem_hiddens[i]).attr('data-product-id');
			if (dth_product_id == product_id) {
				return true;
			}
		}
	}
	return false;
}

//*** OLD */
function renderProductListTableByOrderType() {
	var order_type = $('input[name=order_type]:checked').val();
	order_type = order_type ? order_type : 1; //default = domestic

	if (order_type == 1) {
		$('.dt-unit-price-domestic').show();
		$('.dt-unit-price-export').hide();

		$('.dt-amount-domestic').show();
		$('.dt-amount-export').hide();

		$('.dt-balance-domestic').show();
		$('.dt-balance-export').hide();
	} else {
		$('.dt-unit-price-domestic').hide();
		$('.dt-unit-price-export').show();

		$('.dt-amount-domestic').hide();
		$('.dt-amount-export').show();

		$('.dt-balance-domestic').hide();
		$('.dt-balance-export').show();
	}
}

//*** OLD */
function renderDeliveryProductListTableByOrderType() {
	var order_type = $('input[name=order_type]:checked').val();
	order_type = order_type ? order_type : 1; //default = domestic

	if (order_type == 1) {
		$('.dv-domestic-price').show();
		$('.dv-export-price').hide();

		$('.dv-domestic-amount').show();
		$('.dv-export-amount').hide();
	} else {
		$('.dv-domestic-price').hide();
		$('.dv-export-price').show();

		$('.dv-domestic-amount').hide();
		$('.dv-export-amount').show();
	}
}

function onEditOrderProduct(product_id) {
	var dataitem = getDataitem(product_id);

	var dt_product_seq = $(dataitem).find('.dt-product-seq').val();
	var dt_product_id = $(dataitem).find('.dt-product-id').val();
	var dt_dataitem_type = $(dataitem).find('.dt-dataitem-type').val();
	var dt_product_desc = $(dataitem).find('.dt-product-desc').text();
	var dt_quantity = $(dataitem).find('.dt-quantity').text();
	var dt_unit_price = $(dataitem).find('.dt-unit-price').text();
	var dt_amount = $(dataitem).find('.dt-amount').text();
	var dt_balance = $(dataitem).find('.dt-balance').text();
	var invoice_quantity = $(dataitem).find('.dt-invoice-quantity').val();
	var dt_price_flag = $(dataitem).find('.dt-price-flag').val();
	var dt_invoice_type_name = $(dataitem).find('.dt-invoice-type-name').val();
	var dt_invoice_type_phase = $(dataitem).find('.dt-invoice-type-phase').val();

	if (formatCurrencyToNumber(dt_quantity) < parseFloat(invoice_quantity)) {
		showMessageModal('Quantity must be more than ' + formatCurrency(invoice_quantity, 0));
		dt_quantity = formatCurrency(invoice_quantity, 0);
		$(dataitem).find('.dt-quantity').text(dt_quantity);
	}

	dt_quantity = formatCurrency(dt_quantity, 0);

	if (dt_dataitem_type == 'DATA') {
		deleteDataitemHidden('EDIT', dt_product_id);
		createDataitemHidden('EDIT', dt_product_seq, dt_product_id, dt_product_desc, dt_quantity, dt_unit_price, dt_amount, dt_balance, invoice_quantity, dt_price_flag, dt_invoice_type_name, dt_invoice_type_phase);
	} else if (dt_dataitem_type == 'NEW') {
		deleteDataitemHidden('NEW', dt_product_id);
		createDataitemHidden('NEW', dt_product_seq, dt_product_id, dt_product_desc, dt_quantity, dt_unit_price, dt_amount, dt_balance, invoice_quantity, dt_price_flag, dt_invoice_type_name, dt_invoice_type_phase);
	}

	calculateDataitems();
	calculateSummary();
}

function getDataitem(product_id) {
	var dataitems = $('#productListTable .dataitem');

	if (dataitems) {
		for (var i = 0; i < dataitems.length; i++) {
			var dt_product_id = $(dataitems[i]).find('.dt-product-id').val();
			if (dt_product_id == product_id) {
				return dataitems[i];
			}
		}
	}

	return null;
}

function clearDataitemsAndHiddens() {
	$('#productListTable .dataitem').remove();
	$('.dataitem-hidden').remove();
}

function calculateSummary() {
	var dataitems = $('#productListTable .dataitem');
	var vat = parseFloatOrNumeric($('#sm_vat').text());
	var sum_amount = 0;

	for (var i = 0; i < dataitems.length; i++) {
		var amount = formatCurrencyToNumber($(dataitems[i]).find('.dt-amount').text());
		sum_amount += amount;
	}

	var vat_price = (vat / 100) * sum_amount;
	var total = sum_amount + vat_price;

	$('#sm_amount').text(formatCurrency(sum_amount, 2));
	$('#sm_vat_price').text(formatCurrency(vat_price, 2));
	$('#sm_total').text(formatCurrency(total, 2));
}

function check_order_id() {
	var order_id = $('#order_id').val();
	var invoice_type = $('input[name=\'invoice_group\']:checked').val() == 'S' ? $('#invoice_type').val() : '';
	var temp_order_id = $('#temp_order_id').val();
	var temp_invoice_type = $('#temp_invoice_type').val();

	console.log($('#base_url').val() + 'order/has_data_ajax?order_id=' + encodeString(order_id) + '&invoice_type=' + encodeString(invoice_type));

	if ((order_id != '' && order_id != temp_order_id) || (invoice_type != temp_invoice_type)) {
		$.ajax({
			url: $('#base_url').val() + 'order/has_data_ajax?order_id=' + encodeString(order_id) + '&invoice_type=' + encodeString(invoice_type),
			type: 'GET',
			success: function(resp) {
				if (resp.success) {
					if (resp.has_data) {
						$('#order_id_err').html('&nbsp;&nbsp;Order ID already in use.');
					} else {
						$('#order_id_err').html('');
					}
				}
				checkDisableButton();
			}
		});
	} else {
		$('#order_id_err').html('');
		checkDisableButton();
	}
}

function checkDisableButton() {
	var order_id = $('#order_id').val();
	var temp_order_id = $('#temp_order_id').val();
	var order_id_err = $('#order_id_err').html().trim();

	if (order_id != '' && order_id_err == '') {
		$('#btn_create').removeClass('disabled');
	} else {
		$('#btn_create').addClass('disabled');
	}
}

function cancelOrder() {
	var order_id = encodeString($('#order_id').val());
	localStorage.setItem('order_id', order_id);
	$('#confirmCancelModal').modal('show');
}

function confirmCancelOrder() {
	var order_id = localStorage.getItem('order_id');
	var form = $('#form');
	var action = $('#base_url').val() + 'order/cancel/' + order_id;
	$(form).attr('action', action);
	$(form).submit();
}