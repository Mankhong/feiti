$(document).ready(function () {

    console.log("ready i report sum");
    $("#div_excel").hide();
//    var s = '10/02/2019';
//    var date = s.substring(0, 2);
//    var month = s.substring(3, 5);
//    var year = s.substring(6, 10);


    $("#dateTimeStart").datepicker({
                    format: 'dd/mm/yyyy',
                    autoclose: true,
                    orientation: "bottom auto",
                    todayHighlight: true,
                    todayBtn: true,
                    language: 'EN', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                    thaiyear: false, //Set เป็นปี พ.ศ.
        }).datepicker("setDate");
    $("#dateTimeEnd").datepicker({
                    format: 'dd/mm/yyyy',
                    autoclose: true,
                    orientation: "bottom auto",
                    todayHighlight: true,
                    todayBtn: true,
                    language: 'EN', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                    thaiyear: false, //Set เป็นปี พ.ศ.
        }).datepicker("setDate");
    //loadInitTable();
    var col4 = "Unit Price \n Domestic \n (B)";
    $("#col4").html(col4);

//	var type_page = $("#invoicetype").val();
//	console.log("type_page: " + type_page);
//	if(type_page === '' || type_page === null){
//		$("#rate").attr("disabled", true);
//	}else{
//		$("#rate").attr("disabled", false);
//	}

});



//onchange
$("#invoicetype").change(function () {
    var type_page = $("#invoicetype option:selected").attr("name");
    console.log("type_page: " + type_page)
    if (type_page === 'EN' || type_page === 'MD.EX' || type_page === 'EP' || type_page === 'ET' || type_page === 'ES' || type_page === 'VE' || type_page === undefined) {
        $("#rate").attr("disabled", false);
    } else {
        $("#rate").attr("disabled", true);
        $("#rate").val("");
    }
});

function process(date) {
    var parts = date.split("/");
    return new Date(parts[2], parts[1] - 1, parts[0]);
}

function numberOnly(id) {
    var element = document.getElementById(id);
    var regex = /[^0-9]/gi;
    element.value = element.value.replace(regex, "");
}

function floatOnly(id) {
    var element = document.getElementById(id);
    var regex = /[^0-9.]/gi;
    element.value = element.value.replace(regex, "");
}

$("#btn_search").click(function () {


    try {

        var type_page = $("#invoicetype option:selected").attr("name");
        var _invoicetype = '';
        if (type_page === undefined) {
            _invoicetype = 'All';
        } else {
            _invoicetype = type_page;
        }

        console.log("_invoicetype : " + _invoicetype);
        console.log("### start btn search ###");
        var startDate = $("#dateTimeStart").val();
        var dateTimeEnd = $("#dateTimeEnd").val();
        var customerId = $("#customerId").val();
        var invoiceId = $("#invoiceid").val();
        var invoicetype = $("#invoicetype").val();
        var pathno = $("#pathno").val();
        var pathname = $("#pathname").val();
        var rate = $("#rate").val();



        if ((startDate === null || startDate === '') &&
                (dateTimeEnd === null || dateTimeEnd === '') &&
                (customerId === null || customerId === '') &&
                (invoiceId === null || invoiceId === '') &&
                (invoicetype === null || invoicetype === '') &&
                (pathno === null || pathno === '') &&
                (pathname === null || pathname === '')) {

            notfoundData();
            $('#msgModalMessage').html("กรุณาระบุเงื่อนไขการค้นหา");
            $('#msgModal').modal('show');
        } else {
            //console.log("startDate: " + startDate);
            //console.log("dateTimeEnd: " + dateTimeEnd);

            var search = false;
            if ((startDate !== null && startDate !== '') && (dateTimeEnd !== null && dateTimeEnd !== '')) {

                if (process(startDate) > process(dateTimeEnd)) {
                    console.log("yyy");
                    search = false;
                    $('#msgModalMessage').html("กรุณาระบุวันที่ค้นให้ถูกต้อง");
                    $('#msgModal').modal('show');
                    notfoundData();
                } else {
                    
                    search = true;
                }

            }
            if (_invoicetype === 'EN' || type_page === 'MD.EX' || type_page === 'EP' || type_page === 'ET'|| _invoicetype === 'VE' || _invoicetype === 'All') {
                var input_rate = $('#rate').val();
                
                
                console.log("input_rate.length: " + input_rate.length);
                var myarr = ["0", "00.", "000.","0000.","00000.",
                             ".0", "..0" ,"...0", "....0", "....0", 
                             "0.", "0..","0..." ,"0....", "0.....",
                             "0..0", "0...0", "0....0", "0.....0",
                             "0.0.", "0.0.0.", "0.0.0.0." ,"0.0.0.0.0.",
                             "0.0..", "0.0...", "0.0....", "0.0.....",
                             "00","000","0000","00000","000000","0000000","00000000","000000000",
                             "00.00", "000.000", "0000.0000", "0000.0000","00000.00000"];
                
                if (jQuery.inArray(input_rate, myarr)!== -1) {
                    $('#msgModalMessage').html("กรุณาระบุ Rate ให้ถูกต้อง");
                    $('#msgModal').modal('show');
                    notfoundData();
                    search = false;
                    
                }else {
                    search = true;
                }
            } else {
                search = true;
            }

            console.log("search: " + search);
            if (search) {

                $("#loadMe").modal({
                    backdrop: "static", //remove ability to close modal with click
                    keyboard: false, //remove option to close with keyboard
                    show: true //Display loader!
                });

                var type_page = $("#invoicetype option:selected").attr("name");

                var rate_input = '';
                var rate_display = '';
                if (_invoicetype === 'EN' || type_page === 'MD.EX' || type_page === 'EP' || type_page === 'ET' || type_page === 'VE' || _invoicetype === 'All') {
                    if (rate === null || rate === '') {
                        rate_input = '1';
                    } else {
                        var myarr = ["0.0", "0.00", "0.000","0.0000","0.00000"];
                
                            if (jQuery.inArray(input_rate, myarr)!== -1) {
                                rate_input = '1';

                            }else{
                                rate_input = rate;
                            }
                            
                    }
                } else {

                    if (rate === null || rate === '') {
                        rate_input = '1';
                    } else {
                        rate_input = rate;
                    }

                }

                if (rate === null || rate === '') {
                    rate_display = '';
                } else {
                    rate_display = rate;
                }

                console.log("rate_input: " + rate_input);
                console.log("rate_display: " + rate_display);

                var xhr = $.ajax({
                    url: $('#base_url').val() + "report/get_product_list_ajax",
                    type: 'POST',
                    data: {startDate: startDate,
                        dateTimeEnd: dateTimeEnd,
                        customerId: customerId,
                        invoiceId: invoiceId,
                        invoice_type: invoicetype,
                        pathno: pathno,
                        pathname: pathname,
                        rate: rate_input,
                        input_rate: rate_display
                    },
                    async: true,
                    dataType: 'json',
                    success: function (data) {

                        console.log("length data: " + data.data.length);
                        if (data.data.length === 0) {
                            closePop();
                            notfoundData();
                            $("#div_excel").hide();

                        } else {
                            $("#div_excel").show();



                            var cusArr = [];
                            var invoiceArr = [];
                            var cusArrIndex = 0;
                            var invoiceArrIndex = 0;
                            var invoicePhaseArr = [];
                            var flagPriceArr = [];
                            var invoicePhaseArrIndex = 0;
                            var flagPriceArrIndex = 0;
                            for (var i = 0; i < data.data.length; i++) {
                                //if (data.data[i].invoice_type_name !== null || data.data[i].invoice_type_name !== '') {
                                //console.log(" data.data invoice_type_name: " + data.data[i].invoice_type_name);
                                //cusAll
                                if (!isDupItem(data.data[i].customer_name, cusArr)) {
                                    cusArr[cusArrIndex++] = data.data[i].customer_name;
                                }
                                //cusInvoice
                                if (!isDupItem(data.data[i].invoice_type_name, invoiceArr)) {
                                    invoiceArr[invoiceArrIndex++] = data.data[i].invoice_type_name;
                                    invoicePhaseArr[invoicePhaseArrIndex++] = (data.data[i].phase === null || data.data[i].phase === '') ? '' : data.data[i].phase;
                                    flagPriceArr[flagPriceArrIndex++] = (data.data[i].unit_type === null || data.data[i].unit_type === '') ? '' : data.data[i].unit_type;
                                }
                                //}
                            }


                            var cusDataArrPain = [];
                            var index = 0;
                            var ii = 0;
                            var jj = 0;
                            //type
                            var typeArr = [];
                            var phaseArr = [];
                            var priceFlagArr = [];
                            //console.log(invoiceArr);
                            for (var i = 0; i < invoiceArr.length; i++) {
                                index = 0;
                                //if(invoiceArr[i] != null){
                                typeArr.push(invoiceArr[i]);
                                phaseArr.push(invoicePhaseArr[i]);
                                flagPriceArr.push(flagPriceArr[i]);
                                //}

                                for (var j = 0; j < cusArr.length; j++) {
                                    //cusDataArrPain[ii][jj] = [];
                                    for (var k = 0; k < data.data.length; k++) {
                                        if (data.data[k].invoice_type_name == invoiceArr[i] && data.data[k].customer_name == cusArr[j]) {
                                            console.log("data.data[k].invoice_type_name: " + data.data[k].invoice_type_name);
                                            if (!cusDataArrPain[ii]) {
                                                cusDataArrPain[ii] = [];
                                            }
                                            if (!cusDataArrPain[ii][jj]) {
                                                cusDataArrPain[ii][jj] = [];
                                            }
                                            cusDataArrPain[ii][jj][index++] = data.data[k];
                                        }

                                    }
                                    if (cusDataArrPain[ii] && cusDataArrPain[ii][jj] && cusDataArrPain[ii][jj].length > 0) {
                                        jj++;
                                    }

                                }
                                if (cusDataArrPain[ii] && cusDataArrPain[ii].length > 0) {
                                    ii++;
                                }
                            }

                            /*
                             //grantotal
                             var domestic_price_all = 0;
                             var export_price_all = 0;
                             var quantity_domestic_all = 0;
                             var quantity_export_all = 0;
                             var amount_domestic_all = 0;
                             var amount_export_all = 0;
                             
                             for (var i = 0; i < data.data.length; i++) {
                             if (data.data[i] === undefined) {
                             continue;
                             }
                             domestic_price_all += formatCurrencyToNumber((data.data[i].price_flag === 1) ? data.data[i].unit_price : 0);
                             export_price_all += formatCurrencyToNumber((data.data[i].price_flag === 2) ? data.data[i].unit_price : 0);
                             quantity_domestic_all += formatCurrencyToNumber((data.data[i].price_flag == 1 ? data.data[i].invoice_quantity : 0));
                             quantity_export_all += formatCurrencyToNumber((data.data[i].price_flag == 2 ? data.data[i].invoice_quantity : 0));
                             
                             amount_domestic_all += data.data[i].price_flag === 1 ? formatCurrencyToNumber((data.data[i].unit_price * data.data[i].invoice_quantity)) : 0;
                             amount_export_all += data.data[i].price_flag === 2 ? formatCurrencyToNumber((data.data[i].unit_price * data.data[i].invoice_quantity)) : 0;
                             
                             //                                amount_domestic_all += (data.data[i].price_flag === 1 ? formatCurrencyToNumber(roundup(data.data[i].unit_price * data.data[i].invoice_quantity,2)) : 0 );
                             //                                amount_export_all += (data.data[i].price_flag === 2 ? formatCurrencyToNumber(roundup(data.data[i].unit_price * data.data[i].invoice_quantity,2)) : 0 );
                             
                             //console.log("amount_domestic_all: " + formatCurrencyToNumber(roundup(data.data[i].unit_price * data.data[i].invoice_quantity,2)));
                             //console.log("amount_domestic_all_x: "+ amount_domestic_all);
                             
                             //console.log("amount_export_all: " + formatCurrencyToNumber(roundup(data.data[i].unit_price * data.data[i].invoice_quantity,2)));
                             //console.log("amount_export_all_x: "+ amount_export_all);
                             
                             }
                             
                             var granTotal = '<table class="table table-condensed table-bordered table-font cus-table" style="width: 100%;">\n\
                             <tr>\n\
                             <td colspan="3" style=" width: 697px; background-color: #68d46a; font-weight: bold;">Grand Total</td>\n\
                             <td class="text-right" style="width: 94px; background-color: #68d46a; font-weight: bold;">' + formatCurrency(domestic_price_all, 4) + '</td>\n\
                             <td class="text-right" style="width: 93px; background-color: #68d46a; font-weight: bold;">' + formatCurrency(export_price_all, 4) + '</td>\n\
                             <td class="text-right" style="width: 59px; background-color: #68d46a; font-weight: bold;"></td>\n\
                             <td class="text-right" style="width: 94px; background-color: #68d46a; font-weight: bold;">' + formatCurrency(quantity_domestic_all, 0) + '</td>\n\
                             <td class="text-right" style="width: 94px; background-color: #68d46a; font-weight: bold;">' + formatCurrency(quantity_export_all, 0) + '</td>\n\
                             <td class="text-right" style="width: 93px; background-color: #68d46a; font-weight: bold;">' + formatCurrency(amount_domestic_all, 2) + '</td>\n\
                             <td class="text-right" style="width: 94px; background-color: #68d46a; font-weight: bold;">' + formatCurrency(amount_export_all, 2) + '</td>\n\
                             </tr>\n\
                             </table><br>';
                             */
                            try {

                                var html = setContent(cusDataArrPain, rate_display, typeArr, phaseArr, startDate, dateTimeEnd, flagPriceArr);
                                //var result = html.substr(0, html.length - 4);
                                $("#rs_table").html(html);

                                console.log("Finish");
                                $("#loadMe").modal("hide");
                                closePop();
                            } catch (e) {
                                console.log("error sub");
                            } finally {
                                $("#loadMe").modal("hide");

                            }
                        }
                    }

                });
                return false;
            }



        }

    } catch (e) {
        console.log("Error" + e);
    } finally {
        console.log("finally");
        $("#loadMe").modal("hide");
    }


});

function closePop() {
    console.log("Start colse popup");
    $("#loadMe").modal("hide");
    setTimeout(function () {
        $("#loadMe").modal("hide");
    }, 3000);

}

//'rs_table', 'Export HTML Table to Excel'
$("#btn_excel").click(function () {

    var type_page = $("#invoicetype option:selected").attr("name");
    var invoicetype = '';
    if (type_page === undefined) {
        invoicetype = '_All'
    } else {
        invoicetype = "_" + type_page;
    }

    var file_name = "SaleRepoertBOI_" + getFormattedDate(new Date()) + invoicetype + ".xls";
    //$('.table-bordered td').css('border', '0px solid black');

    $("#rs_table").table2excel({
        filename: file_name

    });
    //tableToExcel('rs_table', 'Export HTML Table to Excel');
    //$("#rs_table").tableToExcel({});
    $('.handle').css('border', '1px solid black');
    $('.handle').css('border-color', '#dee2e6');

    $('.handle3').css('border', '1px solid black');
    $('.handle3').css('border-color', '#dee2e6');

    $('.handle4').css('border-top', '1px solid black');
    $('.handle4').css('border-color', '#dee2e6');
    //$('.table-bordered td').css('border', '1px solid black');
});


function getFormattedDate(date) {
    let year = date.getFullYear();
    let month = (1 + date.getMonth()).toString().padStart(2, '0');
    let day = date.getDate().toString().padStart(2, '0');

    return year + '' + month + '' + day;
}

function isDupItem(item, arrayCus) {
    for (var i = 0; i < arrayCus.length; i++) {
        if (arrayCus[i] === item) {
            return true;
        }
    }
    return false;
}



function setContent(data, rate, typeArr, phaseArr, startDate, dateTimeEnd, flagPriceArr) {
    console.log('data', data);
    var html = '';
    var cusdata;
    var domestic_price_all = 0;
    var export_price_all = 0;
    var quantity_domestic_all = 0;
    var quantity_export_all = 0;
    var amount_domestic_all = 0;
    var amount_export_all = 0;
    var check = 0;

    var count_head = 0;
    var span = '<span class="text-center">Finaly By.....................................</span> &nbsp;&nbsp;\n\
                <span class="text-center">Account check By.....................................</span> &nbsp;&nbsp;\n\
                <span class="text-center">Check By.....................................</span> &nbsp;&nbsp;\n\
                <span class="text-center">Pepared By.....................................</span>&nbsp;&nbsp;';
    var _span = '';

    var _flagType = '';
    for (var i = 0; i < data.length; i++) {
        if (data[i] === undefined) {
            continue;
        }

        console.log("######flagPriceArr[i]: " + flagPriceArr[i]);
        if (flagPriceArr[i] == '1') {
            _flagType = '(B)';
        } else {
            _flagType = '(USB)';
        }
//type_page = $("#invoicetype option:selected").attr("name");
        count_head++;
        var tr = '';
        if (count_head <= 1) {
            _span = span;
            tr = '<tr>\n\
                         <th colspan="10" class="text-left handle" style="width: 700px; height: 45px; font-weight: bold; border: 0px solid black; vertical-align: middle; font-size: 17px;" colspan="7"><span class="text-left">' + _span + '</span></th>\n\
                  </tr>';

        } else {

            _span = '';
        }
        html += '<table class="table table-condensed table-bordered table-font table-border handle3"  style="width: 100%; margin-bottom: 0px;" border="0">\n\
                        <thead>\n\
                                <tr>\n\
                                    <th class="handle2" style="border-left: 0px; border-right: 0px;"></th>\n\
                                    <th class="text-center handle2" colspan="2" style="width: 700px; font-weight: bold; border: 0px solid #dee2e6;; text-align: center; vertical-align: middle; font-size: 22px; height: 45px;" colspan="2"><span class="text-center">SALES REPORT FOR BOI</span></th>\n\
                                    <th class="handle2" style="border: 0px solid #dee2e6;"></th>\n\
                                    <th class="handle2" style="border: 0px solid #dee2e6;"></th>\n\
                                    <th class="handle2" style="border: 0px solid #dee2e6;"></th>\n\
                                    <th class="handle2" style="border: 0px solid #dee2e6;"></th>\n\
                                    <th class="handle2" style="border: 0px solid #dee2e6;"></th>\n\
                                    <th class="handle2" style="border: 0px solid #dee2e6;"></th>\n\
                                    <th class="handle2" style="border: 0px solid #dee2e6;"></th>\n\
                                </tr>\n\
                                <tr>\n\
                                    <th class="handle4" style="border-right: 0px; border-top: 1px;"></th>\n\
                                    <th class="handle4" style="border-left: 0px; border-right: 0px;"></th>\n\
                                    <th class="handle4" style="border-left: 0px; border-right: 0px;"></th>\n\
                                    <th class="text-right handle4" colspan="7" style="text-align: right; font-weight: bold; vertical-align: middle; font-size: 16px;">Start Date ' + startDate + ' ' + typeArr[i] + '</th>\n\
                                </tr>\n\
                                <tr>\n\
\n\                                 <th class="handle4" style="border-right: 0px;"></th>\n\
                                    <th class="handle4" style="border-left: 0px; border-right: 0px;"></th>\n\
                                    <th class="handle4" style="border-left: 0px; border-right: 0px;"></th>\n\
                                    <th class="text-right handle4" colspan="7" style="text-align: right; font-weight: bold; vertical-align: middle; font-size: 16px;">End Date ' + dateTimeEnd + '</th>\n\
                                </tr>\n\
                                ' + tr + '\n\
                         </thead>\n\
                    </table>';


        for (var j = 0; j < data[i].length; j++) {
            if (!data[i][j]) {
                continue;
            }
            check++;
//console.log(j);
//if (j == 0) {
//console.log(data[i][j].length);
//console.log(data[i][j][0]);
//    html += '<div>Type: ' + data[i][j][0] + '</div>';
//}


//if (i == 0) {
            if (check > 1) {
                html += '<table>\n\
                            <tr>\n\
                                <th colspan="10" style="height: 40px; border: 0px solid black;></th>\n\
                            </tr>\n\
                            <tr>\n\
                                <th style="border: 0px solid black;"></th>\n\
                            </tr>\n\
                       </table>';
            }
            html += '<table class="table table-condensed table-bordered table-font" style="width: 100%; margin-bottom: 0px; " border="0">\n\
                            <thead>\n\
                                <tr>\n\
                                    <td class="text-center" style="width:300px; background-color: #f0f8ff; font-weight: bold; text-align: center; vertical-align: middle;">Customer</td>\n\
                                    <td class="text-center" style="width:200px; background-color: #f0f8ff; font-weight: bold; text-align: center; vertical-align: middle;">Part no.</td>\n\
                                    <td class="text-center" style="width:300px; background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Part name</td>\n\
                                    <td class="text-center" style="width:80px; background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Unit Price <br> Domestic <br> (B)</td>\n\
                                    <td class="text-center" style="width:80px; background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Unit Price <br> Export <br>' + _flagType + '</td>\n\
                                    <td class="text-center" style="width:50px; background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Rate </td>\n\
                                    <td class="text-center" style="width:118px; background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Quantity <br>Domestic <br>(PCS)</td>\n\
                                    <td class="text-center" style="width:118px; background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Quantity <br>Export <br>(PCS)</td>\n\
                                    <td class="text-center" style="width:118px; background-color: #f0f8ff; font-weight: bold; text-align: center; vertical-align: middle;">Amount <br>Domestic <br>(B)</td>\n\
                                    <td class="text-center" style="width:118px; background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Amount <br>Export <br>(B)</td>\n\
                                </tr>\n\
                            </thead>\n\
                         <tbody>';


            cusdata = data[i][j];
            html += setTDContent(cusdata, rate);
            html += '</tbody>\n\
                </table>';
            //}
            domestic_price_all += sumAllFunction(cusdata, 1);
            export_price_all += sumAllFunction(cusdata, 2);
            quantity_domestic_all += sumAllFunction(cusdata, 3);
            quantity_export_all += sumAllFunction(cusdata, 4);
            amount_domestic_all += sumAllFunction(cusdata, 5);
            amount_export_all += sumAllFunction(cusdata, 6);
        }
        check = 0;
        html += '<table class="table table-condensed table-bordered table-font table-border" style="width: 100%;">\n\
                    <tr>\n\
                        <td class="text-right" colspan="6" style="width: 881px; background-color: #fcd5b4; font-weight: bold; text-align: right;  height: 35px; font-size: 13px;">Sub Total</td>\n\
                        <td class="text-right" style="width: 115px; background-color: #fcd5b4; font-weight: bold; text-align: right;  height: 35px;">&nbsp;' + formatCurrency(quantity_domestic_all, 0) + '</td>\n\
                        <td class="text-right" style="width: 115px; background-color: #fcd5b4; font-weight: bold; text-align: right;  height: 35px;">&nbsp;' + formatCurrency(quantity_export_all, 0) + '</td>\n\
                        <td class="text-right" style="width: 115px; background-color: #fcd5b4; font-weight: bold; text-align: right;  height: 35px;">&nbsp;' + formatCurrency(amount_domestic_all, 2) + '</td>\n\
                        <td class="text-right" style="width: 114px; background-color: #fcd5b4; font-weight: bold; text-align: right;  height: 35px;">&nbsp;' + formatCurrency(amount_export_all, 2) + '</td>\n\
                     </tr>\n\
                     <tr>\n\
                        <td colspan="10" style="border: 0px solid #dee2e6;"></td>\n\
                     </tr>\n\
                </table>';
        domestic_price_all = 0;
        export_price_all = 0;
        quantity_domestic_all = 0;
        quantity_export_all = 0;
        amount_domestic_all = 0;
        amount_export_all = 0;
    }

    return html;
}

function setTDContent(data, rate) {
    var html = '';
    //console.log("data: " + data.length);
    var sumCulom1 = 0;
    var sumCulom2 = 0;
    var sumCulom3 = 0;
    var sumCulom4 = 0;
    var sumCulom5 = 0;
    var sumCulom6 = 0;
    var sumCulom7 = 0;

    if (data) {

        var padding_1 = 'padding: 5px;';
        var border = ''; //border: 1px solid black;
        var height = "height: 27px;";
        for (var i = 0; i < data.length; i++) {
//console.log(data[i]);
            if (data[i]) {

//                html += '<tr>\n\
//                            <td class="text-left" style="width: 195px; ' + padding_1 + border + height + '">' + data[i].customer_name + '</td>\n\
//                            <td class="text-left" style="width: 200px; ' + padding_1 + border + height + '">&nbsp;' + data[i].product_id.toString() + '</td>\n\
//                            <td class="text-left" style="width: 200px; ' + padding_1 + border + height + '">' + data[i].boi_desc + '</td>\n\
//                            <td class="text-right" style="text-align: right; width:118px; ' + padding_1 + border + height + '">&nbsp;' + (data[i].price_flag === 1 ? formatCurrency(data[i].unit_price, 4) : formatCurrency(0, 4)) + '</td>\n\
//                            <td class="text-right" style="text-align: right; width:118px; ' + padding_1 + border + height + '">&nbsp;' + (data[i].price_flag === 2 ? formatCurrency(data[i].unit_price, 4) : formatCurrency(0, 4)) + '</td>\n\
//                            <td class="text-right" style="text-align: right; width: 50px; ' + padding_1 + border + height + '">' + rate + '</td>\n\
//                            <td class="text-right" style="text-align: right; width:118px; ' + padding_1 + border + height + '">' + formatCurrency((data[i].price_flag == 1 ? data[i].invoice_quantity : formatCurrency(0, 4)), 0) + '</td>\n\
//                            <td class="text-right" style="text-align: right; width:118px; ' + padding_1 + border + height + '">' + formatCurrency((data[i].price_flag == 2 ? data[i].invoice_quantity : formatCurrency(0, 4)), 0) + '</td>\n\
//                            <td class="text-right" style="text-align: right; width:118px; ' + padding_1 + border + height + '">&nbsp;' + formatCurrency((data[i].price_flag == 1 ? (data[i].unit_price * data[i].invoice_quantity) : formatCurrency(0, 2)), 2) + '</td>\n\
//                            <td class="text-right" style="text-align: right; width:118px; ' + padding_1 + border + height + '">&nbsp;' + formatCurrency((data[i].price_flag == 2 ? (data[i].unit_price * data[i].invoice_quantity) : formatCurrency(0, 2)), 2) + '</td>\n\
//                         </tr>';

                //new
                html += '<tr>\n\
                            <td class="text-left" style="width: 195px; ' + padding_1 + border + height + '">' + data[i].customer_name + '</td>\n\
                            <td class="text-left" style="width: 200px; ' + padding_1 + border + height + '">&nbsp;' + data[i].product_id.toString() + '</td>\n\
                            <td class="text-left" style="width: 200px; ' + padding_1 + border + height + '">' + data[i].boi_desc + '</td>\n\
                            <td class="text-right" style="text-align: right; width:118px; ' + padding_1 + border + height + '">&nbsp;' + formatCurrency(data[i].unit_price_domestic, 4) + '</td>\n\
                            <td class="text-right" style="text-align: right; width:118px; ' + padding_1 + border + height + '">&nbsp;' + formatCurrency(data[i].unit_price_export, 4) + '</td>\n\
                            <td class="text-right" style="text-align: right; width: 50px; ' + padding_1 + border + height + '">' + data[i].input_rate + '</td>\n\
                            <td class="text-right" style="text-align: right; width:118px; ' + padding_1 + border + height + '">' + formatCurrency(data[i].invoice_quantity_domestic, 0) + '</td>\n\
                            <td class="text-right" style="text-align: right; width:118px; ' + padding_1 + border + height + '">' + formatCurrency(data[i].invoice_quantity_export, 0) + '</td>\n\
                            <td class="text-right" style="text-align: right; width:118px; ' + padding_1 + border + height + '">&nbsp;' + formatCurrency(data[i].amount_domestic, 2) + '</td>\n\
                            <td class="text-right" style="text-align: right; width:118px; ' + padding_1 + border + height + '">&nbsp;' + formatCurrency(data[i].amount_export, 2) + '</td>\n\
                         </tr>';

                sumCulom1 += data[i].price_flag === 1 ? formatCurrencyToNumber(data[i].unit_price) : 0;
                sumCulom2 += data[i].price_flag === 2 ? formatCurrencyToNumber(data[i].unit_price) : 0;
                sumCulom3 += formatCurrencyToNumber(rate);
//                sumCulom4 += formatCurrencyToNumber((data[i].price_flag == 1 ? data[i].invoice_quantity : 0));
//                sumCulom5 += formatCurrencyToNumber((data[i].price_flag == 2 ? data[i].invoice_quantity : 0));
//                sumCulom6 += data[i].price_flag == 1 ? formatCurrencyToNumber(formatCurrency((data[i].unit_price * data[i].invoice_quantity), 2)) : 0;
//                sumCulom7 += data[i].price_flag == 2 ? formatCurrencyToNumber(formatCurrency((data[i].unit_price * data[i].invoice_quantity), 2)) : 0;

                sumCulom4 += formatCurrencyToNumber(data[i].invoice_quantity_domestic);
                sumCulom5 += formatCurrencyToNumber(data[i].invoice_quantity_export);
                sumCulom6 += formatCurrencyToNumber(data[i].amount_domestic);
                sumCulom7 += formatCurrencyToNumber(data[i].amount_export);
            }
        }

        html += '<tr>\n\
                    <td class="text-right font-weight-bold" colspan="6" style="background-color: #fde9d9; font-weight: bold; text-align: right; height: 35px; font-size: 13px;">Total</td>\n\
                    <td class="text-right font-weight-bold" style="width: 116px; background-color: #fde9d9; font-weight: bold; text-align: right; height: 35px;">&nbsp;' + formatCurrency(sumCulom4, 0) + '</td>\n\
                    <td class="text-right font-weight-bold" style="width: 116px; background-color: #fde9d9; font-weight: bold; text-align: right;  height: 35px;">&nbsp;' + formatCurrency(sumCulom5, 0) + '</td>\n\
                    <td class="text-right font-weight-bold" style="width: 116px; background-color: #fde9d9; font-weight: bold; text-align: right;  height: 35px;">&nbsp;' + formatCurrency(sumCulom6, 2) + '</td>\n\
                    <td class="text-right font-weight-bold" style="width: 116px; background-color: #fde9d9; font-weight: bold; text-align: right; height: 35px;">&nbsp;' + formatCurrency(sumCulom7, 2) + '</td>\n\
                 </tr>';

        sumCulom1 = 0;
        sumCulom2 = 0;
        sumCulom3 = 0;
        sumCulom4 = 0;
        sumCulom5 = 0;
        sumCulom6 = 0;
        sumCulom7 = 0;
    }

    return html;
}

function sumAllFunction(data, colum) {

    var result = 0;
    if (data) {
        for (var i = 0; i < data.length; i++) {
            if (colum === 1) {
                if (data[i]) {
                    result += formatCurrencyToNumber(data[i].price_flag == 1 ? data[i].unit_price : 0);
                }
            }
            if (colum === 2) {
                if (data[i]) {
                    result += formatCurrencyToNumber(data[i].price_flag == 2 ? data[i].unit_price : 0);
                }
            }
            if (colum === 3) {
                if (data[i]) {
                    //result += formatCurrencyToNumber((data[i].price_flag == 1 ? data[i].invoice_quantity : 0));
                    result += formatCurrencyToNumber(data[i].invoice_quantity_domestic);
                }
            }
            if (colum === 4) {
                if (data[i]) {
                    //result += formatCurrencyToNumber((data[i].price_flag == 2 ? data[i].invoice_quantity : 0));
                    result += formatCurrencyToNumber(data[i].invoice_quantity_export);

                }
            }
            if (colum === 5) {
                if (data[i]) {
                    //result += data[i].price_flag == 1 ? formatCurrencyToNumber(formatCurrency((data[i].unit_price * data[i].invoice_quantity), 2)) : 0;
                    result += formatCurrencyToNumber(data[i].amount_domestic);
                }
            }
            if (colum === 6) {
                if (data[i]) {
                    //result += data[i].price_flag == 2 ? formatCurrencyToNumber(formatCurrency((data[i].unit_price * data[i].invoice_quantity), 2)) : 0;
                    result += formatCurrencyToNumber(data[i].amount_export);
                }
            }
        }
    }
    return result;
}

function notfoundData() {
    var tr = '<table class="table table-condensed table-bordered table-font cus-table" style="width: 100%;">\n\
                      <thead>\n\
                            <tr>\n\
                                <td class="text-center"><strong>Customer</strong></td>\n\
                                <td class="text-center"><strong>Part No</strong></td>\n\
                                <td class="text-center"><strong>Part Name</strong></td>\n\
                                <td class="text-center"><strong>Unit Price Domestic</strong></td>\n\
                                <td class="text-center"><strong>Unit Price Export</strong></td>\n\
                                <td class="text-center"><strong>Rate</strong></td>\n\
                                <td class="text-center"><strong>Quantity Domestic PCS</strong></td>\n\
                                <td class="text-center"><strong>Quantity Export PCS</strong></td>\n\
                                <td class="text-center"><strong>Amount Domestic</strong></td>\n\
                                <td class="text-center"><strong>Amount Export</strong></td>\n\
                            </tr>\n\
                        </thead>\n\
                        <tbody id="id-tbody">\n\
                            <tr><td colspan="11" class="text-center">ไม่พบข้อมูลการค้นหา</td></tr>\n\
                        </tbody>\n\
              </table>';
    $("#rs_table").html(tr);
}

$("#btn_clear").click(function () {

    console.log("### start btn clear ###");
    $("#dateTimeStart").val("");
    $("#dateTimeEnd").val("");
    $("#customerId").val("");
    $("#productId").val("");
    $("#invoiceid").val("");
    $("#invoicetype").val("");
    $("#pathno").val("");
    $("#pathname").val("");
    $("#rate").val("");
    $("#div_excel").hide();
    notfoundData();

}
);

