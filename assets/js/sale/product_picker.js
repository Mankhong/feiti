var ProductPicker = {
	showPicker: function (callbackFunctionName, customer_id) {
		$("#productPickerTable").dataTable().fnDestroy();
		$('#productPickerModal').modal('show');

		var t = $('#productPickerTable').DataTable({
			ajax: $('#base_url').val() + 'order/get_picker_products_ajax?customer_id=' + customer_id,
			searching: true,
			ordering: true,
			order: [
				[1, 'asc']
			],
			lengthChange: false,
			pageLength: 10,
			columns: [{
					'data': null,
					'render': function (data, type, row, meta) {
						return meta.row + 1;
					}
				},
				{
					'data': 'id'
				},
				{
					'data': 'desc1'
				},
				{
					'data': 'unit_price',
					'render': function (data, type, row, meta) {
                        return formatCurrency(data, 4);
                    }
				},
				{
					'data': 'price_flag',
					'render': function (data, type, row, meta) {
                        return data == '1' ? 'Local Price' : 'BOI Price';
                    }
				},
				/*
				{
					'data': 'domestic_price',
					'render': function (data, type, row, meta) {
						var price_type = ''; // row['unit_price_type_name'] ? ' (' + row['unit_price_type_name'] + ')' :  '';
						var price = formatCurrency(data)
                        return  price ? price + price_type : '';
                    }
				},
				{
					'data': 'export_price',
					'render': function (data, type, row, meta) {
                        var price_type = '';// row['unit_price_type_name'] ? ' (' + row['unit_price_type_name'] + ')' :  '';
						var price = formatCurrency(data)
                        return  price ? price + price_type : '';
                    }
				},
				{
                    'data': "id",
                    'render': function (data, type, row, meta) {
						var base_url = $('#base_url').val();
						var unit_price_type = row['unit_price_type'] ? row['unit_price_type'] : '';
						var unit_price_type_name = row['unit_price_type_name'] ? row['unit_price_type_name'] :  '';
						var invoice_quantity = row['invoice_quantity'];
						var product_id = encodeString(row['id']);
						
						return '<a class="btn btn-default btn-icon" onclick="' + callbackFunctionName + '(\'' + product_id + '\''
								+ ',\'' + strNotNull(row['desc1']) + '\''
								+ ',\'' + strNotNull(row['domestic_price']) + '\''
								+ ',\'' + strNotNull(row['export_price']) + '\''
								+ ',\'' + strNotNull(unit_price_type) + '\''
								+ ',\'' + strNotNull(unit_price_type_name) + '\''
								+ ',\'' + strNotNull(invoice_quantity) + '\''
								+ ',\'' + strNotNull(row['count_invoiced']) + '\''
								+ ')"><i class="fas fa-share-square"></i></a>';
                    }
				},*/
			],
			columnDefs: [{
					"targets": 0,
					"className": "text-center"
				},
				{
					"targets": 3,
					"className": "text-right"
				},
			],
			fnDrawCallback: function (settings) {
				$('input[aria-controls=productPickerTable]').focus();
				ProductPicker.setSearchEvent();
			},
		});

		//Sort without No.
		t.on('order.dt search.dt', function () {
			t.column(0, {
				search: 'applied',
				order: 'applied'
			}).nodes().each(function (cell, i) {
				cell.innerHTML = i + 1;
			});
		}).draw();

		//Onclick row.
		$('#productPickerTable tbody').unbind('click');
		$('#productPickerTable tbody').on('click', 'tr', function () {
			var t = $('#productPickerTable').DataTable();
			var data = t.row( this ).data();
			ProductPicker.selectedCallBack(data);
		});
	},

	hidePicker: function () {
		$('#productPickerModal').modal('hide');
	},

	setSearchEvent: function() {
		$('input[aria-controls=productPickerTable]').unbind('keydown');
		$('input[aria-controls=productPickerTable]').on('keydown', function(event){
			if (event.keyCode === 13) {
				event.preventDefault();
				var trs = $('#productPickerTable tbody tr');
				if (trs.length === 1 && !$(trs[0]).hasClass('dataTables_empty')) {
					$(trs[0]).click();
				}
			}
		});
	},

	selectedCallBack: function(data) {}
}