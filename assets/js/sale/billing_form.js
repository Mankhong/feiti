$(document).ready(function () {
	$('.customerPickerTrigger').on('click', function () {
		CustomerPicker.showPicker('selectedCustomer');
	});

	CustomerPicker.selectedCallBack = function (data) {
		selectedCustomer(data['id'], data['name'], data['customer_type'], data['address'], data['credit'], data['customer_type_name']);
	}

	$('.invoicePickerTrigger').on('click', function () {
		var customer_id = $('#customer_id').val();
		if (customer_id) {
			InvoicePicker.showPicker(customer_id);
		} else {
			showMessageModal("Customer ID is required, please select Customer first.");
		}
	});

	$('#confirmDeleteButton').on('click', function () {
		$('#confirmDeleteModal').modal('hide');
		var id = localStorage.getItem("delete_id");
		deleteDataitem(id);
		calculateSummary();
	});

	$('#billing_id').on('blur', function(){
		check_billing_id();
	});

	$('#receipt_id').on('blur', function(){
		check_receipt_id();
	});

	InvoicePicker.selectedCallBack = function (selected_items) {
		InvoicePicker.hidePicker();
		var foundDuplicateItem = false;
		if (selected_items) {
			for (var i = 0; i < selected_items.length; i++) {
				var invoice_id = $(selected_items[i]).attr('data-invoice-id');

				var item = {
					id: invoice_id,
					invoice_id: invoice_id,
					invoice_date: $(selected_items[i]).attr('data-invoice-date'),
					print_invoice_id: $(selected_items[i]).attr('data-print-invoice-id'),
					total_amount: $(selected_items[i]).attr('data-total-amount'),
					dataitem_type: 'NEW',
				}

				if (!hasDataitem(item)) {
					addDatatem(item);
					calculateSummary();
				} else {
					foundDuplicateItem = true;
				}
			}
		}

		if (foundDuplicateItem) {
			//showMessageModal('Duplicate Product found.');
		}
	}

	formatDataitemsTotalAmount();
	initDatepicker('.datepicker');
	checkDisableButton();
	calculateSummary();
});

function hasDataitem(item) {
	var dataitems = $('#dataitemTable tr.dataitem');
	for (var i = 0; i < dataitems.length; i++) {
		var dt_item = convertDataitemToItem(dataitems[i]);
		if (dt_item.invoice_id == item.invoice_id) {
			return true;
		}
	}

	return false;
}

function convertDataitemToItem(dataitem) {
	return {
		id: $(dataitem).find('.dt-id').val(),
		invoice_id: $(dataitem).find('.dt-invoice-id').val(),
		invoice_date: $(dataitem).find('.dt-invoice-date').val(),
		print_invoice_id: $(dataitem).find('.dt-print-invoice-id').val(),
		total_amount: $(dataitem).find('.dt-total-amount').val(),
		dataitem_type: $(dataitem).find('.dt-dataitem-type').val()
	}
}

function addDatatem(item) {
	$('#dataitemTable .dataitem-body').append(convertItemToDataitem(item));
	addInsertDataitemHidden(item);
}

function addInsertDataitemHidden(item) {
	var value = convertItemToValue(item);
	var hidden = '<input type="hidden" name="insert_dataitems[]" value="' + value + '"/>';
	$('#dataitem_hidden_container').append(hidden);
}

function addDeleteDataitemHidden(item) {
	var value = convertItemToValue(item);
	var hidden = '<input type="hidden" name="delete_dataitems[]" value="' + value + '"/>';
	$('#dataitem_hidden_container').append(hidden);
}

//-- Dataitem: id|invoice_id|invoice_date|print_invoice_id|total_amount|dataitem_type
function convertItemToValue(item) {
	var delimiter = '&88&';

	return item.id +
		delimiter +
		item.invoice_id +
		delimiter +
		item.invoice_date +
		delimiter +
		item.print_invoice_id +
		delimiter +
		item.total_amount +
		delimiter +
		item.dataitem_type;
}

function convertItemToDataitem(item) {
	return '<tr class="dataitem">' +
		'<td class="text-center">' +
		'<input type="hidden" class="dt-dataitem-type" value="' + item.dataitem_type + '"/>' +
		'<input type="hidden" class="dt-id" value="' + item.id + '"/>' +
		'<input type="hidden" class="dt-invoice-id" value="' + item.invoice_id + '"/>' +
		'<input type="hidden" class="dt-invoice-date" value="' + item.invoice_date + '"/>' +
		'<input type="hidden" class="dt-print-invoice-id" value="' + item.print_invoice_id + '"/>' +
		'<input type="hidden" class="dt-total-amount" value="' + item.total_amount + '"/>' +
		'<span class="dt-invoice-date-sp">' + item.invoice_date + '</span>' +
		'</td>' +
		'<td class="text-left"><span class="dt-invoice-id-sp">' + decodeString(item.invoice_id) + '</span></td>' +
		'<td class="text-left"><span class="dt-print-invoice-id-sp">' + item.print_invoice_id + '</span></td>' +
		'<td class="text-right"><span class="dt-total-amount-sp">' + formatCurrency(item.total_amount, 2) + '</span></td>' +
		'<td class="text-center"><a class="btn btn-default btn-icon" onclick="onclickDeleteDataitem(\'' + item.id + '\')"><i class="fas fa-times fa-sm"></i></a></td>' +
		'</tr>';
}

function convertValueToArray(value) {
	var delimiter = '&88&';
	return value.split(delimiter);
}

function selectedCustomer(customer_id, customer_name, customer_type, customer_address, customer_credit, customer_type_name) {
	resetDataitems();
	CustomerPicker.hidePicker();
	$('#customer_id').val(customer_id);
	$('#customer_name').val(customer_name);
	$('#customer_type').val(customer_type_name);
	$('#customer_address').text(customer_address);
	$('#customer_credit').val(formatCurrency(customer_credit), 2);
}

function resetDataitems() {
	var dataitems = $('#dataitemTable tr.dataitem');
	if (dataitems) {
		for (var i = 0; i < dataitems.length; i++) {
			var item = convertDataitemToItem(dataitems[i]);
			if (item.dataitem_type != 'DATA') {
				deleteDataitem(item.id);
				calculateSummary();
			}
		}
	}
}

function onclickDeleteDataitem(id) {
	localStorage.setItem("delete_id", id);
	$('#confirmDeleteModal').modal('show');
}

function deleteDataitem(id) {
	var dataitems = $('#dataitemTable tr.dataitem');
	for (var i = 0; i < dataitems.length; i++) {
		var item = convertDataitemToItem(dataitems[i]);
		if (item.id == id) {
			if (item.dataitem_type == 'DATA') {
				item.dataitem_type = 'DEL';
				addDeleteDataitemHidden(item);

			} else if (item.dataitem_type == 'NEW') {
				deleteInsertDataitemHidden(item.id);
			}

			$(dataitems[i]).remove();
			break;
		}
	}
}

function deleteInsertDataitemHidden(id) {
	var hiddens = $('input[name=insert_dataitems\\[\\]]');
	for (var i = 0; i < hiddens.length; i++) {
		var values = convertValueToArray(hiddens[i].value);
		if (values[0] == id) {
			$(hiddens[i]).remove();
			break;
		}
	}
}

function formatDataitemsTotalAmount() {
	var dataitems = $('#dataitemTable tr.dataitem');
	for (var i = 0; i < dataitems.length; i++) {
		var total_amount = formatCurrency($(dataitems[i]).find('.dt-total-amount').val());
		$(dataitems[i]).find('.dt-total-amount-sp').text(total_amount);
	}
}

function onclickSave() {
	$('#form').submit();
}

function check_billing_id() {
	var billing_id = $('#billing_id').val();
	var temp_billing_id = $('#temp_billing_id').val();
	
	if (billing_id != '' && billing_id != temp_billing_id) {
		$.ajax({
			url: $('#base_url').val() + 'billing/has_billing_ajax?billing_id=' + encodeString(billing_id),
			type: 'GET',
			success: function(resp) {
				if (resp.success) {
					if (resp.has_data) {
						$('#billing_id_arr').html('&nbsp;&nbsp;Billing ID already in use.');
					} else {
						$('#billing_id_arr').html('');
					}
				}
				checkDisableButton();
			}
		});
	} else {
		$('#billing_id_arr').html('');
	}

	checkDisableButton();
}

function check_receipt_id() {
	var receipt_id = $('#receipt_id').val();
	var temp_receipt_id = $('#temp_receipt_id').val();

	if (receipt_id != '' && receipt_id != temp_receipt_id) {
		$.ajax({
			url: $('#base_url').val() + 'billing/has_receipt_ajax?receipt_id=' + encodeString(receipt_id),
			type: 'GET',
			success: function(resp) {
				if (resp.success) {
					if (resp.has_data) {
						$('#receipt_id_arr').html('&nbsp;&nbsp;Receipt ID already in use.');
					} else {
						$('#receipt_id_arr').html('');
					}
				}
				checkDisableButton();
			}
		});
	} else {
		$('#receipt_id_arr').html('');
	}

	checkDisableButton();
}

function checkDisableButton() {
	var billing_id = $('#billing_id').val();
	var receipt_id = $('#receipt_id').val();
	var billing_id_err = $('#billing_id_arr').html().trim();
	var receipt_id_arr = $('#receipt_id_arr').html().trim();

	if (billing_id != '' && receipt_id != '' && billing_id_err == '' && receipt_id_arr == '') {
		$('#btn_create').removeClass('disabled');
	} else {
		$('#btn_create').addClass('disabled');
	}
}

function calculateSummary() {
	/*
	var dataitems = $('#dataitemTable .dataitem');
	var vat = parseFloatOrNumeric($('#sm_vat').text());
	var sum_amount = 0;
	var upt = '';

	for (var i = 0; i < dataitems.length; i++) {
		var amount = formatCurrencyToNumber($(dataitems[i]).find('.dt-total-amount-sp').text());
		sum_amount += amount;
	}

	var vat_price = (vat / 100) * sum_amount;
	var total = sum_amount + vat_price;

	$('#sm_amount').text(formatCurrency(sum_amount, 2) + upt);
	$('#sm_vat_price').text(formatCurrency(vat_price, 2) + upt);
	$('#sm_total').text(formatCurrency(total, 2) + upt);
	*/
}