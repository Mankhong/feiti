$(document).ready(function () {
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });

    $('.customerPickerTrigger').on('click', function () {
        CustomerPicker.showPicker('selectedCustomer');
    });

    CustomerPicker.selectedCallBack = function (data) {
        selectedCustomer(data['id'], data['name'], data['customer_type'], data['address'], data['credit'], data['customer_type_name']);
    }

    $('.orderProductPickerTrigger').on('click', function () {
        var customer_id = $('#customer_id').val();
        var invoice_type = $('#invoice_type').val();
        var invoice_group = getInvoiceGroup(invoice_type);

        if (customer_id != '') {


            $('#orderProductPickerModal').modal('show');
            $("#rs_div").hide();
            $(".order-product-picker-select-all").prop('disabled', true);
            $(".order-product-picker-select").prop('disabled', true);
            //OrderProductPicker.showPicker(customer_id, invoice_type, invoice_group);

            //$("#rs_div").show();
        } else {
            showMessageModal("Customer ID is required, please select Customer first.");
        }
    });

    $('#order_id').keypress(function (e) {
        if (e.which == 13) {
            if (onValidate()) {
                onSearch();
            }
        }
    });

    $('#product_id').keypress(function (e) {
        if (e.which == 13) {
            if (onValidate()) {
                onSearch();
            }
        }
    });

    $('#product_name').keypress(function (e) {
        if (e.which == 13) {
            onSearch();
        }
    });

    

    $("#btn_search").click(function () {
        if (onValidate()) {
            onSearch();
        }
    });

    function onSearch() {
        if (onValidate()) {
            var customer_id = $('#customer_id').val();
            var invoice_type = $('#invoice_type').val();
            var invoice_group = getInvoiceGroup(invoice_type);

            var order_id = $("#order_id").val();
            var product_id = $("#product_id").val();
            var product_name = $("#product_name").val();

            $("#rs_div").show();
            $(".order-product-picker-select-all").prop('disabled', false);
            $(".order-product-picker-select").prop('disabled', false);
            OrderProductPicker.showPicker(customer_id, invoice_type, invoice_group, order_id, product_id, product_name);
        }
    }
    
    function onValidate() {
        var order_id = $("#order_id").val();
        var product_id = $("#product_id").val();
        var product_name = $("#product_name").val();

        if (order_id === '' && product_id === '' && product_name === '') {
            var table = $('#orderProductPickerTable').DataTable();
            table.clear().draw();
            showMessageModal('กรุณาระบุเงื่อนไขการค้นหา');
            return false;
        }

        return true;
    }

    $("#btn_clear").click(function () {


        $("#order_id").val("");
        $("#product_id").val("");
        $("#product_name").val("");
        $("#rs_div").hide();
        $(".order-product-picker-select-all").prop('disabled', true);
        $(".order-product-picker-select").prop('disabled', true);
    });

    $("#btn_close_popup_title").on("click", function (s) {
        $("#msgModalPopUP").modal('hide');
    });

    $("#btn_close_popup").on("click", function (s) {
        $("#msgModalPopUP").modal('hide');
    });
    //END

    $('#confirmDeleteButton').on('click', function () {
        $('#confirmDeleteModal').modal('hide');
        var ivp_id = localStorage.getItem("delete_ivp_id");
        deleteDataitem(ivp_id);
        calculateSummaryInvoice();
    });

    $('#id_suffix').on('blur', function () {
        check_invoice_id();
    });

    $('#invoice_type').on('change', function () {
        check_invoice_id();
        renderReferInvoiceID();
    });

    $('#cancel_invoice_button').on('click', function () {
        cancelInvoice();
    });

    $('#refer_invoice_id').on('blur', function () {
        if (!validateReferInvoice()) {
            showMessageModal("กรุณาระบุ Refer Invoice ID ให้ถูกต้อง");
        }
    });

    OrderProductPicker.selectedCallBack = function (selected_items) {
        OrderProductPicker.hidePicker();
        var foundDuplicateItem = false;
        if (selected_items) {
            for (var i = 0; i < selected_items.length; i++) {
                var product_id = $(selected_items[i]).attr('data-product-id');
                var order_id = $(selected_items[i]).attr('data-order-id');
                var item = {
                    id: generateIvpID(product_id, order_id),
                    product_id: product_id,
                    product_seq: $(selected_items[i]).attr('data-product-seq'),
                    product_name: $(selected_items[i]).attr('data-product-name'),
                    unit_price: $(selected_items[i]).attr('data-unit-price'),
                    unit_price_type_id: $(selected_items[i]).attr('data-unit-price-type-id'),
                    unit_price_type_name: $(selected_items[i]).attr('data-unit-price-type-name'),
                    invoice_quantity: $(selected_items[i]).attr('data-balance'),
                    quantity: $(selected_items[i]).attr('data-quantity'),
                    balance: $(selected_items[i]).attr('data-balance'),
                    lot_size: $(selected_items[i]).attr('data-lot-size'),
                    lot_no: '',
                    order_id: $(selected_items[i]).attr('data-order-id'),
                    balance: $(selected_items[i]).attr('data-balance'),
                    temp_quantity: 0,
                    dataitem_type: 'NEW',
                }

                if (!hasDataitem(item)) {
                    addDatatem(item);
                } else {
                    foundDuplicateItem = true;
                }
            }
        }

        calculateAndDisplayDataitems();
        calculateSummaryInvoice();

        if (foundDuplicateItem) {
            showMessageModal('Duplicate Product found.');
        }
    }

    calculateAndDisplayDataitems();
    setDefaultInvoiceDate();
    calculateSummaryInvoice();
    setInternalNoteInputs();
    checkDisableButton();
});

function renderReferInvoiceID() {
	if (getInvoiceGroup($('#invoice_type').val()) == 'S') {
		$('#refer_invoice_id_container').show();
	} else {
		$('#refer_invoice_id_container').hide();
		$('#refer_invoice_id').val('');
	}
}

function getInvoiceGroup(invoice_type) {
	if ((parseFloatOrNumeric(invoice_type) >= 11 && parseFloatOrNumeric(invoice_type) < 18) || parseFloatOrNumeric(invoice_type) == 20) {
		return 'S';
	}
	return 'N';
}

function cancelInvoice() {
    var invoice_id = encodeString($('#invoice_id').val());
    localStorage.setItem('invoice_id', invoice_id);
    $('#confirmCancelModal').modal('show');
}

function confirmCancelInvoice() {
    var invoice_id = localStorage.getItem('invoice_id');
    var form = $('#form');
    var action = $('#base_url').val() + 'invoice/cancel/' + invoice_id;
    $(form).attr('action', action);
    $(form).submit();
}

function setInternalNoteInputs() {
    var itn = $('#internal_note').val();
    if (itn != '') {
        var spr_itn = itn.split(/\n/);
        for (var i = 0; i < 3; i++) {
            if (spr_itn[i] !== undefined) {
                $('#internal_note_' + (i + 1)).val(spr_itn[i]);
            } else {
                $('#internal_note_' + (i + 1)).val('');
            }
        }
    } else {
        $('#internal_note_1').val('');
        $('#internal_note_2').val('');
        $('#internal_note_3').val('');
    }
}

function setInternalNoteHidden() {
    var itn_1 = $('#internal_note_1').val();
    var itn_2 = $('#internal_note_2').val();
    var itn_3 = $('#internal_note_3').val();
    var itn = itn_1 +
            (itn_2 !== '' ? '\n' + itn_2 : '') +
            (itn_3 !== '' ? '\n' + itn_3 : '');

    $('#internal_note').val(itn);
}

function generateIvpID(product_id, order_id) {
    return 'T' + product_id + order_id;
}

function setDefaultInvoiceDate() {
    if ($('#invoice_date').val() === '') {
        $('#invoice_date').val(getCurrentDateDDMMYYYY());
    }
}

function selectedCustomer(customer_id, customer_name, customer_type, customer_address, customer_credit, customer_type_name) {
    resetDataitems();
    CustomerPicker.hidePicker();
    $('#customer_id').val(customer_id);
    $('#customer_name').val(customer_name);
    $('#customer_type').val(customer_type);
    $('#delivery_address').text(customer_address);
}

function resetDataitems() {
    var dataitems = $('#dataitemTable tr.dataitem');
    if (dataitems) {
        for (var i = 0; i < dataitems.length; i++) {
            var item = convertDataitemToItem(dataitems[i]);
            if (item.dataitem_type != 'DATA') {
                deleteDataitem(item.id);
            }
        }
    }
}

function addDatatem(item) {
    $('#dataitemTable .dataitem-body').append(convertItemToDataitem(item));
    addInsertDataitemHidden(item);
    createDefaultLotitemHidden(item);
}

function hasDataitem(item) {
    var dataitems = $('#dataitemTable tr.dataitem');
    if (dataitems) {
        for (var i = 0; i < dataitems.length; i++) {
            var dt_item = convertDataitemToItem(dataitems[i]);
            if (dt_item.product_id == item.product_id && dt_item.order_id == item.order_id) {
                return true;
            }
        }
    }

    return false;
}

function deleteDataitem(ivp_id) {
    var dataitems = $('#dataitemTable tr.dataitem');
    for (var i = 0; i < dataitems.length; i++) {
        var item = convertDataitemToItem(dataitems[i]);

        if (item.id == ivp_id) {
            if (item.dataitem_type == 'DATA') {
                item.dataitem_type = 'DEL';
                addDeleteDataitemHidden(item);

            } else if (item.dataitem_type == 'NEW') {
                deleteInsertDataitemHidden(item.id);
                clearLotitemHiddens(item.id);
            }

            $(dataitems[i]).remove();
            break;
        }
    }
}

function convertItemToDataitem(item) {
    return '<tr class="dataitem">' +
            '<td>' +
            '<input type="hidden" class="dt-dataitem-type" value="' + item.dataitem_type + '"/>' +
            '<input type="hidden" class="dt-id" value="' + item.id + '"/>' +
            '<input type="hidden" class="dt-lot-size" value="' + item.lot_size + '"/>' +
            '<input type="hidden" class="dt-quantity" value="' + item.quantity + '"/>' +
            '<input type="hidden" class="dt-balance" value="' + item.balance + '"/>' +
            '<input type="hidden" class="dt-temp-quantity" value="' + item.temp_quantity + '"/>' +
            '<input type="hidden" class="dt-product-id-hidden" value="' + item.product_id + '"/>' +
            '<input type="hidden" class="dt-product-seq-hidden" value="' + item.product_seq + '"/>' +
            '<input type="hidden" class="dt-order-id-hidden" value="' + item.order_id + '"/>' +
            '<span class="dt-order-id">' + decodeString(item.order_id) + '</span>' +
            '</td>' +
            '<td class="text-right"><span class="dt-product-id">' + decodeString(item.product_id) + '</span></td>' +
            '<td class="text-right"><span class="dt-product-name">' + item.product_name + '</span></td>' +
            '<td class="text-right"><span class="dt-lot-no">' + item.lot_no + '</span></td>' +
            '<td class="text-right"><span class="dt-invoice-quantity">' + item.invoice_quantity + '</span></td>' +
            '<td class="text-right"><span class="dt-unit-price">' + item.unit_price + '</span></td>' +
            '<td class="text-right"><span class="dt-amount"></span></td>' +
            '<td class="text-center"><a class="btn btn-default btn-icon" onclick="onclickEditProductPackage(\'' + item.id + '\')"><i class="fas fa-cube"></i></td>' +
            '<td class="text-center"><a class="btn btn-default btn-icon" onclick="onclickDeleteDataitem(\'' + item.id + '\')"><i class="fas fa-times fa-sm"></i></a></td>' +
            '</tr>';
}

function onclickDeleteDataitem(ivp_id) {
    localStorage.setItem("delete_ivp_id", ivp_id);
    $('#confirmDeleteModal').modal('show');
}

function convertDataitemToItem(dataitem) {
    return {
        id: $(dataitem).find('.dt-id').val(),
        product_id: $(dataitem).find('.dt-product-id-hidden').val(),
        product_seq: $(dataitem).find('.dt-product-seq-hidden').val(),
        product_name: $(dataitem).find('.dt-product-name').text(),
        unit_price: $(dataitem).find('.dt-unit-price').text(),
        invoice_quantity: $(dataitem).find('.dt-invoice-quantity').text(),
        quantity: $(dataitem).find('.dt-quantity').val(),
        balance: $(dataitem).find('.dt-balance').text(),
        lot_size: $(dataitem).find('.dt-lot-size').val(),
        lot_no: $(dataitem).find('.dt-lot-no').text(),
        order_id: $(dataitem).find('.dt-order-id-hidden').val(),
        balance: $(dataitem).find('.dt-balance').val(),
        temp_quantity: $(dataitem).find('.dt-temp-quantity').val(),
        dataitem_type: $(dataitem).find('.dt-dataitem-type').val()
    }
}

function addInsertDataitemHidden(item) {
    var value = convertItemToValue(item);
    var hidden = '<input type="hidden" name="insert_dataitems[]" value="' + value + '"/>';
    $('#dataitem_hidden_container').append(hidden);
}

function deleteInsertDataitemHidden(ivp_id) {
    var hiddens = $('input[name=insert_dataitems\\[\\]]');
    for (var i = 0; i < hiddens.length; i++) {
        var values = convertValueToArray(hiddens[i].value);
        if (values[0] == ivp_id) {
            $(hiddens[i]).remove();
            break;
        }
    }
}

function addDeleteDataitemHidden(item) {
    var value = convertItemToValue(item);
    var hidden = '<input type="hidden" name="delete_dataitems[]" value="' + value + '"/>';
    $('#dataitem_hidden_container').append(hidden);
}

function deleteDeleteDataitemHidden(product_id) {
    var hiddens = $('input[name=delete_dataitems\\[\\]]');
    if (hiddens) {
        for (var i = 0; i < hiddens.length; i++) {
            var values = convertValueToArray(hiddens[i].value);
            if (values[1] == product_id) {
                $(hiddens[i]).remove();
                break;
            }
        }
    }
}

//-- Dataitem: id|product_id|product_seq|product_name|invoice_quantity|lot_no|lot_size|unit_price|quantity|order_id|balance|temp_quantity|dataitem_type
function convertItemToValue(item) {
    var delimiter = '&88&';

    return item.id +
            delimiter +
            item.product_id +
            delimiter +
            item.product_seq +
            delimiter +
            item.product_name +
            delimiter +
            item.invoice_quantity +
            delimiter +
            item.lot_no +
            delimiter +
            item.lot_size +
            delimiter +
            item.unit_price +
            delimiter +
            item.quantity +
            delimiter +
            item.order_id +
            delimiter +
            item.balance +
            delimiter +
            item.temp_quantity +
            delimiter +
            item.dataitem_type;
}

function convertValueToArray(value) {
    var delimiter = '&88&';
    return value.split(delimiter);
}

function onclickEditProductPackage(ivp_id) {
    localStorage.setItem("ivp_id", ivp_id);

    var item = findItemFromDataitems(ivp_id);

    setProductPackageModalValues(item);
    //calculateProductPackageAmount();
    $('#productPackageModal').modal('show');
}

function onsaveProductPackage() {
    var ivp_id = localStorage.getItem("ivp_id");
    var quantity = formatCurrencyToNumber($('#productPackageModal #sum_ppkg_quantity').text());
    var lot_no_1 = $('#productPackageModal #ppkg_lot_no_1').val();
    var lot_no_2 = $('#productPackageModal #ppkg_lot_no_2').val();
    var lot_no_3 = $('#productPackageModal #ppkg_lot_no_3').val();
    var lot_no = lot_no_1 +
            (lot_no_2 !== '' ? '\n' + lot_no_2 : '') +
            (lot_no_3 !== '' ? '\n' + lot_no_3 : '');

    updateDataitemPackage(ivp_id, quantity, lot_no);
    generateLotitemHiddens(ivp_id);
    $('#productPackageModal').modal('hide');

    calculateAndDisplayDataitems();
    calculateSummaryInvoice();
}

function setProductPackageModalValues(item) {
    var lot_size = parseFloatOrEmpty(item.lot_size);
    var max_lot_size = formatCurrency(item.lot_size, 0);
    var amount = formatCurrencyToNumber(item.invoice_quantity);
    var quantity = '';
    var product_id = encodeString(item.product_id);
    var order_id = encodeString(item.order_id);

    max_lot_size = (max_lot_size == 0) ? 1 : max_lot_size;

    $('#productPackageModal #ppkg_balance').val(item.balance);
    $('#productPackageModal #ppkg_temp_quantity').val(item.temp_quantity);
    $('#productPackageModal #ppkg_product_id').val(decodeString(item.product_id));
    $('#productPackageModal #ppkg_product_name').val(item.product_name);
    $('#productPackageModal #ppkg_lot_size').val(lot_size);
    //$('#productPackageModal #ppkg_quantity').val(quantity);
    //$('#productPackageModal #ppkg_lot_no').val(item.lot_no);
    $('#productPackageModal #ppkg_amount').val(amount);
    $('#productPackageModal #ppkg_product_seq_hidden').val(item.product_seq);
    $('#productPackageModal #ppkg_product_id_hidden').val(product_id);
    $('#productPackageModal #ppkg_order_id_hidden').val(order_id);
    $('#productPackageModal #ppkg_ivp_id').val(item.id);

    $('#productPackageModal #ppkg_balance_display').text(formatCurrency(item.balance, 0));
    $('#productPackageModal #ppkg_max_lot_size').text(max_lot_size);

    //Lot No
    if (item.lot_no != '') {
        var spr_lot_no = item.lot_no.split(/\n/);
        for (var i = 0; i < 3; i++) {
            if (spr_lot_no[i] !== undefined) {
                $('#productPackageModal #ppkg_lot_no_' + (i + 1)).val(spr_lot_no[i]);
            } else {
                $('#productPackageModal #ppkg_lot_no_' + (i + 1)).val('');
            }
        }
    } else {
        $('#productPackageModal #ppkg_lot_no_1').val('');
        $('#productPackageModal #ppkg_lot_no_2').val('');
        $('#productPackageModal #ppkg_lot_no_3').val('');
    }


    loadPpkgLotitemsTable();
}

//** OLD */
function calculateProductPackageAmount() {
    var lot_size = parseFloat(formatCurrencyToNumber($('#productPackageModal #ppkg_lot_size').val()));
    var quantity = parseFloat($('#productPackageModal #ppkg_quantity').val());
    var result = lot_size * quantity;

    if (result || result == 0) {
        $('#productPackageModal #ppkg_amount').val(formatCurrency(result, 0));
    } else {
        $('#productPackageModal #ppkg_amount').val('');
    }
}

function findItemFromDataitems(ivp_id) {
    var dataitems = $('#dataitemTable tr.dataitem');

    if (dataitems) {
        for (var i = 0; i < dataitems.length; i++) {
            var item = convertDataitemToItem(dataitems[i]);
            if (item.id == ivp_id) { //item.product_id == product_id && item.order_id == order_id) {
                return item;
            }
        }
    }
    return null;
}

function updateDataitemPackage(ivp_id, quantity, lot_no) {
    var dataitems = $('#dataitemTable tr.dataitem');

    for (var i = 0; i < dataitems.length; i++) {
        var item = convertDataitemToItem(dataitems[i]);
        if (item.id == ivp_id) {
            $(dataitems[i]).find('.dt-invoice-quantity').text(quantity);
            $(dataitems[i]).find('.dt-lot-no').text(lot_no);

            item.invoice_quantity = quantity;
            item.lot_no = lot_no;

            if (item.dataitem_type == 'DATA') {
                deleteUpdateDataitemHidden(item.id);
                addUpdateDataitemHidden(item);
            } else if (item.dataitem_type == 'NEW') {
                deleteInsertDataitemHidden(item.id);
                addInsertDataitemHidden(item);
            }

            break;
        }
    }
}

function deleteUpdateDataitemHidden(ivp_id) {
    var hiddens = $('input[name=update_dataitems\\[\\]]');
    if (hiddens) {
        for (var i = 0; i < hiddens.length; i++) {
            var values = convertValueToArray(hiddens[i].value);
            if (values[0] == ivp_id) {
                $(hiddens[i]).remove();
                break;
            }
        }
    }
}

function addUpdateDataitemHidden(item) {
    var value = convertItemToValue(item);
    var hidden = '<input type="hidden" name="update_dataitems[]" value="' + value + '"/>';
    $('#dataitem_hidden_container').append(hidden);
}

function calculateAndDisplayDataitems() {
    var dataitems = $('#dataitemTable tr.dataitem');
    for (var i = 0; i < dataitems.length; i++) {
        var item = convertDataitemToItem(dataitems[i]);
        var invoice_quantity = formatCurrencyToNumber(item.invoice_quantity);
        var quantity = parseFloatOrNumeric(item.quantity);
        var unit_price = formatCurrencyToNumber(item.unit_price);
        var amount = '';
        var balance = '';

        //if (invoice_quantity != '' && quantity != '' && unit_price != '') {
        var temp_amount = invoice_quantity * unit_price;
        balance = quantity - invoice_quantity;

        //format display
        invoice_quantity = formatCurrency(invoice_quantity, 0);
        unit_price = formatCurrency(unit_price, 4);
        balance = formatCurrency(balance, 0);
        amount = formatCurrency(temp_amount);
        //}

        //Negative Amount
        /*
        if (temp_amount < 0) {
            amount = '(' + formatCurrency(Math.abs(temp_amount)) + ')';
        } else {
            amount = formatCurrency(temp_amount);
        }
        */

        $(dataitems[i]).find('.dt-invoice-quantity').text(invoice_quantity);
        $(dataitems[i]).find('.dt-unit-price').text(unit_price);
        $(dataitems[i]).find('.dt-amount').text(amount);
    }
}

function generateLotitemHiddens(ivp_id) {
    clearLotitemHiddens(ivp_id);

    var product_seq = $('#ppkg_product_seq_hidden').val();
    var order_id = $('#ppkg_order_id_hidden').val();
    var new_lotitems = getPpkgLotitems();
    for (var i = 0; i < new_lotitems.length; i++) {
        var lot_size = $(new_lotitems[i]).find('.lot-size').val();
        var lot_num = $(new_lotitems[i]).find('.lot-num').val();
        var quantity = formatCurrencyToNumber($(new_lotitems[i]).find('.quantity').val());
        createLotitemHidden(ivp_id, product_seq, order_id, lot_size, lot_num, quantity);
    }
}

function getLotitemHiddens(ivp_id) { //product_id, order_id) {
    var delimiter = "&88&";
    var resultLotitems = [];
    var lotitems = $("input.lotitems");
    var index = 0;

    for (var i = 0; i < lotitems.length; i++) {
        var values = lotitems[i].value.split(delimiter);
        if (values[0] == ivp_id) { //values[1] == product_id && values[2] == order_id) {
            resultLotitems[index++] = lotitems[i];
        }
    }

    return resultLotitems;
}

// ivp_id|product_seq|order_id|lot_size|lot_num|quantity
function createLotitemHidden(ivp_id, product_seq, order_id, lot_size, lot_num, quantity) {
    var delimiter = "&88&";
    var value = ivp_id +
            delimiter + product_seq +
            delimiter + order_id +
            delimiter + lot_size +
            delimiter + lot_num +
            delimiter + quantity;
    var lotitem_hidden = '<input type="hidden" name="lotitems[]" class="lotitems" value="' + value + '"/>';
    $('#lotitem_hidden_container').append(lotitem_hidden);
}

function createDefaultLotitemHidden(item) {
    var product_seq = item.product_seq;
    var order_id = item.order_id;
    var balance = parseFloatOrNumeric(item.balance);
    var lot_size = parseFloatOrNumeric(item.lot_size);
    var lot_num = 1;
    var rest_lot_size = 0;
    var quantity = balance;

    lot_size = lot_size == 0 ? 1 : lot_size;

    if (lot_size > balance) {
        lot_size = balance;
    } else {
        lot_num = Math.floor(balance / (lot_size == 0 ? 1 : lot_size));
        quantity = lot_size * lot_num;
    }

    createLotitemHidden(item.id, product_seq, order_id, lot_size, lot_num, quantity);

    //create more row
    rest_lot_size = balance % lot_size;
    if (rest_lot_size > 0) {
        createLotitemHidden(item.id, product_seq, order_id, rest_lot_size, 1, rest_lot_size);
    }

}

function clearLotitemHiddens(ivp_id) {
    var delimiter = "&88&";
    var lotitems = $("input.lotitems");
    for (var i = 0; i < lotitems.length; i++) {
        var values = lotitems[i].value.split(delimiter);
        if (values[0] == ivp_id) {
            lotitems[i].remove();
        }
    }
}

function getPpkgLotitems() {
    return $('#ppkg_lotitems_table .ppkg-lotitem');
}

function loadPpkgLotitemsTable() {
    var ivp_id = $('#productPackageModal #ppkg_ivp_id').val();
    var max_lot_size = $('#productPackageModal #ppkg_lot_size').val();
    var lotitem_hiddens = getLotitemHiddens(ivp_id);

    $('#ppkg_lotitems_table tbody').empty();

    if (lotitem_hiddens.length > 0) {
        for (var i = 0; i < lotitem_hiddens.length; i++) {
            var values = splitValues(lotitem_hiddens[i].value);
            // ivp_id|product_seq|order_id|lot_size|lot_num|quantity
            addPpkgLotitem((i + 1), values[3], values[4], values[5]);
        }
    } else {
        //default
        addPpkgLotitem(1, max_lot_size, 1, max_lot_size);
    }
}

function splitValues(strValue) {
    var delimiter = '&88&';
    return strValue.split(delimiter);
}

function calculatePpkgQuantity(lot_size, num) {
    var quantity = parseFloat(lot_size) * parseFloat(num);
    return isNaN(quantity) ? '' : quantity;
}

function addPpkgLotitem(no, lot_size, num, quantity) {
    quantity = formatCurrency(quantity, 0);
    var tr = '<tr class="ppkg-lotitem">' +
            ' <td style="width: 5%;">' +
            ' 		<span class="no">' + no + '.</span>' +
            ' 	</td>' +
            ' 	<td style="width: 25%;">' +
            ' 		<input type="number" class="form-control text-right lot-size" value="' + lot_size + '" onkeydown="onkeyNoDot(event, this)" onkeyup="onchangeLotSize(this); onkeyEnter(event)"/>' +
            ' 	</td>' +
            ' 	<td style="width: 5%; text-align: center;">' +
            ' 		x' +
            ' 	</td>' +
            ' 	<td style="width: 25%;">' +
            ' 		<input type="number" class="form-control text-right lot-num" value="' + num + '" onkeydown="onkeyNoDot(event, this)" onkeyup="onchangeLotNum(this); onkeyEnter(event)"/>' +
            ' 	</td>' +
            ' 	<td style="width: 5%; text-align: center;">' +
            ' 		=' +
            ' 	</td>' +
            ' 	<td style="width: 30%;">' +
            ' 		<input type="text" class="form-control text-right quantity" style="background-color: #ffff9d;" value="' + quantity + '" readonly />' +
            ' 	</td>' +
            ' 	<td style="width: 5%; text-align: center;">' +
            ' 		<button type="button" class="btn btn-default btn-icon delete-lotitem-btn" onclick="onclickDeleteLotitem(this)"><i class="fas fa-times fa-sm"></i></button>' +
            ' 	</td>' +
            ' </tr>';

    $('#ppkg_lotitems_table tbody').append(tr);
    displayDeleteLotitemButton();
    calculateSumLotitemsTable();
}

function onclickAddLotitem() {
    var lot_size = parseFloatOrNumeric($('#productPackageModal #ppkg_lot_size').val());
    var balance = parseFloatOrNumeric($('#productPackageModal #ppkg_balance').val());
    var sumQuantity = sumPpkgQuantity();

    if (sumQuantity < balance) {
        lot_size = lot_size == 0 ? 1 : lot_size;
        var diff = balance - sumQuantity;
        var lot_num = Math.floor(diff / lot_size);
        var quantity = lot_size * lot_num;
        var no = countLotitems() + 1;

        addPpkgLotitem(no++, lot_size, lot_num, quantity);

        //add more rest row
        var rest = diff % lot_size;
        if (rest > 0) {
            addPpkgLotitem(no, rest, 1, rest);
        }
    }
}

function countLotitems() {
    return $('#ppkg_lotitems_table .ppkg-lotitem').length;
}

function onclickDeleteLotitem(el) {
    el.parentNode.parentNode.remove();
    displayDeleteLotitemButton();
    calculateSumLotitemsTable();
}

function displayDeleteLotitemButton() {
    var buttons = $('#ppkg_lotitems_table .delete-lotitem-btn');
    for (var i = 0; i < buttons.length; i++) {
        if (i > 0 && i == buttons.length - 1) { //last
            $(buttons[i]).show();
        } else {
            $(buttons[i]).hide();
        }
    }
}

function calculateSumLotitemsTable() {
    var maxLotSize = parseFloatOrNumeric($('#productPackageModal #ppkg_lot_size').val());
    var balance = parseFloatOrNumeric($('#productPackageModal #ppkg_balance').val());
    var sumLotSize = sumPpkgLotSize();
    var sumLotNum = sumPpkgLotNum();
    var sumQuantity = sumPpkgQuantity();

    var elSumLotSize = $('#ppkg_lotitems_table #sum_ppkg_lot_size');
    var elSumLotNum = $('#ppkg_lotitems_table #sum_ppkg_lot_num');
    var elSumQuantity = $('#ppkg_lotitems_table #sum_ppkg_quantity');

    $(elSumLotSize).text(formatCurrency(sumLotSize, 0));
    $(elSumLotNum).text(formatCurrency(sumLotNum, 0));
    $(elSumQuantity).text(formatCurrency(sumQuantity, 0));

    var pass = "font-pass";
    var fail = "font-fail";
    var disabledSave = false;
    var disabledAdd = false;

    // if (sumLotSize > maxLotSize) {
    // 	$(elSumLotSize).removeClass(pass)
    // 	$(elSumLotSize).addClass(fail);
    // } else {
    // 	$(elSumLotSize).removeClass(fail)
    // 	$(elSumLotSize).addClass(pass);
    // }

    if (sumQuantity > balance) {
        $(elSumQuantity).removeClass(pass)
        $(elSumQuantity).addClass(fail);
        disabledSave = true;
    } else {
        $(elSumQuantity).removeClass(fail)
        $(elSumQuantity).addClass(pass);
    }

    if (sumQuantity >= balance) {
        disabledAdd = true;
    }

    $('#ppkgSaveButton').attr('disabled', disabledSave);
    $('#ppkgAddButton').attr('disabled', disabledAdd);
}

function calculateLotitem(lotitem) {
    var lot_size = $(lotitem).find(".lot-size");
    var lot_num = $(lotitem).find(".lot-num");
    var quantity = $(lotitem).find(".quantity");
    var q = parseFloat(lot_size[0].value) * parseFloat(lot_num[0].value);

    if (isNaN(q)) {
        quantity[0].value = '';
    } else {
        quantity[0].value = formatCurrency(q, 0);
    }
}

function sumPpkgLotSize() {
    var lot_sizes = $('#ppkg_lotitems_table input.lot-size');
    var sum = 0;
    for (var i = 0; i < lot_sizes.length; i++) {
        var lot_size = parseFloatOrNumeric(lot_sizes[i].value);
        sum += lot_size;
    }
    return sum;
}

function sumPpkgLotNum() {
    var lot_nums = $('#ppkg_lotitems_table input.lot-num');
    var sum = 0;
    for (var i = 0; i < lot_nums.length; i++) {
        var lot_num = parseFloatOrNumeric(lot_nums[i].value);
        sum += lot_num;
    }
    return sum;
}

function sumPpkgQuantity() {
    var quantities = $('#ppkg_lotitems_table input.quantity');
    var sum = 0;
    for (var i = 0; i < quantities.length; i++) {
        var quantity = formatCurrencyToNumber(quantities[i].value);
        sum += quantity;
    }
    return sum;
}

function onkeyNoDot(event, el) {
    if (event.keyCode === 110 || event.keyCode === 190) {
        event.preventDefault();
    }
}

function onkeyEnter(event) {
    if (event.keyCode === 13) {
        onsaveProductPackage();
    }
}

function onchangeLotSize(el) {
    var maxLotSize = parseFloatOrNumeric($('#productPackageModal #ppkg_lot_size').val());
    maxLotSize = (maxLotSize == 0) ? 1 : maxLotSize;

    calculateLotitem(el.parentNode.parentNode);
    calculateSumLotitemsTable();
}

function onchangeLotNum(el) {
    calculateLotitem(el.parentNode.parentNode);
    calculateSumLotitemsTable();
}

function calculateSummaryInvoice() {
    var dataitems = $('#dataitemTable .dataitem');
    var vat = parseFloatOrNumeric($('#sm_vat').text());
    var sum_amount = 0;

    for (var i = 0; i < dataitems.length; i++) {
        var quantity = formatCurrencyToNumber($(dataitems[i]).find('.dt-invoice-quantity').text());
        var amount = $(dataitems[i]).find('.dt-amount').text();

        /*
        if (quantity < 0) {
            amount = '-' + amount.replace(/\(/g, '').replace(/\)/g, '');
        }
        */
       
        amount = formatCurrencyToNumber(amount);
        sum_amount += amount;
    }

    var vat_price = (vat / 100) * sum_amount;
    var total = sum_amount + vat_price;

    $('#sm_amount').text(formatCurrency(sum_amount, 2));
    $('#sm_vat_price').text(formatCurrency(vat_price, 2));
    $('#sm_total').text(formatCurrency(total, 2));
}

function onclickSave() {
    setInternalNoteHidden();
    if (isSpecialInvoiceType($('#invoice_type option:selected').text())) {
        if (validateReferInvoice()) {
            $('#form').submit();
        } else {
            showMessageModal('กรุณาระบุ Refer Invoice ID ให้ถูกต้อง');
        }
    } else {
        $('#form').submit();
    }
}

function check_invoice_id() {
    var invoice_id = $('#invoice_type option:selected').text() + $('#id_suffix').val();
    var temp_invoice_id = $('#temp_invoice_id').val();

    $('#invoice_id').val(invoice_id);

    if (invoice_id != '' && invoice_id != temp_invoice_id) {
        $.ajax({
            url: $('#base_url').val() + 'invoice/has_data_ajax?invoice_id=' + encodeString(invoice_id),
            type: 'GET',
            success: function (resp) {
                if (resp.success) {
                    if (resp.has_data) {
                        $('#invoice_id_err').html('&nbsp;&nbsp;Invoice ID already in use.');
                    } else {
                        $('#invoice_id_err').html('');
                    }
                }
                checkDisableButton();
            }
        });
    } else {
        $('#invoice_id_err').html('');
    }
    checkDisableButton();
}

function checkDisableButton() {
    var invoice_id = $('#invoice_id').val();
    var invoice_id_err = $('#invoice_id_err').html().trim();

    if (invoice_id != '' && invoice_id_err == '') {
        $('#btn_create').removeClass('disabled');
    } else {
        $('#btn_create').addClass('disabled');
    }
}

function validateReferInvoice() {
    var referInvoice = $('#refer_invoice_id').val();
    if (referInvoice) {
        var invoice_ids = referInvoice.split(',');
        for (var i = 0; i < invoice_ids.length; i++) {
            if (!correctReferInvoiceType(invoice_ids[i])) {
                return false;
            }
        }
        return true;
    }
    return false;
}

function correctReferInvoiceType(invoice_id) {
    var invoiceTypeOptions = $('#invoice_type option');
    for (var i = 0; i < invoiceTypeOptions.length; i++) {
        var invoice_type = invoiceTypeOptions[i].text;
        //Starts with invoice type
        if ((invoice_id.indexOf('C') !== 0 && invoice_id.indexOf('D') !== 0) && invoice_id.indexOf(invoice_type) === 0) {
            return true;
        }
    }
    return false;
}

function isSpecialInvoiceType(invoice_type) {
    return (invoice_type.indexOf('C') === 0) || (invoice_type.indexOf('D') === 0);
}