//-- Datatables
function initExportButtons(selector, title, filename, columns) {
    $('#export_buttons_container').html('');
    var t = $(selector).DataTable();
    var buttonCommon = {
        exportOptions: {
            columns: [1,2,3,4]
        }
    };

    var filename = filename;
    var title = title;
    var buttons = new $.fn.dataTable.Buttons(t, {
        buttons: [
            /*
            $.extend( true, {}, buttonCommon, {
                extend: 'pdfHtml5',
                text: '<i class="far fa-file-pdf"></i> Pdf',"className": 'btn btn-default btn-sm view-mode-1',
                title: title,
                messageTop: getExportHeader(),
                filename: filename,
                exportOptions: {
                    columns: columns
                }
            }),
            */
            $.extend( true, {}, buttonCommon, {
                extend: 'excelHtml5',
                text: '<i class="far fa-file-excel"></i> Excel',"className": 'btn btn-default btn-sm view-mode-1',
                title: title,
                //messageTop: getExportHeader(),
                filename: filename,
                exportOptions: {
                    columns: columns
                },
                customize: function( xlsx ) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];
                    $('row* ', sheet).each(function(index) {
                        if (index == 0) {
                            $(this).find('c').attr('s', 2);
                        }
                    });
                }
            }),
        ]
    }).container().appendTo($('#export_buttons_container'));
}

function getExportHeader() {
    return 'Export Date: ' + getCurrentDateString();
}

function generateReportName(title) {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

    return title + '_' + yyyy + twoDigits(mm) + twoDigits(dd);
}

function twoDigits(d) {
    return parseInt(d) < 10 ? '0' + d : d;
}