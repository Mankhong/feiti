$(document).ready(function () {

    var billing_id = $("#billing_id").val();
    console.log("billing_id: " + billing_id);

    var receipt_date = $("#receipt_date").val();
    console.log("receipt_date: " + receipt_date);
    var day = receipt_date.substr(0, 2);
    var month = receipt_date.substr(3, 2);
    var year = receipt_date.substr(6, 4);

    console.log("day: " + day);
    console.log("month: " + month);
    console.log("year: " + year);

    var data_ = day + '-' + getMonth(month) + '-' + year;
    $("#html_receipt_date").html(data_);

    var cusaddress = $("#id_customer_address").val();
    $('#spancusAddress').html(encodeLineBreak(cusaddress));

    loadDatatable(billing_id);
});


function loadDatatable(billing_id) {
    //console.log("loadDatatable: " + invoice_id);
    var count = 0;
    $("#rstable").dataTable().fnDestroy();
    var t = $('#rstable').DataTable({

        "ajax": {
            "url": $('#base_url').val() + "report/get_data_billing_invoices",
            "type": "post",
            "data": {billing_id: billing_id}
        },

        "type": "POST",
        "async": true,
        searching: false,
        ordering: true,
        lengthChange: false,
        pageLength: 19,
        columns: [{
                'data': 'id',
                "className": "text-center",
                'render': function (data, type, row, meta) {
                    return meta.row + 1;
                }
            },

            {targets: 0,
                data: 'invoice_id',
                "className": "text-left",
                render: function (data, type, row, meta) {
                    return row.invoice_id;

                }
            },

            {targets: 0,
                data: 'invoice_date',
                "className": "text-center",
                render: function (data, type, row, meta) {
                    return row.invoice_date;

                }
            },
            {targets: 0,
                data: 'total_amount_novat',
                "className": "text-right",
                render: function (data, type, row, meta) {
                    var n = parseFloatOrNumeric(data);
                    var t = formatCurrency(data, 2);

                    if (n < 0) {
                        t = '(' + formatCurrency(Math.abs(n), 2) + ')';
                    }

                    return t;
                }
            },

            {targets: 0,
                data: 'total_amount',
                "className": "text-right",
                render: function (data, type, row, meta) {
                    var n = parseFloatOrNumeric(data);
                    var t = formatCurrency(data, 2);

                    if (n < 0) {
                        t = '(' + formatCurrency(Math.abs(n), 2) + ')';
                    }

                    return t;
                }
            },
        ],

        "defaultContent": '',

        fnDrawCallback: function (settings) {
            appendSummary(calculateSummary());

            /*
             count++;
             if (count > 1) {
             //console.log("count: " + count);
             var info = $('#rstable').DataTable().page.info();
             var page = parseInt(info.page) + parseInt(1);
             var totaldata = 0;
             var pagele = 20;
             var result = 0;
             
             var total_amount = 0;
             var _total_vat = 0;
             var sumAll = 0;
             
             
             $.ajax({
             url: $('#base_url').val() + "report/get_data_billing_invoices",
             type: 'POST',
             data: {billing_id: billing_id,
             customer_id: ''
             },
             async: false,
             dataType: 'json',
             success: function (data) {
             //console.log("datax: " + data.data);
             var total = 0;
             var amount = 0;
             for (var i = 0; i < data.data.length; i++) {
             
             total_amount += formatCurrencyToNumber(formatCurrency(data.data[i].total_amount, 2));
             
             
             _total_vat += formatCurrencyToNumber(formatCurrency(((total_amount * 7) / 100), 2));
             
             sumAll = +parseFloat(total_amount) + parseFloat(_total_vat);
             
             }
             totaldata = data.data.length;
             result = totaldata / pagele;
             console.log("page: " + page + "result: " + Math.ceil(result));
             if (Math.ceil(result) === page) {
             console.log("_total_vat: " + _total_vat + " sumAll: " + formatCurrency(sumAll, 2));
             appendSummary(sumAll);
             }
             }
             });
             }
             */
        },

    });

}

function calculateSummary() {
    var vat = 7;
    var sum = 0;
    var table = $('#rstable').DataTable();
    var table_data = table.rows().data();
    for (var i = 0; i < table_data.length; i++) {
        sum += parseFloatOrNumeric(table_data[i]['total_amount']);
    }
    return sum;
}

function calculateGrandTotal(vat, total_amount) {
    var total_vat = roundup((vat / 100) * total_amount, 2);
    var result = parseFloatOrNumeric(total_amount) + parseFloatOrNumeric(total_vat);
    return result;
}

function appendSummary(sumAll) {
    var table = $('#rstable').DataTable();
    var page_info = table.page.info();
    if ((page_info.page + 1) == page_info.pages) {
        var html = '<tr>\n\
                        <td class="text-center" style="border-top: 1px solid black;" colspan="4"><strong>TOTAL</strong></td>\n\
                        <td class="text-right" style="border-top: 1px solid black; padding-right: 35px;">' + formatCurrency(sumAll, 2) + '</td>\n\
                    </tr>';
        $('tbody').append(html);
    }
}

$("#btn_print").click(function () {



    var billing_id = $("#billing_id").val();
    initViewReport(billing_id);

    var mode = 'popup'; //popup 
    var close = mode == "popup";
    var options = {

        mode: "iframe",
        popClose: close,
        header: false,
    };
    $("div.printableArea").printArea(options);
});

function initViewReport(billing_id) {
    var customer_name = $("#id_customer_name").val();
    var customer_address = $("#id_customer_address").val();
    var html_receipt_date = $('#html_receipt_date').text();
    var header_html = setHtml(billing_id, customer_name, customer_address, html_receipt_date);
    var header_html2 = setHtml2(customer_address);

    var total_amount = 0;
    var _total_vat = 0;
    var count = 0;
    var tbody = '';

    var table = $('#rstable').DataTable();
    var table_data = table.rows().data();
    var page_info = table.page.info();
    //console.log(table_data);

    var font_size_1 = 'font-size: 28px;';

    //array conten
    var pagArr = [];
    var fiveArray = [];
    var index = 0;
    var indexFilve = 0;
    var count = 0;
    var row = 19;

    for (var i = 0; i < table_data.length; i++) {
        fiveArray[indexFilve++] = table_data[i];
        if ((i + 1) % row === 0 || i === table_data.length - 1) {
            pagArr[index++] = fiveArray;
            count++;
            fiveArray = [];
            indexFilve = 0;
        }
    }

    var result = '';


    var totalpage = table_data.length / row;
    console.log("totalpage: " + Math.ceil(totalpage));
    console.log("pages: " + page_info.pages);
    var sum = '';
    var span = '';
    if (Math.ceil(totalpage) <= 1) {
        console.log("#### <= 1");
        sum = '<tr>\n\
                        <td class="text-center" style="border-top: 1px solid black;" colspan="4"><strong>TOTAL</strong></td>\n\
                        <td class="text-right" style="border-top: 1px solid black; padding-right: 35px;">' + formatCurrency(calculateSummary(), 2) + '</td>\n\
                    </tr>';
        for (var i = 0; i < pagArr.length; i++) {
            result += setContent(pagArr[i], header_html, header_html2, sum, i, '', page_info.pages, row);
        }
        $("#content").html(result);

    } else {
        console.log("#### > 1");
        var _pageCount = 0;

        for (var i = 0; i < pagArr.length; i++) {
            _pageCount += 1;

            span = '<span style="font-family: \'Angsana New\'; color: #000;' + font_size_1 + '">' + _pageCount + '/' + page_info.pages + '</span>';
            if (_pageCount == count) {
                sum = '<tr>\n\
                        <td class="text-center" style="border-top: 1px solid black;" colspan="4"><strong>TOTAL</strong></td>\n\
                        <td class="text-right" style="border-top: 1px solid black; padding-right: 35px;">' + formatCurrency(calculateSummary(), 2) + '</td>\n\
                    </tr>';
            } else {
                sum = '';
            }
            result += setContent(pagArr[i], header_html, header_html2, sum, i, span, page_info.pages, row);
        }
        $("#content").html(result);
    }




    /*
     console.log("table_data.length: " + table_data.length);
     for (var i = 0; i < table_data.length; i++) {
     count++;
     total_amount += formatCurrencyToNumber(formatCurrency(table_data[i].total_amount, 2));
     _total_vat += formatCurrencyToNumber(formatCurrency(((total_amount * 7) / 100), 2));
     sumAll = +parseFloat(total_amount) + parseFloat(_total_vat);
     
     var vat = 7;
     var total_vat = (table_data[i].total_amount * vat) / 100;
     console.log("total_vat: " + total_vat);
     var result = (parseFloat(table_data[i].total_amount) + parseFloat(total_vat));
     console.log("result: " + result);
     
     var common_style = 'border-top: 0px solid #dee2e6; font-family: \'Angsana New Bold\'';
     var font_size_1 = 'font-size: 28px;';
     
     tbody += '<tr role="row" class="odd">\n\
     <td class=" text-center" style="' + common_style + font_size_1 + '">' + count + '</td>\n\
     <td class="text-left" style="padding-left: 30px; ' + common_style + font_size_1 + '">' + table_data[i].invoice_id + '</td>\n\
     <td class="text-center" style="padding-left: 40px;' + common_style + font_size_1 + '">' + table_data[i].invoice_date + '</td>\n\
     <td class=" text-right" style="padding-left: 20px; ' + common_style + font_size_1 + '">' + formatCurrency(table_data[i].total_amount) + '</td>\n\
     <td class=" text-right" style="padding-right: 35px; ' + common_style + font_size_1 + '">' + formatCurrency(result, 2) + '</td>\n\
     </tr>';
     
     }
     var sum = '<tr>\n\
     <td class="text-center" style="border-top: 1px solid black;" colspan="4"><strong>TOTAL</strong></td>\n\
     <td class="text-right" style="border-top: 1px solid black;">' + formatCurrency(calculateSummary(), 2) + '</td>\n\
     </tr>';
     
     
     var last_content = lastHtml();
     var result = html += tbody += sum += last_content;
     $("#content").html(result);*/
}

function setContent(table_data, header_html, header_html2, sum, index, span, pages, row) {

    var count = 0;
    var tbody = '';
    var total_amount = 0;
    var _total_vat = 0;
    var sumAll = 0;

    for (var i = 0; i < row; i++) {
        count++;
        var common_style = 'border-top: 0px solid #dee2e6; font-family: \'Angsana New Bold\'';
        var font_size_1 = 'font-size: 28px;';
        console.log("table_data.length:  "+ table_data.length);
        if (table_data[i] === undefined) {
            //continue;
        //}
        //if(!table_data){
            console.log("####");
            tbody += '<tr role="row" class="odd">\n\
                <td class=" text-center" style="' + common_style + font_size_1 + '">&nbsp;</td>\n\
                <td class="text-left" style="padding-left: 30px; ' + common_style + font_size_1 + '"></td>\n\
                <td class="text-center" style="padding-left: 20px;' + common_style + font_size_1 + '"></td>\n\
                <td class=" text-right" style="padding-left: 20px; ' + common_style + font_size_1 + '"></td>\n\
                <td class=" text-right" style="padding-right: 35px; ' + common_style + font_size_1 + '"></td>\n\
            </tr>';
            
        }else{

        total_amount += formatCurrencyToNumber(formatCurrency(table_data[i].total_amount, 2));
        _total_vat += formatCurrencyToNumber(formatCurrency(((total_amount * 7) / 100), 2));
        sumAll = +parseFloat(total_amount) + parseFloat(_total_vat);

        var vat = 7;
        var total_vat = (table_data[i].total_amount * vat) / 100;
        //console.log("total_vat: " + total_vat);
        //var result = (parseFloat(table_data[i].total_amount) + parseFloat(total_vat));
        //console.log("result: " + result);

        var nt_novat = parseFloatOrNumeric(table_data[i].total_amount_novat);
        var nt = parseFloatOrNumeric(table_data[i].total_amount);
        var t_novat = formatCurrency(nt_novat, 2);
        var t = formatCurrency(nt, 2);

        if (nt_novat < 0) {
            t_novat = '(' + formatCurrency(Math.abs(nt_novat), 2) + ')';
        }

        if (nt < 0) {
            t = '(' + formatCurrency(Math.abs(nt), 2) + ')';
        }


        tbody += '<tr>\n\
                <td class=" text-center" style="' + common_style + font_size_1 + '">'  + (count + (19 * index)) +  '</td>\n\
                <td class="text-left" style="padding-left: 30px; ' + common_style + font_size_1 + '">' + table_data[i].invoice_id + '</td>\n\
                <td class="text-center" style="padding-left: 40px;' + common_style + font_size_1 + '">' + table_data[i].invoice_date + '</td>\n\
                <td class=" text-right" style="padding-right: 35px; ' + common_style + font_size_1 + '">' + t_novat + '</td>\n\
                <td class=" text-right" style="padding-right: 35px; ' + common_style + font_size_1 + '">' + t + '</td>\n\
            </tr>';
        }
        
    }

    console.log("sum: " + sum);
    var last_content = lastHtml();
    var html = header_html += span += header_html2 += tbody += sum += last_content;
    return html;

}

function setHtml(billing_id, customer_name, customer_address, html_receipt_date) {

    var common_style = 'border-top: 0px solid #dee2e6; font-family: \'Angsana New\';';
    var font_size_1 = 'font-size: 28px;';
    var html = '<div style="margin-top: 50px;">\n\
        <div class="row pt-2">\n\
            <div class="col-12">\n\
                <div class="row">\n\
                    <div class="col-2 text-right">\n\
                        <image src="../../assets/images/logo.png" style="width: 100px; height: 100px;">\n\
                    </div>\n\
                    <div class="col-10" style="padding-left: 1.0rem !important;">\n\
                        <div class="invoice-title">\n\
                            <div>\n\
                                <h3>บริษัท เฟยตี้ พรีซิชั่น (ไทยแลนด์) จำกัด<br>FEITI PRECISION (THAILAND)CO.,LTD</h3>\n\
                                <hr style="margin-top: 0px;margin-bottom: 15px;">\n\
                                <small>\n\
                                    1/92 หมู่ 5 สวนอุตสาหกรรมโรจนะ ต.คานหาม อ.อุทัย จ.พระนครศรีอยุธยา &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>โทร.</strong> &nbsp;:(035) 227916-9 <strong>โทรสาร</strong> (035) 226148\n\
                                    <br>\n\
                                    1/92 MOO 5 ROJANA INDUSTRIAL PARK TAMBOL KANHAM AMPHUR U-THAI AYUTTHAYA 13210 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>TEL.</strong> &nbsp;:(035) 227916-9 <strong>FAX</strong> : (035) 226148\n\
                                </small>\n\
                            </div>\n\
                        </div>\n\
                    </div>\n\
                </div>\n\
                <div class="row pt-2">\n\
                    <div class="col-2">\n\
                        <address>\n\
                            <span>สำนักงานใหญ่</span>\n\
                        </address>\n\
                    </div>\n\
                    <div class="col-3">\n\
                    </div>\n\
                    <div class="col-2 text-center">\n\
                        <address>\n\
                            <strong>ใบวางบิล</strong><br>\n\
                            <strong>BILLING NOTE</strong>\<br>\n\
                         </address>\n\
                    </div>\n\
                    <div class="col-5">\n\
                    </div>\n\
                </div>\n\
                <div class="row pt-1">\n\
                    <div class="col-2 text-left"  style="width: 150px !important; flex: 0 0 11.666667%;">\n\
                        <strong>เลขที่บิล: </strong> <br> <strong>No: </strong> \n\
                    </div>\n\
                    <div class="col-1 pt-2"> <span style="' + common_style + font_size_1 + '">' + billing_id + '</span> </div>\n\
                    <div class="col-5"></div>\n\
                    <div class="col-3">\n\
                        <div style="display: inline-block;">\n\
                            <strong>เลขประจำตัวผู้เสียภาษีอากร: </strong> \n\
                            <br> <strong>Tax Payer I.D. No.: </strong>  \n\
                        </div>\n\
                    </div>\n\
                    <div class="col-1 pt-2 pl-0 text-left"><span style="' + common_style + font_size_1 + '">0145547001421</span></div>\n\
                </div>\n\
                <div class="row pt-1" style="height:48px;">\n\
                    <div class="col-2 text-left" style="width: 150px !important; flex: 0 0 11.666667%;">\n\
                        <strong>นามลูกค้า:</strong> <br> <strong>Customer: </strong> \n\
                    </div>\n\
                    <div class="col-6 pt-2 text-left"> <span style="' + common_style + font_size_1 + ' line-height: 0.5;">' + customer_name + '</span> </div>\n\
                    <div class="col-1">\n\
                        <div style="display: inline-block;">\n\
                            <strong>วันที่: </strong> \n\
                            <br> <strong>Date: </strong>  \n\
                        </div>\n\
                    </div>\n\
                    <div class="col-2 pt-2 text-left"><span style="' + common_style + font_size_1 + '">' + html_receipt_date + '</span>&nbsp;&nbsp; <span>';
    return html;
}

function setHtml2(customer_address) {

    var common_style = 'border-top: 0px solid #dee2e6; font-family: \'Angsana New\';';
    var font_size_1 = 'font-size: 28px;';
    var html = '</span></div>\n\
                </div>\n\
                <div class="row pt-1" style="height:75px;">\n\
                    <div class="col-2 text-left" style="width: 150px !important; flex: 0 0 11.666667%;">\n\
                        <strong>ที่อยู่ลูกค้า: </strong> <br> <strong>Address: </strong> \n\
                    </div>\n\
                    <div class="col-7 pt-0"> <span style="' + common_style + font_size_1 + ' line-height: 0.7; padding-top: 2px;">' + encodeLineBreak(customer_address) + '</span> </div>\n\
                    <div class="col-1"></div>\n\
                </div>\n\
                <div class="row pt-1">\n\
                    <div class="col-md-12">\n\
                        <div class="panel panel-default">\n\
                            <div style="height: 10px;"></div>\n\
                            <div class="table-responsive" style="height:900px; overflow: hidden;">\n\
                                <table class="result-table" border="1" style="overflow: hidden; height: 877px;">\n\
                                    <thead>\n\
                                        <tr>\n\
                                            <td class="text-center" style="width:7%;"><strong>Item<br>ลำดับ</strong></td>\n\
                                            <td class="text-center" style="width:13%;"><strong>Invoice No.<br>เลขที่ใบสั่งของ</strong></td>\n\
                                            <td class="text-center" style="width:13%;"><strong>Date<br>วันที่</strong></td>\n\
                                            <td class="text-center" style="width:13%;"><strong>Total<br>จำนวนเงิน</strong></td>\n\
                                            <td class="text-center" style="width:13%;"><strong>Grand Total(VAT 7%)<br>จำนวนเงินรวม VAT</strong></td>\n\
                                        </tr>\n\
                                    </thead>\n\
                                   <tbody>';
    return html;
}

function lastHtml() {
    var last_content = '</tbody>\n\
                                </table>\n\
                            </div>\n\
                        </div>\n\
                    </div>\n\
                </div>\n\
                <div style="height: 20px;"></div>\n\
                <div class="row" style="height:147px;">\n\
                    <div class="col-6 ">\n\
                        <div class="panel-body table-font" style="border: 1px solid #000000; padding-top: 20px;">\n\
                            <div>  \n\
                                <div style="display: inline-block"></div><span style="padding-left: 20px;">นัดรับเงินวันที่</span>\n\
                                <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 10%; display: inline-block; "></div>/\n\
                                <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 10%; display: inline-block; "></div>/\n\
                                <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 10%; display: inline-block; "></div>\n\
                            </div>\n\
                           <br><br>\n\
                            <div class="text-center">  \n\
                                <div style="display: inline-block"></div><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 37%; display: inline-block; "></div>\n\
                            </div>\n\
                            <div class="text-center">\n\
                                <div style="display: inline-block"></div><span style="padding-left: 21px;">Receiver/ผู้รับบิล</span>\n\
                            </div>\n\
                            <div class="text-center" style="margin-top: 10px;">  \n\
                                Date:<div style="border-bottom: 1px dashed #000;text-decoration: none; width: 10%; display: inline-block; "></div>/\n\
                                <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 10%; display: inline-block; "></div>/\n\
                                <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 10%; display: inline-block; "></div>\n\
                            </div>\n\
                        </div>\n\
                    </div>\n\
                    <div class="col-6">\n\
                        <div class="panel-body table-font" style="border: 1px solid #000000; padding-top: 20px;">\n\
                            <div> \n\
                                <div style="display: inline-block"></div><span style="padding-left: 20px;">ผู้วางบิล</span>\n\
                            </div>\n\
                           <br><br>\n\
                            <div class="text-center">  \n\
                                <div style="display: inline-block"></div><div style="border-bottom: 1px dashed #000;text-decoration: none; width: 37%; display: inline-block; "></div>       \n\
                            </div>\n\
                            <div class="text-center">\n\
                                <div style="display: inline-block"></div><span style="padding-left: 21px;">Authorizied Signature/ลายเซ็นผู้ได้รับมอบอำนาจ</span>\n\
                            </div>\n\
                            <div class="text-center" style="margin-top: 10px;">  \n\
                                Date:<div style="border-bottom: 1px dashed #000;text-decoration: none; width: 10%; display: inline-block; "></div>/\n\
                                <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 10%; display: inline-block; "></div>/\n\
                                <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 10%; display: inline-block; "></div>\n\
                            </div>\n\
                        </div>\n\
                    </div>\n\
                </div>\n\
            </div>\n\
        </div>\n\
    </div>';

    return last_content;
}
function getMonth(month) {
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    if (month === '01') {
        return monthNames[0];
    } else if (month === '02') {
        return monthNames[1];
    } else if (month === '03') {
        return monthNames[2];
    } else if (month === '04') {
        return monthNames[3];
    } else if (month === '05') {
        return monthNames[4];
    } else if (month === '06') {
        return monthNames[5];
    } else if (month === '07') {
        return monthNames[6];
    } else if (month === '08') {
        return monthNames[7];
    } else if (month === '09') {
        return monthNames[8];
    } else if (month === '10') {
        return monthNames[9];
    } else if (month === '11') {
        return monthNames[10];
    } else if (month === '12') {
        return monthNames[11];
    }
}