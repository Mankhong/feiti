var OrderProductPicker = {
    showPicker: function (customer_id, invoice_type, invoice_group, order_id, product_id, product_name) {

        
        $("#orderProductPickerTable").dataTable().fnDestroy();
        $('#orderProductPickerModal').modal('show');

        OrderProductPicker.showLoading();
        var t = $('#orderProductPickerTable').DataTable({
            ajax: $('#base_url').val() + 'invoice/get_order_product_list_ajax?customer_id=' + customer_id + '&invoice_type=' + invoice_type + '&invoice_group=' + invoice_group + '&order_id=' + order_id + '&product_id=' + product_id + '&product_name=' + product_name,
            searching: false,
            ordering: true,
            order: [
                [1, 'asc']
            ],
            lengthChange: false,
            pageLength: 10,
            columns: [{
                    'data': 'product_id',
                    'render': function (data, type, row, meta) {
                        return '<input type="checkbox" data-product-id="' + encodeString(data) + '"'
                                + ' data-product-seq="' + encodeString(row['product_seq']) + '"'
                                + ' data-product-name="' + row['desc1'] + '"'
                                + ' data-unit-price="' + row['unit_price'] + '"'
                                + ' data-quantity="' + row['quantity'] + '"'
                                + ' data-balance="' + row['balance'] + '"'
                                + ' data-lot-size="' + row['lot_size'] + '"'
                                + ' data-order-id="' + encodeString(row['order_id']) + '"'
                                + ' class="order-product-picker-cb"/>';
                    }
                },
                {
                    'data': 'product_id'
                },
                {
                    'data': 'desc1'
                },
                {
                    'data': 'order_id'
                },
                {
                    'data': 'order_date'
                },
                {
                    'data': 'quantity',
                    'render': function (data, type, row, meta) {
                        return  formatCurrency(data, 0);
                    }
                },
                {
                    'data': 'balance',
                    'render': function (data, type, row, meta) {
                        return  formatCurrency(data, 0);
                    }
                },
            ],
            columnDefs: [{
                    "targets": 0,
                    "className": "text-center no-action",
                    "orderable": false
                },
                {
                    "targets": 4,
                    "className": "text-center"
                },
                {
                    "targets": 5,
                    "className": "text-right"
                },
                {
                    "targets": 6,
                    "className": "text-right"
                },
            ],
            initComplete: function (settings) {
                $('input[aria-controls=orderProductPickerTable]').focus();
                OrderProductPicker.setEvents();
                OrderProductPicker.generateHiddens();
				OrderProductPicker.processDisableButtons();
                OrderProductPicker.hideLoading();
            },
        });

        //-- Custom Filter
        $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex) {
                    $('.order-product-picker-cball').prop('checked', false);
                    $('.order-product-picker-cb').prop('checked', false);
                    return true;
                }
        );

        //Onclick row.
        $('#orderProductPickerTable tbody').unbind('click');
        $('#orderProductPickerTable tbody').on('click', 'td', function () {
            if (!$(this).hasClass('no-action')) {
                var tr = $(this).parent();
                var t = $('#orderProductPickerTable').DataTable();
                var data = t.row(tr).data();
                var product_seq = encodeString(data['product_seq']);
                var order_id = encodeString(data['order_id']);
                var cb = $('input[type=\'checkbox\'][data-product-seq=\'' + product_seq + '\'][data-order-id=\'' + order_id + '\']');
                $(cb).prop('checked', !$(cb).prop('checked'));
            }
        });
    },

    hidePicker: function () {
        OrderProductPicker.clearCriteria();
        $('#orderProductPickerModal').modal('hide');
    },

    setEvents: function () {
        $('.order-product-picker-cball').unbind('click');
        $('.order-product-picker-cball').on('click', function () {
            $('.order-product-picker-cb').prop('checked', $(this).prop('checked'));
        });

        //-- relate to cball
        $('.order-product-picker-cb').unbind('click');
        $('.order-product-picker-cb').on('click', function () {
            var cbs = $('.order-product-picker-cb');

            //checked all
            var sameall = true;
            var checked = false;
            for (var i = 0; i < cbs.length; i++) {
                if (i == 0) {
                    checked = $(cbs[i]).prop('checked');
                } else {
                    if (checked != $(cbs[i]).prop('checked')) {
                        sameall = false;
                        break;
                    }
                }
            }

            if (sameall) {
                $('.order-product-picker-cball').prop('checked', checked);
            }
        });

        //on click select
        $('.order-product-picker-select').unbind('click');
        $('.order-product-picker-select').on('click', function () {
            var selected_items = $('.order-product-picker-cb:checked');
            OrderProductPicker.selectedCallBack(selected_items);
        });

        $('.order-product-picker-select-all').unbind('click');
		$('.order-product-picker-select-all').on('click', function () {
			var selected_items = $('.order-product-picker-hd');
			OrderProductPicker.selectedCallBack(selected_items);
		});
    },

    showLoading: function () {
        $('.picker-loading').css('display', 'block');
    },

    hideLoading: function () {
        $('.picker-loading').css('display', 'none');
    },

    selectedCallBack: function (data) {},

    processDisableButtons: function () {
        var data = $('#orderProductPickerTable').DataTable().rows({filter:'applied'}).data();
		if (data.length > 0) {
			$('.order-product-picker-select-all').removeClass('disabled');
            $('.order-product-picker-select').removeClass('disabled');
		} else {
			$('.order-product-picker-select-all').removeClass('disabled');
			$('.order-product-picker-select').removeClass('disabled');
			$('.order-product-picker-select-all').addClass('disabled');
            $('.order-product-picker-select').addClass('disabled');
		}
		
    },
    
    generateHiddens: function () {
		var html = '';
		var data = $('#orderProductPickerTable').DataTable().rows({filter:'applied'}).data();
		for (var i = 0; i < data.length; i++) {
			html += '<input type="hidden" data-product-id="' + encodeString(data[i]['product_id']) + '"'
            + ' data-product-seq="' + encodeString(data[i]['product_seq']) + '"'
            + ' data-product-name="' + data[i]['desc1'] + '"'
            + ' data-unit-price="' + data[i]['unit_price'] + '"'
            + ' data-quantity="' + data[i]['quantity'] + '"'
            + ' data-balance="' + data[i]['balance'] + '"'
            + ' data-lot-size="' + data[i]['lot_size'] + '"'
            + ' data-order-id="' + encodeString(data[i]['order_id']) + '"'
            + ' class="order-product-picker-hd"/>';
		}
		$('.order-product-picker-hidden-container').html(html);
    },
    
    clearCriteria: function() {
        $('#order_id').val('');
        $('#product_id').val('');
        $('#product_name').val('');
    }
}

