var InvoicePicker = {
	showPicker: function (customer_id) {
		$('#ftSearch').val('');
		$('#ftStartDate').val('');
		$('#ftEndDate').val('');
		$("#invoicePickerTable").dataTable().fnDestroy();
		$('#invoicePickerModal').modal('show');
		
		var t = $('#invoicePickerTable').DataTable({
			ajax: $('#base_url').val() + 'billing/get_invoice_list_ajax?customer_id=' + customer_id,
			searching: true,
			ordering: true,
			order: [
				[1, 'asc']
			],
			lengthChange: false,
			pageLength: 10,
			columns: [{
					'data': 'invoice_id',
					'render': function (data, type, row, meta) {
						var amount = parseFloatOrNumeric(row['amount']);
						return '<input type="checkbox" data-invoice-id="' + encodeString(data) + '"' +
							' data-invoice-date="' + row['invoice_date'] + '"' +
							' data-print-invoice-id="' + row['print_invoice_id'] + '"' +
							' data-total-amount="' + amount + '"' +
							' class="invoice-picker-cb"/>';
					}
				},
				{
					'data': 'invoice_id'
				},
				{
					'data': 'invoice_date'
				},
				{
					'data': 'total_amount',
					'render': function (data, type, row, meta) {
						var amount = parseFloatOrNumeric(row['amount']);
						//var vat_amount = (parseFloatOrNumeric(row['vat']) / 100) * amount;
						return formatCurrency(amount, 2);
					}
				},
			],
			columnDefs: [{
					"targets": 0,
					"className": "text-center no-action",
					"orderable": false
				},
				{
					"targets": 2,
					"className": "text-center",
					"orderable": false
				},
				{
					"targets": 3,
					"className": "text-right"
				},
			],
			initComplete: function (settings) {
				InvoicePicker.setEvents();
				$('#invoicePickerModal #ftSearch').focus();
			},
			drawCallback: function (settings) {
				InvoicePicker.generateHiddens();
				InvoicePicker.processDisableButtons();
			}
		});

		//-- Custom Filter
		$.fn.dataTable.ext.search.push(
			function (settings, data, dataIndex) {
				var match = true;
				//reset checkboxs
				$('.invoice-picker-cball').prop('checked', false);
				$('.invoice-picker-cb').prop('checked', false);

				//-- Date range
				var numStartDate = convertDateToNumber($('#ftStartDate').val());
				var numEndDate = convertDateToNumber($('#ftEndDate').val());
				var numDate = convertDateToNumber(data[2]);

				if (numStartDate != 0 && numEndDate != 0) {
					match = numDate >= numStartDate && numDate <= numEndDate;

				} else if (numStartDate != 0 && numEndDate == 0) {
					match = numDate >= numStartDate;

				} else if (numStartDate == 0 && numEndDate != 0) {
					match = numDate <= numEndDate;
				}

				//-- Search
				if (match) {
					var search = $('#ftSearch').val();
					if (search != '') {
						match = (data[1].toLowerCase()).indexOf(search.toLowerCase()) == 0;
					}
				}
				
				return match;
			}
		);

		//Onclick row.
		$('#invoicePickerTable tbody').unbind('click');
		$('#invoicePickerTable tbody').on('click', 'td', function () {
			if (!$(this).hasClass('no-action')) {
				var tr = $(this).parent();
				var t = $('#invoicePickerTable').DataTable();
				var data = t.row(tr).data();
				var invoice_id = encodeString(data['invoice_id']);

				var cb = $('input[type=\'checkbox\'][data-invoice-id=\'' + invoice_id + '\']');
				$(cb).prop('checked', !$(cb).prop('checked'));
			}
		});

		$('#ftStartDate').on('change', function(){
			t.draw();
		});

		$('#ftEndDate').on('change', function(){
			t.draw();
		});

		$('#ftSearch').on('keyup', function(){
			t.draw();
		});
	},

	hidePicker: function () {
		$('#invoicePickerModal').modal('hide');
		$('#invoicePickerTable_filter input[type=search]').val('');
		$('#invoicePickerModal #ftSearch').val('');
	},

	setEvents: function () {
		$('.invoice-picker-cball').unbind('click');
		$('.invoice-picker-cball').on('click', function () {
			$('.invoice-picker-cb').prop('checked', $(this).prop('checked'));
		});

		//-- relate to cball
		$('.invoice-picker-cb').unbind('click');
		$('.invoice-picker-cb').on('click', function () {
			var cbs = $('.invoice-picker-cb');

			//checked all
			var sameall = true;
			var checked = false;
			for (var i = 0; i < cbs.length; i++) {
				if (i == 0) {
					checked = $(cbs[i]).prop('checked');
				} else {
					if (checked != $(cbs[i]).prop('checked')) {
						sameall = false;
						break;
					}
				}
			}

			if (sameall) {
				$('.invoice-picker-cball').prop('checked', checked);
			}
		});

		//on click select
		$('.invoice-picker-select').unbind('click');
		$('.invoice-picker-select').on('click', function () {
			var selected_items = $('.invoice-picker-cb:checked');
			InvoicePicker.selectedCallBack(selected_items);
		});

		$('.invoice-picker-select-all').unbind('click');
		$('.invoice-picker-select-all').on('click', function () {
			var selected_items = $('.invoice-picker-data-item');
			InvoicePicker.selectedCallBack(selected_items);
		});
	},

	selectedCallBack: function (data) {},

	processDisableButtons: function () {
		var data = $('#invoicePickerTable').DataTable().rows({filter:'applied'}).data();
		if (data.length > 0) {
			$('.invoice-picker-select-all').removeClass('disabled');
			$('.invoice-picker-select').removeClass('disabled');
		} else {
			$('.invoice-picker-select-all').removeClass('disabled');
			$('.invoice-picker-select').removeClass('disabled');
			$('.invoice-picker-select-all').addClass('disabled');
			$('.invoice-picker-select').addClass('disabled');
		}
		
	},

	generateHiddens: function () {
		var html = '';
		var data = $('#invoicePickerTable').DataTable().rows({filter:'applied'}).data();
		for (var i = 0; i < data.length; i++) {
			html += '<input type="hidden" data-invoice-id="' + encodeString(data[i]['invoice_id']) + '"' +
			' data-invoice-date="' + data[i]['invoice_date'] + '"' +
			' data-print-invoice-id="' + data[i]['print_invoice_id'] + '"' +
			' data-total-amount="' + parseFloatOrNumeric(data[i]['amount']) + '"' +
			' class="invoice-picker-data-item"/>';
		}
		$('.invoice-picker-hidden-container').html(html);
	}
}