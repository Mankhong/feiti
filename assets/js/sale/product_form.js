$(document).ready(function () {
	$('.customerPickerTrigger').on('click', function(){
		CustomerPicker.showPicker('selectedCustomer');
	});

	$('#productHistoryTab').on('click', function () {
		var seq = $('#seq').val();
		if (seq != '') {
			loadProductHistoryTable(seq);
		} else {
			$("#productHistoryTable").dataTable().fnDestroy();
		}
	});

	$('#product_id').on('keydown', function(event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			$('.customerPickerTrigger').click();
		}
	});

	$('#customer_id').on('keydown', function(event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			$('.customerPickerTrigger').click();
		}
	});

	CustomerPicker.selectedCallBack = function(data) {
		CustomerPicker.hidePicker();
		$('#customer_id').val(data['id']);
		$('#customer_name').val(data['name']);
		$('#customer_type').val(data['customer_type']);
		
		disablePrice();
		setUnitPriceTypeByCustomerType();
	}
	
	disablePrice();
	setUnitPriceTypeByCustomerType();
});

function loadProductHistoryTable(seq) {
	$("#productHistoryTable").dataTable().fnDestroy();

	var t = $('#productHistoryTable').DataTable({
		ajax: $('#base_url').val() + 'product/get_products_history_list_ajax?seq=' + encodeURIComponent(btoa(seq)),
		searching: false,
		ordering: true,
		paging: false,
		order: [
			[15, 'desc']
		],
		lengthChange: false,
		pageLength: 10,
		columns: [{
				'data': 'id'
			},
			{
				'data': 'customer_id'
			},
			{
				'data': 'product_type_name'
			},
			{
				'data': 'invoice_type_name'
			},
			{
				'data': 'process_name'
			},
			{
				'data': 'desc1'
			},
			{
				'data': 'desc2'
			},
			{
				'data': 'boi_desc'
			},
			{
				'data': 'domestic_price'
			},
			{
				'data': 'export_price'
			},
			{
				'data': 'carton'
			},
			{
				'data': 'unit_price_type_name'
			},
			{
				'data': 'lot_size'
			},
			{
				'data': 'carton_temp'
			},
			{
				'data': 'internal_note'
			},
			{
				'data': 'h_date'
			},
		],
		columnDefs: [
			{
				"targets": 2,
				"className": "text-center"
			},
			{
				"targets": 3,
				"className": "text-center"
			},
			{
				"targets": 4,
				"className": "text-center"
			},
			{
				"targets": 8,
				"className": "text-right"
			},
			{
				"targets": 9,
				"className": "text-right"
			},
			{
				"targets": 11,
				"className": "text-center"
			},
			{
				"targets": 12,
				"className": "text-right"
			},
			{
				"targets": 15,
				"className": "text-center"
			},
		],
		fnDrawCallback: function (settings) {
		},
	});

}

function selectedCustomer(customer_id, customer_name, customer_type, customer_address, customer_credit, customer_type_name) {
	CustomerPicker.hidePicker();
	$('#customer_id').val(customer_id);
	$('#customer_name').val(customer_name);
	$('#customer_type').val(customer_type);
	
	disablePrice();
	setUnitPriceTypeByCustomerType();
}

function setUnitPriceType(type) {
	var unit_price_types = $('input[name=unit_price_type]');

	for (var i = 0; i < unit_price_types.length; i++) {
		if (unit_price_types[i].value == type) {
			$(unit_price_types[i]).prop('checked', true);
		}
	}
}

function disablePrice() {
	var customer_type = $('#customer_type').val();

	/*
	if (customer_type == 1) {
		$('#domestic_price').removeAttr('disabled');
		$('#export_price').attr('disabled', true);
	} else if (customer_type == 2) {
		$('#export_price').removeAttr('disabled');
		$('#domestic_price').attr('disabled', true);
	}
	*/
}

function setUnitPriceTypeByCustomerType() {
	var customer_type = $('#customer_type').val();
	var unit_price_types = $('input[name=unit_price_type]');
	for (var i = 0; i < unit_price_types.length; i++) {
		if (unit_price_types[i].value == customer_type) {
			$(unit_price_types[i]).attr('checked', true);
			break;
		}
	}
}

function setRadiosReadonly(name) {
	$('input[name=' + name + ']').on('click', function(){
		return false;
	});
}

function onclickSave() {
	$('#btn_create').attr('disabled','disabled');
	$('#btn_create').addClass('disabled');
	$('#form').submit();
}

function generateBoi() {
	var part_no = $('#product_id').val();
	var part_name = $('#desc1').val();
	$('#boi_desc').val(part_no + '/' + part_name);
}