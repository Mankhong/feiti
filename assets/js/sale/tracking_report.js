$(document).ready(function () {

    console.log("ready i report tracking");
    $("#rs_div").hide();
    
//    var table = $('#planlist').DataTable()({
//        responsive: true,
//        fixedHeader: true,
//        "processing": false,
//        "serverSide": false,
//        "searching": false,
//        "bFilter": false,
//        "bLengthChange": false,
//        "paging": true,
//        "ordering": true,
//        "info": false,
//        pageLength: 20,
//        lengthChange: false
//        
//    });
    
    

    $("#dateTimeStart").datepicker({
                    format: 'dd/mm/yyyy',
                    autoclose: true,
                    orientation: "bottom auto",
                    todayHighlight: true,
                    todayBtn: true,
                    language: 'EN', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                    thaiyear: false, //Set เป็นปี พ.ศ.
        }).datepicker("setDate");
    $("#dateTimeEnd").datepicker({
                    format: 'dd/mm/yyyy',
                    autoclose: true,
                    orientation: "bottom auto",
                    todayHighlight: true,
                    todayBtn: true,
                    language: 'EN', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                    thaiyear: false, //Set เป็นปี พ.ศ.
        }).datepicker("setDate");


});

$("#btn_search").click(function () {

    console.log("btn_search click");
    var startDate = $("#dateTimeStart").val();
    var dateTimeEnd = $("#dateTimeEnd").val();
    var customerId = $("#customerId").val();
    var productId = $("#productId").val();
    var orderId = $("#orderId").val();
    var invoiceId = $("#invoiceId").val();

    if ((startDate === null || startDate === '') &&
            (dateTimeEnd === null || dateTimeEnd === '') &&
            (customerId === null || customerId === '') &&
            (productId === null || productId === '') &&
            (orderId === null || orderId === '') &&
            (invoiceId === null || invoiceId === '')) {

        var table = $('#planlist').DataTable();
        table.clear().draw();

        $('#msgModalMessage').html("กรุณาระบุเงื่อนไขการค้นหา");
        $('#msgModal').modal('show');
    } else {

        console.log("btn_search else");
        var search = false;
        if ((startDate !== null && startDate !== '') && (dateTimeEnd !== null && dateTimeEnd !== '')) {

            if (process(startDate) > process(dateTimeEnd)) {
                search = false;
                $('#msgModalMessage').html("กรุณาระบุวันที่ค้นให้ถูกต้อง");
                $('#msgModal').modal('show');
                var table = $('#planlist').DataTable();
                table.clear().draw();
            } else {
                search = true;
            }
        } else {
            search = true;
        }
        console.log("btn_search search: " + search);
        //search
        if (search) {

            console.log("#### search ####");
//            $("#loadMe").modal({
//                backdrop: "static", //remove ability to close modal with click
//                keyboard: false, //remove option to close with keyboard
//                show: true //Display loader!
//            });
            $("#rs_div").show();
            loadhome(startDate, dateTimeEnd, customerId, invoiceId, productId, orderId);


//            var xhr = $.ajax({
//                url: $('#base_url').val() + "report/get_tracking_list_ajax",
//                type: 'POST',
//                data: {startDate: startDate,
//                    dateTimeEnd: dateTimeEnd,
//                    customerId: customerId,
//                    invoiceId: invoiceId,
//                    productId: productId,
//                    orderId: orderId
//                },
//                async: true,
//                dataType: 'json',
//                success: function (data) {
//
//                    console.log("length data: " + data.data.length);
//                    if (data.data.length > 0) {
//                        var html = setContent(data);
//                        //console.log("html: " + html);
//                        $("#rs_table").html(html);
//                        $("#loadMe").modal("hide");
//                        closePop();
//                    } else {
//                        notfoundData();
//                        closePop();
//                    }
//                }
//            });
        }

    }

});

function closePop() {
    console.log("Start colse popup");
    $("#loadMe").modal("hide");
    setTimeout(function () {
        $("#loadMe").modal("hide");
    }, 3000);

}

function setContent(data) {
    var html = '';
    html += '<table class="table table-condensed table-bordered table-font" style="width: 100%; margin-bottom: 0px; " border="0">\n\
                            <thead>\n\
                                <tr>\n\
                                    <td class="text-center" style="width:300px; background-color: #f0f8ff; font-weight: bold; text-align: center; vertical-align: middle;">Customer Name</td>\n\
                                    <td class="text-center" style="width:200px; background-color: #f0f8ff; font-weight: bold; text-align: center; vertical-align: middle;">Order Date</td>\n\
                                    <td class="text-center" style="width:300px; background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Order Id</td>\n\
                                    <td class="text-center" style="width:80px; background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Product Id</td>\n\
                                    <td class="text-center" style="width:80px; background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Unit Price</td>\n\
                                    <td class="text-center" style="width:50px; background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Quantity</td>\n\
                                    <td class="text-center" style="width:118px; background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Amount</td>\n\
                                    <td class="text-center" style="width:118px; background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Balance</td>\n\
                                    <td class="text-center" style="width:118px; background-color: #f0f8ff; font-weight: bold; text-align: center; vertical-align: middle;">Invoice Id</td>\n\
                                    <td class="text-center" style="width:118px; background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Invoice Date</td>\n\
                                    <td class="text-center" style="width:118px; background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Billing Id</td>\n\
                                    <td class="text-center" style="width:118px; background-color: #f0f8ff; font-weight: bold;  text-align: center; vertical-align: middle;">Billing Date</td>\n\
                                </tr>\n\
                            </thead>\n\
                         <tbody>';

    var padding_1 = 'padding: 5px;';
    var border = ''; //border: 1px solid black;
    var height = "height: 27px;";

    console.log(data.data.length);
    for (var i = 0; i < data.data.length; i++) {
        console.log("data.data[i].customer_name: " + data.data[i].customer_name);
        html += '<tr>\n\
                            <td class="text-left" style="width: 195px; ' + padding_1 + border + height + '">' + data.data[i].customer_name + '</td>\n\
                            <td class="text-center" style="width: 180px; ' + padding_1 + border + height + '">' + data.data[i].order_date + '</td>\n\
                            <td class="text-left" style="width: 160px; ' + padding_1 + border + height + '">' + data.data[i].order_id + '</td>\n\
                            <td class="text-left" style="text-align: right; width:180px; ' + padding_1 + border + height + '">' + data.data[i].product_id + '</td>\n\
                            <td class="text-right" style="text-align: right; width:118px; ' + padding_1 + border + height + '">' + formatCurrency(data.data[i].unit_price, 4) + '</td>\n\
                            <td class="text-right" style="text-align: right; width: 50px; ' + padding_1 + border + height + '">' + data.data[i].invoice_quantity + '</td>\n\
                            <td class="text-right" style="text-align: right; width:118px; ' + padding_1 + border + height + '">' + formatCurrency(data.data[i].amount, 2) + '</td>\n\
                            <td class="text-right" style="text-align: right; width:118px; ' + padding_1 + border + height + '">' + data.data[i].balance + '</td>\n\
                            <td class="text-left" style="text-align: right; width:118px; ' + padding_1 + border + height + '">' + (data.data[i].invoices_id === null ? '' : data.data[i].invoices_id) + '</td>\n\
                            <td class="text-center" style="text-align: right; width:118px; ' + padding_1 + border + height + '">' + (data.data[i].invoice_date === null ? '' : data.data[i].invoice_date) + '</td>\n\
                            <td class="text-right" style="text-align: right; width:118px; ' + padding_1 + border + height + '">0</td>\n\
                            <td class="text-left" style="text-align: right; width:118px; ' + padding_1 + border + height + '">' + (data.data[i].billing_id === null ? '' : data.data[i].billing_id) + '</td>\n\
                            <td class="text-center" style="text-align: right; width:118px; ' + padding_1 + border + height + '">' + (data.data[i].billing_date === null ? '' : data.data[i].billing_date) + '</td>\n\
                         </tr>';
    }

    var table = "</tbody>\n\
            </table>";
    var result = html += table;
    return result;

}

function process(date) {
    var parts = date.split("/");
    return new Date(parts[2], parts[1] - 1, parts[0]);
}

function notfoundData() {
    var tr = '<table class="table table-condensed table-bordered table-font cus-table" style="width: 100%;">\n\
                      <thead>\n\
                            <tr>\n\
                                <td class="text-center"><strong>Customer Name</strong></td>\n\
                                <td class="text-center"><strong>Order Date</strong></td>\n\
                                <td class="text-center"><strong>Order Id</strong></td>\n\
                                <td class="text-center"><strong>Product Id</strong></td>\n\
                                <td class="text-center"><strong>Unit Price</strong></td>\n\
                                <td class="text-center"><strong>Quantity</strong></td>\n\
                                <td class="text-center"><strong>Amount</strong></td>\n\
                                <td class="text-center"><strong>Balance</strong></td>\n\
                                <td class="text-center"><strong>Invoice Id</strong></td>\n\
                                <td class="text-center"><strong>Invoice Date</strong></td>\n\
                                <td class="text-center"><strong>Billing Id</strong></td>\n\
                                <td class="text-center"><strong>Billing Date</strong></td>\n\
                            </tr>\n\
                        </thead>\n\
                        <tbody id="id-tbody">\n\
                            <tr><td colspan="12" class="text-center">ไม่พบข้อมูลการค้นหา</td></tr>\n\
                        </tbody>\n\
              </table>';
    $("#rs_table").html(tr);
}

$("#btn_clear").click(function () {

//    $("#planlist").hide();
//    console.log("### start btn clear ###");
    $("#dateTimeStart").val("");
    $("#dateTimeEnd").val("");
    $("#customerId").val("");
    $("#productId").val("");
    $("#invoiceId").val("");
    $("#orderId").val("");
//    notfoundData();
    var table = $('#planlist').DataTable();
    table.clear().draw();

});


function loadhome(startDate, dateTimeEnd, customerId, invoiceId, productId, orderId) {


    $("#planlist").dataTable().fnDestroy();
    var table = $('#planlist').DataTable({
//        responsive: true,
//        fixedHeader: true,
//        "processing": false,
//        "serverSide": false,
//        "searching": false,
//        "bFilter": false,
//        "bLengthChange": false,
//        "paging": true,
//        "ordering": true,
//        "info": false,
        pageLength: 20,
        searching: false,
        ordering: true,
        lengthChange: false,
        ajax: {
            url: "get_tracking_list_ajax_data_table",
            type: "POST",
            async: true,
            data: {startDate: startDate,
                dateTimeEnd: dateTimeEnd,
                customerId: customerId,
                invoiceId: invoiceId,
                productId: productId,
                orderId: orderId
            },
        },
        "columns": [

            {
                "className": "text-left",
                "data": "customer_name",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },
            {
                "className": "text-center",
                "data": "order_date",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },
            {
                "className": "text-left",
                "data": "order_id",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },
            {
                "className": "text-left",
                "data": "product_id",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },
            {
                "className": "text-right",
                "data": "unit_price",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },
            {
                "className": "text-right",
                "data": "quantity",
                "render": function (data, type, row, meta) {
                    return  formatMoney(data);

                }
            },
            {
                "className": "text-right",
                "data": "amount",
                "render": function (data, type, row, meta) {
                    return   formatMoney(data);

                }
            },
            {
                "className": "text-right",
                "data": "balance",
                "render": function (data, type, row, meta) {
                    return  formatMoney(data);

                }
            },
            {
                "className": "text-left",
                "data": "invoices_id",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },
            {
                "className": "text-center",
                "data": "invoice_date",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },
            
            {
                "className": "text-right",
                "data": "invoice_quantity",
                "render": function (data, type, row, meta) {
                    if(row.invoices_id == null || row.invoices_id == ''){
                         return  '';
                    }else{
                        return  formatMoney(data);
                    }
                    

                }
            },
            
            {
                "className": "text-left",
                "data": "billing_id",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },
            {
                "className": "text-center",
                "data": "billing_date",
                "render": function (data, type, row, meta) {
                    return  data;

                }
            },
        ],
        rowReorder: true
    });
}

function formatMoney(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}