$(document).ready(function () {
	$('.customerPickerTrigger').on('click', function(){
		CustomerPicker.showPicker('selectedCustomer');
	});
});

function selectedCustomer(customer_id, customer_name, customer_type, customer_address, customer_credit, customer_type_name) {
	CustomerPicker.hidePicker();
	$('#customer_id').val(customer_id);
	$('#customer_name').val(customer_name);
	$('#customer_type').val(customer_type);
	
	setUnitPriceType(customer_type == 1 ? 2 : 1);
}

function onclickCreate() {
	$('#form').submit();
}

function setUnitPriceType(type) {
	var unit_price_types = $('input[name=unit_price_type]');

	if (unit_price_types) {
		for (var i = 0; i < unit_price_types.length; i++) {
			if (unit_price_types[i].value == type) {
				$(unit_price_types[i]).prop('checked', true);
			}
		}
	}
}