var Dataitems = {
	loadData: function (ajax, data, dataitemsId, idCol, imageCol, titleCol, desclCols, descLabels, onclick) {
		$.ajax({
			method: 'POST',
			url: ajax,
			data: data,
			success: function (resp) {
				var html = '';
				if (resp.data) {
                    var itemHtml = '';
					for (var i = 0; i < resp.data.length; i++) {
                        var image = resp.data[i][imageCol] ? resp.data[i][imageCol] : '#';
                        var id = encodeString(resp.data[i][idCol]);
                        itemHtml += '<div class="col-sm-12 col-md-3">' 
                        + '<div class="dataitem-item shadow-sm" onclick="' + onclick + '(\'' + id + '\')">' 
                        + '<div class="dataitem-item-image"><img src="' + image + '"/></div>'
                        + '<div class="dataitem-item-text"><div class="title">' + strNotNull(resp.data[i][titleCol]) + '</div>';
                        
                        for (var c = 0; c < desclCols.length; c++) {
                            itemHtml += '<div class="desc">';

                            if (descLabels) {
                                itemHtml += descLabels[c] + ': ';
                            }
                            
                            itemHtml += strNotNull(resp.data[i][desclCols[c]]) + '</div>';
                        }

                        itemHtml += '</div></div></div>';
                            
						if (i == resp.data.length-1)  {
                            html += _coverRowTag(itemHtml);
                            itemHtml = '';
						}
					}
				}
				$(('#' + dataitemsId)).html(html);
			}
		});
    },
    
    filterData: function(filterText) {
        var dataitems = $('.dataitem-item');
        for (var i = 0; i < dataitems.length; i++) {
            var title = $(dataitems[i]).find('.title');
            var descs = $(dataitems[i]).find('.desc');

            if (filterText) {
                var foundDesc = false;
                if (descs) {
                    for (var d = 0; d < descs.length; d++) {
                        if ($(descs[d]).html().indexOf(filterText) >= 0) {
                            foundDesc = true;
                            break;
                        }
                    }

                } else {
                    foundDesc = true;
                }

                if ((title && $(title).html().indexOf(filterText) < 0) && !foundDesc) {
                    $(dataitems[i]).parent().hide();
                } else {
                    $(dataitems[i]).parent().show();
                }
            } else {
                $(dataitems[i]).parent().show();
            }
        }
    }
}

function _coverRowTag(html) {
    return '<div class="row">' + html + '</div>';
}

function strNotNull(s){
    return s ? s : '';
}