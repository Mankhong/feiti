$(document).ready(function () {
    //processDefaultMode();
    
    $('.view-mode-1-button').on('click', function () {
        $('#flag_search').val("1");
        processViewMode(1);
        
    });

    $('.view-mode-2-button').on('click', function () {
        $('#flag_search').val("2");
        processViewMode(2);
        
    });

    $('#action_search').on('keyup', function () {
        Dataitems.filterData(this.value);
    });

    init_date();
    $("#rs_div").hide();
});

function init_date() {
    $("#dateTimeStart").datepicker({
                    format: 'dd/mm/yyyy',
                    autoclose: true,
                    orientation: "bottom auto",
                    todayHighlight: true,
                    todayBtn: true,
                    language: 'EN', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                    thaiyear: false, //Set เป็นปี พ.ศ.
        }).datepicker("setDate");
    $("#dateTimeEnd").datepicker({
                    format: 'dd/mm/yyyy',
                    autoclose: true,
                    orientation: "bottom auto",
                    todayHighlight: true,
                    todayBtn: true,
                    language: 'EN', //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
                    thaiyear: false, //Set เป็นปี พ.ศ.
        }).datepicker("setDate");

}

$("#btn_search").click(function () {
    onSearch();
});

function  onSearch() {
        
    var search = onValidate();
    if (search) {
        var flag_search = $('#flag_search').val();
        console.log("flag_search: " + flag_search);
        if (flag_search == "2") {
            //loadDataitems();
        } else {
            
            var startDate = $("#dateTimeStart").val();
            var dateTimeEnd = $("#dateTimeEnd").val();
            var order_id = $("#order_id").val();
            var customer_name = $("#customer_name").val();

            console.log("#### search ####");
            $("#rs_div").show();
            $("#dataitems").hide();
            
            loadDatatable(startDate, dateTimeEnd, order_id, customer_name);
            
        }
    }

}

function onValidate() {
    var startDate = $("#dateTimeStart").val();
    var dateTimeEnd = $("#dateTimeEnd").val();
    var order_id = $("#order_id").val();
    var customer_name = $("#customer_name").val();
    var search = false;
    if ((startDate === null || startDate === '') &&
            (dateTimeEnd === null || dateTimeEnd === '') &&
            (order_id === null || order_id === '') &&
            (customer_name === null || customer_name === '')) {

        var table = $('#rstable').DataTable();
        table.clear().draw();


        $('#msgModalMessage').html("กรุณาระบุเงื่อนไขการค้นหา");
        $('#msgModal').modal('show');
    } else {

        if ((startDate !== null && startDate !== '') && (dateTimeEnd !== null && dateTimeEnd !== '')) {

            if (process(startDate) > process(dateTimeEnd)) {
                search = false;
                var table = $('#rstable').DataTable();
                table.clear().draw();

                $('#msgModalMessage').html("กรุณาระบุวันที่ค้นให้ถูกต้อง");
                $('#msgModal').modal('show');

            } else {
                search = true;
            }
        } else {
            search = true;
        }
    }
    return search;
}
function process(date) {
    var parts = date.split("/");
    return new Date(parts[2], parts[1] - 1, parts[0]);
}

function closeLoading() {
    console.log("Start colse popup");
    $("#loadMe").modal("hide");
    setTimeout(function () {
        $("#loadMe").modal("hide");
    }, 3000);

}

$("#btn_clear").click(function () {

//    $("#planlist").hide();
//    console.log("### start btn clear ###");
    $("#dateTimeStart").val("");
    $("#dateTimeEnd").val("");
    $("#order_id").val("");
    $("#customer_name").val("");
    var table = $('#rstable').DataTable();
    table.clear().draw();

    $("#dataitems").hide();
    $("#rs_div").hide();

});

function exportExcel() {
    var delimiter = '&88&';
    var rows = $('#rstable').DataTable().rows({filter: 'applied'}).data();
    var value = '';
    for (var i = 0; i < rows.length; i++) {
        if (i > 0) {
            value += delimiter;
        }
        value += rows[i].id;
    }

    $('#order_ids').val(value);
    $('#export_excel_form').submit();
}

function processDefaultMode() {
    var viewMode = localStorage.getItem('viewMode');
    if (viewMode) {
        processViewMode(viewMode);
    } else {
        processViewMode(1);
    }
}

//ViewModde 1 = table, 2 = item
function processViewMode(viewMode) {
    console.log("processViewMode: " + viewMode);

    if (viewMode == 2) {
        $('.view-mode-1').hide();
        var search = onValidate();
        console.log("search: " + search);
        if (search) {
            
            loadDataitems();
        }


    } else {
        $('.view-mode-2').hide();
        $("#dataitems").hide();
        //loadDatatable();
        onSearch();
        hideSearch();
        $('.view-mode-1').show();
    }
    $('#view_mode').val(viewMode);
    localStorage.setItem('viewMode', viewMode);
}

function hideSearch() {
    $('#rstable_filter').css('display', 'none');
}

function loadDatatable(startDate, dateTimeEnd, order_id, customer_name) {


    console.log("loadDatatable");
    console.log("startDate: " + startDate);
    console.log("dateTimeEnd: " + dateTimeEnd);
    console.log("order_id: " + order_id);
    console.log("customer_name: " + customer_name);
    var rowId = 1;
    $("#rstable").dataTable().fnDestroy();
    showLoading();
            
    var t = $('#rstable').DataTable({

        pageLength: 20,
        searching: false,
        ordering: true,
        lengthChange: false,
        "info": false,
        responsive: true,
//        order: [
//            [4, 'asc'],
//            [1, 'asc']
//        ],
        ajax: {
            url: "order/get_order_list_ajax",
            type: "POST",
            
            data: {startDate: startDate,
                dateTimeEnd: dateTimeEnd,
                order_id: order_id,
                customer_name: customer_name
            },
        },
        "rowId": 'extn',
        columns: [
            {targets: 1,
                data: null,
                "className": "text-center",
                render: function (data, type, row, meta) {
                    return rowId++;
                }
            },
            {
                'data': 'id'
            },
            {
                'data': 'order_date'
            },
            {
                'data': 'customer_name'
            },
            {
                'data': 'delivery_date'
            },
            {
                'data': 'last_update_date'
            },
            {
                'data': 'last_update_user'
            },
            {
                'data': "id",
                'render': function (data, type, row, meta) {
                    var base_url = $('#base_url').val();
                    var id = encodeURIComponent(btoa(data));
                    return '<a class="btn btn-default btn-icon" href="' + base_url + 'order/edit/' + id + '"><i class="far fa-edit"></i></a>';
                }
            },
        ],
        //"order": [[ 1, 'asc' ]],


        columnDefs: [{
                "targets": 0,
                "className": "text-center"
            },
            {
                "targets": 2,
                "className": "text-center"
            },
            {
                "targets": 4,
                "className": "text-center"
            },
            {
                "targets": 5,
                "className": "text-center"
            },
            {
                "targets": 7,
                "className": "text-center"
            },
        ],
        
        initComplete: function(settings, json){
            hideLoading();
        }
    });

    //Sort without No.
//    t.on('order.dt search.dt', function () {
//        t.column(0, {
//            search: 'applied',
//            order: 'applied'
//        }).nodes().each(function (cell, i) {
//            cell.innerHTML = i + 1;
//        });
//    }).draw();


    //=== Custom Filter
    $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var match = true;
                var search = $('#action_search').val();
                if (search) {
                    match = (data[1].toLowerCase().indexOf(search.toLowerCase()) == 0 || data[3].toLowerCase().indexOf(search.toLowerCase()) == 0);
                }

                return match;
            }
    );

    $('#action_search').keyup(function (event) {
        if ($('#view_mode').val() == 1) {
            t.draw();
        }
    });
    //=== End Custom Filter

    //Onclick row.
    $('#rstable tbody').on('click', 'tr', function () {
        var t = $('#rstable').DataTable();
        var data = t.row(this).data();
        var id = encodeString(data['id']);
        window.location.href = $('#base_url').val() + 'order/edit/' + id;
    });

}

function loadDataitems() {
    var startDate = $("#dateTimeStart").val();
    var dateTimeEnd = $("#dateTimeEnd").val();
    var order_id = $("#order_id").val();
    var customer_name = $("#customer_name").val();
    $('.view-mode-2').show();
    $("#dataitems").show();
    Dataitems.loadData('order/get_order_list_ajax',
            {startDate, dateTimeEnd, order_id, customer_name},
            'dataitems', 'id', 'customer_image_path', 'id', ['customer_id', 'order_date', 'delivery_date'], ['Customer', 'Order Date', 'Delivery Date'], 'onclickItem');
}

function onclickItem(id) {
    var base_url = $('#base_url').val();
    window.location.href = base_url + 'order/edit/' + id;
}