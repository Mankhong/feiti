$(document).ready(function () {
	$('#saveButton').on('click', function () {
		generateChangedItems();
		if ($('input[name=update_ids\\[\\]]').length > 0) {
			var form = $('#form');
			var action = $('#base_url').val() + 'invoice_type/save';
			$(form).attr('action', action);
			$(form).submit();
		} else {
			showMessageModal("No data changed.");
		}
	});

	$('#createButton').on('click', function () {
		$('#itemInputError').text('');
		$('#createItemModal').modal('show');
	});

	$('#createItemButton').on('click', function () {
		var insert_invoice_name = $('#itemInput').val();
		var insert_invoice_phase = $('#itemPhaseInput').val();

		if (insert_invoice_name !== '') {
			$('#createItemModal').modal('hide');
			$('#insert_invoice_name').val(insert_invoice_name);
			$('#insert_invoice_phase').val(insert_invoice_phase);

			var form = $('#form');
			var action = $('#base_url').val() + 'invoice_type/create';
			$(form).attr('action', action);
			$(form).submit();
		} else {
			$('#itemInputError').html('Please enter invoice type<br>');
		}

	});
});

function generateChangedItems() {
	var html = '';
	var dataitems = $('#rstable .dataitem');

	if (dataitems) {
		for (var i = 0; i < dataitems.length; i++) {
			var id = $(dataitems[i]).find('.dt-id').val();
			var temp_used = $(dataitems[i]).find('.dt-temp-used').val();
			var used = $(dataitems[i]).find('input[name=used_' + id + ']:checked').val();

			if (used != temp_used) {
				html += '<input type="hidden" name="update_ids[]" value="' + id + '"/><input type="hidden" name="update_useds[]" value="' + used + '"/>';
			}
		}
	}

	$('#hiddenContainer').html(html);
}