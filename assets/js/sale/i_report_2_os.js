$(document).ready(function () {
    console.log("init deriver report");

    var invoice_id = $("#invoice_id").val();
    console.log("invoice_id: " + invoice_id);

    getHeaderReport(invoice_id);
    loadDatatable(invoice_id);

    initFontSizeByOs();
});

function initFontSizeByOs() {
    var userAgent = navigator.userAgent;
    if (userAgent.indexOf('Windows NT 5') >= 0) {
        console.log('XP!');
        localStorage.setItem('font_size_07', 'font-size: 17px;');
        localStorage.setItem('font_size_08', 'font-size: 19px;');
        localStorage.setItem('font_size_09', 'font-size: 21px;');
        localStorage.setItem('font_size_1', 'font-size: 24px;');
        localStorage.setItem('font_size_2', 'font-size: 26px;');
        localStorage.setItem('font_size_3', 'font-size: 28px;');
        localStorage.setItem('font_size_4', 'font-size: 30px;');
        localStorage.setItem('font_size_5', 'font-size: 32px;');
        localStorage.setItem('font_size_6', 'font-size: 34px;');

        localStorage.setItem('offset_1', 0);
        localStorage.setItem('offset_2', 14);
        localStorage.setItem('offset_3', 0);

    } else {
        console.log('Windows 7+');
        localStorage.setItem('font_size_07', 'font-size: 17px;');
        localStorage.setItem('font_size_08', 'font-size: 19px;');
        localStorage.setItem('font_size_09', 'font-size: 21px;');
        localStorage.setItem('font_size_1', 'font-size: 24px;');
        localStorage.setItem('font_size_2', 'font-size: 26px;');
        localStorage.setItem('font_size_3', 'font-size: 28px;');
        localStorage.setItem('font_size_4', 'font-size: 30px;');
        localStorage.setItem('font_size_5', 'font-size: 32px;');
        localStorage.setItem('font_size_6', 'font-size: 34px;');

        localStorage.setItem('offset_1', 22);
        localStorage.setItem('offset_2', 22);
        localStorage.setItem('offset_3', 10);
    }
}

function getHeaderReport(invoice_id) {
    $.ajax({
        type: 'POST',
        data: {invoice_id: invoice_id,
            customer_id: ''
        },
        url: $('#base_url').val() + 'report/get_detail_report',
        async: true,
        success: function (data) {
            console.log("data " + data.data);

            var date = data.data[0].invoice_date;
            var day = date.substr(8, 9);
            var month = date.substr(5, 2);
            var year = date.substr(0, 4);

            //set to header
            $("#v_customer_name").html(data.data[0].cus_name);
            $("#v_cusmer_address").html(data.data[0].cus_address);

            $("#v_invoice_date").html(day + '-' + getMonth(month) + '-' + year);
            $("#v_invoice_id").html(data.data[0].invoice_id);
        }
    });
}

function loadDatatable(invoice_id) {
    var pageNo = 1;
    var count = 0;
    $("#rstable").dataTable().fnDestroy();
    var t = $('#rstable').DataTable({
        "ajax": {
            "url": $('#base_url').val() + "report/get_report_2_list",
            "type": "post",
            "data": {invoice_id: invoice_id}
        },
        searching: false,
        ordering: true,
//        order: [
//            [1, 'asc']
//        ],
        lengthChange: false,
        pageLength: 7,
        columns: [{
                'data': null,
                "className": "text-center",
                'render': function (data, type, row, meta) {
                    return meta.row + 1;
                }
            },
            {targets: 0,
                data: 'order_id',
                "className": "text-center",
                render: function (data, type, row, meta) {
                    return replaceDuplicateOrder(data);
                }
            },
            {targets: 0,
                data: 'product_id',
                "className": "text-left",
                render: function (data, type, row, meta) {
                    var desc2 = (row.desc2 === null || row.desc2 === '') ? '' : '<br>' + row.desc2;
                    var lotno = row.lot_no === null ? '' : '<br>' + encodeLineBreak(row.lot_no);
                    var carton = row.carton === null ? '' : '&nbsp;' + row.carton;
                    var result = row.product_id + "<br>" + row.desc1 + desc2 + carton + lotno;
                    return result;

                }
            },
            {targets: 0,
                data: 'lot_size',
                "className": "text-center",
                render: function (data, type, row, meta) {
                    var product_lots_size = formatCurrency(row.product_lots_size, 0) + " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;x";
                    var quantity = row.invoice_quantity;
                    var div = formatCurrencyToNumber(quantity) / formatCurrencyToNumber(product_lots_size);
                    return product_lots_size + "&nbsp;&nbsp;" + formatCurrency(div, 0);

                }
            },

            {targets: 0,
                data: 'quantity',
                "className": "text-right",
                render: function (data, type, row, meta) {
                    var total = (parseFloat(row.product_lots_size === null ? 1 : row.product_lots_size) * parseFloat(row.invoice_quantity));
                    var quantity = formatCurrency(row.invoice_quantity, 0);
                    var resul = "<span class=\"text-left\">" + quantity + "</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n\
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n\
                                      <span class=\"text-right\"></span>";
                    return  resul;
                }
            },
        ],
        "defaultContent": '',

        fnDrawCallback: function (settings) {
            //appendSummary();

            count++;
            if (count > 1) {
                console.log("count: " + count);
                var info = $('#rstable').DataTable().page.info();
                var page = parseInt(info.page) + parseInt(1);
                var totaldata = 0;
                var pagele = 5;
                var result = 0;

                var unit_price = 0;
                var quantity = 0;
                $.ajax({
                    url: $('#base_url').val() + "report/get_report_2_list",
                    type: 'POST',
                    data: {invoice_id: invoice_id,
                        customer_id: ''
                    },
                    async: false,
                    dataType: 'json',
                    success: function (data) {
                        console.log("datax: " + data.data);

                        var internal_note = data.data[0].internal_note;

                        totaldata = data.data.length;
                        result = totaldata / pagele;
                        console.log("page: " + page + "result: " + Math.ceil(result));
                        if (Math.ceil(result) === page) {
                            appendSummary(internal_note);
                        }
                    }
                });
            }


        },

    });


}

function appendSummary(internal_note) {
    console.log("start appendSummary");
    var html = '<tr>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                </tr>\n\
                <tr>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                </tr>\n\
                <tr>\n\
                 <td colspan="6">\n\
                    FM-CO-13 REV 00/03-01-2006\n\
                 </td>\n\
                </tr>';
    $('tbody').append(html);

    /*                <tr>\n\
     <td align="center" colspan="4">\n\
     Total\n\
     </td>\n\
     <td align="right">1000</td>   \n\
     </tr>\n\
     */
}

$("#btn_print").click(function () {


    var invoice_id = $("#invoice_id").val();
    initViewReport(invoice_id);
    var mode = 'popup'; //popup 
    var close = mode == "popup";
    var options = {
        mode: "iframe",
        standard: "html5",
        retainAttr: ["id", "class"],
        printDelay: 500,
        printAlert: true
    };
    $("div.printableArea").printArea(options);
});

function formatMoney(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

function setHeader(invoice_id, totalpage) {

    var details = [];
    details = get_detail_arr(invoice_id);
    var date = details.data[0].invoice_date;
    var day = date.substr(8, 9);
    var month = date.substr(5, 2);
    var year = date.substr(0, 4);

    var begin_page_height = 20 + parseFloatOrNumeric(localStorage.getItem('offset_1'));

    var header = '<div class="container">\n\
                    <div class="row pt-2">\n\
                        <div class="col-12">\n\
                            <div class="row">\n\
                                <div class="col-2" style="display: none;">\n\
                                    <image src="../assets/images/default_image.jpg">\n\
                                </div>\n\
                                    <table>\n\
                                            <tr>\n\
                                                <td height="130"></td>\n\
                                            </tr>\n\
                                    </table>\n\
                                <div class="col-10" style="padding-left: 3.0rem !important;">\n\
                                    <div class="invoice-title">\n\
                                        <div style="display: none;">\n\
                                            <h2></h2>\n\
                                            <hr style="margin-top: 0px;margin-bottom: 15px;">\n\
                                            <small>\n\
                                                <br>\n\
                                                <br>\n\
                                                <br>\n\
                                            </small>\n\
                                        </div>\n\
                                    </div>\n\
                                </div>\n\
                            </div>\n\
                            \n\
                            <div style="height: ' + begin_page_height + 'px;"></div>';

    return header;
}

function setHeader2(invoice_id, totalpage) {

    var details = [];
    details = get_detail_arr(invoice_id);
    var date = details.data[0].invoice_date;
    var day = date.substr(8, 9);
    var month = date.substr(5, 2);
    var year = date.substr(0, 4);

    var common_style = 'font-family: \'Calibri\'; color: #000;';
    var font_size_1 = localStorage.getItem('font_size_08');
    
    var header2 = '<div class="row pt-3">\n\
                                <div class="col-6 p-0" style="height:158px; border: 0px solid #333;">\n\
                                \n\
                                    <div class="panel-body" style="border: 0px solid #d2c6c6; width: 490px;">\n\
                                        <div style="height: 30px;"></div>\n\
                                        <div class="pl-0">\n\
                                            <address>\n\
                                                <span id="v_customer_name" style="' + font_size_1 + common_style + '">' + details.data[0].cus_name + '</span><br>\n\
                                                <span id="v_cusmer_address" style="' + font_size_1 + common_style + '">' + details.data[0].cus_address + '</span><br><br>\n\
                                            </address>\n\
                                        </div>\n\
                                    </div>\n\
                                </div>\n\
                                \n\
                                <div class="col-6 text-center" style="padding-top: 10px; height: 113px;">\n\
                                    <div style="height: 130px;"></div>\n\
                                    <address style="text-align: left; padding-left: 190px;">\n\
                                        <span id="v_invoice_date" style="' + font_size_1 + common_style + '">' + day + '-' + getMonth(month) + '-' + year + '</span><br>\n\
                                        <span id="v_invoice_id" style="' + font_size_1 + common_style + '">' + details.data[0].invoice_id + '</span> &nbsp;&nbsp;';

    return  header2;
}
function subHeader() {

    var html = '';
    var _subHeader = '<br></address>\n\
                                </div>\n\
                            </div>\n\
                        </div>\n\
                    </div>\n\
                     <table>\n\
                          <tr>\n\
                              <td height="' + (72 + parseFloatOrNumeric(localStorage.getItem('offset_2'))) + '"></td>\n\
                           </tr>\n\
                     </table>';

    var common_style = 'border-top: 0px solid #dee2e6; height: 35px;';

    var tbody = '<div class="row pt-3" style="margin-right: 10px; margin-left: -30px;">\n\
                                    <div class="col-md-12">\n\
                                      <div class="panel panel-default" style="height:786px;">\n\
                                          <div>\n\
                                              <table class="table table-condensed tablecustom" border="0" style="table-layout: fixed; width:100%;">\n\
                                                  <thead>\n\
                                                      <tr >\n\
                                                        <td class="text-center" style="width: 35px; ' + common_style + '" ><strong></strong></td>\n\
                                                        <td class="text-center" style="width: ' + (215 + parseFloatOrNumeric(localStorage.getItem('offset_3'))) + 'px; ' + common_style + '"><strong></strong></td>\n\
                                                        <td class="text-center" style="width: 410px; ' + common_style + '"><strong></strong></td>\n\
                                                        <td class="text-center" style="width: 190px; ' + common_style + '"><strong></strong></td>\n\
                                                        <td class="text-center" style="' + common_style + '"><strong></strong></td>\n\
                                                      </tr>\n\
                                                  </thead>\n\
                                                  <tbody id="body_content" >';

    html += _subHeader += tbody;

    return html;
}

function get_detail_arr(invoice_id) {

    console.log("get_detail_arr: ");
    var arr = [];
    $.ajax({
        url: $('#base_url').val() + 'report/get_detail_report',
        type: 'POST',
        data: {invoice_id: invoice_id,
            customer_id: ''
        },
        async: false,
        dataType: 'json',
    }).done(function (data) {
        console.log("report data: " + data);
        if (data === null) {
            arr = [];
        } else {
            arr = data;
        }
    });

    return arr;
}

function setLastBody(totalpage) {
    var end_page_height = 185;

    //console.log("setLastBody: "+ setLastBody);
    var lastEnterDiv2 = '';
    var lastEnterDiv3 = '';
    for (var i = 0; i < 18; i++) { //38
        lastEnterDiv2 += '<table border="0">\n\
                                            <tr>\n\
                                                <td height="10"></td>\n\
                                            </tr>\n\
                                </table>';
    }

    lastEnterDiv3 += '<table border="0">\n\
                                            <tr>\n\
                                                <td height="7"></td>\n\
                                            </tr>\n\
                                </table>';


    var lastbody = '';
    if (totalpage > 1) {
        lastbody = '              </tbody>\n\
                                </table>\n\
                              </div>\n\
                          </div>\n\
                      </div>\n\
                   </div>\n\
                </div>' +
                '<div style="height: ' + end_page_height + 'px;"></div>';
                //lastEnterDiv2 + lastEnterDiv3;
    } else {
        lastbody = '              </tbody>\n\
                                </table>\n\
                              </div>\n\
                          </div>\n\
                      </div>\n\
                   </div>';
    }

    return lastbody;
}

function setFooter() {
    var footer = '  <br>\n\
                    <br>\n\
                    <br>\n\
                    <div class="row" pb-4>\n\
                        <div class="col-4">\n\
                            <div align="center">\n\
                                <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"></div>\n\
                                <div><span>Check by</span></div>\n\
                            </div>\n\
                        </div>\n\
                        <div class="col-4">\n\
                            <div align="center">\n\
                                <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"></div>\n\
                                <div><span>Approve by</span></div>\n\
                            </div>\n\
                        </div>\n\
                        <div class="col-4">\n\
                            <div align="center">\n\
                                <div style="border-bottom: 1px dashed #000;text-decoration: none; width: 100%; display: inline-block;"></div>\n\
                                <div><span>Receipt by</span></div>\n\
                            </div>\n\
                        </div>\n\
                    </div>\n\
                </div>\n\
            ';
    return footer;
}

function initViewReport(invoice_id) {

    console.log("initViewReport: " + invoice_id);
    var total_row = 7;
    var modd = 2;
    $.ajax({
        url: $('#base_url').val() + "report/get_report_2_list",
        data: {invoice_id: invoice_id},
        async: false,
        type: 'POST',
        success: function (data) {
            console.log("datax: " + data.data.length);
            var page = data.data.length / total_row;
            console.log("page: " + Math.ceil(page));
            var totalpage = data.data.length / total_row;
            //var div = '<div class="printableArea" id="printableAreatest"></div>';
            //$("#contentdiv").html(div);

            var pagArr = [];
            var fiveArray = [];
            var index = 0;
            var indexFilve = 0;
            var result = '';
            var count = 0;
            for (var i = 0; i < data.data.length; i++) {
                fiveArray[indexFilve++] = data.data[i];

                if ((i + 1) % total_row === 0 || i === data.data.length - 1) {
                    pagArr[index++] = fiveArray;
                    count++;
                    fiveArray = [];
                    indexFilve = 0;
                }
            }

            var totaldata = data.data.length;
            var result = totaldata / total_row;
            console.log("totaldata: " + totaldata);
            console.log("count: " + count);
            totaldata = data.data.length;
            console.log("page: " + count + "result: " + Math.ceil(result));

            if (Math.ceil(totalpage) <= 1) {
                result = formatCurrency(result, 1);
                for (var i = 0; i < pagArr.length; i++) {

                    var internal_note = data.data[0].internal_note;
                    var _note = '<tr>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;" colspan="2"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    </tr>';

                    result += setContent(invoice_id, pagArr[i], Math.ceil(page), i, _note, '', 0);

                }
            } else {
                result = formatCurrency(result, 1);
                var span = '';
                var _pageCount = 0;
                for (var i = 0; i < pagArr.length; i++) {
                    _pageCount += 1;
                    if (Math.ceil(result) !== count) {
                        var internal_note = data.data[0].internal_note;
                    } else {
                        var internal_note = '&nbsp;';
                    }

                    span = '<span style="' + localStorage.getItem('font_size_08') + 'font-family: \'Calibri\'; color: #000;">'+_pageCount+'/' +  Math.ceil(page) + '</span>';
                    var _note = '<tr>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;" colspan="2"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    </tr>';

                    result += setContent(invoice_id, pagArr[i], Math.ceil(page), i, _note, span, _pageCount);

                }
            }

//            console.log(pagArr);
//            for (var i = 0; i < pagArr.length; i++) {
//                result += setContent(invoice_id, pagArr[i],Math.ceil(page),i);
//            }

            var z = totaldata % modd;
            console.log("mod z: " + z);
            if (z === 0) {
                console.log("result substring === 0");
                result = '<br>' + result.substring(1, result.length);
            } else {
                console.log("result substring !== 0");
                result = '<br>' + result.substring(3, result.length);
            }
            console.log("resultresultresult : "+ result);
            $("#printableAreatest").html(result);


        }
    });
}

function setContent(invoice_id, data, totalpage, index, _note, span, _pageCount) {

    console.log("setContent _pageCount: " + _pageCount);
    var body = '';
    var count = 0;
    var countsum = 0;
    var unit_price = 0;
    var quantity = 0;
    //
    var br = '';
    var brlotno = '';
    for (var i = 0; i < data.length; i++) {
        count++;
        countsum++;
        var dese2 = (data[i].desc2 === null || data[i].desc2 === '') ? '' : '<br>' + data[i].desc2;

        if (data[i].desc2 === null || data[i].desc2 === '') {
            br = '<br> &nbsp;';
        }
        var carton = data[i].carton === null ? '&nbsp; <br>' : '&nbsp;(' + data[i].carton + ')<br>';

        if (data[i].lot_no === null || data[i].lot_no === '') {
            brlotno = '<br> &nbsp; <br>';
        }

        var lotno = data[i].lot_no === null ? '<br> &nbsp; <br>' : encodeLineBreak(data[i].lot_no);

        var common_style = 'border-top: 0px solid #dee2e6; font-family: \'Calibri\'; color: #000;';
        var line_height = 'line-height: 0.9;';
        var font_size_1 = 'font-size: 24px;';
        var font_size_2 = 'font-size: 28px;';
        var font_size_12 = localStorage.getItem('font_size_07');
        var font_size_22 = localStorage.getItem('font_size_09');

        var div = formatCurrencyToNumber(data[i].invoice_quantity) / formatCurrencyToNumber(data[i].product_lots_size);
        body += '<tr role="row" class="odd">\n\
                        <td style="padding-left: 2px; ' + common_style + font_size_12 + line_height + '" class="text-left">' + (count + (7 * index)) + '</td>\n\
                        <td style="' + common_style + font_size_12 + line_height + '" class="text-left">' + replaceDuplicateOrder(data[i].order_id) + '</td>\n\
                        <td style="padding-left: 10px; white-space: nowrap; ' + common_style + font_size_12 + line_height + '" class="text-left"><span style="text-overflow: ellipsis;">' + data[i].product_id + '<br>' + data[i].desc1 + dese2 + carton + lotno + brlotno+  '</spna> </td>\n\
                        <td style="padding-right: 10px; ' + common_style + font_size_22 + line_height + '" class=" text-right">' + formatCurrency(data[i].product_lots_size, 0) + ' &nbsp;&nbsp;x ' + formatCurrency(div, 0) + '</td>\n\
                        <td style="padding-right: 10px; ' + common_style + line_height + font_size_22 + '" class=" text-right">' + formatCurrency((data[i].invoice_quantity), 0) + '</td>\n\
                    </tr>';

        unit_price += parseFloat(data[i].unit_price);
        quantity += parseFloat(data[i].quantity);

    }


    var h2 = '';
    var allh = '';
    if (_pageCount > 1) {
        var enter = '';

        enter = '<table border="0">\n\
                    <tr>\n\
                      <td height="28"></td>\n\
                    </tr>\n\
                 </table>';
        h2 = setHeader2(invoice_id, totalpage);
        allh = enter += h2;
    } else {
        h2 = setHeader2(invoice_id, totalpage);
        allh = h2;
    }


    var h = setHeader(invoice_id, totalpage);
    var subh = subHeader();
    var las = setLastBody(totalpage);
    var footer = setFooter();
    var result = h += allh += span += subh += body += _note += las;
    var html = result; //+= footer
    return html;
}

function encodeLineBreak(str) {
    return str.replace(/(?:\r\n|\r|\n)/g, '<br>');
}

function decodeLineBreak(str) {
    str = str.replace(/\<br\>/g, "\n");
    str = str.replace(/\<br \/\>/g, "\n");
    return str;
}

function getMonth(month) {
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    if (month === '01') {
        return monthNames[0];
    } else if (month === '02') {
        return monthNames[1];
    } else if (month === '03') {
        return monthNames[2];
    } else if (month === '04') {
        return monthNames[3];
    } else if (month === '05') {
        return monthNames[4];
    } else if (month === '06') {
        return monthNames[5];
    } else if (month === '07') {
        return monthNames[6];
    } else if (month === '08') {
        return monthNames[7];
    } else if (month === '09') {
        return monthNames[8];
    } else if (month === '10') {
        return monthNames[9];
    } else if (month === '11') {
        return monthNames[10];
    } else if (month === '12') {
        return monthNames[11];
    }
}