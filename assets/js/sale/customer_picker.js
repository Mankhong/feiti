var CustomerPicker = {
	showPicker: function (callbackFunctionName) {
        $("#customerPickerTable").dataTable().fnDestroy();
		$('#customerPickerModal').modal('show');

		var t = $('#customerPickerTable').DataTable({
            ajax: $('#base_url').val() + 'customer/get_customer_list_ajax',
            select: true,
			searching: true,
			ordering: true,
			order: [
				[1, 'asc']
			],
			lengthChange: false,
			pageLength: 10,
			columns: [{
					'data': null,
					'render': function (data, type, row, meta) {
						return meta.row + 1;
					}
				},
				{
					'data': 'id'
				},
				{
					'data': 'name'
				},
				{
					'data': 'customer_type_name'
				},
				{
					'data': 'business_type_name'
                },
			],
			columnDefs: [{
					"targets": 0,
					"className": "text-center"
				},
				{
					"targets": 1,
					"className": "text-center"
				},
			],
			fnDrawCallback: function (settings) {
				$('input[aria-controls=customerPickerTable]').focus();
				CustomerPicker.setSearchEvent();
			},
		});

		//=== Custom Filter
		$.fn.dataTable.ext.search.push(
			function (settings, data, dataIndex) {
				var search = $('#customerPickerTable_filter input[type=search]').val();
				var match = true;

				if (search != '') {
					return (data[1].toLowerCase()).indexOf(search.toLowerCase()) == 0;
				}

				return match;
			}
		);

		//Sort without No.
		t.on('order.dt search.dt', function () {
			t.column(0, {
				search: 'applied',
				order: 'applied'
			}).nodes().each(function (cell, i) {
				cell.innerHTML = i + 1;
			});
		}).draw();
		
		//Onclick row.
		$('#customerPickerTable tbody').unbind('click');
		$('#customerPickerTable tbody').on('click', 'tr', function () {
			var t = $('#customerPickerTable').DataTable();
			var data = t.row( this ).data();
			CustomerPicker.selectedCallBack(data);
		});
    },
    
    hidePicker: function() {
        $('#customerPickerModal').modal('hide');
		$('#customerPickerTable_filter input[type=search]').val('');
	},

	setSearchEvent: function() {
		$('input[aria-controls=customerPickerTable]').unbind('keydown');
		$('input[aria-controls=customerPickerTable]').on('keydown', function(event){
			if (event.keyCode === 13) {
				event.preventDefault();
				var trs = $('#customerPickerTable tbody tr');
				if (trs.length === 1 && !$(trs[0]).hasClass('dataTables_empty')) {
					$(trs[0]).click();
				}
			}
		});
	},

	selectedCallBack: function(data) {},
}