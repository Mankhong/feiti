$(document).ready(function () {
	$('.customerPickerTrigger').on('click', function(){
		CustomerPicker.showPicker('selectedCustomer');
	});
});

function selectedCustomer(customer_id, customer_name, customer_type, customer_address, customer_credit, customer_type_name) {
	CustomerPicker.hidePicker();
	$('#customer_id').val(customer_id);
	$('#customer_name').val(customer_name);
	$('#customer_type').val(customer_type);
}

function onclickSave() {
	$('#form').submit();
}