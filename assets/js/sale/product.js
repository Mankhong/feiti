$(document).ready(function () {
	processDefaultMode();

	$('.view-mode-1-button').on('click', function () {
		processViewMode(1);
	});

	$('.view-mode-2-button').on('click', function () {
		processViewMode(2);
	});

	$('#action_search').on('keyup', function () {
		Dataitems.filterData(this.value);
	});
});

function processDefaultMode() {
	var viewMode = localStorage.getItem('viewMode');
	if (viewMode) {
		processViewMode(viewMode);
	} else {
		processViewMode(1);
	}
}

//ViewModde 1 = table, 2 = item
function processViewMode(viewMode) {
	if (viewMode == 2) {
		$('.view-mode-1').hide();
		loadDataitems();
		$('.view-mode-2').show();

	} else {
		$('.view-mode-2').hide();
		loadDatatable();
		hideSearch();
		$('.view-mode-1').show();
	}
	$('#view_mode').val(viewMode);
	localStorage.setItem('viewMode', viewMode);
}

function hideSearch() {
	$('#rstable_filter').css('display', 'none');

	// var html = $('#rstable_filter').html();
	// html = '<a class="btn btn-default btn-sm"><i class="far fa-file-excel"></i> Excel</a>' +
	// 	'<a class="btn btn-default btn-sm ml-1 mr-1"><i class="far fa-file-pdf"></i> Pdf</a>' +
	// 	html;
	// $('#rstable_filter').html(html);
}

function loadDatatable() {
	$("#rstable").dataTable().fnDestroy();

	var t = $('#rstable').DataTable({
		ajax: 'product/get_product_list_ajax',
		searching: true,
		ordering: true,
		order: [
			[1, 'asc']
		],
		lengthChange: false,
		pageLength: 20,
		columns: [{
				'data': null,
				'render': function (data, type, row, meta) {
					return meta.row + 1;
				}
			},
			{ 'data': 'id' },
			{ 'data': 'desc1' },
			{ 'data': 'customer_name' },
			{ 'data': 'product_type_name' },
			{ 'data': 'invoice_type_name' },
			{ 'data': 'process_name' },
			{ 'data': 'last_update_date' },
			{ 'data': 'last_update_user' },
			{
				'data': "seq",
				'render': function (data, type, row, meta) {
					var base_url = $('#base_url').val();
					var seq = encodeString(data);
					return '<a class="btn btn-default btn-icon" href="' + base_url + 'product/edit/' + seq + '"><i class="far fa-edit"></i></a>';
				}
			},
			{ 'data': 'boi_desc' },
			{ 'data': 'domestic_price' },
			{ 'data': 'export_price' },
			{ 'data': 'lot_size' },
			{ 'data': 'internal_note' },
			{ 'data': 'carton' },
			{ 'data': 'carton_temp' },
			{ 'data': 'invoice_type_name' },
			{ 'data': 'process_name' },
			{ 'data': 'desc2' },
			{ 'data': 'customer_id' },
			{ 'data': 'customer_name' },
		],
		columnDefs: [{
				"targets": 0,
				"className": "text-center"
			},
			{
				"targets": 6,
				"className": "text-center"
			},
			{
				"targets": 8,
				"className": "text-center"
			},
			{
				"targets": 10,
				"className": "d-none"
			},
			{
				"targets": 11,
				"className": "d-none"
			},
			{
				"targets": 12,
				"className": "d-none"
			},
			{
				"targets": 13,
				"className": "d-none"
			},
			{
				"targets": 14,
				"className": "d-none"
			},
			{
				"targets": 15,
				"className": "d-none"
			},
			{
				"targets": 16,
				"className": "d-none"
			},
			{
				"targets": 17,
				"className": "d-none"
			},
			{
				"targets": 18,
				"className": "d-none"
			},
			{
				"targets": 19,
				"className": "d-none"
			},
			{
				"targets": 20,
				"className": "d-none"
			},
			{
				"targets": 21,
				"className": "d-none"
			},
		],
		fnDrawCallback: function (settings) {
			//initExportButtons('#rstable', 'Products', generateReportName('PRODUCTS'), [1, 2, 19, 10, 20, 21, 11, 12, 17, 18, 13, 15, 16]);
		},
	});

	//Sort without No.
	t.on('order.dt search.dt', function () {
		t.column(0, {
			search: 'applied',
			order: 'applied'
		}).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();

	//=== Custom Filter
	$.fn.dataTable.ext.search.push(
		function (settings, data, dataIndex) {
			var match = true;
			var search = $('#action_search').val();
			if (search != '') {
				match = (data[1].toLowerCase().indexOf(search.toLowerCase()) == 0 || data[2].toLowerCase().indexOf(search.toLowerCase()) == 0);
			}

			return match;
		}
	);

	$('#action_search').keyup(function (event) {
		if ($('#view_mode').val() == 1) {
			t.draw();
		}
	});
	//=== End Custom Filter

	$('#rstable tbody').on('click', 'tr', function () {
		var data = t.row(this).data();
		var seq = encodeString(data['seq']);
		window.location.href = $('#base_url').val() + 'product/edit/' + seq;
	});

}

function loadDataitems() {
	Dataitems.loadData('product/get_product_list_ajax', null, 'dataitems', 'seq', 'image_path', 'id', ['customer_id', 'desc1', 'product_type_name'], ['Customer', 'Description 1', 'Product Type'], 'onclickItem');
}

function onclickItem(seq) {
	var base_url = $('#base_url').val();
	window.location.href = base_url + 'product/edit/' + seq;
}

function exportExcel() {
	var delimiter = '&88&';
	var rows = $('#rstable').DataTable().rows( {filter : 'applied'}).data();
	var value = '';
	for (var i = 0; i < rows.length; i++) {
		if (i > 0) {
			value += delimiter;
		}
		value += rows[i].seq;
	}

	$('#product_seqs').val(value);
	$('#export_excel_form').submit();
}