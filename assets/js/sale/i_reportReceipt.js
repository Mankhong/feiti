$(document).ready(function () {

    var billing_id = $("#billing_id").val();
    console.log("billing_id: " + billing_id);

    var receipt_date = $("#receipt_date").val();
    console.log("receipt_date: " + receipt_date);
    var day = receipt_date.substr(0, 2);
    var month = receipt_date.substr(3, 2);
    var year = receipt_date.substr(6, 4);

    console.log("day: " + day);
    console.log("month: " + month);
    console.log("year: " + year);
    
    var cusaddress = $("#id_customer_address").val();
    console.log("cusaddress: " + cusaddress);
    $('#spancusAddress').html(encodeLineBreak(cusaddress));
    
    var data_ = day + '-' + getMonth(month) + '-' + year;
    $("#html_receipt_date").html(data_);
    appendLoadTable(billing_id);
});


function appendLoadTable(billing_id) {

    $.ajax({
        url: $('#base_url').val() + "report/get_data_billing_invoices",
        type: 'POST',
        data: {billing_id: billing_id,
            customer_id: ''
        },
        async: false,
        dataType: 'json',
        success: function (data) {
            //console.log("datax: " + data.data);
            var total = 0;
            var sumTotal= 0;
            var count = 0;
            var tr;
            var html = '';
            var invoice_id = '';
           
            for (var i = 0; i < data.data.length; i++) {
                //set receipt id
                if (i == 0) {
                    $('#receipt_id').html(data.data[i].receipt_id);
                }

                count ++;
                invoice_id += data.data[i].invoice_id + ", ";
                total +=  parseFloatOrNumeric(data.data[i].total_amount);
                console.log("##total: "+ total);
                sumTotal = total;        
            }
            console.log("sumTotal# "+ sumTotal);
            
             tr = '<tr>\n\
                        <td class="text-left" id="number"> INV NO. <br>'  + invoice_id.slice(0,-2) + '</td>\n\
                        <td class="text-right" >' + formatCurrency(sumTotal,2) + '</td>\n\
                </tr>';
            
            var sum = '<tr>\n\
                        <td class="text-center" style="border-top: 1px solid back;">' + BAHTTEXT(sumTotal) + '</td>\n\
                        <td class="text-right" style="border-top: 1px solid back;">' + formatCurrency(sumTotal, 2) + '</td>\n\
                    </tr>';
            
            html = tr += sum;
            $("#id-tbody").append(html);
        }
    });

}

function loadDatatable(billing_id) {
    console.log("loadDatatable billing_id: " + billing_id);
    var count = 0;
    $("#rstable").dataTable().fnDestroy();
    var t = $('#rstable').DataTable({
        "ajax": {
            "url": $('#base_url').val() + "report/get_data_billing_invoices",
            "type": "post",
            "data": {billing_id: billing_id}
        },

        "type": "POST",
        "async": true,
        searching: false,
        ordering: true,
        lengthChange: false,
        pageLength: 7,
        
        
        columns: [

            {targets: 0,
                data: 'invoice_id',
                "className": "text-left",
                render: function (data, type, row, meta) {
                    var invoice_id = '';
                    var result = invoice_id += row.invoice_id +", ";
                    return result;

                }
            },

            {targets: 0,
                data: 'total_amount',
                "className": "text-right",
                render: function (data, type, row, meta) {
                    return formatCurrency(data);
                }
            },
        ],

        "defaultContent": '',

        fnDrawCallback: function (settings) {
            calculateSummary();
            /*
            count++;
            if (count > 1) {
                //console.log("count: " + count);
                var info = $('#rstable').DataTable().page.info();
                var page = parseInt(info.page) + parseInt(1);
                var totaldata = 0;
                var pagele = 7;
                var result = 0;

                var total_amount = 0;
                var _total_vat = 0;
                var sumAll = 0;


                $.ajax({
                    url: $('#base_url').val() + "report/get_data_billing_invoices",
                    type: 'POST',
                    data: {billing_id: billing_id,
                        customer_id: ''
                    },
                    async: false,
                    dataType: 'json',
                    success: function (data) {
                        //console.log("datax: " + data.data);
                        var total = 0;
                        var amount = 0;
                        for (var i = 0; i < data.data.length; i++) {

                            total_amount += formatCurrencyToNumber(formatCurrency(data.data[i].total_amount, 2));


                            _total_vat += formatCurrencyToNumber(formatCurrency(((total_amount * 7) / 100), 2));

                            sumAll = +parseFloat(total_amount) + parseFloat(_total_vat);

                        }
                        totaldata = data.data.length;
                        result = totaldata / pagele;
                        console.log("page: " + page + "result: " + Math.ceil(result));
                        if (Math.ceil(result) === page) {
                            console.log("_total_vat: " + _total_vat + " sumAll: " + formatCurrency(sumAll, 2));
                            appendSummary(sumAll);
                        }
                    }
                });
            }
            */
        },

    });
    
}

function calculateSummary() {
    var vat = 7;
    var sum = 0;
    var table = $('#rstable').DataTable();
    var table_data = table.rows().data();
    var page_info = table.page.info();
    for (var i = 0; i < table_data.length; i++) {
        sum += calculateGrandTotal(vat, table_data[i]['total_amount']);
    }

    if ((page_info.page + 1) == page_info.pages) {
        appendSummary(sum);
    }
}

function calculateGrandTotal(vat, total_amount) {
    var total_vat = roundup((vat / 100) * total_amount, 2);
    var result = parseFloatOrNumeric(total_amount) + parseFloatOrNumeric(total_vat);
    return result;
}

function appendSummary(sumAll) {
    //console.log("start appendSummary");

    var html = '<tr>\n\
                    <td class="text-left" style="border-top: 1px solid back;">' + BAHTTEXT(sumAll) + '</td>\n\
                    <td class="text-right" style="border-top: 1px solid back;">' + formatCurrency(sumAll, 2) + '</td>\n\
                </tr>';
    $('tbody').append(html);
}

$("#btn_print").click(function () {



    var billing_id = $("#billing_id").val();
    initViewReport(billing_id);

    var mode = 'popup'; //popup 
    var close = mode == "popup";
    var options = {

        mode: "iframe",
        popClose: close,
        header: false,
        popX: 5,
    };
    $("div.printableArea").printArea(options);
});

function setHtml(receipt_id, customer_name, customer_address, html_receipt_date) {
    console.log("setHtml: ");

    var common_style = 'font-family: \'Angsana New Bold\'; color: #000;';
    var font_size_1 = 'font-size: 32px;';
    var font_size_2 = 'font-size: 26px;';
    var begin_page_height = 170;

    var html = '<div class="container">\n\
    <div class="row pt-2">\n\
        <div class="col-12">\n\
            <div class="row">\n\
                <div class="col-2">\n\
                    <div style="width: 140px; height: ' + begin_page_height + 'px;"> </div>\n\
                </div>\n\
                <div class="col-10" style="padding-left: 1.0rem !important;">\n\
                    <div class="invoice-title">\n\
                        <div>\n\
                            <h3></h3>\n\
                            <small>\n\
                                <br>\n\
                            </small>\n\
                        </div>\n\
                    </div>\n\
                </div>\n\
            </div>\n\
            <div class="row">\n\
                <div class="col-2">\n\
                </div>\n\
                <div class="col-5">\n\
                    <span></span>\n\
                </div>\n\
                <div class="col-5">\n\
                    <span></span>\n\
                </div>\n\
            </div>\n\
            <div class="row pt-4">\n\
                <div class="col-2">\n\
                </div>\n\
                <div class="col-3">\n\
                </div>\n\
                <div class="col-2 text-center">\n\
                    <address>\n\
                        <strong></strong><br>\n\
                        <strong></strong><br>\n\
                    </address>\n\
                </div>\n\
                <div class="col-5">\n\
                </div>\n\
            </div>\n\
            <table><tr><td height="65"></td></tr></table>\n\
            <div class="row pt-3">\n\
                <div class="col-2 text-left"  style="width: 150px !important; flex: 0 0 11.666667%;">\n\
                    <strong> </strong> <br> <strong> </strong>\n\
                </div>\n\
                <div class="col-2" style="height:62px;"> \n\
                    <div style="padding-top: 32px;"><span id="html_receipt_date" style="' + common_style + font_size_1 + '">' + html_receipt_date + '</span> </div>\n\
                </div>\n\
                <div class="col-4"></div>\n\
                <div class="col-1">\n\
                    <div style="display: inline-block;">\n\
                        <strong> </strong>\n\
                        <br> <strong></strong>\n\
                    </div>\n\
                </div>\n\
                <div class="col-3 pt-2 text-left" style="height:62px;">\n\
                    <div style="padding-top: 25px;"><span style="padding-left: 70px; ' + common_style + font_size_1 + '">' + receipt_id + '</span></div>\n\
                </div>\n\
            </div>\n\
            <table><tr><td style="height: 60px;"></td></tr></table>\n\
            <div class="row">\n\
                <div class="col-md-12">\n\
                    <div class="panel panel-default">\n\
                        <div class="">\n\
                          <table border="0" style="overflow: hidden;">\n\
                                <thead>\n\
                                    <tr>\n\
                                        <td class="text-left" colspan="2" style="width:50%; padding-left: 65px; height:100px; vertical-align: top;">\n\
                                            <div style="line-height: 0.7; width: 500px;' + common_style + font_size_1 + '">' + customer_name + '</div>\n\
                                        </td>\n\
                                        <td class="text-left" colspan="2" style="width:50%; padding-left: 85px; vertical-align: top;">\n\
                                            <div style="line-height: 0.7; width: 500px;' + common_style + font_size_2 + '">' + encodeLineBreak(customer_address) + '</div>\n\
                                        </td>\n\
                                    </tr>\n\
                                </thead>\n\
                            </table>\n\
                        </div>\n\
                    </div>\n\
                </div>\n\
            </div>\n\
            <table><tr><td height="60"></td></tr></table>\n\
            <div class="row p-0">\n\
                <div class="col-md-12">\n\
                    <div class="panel panel-default">\n\
                        <div class="table-responsive" stype="overflow: hidden;">\n\
                                <table style="overflow: hidden;">\n\
                                <tbody id="body_content">';
    return html;
}

function lastHtml() {
    var last_content = '</tbody>\n\
                                        </table>\n\
                                    </div>\n\
                                </div>\n\
                            </div>\n\
                        </div>\n\
                        <div class="row">\n\
                            <div class="col-12">\n\
                            </div>\n\
                        </div>\n\
                        <div class="row" pb-4>\n\
                            <div class="col-4">\n\
                                <div align="center">\n\
                                    <div style="border-bottom: 0px dashed #000;text-decoration: none; width: 100%; display: inline-block;"></div>\n\
                                    <div><span></span></div>\n\
                                </div>\n\
                            </div>\n\
                            <div class="col-4">\n\
                                <div align="center">\n\
                                    <div style="border-bottom: 0px dashed #000;text-decoration: none; width: 100%; display: inline-block;"></div>\n\
                                    <div><span></span></div>\n\
                                </div>\n\
                            </div>\n\
                            <div class="col-4">\n\
                                <div align="center">\n\
                                    <div style="border-bottom: 0px dashed #000;text-decoration: none; width: 100%; display: inline-block;"></div>\n\
                                    <div><span></span></div>\n\
                                </div>\n\
                            </div>\n\
                        </div>\n\
                    </div>\n\
                </div>\n\
            </div></div>\n\
        </div>\n\
    </div>';

    return last_content;
}

function initViewReport(billing_id) {


    var customer_name = $("#id_customer_name").val();
    var customer_address = $("#id_customer_address").val();
    var html_receipt_date = $('#html_receipt_date').text();
    var body = '';
    var tr = '';

    $.ajax({

        url: $('#base_url').val() + "report/get_data_billing_invoices",
        data: {billing_id: billing_id},
        async: false,
        type: 'POST',
        success: function (data) {
            console.log("datax: " + data.data.length);
            var result = '';
            var count = 0;

            //sum all
            var sum = '';
            var total = 0;
            var sumTotal = 0;
            var invoice = '';
            var receipt_id = '';
            for (var i = 0; i < data.data.length; i++) {
                count++;
                //invoice += "LM-001-1" + " ,";//data.data[i].invoice_id + ",";
                invoice += data.data[i].invoice_id + ", ";
                //total =  formatCurrencyToNumber(formatCurrency(data.data[i].grand_total),2);
				total +=  parseFloatOrNumeric(data.data[i].total_amount);
                sumTotal = total;  
                receipt_id = data.data[i].receipt_id;
            }

            var common_style = 'font-family: \'Angsana New\'; color: #000;';
            var font_size_1 = 'font-size: 32px;';

            body += '<tr role="row" class="odd">\n\
                        <td style="width:35%; border-top: 0px solid #dee2e6; padding-left: 40px; padding-top: 5px; line-height: 0.7; vertical-align: top; ' + common_style + font_size_1 + '" class="text-left" colspan="3">' + 'INV NO. <br><div style="width: 784px; height:340px;">' + invoice.slice(0,-2) + ' </div></td>\n\
                        <td style="width:10%; border-top: 0px solid #dee2e6; padding-right: 50px line-height: 0.7; padding-top: 9px; vertical-align: top;' + common_style + font_size_1 + '" class=" text-right">' + formatCurrency(sumTotal, 2) + '</td>\n\
                    </tr>';

            var sum = '<tr><td style="height: 170px;"></td></tr>\n\
                      <tr>\n\
                        <td class="text-center" style="border-top: 1px solid back; ' + common_style + font_size_1 + '" colspan="2"><div style="padding-top: 12px; padding-left: 16px; text-align: left; vertical-align: top;">' + BAHTTEXT(sumTotal) + '</div></td>\n\
                        <td class="text-center" style="border-top: 1px solid back; ' + common_style + font_size_1 + '"></td>\n\
                        <td class="text-right" style="border-top: 1px solid back; ' + common_style + font_size_1 + '"><div style="padding-top: 12px; vertical-align: top; padding-top: 5px;">' + formatCurrency(sumTotal, 2) + '</div></td>\n\
                    </tr>';

            var html = setHtml(receipt_id, customer_name, customer_address, html_receipt_date);
            var last_content = lastHtml();
            result = html += body += sum += last_content;

            $("#printableAreatest").html(result);
        }
    });
}

function setContent(billing_id, data, totalpage, index, _note, span, _pageCount, sum) {

    console.log("setContent _pageCount: " + _pageCount);
    var body = '';
    var count = 0;
    var countsum = 0;
    var unit_price = 0;
    var quantity = 0;
    var total = 0;
    var br = '';
    var brlotno = '';
    for (var i = 0; i < data.length; i++) {
        count++;
        countsum++;
        var dese2 = (data[i].desc2 === null || data[i].desc2 === '') ? '' : '<br>' + data[i].desc2;

        if (data[i].desc2 === null || data[i].desc2 === '') {
            br = '<br> &nbsp;';
        }
        var carton = data[i].carton === null ? '' : '&nbsp;(' + data[i].carton + ')<br>';

        if (data[i].lot_no === null || data[i].lot_no === '') {
            brlotno = '<br> &nbsp; <br>';
        }

        var lotno = data[i].lot_no === null ? '' : encodeLineBreak(data[i].lot_no);

        //lotno = "ABDEFGHIJKLMNOPQRSTUVWXYZABDEFGHIJ";
        //style="border-top: 0px solid #dee2e6; font-size: 26px;font-family: \'Angsana New Bold\'; color: #000;"
        var div = formatCurrencyToNumber(data[i].invoice_quantity) / formatCurrencyToNumber(data[i].product_lots_size);
        var common_style = 'border-top: 0px solid #dee2e6; font-family: \'Angsana New\'; color: #000;';
        var font_size_1 = 'font-size: 28px;';
        body += '<tr role="row" class="odd">\n\
                        <td style="width:7%; line-height: 0.6; ' + common_style + font_size_1 + '" class=" text-center">' + (count + (5 * index)) + '</td>\n\
                        <td style="width:35%; padding-left: 30px; line-height: 0.6; ' + common_style + font_size_1 + '" class="text-left" colspan="2">' + data[i].product_id + '<br>' + data[i].desc1 + dese2 + carton + lotno + brlotno + ' </td>\n\
                        <td style="width:10%; padding-right: 50px line-height: 0.6; ' + common_style + font_size_1 + '" class=" text-right">' + formatCurrency((data[i].invoice_quantity), 0) + '</td>\n\
                    </tr>';

        unit_price += parseFloat(data[i].unit_price);
        quantity += parseFloat(data[i].quantity);
        total += unit_price * quantity;
    }

    var customer_name = $("#id_customer_name").val();
    var customer_address = $("#id_customer_address").val();
    var html_receipt_date = $('#html_receipt_date').text();

    var html = setHtml(billing_id, customer_name, customer_address, html_receipt_date);
    var last_content = lastHtml();



    var result = html += body += sum += last_content;

    return result;
}

function encodeLineBreak(str) {
    return str.replace(/(?:\r\n|\r|\n)/g, '<br>');
}

function getMonth(month) {
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    if (month === '01') {
        return monthNames[0];
    } else if (month === '02') {
        return monthNames[1];
    } else if (month === '03') {
        return monthNames[2];
    } else if (month === '04') {
        return monthNames[3];
    } else if (month === '05') {
        return monthNames[4];
    } else if (month === '06') {
        return monthNames[5];
    } else if (month === '07') {
        return monthNames[6];
    } else if (month === '08') {
        return monthNames[7];
    } else if (month === '09') {
        return monthNames[8];
    } else if (month === '10') {
        return monthNames[9];
    } else if (month === '11') {
        return monthNames[10];
    } else if (month === '12') {
        return monthNames[11];
    }
}

function BAHTTEXT(num, suffix) {
    'use strict';

    if (typeof suffix === 'undefined') {
        suffix = 'บาทถ้วน';
    }

    num = num || 0;
    num = num.toString().replace(/[, ]/g, ''); // remove commas, spaces

    if (isNaN(num) || (Math.round(parseFloat(num) * 100) / 100) === 0) {
        return 'ศูนย์บาทถ้วน';
    } else {

        var t = ['', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน'],
                n = ['', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า'],
                len,
                digit,
                text = '',
                parts,
                i;

        if (num.indexOf('.') > -1) { // have decimal

            /* 
             * precision-hack
             * more accurate than parseFloat the whole number 
             */

            parts = num.toString().split('.');

            num = parts[0];
            parts[1] = parseFloat('0.' + parts[1]);
            parts[1] = (Math.round(parts[1] * 100) / 100).toString(); // more accurate than toFixed(2)
            parts = parts[1].split('.');

            if (parts.length > 1 && parts[1].length === 1) {
                parts[1] = parts[1].toString() + '0';
            }

            num = parseInt(num, 10) + parseInt(parts[0], 10);


            /* 
             * end - precision-hack
             */
            text = num ? BAHTTEXT(num) : '';

            if (parseInt(parts[1], 10) > 0) {
                text = text.replace('ถ้วน', '') + BAHTTEXT(parts[1], 'สตางค์');
            }

            return text;

        } else {

            if (num.length > 7) { // more than (or equal to) 10 millions

                var overflow = num.substring(0, num.length - 6);
                var remains = num.slice(-6);
                return BAHTTEXT(overflow).replace('บาทถ้วน', 'ล้าน') + BAHTTEXT(remains).replace('ศูนย์', '');

            } else {

                len = num.length;
                for (i = 0; i < len; i = i + 1) {
                    digit = parseInt(num.charAt(i), 10);
                    if (digit > 0) {
                        if (len > 2 && i === len - 1 && digit === 1 && suffix !== 'สตางค์') {
                            text += 'เอ็ด' + t[len - 1 - i];
                        } else {
                            text += n[digit] + t[len - 1 - i];
                        }
                    }
                }

                // grammar correction
                text = text.replace('หนึ่งสิบ', 'สิบ');
                text = text.replace('สองสิบ', 'ยี่สิบ');
                text = text.replace('สิบหนึ่ง', 'สิบเอ็ด');

                return text + suffix;
            }

        }

    }
}