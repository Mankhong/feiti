$(document).ready(function () {
    //console.log("init tax report");

//    var invoice_id = 'IV003';
    var invoice_id = $("#invoice_id").val();
    getHeaderReport(invoice_id);
    loadDatatable(invoice_id);

    //nowrapColumn('col-nowrap');
});


function encodeLineBreak(str) {
    return str.replace(/(?:\r\n|\r|\n)/g, '<br>');
}

function setHtmlDetail(invoice_id, totalpage) {

    console.log("## setHtmlDetail : ");
    var html = '';
    var details = [];
    details = get_detail_arr(invoice_id);

    var date = details.data[0].invoice_date;
    var day = date.substr(8, 9);
    var month = date.substr(5, 2);
    var year = date.substr(0, 4);

    var enterHeader = '';
    var enterHeader2 = '';
    var _pageCount = '';
    var begin_page_height = 0;
    //var _span = '';

    if (totalpage > 1) {

        //_span = span;

        //400 px; to 287px;
//        enterHeader = '<table border="1">\n\
//                                            <tr>\n\
//                                                <td height="287"></td>\n\
//                                            </tr>\n\
//                                </table>';
//        enterHeader2 = '<table border="1">\n\
//                                            <tr>\n\
//                                                <td height="25"></td>\n\
//                                            </tr>\n\
//                                </table>';
        //***
        // for (var i = 0; i < 40; i++) {
        //     enterHeader += '<table border="0" class="multipage-table">\n\
        //                                     <tr>\n\
        //                                         <td height="10"></td>\n\
        //                                     </tr>\n\
        //                         </table>';
        // }
    } else {
        //_span = '';
        enterHeader = '';
        enterHeader2 = '';
    }

    enterHeader = '<div class="begin-page" style="height: ' + begin_page_height + 'px; border: 0px solid #333;"></div>';

    //102
    var cus_address = details.data[0].cus_address === null ? '' : encodeLineBreak(details.data[0].cus_address);
    var detail_data = enterHeader + enterHeader2 + '<div class="row pt-2">\n\
                            <div class="col-12">\n\
                                <div class="row" >\n\
                                    <table>\n\
                                            <tr>\n\
                                                <td height="50"></td>\n\
                                            </tr>\n\
                                    </table>\n\
                                <div class="col-2" style="display: none;">\n\
                                        <image src="assets/images/default_image.jpg">\n\
                                </div>\n\
                                <div class="col-10" style="padding-left: 3.0rem !important;">\n\
                                    <div class="invoice-title">\n\
                                        <div style="display: none;">\n\
                                                <h2>บริษัท เฟยตี้ พรีซิชั่น (ไทยแลนด์) จำกัด<br>FEITI PRESISION (THAILAND)CO.,LTD</h2>\n\
                                                <hr style="margin-top: 0px;margin-bottom: 15px;">\n\
                                                <small>\n\
                                                    1/92 หมู่ 5 สวนอุตสาหกรรมโรจนะ ต.คานหาม อ.อุทัย จ.พระนครศรีอยุธยา โทร (035) 227916-9 โทรสาร (035)226148\n\
                                                    <br>\n\
                                                    1/92 MOO 5 ROJANA TUMBOL KANHAM AMPHUR U-THAI AYUTTHAYA TEL:(035)227916-9 FAX:(035)226148\n\
                                                </small>\n\
                                            </div>\n\
                                        </div>\n\
                                    </div>\n\
                                </div>\n\
                                <div class="row pt-4">\n\
                                    <div class="col-6" style="display: none;">\n\
                                        <address>\n\
                                            <strong>เลขประจำตัวผู้เสียภาษี: 0145547001421</strong><br>\n\
                                            <span>สำนักงานใหญ่</span><br>\n\
                                        </address>\n\
                                    </div>\n\
                                    <div class="col-6 text-right"  style="display: none;">\n\
                                        <address>\n\
                                            <strong>Tax Payer ID No: 0145547001421</strong><br>\n\
                                            <span>RoHS</span><br>\n\
                                        </address>\n\
                                    </div>\n\
                                    <table>\n\
                                            <tr>\n\
                                                <td height="95"></td>\n\
                                            </tr>\n\
                                    </table>\n\
                                </div>\n\
                                <div class="row">\n\
                                    <div class="col-2" >\n\
                                        <div align="center" class="panel-body" style="border: 0px solid #d2c6c6;display: none;">\n\
                                            <address>\n\
                                                <strong>ต้นฉบับ</strong><br>\n\
                                                <span>ORIGINAL</span><br>\n\
                                            </address>\n\
                                        </div>\n\
                                    </div>\n\
                                    <div class="col-3" ></div>\n\
                                    <div class="col-7 text-left" style="display: none;">\n\
                                        <address>\n\
                                            <strong>ใบกำกับภาษี/ใบแจ้งหนี้</strong><br>\n\
                                            <strong>Tax Invoice/Invoice</strong><br>\n\
                                        </address>\n\
                                    </div>\n\
                                </div>\n\
                                <div class="row" style="height:140px; padding: 0px; border: 0px solid #000;">\n\
                                    <div class="col-1 pt-3" style="width: 98px;" style="display: none;">\n\
                                        <strong style="display: none;">SOLD TO:</strong>\n\
                                    </div>\n\
                                    <div class="col-10">\n\
                                        <div class="row">\n\
                                            <div class="col-8">\n\
                                                <div style="padding-left: 10px;">\n\
                                                    <address>\n\
                                                        <span id="v_customer_name2" style="border-top: 0px solid #dee2e6; font-size: 26px; font-weight: bold; font-family: \'Cordia New\'; color: #000; white-space: nowrap; overflow: hidden;" >' + details.data[0].cus_name + '</span><br>\n\
                                                        <span id="v_cusmer_address2" style="border-top: 0px solid #dee2e6; font-size: 30px;font-family: \'Cordia New\'; color: #000; line-height: 0.8;">' + cus_address + '</span><br>\n\
                                                    </address>\n\
                                                </div>\n\
                                            </div>\n\
                                            <div class="col-1 text-right">\n\
                                                <div>\n\
                                                    <strong style="display: none;">Date: </strong><br>\n\
                                                    <strong style="display: none;">InvoiceNo: </strong>\n\
                                                </div>\n\
                                            </div>\n\
                                            <div class="col-3 text-left" style="padding-top: 32px;">\n\
                                                <div>\n\
                                                    <strong><span style="border-top: 0px solid #dee2e6; font-size: 21px;font-family: \'Calibri\'; font-weight: bold; color: #000;">' + day + '-' + getMonth(month) + '-' + year + ' </span></strong><br>\n\
                                                    <strong><span style="border-top: 0px solid #dee2e6; font-size: 26px;font-family: \'Calibri\'; font-weight: bold; color: #000; padding-left: 50px; line-height: 30px;">' + details.data[0].invoice_id + '</span></strong>&nbsp;&nbsp;';


    html += detail_data;
    return html;
}

function subSetHtmlDetail(length_date) {
    var html = '';

    var sub_header = '</div>\n\
                                            </div>\n\
                                        </div>\n\
                                    </div>\n\
                                </div>\n\
                                <div style="height:75px;">\n\
                                </div>';

    var header = '<div class="row pt-1">\n\
                                    <div class="col-md-12">\n\
                                      <div class="panel panel-default" style"height:814px;">\n\
                                          <div>\n\
                                              <table class="table table-condensed tablecustom " border="0" style="height:617px; table-layout: fixed; width:100%;" border="0"; >\n\
                                                  <tbody id="body_content">';

    html += sub_header += header;

    return html;
}
function setLastBody(totalpage, index) {
    var lastbody = '';
    var lastEnterDiv = '</div>\n\
                       </div>';
    if (totalpage > 1) {
        lastbody = '              </tbody>\n\
                                </table>\n\
                              </div>\n\
                          </div>\n\
                      </div>\n\
                  </div>' + lastEnterDiv;
    } else {
        lastbody = '              </tbody>\n\
                                </table>\n\
                              </div>\n\
                          </div>\n\
                      </div>\n\
                  </div>' + lastEnterDiv;
        ;
    }

    var end_page_height = ((index + 1) == totalpage) ? 30: 345; //370
    lastbody += '<div class="end-page" style="height: ' + end_page_height + 'px; border: 0px solid #333;"></div>';

    return lastbody;
}

$("#btn_print").click(function () {



    var invoice_id = $("#invoice_id").val();
    initViewReport(invoice_id);

    var mode = 'popup'; //popup 
    var close = mode == "popup";
    var options = {

        mode: "iframe",
        popClose: close,
//        header: false,
//        standard: "html5",
//        retainAttr: ["id", "class"],
//        printDelay: 500,
//        printAlert: false
    };
    $("div.printableArea").printArea(options);
});

function initViewReport(invoice_id) {

    //console.log("initViewReport: " + invoice_id);

    $.ajax({
        url: $('#base_url').val() + "report/get_invoice_list",
        data: {invoice_id: invoice_id},
        async: false,
        type: 'POST',
        success: function (data) {

            console.log("data length: " + data.data.length);
            var totalpage = data.data.length / 8;
            console.log("totalpage: " + totalpage);
            console.log("Math.ceil(totalpage): " + Math.ceil(totalpage));

            //var div = '<div class="printableArea" id="printableAreatest"></div>';
            //$("#contentdiv").html(div);

            var pagArr = [];
            var fiveArray = [];
            var index = 0;
            var indexFilve = 0;
            var result = '';
            var count = 0;
            for (var i = 0; i < data.data.length; i++) {
                fiveArray[indexFilve++] = data.data[i];

                if ((i + 1) % 8 === 0 || i === data.data.length - 1) {
                    pagArr[index++] = fiveArray;
                    count++;
                    fiveArray = [];
                    indexFilve = 0;
                }
            }

            var totaldata = data.data.length;
            var result = totaldata / 8;
            console.log("totaldata: " + totaldata);
            //console.log("count: " + count);
            //totaldata = data.data.length;
            //console.log("page: " + count + "result: " + Math.ceil(result));


            //sum all
            var sum = '';
            var total = 0;
            var amount = 0;
            var unit_price = 0;
            var quantity = 0;

            for (var i = 0; i < data.data.length; i++) {


                unit_price = formatCurrencyToNumber(formatCurrency(data.data[i].unit_price, 4));
                quantity = formatCurrencyToNumber(data.data[i].invoice_quantity);

                ////console.log("unit_price x: " + unit_price);
                ////console.log("quantity x: " + quantity);
                amount += formatCurrencyToNumber(formatCurrency((unit_price * quantity), 2));

                total += (unit_price * quantity);
                internal_note = data.data[i].internal_note;
            }

            //console.log("amount y: " + amount);
            //console.log("internal_note: " + internal_note);
            //console.log("total: " + total);

            var _total = formatCurrency(amount, 2);
            //console.log("_total: " + _total);

            var vat = (amount * 7) / 100;
            //console.log("vat: " + vat);

            var _vat = formatCurrency(vat, 2);
            //console.log("_vat: " + _vat);

            var sumall = formatCurrencyToNumber(_total) + formatCurrencyToNumber(_vat);
            //console.log("sumall: " + sumall);

            var sum_font_size = 28;
            var sum_font_size_2 = 21;

            var flag = '';
            if (Math.ceil(totalpage) <= 1) {
                console.log("if totalpage <= 1");
                flag = 1;

                for (var i = 0; i < pagArr.length; i++) {

                    sum = '<tr height="10">\n\
                        <td style="border-top: 0px solid #dee2e6; height: 70px;" class="thick-line" colspan="3" ></td>\n\
                        <td style="border-top: 0px solid #dee2e6;" class="thick-line text-center" colspan="2" ></td>\n\
                        <td style="border-top: 0px solid #dee2e6; font-weight: bold; font-size: ' + sum_font_size_2 + 'px;font-family: \'Calibri\'; color: #000; padding-right: 35px;" class="thick-line text-right" ><div>' + formatCurrency(_total, 2) + '</div></td>\n\
                    </tr>\n\
                    <tr height="60">\n\
                        <td style="border-top: 0px solid #dee2e6; height: 70px; font-size: 24px;font-family: \'Angsana New Bold\'; color: #000;" align="center" class="no-line" colspan="4" ></td>\n\
                        <td style="border-top: 0px solid #dee2e6;" class="no-line text-center"  ></td>\n\
                        <td style="border-top: 0px solid #dee2e6; font-weight: bold; font-size: ' + sum_font_size_2 + 'px;font-family: \'Calibri\'; color: #000; padding-right: 35px;" class="no-line text-right">' + formatCurrency(_vat, 2) + '</td>\n\
                    </tr>\n\
                    <tr>\n\
                        <td style="border-top: 0px solid #dee2e6; height: 70px; border-top: none; text-align: center;" class="no-line" colspan="3">' + BAHTTEXT(sumall) + '</td>\n\
                        <td style="border-top: 0px solid #dee2e6;" class="no-line text-center" colspan="2" ></td>\n\
                        <td style="border-top: 0px solid #dee2e6; font-weight: bold; font-size: ' + sum_font_size_2 + 'px;font-family: \'Calibri\'; color: #000; padding-right: 35px;" class="no-line text-right" ><div>' + formatCurrency(sumall, 2) + '</div></td>\n\
                    </tr>';

                    var internal_note = data.data[0].internal_note === null || data.data[0].internal_note === '' ? '&nbsp;<br>&nbsp;<br>&nbsp;<br>' : data.data[0].internal_note;
                    var _note = '<tr>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0; padding: 0px 0px 0px 20px;"><div style="height: 70px;">' + internal_note + '</div></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    </tr>';

                    console.log("result if < 1 : " + result);
                    result = '<br>';
                    result += setContent(invoice_id, pagArr[i], Math.ceil(totalpage), i, _note, sum, 1, '');

                }

                if (data.data.length === 0) {
                    //$("#printableAreatest").html('');
                } else {
                    //console.log("#####result : " + result);
                    $("#printableAreatestContent").html(result);
                }


            } else {

                result = formatCurrency(result, 1);
                console.log("###################### else ########################: " + result);
                //console.log("else: result: "+ Math.ceil(result) + " count: " +count); 
                var pageCount = 0;
                var _pageCount = 0;
                var modd = 2;
                var span = '';

                for (var i = 0; i < pagArr.length; i++) {
                    pageCount++;
                    _pageCount += 1;
                    //console.log("###################### else ########################: " + pagArr.length);
                    //console.log("###################### pageCount ########################: " + pageCount);

                    if (_pageCount == count) {
                        console.log("###################### !== count ########################: ");
                        var internal_note = data.data[0].internal_note;
                        flag = 2;

                        span = '<span style=" font-size: 28px;font-family: \'Angsana New Bold\'; color: #000;">'+_pageCount+'/' + Math.ceil(totalpage)+ '</span>';
                        sum = '<tr height="43">\n\
                                    <td style="border-top: 0px solid #dee2e6; height: 70px;" class="thick-line" colspan="3" ></td>\n\
                                    <td style="border-top: 0px solid #dee2e6;" class="thick-line text-center" colspan="2" ></td>\n\
                                    <td style="border-top: 0px solid #dee2e6; font-weight: bold; font-size: ' + sum_font_size_2 + 'px;font-family: \'Calibri\'; color: #000; padding-right: 35px;" class="thick-line text-right" ><div>' + formatCurrency(_total, 2) + '</div></td>\n\
                                </tr>\n\
                                <tr height="70">\n\
                                    <td style="border-top: 0px solid #dee2e6; height: 70px; font-size: 26px;font-family: \'Angsana New\'; color: #000;" align="center" class="no-line" colspan="3" ></td>\n\
                                    <td style="border-top: 0px solid #dee2e6;" class="no-line text-center" colspan="2" ></td>\n\
                                    <td style="border-top: 0px solid #dee2e6; font-weight: bold; font-size: ' + sum_font_size_2 + 'px;font-family: \'Calibri\'; color: #000; padding-right: 35px;" class="no-line text-right">' + formatCurrency(_vat, 2) + '</td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <td style="border-top: 0px solid #dee2e6; height: 70px; border-top: none; text-align: center;" class="no-line" colspan="3">' + BAHTTEXT(sumall) + '</td>\n\
                                    <td style="border-top: 0px solid #dee2e6;" class="no-line text-center" colspan="2" ></td>\n\
                                    <td style="border-top: 0px solid #dee2e6; font-weight: bold; font-size: ' + sum_font_size_2 + 'px;font-family: \'Calibri\'; color: #000; padding-right: 35px;" class="no-line text-right" ><div>' + formatCurrency(sumall, 2) + '</div></td>\n\
                                </tr>';

                    } else {
                        console.log("###################### == count ########################: ");
                        var internal_note = '&nbsp;';
                        flag = 1;
                        span = '<span style=" font-size: ' + sum_font_size + 'px;font-family: \'Angsana New Bold\'; color: #000;">'+_pageCount+'/' + Math.ceil(totalpage) + '</span>';
                        sum = '<tr height="43">\n\
                                    <td style="border-top: 0px solid #dee2e6;" class="thick-line" colspan="3" ></td>\n\
                                    <td style="border-top: 0px solid #dee2e6;" class="thick-line text-center" colspan="2" ></td>\n\
                                    <td style="border-top: 0px solid #dee2e6; font-weight: bold; font-size: ' + sum_font_size_2 + 'px;font-family: \'Calibri\'; color: #000; padding-right: 35px;" class="thick-line text-right" ><div></div></td>\n\
                                </tr>\n\
                                <tr height="70">\n\
                                    <td style="border-top: 0px solid #dee2e6; font-size: ' + sum_font_size + 'px;font-family: \'Angsana New\'; color: #000;" align="center" class="no-line" colspan="3" ></td>\n\
                                    <td style="border-top: 0px solid #dee2e6;" class="no-line text-center" colspan="2" ></td>\n\
                                    <td style="border-top: 0px solid #dee2e6; font-weight: bold; font-size: ' + sum_font_size_2 + 'px;font-family: \'Calibri\'; color: #000; padding-right: 35px;" class="no-line text-right"></td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <td style="border-top: 0px solid #dee2e6;" class="no-line" style="border-top: none;" colspan="3"></td>\n\
                                    <td style="border-top: 0px solid #dee2e6;" class="no-line text-center" colspan="2" ></td>\n\
                                    <td style="border-top: 0px solid #dee2e6; font-weight: bold; font-size: ' + sum_font_size_2 + 'px;font-family: \'Calibri\'; color: #000; padding-right: 35px;" class="no-line text-right" ><div></div></td>\n\
                                </tr>';
                    }


                    var _note = '<tr>\n\
                    <td style="border-top: 0;"></td>\n\<td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0; padding: 0px 0px 0px 20px;" ><div style="height: 70px;"> ' + internal_note + '</div></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    </tr>';


                    result += setContent(invoice_id, pagArr[i], Math.ceil(totalpage), i, _note, sum, flag, span);

                }

                var z = totaldata % modd;
                console.log("mod z: " + z);
                if (z === 0) {
                    console.log("result substring === 0");
                    result = '<br>' + result.substring(3, result.length);
                } else {
                    console.log("result substring !== 0");
                    result = '<br>' + result.substring(3, result.length);
                }
                //console.log("###############else result: " + result);
                $("#printableAreatestContent").html(result);
            }


            // end conten

        }
    });
}

function setContent(invoice_id, data, totalpage, index, _note, _sum, flag, span) {
    //content
    console.log("## setContent data lenght: " + data.length + " pageCount: " + span);
    var body = '';
    var sum = _sum;
    var count = 0;
    var countsum = 0;
    var total = 0;
    var vat = 0;
    var unit_price = 0;
    var quantity = 0;
    var amount = 0;
    var page_c = 0;
    var _span = '';

    for (var i = 0; i < data.length; i++) {
        count++;
        countsum++;
        page_c = count;
        var dese2;
        if (data[i].desc2 === null || data[i].desc2 === '') {
            dese2 = '&nbsp;&nbsp;';
        } else {
            dese2 = data[i].desc2;
        }
        var carton = data[i].carton === null ? '' : '&nbsp;' + data[i].carton;
        var common_style = 'border-top: 0px solid #dee2e6; color: #000;';
        var font_size_1 = 'font-size: 24px;';
        var font_size_2 = 'font-size: 28px;';
        var font_size_3 = 'font-size: 26px;';
        var font_size_22 = 'font-size: 21px;';
        
        var font_family = 'font-family: \'Angsana New Bold\';';
        var font_family_2 = 'font-family: \'Cordia New\';';
        var font_family_3 = 'font-family: \'Calibri\';font-weight: bold;';
        var line_height = 'line-height: 0.8;';
        var padding_1 = 'padding: 0px 10px 0px 10px;';
        var padding_2 = 'padding: 0px 35px 0px 10px;';
        var padding_3 = 'padding: 0px 10px 0px 0px;';

        body += '<tr role="row" class="odd">\n\
                        <td class="text-left" style="width:68px; ' + padding_1 + common_style + font_size_1 + font_family + line_height + '" >' + (count + (8 * index)) + '</td>\n\
                        <td class="text-left" style="width:125px; font-weight: bold; ' + padding_3 + common_style + font_size_1 + font_family_2 + line_height + '">' + data[i].order_id + '</td>\n\
                        <td class="text-left col-nowrap" style="width:380px; font-weight: bold; white-space: nowrap; ' + padding_1 + common_style + font_size_3 + font_family_2 + line_height + '" ><span style="width:250px; text-overflow: ellipsis;">' + data[i].product_id + '<br>' + data[i].desc1 + '<br>' + dese2 + '</span></td>\n\
                        <td class="text-right" style="width:135px;' + padding_1 + common_style + font_size_22 + font_family_3 + line_height + '">' + formatCurrency(data[i].invoice_quantity, 0) + '</td>\n\
                        <td class="text-right" style="width:115px;' + padding_1 + common_style + font_size_22 + font_family_3 + line_height + '">' + formatCurrency(data[i].unit_price, 4) + '</td>\n\
                        <td class="text-right" style="' + padding_2 + common_style + font_size_22 + font_family_3 + line_height + '">' + formatCurrency((data[i].invoice_quantity * data[i].unit_price), 2) + '</td>\n\
                    </tr>';

        unit_price = parseFloat(data[i].unit_price);
        quantity = parseFloat(data[i].invoice_quantity);
        total += unit_price * quantity;
        amount += formatCurrencyToNumber(formatCurrency((unit_price * quantity), 2));
    }

    var enter = '';
    if (data.length === 1) {
        enter += '<tr><td style="border-top: 0px solid #dee2e6;" height="400" colspan="5" align="center" ></td></tr>';
    } else if (data.length === 2) {
        enter += '<tr><td style="border-top: 0px solid #dee2e6;" height="295" colspan="5" align="center" ></td></tr>';
    } else if (data.length === 3) {
        enter += '<tr><td style="border-top: 0px solid #dee2e6;" height="235" colspan="5" align="center" ></td></tr>';
    } else if (data.length === 4) {
        enter += '<tr><td style="border-top: 0px solid #dee2e6;" height="175" colspan="5" align="center" ></td></tr>';
    } else if (data.length === 5) {
        enter += '<tr><td style="border-top: 0px solid #dee2e6;" height="120" colspan="5" align="center" ></td></tr>';
    } else if (data.length === 6) {
        enter += '<tr><td style="border-top: 0px solid #dee2e6;" height="60" colspan="5" align="center" ></td></tr>';
    } else if (data.length === 7) {
        enter += '<tr><td style="border-top: 0px solid #dee2e6;" height="5" colspan="5" align="center" ></td></tr>';
    } else if (data.length === 8) {
        //enter += '<tr><td style="border-top: 0px solid #dee2e6;" height="5" colspan="5" align="center" ></td></tr>';
    }

    var _total = formatCurrency(amount, 2);
    //console.log("_total: " + _total);

    var vat = (total * 7) / 100;
    //console.log("vat: " + vat);

    var _vat = formatCurrency(vat, 2);
    //console.log("_vat: " + _vat);

    var sumall = formatCurrencyToNumber(_total) + formatCurrencyToNumber(_vat);
    //console.log("sumall: " + sumall);

    _span += span;
    var d = setHtmlDetail(invoice_id, flag);
    var subd = subSetHtmlDetail(data.length);
    var las = setLastBody(totalpage, index);
    var items_area_begin = '<tr><td class="items-area-td" colspan="6" style="border: none; padding: 0px;"><div class="items-area" style="width: 100%; height: 560px; border: 0px solid #333; padding: 0px;">';
    var items_area_end = '</div></td></tr>';
    body = items_area_begin + '<table class="body-table" style="width: 100%;">' + body + _note + '</table>' + items_area_end;

    var resulthtml = d += span += subd += body += sum += las;
    //var resulthtml = d += span += subd += body += _note += enter += sum += las;
    return resulthtml;
}



function get_detail_arr(invoice_id) {

    //console.log("get_detail_arr: ");
    var arr = [];
    $.ajax({
        url: $('#base_url').val() + 'report/get_detail_report',
        type: 'POST',
        data: {invoice_id: invoice_id,
            customer_id: ''
        },
        async: false,
        dataType: 'json',
    }).done(function (data) {
        //console.log("report data: " + data);
        if (data === null) {
            arr = [];
        } else {
            arr = data;
        }
    });

    return arr;
}



function getHeaderReport(invoice_id) {
    //console.log("getHeaderReport : invoice_id : " + invoice_id);
    $.ajax({
        type: 'POST',
        data: {invoice_id: invoice_id,
            customer_id: ''
        },
        url: $('#base_url').val() + 'report/get_detail_report',
        async: true,
        success: function (data) {
            //console.log("data " + data.data[0].invoice_id);

            //set to header
            $("#v_customer_name").html(data.data[0].cus_name);
            $("#v_cusmer_address").html(data.data[0].cus_address);
            var date = data.data[0].invoice_date;
            //console.log("date: " + date);

            var day = date.substr(8, 9);
            var month = date.substr(5, 2);
            var year = date.substr(0, 4);

            //console.log("day: " + day);
            //console.log("month: " + month);
            //console.log("year: " + year);

            $("#v_invoice_date").html(day + '-' + getMonth(month) + '-' + year);
            $("#v_invoice_id").html(data.data[0].invoice_id);
        }
    });
}

function loadDatatable(invoice_id) {
    //console.log("loadDatatable: " + invoice_id);
    var count = 0;
    $("#rstable").dataTable().fnDestroy();
    var t = $('#rstable').DataTable({

        "ajax": {
            "url": $('#base_url').val() + "report/get_invoice_list",
            "type": "post",
            "data": {invoice_id: invoice_id}
        },

        "type": "POST",
        "async": true,
        searching: false,
        ordering: true,
//        order: [
//            [1, 'asc']
//        ],
        lengthChange: false,
        pageLength: 8,
        columns: [{
                'data': 'id',
                'render': function (data, type, row, meta) {
                    return meta.row + 1;
                }
            },
            {
                'data': 'order_id'
            },
            {targets: 0,
                data: 'product_id',
                "className": "text-left",
                render: function (data, type, row, meta) {

                    var desc2 = row.desc2 === null ? '' : row.desc2;
                    //var carton = row.carton === null ? '' : row.carton;
                    var result = row.product_id + "<br>" + row.desc1 + "  " + "<br>" + desc2;
                    return result;

                }
            },
            {targets: 0,
                data: 'invoice_quantity',
                "className": "text-right",
                render: function (data, type, row, meta) {
                    return formatCurrency(row.invoice_quantity, 0);

                }
            },

            {targets: 0,
                data: 'unit_price',
                "className": "text-right",
                render: function (data, type, row, meta) {
                    return formatCurrency(row.unit_price, 4);

                }
            },
            {targets: 0,
                data: 'unit_price',
                "className": "text-right",
                render: function (data, type, row, meta) {
                    var total = (parseFloat(row.unit_price) * parseFloat(row.invoice_quantity));
                    return formatCurrency(total, 2);

                }
            },
        ],

        columnDefs: [{
                "targets": 0,
                "className": "text-center"
            },
            {
                "targets": 1,
                "className": "text-center"
            },
            {
                "targets": 5,
                "className": "text-center"
            },
        ],
        fnDrawCallback: function (settings) {
            calculateSummary();
            //rollback function

            /*
            count++;
            if (count > 1) {
                //console.log("count: " + count);
                var info = $('#rstable').DataTable().page.info();
                var page = parseInt(info.page) + parseInt(1);
                var totaldata = 0;
                var pagele = 8;
                var result = 0;

                var unit_price = 0;
                var quantity = 0;
                var internal_note = '';

                console.log("<<<");
                console.log($('#rstable').DataTable().rows().data());

                $.ajax({
                    url: $('#base_url').val() + "report/get_invoice_list",
                    type: 'POST',
                    data: {invoice_id: invoice_id,
                        customer_id: ''
                    },
                    async: false,
                    dataType: 'json',
                    success: function (data) {
                        console.log(">>>");
                        console.log(data.data);
                        //console.log("datax: " + data.data);
                        var total = 0;
                        var amount = 0;
                        for (var i = 0; i < data.data.length; i++) {


                            unit_price = formatCurrencyToNumber(formatCurrency(data.data[i].unit_price, 4));
                            quantity = formatCurrencyToNumber(data.data[i].invoice_quantity);

                            ////console.log("unit_price x: " + unit_price);
                            ////console.log("quantity x: " + quantity);
                            amount += formatCurrencyToNumber(formatCurrency((unit_price * quantity), 2));

                            total += (unit_price * quantity);
                            internal_note = data.data[i].internal_note;
                        }

                        //console.log("amount y: " + amount);
                        //console.log("internal_note: " + internal_note);
                        //console.log("total: " + total);

                        var _total = formatCurrency(amount, 2);
                        //console.log("_total: " + _total);

                        var vat = (total * 7) / 100;
                        //console.log("vat: " + vat);

                        var _vat = formatCurrency(vat, 2);
                        //console.log("_vat: " + _vat);

                        var sumall = formatCurrencyToNumber(_total) + formatCurrencyToNumber(_vat);
                        //console.log("sumall: " + sumall);

                        totaldata = data.data.length;
                        result = totaldata / pagele;
                        //console.log("page: " + page + "result: " + Math.ceil(result));
                        if (Math.ceil(result) === page) {
                            appendSummary(_total, _vat, formatCurrency(sumall, 2), internal_note);
                        }
                    }
                });
            }
            */

        },

    });


}

function calculateSummary() {
    var info = $('#rstable').DataTable().page.info();
    var page = parseInt(info.page) + parseInt(1);
    var totaldata = 0;
    var pagele = 8;
    var result = 0;

    var unit_price = 0;
    var quantity = 0;
    var internal_note = '';

    var data = $('#rstable').DataTable().rows().data();
    var total = 0;
    var amount = 0;
    for (var i = 0; i < data.length; i++) {
        unit_price = formatCurrencyToNumber(formatCurrency(data[i].unit_price, 4));
        quantity = formatCurrencyToNumber(data[i].invoice_quantity);
        amount += formatCurrencyToNumber(formatCurrency((unit_price * quantity), 2));

        total += (unit_price * quantity);
        internal_note = data[i].internal_note;
    }

    var _total = formatCurrency(amount, 2);
    var vat = (amount * 7) / 100;
    var _vat = formatCurrency(vat, 2);
    var sumall = formatCurrencyToNumber(_total) + formatCurrencyToNumber(_vat);

    totaldata = data.length;
    result = totaldata / pagele;
    if (Math.ceil(result) === page) {
        appendSummary(_total, _vat, formatCurrency(sumall, 2), internal_note);
    }
}

function appendSummary(tatal, vat, sumall, internal_note) {
    //console.log("start appendSummary");

    var html = '<tr>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                </tr>\n\
               <tr>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0; ">' + internal_note + '</td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                </tr>\n\
                <tr>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                    <td style="border-top: 0;"></td>\n\
                </tr>\n\
                <tr>\n\
                   <td class="thick-line" colspan="3">\n\
                    Please issue crossed cheque to FEITI PRECITION (THAILAND)CO.,LTD<br>\n\
                    กรุณาสั่งจ่ายเช็คขีดคร่อมในนามบริษัท เฟยตี้ พรีซิซัน(ไทยแลนด์)จำกัด \n\
                    \n\
                    </td>\n\
                    <td class="thick-line text-center" colspan="2"><span>Total<br>มูลค่าสินค้า</span></td>\n\
                    <td class="thick-line text-right" >' + tatal + '</td>\n\
                </tr>\n\
                <tr>\n\
                    <td align="center" class="no-line" colspan="3" style="border-bottom: none;"></td>\n\
                    <td class="no-line text-center" colspan="2"><span>VAT 7%<br>ภาษีมูลค่าเพิ่ม</span></td>\n\
                    <td class="no-line text-right">' + vat + '</td>\n\
                </tr>\n\
                <tr>\n\
                    <td class="no-line" style="border-top: none; text-align: center;" colspan="3">' + BAHTTEXT(sumall) + '</td>\n\
                    <td class="no-line text-center" colspan="2"><span>GRAND TOTAL<br>ยอดรวม</span></td>\n\
                    <td class="no-line text-right">' + sumall + '</td>\n\
                </tr>';
    $('tbody').append(html);
}

function formatMoney(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

function formatMoneyFlow(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(4).slice(2) : "");
}


function getMonth(month) {
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    if (month === '01') {
        return monthNames[0];
    } else if (month === '02') {
        return monthNames[1];
    } else if (month === '03') {
        return monthNames[2];
    } else if (month === '04') {
        return monthNames[3];
    } else if (month === '05') {
        return monthNames[4];
    } else if (month === '06') {
        return monthNames[5];
    } else if (month === '07') {
        return monthNames[6];
    } else if (month === '08') {
        return monthNames[7];
    } else if (month === '09') {
        return monthNames[8];
    } else if (month === '10') {
        return monthNames[9];
    } else if (month === '11') {
        return monthNames[10];
    } else if (month === '12') {
        return monthNames[11];
    }
}

function BAHTTEXT(num, suffix) {
    'use strict';

    if (typeof suffix === 'undefined') {
        suffix = 'บาทถ้วน';
    }

    num = num || 0;
    num = num.toString().replace(/[, ]/g, ''); // remove commas, spaces

    if (isNaN(num) || (Math.round(parseFloat(num) * 100) / 100) === 0) {
        return 'ศูนย์บาทถ้วน';
    } else {

        var t = ['', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน'],
                n = ['', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า'],
                len,
                digit,
                text = '',
                parts,
                i;

        if (num.indexOf('.') > -1) { // have decimal

            /* 
             * precision-hack
             * more accurate than parseFloat the whole number 
             */

            parts = num.toString().split('.');

            num = parts[0];
            parts[1] = parseFloat('0.' + parts[1]);
            parts[1] = (Math.round(parts[1] * 100) / 100).toString(); // more accurate than toFixed(2)
            parts = parts[1].split('.');

            if (parts.length > 1 && parts[1].length === 1) {
                parts[1] = parts[1].toString() + '0';
            }

            num = parseInt(num, 10) + parseInt(parts[0], 10);


            /* 
             * end - precision-hack
             */
            text = num ? BAHTTEXT(num) : '';

            if (parseInt(parts[1], 10) > 0) {
                text = text.replace('ถ้วน', '') + BAHTTEXT(parts[1], 'สตางค์');
            }

            return text;

        } else {

            if (num.length > 7) { // more than (or equal to) 10 millions

                var overflow = num.substring(0, num.length - 6);
                var remains = num.slice(-6);
                return BAHTTEXT(overflow).replace('บาทถ้วน', 'ล้าน') + BAHTTEXT(remains).replace('ศูนย์', '');

            } else {

                len = num.length;
                for (i = 0; i < len; i = i + 1) {
                    digit = parseInt(num.charAt(i), 10);
                    if (digit > 0) {
                        if (len > 2 && i === len - 1 && digit === 1 && suffix !== 'สตางค์') {
                            text += 'เอ็ด' + t[len - 1 - i];
                        } else {
                            text += n[digit] + t[len - 1 - i];
                        }
                    }
                }

                // grammar correction
                text = text.replace('หนึ่งสิบ', 'สิบ');
                text = text.replace('สองสิบ', 'ยี่สิบ');
                text = text.replace('สิบหนึ่ง', 'สิบเอ็ด');

                return text + suffix;
            }

        }

    }
}
function nowrapColumn(tdClass) {
    var padding = 5; //px
    var cols = $('.' + tdClass);
    for (var i = 0; i < cols.length; i++) {
        var td = cols[i];
        var div = $(cols[i]).find('div');
        $(div).width($(td).width() - padding);
    }
}
