function showMessageModal(message) {
    if (message) {
        $('#msgModalMessage').html(message);
    }
    $('#msgModal').modal('show');
}

//yyyy-mm-dd
function getCurrentDateString() {
    var spl = '-';
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    return yyyy + spl + (mm < 10 ? '0' + mm : mm) + spl + (dd < 10 ? '0' + dd : dd);
}

function getCurrentDateDDMMYYYY() {
    var spl = '/';
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    return (dd < 10 ? '0' + dd : dd) + spl + (mm < 10 ? '0' + mm : mm) + spl + yyyy;
}

function strNotNull(str) {
    return (str == null || str == undefined) ? '' : str;
}

function convertCurrencyInputByClass() {
	var currencyInputs = $("input.pz-currency-input");
	for (var i = 0; i < currencyInputs.length; i++) {
		var input = currencyInputs[i];
		input.value = formatCurrency(input.value);
	}
}

function convertCurrencyInput(id) {
	var input = $("#" + id);
	input.value = formatCurrency(input.value);
}

function formatCurrency(num, decimal) {
    decimal = (decimal == undefined) ? 2 : decimal;
    var number = roundup(formatCurrencyToNumber(num), decimal);
    var dc = String(number).split(".");

    var n = dc[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    var d = strNotNull(dc[1]);
    return d == '' ? n : (n + '.' + d);
}

function formatCurrency2(num, decimal) {
    var dc = String(formatCurrencyToNumber(num)).split(".");
    var n = dc[0];
    var d = parseFloatOrNumeric('0.' + dc[1]);

    n = n.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
	
    decimal = (decimal == undefined) ? 2 : decimal;
    
    d = roundup(d, decimal);
    var dd = String(d).split('.');
    d = strNotNull(dd[1]);
    
    return d == '' ? n : (n + '.' + d);
}

//Example decimal: 0.001
function roundup2(decimal, precision) {
    var xr = '1';
    for (var i = 0; i < precision; i++) {
        xr += '0';
    }

    xr = parseFloat(xr);
    var r = xr * decimal;
    r = Math.ceil(r.toFixed(2));
    
    return (r / xr).toFixed(precision);
}

function roundup(decimal, precision) {
    var xr = '1';
    for (var i = 0; i < precision; i++) {
        xr += '0';
    }

    xr = parseFloat(xr);
    decimal = Math.round((decimal * xr).toFixed(2)) / xr;

    return decimal.toFixed(precision);
}

function formatCurrencyToNumber(currency) {
    return parseFloatOrNumeric(String(currency).replace(/,/g,''));
}

function parseFloatOrEmpty(number) {
    number = parseFloat(number);
    return isNaN(number) ? '' : number;
}

function parseFloatOrNumeric(number) {
    number = parseFloat(number);
    return isNaN(number) ? 0 : number;
}

//encodeURIComponent and base64
function encodeString(str) {
    return encodeURIComponent(btoa(str));
}

function decodeString(str) {
    return atob(decodeURIComponent(str));
}

function showLoading() {
    $('#loadingModal').modal('show');
}

function hideLoading() {
    $('#loadingModal').modal('hide');
}

function initDatepicker(selector) {
    $(selector).datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true
	});
}

function minTextLine(text, line) {
    if (text !== undefined) {
        var count = (text.match(/\n/g) || []).length;
        for (var i = 0; i < (line - count); i++) {
            text += '\n';
        }
    }
    return text;
}

//Example 30/01/2019 to 20190130
function convertDateToNumber(date) {
    var sprDate = date.split('/');
    if (sprDate.length === 3) {
        return parseInt(sprDate[2] + sprDate[1] + sprDate[0]);
    }

    return 0;
}

function encodeLineBreak(str) {
    return str.replace(/(?:\r\n|\r|\n)/g, '<br>');
}

function checkRadio(radioName, radioValue) {
    $('input:radio[name=' + radioName + ']').prop('checked', false);
    $('input:radio[name=' + radioName + '][value=' + radioValue + ']').prop('checked', true);
}

function getRadioValue(radioName) {
    return $('input:radio[name=' + radioName + ']:selected').val();
}

function getNextMountNumber(monthNum) {
    if (monthNum >= 12) {
        return 1;
    }
    return monthNum + 1;
}

function getTimesheetCurrentMonth() {
    var now = new Date();
    var month = now.getMonth() + 1;
    var year = now.getFullYear();
    var date = now.getDate();

    if (date > 20) {
        month += 1;

        if (month > 12) {
            month = 1;
        }
    }

    return month;
}

function replaceDuplicateOrder(order_id) {
    var length = strNotNull(order_id).indexOf('#');
    if (length != -1) {
        return order_id.substr(0, length);
    }
    return order_id;
}

function renderBarChart(selector, labels, series, header, subHeader, options) {
    var data = {
        labels: labels,
        series: series
    };
      
    if (!options) {
        options = { 
            plugins: [
                Chartist.plugins.tooltip(),
            ]
        };
    }

    $('.chart-header-pz').text(header);
    $('.chart-sub-header-pz').text(subHeader);
    
    var chart = new Chartist.Bar(selector, data, options).on('draw', function(data) {
        if(data.type === 'bar') {
            data.element.attr({
            style: 'stroke-width: 30px'
            });
        }
    }).on('created', function(bar) {
        $('.ct-bar').on('mouseover', function() {
          $('#tooltip').html('<b>Selected Value: </b>' + $(this).attr('ct:value'));
        });
      
        $('.ct-bar').on('mouseout', function() {
          $('#tooltip').html('<b>Selected Value:</b>');
        });
      });

}

function getDaysInMonth(month, year) {
    return new Date(year, month - 1, 0).getDate();
}