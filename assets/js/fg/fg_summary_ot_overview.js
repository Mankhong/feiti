$(document).ready(function () {
	initDatepicker('.datepicker');

	$('#btnSearch').on('click', function(){
		search();
	});

	$('#btnClear').on('click', function(){
		clear();
	});

	search();
});

function clear() {
	$('#ctrYear').val('');
	$('#ctrDepartment').val('');
}

function search() {
	var year = $('#ctrYear').val();
	var department = $('#ctrDepartment').val();

	//console.log($('#base_url').val() + 'fg_summary_ot_overview/get_summary_ajax?year=' + encodeString(year) + '&department=' + encodeString(department));

	showLoading();
	$.ajax({
		url: $('#base_url').val() + 'fg_summary_ot_overview/get_summary_ajax?year=' + encodeString(year) + '&department=' + encodeString(department),
		type: 'GET',
		success: function(resp) {
			if (resp.success) {
				var data = resp.data;
				var header = 'Summary O-T Overview';
				var y = year.substr(2, 3);
				var labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
				var series = [];

				for (var i = 0; i < 12; i++) {
					labels[i] = labels[i] + '-' + y;
				}

				if (data.length > 0) {
					series[0] = [];
					for (var i = 0; i < 12; i++) {
						series[0][i] = data[0]['m' + (i+1)];
					}
				}
				
				//renderResultTable(labels, series[0]);
				renderBarChart('.ct-chart', labels, series, header, '');
				hideLoading();
			}
		},
		fail: function(resp) {
			hideLoading();
		}
	});
}

function renderResultTable(months, sums) {
	var html = '<table id="rstable" class="table table-sm table-bordered tb-timesheet" style="width: 100%;">'
				+ '	<thead>'
				+ '		<tr>'
				+ '			<th class="text-center col-highlight">Month</th>';
				
	//-- HEADER
	for (var i = 0; i < months.length; i++) {
		html += '<th class="text-center">' + months[i] + '</th>';
	}
	html += '			<th class="text-right col-highlight">Total</th>'
			+ '		</tr>'
			+ '	</thead>'
			+ ' <tbody><tr><td class="text-center col-highlight"><strong>Total OT</strong></td>';
	//-- END HEADER

	//-- BODY
	var total = 0;
	for (var i = 0; i < sums.length; i++) {
		html += '<td class="text-right">' + formatCurrency(sums[i], 0) + '</td>';
		total += parseFloatOrNumeric(sums[i]);
	}
	//-- END BODY

	html += '<td class="text-right col-highlight">' + total+ '</td></tr><tbody></table>';

	$('#rstableContainer').html(html);
}