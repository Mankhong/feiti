$(document).ready(function () {
	initDatepicker('.datepicker');

	$('#btnSearch').on('click', function () {
		search();
	});

	$('#btnClear').on('click', function () {
		clear();
		setDefaultCriteria();
	});

	$('#ctrEmployeeNo').on('keyup', function (event) {
		if (event.keyCode === 13) {
			search();
		}
	});
	
	setDefaultCriteria();
});

function setDefaultCriteria() {
	$('#ctrDate').val(getCurrentDateDDMMYYYY());
}

function search() {
	if (validateSearch()) {
		var date = encodeString($('#ctrDate').val());
		var department = $('#ctrDepartment').val();
		var employee_type = $('input:radio[name=\'ctrEmployeeType\']:checked').val();
		var employee_no = encodeString($('#ctrEmployeeNo').val());

		loadDatatable(date, department, employee_type, employee_no);
	}
}

function validateSearch() {
	if ($('#ctrDate').val() == '') {
		showMessageModal('กรุณาระบุ Date');
		return false;
	}
	return true;
}

function showResult() {
	$('#resultContainer').show();
}

function hideResult() {
	$('#resultContainer').hide();
}

function clear() {
	hideResult();
	clearCriterias();
}

function clearCriterias() {
	$('#ctrDate').val('');
	$('#ctrDepartment').val('');
	$('#ctrEmployeeNo').val('');
	checkRadio('ctrEmployeeType', 'A');
}

function loadDatatable(date, department, employee_type, employee_no) {
	$("#rstable").dataTable().fnDestroy();

	showLoading();
	//console.log('fg_time_daily/get_daily_timesheet_ajax?date=' + date + '&department=' + department + '&employee_type=' + employee_type + '&employee_no=' + employee_no);
	var t = $('#rstable').DataTable({
		ajax: 'fg_time_daily/get_daily_timesheet_ajax?date=' + date + '&department=' + department + '&employee_type=' + employee_type + '&employee_no=' + employee_no,
		searching: false,
		ordering: true,
		lengthChange: false,
		paging: true,
		pageLength: 30,
		columns: [{
				'data': null,
				'render': function (data, type, row, meta) {
					return meta.row + 1;
				}
			},
			{
				'data': 'ems_no'
			},
			{
				'data': 'fullname_th'
			},
			{
				'data': 'department_name',
			},
			{
				'data': 'shift_name'
			},
			{
				'data': 'leave_type'
			},
			{
				'data': 'date'
			},
			{
				'data': 'start_work'
			},
			{
				'data': 'end_work'
			},
		],
		columnDefs: [{
			"targets": 0,
			"className": "text-center"
		}, {
			"targets": 1,
			"className": "text-center"
		}, {
			"targets": 5,
			"className": "text-center"
		}, {
			"targets": 6,
			"className": "text-center"
		}, {
			"targets": 7,
			"className": "text-center"
		}, {
			"targets": 8,
			"className": "text-center"
		}],
		oLanguage: {
			"sInfo": "" //Hide showing num of records
		},
		fnDrawCallback: function (settings) {
			//initExportButtons('#rstable', 'Billings', 'billings');

		},
		initComplete: function (settings, json) {
			hideLoading();
			showResult();
		}
	});

	//Sort without No.
	t.on('order.dt search.dt', function () {
		t.column(0, {
			search: 'applied',
			order: 'applied'
		}).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();
}

function exportExcel() {
	var date = encodeString($('#ctrDate').val());
	var department = $('#ctrDepartment').val();
	var employeeType = $('input:radio[name=\'ctrEmployeeType\']:checked').val();
	var employeeNo = encodeString($('#ctrEmployeeNo').val());

	$('#export_excel_form #date').val(date);
	$('#export_excel_form #department').val(department);
	$('#export_excel_form #employeeType').val(employeeType);
	$('#export_excel_form #employeeNo').val(employeeNo);
	$('#export_excel_form').submit();
}