$(document).ready(function () {
	$('#form').submit(function(e){
		e.preventDefault(); 
		
		showLoading();
		$.ajax({
			url: $('#base_url').val() + 'fg_import/upload_excel/',
			type: 'post',
			data:new FormData(this),
			processData:false,
			contentType:false,
			success: function(data){

				if (data.success) {
					var is_success = (data.count_fail == 0);
					var title = is_success ? 'SUCCESS' : 'FAIL';
					var color = is_success ? '#5B0' : '#F00';
					var result = '<span style="color: ' + color + ';">[ total records: ' + data.count_total
							   + ', success: ' + data.count_success
							   + ', fail: ' + data.count_fail
							   + ', inserted: ' + data.count_inserted + ' ]</span>';

					showMessageModal(title + ' : ' + result);
				} else {
					showMessageModal('FAIL. ' + data.message);
				}

				hideLoading();
			},
			error: function(data) {
				hideLoading();
			}
		});
	});

	$('#cal_form').submit(function(e){
		e.preventDefault(); 
		var date = $('#ctrDate').val();
		var badgenumber = convertBadgenumber($('#ctrEmployeeNo').val());
		$('#ctrBadgenumber').val(badgenumber);

		if (badgenumber || date) {
			showLoading();
			$.ajax({
				url: $('#base_url').val() + 'fg_import/calculate_timesheet',
				type: 'post',
				data:new FormData(this),
				processData:false,
				contentType:false,
				success: function(data){
					hideLoading();
					showMessageModal('Calculate timesheet success');
				},
				error: function(data) {
					hideLoading();
				}
			});
		} else {
			var startDate = $('#ctrStartDate').val();
			$('#ctrCalculatingDate').val(startDate);
			$('#ctrDate').val(startDate);
			showCustomLoading('Calculating... ' + startDate);
			$.ajax({
				url: $('#base_url').val() + 'fg_import/calculate_timesheet',
				type: 'post',
				data:new FormData(this),
				processData:false,
				contentType:false,
				success: function(data){
					calDateByDate();
				},
				error: function(data) {
					hideCustomLoading();
				}
			});
		}
	});

	$('#init_form').submit(function(e){
		e.preventDefault(); 
		var badgenumber = convertBadgenumber($('#initCtrEmployeeNo').val());
		$('#initCtrBadgenumber').val(badgenumber);
		
		showLoading();
		$.ajax({
			url: $('#base_url').val() + 'fg_import/init_timesheet',
			type: 'post',
			data:new FormData(this),
			processData:false,
			contentType:false,
			success: function(data){
				hideLoading();
				showMessageModal('Init timesheet success');
			},
			error: function(data) {
				hideLoading();
			}
		});
	});

	$('#btnClear').on('click', function(){
		clear();
	});

	$('#ctrYear').on('change', function(){
		initDateDropdown();
	});

	$('#ctrMonth').on('change', function(){
		initDateDropdown();
	});

	setDefaultCtr();
});

function showCustomLoading(message) {
	$('#customLoadingMessage').html(message);
	$('#customLoadingModal').modal('show');
}

function hideCustomLoading() {
	$('#customLoadingModal').modal('hide');
}

function calDateByDate() {
	var clDate = $('#ctrCalculatingDate').val();
	if (clDate) {

		var hasNext = false;
		var dateItems = $('#ctrDate option');
		for (var i = 0; i < dateItems.length; i++) {
			if (clDate == dateItems[i].value) {
				if ((i + 1) < dateItems.length) {
					$('#ctrCalculatingDate').val(dateItems[i + 1].value);
					hasNext = true;
					break;
				} else {
					$('#ctrCalculatingDate').val('');
				}
			}
		}

		if (hasNext) {
			$('#ctrDate').val($('#ctrCalculatingDate').val());
			showCustomLoading('Calculating... ' + $('#ctrDate').val());
			$.ajax({
				url: $('#base_url').val() + 'fg_import/calculate_timesheet',
				type: 'post',
				data:new FormData($('#cal_form')[0]),
				processData:false,
				contentType:false,
				success: function(data){
					calDateByDate();
				},
				error: function(data) {
					hideCustomLoading();
				}
			});

		} else {
			hideCustomLoading();
		}

	} else {
		$('#ctrCalculatingDate').val('');
		hideCustomLoading();
	}
}

function convertBadgenumber(badgenumber) {
	if (badgenumber) {
		badgenumber = badgenumber.replace('A', '').replace('B', '').replace('C', '').replace('FP', '');
		return parseInt(badgenumber).toString();
	}

	return '';
}

function setDefaultCtr() {
	var now = new Date();
	var date = now.getDate();
	var month = now.getMonth() + 1;
	var year = now.getFullYear();

	if (month == 12 && date > 20) {
		year += 1;
	}

	$('#ctrMonth').val(getTimesheetCurrentMonth());
	$('#ctrYear').val(year);
	$('#initCtrMonth').val(getTimesheetCurrentMonth());
	$('#initCtrYear').val(year);

	initDateDropdown();
	$('#ctrDate').val(now.getDate() + '/' + month + '/' + year);
	//$('#initCtrDate').val(now.getDate() + '/' + month + '/' + year);
}

function initDateDropdown() {
	var month = $('#ctrMonth').val();
	var year = $('#ctrYear').val();
	var month_before = getMonthBefore(month);
	var year_before = getYearBefore(month, year);
	var days_before = getDaysInMonth(month, year);
	var days = 20;
	var html = '<option value="">-- All --</option>';

	for (var i = 21; i <= days_before; i++) {
		var date =  + i + '/' + month_before + '/' + year_before;
		html += '<option value="' + date + '">' + date + '</option>';

		if (i == 21) { 
			$('#ctrStartDate').val(date);
			$('#initCtrStartDate').val(date);
		}
	}

	for (var i = 1; i <= days; i++) {
		var date =  + i + '/' + month + '/' + year;
		html += '<option value="' + date + '">' + date + '</option>';

		if (i == days) { 
			$('#ctrEndDate').val(date);
			$('#initCtrEndDate').val(date);
		}
	}

	$('#ctrDate option').remove();
	$('#ctrDate').append(html);
	$('#ctrDate').val('');

	$('#initCtrDate option').remove();
	$('#initCtrDate').append(html);
	$('#initCtrDate').val('');

	console.log('<<< html', html);
}

function getMonthBefore(currentMonth) {
	return currentMonth == 1 ? 12 : (currentMonth - 1);
}

function getYearBefore(currentMonth, currentYear) {
	return currentMonth == 1 ? (currentYear - 1) : currentYear;
}

function clear() {
	$('#ctrDate').val('');
	$('#initCtrDate').val('');
	checkRadio('ctrShiftA', 'D');
	checkRadio('ctrShiftB', 'D');
}