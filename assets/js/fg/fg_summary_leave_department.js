$(document).ready(function () {
	initDatepicker('.datepicker');

	$('#btnSearch').on('click', function(){
		search();
	});

	$('#btnClear').on('click', function(){
		clear();
	});

	search();
});

function clear() {
	$('#ctrStartDate').val('');
	$('#ctrEndDate').val('');
	$('#ctrLeaveType').val('');
}

function search() {
	var start_date = $('#ctrStartDate').val();
	var end_date = $('#ctrEndDate').val();
	var leave_type = $('#ctrLeaveType').val();

	//console.log($('#base_url').val() + 'fg_summary_leave_department/get_summary_ajax?start_date=' + encodeString(start_date) + '&end_date=' + encodeString(end_date) + '&leave_type=' + encodeString(leave_type));

	showLoading();
	$.ajax({
		url: $('#base_url').val() + 'fg_summary_leave_department/get_summary_ajax?start_date=' + encodeString(start_date) + '&end_date=' + encodeString(end_date) + '&leave_type=' + encodeString(leave_type),
		type: 'GET',
		success: function(resp) {
			if (resp.success) {
				var data = resp.data;
				var departments = resp.departments;
				var header = 'Summary Leave of Department';
				var sub_header = getSubHeader();
				var keys = ['dp1', 'dp2', 'dp3', 'dp4', 'dp5', 'dp6', 'dp7', 'dp8', 'dp9', 'dp10', 'dp11', 'dp12', 'dp13', 'dp14', 'dp15', 'dp16', 'dp17'];
				var labels = [];
				var series = [];

				for(var i = 0; i < departments.length; i++) {
					labels[i] = departments[i].id;
				}

				if (data.length > 0) {
					series[0] = [];
					for (var i = 0; i < keys.length; i++) {
						series[0][i] = data[0][keys[i]];
						keys[i] = keys[i].toUpperCase();
					}
				}
				
				renderBarChart('.ct-chart', labels, series, header, sub_header);
				hideLoading();
			}
		},
		fail: function(resp) {
			hideLoading();
		}
	});
}

function getSubHeader() {
	var start_date = $('#ctrStartDate').val()
	var end_date = $('#ctrEndDate').val();

	if (start_date != '' && end_date != '') {
		return start_date + ' - ' + end_date;
	} else if (start_date != '') {
		return 'From ' + start_date;
	} else if (end_date != '') {
		return 'To ' + end_date;
	}

	return '';
}