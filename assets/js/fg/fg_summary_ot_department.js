$(document).ready(function () {
	initDatepicker('.datepicker');

	$('#btnSearch').on('click', function(){
		search();
	});

	$('#btnClear').on('click', function(){
		clear();
	});

	search();
});

function clear() {
	$('#ctrStartDate').val('');
	$('#ctrEndDate').val('');
}

function search() {
	var start_date = $('#ctrStartDate').val();
	var end_date = $('#ctrEndDate').val();

	showLoading();
	$.ajax({
		url: $('#base_url').val() + 'fg_summary_ot_department/get_summary_ajax?start_date=' + encodeString(start_date) + '&end_date=' + encodeString(end_date),
		type: 'GET',
		success: function(resp) {
			if (resp.success) {
				var data = resp.data;
				var header = 'Summary O-T Report of Department';
				var sub_header = getSubHeader();
				var labels = [];
				var series = [];

				if (data.length > 0) {
					series[0] = [];
					for (var i = 0; i < data.length; i++) {
						labels[i] = data[i].department_id;
						series[0][i] = data[i].sum_ot;
					}
				}
				
				//renderResultTable(data);
				renderBarChart('.ct-chart', labels, series, header, sub_header);
				hideLoading();
			}
		},
		fail: function(resp) {
			hideLoading();
		}
	});
}

function getSubHeader() {
	var start_date = $('#ctrStartDate').val()
	var end_date = $('#ctrEndDate').val();

	if (start_date != '' && end_date != '') {
		return start_date + ' - ' + end_date;
	} else if (start_date != '') {
		return 'From ' + start_date;
	} else if (end_date != '') {
		return 'To ' + end_date;
	}

	return '';
}

function renderResultTable(data) {
	var html = '<table id="rstable" class="table table-sm table-bordered tb-timesheet" style="width: 100%;">'
				+ '	<thead>'
				+ '		<tr>'
				+ '			<th class="text-center col-highlight">Department</th>';
				
	//-- HEADER
	var header_columns = [];
	for (var i = 0; i < data.length; i++) {
		header_columns[i] = data[i].department_id;
		html += '<th class="text-center">' + header_columns[i] + '</th>';
	}
	html += '			<th class="text-right col-highlight">Total</th>'
			+ '		</tr>'
			+ '	</thead>'
			+ ' <tbody><tr><td class="text-center col-highlight"><strong>Total OT</strong></td>';
	//-- END HEADER

	//-- BODY
	var total = 0;
	for (var i = 0; i < data.length; i++) {
		html += '<td class="text-right">' + formatCurrency(data[i].sum_ot, 0) + '</td>';
		total += parseFloatOrNumeric(data[i].sum_ot);
	}
	//-- END BODY

	html += '<td class="text-right col-highlight">' + total+ '</td></tr><tbody></table>';

	$('#rstableContainer').html(html);
}