$(document).ready(function () {
	$('#btnCancel').on('click', function(){
		cancelHoliday();
	});

	initDatepicker('.datepicker');
});

function cancelHoliday() {
	var holidayID = encodeString($('#id').val());
	localStorage.setItem('holiday_id', holidayID);
	$('#confirmCancelModal').modal('show');
}

function confirmCancelHoliday() {
	var holidayID = localStorage.getItem('holiday_id');
	var form = $('#form');
	var action = $('#base_url').val() + 'fg_holiday/cancel/' + holidayID;
	$(form).attr('action', action);
	$(form).submit();
}

function onclickSave() {
	$('#form').submit();
}
