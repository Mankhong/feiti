$(document).ready(function(){
	$('#id').on('blur', function(){
		check_employee_id();
	});

	initDatepicker('.datepicker');
	checkDisableButton();
});

function onclickSave() {
	$('#form').submit();
}

function check_employee_id() {
	var employee_id = $('#id').val();
	var temp_employee_id = $('#temp_employee_id').val();

	if (employee_id != '' && employee_id != temp_employee_id) {
		console.log($('#base_url').val() + 'fg_employee/has_data_ajax?employee_id=' + encodeString(employee_id));
		$.ajax({
			url: $('#base_url').val() + 'fg_employee/has_data_ajax?employee_id=' + encodeString(employee_id),
			type: 'GET',
			success: function(resp) {
				if (resp.success) {
					if (resp.has_data) {
						$('#employee_id_err').html('&nbsp;&nbsp;Employee ID already in use.');
					} else {
						$('#employee_id_err').html('');
					}
				}
				checkDisableButton();
			}
		});
	} else {
		$('#employee_id_err').html('');
	}

	checkDisableButton();
}

function checkDisableButton() {
	var id = $('#id').val();
	var temp_employee_id = $('#temp_employee_id').val();
	var employee_id_err = $('#employee_id_err').html().trim();

	if (id != '' && employee_id_err == '') {
		$('#btn_create').removeClass('disabled');
	} else {
		$('#btn_create').addClass('disabled');
	}
}