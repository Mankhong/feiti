$(document).ready(function () {
	initDatepicker('.datepicker');

	$('#btnSearch').on('click', function(){
		search();
	});

	$('#btnClear').on('click', function(){
		clear();
	});

	$('#ctrEmployeeNo').on('keyup', function(event){
		if (event.keyCode === 13) {
			search();
		}
	});

	$('#ctrEmployeeType').change(function() {
		$('#ctrEmployeeNo').val('');
	});

	$('#ctrEmployeeID').focus();
	setDefaultCtr();

	window.addEventListener("resize", function(){
		fitTable();
	});

	//*** */
	//search();

});

function fitTable() {
	var offset = 20;
	var width = (window.innerWidth - offset) + 'px';
	$('div.dataTables_wrapper').css('width', width);
}

function setDefaultCtr() {
	var now = new Date()
	var date = now.getDate();
	var month = now.getMonth() + 1;
	var year = now.getFullYear();

	if (month == 12 && date > 20) {
		year += 1;
	}

	$('#ctrMonth').val();
	$('#ctrYear').val(year);
}

function search() {
	if (validateSearch()) {
		var year = $('#ctrYear').val();
		var month = $('#ctrMonth').val();
		var department = $('#ctrDepartment').val();
		var employeeType = $('#ctrEmployeeType').val();
		//var employeeType = $('input:radio[name=\'ctrEmployeeType\']:checked').val();
		var employeeNo = encodeString($('#ctrEmployeeNo').val());

		clearResult(); 
		loadDatatable(year, month, department, employeeType, employeeNo);
	}
}

function validateSearch() {
	/*
	if ($('#ctrEmployeeID').val() == '') {
		showMessageModal('กรุณาระบุ Employee ID');
		return false;
	}
	*/

	return true;
}

//*** */
function clear() {
	hideResult();
	clearCriterias();
	clearResult();
}

//*** */
function clearCriterias() {
	$('#ctrStartDate').val('');
	$('#ctrEndDate').val('');
	$('#ctrDepartment').val('');
	$('#ctrEmployeeType').val('');
	$('#ctrEmployeeNo').val('');
}

//*** */
function clearResult() {
	//*** */
}

function showResult() {
	$('#resultContainer').show();
}

function hideResult() {
	$('#resultContainer').hide();
}

function loadDatatable(year, month, department, employeeType, employeeNo) {
	$("#rstable").dataTable().fnDestroy();

	hideResult();
	showLoading();
	var t = $('#rstable').DataTable({
		ajax: 'fg_time_summary/get_summary_timesheet_ajax?year=' + year + '&month=' + month + '&department=' + department + '&employeeType=' + employeeType + '&employeeNo=' + employeeNo,
		searching: false,
		ordering: false,
		lengthChange: false,
		pageLength: 20,
		scrollX: true,
		scrollCollapse: true,
		fixedColumns:   {
			leftColumns: 7,
		},
		columns: [{
				'data': null,
				'render': function (data, type, row, meta) {
					return '<span class="item-no">' + (meta.row + 1) + '</span>';
				}
			},
			{
				'data': 'department_name',
				'render': function (data, type, row, meta) {
					return data;
				}
			},
			{
				'data': 'ems_no',
				'render': function (data, type, row, meta) {
					return '<span class="item-ems-no">' + strNotNull(data) + '</span>';
				}
			},
			{
				'data': 'fullname_th',
				'render': function (data, type, row, meta) {
					return '<span class="item-fullnamme-th">' + strNotNull(data) + '</span>';
				}
			},
			{
				'data': 'entry_date',
				'render': function (data, type, row, meta) {
					return '<span class="item-entry-date">' + strNotNull(data) + '</span>';
				}
			},
			{
				'data': 'department_id',
				'render': function (data, type, row, meta) {
					return data;
				}
			},
			{
				'data': 'position_name',
				'render': function (data, type, row, meta) {
					return '<span class="item-position-name">' + strNotNull(data) + '</span>';
				}
			},
			{
				'data': 'working_day100',
				'render': function (data, type, row, meta) {
					if (row['working_day100'] != '') {
						var n = parseFloatOrNumeric(row['working_day100']) - parseFloatOrNumeric(row['co']);

						if (row['employee_type'] == 'B') {
							n += parseFloatOrNumeric(row['pay_holiday']);
						}

						return '<span class="item-working-day100">' + n + '</span>';
					}
					return '';
				}
			},
			{
				'data': 'sum_night_shift',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-sum-night-shift' + (n > 0 ? ' nsf-color-1' : ' nsf-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'sum_food_travel_day',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-sum-food-travel-day' + (n > 0 ? ' ftd-color-1' : ' ftd-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'sum_ot1',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-sum-ot1' + (n > 0 ? ' ot-color-1' : ' ot-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'sum_ot15',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-sum-ot15' + (n > 0 ? ' ot-color-1' : ' ot-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'sum_ot2',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-sum-ot2' + (n > 0 ? ' ot-color-1' : ' ot-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'sum_ot3',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-sum-ot3' + (n > 0 ? ' ot-color-1' : ' ot-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'prev_diligence',
				'render': function (data, type, row, meta) {
					var d = strNotNull(data);
					var n = (d == '' ? d : formatCurrency(d, 0));
					return '<span class="item-diligence">' + n + '</span>';
				}
			},
			{
				'data': 'diligence',
				'render': function (data, type, row, meta) {
					var d = strNotNull(data);
					var n = (d == '' ? d : formatCurrency(d, 0));
					return '<span class="item-diligence">' + n + '</span>';
				}
			},
			{
				'data': 'slh',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-slh' + (n > 0 ? ' lv-color-1' : ' lv-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'sl',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-sl' + (n > 0 ? ' lv-color-1' : ' lv-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': null,
				'render': function (data, type, row, meta) {
					var totalSl = parseFloatOrNumeric(row['slh']) + parseFloatOrNumeric(row['sl']);
					return '<span class="item-total-sl' + (totalSl > 0 ? ' lv-color-1' : ' lv-color-2') + '">' + totalSl + '</span>';
				}
			},
			{
				'data': 'al',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-al' + (n > 0 ? ' lv-color-1' : ' lv-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'ab',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-ab' + (n > 0 ? ' lv-color-1' : ' lv-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'co',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-co' + (n > 0 ? ' lv-color-1' : ' lv-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'asl',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-asl' + (n > 0 ? ' lv-color-1' : ' lv-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'dfl',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-dfl' + (n > 0 ? ' lv-color-1' : ' lv-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'mrl',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-mrl' + (n > 0 ? ' lv-color-1' : ' lv-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'ma',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-ma' + (n > 0 ? ' lv-color-1' : ' lv-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'ml',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-ml' + (n > 0 ? ' lv-color-1' : ' lv-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'rl',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-rl' + (n > 0 ? ' lv-color-1' : ' lv-color-2') + '">' + n + '</span>';
				}
			},{
				'data': 'cl',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-cl' + (n > 0 ? ' lv-color-1' : ' lv-color-2') + '">' + n + '</span>';
				}
			},{
				'data': 'clh',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-clh' + (n > 0 ? ' lv-color-1' : ' lv-color-2') + '">' + n + '</span>';
				}
			},{
				'data': null,
				'render': function (data, type, row, meta) {
					//var n = parseFloatOrNumeric(data);
					return '';//'<span class="item-warning-letter' + (n > 0 ? ' lv-color-1' : ' lv-color-2') + '">' + n + '</span>';
				}
			},{
				'data': null,
				'render': function (data, type, row, meta) {
					//var n = parseFloatOrNumeric(data);
					return '';//'<span class="item-late-times' + (n > 0 ? ' lv-color-1' : ' lv-color-2') + '">' + n + '</span>';
				}
			},{
				'data': null,
				'render': function (data, type, row, meta) {
					//var n = parseFloatOrNumeric(data);
					return '';//'<span class="item-late-minute' + (n > 0 ? ' lv-color-1' : ' lv-color-2') + '">' + n + '</span>';
				}
			},
		],
		columnDefs: [{
				"targets": 0,
				"className": "text-center"
			},{
				"targets": 2,
				"className": "text-center"
			},{
				"targets": 4,
				"className": "text-center"
			},{
				"targets": 7,
				"className": "text-right"
			},{
				"targets": 8,
				"className": "text-right"
			},{
				"targets": 9,
				"className": "text-right"
			},{
				"targets": 10,
				"className": "text-right"
			},{
				"targets": 11,
				"className": "text-right"
			},{
				"targets": 12,
				"className": "text-right"
			},{
				"targets": 13,
				"className": "text-right"
			},{
				"targets": 14,
				"className": "text-right diligence-col"
			},{
				"targets": 15,
				"className": "text-right diligence-col"
			},{
				"targets": 16,
				"className": "text-right"
			},{
				"targets": 17,
				"className": "text-right"
			},{
				"targets": 18,
				"className": "text-right"
			},{
				"targets": 19,
				"className": "text-right"
			},{
				"targets": 20,
				"className": "text-right"
			},{
				"targets": 21,
				"className": "text-right"
			},{
				"targets": 22,
				"className": "text-right"
			},{
				"targets": 23,
				"className": "text-right"
			},{
				"targets": 24,
				"className": "text-right"
			},{
				"targets": 25,
				"className": "text-right"
			},{
				"targets": 26,
				"className": "text-right"
			},{
				"targets": 27,
				"className": "text-right"
			},{
				"targets": 28,
				"className": "text-right"
			},{
				"targets": 29,
				"className": "text-right"
			}
		],
		initComplete: function (settings, json) {
			setDiligenceLabels();
			hideLoading();
			fitTable();
			showResult();
		}
	});

	//Sort without No.
	/*
	t.on('order.dt search.dt', function () {
		t.column(0, {
			search: 'applied',
			order: 'applied'
		}).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();
	*/
}

function setDiligenceLabels() {
	var month_names = ['Jan', 'Feb', 'Mar', 'Apl', 'May' , 'Jun', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	var m = $('#ctrMonth').val();
	var y = $('#ctrYear').val().substring(2);
	var monthBefore = '';
	var month = '';

	/*
	if (m != '') {
		monthBefore = month_names[m-2] + "'" + y;
		month = month_names[m-1] + "'" + y;
		showDiligenceColumns();
	} else {
		hideDiligenceColumns();
	}
	*/
	
	$('#thMonthBefore').text(monthBefore);
	$('#thMonth').text(month);
}

function showDiligenceColumns() {
	$('.diligence-col').removeClass('d-none');
}

function hideDiligenceColumns() {
	$('.diligence-col').removeClass('d-none');
	$('.diligence-col').addClass('d-none');
}

function exportOTExcel() {
	var year = $('#ctrYear').val();
	var month = $('#ctrMonth').val();
	var department = $('#ctrDepartment').val();
	var employeeType = $('#ctrEmployeeType').val();
	//var employeeType = $('input:radio[name=\'ctrEmployeeType\']:checked').val();
	var employeeNo = encodeString($('#ctrEmployeeNo').val());

	$('#export_excel_ot_form #year').val(year);
	$('#export_excel_ot_form #month').val(month);
	$('#export_excel_ot_form #department').val(department);
	$('#export_excel_ot_form #employeeType').val(employeeType);
	$('#export_excel_ot_form #employeeNo').val(employeeNo);
	$('#export_excel_ot_form').submit();
}

function exportExcel() {
	var year = $('#ctrYear').val();
	var month = $('#ctrMonth').val();
	var department = $('#ctrDepartment').val();
	var employeeType = $('#ctrEmployeeType').val();
	//var employeeType = $('input:radio[name=\'ctrEmployeeType\']:checked').val();
	var employeeNo = encodeString($('#ctrEmployeeNo').val());

	$('#export_excel_form #year').val(year);
	$('#export_excel_form #month').val(month);
	$('#export_excel_form #department').val(department);
	$('#export_excel_form #employeeType').val(employeeType);
	$('#export_excel_form #employeeNo').val(employeeNo);
	$('#export_excel_form').submit();
}