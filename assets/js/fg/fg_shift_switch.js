$(document).ready(function () {
	initDatepicker('.datepicker');

	$('#btnAdd').on('click', function(){
		insertShiftSwitch();
	});

	$('#btnClear').on('click', function(){
		clear();
	});

	$('#btnEditorSave').on('click', function(){
		editShfitSwitch();
	});

	loadDatatable();
});

function clear() {
	$('#ctrDate').val('');
	checkRadio('ctrShiftA', 'D');
	checkRadio('ctrShiftB', 'D');
}

function insertShiftSwitch() {
	var date = $('#ctrDate').val();
	var shiftA = $('input:radio[name=ctrShiftA]:checked').val();
	var shiftB = $('input:radio[name=ctrShiftB]:checked').val();

	if (validateInsert(date, shiftA, shiftB)) {
		var item = {'date' : date,
			'shift_a' : shiftA,
			'shift_b' : shiftB,
		};

		showLoading();
		$.ajax({
			url: 'fg_shift_switch/insert_shift_switch_ajax',
			type: 'POST',
			data: item,
			success: function(resp) {
				loadDatatable();
			},
			error: function(resp) {
				console.log(resp);
				hideLoading();
				showMessageModal('พบข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้!');
			},
		});
	}
}

function validateInsert(date, shiftA, shiftB, id) {
	if (date === '') {
		showMessageModal('กรุณาระบุวันที่เปลี่ยนกะ (Date)');
		return false;
	}

	if (shiftA === '') {
		showMessageModal('กรุณาระบุกะ A');
		return false;
	}

	if (shiftB === '') {
		showMessageModal('กรุณาระบุกะ B');
		return false;
	}

	if (shiftA === shiftB) {
		showMessageModal('กะ A และ กะ B ต้องไม่ซ้ำกัน');
		return false;
	}

	if (id === '') {
		showMessageModal('ไม่พบ ID ข้อมูลที่เลือก กรุณาดำเนินการใหม่อีกครั้ง');
		return false;
	}

	return true;
}

function editShfitSwitch() {
	var id = $('#edtId').val();
	var date = $('#edtDate').val();
	var shiftA = $('input:radio[name=edtShiftA]:checked').val();
	var shiftB = $('input:radio[name=edtShiftB]:checked').val();

	if (validateInsert(date, shiftA, shiftB, id)) {
		var item = {'id' : id,
			'date' : date,
			'shift_a' : shiftA,
			'shift_b' : shiftB,
		};

		console.log(item);

		showLoading();
		$.ajax({
			url: 'fg_shift_switch/edit_shift_switch_ajax',
			type: 'POST',
			data: item,
			success: function(resp) {
				hideEditorModal();
				hideLoading();
				loadDatatable();
			},
			error: function(resp) {
				console.log(resp);
				hideLoading();
				showMessageModal('พบข้อผิดพลาด ไม่สามารถแก้ไขข้อมูลได้!');
			},
		});
	}
}

function editItem(e) {
	var tr = $(e).parent().parent();
	var id = $(tr).find('.item-id').val();
	var no = $(tr).find('.item-no').text();
	var date = $(tr).find('.item-date').text();
	var shiftA = $(tr).find('.item-shift-a').val();
	var shiftB = $(tr).find('.item-shift-b').val();

	//Default shift
	shiftA = (shiftA == '' ? 'D' : shiftA);
	shiftB = (shiftB == '' ? 'D' : shiftB);

	$('#edtId').val(id);
	$('#edtNo').text(no);
	$('#edtDate').val(date);

	$('input:radio[name=edtShiftA]').prop('checked', false);
	$('input:radio[name=edtShiftA][value=\'' + shiftA +'\']').prop('checked', true);

	$('input:radio[name=edtShiftB]').prop('checked', false);
	$('input:radio[name=edtShiftB][value=\'' + shiftB +'\']').prop('checked', true);

	showEditorModal();
}

function showEditorModal() {
	$('#editorModal').modal('show');
}

function hideEditorModal() {
	$('#editorModal').modal('hide');
}


function loadDatatable() {
	$("#rstable").dataTable().fnDestroy();

	showLoading();
	var t = $('#rstable').DataTable({
		ajax: 'fg_shift_switch/get_shift_switch_list_ajax',
		searching: false,
		ordering: false,
		lengthChange: false,
		pageLength: 30,
		columns: [{
				'data': 'id',
				'render': function (data, type, row, meta) {
					return '<span class="item-no">' + (meta.row + 1) + '</span>' +
						'<input type="hidden" class="item-id" value="' + data + '"/>';
				}
			},
			{
				'data': 'fm_date',
				'render': function (data, type, row, meta) {
					return '<span class="item-date">' + strNotNull(data)+ '</span>';
				}
			},
			{
				'data': 'shift_a',
				'render': function (data, type, row, meta) {
					var sh = (data == 'D' ? 'Day' : (data == 'N' ? 'Night' : ''));
					return '<span class="item-shift-a-dsp">' + sh + '</span>' +
						'<input type="hidden" class="item-shift-a" value="' + strNotNull(data) + '"/>';
				}
			},
			{
				'data': 'shift_b',
				'render': function (data, type, row, meta) {
					var sh = (data == 'D' ? 'Day' : (data == 'N' ? 'Night' : ''));
					return '<span class="item-shift-b-dsp">' + sh + '</span>' +
						'<input type="hidden" class="item-shift-b" value="' + strNotNull(data) + '"/>';
				}
			},
			{
				'data': 'last_update_date'
			},
			{
				'data': 'last_update_user'
			},
			{
				'data': "id",
				'render': function (data, type, row, meta) {
					var base_url = $('#base_url').val();
					var id = encodeString(data);
					return '<a class="btn btn-default btn-icon" onclick="editItem(this)"><i class="far fa-edit"></i></a>';
				}
			},
		],
		columnDefs: [{
				"targets": 0,
				"className": "text-center"
			},
			{
				"targets": 1,
				"className": "text-center"
			},
			{
				"targets": 2,
				"className": "text-center"
			},
			{
				"targets": 3,
				"className": "text-center"
			},
			{
				"targets": 4,
				"className": "text-center"
			},
			{
				"targets": 6,
				"className": "text-center"
			},
		],
		initComplete: function (settings, json) {
			hideLoading();
		},
	});

	//Sort without No.
	t.on('order.dt search.dt', function () {
		t.column(0, {
			search: 'applied',
			order: 'applied'
		}).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();

}