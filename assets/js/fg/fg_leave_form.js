$(document).ready(function () {
	$('.employeePickerTrigger').on('click', function(){
		EmployeePicker.showPicker();
	});

	$('#btnCancel').on('click', function(){
		cancelLeave();
	});

	$('#employee_id').on('keydown', function(event) {
		if (event.keyCode === 13) {
			event.preventDefault();
			$('.customerPickerTrigger').click();
		}
	});

	EmployeePicker.selectedCallBack = function(data) {
		EmployeePicker.hidePicker();
		$('#employee_id').val(data['employee_id']);
		$('#employee_no').val(data['ems_no']);
		$('#employee_name').val(data['fullname_th']);
	}

	initDatepicker('.datepicker');
});

function cancelLeave() {
	var leaveId = encodeString($('#id').val());
	localStorage.setItem('leave_id', leaveId);
	$('#confirmCancelModal').modal('show');
}

function confirmCancelLeave() {
	var leaveId = localStorage.getItem('leave_id');
	var form = $('#form');
	var action = $('#base_url').val() + 'fg_leave/cancel/' + leaveId;
	$(form).attr('action', action);
	$(form).submit();
}

function onclickSave() {
	var employee_id = encodeString($('#employee_id').val());
	var from_date = encodeString($('#from_date').val());
	var to_date = encodeString($('#to_date').val());
	var from_date_temp = encodeString($('#from_date_temp').val());
	var to_date_temp = encodeString($('#to_date_temp').val());
	var action_type = $('#action_type').val();

	if (employee_id == '') {
		showMessageModal('กรุณาระบุ Employee');
		return;
	}

	if (from_date == '') {
		showMessageModal('กรุณาระบุ From Date');
		return;
	}

	if (to_date == '') {
		showMessageModal('กรุณาระบุ To Date');
		return;
	}

	var ajax_url = $('#base_url').val() + 'fg_leave/has_leave_ajax?employee_id=' + employee_id + '&from_date=' + from_date + '&to_date=' + to_date + '&from_date_temp=' + from_date_temp + '&to_date_temp=' + to_date_temp + '&action_type=' + action_type;

	//console.log(ajax_url);

	$.ajax({
		url:  ajax_url,
		type: 'GET',
		success: function(resp) {
			if (resp.success) {
				if (!resp.has_leave) {
					$('#form').submit();
				} else {
					$('#confirmDuplicateModalMessage').html(resp.err_message);
					$('#confirmDuplicateModal').modal('show');
				}
			} else {
				showMessageModal('พบข้อผิดพลาด ไม่สามารถดำเนินการได้!');
			}
		}
	});
}

function confirmSubmitLeave() {
	$('#form').submit();
}