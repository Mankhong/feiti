$(document).ready(function () {
	loadDatatable();
});

function hideSearch() {
	$('#rstable_filter').css('display', 'none');
}

function loadDatatable() {
	$("#rstable").dataTable().fnDestroy();

	var t = $('#rstable').DataTable({
		ajax: 'fg_holiday/get_holiday_list_ajax',
		searching: true,
		ordering: false,
		lengthChange: false,
		pageLength: 20,
		columns: [{
				'data': null,
				'render': function (data, type, row, meta) {
					return meta.row + 1;
				}
			},
			{
				'data': 'fm_date'
			},
			{
				'data': 'desc'
			},
			{
				'data': 'holiday_type_name'
			},
			{
				'data': 'last_update_date'
			},
			{
				'data': 'last_update_user'
			},
		],
		columnDefs: [{
				"targets": 0,
				"className": "text-center"
			},
			{
				"targets": 1,
				"className": "text-center"
			},
			{
				"targets": 4,
				"className": "text-center"
			},
		],
		fnDrawCallback: function (settings) {
			hideSearch();
		},
	});

	//Sort without No.
	t.on('order.dt search.dt', function () {
		t.column(0, {
			search: 'applied',
			order: 'applied'
		}).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();

	//=== Custom Filter
	$.fn.dataTable.ext.search.push(
		function (settings, data, dataIndex) {
			var match = true;
			var search = $('#action_search').val();
			if (search != '') {
			    match = (data[1].toLowerCase().indexOf(search.toLowerCase()) >= 0 || data[2].toLowerCase().indexOf(search.toLowerCase()) >= 0);
			}

			return match;
		}
	);

	$('#action_search').keyup(function (event) {
		t.draw();
    });
	//=== End Custom Filter

	$('#rstable tbody').on('click', 'tr', function () {
		var data = t.row(this).data();
		var id = encodeString(data['id']);
		window.location.href = $('#base_url').val() + 'fg_holiday/edit/' + id;
	});
}