var EmployeePicker = {
	showPicker: function () {
        $("#employeePickerTable").dataTable().fnDestroy();
		$('#employeePickerModal').modal('show');

		var t = $('#employeePickerTable').DataTable({
            ajax: $('#base_url').val() + 'fg_employee/get_employee_list_ajax',
            select: true,
			searching: true,
			ordering: true,
			order: [
				[1, 'asc']
			],
			lengthChange: false,
			pageLength: 10,
			columns: [{
					'data': null,
					'render': function (data, type, row, meta) {
						return meta.row + 1;
					}
				},
				{
					'data': 'ems_no'
				},
				{
					'data': 'fullname_th'
				},
				{
					'data': 'department_name'
				},
				{
					'data': 'job_position_name'
                },
			],
			columnDefs: [{
					"targets": 0,
					"className": "text-center"
				},
				{
					"targets": 1,
					"className": "text-center"
				},
			],
			fnDrawCallback: function (settings) {
				$('input[aria-controls=employeePickerTable]').focus();
				EmployeePicker.setSearchEvent();
			},
		});

		//=== Custom Filter
		$.fn.dataTable.ext.search.push(
			function (settings, data, dataIndex) {
				var search = $('#employeePickerTable_filter input[type=search]').val();
				var match = true;

				if (search != '') {
					return (data[1].toLowerCase()).indexOf(search.toLowerCase()) >= 0 || (data[2].toLowerCase()).indexOf(search.toLowerCase()) >= 0;
				}

				return match;
			}
		);

		//Sort without No.
		t.on('order.dt search.dt', function () {
			t.column(0, {
				search: 'applied',
				order: 'applied'
			}).nodes().each(function (cell, i) {
				cell.innerHTML = i + 1;
			});
		}).draw();
		
		//Onclick row.
		$('#employeePickerTable tbody').on('click', 'tr', function () {
			var t = $('#employeePickerTable').DataTable();
			var data = t.row( this ).data();
			EmployeePicker.selectedCallBack(data);
		});
    },
    
    hidePicker: function() {
        $('#employeePickerModal').modal('hide');
		$('#employeePickerTable_filter input[type=search]').val('');
	},

	setSearchEvent: function() {
		$('input[aria-controls=employeePickerTable]').unbind('keydown');
		$('input[aria-controls=employeePickerTable]').on('keydown', function(event){
			if (event.keyCode === 13) {
				event.preventDefault();
				var trs = $('#employeePickerTable tbody tr');
				if (trs.length === 1 && !$(trs[0]).hasClass('dataTables_empty')) {
					$(trs[0]).click();
				}
			}
		});
	},

	selectedCallBack: function(data) {},
}