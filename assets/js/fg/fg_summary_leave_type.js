$(document).ready(function () {
	initDatepicker('.datepicker');

	$('#btnSearch').on('click', function(){
		search();
	});

	$('#btnClear').on('click', function(){
		clear();
	});

	search();
});

function clear() {
	$('#ctrStartDate').val('');
	$('#ctrEndDate').val('');
	$('#ctrDepartment').val('');
}

function search() {
	var start_date = $('#ctrStartDate').val();
	var end_date = $('#ctrEndDate').val();
	var department = $('#ctrDepartment').val();

	showLoading();
	$.ajax({
		url: $('#base_url').val() + 'fg_summary_leave_type/get_summary_ajax?start_date=' + encodeString(start_date) + '&end_date=' + encodeString(end_date) + '&department=' + encodeString(department),
		type: 'GET',
		success: function(resp) {
			if (resp.success) {
				var data = resp.data;
				var header = 'Summary Leave of Leave Type';
				var sub_header = getSubHeader();
				var keys = ['ab', 'al', 'asl', 'cl', 'co', 'dfl', 'ma', 'ml', 'mrl', 'rl', 'sl', 'slh'];
				var series = [];

				if (data.length > 0) {
					series[0] = [];
					for (var i = 0; i < keys.length; i++) {
						series[0][i] = data[0][keys[i]];
						keys[i] = keys[i].toUpperCase();
					}
				}
				
				renderBarChart('.ct-chart', keys, series, header, sub_header);
				hideLoading();
			}
		},
		fail: function(resp) {
			hideLoading();
		}
	});
}

function getSubHeader() {
	var start_date = $('#ctrStartDate').val()
	var end_date = $('#ctrEndDate').val();

	if (start_date != '' && end_date != '') {
		return start_date + ' - ' + end_date;
	} else if (start_date != '') {
		return 'From ' + start_date;
	} else if (end_date != '') {
		return 'To ' + end_date;
	}

	return '';
}