$(document).ready(function () {
	initDatepicker('.datepicker');

	$('#btnSearch').on('click', function(){
		search();
	});

	$('#btnClear').on('click', function(){
		clear();
	});

	$('#btnEditorSave').on('click', function(){
		updateItem();
	});

	$('#btnDiligence').on('click', function(){
		createOrUpdateDiligence();
	});

	$('#ctrEmployeeID').on('keyup', function(event){
		if (event.keyCode === 13) {
			search();
		}
	});

	$('#ctrEmployeeID').focus();

	setDefaultCtr();
	autoSearchForMember();
});

function autoSearchForMember() {
	if ($('#isMember').val() == '1') {
		search();
	}
}

function createOrUpdateDiligence() {
	var monthly_data_id = $('#monthly_data_id').val();
	var ems_no =  $('#dspEmployeeNo').val();
	var year = $('#dspYear').val();
	var month = $('#dspMonth').val();
	var diligence = $('#monthly_data_diligence').val();

	if (validateCreateOrUpdateDiligence()) {
		var item = {'id' : monthly_data_id, 'ems_no' : ems_no, 'year' : year, 'month' : month, 'diligence' : diligence };

		showLoading();
		$.ajax({
			url: 'fg_time/create_or_update_diligence_ajax',
			type: 'POST',
			data: item,
			success: function(resp) {
				hideLoading();
				showMessageModal('บันทึกข้อมูลสำเร็จแล้ว');
			},
			error: function(resp) {
				console.log(resp);
				hideLoading();
				showMessageModal('พบข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้!');
			},
		});

	}
}

function validateCreateOrUpdateDiligence() {
	if ($('#monthly_data_diligence').val() == '') {
		showMessageModal('กรุณาระบุ Diligence');
		return false;
	}

	if ($('#dspYear').val() == '') {
		showMessageModal('ไม่พบข้อมูล ปี กรุณาทำรายการใหม่อีกครั้ง');
		return false;
	}

	if ($('#dspMonth').val() == '') {
		showMessageModal('ไม่พบข้อมูล เดือน กรุณาทำรายการใหม่อีกครั้ง');
		return false;
	}

	return true;
}

function setDefaultCtr() {
	var now = new Date()
	var date = now.getDate();
	var month = now.getMonth() + 1;
	var year = now.getFullYear();

	if (month == 12 && date > 20) {
		year += 1;
	}

	$('#ctrMonth').val(getTimesheetCurrentMonth());
	$('#ctrYear').val(year);
}

function search() {
	if (validateSearch()) {
		var year = $('#ctrYear').val();
		var month = $('#ctrMonth').val();
		var ems_no = encodeString($('#ctrEmployeeID').val());

		clearResult();
		showLoading();
		$.ajax({
			url: 'fg_time/get_employee_info_ajax?ems_no=' + ems_no + '&year=' + year + '&month=' + month,
			type: 'GET',
			success: function(resp) {
				if (resp.success) {
					if (resp.data.length > 0) {
						var result = resp.data[0];
						$('#dspEmployeeID').val(result['employee_id']);
						$('#dspEmployeeNo').val(result['ems_no']);
						$('#dspEmployeeType').val(result['employee_type_name']);
						$('#dspEmployeeName').val(result['fullname_th']);
						$('#dspEmployeeDepartment').val(result['department_id']);
						$('#dspShift').val(result['shift_name']);
						$('#dspYear').val(year);
						$('#dspMonth').val(month);
					}

					if (resp.monthly_data.length > 0) {
						var result = resp.monthly_data[0];
						$('#montly_data_id').val(result['id']);
						$('#monthly_data_diligence').val(result['diligence']);
					} else {
						$('#montly_data_id').val('');
						$('#monthly_data_diligence').val('');
					}

					loadDatatable(ems_no, year, month);
				} else {
					hideLoading();
					showMessageModal(resp.message);
				}
			},
			error: function(resp) {
				showMessageModal('ไม่พบข้อมูลพนักงาน!');
			},
		});
	}
}

function validateSearch() {
	if ($('#ctrEmployeeID').val() == '') {
		showMessageModal('กรุณาระบุ Employee ID');
		return false;
	}

	return true;
}

function showResult() {
	$('#resultContainer').show();
}

function hideResult() {
	$('#resultContainer').hide();
}

function clear() {
	hideResult();
	clearCriterias();
	clearResult();
}

function clearCriterias() {
	$('#ctrYear').val('0');
	$('#ctrMonth').val('0');
	$('#ctrEmployeeID').val('');
	$('#ctrEmployeeName').val('');
}

function clearResult() {
	$('#dspEmployeeID').val('');
	$('#dspEmployeeType').val('');
	$('#dspEmployeeName').val('');
	$('#dspEmployeeDepartment').val('');
}

function loadDatatable(ems_no, year, month) {
	$("#rstable").dataTable().fnDestroy();
	 
	showLoading();
	var t = $('#rstable').DataTable({
		ajax: 'fg_time/get_employee_timesheet_ajax?ems_no=' + ems_no + '&year=' + year + '&month=' + month,
		searching: false,
		ordering: false,
		lengthChange: false,
		paging: false,
		columns: [{
				'data': 'id',
				'render': function (data, type, row, meta) {
					return '<span class="item-no">' + (meta.row + 1) + '</span>' +
						'<input type="hidden" class="item-id" value="' + data + '"/>' +
						'<input type="hidden" class="item-holiday-type" value="' + strNotNull(row['holiday_type']) + '"/>' +
						'<span class="item-total-hours d-none">' + parseFloatOrNumeric(row['total_hours']) + '</span>' +
						'<span class="item-working-day d-none">' + parseFloatOrNumeric(row['working_day']) + '</span>';
				}
			},
			{
				'data': 'date',
				'render': function (data, type, row, meta) {
					return '<span class="item-date">' + data + '</span>';
				}
			},
			{
				'data': 'start_work',
				'render': function (data, type, row, meta) {
					return '<span class="item-start-work' + (parseFloatOrNumeric(row['total_hours']) < 8 ? ' ts-text-color-2' : '') + '">' + strNotNull(data) + '</span>';
				}
			},
			{
				'data': 'end_work',
				'render': function (data, type, row, meta) {
					return '<span class="item-end-work' + (parseFloatOrNumeric(row['total_hours']) < 8 ? ' ts-text-color-2' : '') + '">' + strNotNull(data) + '</span>';
				}
			},
			{
				'data': 'leave_type',
				'render': function (data, type, row, meta) {
					return '<span class="item-leave-type' + (parseFloatOrNumeric(row['total_hours']) < 8 ? ' ts-text-color-2' : '') + '">' + strNotNull(data) + '</span>';
				}
			},
			/*
			{
				'data': 'total_hours',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-total-hours' + (parseFloatOrNumeric(row['total_hours']) < 8 ? ' ts-text-color-2' : '') + '' + (n > 0 ? ' ts-color-1' : ' ts-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'working_day',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-working-day' + (parseFloatOrNumeric(row['total_hours']) < 8 ? ' ts-text-color-2' : '') + '' + (n > 0 ? ' ts-color-1' : ' ts-color-2') + '">' + n + '</span>';
				}
			},
			*/
			{
				'data': 'night_shift',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-night-shift' + (parseFloatOrNumeric(row['total_hours']) < 8 ? ' ts-text-color-2' : '') + '' + (n > 0 ? ' nsf-color-1' : ' nsf-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'food_travel_day',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-food-travel-day' + (parseFloatOrNumeric(row['total_hours']) < 8 ? ' ts-text-color-2' : '') + '' + (n > 0 ? ' ftd-color-1' : ' ftd-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'ot1',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-ot1' + (parseFloatOrNumeric(row['total_hours']) < 8 ? ' ts-text-color-2' : '') + '' + (n > 0 ? ' ot-color-3' : ' ot-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'ot15',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-ot15' + (parseFloatOrNumeric(row['total_hours']) < 8 ? ' ts-text-color-2' : '') + '' + (n > 0 ? ' ot-color-3' : ' ot-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'ot2',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-ot2' + (parseFloatOrNumeric(row['total_hours']) < 8 ? ' ts-text-color-2' : '') + '' + (n > 0 ? ' ot-color-3' : ' ot-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'ot3',
				'render': function (data, type, row, meta) {
					var n = parseFloatOrNumeric(data);
					return '<span class="item-ot3' + (parseFloatOrNumeric(row['total_hours']) < 8 ? ' ts-text-color-2' : '') + '' + (n > 0 ? ' ot-color-3' : ' ot-color-2') + '">' + n + '</span>';
				}
			},
			{
				'data': 'remark',
				'render': function (data, type, row, meta) {
					return '<span class="item-remark' + (parseFloatOrNumeric(row['total_hours']) < 8 ? ' ts-text-color-2' : '') + '">' + strNotNull(data) + '</span>';
				}
			},
			{
				'data': "userid",
				'render': function (data, type, row, meta) {
					var base_url = $('#base_url').val();
					var id = encodeURIComponent(btoa(data));
					var isAdmin = $('#isAdmin').val();
					var isSubContract = $('#isSubContract').val();
					return (isAdmin == '1' || isSubContract == '1') ? '<a class="btn btn-default btn-icon-sm" onclick="editItem(this)"><i class="far fa-edit"></i></a>' : '';
				}
			},
		],
		columnDefs: [{
				"targets": 0,
				"className": "text-center"
			},{
				"targets": 1,
				"className": "text-center"
			},{
				"targets": 2,
				"className": "text-center"
			},{
				"targets": 3,
				"className": "text-center"
			},{
				"targets": 4,
				"className": "text-center"
			},{
				"targets": 5,
				"className": "text-right"
			},{
				"targets": 6,
				"className": "text-right"
			},{
				"targets": 7,
				"className": "text-right"
			},{
				"targets": 8,
				"className": "text-right"
			},{
				"targets": 9,
				"className": "text-right"
			},{
				"targets": 10,
				"className": "text-right"
			},{
				"targets": 12,
				"className": "text-center"
			},
		],
		oLanguage: {
			"sInfo": "" //Hide showing num of records
		},
		fnDrawCallback: function (settings) {
			//initExportButtons('#rstable', 'Billings', 'billings');
			
		},
		initComplete: function (settings, json) {
			calculateSummary();
			hideLoading();
			showResult();
			highlightHolidays();
		}
	});

	//Sort without No.
	/*
	t.on('order.dt search.dt', function () {
		t.column(0, {
			search: 'applied',
			order: 'applied'
		}).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();
	*/
}

function highlightHolidays() {
	var trs = $('#rstable tr');
	for (var i = 0; i < trs.length; i++) {
		var holiday_type = $(trs[i]).find('.item-holiday-type').val();

		$(trs[i]).removeClass('tr-holiday-1');
		$(trs[i]).removeClass('tr-holiday-2');
		$(trs[i]).removeClass('tr-holiday-3');
		$(trs[i]).removeClass('tr-holiday-4');
		
		if (holiday_type == '1') {
			$(trs[i]).addClass('tr-holiday-1');
		} else if (holiday_type == '2') {
			$(trs[i]).addClass('tr-holiday-2');
		} else if (holiday_type == '3') {
			$(trs[i]).addClass('tr-holiday-3');
		} else if (holiday_type == '4') {
			$(trs[i]).addClass('tr-holiday-4');
		}
	}
}

function calculateSummary() {
	var data = $('#rstable').DataTable().data();
	var sum_total_hours = 0;
	var sum_working_day = 0;
	var sum_night_shift = 0;
	var sum_food_travel_day = 0;
	var sum_ot1 = 0;
	var sum_ot15 = 0;
	var sum_ot2 = 0;
	var sum_ot3 = 0;

	for (var i = 0; i < data.length; i++) {
		sum_total_hours += parseFloatOrNumeric(data[i]['total_hours']);
		sum_working_day += parseFloatOrNumeric(data[i]['working_day']);
		sum_night_shift += parseFloatOrNumeric(data[i]['night_shift']);
		sum_food_travel_day += parseFloatOrNumeric(data[i]['food_travel_day']);
		sum_ot1 += parseFloatOrNumeric(data[i]['ot1']);
		sum_ot15 += parseFloatOrNumeric(data[i]['ot15']);
		sum_ot2 += parseFloatOrNumeric(data[i]['ot2']);
		sum_ot3 += parseFloatOrNumeric(data[i]['ot3']);
	}

	//$('#sp_sum_total_hours').text(sum_total_hours);
	$('#sp_sum_working_day').text(sum_working_day);
	$('#sp_sum_night_shift').text(sum_night_shift);
	$('#sp_sum_food_travel_day').text(sum_food_travel_day);
	$('#sp_sum_ot1').text(sum_ot1);
	$('#sp_sum_ot15').text(sum_ot15);
	$('#sp_sum_ot2').text(sum_ot2);
	$('#sp_sum_ot3').text(sum_ot3);

	$('#sp_sum_night_shift').removeClass('nsf-color-1');
	if (sum_night_shift > 0) {
		$('#sp_sum_night_shift').addClass('nsf-color-1');
	}

	$('#sp_sum_food_travel_day').removeClass('ftd-color-1');
	if (sum_food_travel_day > 0) {
		$('#sp_sum_food_travel_day').addClass('ftd-color-1');
	}

	$('#sp_sum_ot1').removeClass('ot-color-3');
	if (sum_ot1 > 0) {
		$('#sp_sum_ot1').addClass('ot-color-3');
	}

	$('#sp_sum_ot15').removeClass('ot-color-3');
	if (sum_ot15 > 0) {
		$('#sp_sum_ot15').addClass('ot-color-3');
	}

	$('#sp_sum_ot2').removeClass('ot-color-3');
	if (sum_ot2 > 0) {
		$('#sp_sum_ot2').addClass('ot-color-3');
	}

	$('#sp_sum_ot3').removeClass('ot-color-3');
	if (sum_ot3 > 0) {
		$('#sp_sum_ot3').addClass('ot-color-3');
	}
	
}

function editItem(e) {
	var tr = $(e).parent().parent();
	var id = $(tr).find('.item-id').val();
	var no = $(tr).find('.item-no').text();
	var date = $(tr).find('.item-date').text();
	var startWork = $(tr).find('.item-start-work').text();
	var endWork = $(tr).find('.item-end-work').text();
	var leaveType = $(tr).find('.item-leave-type').text();
	var totalHours = $(tr).find('.item-total-hours').text();
	var workingDay = $(tr).find('.item-working-day').text();
	var nightShift = $(tr).find('.item-night-shift').text();
	var foodTravelDay = $(tr).find('.item-food-travel-day').text();
	var ot1 = $(tr).find('.item-ot1').text();
	var ot15 = $(tr).find('.item-ot15').text();
	var ot2 = $(tr).find('.item-ot2').text();
	var ot3 = $(tr).find('.item-ot3').text();
	var remark = $(tr).find('.item-remark').text();


	var employeeID = $('#dspEmployeeID').val();
	var employeeNo = $('#dspEmployeeNo').val();
	var employeeName = $('#dspEmployeeName').val();
	var sprtStartWork = startWork.split(':');
	var sprtEndWork = endWork.split(':');
	var startWorkHour = '';
	var startWorkMinute = '';
	var endWorkHour = '';
	var endWorkMinute = '';

	if (sprtStartWork.length == 2) {
		startWorkHour = sprtStartWork[0];
		startWorkMinute = sprtStartWork[1];
	}

	if (sprtEndWork.length == 2) {
		endWorkHour = sprtEndWork[0];
		endWorkMinute = sprtEndWork[1];
	}

	var title = date + ' : ' + employeeNo + ' ' + employeeName;
	$('#editorModalTitle').text(title);
	$('#edtId').val(id);
	$('#edtNo').val(no);
	$('#edtDate').val(date);
	$('#edtStartWorkHour').val(startWorkHour);
	$('#edtStartWorkMinute').val(startWorkMinute);
	$('#edtEndWorkHour').val(endWorkHour);
	$('#edtEndWorkMinute').val(endWorkMinute);

	$('#edtLeaveType').val(leaveType);
	$('#edtTotalHours').val(totalHours);
	$('#edtWorkingDay').val(workingDay);
	$('#edtOt1').val(ot1);
	$('#edtOt15').val(ot15);
	$('#edtOt2').val(ot2);
	$('#edtOt3').val(ot3);
	$('#edtRemark').val(remark);

	checkRadio('edtNightShift', nightShift);
	checkRadio('edtFoodTravelDay', foodTravelDay);

	showEditorModal();
}

function showEditorModal() {
	$('#editorModal').modal('show');
}

function hideEditorModal() {
	$('#editorModal').modal('hide');
}

function updateItem() {
	if (validateUpdateItem()) {
		hideEditorModal();
		showLoading();

		var id = $('#edtId').val();
		var startWork = $('#edtStartWorkHour').val() + ':' + $('#edtStartWorkMinute').val();
		var endWork = $('#edtEndWorkHour').val() + ':' + $('#edtEndWorkMinute').val();
		var leaveType = $('#edtLeaveType').val();
		var totalHours = $('#edtTotalHours').val();
		var workingDay = $('#edtWorkingDay').val();
		var nightShift = $('input[name=edtNightShift]:checked').val();
		var foodTravelDay = $('input[name=edtFoodTravelDay]:checked').val();
		var ot1 = $('#edtOt1').val();
		var ot15 = $('#edtOt15').val();
		var ot2 = $('#edtOt2').val();
		var ot3 = $('#edtOt3').val();
		var remark = $('#edtRemark').val();

		if(startWork.length != 5) {
			startWork = '';
		}

		if(endWork.length != 5) {
			endWork = '';
		}

		var item = {
			'id' : id,
			'startWork' : startWork,
			'endWork' : endWork,
			'totalHours' : totalHours,
			'workingDay' : workingDay,
			'nightShift' : nightShift,
			'foodTravelDay' : foodTravelDay,
			'ot1' : ot1,
			'ot15' : ot15,
			'ot2' : ot2,
			'ot3' : ot3,
			'remark' : remark,
		};

		$.ajax({
			url: 'fg_time/update_employee_timesheet_ajax',
			type: 'POST',
			data: item,
			success: function(resp) {
				search();
			},
			error: function(resp) {
				console.log(resp);
				hideLoading();
				showMessageModal('พบข้อผิดพลาด ไม่สามารถแก้ไขข้อมูลได้!');
			},
		});
	}
}

function validateUpdateItem() {
	var timeStart = $('#edtStartWorkHour').val() + '' + $('#edtStartWorkMinute').val();
	var timeEnd = $('#edtEndWorkHour').val() + '' + $('#edtEndWorkMinute').val();

	if (timeStart.length != 0 && timeStart.length != 4) {
		showMessageModal('กรุณาระบุเวลาเข้า ชั่วโมง:นาที ให้ครบถ้วน');
		return false;
	}

	if (timeEnd.length != 0 && timeEnd.length != 4) {
		showMessageModal('กรุณาระบุเวลาออก ชั่วโมง:นาที ให้ครบถ้วน');
		return false;
	}

	return true;
}

function exportExcel() {
	var year = $('#ctrYear').val();
	var month = $('#ctrMonth').val();
	var employeeName = $('#ctrEmployeeName').val();
	var employeeNo = encodeString($('#ctrEmployeeID').val());

	$('#export_excel_form #year').val(year);
	$('#export_excel_form #month').val(month);
	$('#export_excel_form #employeeName').val(employeeName);
	$('#export_excel_form #employeeNo').val(employeeNo);
	$('#export_excel_form').submit();
}