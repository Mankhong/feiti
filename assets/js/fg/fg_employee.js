$(document).ready(function () {
	processDefaultMode();

	$('.view-mode-1-button').on('click', function () {
		processViewMode(1);
	});

	$('.view-mode-2-button').on('click', function () {
		processViewMode(2);
	});

	$('#action_search').on('keyup', function () {
		Dataitems.filterData(this.value);
	});

	$('input[name="resign_flag"]').on('click', function () {
		loadDatatable();
	});
});

function processResignDateLabel(resign_flag) {
	if (resign_flag == 'Y') {
		$('#resign_date_column_label').text('Resign Date');
	} else {
		$('#resign_date_column_label').text('Probation Date');
	}
}

function processDefaultMode() {
	var viewMode = localStorage.getItem('viewMode');
	if (viewMode) {
		processViewMode(viewMode);
	} else {
		processViewMode(1);
	}
}

//ViewModde 1 = table, 2 = item
function processViewMode(viewMode) {
	if (viewMode == 2) {
		$('.view-mode-1').hide();
		loadDataitems();
		$('.view-mode-2').show();

	} else {
		$('.view-mode-2').hide();
		loadDatatable();
		hideSearch();
		$('.view-mode-1').show();
	}
	$('#view_mode').val(viewMode);
	localStorage.setItem('viewMode', viewMode);
}

function hideSearch() {
	$('#rstable_filter').css('display', 'none');
}

function loadDatatable() {
	$("#rstable").dataTable().fnDestroy();
	var resign_flag = $('input[name="resign_flag"]:checked').val();
	processResignDateLabel(resign_flag);

	showLoading();
	var t = $('#rstable').DataTable({
		ajax: 'fg_employee/get_employee_list_ajax?resign_flag=' + resign_flag,
		searching: true,
		ordering: true,
		order: [
			[1, 'asc']
		],
		lengthChange: false,
		pageLength: 20,
		columns: [{
				'data': null,
				'render': function (data, type, row, meta) {
					return meta.row + 1;
				}
			},
			{
				'data': 'ems_no'
			},
			{
				'data': 'fullname_th'
			},
			{
				'data': 'department_id'
			},
			{
				'data': 'job_position_name'
			},
			{
				'data': 'fm_startdate'
			},
			{
				'data': 'fm_birthdate'
			},
			{
				'data': 'pid'
			},
			{
				'data': 'house_no'
			},
			{
				'data': 'certificate_level_name'
			},
			{
				'data': 'fm_resign_date',
				'render': function (data, type, row, meta) {
					return resign_flag == 'Y' ? row['fm_resign_date'] : row['fm_resign_probation_date'];
				}
			},
			/* {
				'data': 'last_update_user'
			}, */
		],
		columnDefs: [{
				"targets": 0,
				"className": "text-center"
			},
			{
				"targets": 1,
				"className": "text-center"
			},
			{
				"targets": 5,
				"className": "text-center"
			},
			{
				"targets": 6,
				"className": "text-center"
			},
			{
				"targets": 7,
				"className": "text-center"
			},
		],
		initComplete: function(settings, json){
			hideSearch();
            hideLoading();
        }
	});

	//Sort without No.
	t.on('order.dt search.dt', function () {
		t.column(0, {
			search: 'applied',
			order: 'applied'
		}).nodes().each(function (cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();

	//=== Custom Filter
	$.fn.dataTable.ext.search.push(
		function (settings, data, dataIndex) {
			var match = true;
			var search = $('#action_search').val();
			if (search != '') {
				match = (data[1].toLowerCase().indexOf(search.toLowerCase()) >= 0 || data[2].toLowerCase().indexOf(search.toLowerCase()) == 0);
			}

			return match;
		}
	);

	$('#action_search').keyup(function (event) {
		if ($('#view_mode').val() == 1) {
			t.draw();
		}
	});
	//=== End Custom Filter

	$('#rstable tbody').on('click', 'tr', function () {
		var t = $('#rstable').DataTable();
		var data = t.row(this).data();
		var id = encodeString(data['employee_id']);
		window.location.href = $('#base_url').val() + 'fg_employee/edit/' + id;
	});

}

function loadDataitems() {
	var resign_flag = $('input[name="resign_flag"]:checked').val();
	Dataitems.loadData('fg_employee/get_employee_list_ajax?resign_flag=' + resign_flag, null, 'dataitems', 'employee_id', 'image_path', 'employee_id', ['fullname_th', 'department_name', 'employee_type_name'], ['Name', 'Department', 'Employee Type'], 'onclickItem');
}

function onclickItem(id) {
	var base_url = $('#base_url').val();
	window.location.href = base_url + 'fg_employee/edit/' + id;
}

function exportExcel() {
	var resign_flag = $('input[name="resign_flag"]:checked').val();
	var delimiter = '&88&';
	var rows = $('#rstable').DataTable().rows( {filter : 'applied'}).data();
	var value = '';
	for (var i = 0; i < rows.length; i++) {
		if (i > 0) {
			value += delimiter;
		}
		value += rows[i].employee_id;
	}

	$('#export_excel_form #employee_ids').val(value);
	$('#export_excel_form #resign_flag').val(resign_flag);
	$('#export_excel_form').submit();
}