// common.js required.

$(document).ready(function () {
	pzInitEvent();
});

function pzClearEvent() {
	$("div.pz-input-editor").unbind("click");
	$("input.pz-input-editor").unbind("keyup");
	$("input.pz-input-editor").unbind("blur");
}

function pzInitEvent() {
	$("div.pz-input-editor").on("click", function(){
		pzEdit($(this).attr("data-id"));
	});

	$("input.pz-input-editor").on("keyup", function(event){
		if (event.keyCode === 13) {
			$(this).blur();
		}
	});

	$("input.pz-input-editor").on("blur", function(event){
		pzDisplay($(this).attr("data-id"));
	});
}

function pzEdit(id) {
	var div = pzGetDiv(id);
	var input = pzGetInput(id);

	var value = $(div).text();
	if ($(div).attr("data-type") == "currency") {
		value = formatCurrencyToNumber(value);
		if (value == 0) {
			value = '';
		}
	}

	$(input).val(value);
	$(div).hide();
	$(input).show();
	$(input).focus();
}

function pzDisplay(id) {
	var div = pzGetDiv(id);
	var input = pzGetInput(id);

	var value = $(input).val();
	if ($(div).attr("data-type") == "currency") {
		var decimal = parseFloatOrNumeric($(div).attr("data-decimal"));
		value = formatCurrency(value, decimal);
	}

	$(div).text(value);
	$(div).show();
	$(input).hide();
}

function pzGetDiv(id) {
	var divs = $("div.pz-input-editor");
	for (var i = 0; i < divs.length; i++) {
		if ($(divs[i]).attr("data-id") == id) {
			return divs[i];
		}
	}
	return null;
}

function pzGetInput(id) {
	var inputs = $("input.pz-input-editor");
	for (var i = 0; i < inputs.length; i++) {
		if ($(inputs[i]).attr("data-id") == id) {
			return inputs[i];
		}
	}
	return null;
}