$(document).ready(function () {
	
});

function selectImage() {
	$('#image_upload').click();
}

function displayImage(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#image_img')
				.attr('src', e.target.result);
		};

		reader.readAsDataURL(input.files[0]);
	}
}

function showImage() {
	$('#imageModalImage').attr('src', $('#image_img').attr('src'));
	$('#imageModal').modal('show');
}